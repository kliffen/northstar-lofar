// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Mar 3, 2005
 *
 *
 */
package eu.radionet.northstar.business.targetlist;

import java.io.Serializable;
import java.util.Hashtable;

/**
 * @author Anton Smit
 */
public class TargetFileBean implements Serializable
{
	protected Hashtable fields = new Hashtable();
    protected String fieldName = null;
    protected Double ra = null;
    protected Double dec = null;
    protected String Epoch = null;
    protected Integer exposureTime = null; // in seconds;
    protected Double exposureTimeDouble = null; // in hours;
    protected String field1 = null;
    // ING mag
    // JCMT priority
    protected String field2 = null;
    // ING Colour
    
   
    /**
     * @return Returns the dec.
     */
    public Double getDec()
    {
        return dec;
    }
    /**
     * @param dec The dec to set.
     */
    public void setDec(Double dec)
    {
        this.dec = dec;
    }
    /**
     * @return Returns the epoch.
     */
    public String getEpoch()
    {
        return Epoch;
    }
    /**
     * @param epoch The epoch to set.
     */
    public void setEpoch(String epoch)
    {
        Epoch = epoch;
    }
    /**
     * @return Returns the exposureTime.
     */
    public Integer getExposureTime()
    {
        return exposureTime;
    }
    /**
     * @param exposureTime The exposureTime to set.
     */
    public void setExposureTime(Integer exposureTime)
    {
        this.exposureTime = exposureTime;
    }
    /**
     * @return Returns the fieldName.
     */
    public String getFieldName()
    {
        return fieldName;
    }
    /**
     * @param fieldName The fieldName to set.
     */
    public void setFieldName(String fieldName)
    {
        this.fieldName = fieldName;
    }
    /**
     * @return Returns the ra.
     */
    public Double getRa()
    {
        return ra;
    }
    /**
     * @param ra The ra to set.
     */
    public void setRa(Double ra)
    {
        this.ra = ra;
    }
    /**
     * @return Returns the field1.
     */
    public String getField1()
    {
        return field1;
    }
    /**
     * @param field1 The field1 to set.
     */
    public void setField1(String field1)
    {
        this.field1 = field1;
    }
    /**
     * @return Returns the field2.
     */
    public String getField2()
    {
        return field2;
    }
    /**
     * @param field2 The field2 to set.
     */
    public void setField2(String field2)
    {
        this.field2 = field2;
    }
	public Hashtable getFields() {
		return fields;
	}
	public void setFields(Hashtable fields) {
		this.fields = fields;
	}
	public Double getExposureTimeDouble() {
		return exposureTimeDouble;
	}
	public void setExposureTimeDouble(Double exposureTimeDouble) {
		this.exposureTimeDouble = exposureTimeDouble;
	}
}
