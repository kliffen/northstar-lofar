<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />

<html:form action="/processStatistics" method="POST">

<TABLE width="100%">
<TR>
	<TD align="center" class="bold">Statistics:</TD>
</TR>	

<TR>
	<TD align="center"> &nbsp; </TD>
</TR>	
<TR>
	<TD align="center"> Total: </TD>
</TR>
<TR>
	<TD align="center"><TABLE align="center" class="projecttable">
	<TR><TD colspan="3" width="300" class="spacer"/></TR>
	<TR><TD width="50"/><TD width="150">Amount of proposals &nbsp; </TD><TD><bean:write name="statisticsForm" property="totalProposals"/></TD></TR>
	<TR><TD width="50"/><TD width="150">Amount of targets &nbsp; </TD><TD><bean:write name="statisticsForm" property="totalTargets"/></TD></TR>
	<TR><TD width="50"/><TD width="150">Amount of time </TD><TD><bean:write name="statisticsForm" property="totalTime"/>h</TD></TR>
	<TR><TD width="50"/><TD width="150">Amount of storage </TD><TD><bean:write name="statisticsForm" property="totalStorage"/>GB</TD></TR>
	</TABLE>
	<br>
	</TD>
</TR>
<logic:notEmpty name="statisticsForm" property="statisticsBeans">
	<TR>
		<TD align="center"> categories: </TD>
	</TR>

	<TR>
		<TD>
			<TABLE align="center" class="projecttable">
					<TR><TD  width="150">Name:</TD><TD  width="75">proposals</TD><TD  width="75">targets</TD><TD  width="75">time</TD><TD  width="75">Storage</TD></TR>
				<%
			int i=0;
			String styleClass = "";
			%>
			<logic:iterate id="statisticsBean" property="statisticsBeans" name="statisticsForm">
				 <%
	 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
	 			%>
	 			<TR>
				<TD class="tablefield<%=styleClass%>"><bean:write name="statisticsBean" property="insName"/></TD>
				<TD class="tablefield<%=styleClass%>"><bean:write name="statisticsBean" property="proposals"/></TD>
				<TD class="tablefield<%=styleClass%>"><bean:write name="statisticsBean" property="targets"/></TD>
				<TD class="tablefield<%=styleClass%>"><bean:write name="statisticsBean" property="time"/>h</TD>
				<TD class="tablefield<%=styleClass%>"><bean:write name="statisticsBean" property="storage"/>GB</TD>
				</TR>
	 		</logic:iterate>
			</TABLE>
				<TR>
					<TD align="center" class="spacer"> &nbsp; </TD>
				</TR>
		</TD>
	</TR>
	
</logic:notEmpty>

<TR>
	<TD align="center" class="spacer"> &nbsp; </TD>
</TR>	
	
<logic:notEmpty name="statisticsForm" property="statisticsTotalsBeans">
		<TR>
			<TD align="center"> modes: </TD>
		</TR>
	
		<TR><TD align="center">
		<TABLE align="center" class="projecttable">
			<tr><td width="150" >Mode: </td><td width="75">proposals : </td><td width="75">targets : </td><td width="75">time : </td><td width="75">storage : </td></tr>
			<%
			int i=0;
			String styleClass = "";
			%>
			<logic:iterate id="statisticsBean" property="statisticsTotalsBeans"  name="statisticsForm">
			   <%
	 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
	 			%>
				<TR>
						<TD class="tablefield<%=styleClass%>"><bean:write name="statisticsBean" property="insName"/></TD>
						<TD class="tablefield<%=styleClass%>"><bean:write name="statisticsBean" property="proposals"/></TD>	
						<TD class="tablefield<%=styleClass%>"><bean:write name="statisticsBean" property="targets"/></TD>
						<TD class="tablefield<%=styleClass%>"><bean:write name="statisticsBean" property="time"/>m</TD>
						<TD class="tablefield<%=styleClass%>"><bean:write name="statisticsBean" property="storage"/></TD>		
				</TR>
			</logic:iterate>
		</TABLE></TD></TR>
	</logic:notEmpty>

<TR>
	<TD align="center"> &nbsp; </TD>
</TR>


<TR>
	<TD align="center"><html:cancel >Ok</html:cancel></TD>
</TR>	
</TABLE>

</html:form>
	