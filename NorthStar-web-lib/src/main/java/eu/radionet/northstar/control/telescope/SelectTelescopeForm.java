// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SelectTelescopeForm.java 
 *
 * Created on Feb 17, 2005
 *
 * Version $Id: SelectTelescopeForm.java,v 1.2 2006-08-04 09:45:01 holties Exp $
 *
 */
package eu.radionet.northstar.control.telescope;

import java.util.Set;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * The SelectTelescopeForm provides
 *
 * @author Nico Vermaas
 *
 */
public class SelectTelescopeForm extends ActionForm {
    private String proposalId = null;
	protected String selectedCommunity = null;
	protected Set communities = new TreeSet();
	protected String selectedTelescope = null;
	protected Set telescopes = new TreeSet();
	protected String selectedCategory = null;
	protected Set semesters = new TreeSet();
	protected String selectedSemesterId = null;
	protected Set categories = new TreeSet();  
	protected String selectButton = null;
    protected boolean noFacilityChoice = false;
	protected boolean noCommunityChoice = false;
	protected boolean noSemesterChoice = false;	
	protected boolean noCategoryChoice = false;	
    protected String selectedTelescopeLabel = null;
	protected String selectedCommunityLabel = null;
	protected String selectedCategoryLabel = null;
	protected String selectedSemesterLabel = null;
	protected String deadLineTime = null;
	protected boolean immediate = false;
    
    protected boolean acceptsProposals = true;
	/**
	 * @return Returns the telescopes.
	 */
	public Set getTelescopes() {
		return telescopes;
	}
	/**
	 * @param telescopes The telescopes to set.
	 */
	public void setTelescopes(Set telescopes) {
		this.telescopes = telescopes;
	}


    /**
     * @return Returns the proposalId.
     */
    public String getProposalId() {
        return proposalId;
    }
    /**
     * @param proposalId The proposalId to set.
     */
    public void setProposalId(String proposalId) {
        this.proposalId = proposalId;
    }
	public Set getCategories() {
		return categories;
	}
	public void setCategories(Set categories) {
		this.categories = categories;
	}
	public Set getCommunities() {
		return communities;
	}
	public void setCommunities(Set communities) {
		this.communities = communities;
	}
	public String getSelectedCommunity() {
		return selectedCommunity;
	}
	public void setSelectedCommunity(String selectedCommunity) {
		this.selectedCommunity = selectedCommunity;
	}
	public String getSelectedSemesterId() {
		return selectedSemesterId;
	}
	public void setSelectedSemesterId(String selectedSemesterId) {
		this.selectedSemesterId = selectedSemesterId;
	}
	public String getSelectedTelescope() {
		return selectedTelescope;
	}
	public void setSelectedTelescope(String selectedTelescope) {
		this.selectedTelescope = selectedTelescope;
	}
	public Set getSemesters() {
		return semesters;
	}
	public void setSemesters(Set semesters) {
		this.semesters = semesters;
	}
	public String getSelectButton() {
		return selectButton;
	}
	public void setSelectButton(String selectButton) {
		this.selectButton = selectButton;
	}
	public String getSelectedCategory() {
		return selectedCategory;
	}
	public void setSelectedCategory(String selectedCategory) {
		this.selectedCategory = selectedCategory;
	}
    public boolean isNoFacilityChoice() {
        return noFacilityChoice;
    }
    public void setNoFacilityChoice(boolean noFacilityChoice) {
        this.noFacilityChoice = noFacilityChoice;
    }
	public boolean isNoCategoryChoice() {
		return noCategoryChoice;
	}
	public void setNoCategoryChoice(boolean noCategoryChoice) {
		this.noCategoryChoice = noCategoryChoice;
	}
	public boolean isNoCommunityChoice() {
		return noCommunityChoice;
	}
	public void setNoCommunityChoice(boolean noCommunityChoice) {
		this.noCommunityChoice = noCommunityChoice;
	}
	public boolean isNoSemesterChoice() {
		return noSemesterChoice;
	}
	public void setNoSemesterChoice(boolean noSemesterChoice) {
		this.noSemesterChoice = noSemesterChoice;
	}
    public String getSelectedTelescopeLabel() {
        return selectedTelescopeLabel;
    }
    public void setSelectedTelescopeLabel(String selectedTelescopeLabel) {
        this.selectedTelescopeLabel = selectedTelescopeLabel;
    }
	public String getSelectedCategoryLabel() {
		return selectedCategoryLabel;
	}
	public void setSelectedCategoryLabel(String selectedCategoryLabel) {
		this.selectedCategoryLabel = selectedCategoryLabel;
	}
	public String getSelectedCommunityLabel() {
		return selectedCommunityLabel;
	}
	public void setSelectedCommunityLabel(String selectedCommunityLabel) {
		this.selectedCommunityLabel = selectedCommunityLabel;
	}
	public String getSelectedSemesterLabel() {
		return selectedSemesterLabel;
	}
	public void setSelectedSemesterLabel(String selectedSemesterLabel) {
		this.selectedSemesterLabel = selectedSemesterLabel;
	}
	
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
		if (this.selectButton != null){
	        if (AstronValidator.isBlankOrNull(this.selectedTelescope)) {
	            errors.add("selectedTelescope", new ActionMessage(
	                    "error.required.telescope"));
	        } 				
		}


        return errors;
    }
	public String getDeadLineTime() {
		return deadLineTime;
	}
	public void setDeadLineTime(String deadLineTime) {
		this.deadLineTime = deadLineTime;
	}
	public boolean isImmediate() {
		return immediate;
	}
	public void setImmediate(boolean immediate) {
		this.immediate = immediate;
	}
    
    /**
     * @return Returns the acceptsProposals.
     */
    public boolean isAcceptsProposals()
    {
        return acceptsProposals;
    }
    /**
     * @param acceptsProposals The acceptsProposals to set.
     */
    public void setAcceptsProposals(boolean acceptsProposals)
    {
        this.acceptsProposals = acceptsProposals;
    }
 
    
}
