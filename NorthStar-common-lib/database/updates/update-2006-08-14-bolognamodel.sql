alter table `additionalissues` change column `previousprojects` `previousallocationsspecifics` text;
alter table `additionalissues` change column `relatedprojects` `linkedproposalselsewherespecifics` text;
alter table `additionalissues` add column `linkedproposalselsewhere` tinyint(1) NOT NULL default '0' after `linkedproposalsspecifics`;
alter table `additionalissues` add column `previousallocations`  tinyint(1) NOT NULL default '0' after `linkedproposalselsewherespecifics`;

UPDATE `additionalissues` SET `linkedproposalselsewhere`=1 WHERE `linkedproposalselsewherespecifics` IS NOT NULL AND trim(`linkedproposalselsewherespecifics`) != '' ;
UPDATE `additionalissues` SET `previousallocations`=1 WHERE `previousallocationsspecifics` IS NOT NULL AND trim(`previousallocationsspecifics`) != '' ;
UPDATE `additionalissues` SET `additionalremarks`=CONCAT(`additionalremarks`, ' ', `relatedpublications`, ' ', `data_analyse`);

alter table `thesis` add column `student_level` varchar(255) after `student_name`;

ALTER TABLE `additionalissues` DROP COLUMN `relatedpublications`;
ALTER TABLE `additionalissues` DROP COLUMN `data_analyse`;
