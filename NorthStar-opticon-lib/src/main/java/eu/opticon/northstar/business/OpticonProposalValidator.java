// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nl.astron.util.AstronValidator;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.ProposalValidator;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.ProposalError;
import eu.radionet.northstar.data.entities.Resource;
import eu.radionet.northstar.data.entities.Target;

/**
 * This class validates:
 * 
 * 
 */
public class OpticonProposalValidator extends ProposalValidator {
    
       
    /**
	 * Validates the telescope specific observing request
	 * - if the proposal is long term, details should be supplied.
	 * - each observing run should have requested time and minimum time. 
	 * - each observing run should have a telescope and instrument
	 * - 
	 * @see eu.radionet.northstar.business.ProposalValidator#validate(eu.radionet.northstar.data.entities.ObservingRequest)
	 */
	public ProposalError validate(ObservingRequest observingRequest)
	{
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
		List errors = new ArrayList();
		
		ProposalError error = new ProposalError("observingRequest",
                "Observing Request");
		/*
		if (contextConfiguration != null){
			
			FieldsDefinitionType fieldsDefinitionType = 
				(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
	
			if (OptionsDelegate.allowedToDisplay(OpticonConstants.ALLOCATION_JUSTIFICATION, fieldsDefinitionType)
				    && AstronValidator.isBlankOrNull(opticonObservingRequest.getAllocationJustification()))
				{
					errors.add(new ProposalError("allocationJustification",
							"Allocation Justification"));
				}
			
			
			if (OptionsDelegate.isRequired(OpticonConstants.NEW_OBSERVER_EXPERIENCE, fieldsDefinitionType))
			{
				if (AstronValidator.isBlankOrNull(opticonObservingRequest.getNewObserverExperience()))
				{
					errors.add(new ProposalError("newObserverExperience", "New Observer Experience"));
				}
			}
			
		}
		*/
		if (opticonObservingRequest.isLongTermProposal()
				&& AstronValidator.isBlankOrNull(opticonObservingRequest
						.getLongTermProposalSpecifics()))
		{
			errors.add(new ProposalError("longTermProposalSpecifics",
					"Long term proposal specifics"));
		}

		if(observingRequest.getObservations().size() == 0){
			errors.add(new ProposalError("noObservations", "No observations have been created"));
			
		}
		
        for (int i = 0; i < observingRequest.getObservations().size(); i++) {
            Observation observation = (Observation) observingRequest.getObservations().get(i);
            ProposalError errorChild = validate(observation, observingRequest);
            if (!errorChild.isEmpty()) {
                error.getErrors().add(errorChild);
            }
           
        }
		
        //     Check observingrequest Targets
        
        if (opticonObservingRequest.getTargets() != null){
	        Iterator targetIt = opticonObservingRequest.getTargets().iterator();
	        while (targetIt.hasNext())
	        {
	            Target target = (Target) targetIt.next();
	        	ProposalError errorChild = validate((target) );
	            if (!errorChild.isEmpty()) 
	            {
	               error.getErrors().add(errorChild);
	            }
	            Iterator obsit = observingRequest.getObservations().iterator();
	            boolean found = false;
	            while(obsit.hasNext() && found==false){
	            	 OpticonObservation opticonObservation = (OpticonObservation) obsit.next();
	                 if(opticonObservation.getTargets() != null){
	                 	Iterator tarit = opticonObservation.getTargets().iterator();
	                 	while (tarit.hasNext()){
	                 		if( target.equals(tarit.next())){
	                 			found = true;
	                 		}
	                 	}
	                 	
	                 }
	            }
	            if(!found){
	            	 errors.add(new ProposalError("noTargetRun", "Target "+target.getFieldName() +" has no run"));
	            }
	            
	        }
        }
		error.setDescription("Observing Request");
		error.getErrors().addAll(0, errors);
		return error;

	}

	/**
	 * Validates the telescope specific observation
	 * 
	 * @see eu.radionet.northstar.business.ProposalValidator#validate(eu.radionet.northstar.data.entities.Observation)
	 */
	public ProposalError validate(Observation observation,ObservingRequest observingRequest)
	{
		
		
		List errors = new ArrayList();
		OpticonObservation opticonObservation = (OpticonObservation) observation;
		
		/*
		 * Check if there is actually time requested 
		 */
		Set allocations = observingRequest.getAllocations().entrySet();
		Iterator allocIt = allocations.iterator();

		boolean isRequest = false;
		while (allocIt.hasNext() && !isRequest)
		{
			Map.Entry allocation = (Map.Entry) allocIt.next();

			Resource resource = (Resource) allocation.getValue();
			if ((resource != null) && (resource.getValue().doubleValue() > 0))
			{
				isRequest = true;
			}

		}

		if (!isRequest)
		{
			allocations = opticonObservation.getAllocations().entrySet();
			allocIt = allocations.iterator();
			while (allocIt.hasNext() && !isRequest)
			{
				Map.Entry allocation = (Map.Entry) allocIt.next();
	
				Resource resource = (Resource) allocation.getValue();
				if ((resource != null) && (resource.getValue().doubleValue() > 0))
				{
					isRequest = true;
				}

			}
		}
		
		if (!isRequest)
		{
			errors.add(new ProposalError("requestedTime", "Time requested this semester"));
		}
				
		if (AstronValidator.isBlankOrNull(opticonObservation
				.getTelescopeConfiguration()))
		{
			errors.add(new ProposalError("telescopeConfiguration",
					"No telescope specified"));
		}

		//   Check if instrument has been selected.
		
		if (opticonObservation.getInstrument() != null
				&& !AstronValidator.isBlankOrNull(opticonObservation
						.getInstrument().getName()))
		{
			ProposalError errorChild = validate(opticonObservation
					.getInstrument(), opticonObservation.getTelescopeConfiguration());
			if (!errorChild.isEmpty())
			{
				errors.add(errorChild);
			}
		}
		else
		{
			errors.add(new ProposalError("instrumentConfiguration",
					"No instrument specified"));
		}
		//ProposalError error = super.validate(observation,observingRequest);
		ProposalError error = new ProposalError("observation", "Observing run");
		error.setDescription("Observing run ");
		error.getErrors().addAll(0, errors);
		return error;
	}
    
	

	protected boolean isFilterDetailsRequired(OpticonInstrument instrument)
	{
		return false;
		
	}
	
	
	public ProposalError validate(OpticonInstrument instrument, String telescope) 
	{
		HashMap enteredValues = new HashMap();
		enteredValues.put(OpticonConstants.TELESCOPE_CONFIGURATIONS,
				OptionsUtils.getList(telescope));
		ProposalError error = new ProposalError("instrumentConfiguration", instrument
				.getName()
				+ " Instrument");
		List configurations = OptionsDelegate.getOptionsConfigurationTypes(
				OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredValues,
				contextConfiguration);
		ConfigureOptionType configureOptionType = (ConfigureOptionType) OptionsDelegate
				.getOption(instrument.getName(), configurations);

		if (configureOptionType != null
				&& configureOptionType.getSubfields() != null) {
			FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) configureOptionType
					.getSubfields();
			if (OptionsDelegate.isRequired(
					OpticonConstants.INSTRUMENT_FRAME_EXPOSURE_TIME_DETAILS,
					fieldsDefinitionType)) {
				if (instrument.isNonDefaultFrameExposureTime()
						&& instrument.getNonDefaultFrameExposureTimeDetails() == null) {
					error.getErrors().add(
							new ProposalError("nonDefaultFrameExposureTimeDetails",
									"Non Default Frame Exposure Time Details"));
				}
			}
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_FILTER,
					fieldsDefinitionType)) {
				if (instrument.getFilters().isEmpty()) {
					error.getErrors()
							.add(new ProposalError("instrumentFilters", "Filter(s)"));
				}
			}
			if (isFilterDetailsRequired(instrument))
			{
				if (AstronValidator.isBlankOrNull(instrument.getFilterDetails()))
				{
					error.getErrors().add(new ProposalError("instrumentFilterDetails", "Filter details"));
				}
				
			}
			// ToDofilter details
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_STATION, 
					fieldsDefinitionType)) 
			{
				if (AstronValidator.isBlankOrNull(instrument.getStation())) 
				{
					error.getErrors()
						.add(new ProposalError("instrumentStation", "Instrument station"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_ANTENNA, 
					fieldsDefinitionType)) 
			{
				if (AstronValidator.isBlankOrNull(instrument.getAntenna())) 
				{
					error.getErrors()
						.add(new ProposalError("instrumentAntenna", "Instrument antenna"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_CLOCK, 
					fieldsDefinitionType)) 
			{
				if (AstronValidator.isBlankOrNull(instrument.getClock())) 
				{
					error.getErrors()
						.add(new ProposalError("instrumentClock", "Instrument clock"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_MODE, 
					fieldsDefinitionType)) 
			{
				if (AstronValidator.isBlankOrNull(instrument.getMode())) 
				{
					error.getErrors()
						.add(new ProposalError("instrumentMode", "Instrument mode"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_ONE_FILTER, 
					fieldsDefinitionType)) 
			{
				if (AstronValidator.isBlankOrNull(instrument.getOneFilter())) 
				{
					error.getErrors()
						.add(new ProposalError("instrumentFilter", "Instrument filter"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_READOUT, 
					fieldsDefinitionType)) 
			{
				if (AstronValidator.isBlankOrNull(instrument.getReadOut())) 
				{
					error.getErrors()
						.add(new ProposalError("instrumentReadOut", "Read Out"));
				}
			}			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_ORDER_FILTER, 
					fieldsDefinitionType)) 
			{
				if (AstronValidator.isBlankOrNull(instrument.getOrderFilter())) 
				{
					error.getErrors()
						.add(new ProposalError("instrumentOrderFilter", "Order filter"));
				}
			}			
			if (OptionsDelegate.isRequired(
					OpticonConstants.INSTRUMENT_WAVELENGTH, fieldsDefinitionType)) {
				if (AstronValidator.isBlankOrNull(instrument.getWavelength())) {
					error.getErrors().add(
							new ProposalError("wavelength", "Central wavelength"));
				}
			}
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_ORDER,
					fieldsDefinitionType)) {
				if (instrument.getOrder() == null) {
					error.getErrors().add(new ProposalError("order", "Order"));
				}
			}
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_GUIDELINE,
					fieldsDefinitionType)) {
				if (instrument.isGuideline() == false) {
					error.getErrors().add(new ProposalError("guideline", "accept guideline"));
				}
			}
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_SLIT,
					fieldsDefinitionType)) {
				if (AstronValidator.isBlankOrNull(instrument.getSlit())) {
					error.getErrors().add(new ProposalError("slit", "Slit"));
				}
			}
			if (OptionsDelegate.isRequired(
					OpticonConstants.INSTRUMENT_SLIT_POSITION_ANGLE,
					fieldsDefinitionType)) {
				if (instrument.getSlitPositionAngle() == null) {
					error.getErrors().add(
							new ProposalError("slitPositionAngle",
									"Slit Position Angle"));
				}
			}
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_GRATING,
					fieldsDefinitionType)) {
				if (AstronValidator.isBlankOrNull(instrument.getGrating())) {
					error.getErrors().add(
							new ProposalError("grating", "Grating"));
				}
			}
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_GRISM,
					fieldsDefinitionType)) {
				if (AstronValidator.isBlankOrNull(instrument.getGrism())) {
					error.getErrors().add(new ProposalError("grism", "Grism"));
				}
			}
			if (OptionsDelegate.isRequired(
					OpticonConstants.INSTRUMENT_MICROSTEPPING,
					fieldsDefinitionType)) {
				if (AstronValidator
						.isBlankOrNull(instrument.getMicrostepping())) {
					error.getErrors()
							.add(
									new ProposalError("microstepping",
											"Microstepping"));
				}
			}

			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_COMMENTS,
					fieldsDefinitionType)) {
				if (AstronValidator.isBlankOrNull(instrument.getComments())) {
					error.getErrors().add(
							new ProposalError("comments",
									"Configuration comments"));
				}
			}
		}
		return error;
	}	

		
    public ProposalError validate(Target target)
    {
    	List errors = new ArrayList();
    	OpticonTarget opticonTarget = (OpticonTarget) target;
        FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
    	
		Double totalDuration = NorthStarDelegate.getResourceValue(
				opticonTarget.getAllocations(),
				OpticonConstants.TOTAL_DURATION);
		// if exposure time is not visible, then it cannot be valildated.
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_EXPOSURE_TIME,
				fieldsDefinitionType)){
  		if (totalDuration == null || AstronValidator.isBlankOrNull(String.valueOf(totalDuration))); 
	        {
	            errors.add(new ProposalError("totalDuration", "Exposure time"));
	        }
		}
        
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SN, fieldsDefinitionType))
		{
			if (opticonTarget.getSn()== null)
			{
	            errors.add(new ProposalError("selectedTargetSn", "S/N"));
				
			}
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_MAGNITUDE,
				fieldsDefinitionType))
		{
	        if (opticonTarget.getMagnitude()== null) 
	        {
	            errors.add(new ProposalError("selectedTargetMagnitude", "Magnitude"));
	        }
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SEEING_RANGE,
						fieldsDefinitionType))
		{
			if (opticonTarget.getSeeingLower()== null || opticonTarget.getSeeingLower() == null)
			{
	            errors.add(new ProposalError("targetSeeingUpper", "Seeing"));
				
			}
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_FLUX, fieldsDefinitionType))
		{
			/*/																				//
			//																				//
			// 			opticon spec, flux is only required for infrared instruments.		//
			// 																				//
			//																				/*/
			OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX,
					fieldsDefinitionType);
			
			if (opticonTarget.getFlux() == null)
			{
				errors.add(new ProposalError("targetFlux", "Infrared magnitude or Flux"));

			}
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_FLUX_FREQUENCY, fieldsDefinitionType))
		{
			OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX_FREQUENCY,
					fieldsDefinitionType);
			
			if (opticonTarget.getFluxFrequency() == null)
			{
				errors.add(new ProposalError("targetFluxFrequency", "Flux frequency"));

			}
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SPECTRAL_INDEX, fieldsDefinitionType))
		{
			OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECTRAL_INDEX,
					fieldsDefinitionType);
			
			if (opticonTarget.getSpectralIndex() == null)
			{
				errors.add(new ProposalError("targetSpectralIndex", "Spectral index"));

			}
		}
		
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SKY_QUALITIES, fieldsDefinitionType))
		{
			if (AstronValidator.isBlankOrNull(opticonTarget.getSkyQuality()))
			{
				errors.add(new ProposalError("selectedTargetSkyQuality", "Sky quality"));

			}
		}
		ProposalError error = new ProposalError("Target", "Target ("
                + target.getFieldName() + ")");
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_RA, fieldsDefinitionType))
		{
			error = super.validate(target);
		}
		 if (AstronValidator.isBlankOrNull(target.getFieldName())) {
	            error.setDescription("Target ()");
	        } else {
	            error.setDescription(" Target (" + target.getFieldName() + ")");
	        }
        error.getErrors().addAll(0, errors);        
        return error;       
  
    }
    
    
    
}
