// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProposalBean.java 
 *
 * Created on Feb 3, 2005
 *
 * Version $Id: ProposalBean.java,v 1.2 2006-06-30 10:01:00 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.list;

import java.io.Serializable;

/**
 * The ProposalBean provides
 * 
 * @author Bastiaan Verhoef
 * 
 */
public class ProposalBean implements Serializable {
	protected String id = null;

	protected String piName = null;

	protected String code = null;

	protected String title = null;

	protected String status = null;

	protected String statusTime = null;

	protected String community = null;

	protected String category = null;

	protected String canSubmitOrRetract = null;

	protected String canDelete = null;

	protected boolean contactAuthor = false;
	
	protected boolean available =false;

	protected boolean locked = false;

	/**
	 * @return Returns the code.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            The code to set.
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return Returns the id.
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(String id) {
		this.id = id;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(String community) {
		this.community = community;
	}

	/**
	 * @return Returns the status.
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status
	 *            The status to set.
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return Returns the title.
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title
	 *            The title to set.
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return Returns the statusTime.
	 */
	public String getStatusTime() {
		return statusTime;
	}

	/**
	 * @param statusTime
	 *            The statusTime to set.
	 */
	public void setStatusTime(String statusTime) {
		this.statusTime = statusTime;
	}

	public String getStatusClass() {
		if (status != null) {
			return this.status.replaceAll(" ", "_");
		} else {
			return "";
		}
	}

	/**
	 * @return Returns the canSubmitOrRetract.
	 */
	public String getCanSubmitOrRetract() {
		return canSubmitOrRetract;
	}

	/**
	 * @param canSubmitOrRetract
	 *            The canSubmitOrRetract to set.
	 */
	public void setCanSubmitOrRetract(String canSubmitOrRetract) {
		this.canSubmitOrRetract = canSubmitOrRetract;
	}

	/**
	 * @return Returns the canDelete.
	 */
	public String getCanDelete() {
		return canDelete;
	}

	/**
	 * @param canDelete
	 *            The canDelete to set.
	 */
	public void setCanDelete(String canDelete) {
		this.canDelete = canDelete;
	}

	public boolean isLocked() {
		return locked;
	}

	public void setLocked(boolean locked) {
		this.locked = locked;
	}

	public boolean isContactAuthor() {
		return contactAuthor;
	}

	public void setContactAuthor(boolean contactAuthor) {
		this.contactAuthor = contactAuthor;
	}

	public String getPiName() {
		return piName;
	}

	public void setPiName(String piName) {
		this.piName = piName;
	}

	public boolean isAvailable() {
		return available;
	}

	public void setAvailable(boolean available) {
		this.available = available;
	}

}
