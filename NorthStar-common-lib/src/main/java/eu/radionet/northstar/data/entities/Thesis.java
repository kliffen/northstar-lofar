// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>


package eu.radionet.northstar.data.entities;

import java.io.Serializable;
import java.util.Date;

//import java.util.*;

/**
 * The AdditionalIssues provides
 *
 * @author Bastiaan Verhoef
 *
 */
public class Thesis implements Serializable
{
    protected Integer id = null;
    protected Integer fakeId = null;
    protected Member student = null;
    protected String studentName = null;
    protected String studentLevel = null;
    protected Member supervisor = null;
    protected String supervisorName = null;
    protected Date expectedCompletionDate = null;
    protected boolean dataRequired = false;
    /**
     * @return Returns the fakeId.
     */
    public Integer getFakeId() {
        return fakeId;
    }
    /**
     * @param fakeId The fakeId to set.
     */
    public void setFakeId(Integer fakeId) {
        this.fakeId = fakeId;
    }
    /**
     * @return Returns the dataRequired.
     */
    public boolean isDataRequired() {
        return dataRequired;
    }
    /**
     * @param dataRequired The dataRequired to set.
     */
    public void setDataRequired(boolean dataRequired) {
        this.dataRequired = dataRequired;
    }
    /**
     * @return Returns the expectedCompletionDate.
     */
    public Date getExpectedCompletionDate() {
        return expectedCompletionDate;
    }
    /**
     * @param expectedCompletionDate The expectedCompletionDate to set.
     */
    public void setExpectedCompletionDate(Date expectedCompletionDate) {
        this.expectedCompletionDate = expectedCompletionDate;
    }
    /**
     * @return Returns the id.
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id The id to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return Returns the student.
     */
    public Member getStudent() {
        return student;
    }
    /**
     * @param student The student to set.
     */
    public void setStudent(Member student) {
        this.student = student;
    }
    /**
     * @return Returns the studentName.
     */
    public String getStudentName() {
        if (student!=null) {
            return student.getName();
        } else {
            return studentName;
        }
    }
    /**
     * @param studentName The studentName to set.
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
    
    public String getStudentLevel() {
		return studentLevel;
	}
	public void setStudentLevel(String studentLevel) {
		this.studentLevel = studentLevel;
	}
	/**
     * @return Returns the supervisor.
     */
    public Member getSupervisor() {
        return supervisor;
    }
    /**
     * @param supervisor The supervisor to set.
     */
    public void setSupervisor(Member supervisor) {
        this.supervisor = supervisor;
    }
    /**
     * @return Returns the supervisorName.
     */
    public String getSupervisorName() {
        if (supervisor!=null) {
            return supervisor.getName();
        } else {
            return supervisorName;
        }
    }
    /**
     * @param supervisorName The supervisorName to set.
     */
    public void setSupervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
    }

}