// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts.util.LabelValueBean;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.ProposalValidator;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.ProposalError;
import eu.radionet.northstar.data.entities.Resource;
import eu.radionet.northstar.data.entities.Target;

/*
 * lofar validations:
 * 
 * 
 * //nv 6sep2010: VL1,VL3,VL4 disabled
 * 
 * VL1  there should be at least 1 observation specified
 * VL2  if it is a long term proposal, then the long term details should be given
 * VL3  total amount of hours should be specified 
 * VL4  total amount of storage should be specified
 * VL5  each observation should have an instrument specified 
 * VL6  each observation should have a station, clock, filter and antenna selected
 * VL7  each observation should have central freq and bandwidth specified
 * VL8  if it is a non-contiguous filter, filter details should be given.
 * VL9  interferometer and direct storage observations should have the integration time specified
 * VL10 tied array observations should have the amount of beams and sample rate defined
 * VL11 if imaging is used, field size, pixel size, amount of frequency channels should be specified
 * VL12 if averaging is used, averaging time and averaging frequency should be specified.
 * VL13 if TBB instrument is selected, the total nr. of events should be specified.
 * VL14 if piggy-back mode is enabled, then the details should be given
 * VL15 a non-standard instrument should be specified.
 * VL16 targets should have ra,dec and time specified.
 * VL17 each observation should have one target...
 * VL18
 * 
 */

public class ApertifProposalValidator extends ProposalValidator{
	public ProposalError validate(ObservingRequest observingRequest)
	{
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
		List errors = new ArrayList();
		
		ProposalError error = new ProposalError("observingRequest",
                "Observing Request");
		//VL1
		//nv 6sep2010, this version allows 0 obesrvations
/*		
		if(observingRequest.getObservations().size() == 0){
			errors.add(new ProposalError("noObservations", "No observations have been created"));
		}
*/
		//VL2
		if (opticonObservingRequest.isLongTermProposal()
				&& AstronValidator.isBlankOrNull(opticonObservingRequest
						.getLongTermProposalSpecifics())){
			errors.add(new ProposalError("longTermProposalSpecifics",
					"Long term proposal specifics"));
		}
		
		//VL3, VL4
		Set allocations = observingRequest.getAllocations().entrySet();
		Iterator allocIt = allocations.iterator();
		boolean storage = false;
		boolean timerequest = false;
		while (allocIt.hasNext()){
			Map.Entry allocation = (Map.Entry) allocIt.next();
			Resource resource = (Resource) allocation.getValue();
			if ((resource != null) && (resource.getValue().doubleValue() > 0)){
				if(allocation.getKey().toString().equalsIgnoreCase("LOFAR_Global")){
					timerequest=true;
				}
				if(allocation.getKey().toString().equalsIgnoreCase("LOFAR_longTermStorage")){
					storage=true;
				}
				
			}
		}

		//nv 6sep2010, this version allows 0 obesrvations
/*			
		if (!storage)	{
			errors.add(new ProposalError("requestedStorage", "Long term storage requested this semester"));
		}
		if (!timerequest)	{
			errors.add(new ProposalError("requestedTime", "Time requested this semester"));
		}
*/
		
		// check observations
        for (int i = 0; i < observingRequest.getObservations().size(); i++) {
            Observation observation = (Observation) observingRequest.getObservations().get(i);
            ProposalError errorChild = validate(observation, observingRequest);
            if (!errorChild.isEmpty()) {
                error.getErrors().add(errorChild);
            }
            
            //VL17 
            List urlList = new ArrayList();
    		String Url = null;
    		int maxTargets =1;
    		
    		urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.MAX_TARGETS ,
    					new HashMap(), contextConfiguration);
    		if (urlList != null && urlList.size() > 1){
    			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
    			Url =  urlValue.getValue();
    			if(AstronValidator.isPositiveInt(Url)){
    				maxTargets = new Integer(Url).intValue();
    			}
    		}
    		
            
            if(observation.getTargets() == null || observation.getTargets().size() ==0){
            	errors.add(new ProposalError("noTargets", "Observation has no targets"));
            }
            
            if(observation.getTargets() != null && observation.getTargets().size() > maxTargets  ){
            	errors.add(new ProposalError("maxTargets", "max amount of targets has been exceeded"));
            }
           
        }
		
		//  Check observingrequest Targets
        if (opticonObservingRequest.getTargets() != null){
	        Iterator targetIt = opticonObservingRequest.getTargets().iterator();
	        while (targetIt.hasNext()){
	            Target target = (Target) targetIt.next();
	        	ProposalError errorChild = validate((target) );
	            if (!errorChild.isEmpty()){
	               error.getErrors().add(errorChild);
	            }
	            Iterator obsit = observingRequest.getObservations().iterator();
	            boolean found = false;
	            while(obsit.hasNext() && found==false){
	            	 OpticonObservation opticonObservation = (OpticonObservation) obsit.next();
	                 if(opticonObservation.getTargets() != null){
	                 	Iterator tarit = opticonObservation.getTargets().iterator();
	                 	while (tarit.hasNext()){
	                 		if( target.equals(tarit.next())){
	                 			found = true;
	                 		}
	                 	}
	                 	
	                 }
	            }
	            if(!found){
	            	 errors.add(new ProposalError("noTargetRun", "Target "+target.getFieldName() +" has no run"));
	            }
	            
	        }
        }
        
        
		error.setDescription("Observing Request");
		error.getErrors().addAll(0, errors);
		return error;

	}

	/**
	 * Validates the telescope specific observation
	 * 
	 * @see eu.radionet.northstar.business.ProposalValidator#validate(eu.radionet.northstar.data.entities.Observation)
	 */
	public ProposalError validate(Observation observation,ObservingRequest observingRequest)
	{
		List errors = new ArrayList();
		OpticonObservation opticonObservation = (OpticonObservation) observation;
		
			
		// this should not be possible as the lofar telescope is automatically selected, but check anyway.		
		if (AstronValidator.isBlankOrNull(opticonObservation
				.getTelescopeConfiguration())){
			errors.add(new ProposalError("telescopeConfiguration",
					"No telescope specified"));
		}

		//VL5 
		if (opticonObservation.getInstrument() != null
				&& !AstronValidator.isBlankOrNull(opticonObservation
						.getInstrument().getName())){
			// check instrument
			ProposalError errorChild = validate(opticonObservation
					.getInstrument(), opticonObservation.getTelescopeConfiguration());
			if (!errorChild.isEmpty()){
				errors.add(errorChild);
			}
		}
		else{
			errors.add(new ProposalError("instrumentConfiguration",
					"No instrument specified"));
		}
		
		//ProposalError error = super.validate(observation,observingRequest);
		ProposalError error = new ProposalError("observation", "Observing run");
		error.setDescription("Observing run ");
		error.getErrors().addAll(0, errors);
		return error;
	}
    
	

	protected boolean isFilterDetailsRequired(OpticonInstrument instrument)
	{
		return false;
		
	}
	
	
	public ProposalError validate(OpticonInstrument instrument, String telescope) 
	{
		HashMap enteredValues = new HashMap();
		enteredValues.put(OpticonConstants.TELESCOPE_CONFIGURATIONS,
				OptionsUtils.getList(telescope));
		ProposalError error = new ProposalError("instrumentConfiguration", instrument
				.getName() + " Instrument");
		List configurations = OptionsDelegate.getOptionsConfigurationTypes(
				OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredValues,
				contextConfiguration);
		ConfigureOptionType configureOptionType = (ConfigureOptionType) OptionsDelegate
				.getOption(instrument.getName(), configurations);

		if (configureOptionType != null
				&& configureOptionType.getSubfields() != null) {
			FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) configureOptionType
					.getSubfields();
			
			//VL6
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_STATION, 
					fieldsDefinitionType)){
				if (AstronValidator.isBlankOrNull(instrument.getStation())){
					error.getErrors()
						.add(new ProposalError("instrumentStation", "Instrument station"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_ANTENNA, 
					fieldsDefinitionType)){
				if (AstronValidator.isBlankOrNull(instrument.getAntenna())){
					error.getErrors()
						.add(new ProposalError("instrumentAntenna", "Instrument antenna"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_CLOCK, 
					fieldsDefinitionType)){
				if (AstronValidator.isBlankOrNull(instrument.getClock())){
					error.getErrors()
						.add(new ProposalError("instrumentClock", "Instrument clock"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_FILTER,
					fieldsDefinitionType)) {
				if (instrument.getFilters().isEmpty()) {
					error.getErrors()
							.add(new ProposalError("instrumentFilters", "Filter(s)"));
				}
			}

			//VL7
			if (instrument.getFilterBandwidth() == null || instrument.getFilterBandwidth().doubleValue() < 0 ){
				error.getErrors()
					.add(new ProposalError("filterBandwidth", "Instrument bandwidth"));
			}
			
			if (instrument.getFilterFrequency() == null || instrument.getFilterFrequency().doubleValue() < 0 ) {
				error.getErrors()
					.add(new ProposalError("filterFrequency", "Instrument frequency"));
			}
			
			//VL8
			if (instrument.isFilterContiguousCoverage() && instrument.getFilterDetails() == null ){
				error.getErrors()
					.add(new ProposalError("filterContiguousCoverage", "Instrument filter coverage details"));
			}
			
			//VL9
			if(instrument.getName().startsWith("Interfero") || instrument.getName().startsWith("Direct") ){
				if(instrument.getIntegrationTime()==null){
					error.getErrors()
						.add(new ProposalError("integrationTime", "Integration time"));
				}
			}
			
			//VL10
			if(instrument.getName().startsWith("Tied")){
				if(instrument.getBeams() == null){
					error.getErrors()
						.add(new ProposalError("integrationTime", "Specified beams"));
				}
				if(instrument.getSamplerate() == null){
					error.getErrors()
						.add(new ProposalError("integrationTime", "Sample rate"));
				}
			}
			
			
			//VL11
			if(instrument.isImaging() ){
				if(instrument.getPixelSizeX() ==null || instrument.getPixelSizeY() == null){ 
					error.getErrors().add(new ProposalError("instrumentImaging", "Pixel size"));
				}
				if(instrument.getFieldSizeX() ==null || instrument.getFieldSizeY()==null){
					error.getErrors().add(new ProposalError("instrumentImaging", "Field size"));
				}
				if(instrument.getFrequencyChannels() == null){
					error.getErrors().add(new ProposalError("instrumentImaging", "Frequency channels"));
				}
			}
			
			
			//VL12
			if(instrument.isAveraging()){
				if(instrument.getAveragingTime() == null || instrument.getAveragingTime() <0){
					error.getErrors().add(new ProposalError("instrumentAveraging","Averaging time"));
				}
				if(instrument.getAveragingFrequency() == null || instrument.getAveragingFrequency() <0){
					error.getErrors().add(new ProposalError("instrumentAveraging","Averaging frequency"));
				}
			}
			
			//VL14
			if(instrument.isPiggyBack()){
				if(AstronValidator.isBlankOrNull(instrument.getPiggyBackSpecifics() ) ){
					error.getErrors().add(new ProposalError("instrumentPiggyBack","Piggyback specifics"));
				}
			}
			
			//VL15
			if(instrument.getName() != null && instrument.getName().startsWith("Non")){
				if(AstronValidator.isBlankOrNull(instrument.getComments() )){
					error.getErrors().add(new ProposalError("instrumentNonStandard","Non standard specifics"));
				}
			}
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_FRAME_EXPOSURE_TIME_DETAILS,
					fieldsDefinitionType)) {
				if (instrument.isNonDefaultFrameExposureTime()
						&& instrument.getNonDefaultFrameExposureTimeDetails() == null) {
					error.getErrors().add(
							new ProposalError("nonDefaultFrameExposureTimeDetails",
									"Non Default Frame Exposure Time Details"));
				}
			}
			
			//VL16
			
			
		}
		return error;
	}	

		
    public ProposalError validate(Target target)
    {
    	List errors = new ArrayList();
    	OpticonTarget opticonTarget = (OpticonTarget) target;
        FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
    	
		Double totalDuration = NorthStarDelegate.getResourceValue(
				opticonTarget.getAllocations(),
				OpticonConstants.TOTAL_DURATION);
		// if exposure time is not visible, then it cannot be validated.
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_EXPOSURE_TIME,
				fieldsDefinitionType)){
	        if (totalDuration == null){
	            errors.add(new ProposalError("totalDuration", "Exposure time"));
	        }
		}
        
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SN, fieldsDefinitionType)){
			if (opticonTarget.getSn()== null){
	            errors.add(new ProposalError("selectedTargetSn", "S/N"));
				
			}
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_MAGNITUDE,
				fieldsDefinitionType)){
	        if (opticonTarget.getMagnitude()== null){
	            errors.add(new ProposalError("selectedTargetMagnitude", "Magnitude"));
	        }
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SEEING_RANGE,
						fieldsDefinitionType)){
			if (opticonTarget.getSeeingLower()== null || opticonTarget.getSeeingLower() == null){
	            errors.add(new ProposalError("targetSeeingUpper", "Seeing"));
				
			}
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_FLUX, fieldsDefinitionType)){
			OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX,fieldsDefinitionType);
			
			if (opticonTarget.getFlux() == null){
				errors.add(new ProposalError("targetFlux", "Infrared magnitude or Flux"));

			}
		}
		
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SKY_QUALITIES, fieldsDefinitionType)){
			if (AstronValidator.isBlankOrNull(opticonTarget.getSkyQuality())){
				errors.add(new ProposalError("selectedTargetSkyQuality", "Sky quality"));

			}
		}
		ProposalError error = new ProposalError("Target", "Target ("
                + target.getFieldName() + ")");
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_RA, fieldsDefinitionType)){
			error = super.validate(target);
		}
		 if (AstronValidator.isBlankOrNull(target.getFieldName())) {
	            error.setDescription("Target ()");
	        } else {
	            error.setDescription(" Target (" + target.getFieldName() + ")");
	        }
        error.getErrors().addAll(0, errors);        
        return error;       
  
    }
    

}
