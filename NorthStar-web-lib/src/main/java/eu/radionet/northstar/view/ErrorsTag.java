// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ErrorsTag.java 
 *
 * Created on Dec 8, 2004
 *
 * Version $id$
 *
 */
package eu.radionet.northstar.view;

import java.io.IOException;
import java.util.Iterator;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;

import org.apache.struts.action.ActionMessages;
import org.apache.struts.taglib.TagUtils;

/**
 * The ErrorsTag provides
 * 
 * @author Bastiaan Verhoef
 *  
 */
public class ErrorsTag extends org.apache.struts.taglib.html.ErrorsTag {


    private String ifnotexist = null;



    public int doStartTag() throws JspException {
        ActionMessages errors = TagUtils.getInstance().getActionMessages(
                pageContext, name);
        Iterator errorsIterator;
        if (property == null) {
            errorsIterator = errors.get();
        } else {
            errorsIterator = errors.get(property);
        }
        if (errorsIterator.hasNext()) {
            return super.doStartTag();
        }

        if (ifnotexist != null) {
            // Print this element to our output writer
            JspWriter writer = pageContext.getOut();
            try {
                writer.print(ifnotexist);
            } catch (IOException e) {
                throw new JspException("Cannot print");
            }
        }
        // Evaluate the body of this tag
        return (EVAL_BODY_INCLUDE);
    }
    /**
     * @return Returns the ifnotexist.
     */
    public String getIfnotexist() {
        return ifnotexist;
    }
    /**
     * @param ifnotexist The ifnotexist to set.
     */
    public void setIfnotexist(String ifnotexist) {
        this.ifnotexist = ifnotexist;
    }
}