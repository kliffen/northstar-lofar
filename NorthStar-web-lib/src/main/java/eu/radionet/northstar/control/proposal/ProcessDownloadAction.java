// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessDownloadAction.java 
 *
 * Created on Feb 9, 2005
 *
 * Version $Id: ProcessDownloadAction.java,v 1.5 2008-07-18 13:59:28 boelen Exp $
 *
 */
package eu.radionet.northstar.control.proposal;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.util.PdfUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.FigureFile;
import eu.radionet.northstar.data.entities.JustificationFile;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ScientificFile;
import eu.radionet.northstar.data.entities.TechnicalDetailsFile;

/**
 * The ProcessDownloadAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessDownloadAction extends LockedAction {
	private Log log = LogFactory.getLog(this.getClass());

	static private final int MAX_AGE_IN_SECONDS = 600;

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionMessages errors = new ActionMessages();
		HttpSession session = request.getSession();
		/*
		 * retrieve proposal data from session with information
		 */
		ProposalData proposalData = (ProposalData) session
				.getAttribute(Constants.PROPOSAL_DATA);
		String justificationFileParam = request
				.getParameter("justificationFileType");
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		// proposalForm.get

		if (!AstronValidator.isBlankOrNull(justificationFileParam)) {
			if (justificationFileParam.equals("scientific")) {
				/*
				 * retrieve justificationFile
				 */
				ScientificFile justificationFile = proposalData
						.getScientificFile();
				if (justificationFile == null) {
					try {
						justificationFile = proposalDelegate
								.getScientificFile(proposalData.getProposal()
										.getJustification());
					} catch (DatabaseException de) {
						log.error("Cannot retrieve own proposals: "
								+ de.getMessage());
						errors.add(Constants.ERROR, new ActionMessage(
								"error.database.retrieve.proposal"));
						saveErrors(request, errors);
						return mapping.findForward("refresh");
					}

				}

				try {
					return downloadScientificFile(proposalData.getProposal(),
							justificationFile, response);
				} catch (IOException io) {
					log.warn(LogMessage.getMessage(ownUserAccount, "Exception while downloading scientific file " + io
							.getClass().getName()
							+ " " + io.getMessage()));
				}
			} else if (justificationFileParam.equals("figure")) {
				/*
				 * retrieve justificationFile
				 */
				FigureFile justificationFile = proposalData.getFigureFile();
				if (justificationFile == null) {
					try {
						justificationFile = proposalDelegate
								.getFigureFile(proposalData.getProposal()
										.getJustification());
					} catch (DatabaseException de) {
						log.error("Cannot retrieve own proposals: "
								+ de.getMessage());
						errors.add(Constants.ERROR, new ActionMessage(
								"error.database.retrieve.proposal"));
						saveErrors(request, errors);
						return mapping.findForward("refresh");
					}
				}

				try {
					return downloadFigureFile(proposalData.getProposal(),
							justificationFile, response);
				} catch (IOException io) {
					log.warn(LogMessage.getMessage(ownUserAccount, "Exception while downloading figure file " + io
							.getClass().getName()
							+ " " + io.getMessage()));
				}
			} else if (justificationFileParam.equals("technicalDetails")) {
				/*
				 * retrieve justificationFile
				 */
				TechnicalDetailsFile justificationFile = proposalData
						.getTechnicalDetailsFile();
				if (justificationFile == null) {
					try {
						justificationFile = proposalDelegate
								.getTechnicalDetailsFile(proposalData
										.getProposal().getJustification());
					} catch (DatabaseException de) {
						log.error("Cannot retrieve own proposals: "
								+ de.getMessage());
						errors.add(Constants.ERROR, new ActionMessage(
								"error.database.retrieve.proposal"));
						saveErrors(request, errors);
						return mapping.findForward("refresh");
					}
				}
				try {
					return downloadTechnicalDetailsFile(proposalData
							.getProposal(), justificationFile, response);
				} catch (IOException io) {
					log.warn(LogMessage.getMessage(ownUserAccount,"Exception while downloading technical details file " +  io
							.getClass().getName()
							+ " " + io.getMessage()));
				}
			}
			else if (justificationFileParam.equals("latexClass")) {
				String Url = getLatexFileUrl("class",proposalData.getProposal());
				JustificationFile justificationFile = new JustificationFile();
				// get real path
				Url = session.getServletContext().getRealPath(Url);
				// open file
				File latexFile = new File(Url);
				String fileName = latexFile.getName();
				int fileLength = (int) latexFile.length();

				if (fileLength > 0){
					byte[] uploadFile = new byte[fileLength];
					try{
						uploadFile = readFile(latexFile,fileLength);
					} catch (IOException io) {
						log.warn(LogMessage.getMessage(ownUserAccount,"Exception while downloading latex file " +  io
								.getClass().getName()
								+ " " + io.getMessage()));
					}
					justificationFile.setUploadFile(uploadFile);
				}
				else{
					log.warn(LogMessage.getMessage(ownUserAccount,"File not found at: "+Url) );
					errors.add(Constants.ERROR, new ActionMessage(
					"error.class.file.unavailable"));
					saveErrors(request, errors);
					return mapping.findForward("refresh");
				}
				return downloadFile(justificationFile, fileName, fileLength , response);
			}
			
			else if (justificationFileParam.equals("latexTemplate")) {
				String Url = getLatexFileUrl("template",proposalData.getProposal());
				JustificationFile justificationFile = new JustificationFile();
				// get real path
				Url = session.getServletContext().getRealPath(Url);
				// open file
				File latexFile = new File(Url);
				String fileName = latexFile.getName();
				int fileLength = (int) latexFile.length();

				if (fileLength > 0){
					byte[] uploadFile = new byte[fileLength];
					try{
						uploadFile = readFile(latexFile,fileLength);
					} catch (IOException io) {
						log.warn(LogMessage.getMessage(ownUserAccount,"Exception while downloading latex file " +  io
								.getClass().getName()
								+ " " + io.getMessage()));
					}
					justificationFile.setUploadFile(uploadFile);
				}
				else{
					log.warn(LogMessage.getMessage(ownUserAccount,"File not found at: "+Url) );
					errors.add(Constants.ERROR, new ActionMessage(
					"error.template.file.unavailable"));
					saveErrors(request, errors);
					return mapping.findForward("refresh");
				}
				return downloadFile(justificationFile, fileName, fileLength , response);
			}
				
			
		}
		return null;

	}
	
	private byte[] readFile(File file, int length) throws IOException {
		InputStream inStream = new FileInputStream(file);
        byte[] uploadFile = new byte[length];
        
        // Read in the bytes
        int offset = 0;
        int numRead = 0;
        while (offset < uploadFile.length
               && (numRead=inStream.read(uploadFile, offset, uploadFile.length-offset)) >= 0) {
            offset += numRead;
        }

        if (offset < uploadFile.length) {
            throw new IOException("Could not completely read file " + file.getName());
        }
		return uploadFile;
	}

	public String getLatexFileUrl(String fileType, Proposal proposal)
	{
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		
		List urlList = new ArrayList();
		String Url = new String();
		if (fileType.equalsIgnoreCase("class") )
		{
			//Url = telescopeConfiguration.getLatexClassFile();
			urlList = OptionsUtils.getLabelValueBeans(Constants.LATEX_CLASS_FILE,
					new HashMap(), contextConfiguration);
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			 Url =  urlValue.getValue(); 
			} 
		}
		if (fileType.equalsIgnoreCase("template"))
		{
			urlList = OptionsUtils.getLabelValueBeans(Constants.LATEX_TEMPLATE_FILE,
					new HashMap(), contextConfiguration);
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			 Url =  urlValue.getValue(); 
			} 	
		}
		return Url; 
	}

	public ActionForward downloadFile(JustificationFile justificationFile,
			String fileName, int filesize, HttpServletResponse response)
			throws SQLException, IOException {
		response.setContentLength(filesize);
		//response.setContentType("application/pdf");

		response.setHeader("Content-disposition", "attachment; filename="
				+ fileName);
		response.setHeader("Cache-Control", "max-age=" + MAX_AGE_IN_SECONDS);

		ServletOutputStream outStream = response.getOutputStream();
		outStream.write(justificationFile.getUploadFile());
		outStream.flush();

		return null;
	}

	public ActionForward downloadScientificFile(Proposal proposal,
			ScientificFile scientificFile, HttpServletResponse response)
			throws SQLException, IOException {
		return downloadFile(scientificFile, proposal.getJustification()
				.getScientificFileName(), proposal.getJustification()
				.getScientificFileSize().intValue(), response);
	}

	public ActionForward downloadTechnicalDetailsFile(Proposal proposal,
			TechnicalDetailsFile technicalDetailsFile,
			HttpServletResponse response) throws SQLException, IOException {
		return downloadFile(technicalDetailsFile, proposal.getJustification()
				.getTechnicalDetailsFileName(), proposal.getJustification()
				.getTechnicalDetailsFileSize().intValue(), response);
	}

	public ActionForward downloadFigureFile(Proposal proposal,
			FigureFile figureFile, HttpServletResponse response)
			throws SQLException, IOException {
		return downloadFile(figureFile, proposal.getJustification()
				.getFigureFileName(), proposal.getJustification()
				.getFigureFileSize().intValue(), response);
	}
}