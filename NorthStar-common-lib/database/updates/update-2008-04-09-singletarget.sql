DROP TABLE IF EXISTS `target`;
CREATE TABLE `target` (
  `id` int(11) NOT NULL auto_increment,
  `observationid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `fieldname` varchar(255) default NULL,
  `ra` double default NULL,
  `decl` double default NULL,
  `epoch` varchar(255) default NULL,
  `targettype` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observation_target_IND` (`observationid`),
  KEY `observation_index_target_IND` (`indexid`),
  CONSTRAINT `observation_target_FK` FOREIGN KEY (`observationid`) REFERENCES `observation` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `targetallocation`;
CREATE TABLE `targetallocation` (
  `id` int(11) NOT NULL auto_increment,
  `resourceid` int(11) default NULL,
  `targetid` int(11) default NULL,
  `allocationkey` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `target_targetallocation_IND` (`targetid`),
  KEY `resource_targetallocation_IND` (`resourceid`),
  KEY `target_key_targetallocation_IND` (`allocationkey`),
  CONSTRAINT `target_targetallocation_FK` FOREIGN KEY (`targetid`) REFERENCES `target` (`id`),
  CONSTRAINT `resource_targetallocation_FK` FOREIGN KEY (`resourceid`) REFERENCES `resource` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- fill the table

INSERT target (`observationid`, `indexid`, `fieldname`, `ra`, `decl`, `epoch`, `targettype`)
  SELECT o.id, 0, o.fieldname, o.ra, o.decl, o.epoch, "SUPER"
  FROM observation o;

-- remove the old obsolete columns
ALTER TABLE observation DROP COLUMN `fieldname`;
ALTER TABLE observation DROP COLUMN `ra`;
ALTER TABLE observation DROP COLUMN `decl`;
ALTER TABLE observation DROP COLUMN `epoch`;