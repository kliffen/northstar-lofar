// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ActionServlet.java 
 *
 * Created on Nov 22, 2004
 *
 * Version $id$
 *
 */
package eu.radionet.northstar.control;

import java.io.IOException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronValidator;

import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.CollaborationConfiguration;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.deadline.TaskDaemon;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.data.Repository;
import eu.radionet.northstar.data.entities.Semester;

/**
 * The ActionServlet provides an extension of the Struts ActionServlet. It
 * provides authentication and authorization
 * 
 * @author Bastiaan Verhoef
 */
public class ActionServlet extends org.apache.struts.action.ActionServlet {
	public static TaskDaemon taskDaemon = null;

	private Log log = LogFactory.getLog(this.getClass());



	public void destroy() {
		if (taskDaemon != null){
			taskDaemon.destroy();
		}
		super.destroy();
	}

	protected void process(HttpServletRequest request,
			HttpServletResponse response) throws ServletException,
			java.io.IOException {
		String servletPath = request.getServletPath();
		HttpSession session = request.getSession();
		if (servletPath.equals(Constants.INTERNAL_ERROR_ACTION)) {
			super.process(request, response);
		}
		if (request.getUserPrincipal() != null) {
			UserAccount ownUserAccount = (UserAccount) session
					.getAttribute(Constants.OWN_USERACCOUNT);
			if (ownUserAccount == null) {
				try {

					NorthStarDelegate northStarDelegate = NorthStarDelegate
							.getInstance();

					ownUserAccount = northStarDelegate
							.getUserAccountByName(request.getUserPrincipal()
									.getName());
					CollaborationConfiguration
							.setAcceptInvitation(NorthStarUtils.getLinkPath(request));
					session.setAttribute(Constants.OWN_USERACCOUNT,
							ownUserAccount);
					log.info(LogMessage.getMessage(ownUserAccount,
							getPath(request) +" session : "
									+ session.getId()));
					String browser = request
							.getParameter(Constants.BROWSER_PARAM);
					if (browser == null) {
						browser = Constants.GECKO;
					}
					session.setAttribute(Constants.BROWSER, browser);
					superProcess(request, response, ownUserAccount);
				} catch (DatabaseException de) {
					log.fatal(LogMessage.getMessage(ownUserAccount,
							getPath(request) +"\n" + "DatabaseException: " + de.getMessage(), de));
					loginRedirect(request, response, request.getContextPath()
							+ Constants.INTERNAL_ERROR_ACTION);

				}

			} else if (servletPath.equals(Constants.FORWARD_ACTION)) {
				loginRedirect(request, response, request
						.getParameter(Constants.FORWARD));
			} else {
				if (AstronValidator.isBlankOrNull(CollaborationConfiguration
						.getAcceptInvitation())) {
					CollaborationConfiguration
							.setAcceptInvitation(NorthStarUtils.getLinkPath(request));
				}
				superProcess(request, response, ownUserAccount);
			}
		} else {
			superProcess(request, response, null);
		}

	}

	protected String getPath(HttpServletRequest request){
		String path = request.getScheme() + "://" + request.getServerName();
		if (request.getServerPort() != 80 && request.getServerPort() != 443) {
			path += ":" + request.getServerPort();
		}
		path += request.getContextPath();
		path = path + request.getServletPath();
		if (request.getQueryString() != null) {
			path += "?" + request.getQueryString();
		}
		return path;
	}
	protected void superProcess(HttpServletRequest request,
			HttpServletResponse response, UserAccount ownUserAccount) {

		try {
			if (log.isInfoEnabled()) {

				log.info(LogMessage.getMessage(ownUserAccount, getPath(request)));

			}
			super.process(request, response);

		} catch (ServletException e) {
			if (e.getRootCause() != null) {
				Throwable throwable = e.getRootCause();
				if (throwable.getClass().equals(FileUploadException.class)) {
					log
							.warn(LogMessage.getMessage(ownUserAccount,
									getPath(request) +"\n" + "File upload exception: "
											+ throwable.getMessage()));
				} else {
					log.fatal(LogMessage.getMessage(ownUserAccount,
							getPath(request) +"\n" + "Unhandled exception: " + throwable.getMessage(),
							throwable));
				}
				loginRedirect(request, response, request.getContextPath()
						+ Constants.INTERNAL_ERROR_ACTION);
			} else {
				log.fatal(LogMessage.getMessage(ownUserAccount,
						getPath(request) +"\n" + "Unhandled exception: ", e));
				loginRedirect(request, response, request.getContextPath()
						+ Constants.INTERNAL_ERROR_ACTION);
			}

		} catch (Exception e) {
			log.fatal(LogMessage.getMessage(ownUserAccount,
					getPath(request) +"\n" + "Unhandled exception: ", e));
			loginRedirect(request, response, request.getContextPath()
					+ Constants.INTERNAL_ERROR_ACTION);
		}
	}

	/**
	 * The loginRedirect method provides redirecting
	 * 
	 * @param res
	 * @param url
	 */
	protected void loginRedirect(HttpServletRequest request,
			HttpServletResponse res, String url) {
		try {
			res.sendRedirect(url);
		} catch (java.io.IOException ioe) {
			HttpSession session = request.getSession();
			UserAccount ownUserAccount = (UserAccount) session
					.getAttribute(Constants.OWN_USERACCOUNT);
			log.fatal(LogMessage.getMessage(ownUserAccount, getPath(request) +"\n" +"IOException: "),
					ioe);

		}
	}

	public void init() throws ServletException {
		super.init();
	}

	public void init(ServletConfig servletConfig) throws ServletException {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
		ServletContext context = servletConfig.getServletContext();

		try {
			NorthStarUtils.loadNorthStarConfiguration(context);
			log.info("NorthStar configuration loaded");
			Repository.init(NorthStarConfiguration.getMappingFiles());
			log.info("Hibernate files loaded");
		} catch (IOException ioe) {
			log.fatal(LogMessage.getMessage("IOException: " + ioe.getMessage(),
					ioe));
			throw new ServletException();
		} catch (Exception ioe) {
			log.fatal(LogMessage.getMessage("Exception: " + ioe.getMessage(),
					ioe));
			throw new ServletException();
		}
		try {
			NorthStarDelegate northStarDelegate = NorthStarDelegate
					.getInstance();
			List semesters = northStarDelegate.getNotClosedSemesters();
			taskDaemon = TaskDaemon.getInstance(servletConfig);
			Iterator semesterIterator = semesters.iterator();
			while (semesterIterator.hasNext()) {
				Semester semester = (Semester) semesterIterator.next();
				taskDaemon.addDeadlineTask(semester);
			}
		} catch (DatabaseException de) {
			log.fatal(LogMessage.getMessage("DatabaseException: "
					+ de.getMessage(), de));
		}
		super.init(servletConfig);
	}



	protected void logParameters(ServletConfig servletConfig) {
		try {
			Enumeration e = servletConfig.getInitParameterNames();
			while (e.hasMoreElements()) {
				String parameterName = (String) e.nextElement();
				String value = servletConfig.getInitParameter(parameterName);
				log.info("Parameter: " + parameterName + " value: " + value);
			}
			ServletContext context = servletConfig.getServletContext();
			Enumeration attributes = context.getInitParameterNames();
			while (attributes.hasMoreElements()) {
				String parameterName = (String) attributes.nextElement();
				String value = context.getInitParameter(parameterName);
				log.info("Context Parameter: " + parameterName + " value: "
						+ value);
			}
		} catch (Exception e) {
			log.error("execption");
		}
	}
}