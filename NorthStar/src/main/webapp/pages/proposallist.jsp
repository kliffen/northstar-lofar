<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />

<html:form action="/processProposalList" method="POST">

	<TABLE>
		<TR>
			<TD width="100%">
			<TABLE width="100%">
				<TR>

					<TD width="100"><jsp:include
						page="/pages/layout/box_header_noFullWidth.jsp" />
					<TABLE width="100" height="25">
						<TR>
							<TD width='20'><html:link title="Personal Account Information (opens in new window)" href="/useradministration/user/setUpUserAccountOverview.do" target="_blank"><html:img width='20' page="/images/list/user.gif"/></html:link></TD>
					  		<TD width="80"><html:link title="Personal Account Information (opens in new window)" href="/useradministration/user/setUpUserAccountOverview.do" target="_blank">My Account</html:link></TD>
				  		</TR>
					</TABLE>

					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" /></TD>
					
					<logic:equal name="proposalListForm" property="hasReviewerPrivileges" value="true">		
						<TD width="100"><jsp:include
							page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="100" height="25">
							<TR>
								<TD width='20'><html:link title="Go to Proposal review (opens in new window)" href="/proposal-review/review/index.do" target="_blank"><html:img width='20' page="/images/list/user.gif"/></html:link></TD>
						  		<TD width="80"><html:link title="Go to Proposal review (opens in new window)" href="/proposal-review/review/index.do" target="_blank">Proposal review</html:link></TD>
					  		</TR>
						</TABLE>
						<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" /></TD>
					</logic:equal>

					<TD width="*" class="center"><img src="<%=request.getContextPath()%><bean:write name="proposalListForm" property="bannerImageUri"/>" /></TD>
					<TD width="80"><jsp:include
						page="/pages/layout/box_header_noFullWidth.jsp" />
					<TABLE width="100" height="25">
						<TR>
							<TD width='20' height="25"><%="<a title=\"Opens the help index (opens in new window)\" href=\""
							+ request.getContextPath()
							+ "/northstarHelp.do\" target=\"help\"><img width='24'src=\""
							+ request.getContextPath()
							+ "/images/list/help.gif\"></a>"%></TD>
							<TD width="80" height="25"><%="<a title=\"Opens the help index (opens in new window)\" href=\""
							+ request.getContextPath()
							+ "/northstarHelp.do\" target=\"help\">Help Index</a>"%></TD>
						</TR>
					</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
					<TD width="90"><jsp:include
						page="/pages/layout/box_header_noFullWidth.jsp" />
					<TABLE width="90" height="25">
						<TR>
							<TD width='20' height="25"><a title="Submit a support ticket"
								href="https://support.astron.nl/rohelpdesk"><img width="20"
								src="<%=request.getContextPath()%>/images/list/info.gif"></a></TD>
							<TD width='70'><a title="Submit a support ticket"
								href="https://support.astron.nl/rohelpdesk">Get Support</a></TD>
						</TR>
					</TABLE>

					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" /></TD>
					<TD width="70"><jsp:include
						page="/pages/layout/box_header_noFullWidth.jsp" />
					<TABLE width="70" height="25">
						<TR>
							<TD width='20' height="25"><a title="Logout"
								href="<%=request.getContextPath()%>/logout.do"><img width="20"
								src="<%=request.getContextPath()%>/images/list/logout.gif"></a></TD>
							<TD width="50" height="25"><a title="Logout"
								href="<%=request.getContextPath()%>/logout.do">Logout</a></TD>
						</TR>
					</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" /></TD>
				</TR>

			</TABLE>
			</TD>
		</TR>
		<logic:notEmpty name="proposalListForm" property="welcomeMessage">
			<TR>
				<TD><jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
				<pre><bean:write name="proposalListForm" property="welcomeMessage"
					filter="false" /></pre> <jsp:include
					page="/pages/layout/box_footer_noFullWidth.jsp" /></TD>
			</TR>
		</logic:notEmpty>
		<tr>
			<td>&nbsp;</td>
		</tr>

		<TR>
			<TD width="100%">
			<TABLE width="100%">
				<TR>

					<TD width="*" align="center"><jsp:include
						page="/pages/layout/box_header_noFullWidth.jsp" />
					<TABLE width="60" height="25">
						<TR>

							<TD width='120'>
							<TABLE width="120">
								<TR>
									<TD width='20'><%="<a title=\"Get started by reading the readme page. Opens in new window.\" href=\""
							+ request.getContextPath()
							+ "/northstarHelp.do#general\" target=\"help\"><img width='20'src=\""
							+ request.getContextPath()
							+ "/images/list/readme.gif\"></a>"%></TD>
									<TD width="100"><%="<a title=\"Get started by reading the readme page. Opens in new window.\" href=\""
							+ request.getContextPath()
							+ "/northstarHelp.do#general\" target=\"help\">Read me first!</a>"%></TD>
								</TR>
							</TABLE>
							</TD>
						</TR>
					</TABLE>


					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" /></TD>


					<TD width="60"><jsp:include
						page="/pages/layout/box_header_noFullWidth.jsp" />
					<TABLE width="60" height="25">
						<TR>
							<TD width='20' height="25"><%="<a title=\"Opens the help window\" href=\""
							+ request.getContextPath()
							+ "/northstarHelp.do#proposallist\" target=\"help\"><img width='24'src=\""
							+ request.getContextPath()
							+ "/images/list/help.gif\"></a>"%></TD>
							<TD width="40" height="25"><%="<a title=\"Opens the help window.\" href=\""
							+ request.getContextPath()
							+ "/northstarHelp.do#proposallist\" target=\"help\">Help</a>"%></TD>
						</TR>
					</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
				</TR>
			</TABLE>
			</TD>
		</TR>
	<logic:equal name="proposalListForm" property="proposalDataOnSession" value="true">
	<TR>
		<TD>
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				<TD valign='top'><PRE>
 There already is a proposal open for editing!
 
 Possibly you have another window with a proposal in edit mode.
 If this is the case, please close <b>this</b> window with the list of
 proposals. You can return to the list of proposals from the
 other window.
 
 All changes since the last save action in the proposal being edited
 will be lost if you perform an action on any proposal from this page!
</PRE>
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	</logic:equal>	
	 <logic:equal name="proposalListForm" property="enableSelectTelescope" value="true">		
	<TR>
		<td><b>Telescopes :</b>
 
	 <logic:iterate id="telescope" property="telescopes" name="proposalListForm">
  	   
	  	    <html:multibox property="selectedTelescopes" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
	            <bean:write name="telescope" />
			</html:multibox>
		
			<bean:write name="telescope"/>
	 </logic:iterate>   
		</td>
	</TR>
	</logic:equal>
	<TR>
		<td><b>Show reviewed proposals :</b>
			<html:radio styleClass="radio" property="showReviewedProposals" value="true" onclick="document.forms[0].submit()">Yes</html:radio>
			<html:radio styleClass="radio" property="showReviewedProposals" value="false" onclick="document.forms[0].submit()">No</html:radio>

		</td>
	</TR>	
	
		<logic:notEmpty name="proposalListForm" property="telescopeBeans">
			<TR>
				<TD>
				<table align="center" class="projecttable">
					<TR>
						<td class="proposaltableheader">&nbsp;Proj_ID&nbsp;</td>
						<td class="proposaltableheader">&nbsp;PI&nbsp;</td>
						<td class="proposaltableheader">&nbsp;Title&nbsp;</td>
						<td class="proposaltableheader">&nbsp;Community&nbsp;</td>
						<td class="proposaltableheader">&nbsp;Category&nbsp;</td>						
						<td class="proposaltableheader">&nbsp;Status&nbsp;</td>
						<td class="proposaltableheader" colspan="5">Options</td>
					</TR>
					<logic:iterate id="telescopeBean" name="proposalListForm"
						property="telescopeBeans" indexId="telescopeIndex">
					<tr>
						<td class="proposalsubtableheader" colspan="11"><bean:write
								name="telescopeBean" property="description" /> Proposals</td>
					</tr>
					<%int i = 0;
			String styleClass = "";

			%>
					<logic:iterate id="proposalBean" name="telescopeBean"
						property="proposalBeans" indexId="proposalIndex">
						<%styleClass = (i++ % 2 == 0 ? "" : "_odd");

			%>
						<TR>
							<html:hidden property='<%= "telescopeBean[" + telescopeIndex + "].proposalBean[" + proposalIndex + "].id" %>'/>
							<TD class="tablefield_pri<%=styleClass%>" valign="top"><bean:write
								name="proposalBean" property="code" /></TD>
							<TD class="tablefield<%=styleClass%>" valign="top"><bean:write
								name="proposalBean" property="piName" /></TD>
							<TD class="tablefield<%=styleClass%>" valign="top"><bean:write
								name="proposalBean" property="title" /></TD>
							<TD class="tablefield<%=styleClass%>" valign="top"><bean:write
								name="proposalBean" property="community" /></TD>
							<TD class="tablefield<%=styleClass%>" valign="top"><bean:write
								name="proposalBean" property="category" /></TD>								
							<TD nowrap="true" valign="top"
								class="status_<bean:write name="proposalBean" property="statusClass"/><%=styleClass%>">
							<bean:write name="proposalBean" property="status" /> <logic:equal
								name="proposalBean" property="locked" value="true">
								<html:img page="/images/list/lock.gif" height="16" width="16" />
							</logic:equal></TD>

							<!-- look in the bean if there should be a Submit button or a Retract button -->
							<logic:equal value="Submit" name="proposalBean"
								property="canSubmitOrRetract">
								<TD class="tablefield<%=styleClass%>" valign="top"><html:submit
									indexed="false"  property='<%= "telescopeBean[" + telescopeIndex + "].editButton[" + proposalIndex + "]" %>' styleClass="list_edit"
									title="Edit this proposal">Edit</html:submit></TD>
								<TD valign="top" class="tablefield<%=styleClass%>"><logic:equal
									name="proposalBean" property="contactAuthor" value="true">
									<html:submit indexed="false" property='<%= "telescopeBean[" + telescopeIndex + "].deleteButton[" + proposalIndex + "]" %>'
										styleClass="list_delete"
										title="Delete this proposal. Prompt first">Delete</html:submit>
								</logic:equal></TD>
								<TD valign="top" class="tablefield<%=styleClass%>"><logic:equal
									name="proposalBean" property="contactAuthor" value="true">
									<html:submit indexed="false" property='<%= "telescopeBean[" + telescopeIndex + "].submitButton[" + proposalIndex + "]" %>'
										styleClass="list_submit"
										title="Submit this proposal. Prompt first">Submit</html:submit>
								</logic:equal></TD>
								<TD valign="top" class="tablefield<%=styleClass%>"><html:submit
									indexed="false" property='<%= "telescopeBean[" + telescopeIndex + "].viewProposalButton[" + proposalIndex + "]" %>'
									title="View your proposal. (pdf)" styleClass="list_view_pdf">View</html:submit></TD>
							</logic:equal>
							<logic:equal value="Retract" name="proposalBean"
								property="canSubmitOrRetract">
								<TD valign="top" class="tablefield<%=styleClass%>" width="67">&nbsp;</TD>
								<TD valign="top" class="tablefield<%=styleClass%>" width="67">&nbsp;</TD>
								<TD valign="top" class="tablefield<%=styleClass%>"><logic:equal
									name="proposalBean" property="contactAuthor" value="true">
									<html:submit indexed="false" property='<%= "telescopeBean[" + telescopeIndex + "].retractButton[" + proposalIndex + "]" %>'
										styleClass="list_retract"
										title="Retract this proposal. Prompt first">Retract</html:submit>
								</logic:equal></TD>

								<TD valign="top" class="tablefield<%=styleClass%>"><html:submit
									indexed="false" property='<%= "telescopeBean[" + telescopeIndex + "].viewProposalButton[" + proposalIndex + "]" %>'
									title="View your proposal. (pdf)" styleClass="list_view_pdf">View</html:submit></TD>
							</logic:equal>
							<logic:equal value="" name="proposalBean"
								property="canSubmitOrRetract">
								<TD valign="top" class="tablefield<%=styleClass%>" width="67">&nbsp;</TD>
								<TD valign="top" class="tablefield<%=styleClass%>" width="67">&nbsp;</TD>
								<TD valign="top" class="tablefield<%=styleClass%>" width="67">&nbsp;</TD>
								<TD valign="top" class="tablefield<%=styleClass%>"><html:submit
									indexed="false" property='<%= "telescopeBean[" + telescopeIndex + "].viewProposalButton[" + proposalIndex + "]" %>'
									title="View your proposal. (pdf)" styleClass="list_view_pdf">View</html:submit></TD>

							</logic:equal>

						</TR>

					</logic:iterate>					
					</logic:iterate>
					<tr>
						<td colspan="11" class="spacer">&nbsp;</td>
					</tr>
				</table>
				</TD>
			</TR>

		</logic:notEmpty>

		<logic:empty name="proposalListForm" property="telescopeBeans">
		<logic:equal name="proposalListForm" property="showReviewedProposals" value="true">
			<TR>
				<TD align="center">- Currently your proposal list is empty -</TD>
			</TR>
		</logic:equal>
		<logic:equal name="proposalListForm" property="showReviewedProposals" value="false">
			<TR>
				<TD align="center">- Currently there are no proposals in preparation -</TD>
			</TR>
		</logic:equal>		
		</logic:empty>

		<TR>
			<TD>&nbsp;</TD>
		</TR>
		<TR>
			<TD align="center"><html:submit tabindex="1" property="newButton"
				styleClass="list_create"
				title="Click to start creating a new proposal.">Create new proposal</html:submit></TD>
		</TR>

	</TABLE>

</html:form>

