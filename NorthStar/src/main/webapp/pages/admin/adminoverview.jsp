<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />

<html:form action="/processAdminOverview" method="GET">

<TABLE>

	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td>Locked proposals:</td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>		
	<logic:notEmpty name="adminOverviewForm" property="proposalLockBeans">
	<TR>
		<TD>
			<table align="center" class="projecttable">
				<TR>
					<td class="tableheader">&nbsp;User&nbsp;</td>
					<td class="tableheader">&nbsp;Last action&nbsp;</td>					
					<td class="tableheader">&nbsp;(ID)&nbsp;</td>
     				<td class="tableheader">&nbsp;Proj_ID&nbsp;</td>
     				<td class="tableheader">&nbsp;Title&nbsp;</td>
     				<td class="tableheader">&nbsp;Status&nbsp;</td>
					<td class="tableheader">&nbsp;Telescope&nbsp;</td>     				
     				<td class="tableheader">&nbsp;Contact Author&nbsp;</td>
					<td class="tableheader">&nbsp;Lock since&nbsp;</td>

				</TR>
			<%
			int i=0;
			String styleClass = "";
			%>
	 			<logic:iterate id="proposalLockBean" name="adminOverviewForm" property="proposalLockBeans">
	 			<%
	 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
	 			%>
 	 			<TR>
					<TD class="tablefield_pri<%=styleClass%>" valign="top"><bean:write name="proposalLockBean" property="owner"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="proposalLockBean" property="lastAction"/></TD>					
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="proposalLockBean" property="id"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="proposalLockBean" property="code"/></TD>					
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="proposalLockBean" property="title"/></TD>
					<TD nowrap="true"  valign="top" class="status_<bean:write name="proposalLockBean" property="statusClass"/><%=styleClass%>">
						<bean:write name="proposalLockBean" property="status"/>
					</TD>
					<TD class="tablefield<%=styleClass%>"  valign="top"><bean:write name="proposalLockBean" property="telescope"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="proposalLockBean" property="contactAuthor"/></TD>

					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="proposalLockBean" property="since"/></TD>

	 			</TR>

	 			</logic:iterate>
				<tr><td colspan="9" class="spacer">&nbsp;</td></tr>
			</table>
		</TD>
	</TR>
	
	</logic:notEmpty>
	
	<logic:empty name="adminOverviewForm" property="proposalLockBeans">
		<TR>
		<TD align="center">- Currently no user works on proposals -</TD>
	</TR>
	</logic:empty>
	

	
</TABLE>

</html:form>

