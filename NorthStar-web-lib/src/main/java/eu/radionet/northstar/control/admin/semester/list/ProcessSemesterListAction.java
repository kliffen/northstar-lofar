// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.semester.list;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.control.ActionServlet;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.Semester;

public class ProcessSemesterListAction extends Action {

	private NorthStarDelegate northStarDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SemesterListForm semesterListForm = (SemesterListForm) form;

		HttpSession session = request.getSession();
		/*
		 * if edit button is pressed
		 */
		if (!AstronValidator.isBlankOrNull(semesterListForm.getEditButton(0))) {
			int index = semesterListForm.getSelectedValue();
			SemesterBean semesterBean = semesterListForm.getSemesterBean(index);
			return ParameterAppender.append(mapping.findForward("addedit"),
					"semesterId", semesterBean.getId());
		} else if (!AstronValidator
				.isBlankOrNull(semesterListForm.getAddButton())) {
			return mapping.findForward("addedit");
		} else if (!AstronValidator.isBlankOrNull(semesterListForm
				.getDeleteButton(0))) {
			int index = semesterListForm.getSelectedValue();
			SemesterBean semesterBean = semesterListForm.getSemesterBean(index);
			northStarDelegate = NorthStarDelegate.getInstance();
			Semester semester = northStarDelegate.getSemester(AstronConverter
					.toInteger(semesterBean.getId()));
			if (semester.getLastAssigned().intValue() == 0) {
				northStarDelegate.deleteSemester(semester);
			}
			List semesters = northStarDelegate.getNotClosedSemesters();
			ActionServlet.taskDaemon.cancelAllTasks();
			Iterator semesterIterator = semesters.iterator();
			while (semesterIterator.hasNext()){
				Semester temp = (Semester) semesterIterator.next();
				ActionServlet.taskDaemon.addDeadlineTask(temp);
			}	
		}else {
			SelectionBean selectionBean = new SelectionBean();
			selectionBean.setSelectedTelescopes(semesterListForm.getSelectedTelescopes());
			selectionBean.setSelectedCategories(semesterListForm.getSelectedCategories());
			selectionBean.setAfterCurrentDate(semesterListForm.isAfterCurrentDate());
			selectionBean.setSelectedCommunities(semesterListForm.getSelectedCommunities());
			session.setAttribute(Constants.ADMIN_SEMESTER_SELECTION,
					selectionBean);
		}
		return mapping.findForward(Constants.REFRESH);
	}
}
