<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<html:html>
<head>
	<link rel=stylesheet href="<%=request.getContextPath()%>/css/proposalLayout.css" type="text/css">
    <title>NorthStar Help File</title>
</head>

<body>
<jsp:include page="../../../layout/box_header_noFullWidth.jsp" /> 
<h2>the NorthStar Help Page:</h2><PRE>
A walk through manual for the NorthStar tool for LOFAR proposals can be found at:</PRE>
<br/>
<a href="http://old.astron.nl/sites/astron.nl/files/cms/PDF/LOFARPSP.pdf">
Walk through manual of NorthStar [PDF]</a>
<br/>
<PRE>

Further information on LOFAR and its technical characteristics can be found at:</PRE>
<br/>
<a href="http://old.astron.nl/radio-observatory/astronomers/technical-information/lofar-technical-information">
http://old.astron.nl/radio-observatory/astronomers/technical-information/lofar-technical-information</a>
<br/>
<jsp:include page="../../../layout/box_footer_noFullWidth.jsp" /> 
</body>
</html:html>