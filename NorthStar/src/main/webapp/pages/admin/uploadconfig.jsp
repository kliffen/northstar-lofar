<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />

<html:form action="/setUpUploadConfig" enctype="multipart/form-data" method="POST">
<html:hidden property="currentConfigFileUrl" styleId="currentConfigFileUrl"/>
<html:hidden property="currentOptionsFileUrl" styleId="currentOptionsFileUrl"/>

	<TABLE>
		<tr><td>
			&nbsp;<br>
		</td></tr>

		<tr><td>
		<TABLE>
		
		<logic:equal name="uploadConfigForm" property="hasMultipleAccess" value="true">
			<tr><td><br><br></td><td>Telescope:</td><td> 
			<bean:define id="mytelescopelist" name="uploadConfigForm" property="userTelescopes"/>
			<html:select name="uploadConfigForm" property="selectedTelescope" onchange="document.forms[0].submit()">
				<html:options collection="mytelescopelist" property="telescope" labelProperty="telescope"/>
			</html:select>
			</td></tr>
		</logic:equal>
		<logic:equal name="uploadConfigForm" property="hasMultipleAccess" value="false">
			<tr><td><br><br></td><td>Telescope:</td><td>
			 			<bean:write name="uploadConfigForm" property="selectedTelescope"/>
			 </td></tr>
		</logic:equal>
		<tr><td><br><br></td><td>
			Current config: 
			</td><td>
				<html:link action="/processUploadConfig.do?fileType=config">
							<bean:write name="uploadConfigForm" property="currentConfigFileUrl"/>
					</html:link>
					( <bean:write name="uploadConfigForm" property="currentConfigFileDate"/> )
			</td></tr>
			<tr><td><br><br></td><td>
			Current options: 
			</td><td>
				<html:link action="/processUploadConfig.do?fileType=options">
							<bean:write name="uploadConfigForm" property="currentOptionsFileUrl"/>
					</html:link> 
					( <bean:write name="uploadConfigForm" property="currentOptionsFileDate"/> )
			</td></tr>
			<tr><td><br><br></td><td>
			Upload config file: 
			</td><td>
				<logic:equal name="uploadConfigForm" property="configOk" value="false">
					<html:file  property="configFile" size="60" style="textarea"/>
					<html:submit property="uploadFileButton"  title="Upload" styleClass="list_upload">Upload</html:submit>
				</logic:equal>
				<logic:equal name="uploadConfigForm" property="configOk" value="true">
					<html:text property="newConfigFileName" size="60" />
					<font color="green"> OK</font>
				</logic:equal>
			</td></tr>
			<tr><td><br><br></td><td>
			Upload options file: 
			</td><td>
				<logic:equal name="uploadConfigForm" property="optionsOk" value="false">
					<html:file  property="optionsFile" size="60" style="textarea"/>
					<html:submit property="uploadFileButton"  title="Upload" styleClass="list_upload">Upload</html:submit>
				</logic:equal>
				<logic:equal name="uploadConfigForm" property="optionsOk" value="true">
					<html:text property="newOptionsFileName" size="60" />
					<font color="green"> OK</font>
				</logic:equal>
			</td></tr>
			<tr><td><br><br></td><td>
			previous config file: 
			</td><td>
				( <bean:write name="uploadConfigForm" property="previousConfigFileDate"/> )
				<logic:equal name="uploadConfigForm" property="optionsOk" value="false">
				
					<html:link action="/processUploadConfig.do?fileRestore=config"> restore </html:link>
				</logic:equal>
			</td></tr>
			<tr><td><br><br></td><td>
			previous options file: 
			</td><td>
				( <bean:write name="uploadConfigForm" property="previousOptionsFileDate"/> )
				<logic:equal name="uploadConfigForm" property="optionsOk" value="false">
				
					<html:link action="/processUploadConfig.do?fileRestore=options"> restore </html:link>
				</logic:equal>
			</td></tr>
			
		</TABLE>
		</td></tr>		
		<tr><td>
			&nbsp; <span class="error"><astron:errors property="uploadConfig" ifnotexist=""/></span>
		</td></tr>
	</TABLE>
	<table width="100%">
		<tr>
			<td class="right">
				<br>
		</tr>
	</table>

</html:form>




