// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.justification;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.Utils;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProcessProposalAction;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.Justification;

public class ProcessJustificationAction extends ProcessProposalAction {

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		JustificationForm justificationForm = (JustificationForm) form;
		HttpSession session = request.getSession();
		ActionMessages errors = new ActionMessages();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount, Constants.ENTER_CLASS));
		}
		/*
		 * if pressed on Discard changes
		 */
		if (isCancelled(request)) {

			return cancel(mapping, request, ownUserAccount);

		}
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		Justification justification = proposalData.getProposal().getJustification();
		if (justification == null) {
			justification = new Justification();
			proposalData.getProposal().setJustification(justification);
		}
		errors = validate(justificationForm, mapping, request);
		if (errors.size() > 0) {
			saveErrors(request, errors);
			return mapping.findForward(Constants.REFRESH);
		}
		justification.setTitle(justificationForm.getTitle());
		justification.setAbstractText(justificationForm.getAbstractText());
		justification.setEnvelopeSheet(justificationForm.getSelectedEnvelopeSheet());
		justification.setDefaultROProcessing(justificationForm.isDefaultROProcessing());
		justification.setExternalProcessing(justificationForm.isExternalProcessing());
		justification.setExternalProcessingReason(justificationForm.getExternalProcessingReason());
		justification.setCepRequesting(justificationForm.isCepRequesting());
		justification.setCepRequestingReason(justificationForm.getCepRequestingReason());
		justification.setInternationalStation(justificationForm.isInternationalStation());
		justification.setInternationalStationEssential(justificationForm.isInternationalStationEssential());
		justification.setInternationalStationEssentialReason(justificationForm.getInternationalStationEssentialReason());
		justification.setLtaRawStorage(justificationForm.isLtaRawStorage());
		justification.setLtaRawStorageReason(justificationForm.getLtaRawStorageReason());
		justification.setLtaStorage(justificationForm.isLtaStorage());
		justification.setLtaStorageReason(justificationForm.getLtaStorageReason());
		justification.setLtaStorageLocation(justificationForm.getLtaStorageLocation());
		justification.setMaxDataRate(justificationForm.getMaxDataRate());

		justification.setNighttime(justificationForm.isNighttime());
		justification.setNightTimeReason(justificationForm.getNightTimeReason());
		justification.setOfflineROProcessing(justificationForm.isOfflineROProcessing());
		justification.setOfflineROProcessingReason(justificationForm.getOfflineROProcessingReason());
		justification.setOtherSchedulingConstraints(justificationForm.isOtherSchedulingConstraints());
		justification.setOtherSchedulingConstraintsReason(justificationForm.getOtherSchedulingConstraintsReason());
		justification.setCombinedDataProductRequest(justificationForm.isCombinedDataProductRequest());
		justification.setCombinedDataProductRequestReason(justificationForm.getCombinedDataProductRequestReason());
		justification.setParallelObservation(justificationForm.isParallelObservation());
		justification.setParallelObservationReason(justificationForm.getParallelObservationReason());
		justification.setRoProcessing(justificationForm.isRoProcessing());
		justification.setRoProcessingReason(justificationForm.getRoProcessingReason());
		justification.setSensitivity(justificationForm.getSensitivity());

		justification.setCoObservationTeam(justificationForm.isCoObservationTeam());
		justification.setFillerTime(justificationForm.isFillerTime());
		justification.setFillerTimeReason(justificationForm.getFillerTimeReason());
		justification.setObservationStrategyText(justificationForm.getObservationStrategyText());
		/*
		 * if forward in form is set, forward to an other pages. (See jsp's for
		 * more info)
		 */
		if (!AstronValidator.isBlankOrNull(justificationForm.getForward())) {
			return mapping.findForward(justificationForm.getForward());
		}

		if (justificationForm.getUploadScientificFileButton() != null) {
			return ParameterAppender.append(mapping.findForward(Constants.UPLOAD), "kindUploadFile",
					Constants.SCIENTIFIC);
		} else if (justificationForm.getUploadTechnicalDetailsFileButton() != null) {
			return ParameterAppender.append(mapping.findForward(Constants.UPLOAD), "kindUploadFile",
					Constants.TECHNICAL);

		} else if (justificationForm.getUploadFigureFileButton() != null) {
			return ParameterAppender.append(mapping.findForward(Constants.UPLOAD), "kindUploadFile", Constants.FIGURE);

		} else if (justificationForm.getDeleteFigureFileButton() != null) {
			proposalData.setDeleteFigureFile(true);
			proposalData.setFigureFile(null);
			justification.setFigureFileDate(null);
			justification.setFigureFileName(null);
			justification.setFigureFilePath(null);
			justification.setFigureFileSize(null);
			justification.setFigureFilePages(null);
			return mapping.findForward(Constants.REFRESH);
		} else if (justificationForm.getDeleteTechnicalDetailsFileButton() != null) {
			proposalData.setDeleteTechnicalDetailsFile(true);
			proposalData.setTechnicalDetailsFile(null);
			justification.setTechnicalDetailsFileDate(null);
			justification.setTechnicalDetailsFileName(null);
			justification.setTechnicalDetailsFilePath(null);
			justification.setTechnicalDetailsFileSize(null);
			justification.setTechnicalDetailsFilePages(null);
			return mapping.findForward(Constants.REFRESH);
		}
		ActionForward forward = processGeneralButtons(justificationForm, mapping, request, errors, proposalData,
				ownUserAccount);
		if (forward != null) {
			return forward;
		}
		return mapping.getInputForward();
	}

	private ActionMessages validate(JustificationForm justificationForm, ActionMapping mapping,
			HttpServletRequest request) {
		ActionMessages errors = new ActionMessages();
		/*
		 * Integer titleCount =
		 * Utils.countCharacters(justificationForm.getTitle()); Integer
		 * abstractCount =
		 * Utils.countWords(justificationForm.getAbstractText());
		 * 
		 * HttpSession session = request.getSession(); ProposalData proposalData
		 * = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		 * TelescopeConfiguration telescopeConfiguration =
		 * NorthStarConfiguration.getTelescopeConfiguration(proposalData.
		 * getProposal());
		 * 
		 * if (titleCount.intValue() >
		 * telescopeConfiguration.getMaxCharsForTitle()){ errors.add("title",
		 * new ActionMessage( "error.exceed.maximumcharacters")); } if
		 * (abstractCount.intValue() >
		 * telescopeConfiguration.getMaxWordsForAbstract()){
		 * errors.add("abstractText", new ActionMessage(
		 * "error.exceed.maximumwords")); }
		 */
		if (justificationForm.isAllowTechnicalJustification()) {
			if (justificationForm.getSensitivity() == 0) {
				errors.add("sensitivity", new ActionMessage("error.required"));
			}

			if (justificationForm.getMaxDataRate() == 0) {
				errors.add("maxDataRate", new ActionMessage("error.required"));
			}
		}
		return errors;
	}

}
