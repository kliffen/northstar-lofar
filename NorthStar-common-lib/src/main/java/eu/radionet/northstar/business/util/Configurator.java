// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.util;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.ServletContext;

import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.radionet.northstar.business.configuration.CollaborationConfiguration;
import eu.radionet.northstar.business.configuration.CommunityConfiguration;
import eu.radionet.northstar.business.configuration.EmailConfiguration;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.Telescope;
import eu.radionet.northstar.business.configuration.options.ConfigurationType;
import eu.radionet.northstar.business.deadline.WebDAVConfigurator;

public class Configurator {
	private Log log = LogFactory.getLog(this.getClass());

	protected static final String NORTHSTAR = "northstar";

	protected static final String NAME = "name";

    protected static final String BANNER_IMAGE_URI = "banner-image-uri";

    protected static final String WELCOME_URI = "welcome-uri";

    protected static final String NORTHSTAR_HELP_URI = "northstar-help-uri";

	protected static final String DESCRIPTION = "description";

	protected static final String EMAIL_SENDER = "emailsender";

	protected static final String ADMINISTRATOR_EMAIL = "administrator-email";

	protected static final String MAPPING_FILES = "mapping-files";

	protected static final String MAPPING_FILE = "mapping-file";

	protected static final String TELESCOPES = "telescopes";

	protected static final String TELESCOPE = "telescope";

	protected static final String START_TIME = "starttime";

	protected static final String END_TIME = "endtime";

	protected static final String COMMUNITY = "community";

	protected static final String COMMUNITIES = "communities";

	protected static final String TELESCOPE_CONFIGURATION_FILE = "telescope-configuration-file";

	protected static final String EMAILS = "emails";

	protected static final String EMAIL = "email";

	protected static final String TYPE = "type";

	protected static final String URI = "uri";

	protected static final String BCC = "bcc";

	protected static final String COLLABORATION = "collaboration";

	protected static final String COMMITTEES = "committees";

	protected static final String COMMITTEE = "committee";
	
	protected static final String MAINTENANCE_ACCOUNT = "maintenanceAccount";

	protected static final String REPOSITORY = "repository";
	
	protected static final String RESTRICT_USER_TELESCOPE = "restrict-user-telescope";

	protected static final String LABELS_FILE = "labels-file";

	/*
	 * location of the common options
	 */
	protected static final String COMMON_OPTIONS_URI = "/conf/common-options.xml";

	
	protected ServletContext servletContext = null;

	public void configure(Document document, ServletContext servletContext)
			throws Exception {
		this.servletContext = servletContext;
		/*
		 * search for project
		 */
		NodeList elements = document.getElementsByTagName(NORTHSTAR);
		for (int i = 0; i < elements.getLength(); i++) {
			Node node = (Node) elements.item(i);
			// log.info("Node: " + node.getNodeName());
			parseNorthStarNode(node);
		}
		/*
		 * add the common options to the NorthStarConfiguration
		 */
		Map options = null;
		NorthStarConfiguration configuration = NorthStarConfiguration.getInstance();
		try 
		{
			String commonOptionsFile = NorthStarUtils.read(COMMON_OPTIONS_URI,servletContext);						
			XMLConverter xmlConverter = XMLConverter.getInstance();
			Document optionConfigDocument = 
				xmlConverter.convertStringToDocument(commonOptionsFile);
			OptionsConfigurator optionsConfigurator = new OptionsConfigurator();
			options =  optionsConfigurator.configure(optionConfigDocument,servletContext);
			
		} 
		catch (NullPointerException npe) 
		{
			log.info("Cannot read : " + COMMON_OPTIONS_URI);
		}
		
		configuration.setCommonOptions(options);
		
		
	}

	
	protected void parseNorthStarNode(Node northStarNode) throws Exception {
		NorthStarConfiguration configuration = NorthStarConfiguration
				.getInstance();
		for (int i = 0; i < northStarNode.getChildNodes().getLength(); i++) {
			Node nodeChild = northStarNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, NAME)) {
					configuration.setName(getValue(nodeChild));
                } else if (equal(nodeChild, BANNER_IMAGE_URI)) {
                    configuration.setBannerImageUri(getValue(nodeChild));
				} else if (equal(nodeChild, WELCOME_URI)) {
					configuration.setWelcomeUri(getValue(nodeChild));
					try {
						configuration.setWelcomeMessage(NorthStarUtils.read(
								NorthStarConfiguration.getWelcomeUri(),
								servletContext));
					} catch (NullPointerException npe) {
						log.error("Cannot read : "
								+ NorthStarConfiguration.getWelcomeUri());
					}
                } else if (equal(nodeChild, NORTHSTAR_HELP_URI)) {
                    configuration.setNorthstarHelpUri(getValue(nodeChild));
                } else if (equal(nodeChild, LABELS_FILE)) {
                    configuration.setLabelsFile(getValue(nodeChild));
				} else if (equal(nodeChild, EMAIL_SENDER)) {
					configuration.setEmailSender(getValue(nodeChild));
				} else if (equal(nodeChild, MAINTENANCE_ACCOUNT)) {
					configuration.setMaintenanceAccount(getValue(nodeChild));
				} else if (equal(nodeChild, ADMINISTRATOR_EMAIL)) {
					configuration.setAdministratorEmail(getValue(nodeChild));
				} else if (equal(nodeChild, TELESCOPES)) {
					parseTelescopes(nodeChild, configuration);
				} else if (equal(nodeChild, COLLABORATION)) {
					parseCollaboration(nodeChild, configuration);
				} else if (equal(nodeChild,	RESTRICT_USER_TELESCOPE)){
					configuration.setRestrictUserTelescope(true);
				} else if (equal(nodeChild, COMMUNITIES)) {
					parseCommunities(nodeChild, configuration);
				} else if (equal(nodeChild, EMAILS)) {
					parseEmails(nodeChild, NorthStarConfiguration
							.getEmailConfigurations());
				} else if (equal(nodeChild, REPOSITORY)) {
					WebDAVConfigurator webDAVConfigurator = new WebDAVConfigurator();
					NorthStarConfiguration.setWebDAVConfiguration(webDAVConfigurator.parseRepositoryNode(nodeChild));

				}
			}

		}

	}


	protected void parseCollaboration(Node collaborationNode,
			NorthStarConfiguration configuration) throws IOException {
		CollaborationConfiguration collaborationConfiguration = new CollaborationConfiguration();
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < collaborationNode.getChildNodes().getLength(); i++) {
			Node telescopeChild = collaborationNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, telescopeChild
					.getClass())) {
				if (equal(telescopeChild, EMAILS)) {
					parseEmails(telescopeChild, collaborationConfiguration
							.getEmailConfigurations());
				}

			}
		}
		configuration.setCollaborationConfiguration(collaborationConfiguration);
	}

	protected void parseCommunities(Node telescopesNode,
			NorthStarConfiguration configuration) throws Exception {
		NorthStarConfiguration.getCommunityConfigurations().clear();
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < telescopesNode.getChildNodes().getLength(); i++) {
			Node telescopeChild = telescopesNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, telescopeChild
					.getClass())) {
				if (equal(telescopeChild, COMMUNITY)) {
					configuration
							.addCommunityConfiguration(parseCommunity(telescopeChild));
				}

			}
		}
	}

	protected CommunityConfiguration parseCommunity(Node telescopeNode)
			throws Exception {
		CommunityConfiguration communityConfiguration = new CommunityConfiguration();
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < telescopeNode.getChildNodes().getLength(); i++) {
			Node nodeChild = telescopeNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, NAME)) {
					communityConfiguration.setName(getValue(nodeChild));
				} else if (equal(nodeChild, DESCRIPTION)) {
					communityConfiguration.setDescription(getValue(nodeChild));
				}

			}
		}
		return communityConfiguration;
	}

	protected void parseTelescopes(Node telescopesNode,
			NorthStarConfiguration configuration) throws Exception {
		NorthStarConfiguration.getTelescopes().clear();
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < telescopesNode.getChildNodes().getLength(); i++) {
			Node telescopeChild = telescopesNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, telescopeChild
					.getClass())) {
				if (equal(telescopeChild, TELESCOPE)) {
					Telescope telescope = parseTelescope(telescopeChild);
					if (telescope != null) {
						NorthStarConfiguration.getTelescopes().add(telescope);
					}
				}

			}
		}
	}

	protected Telescope parseTelescope(Node telescopeNode) throws Exception {
		Telescope telescope = new Telescope();
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < telescopeNode.getChildNodes().getLength(); i++) {
			Node nodeChild = telescopeNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, NAME)) {
					telescope.setName(getValue(nodeChild));
				} else if (equal(nodeChild, DESCRIPTION)) {
					telescope.setDescription(getValue(nodeChild));
				} else if (equal(nodeChild, TELESCOPE_CONFIGURATION_FILE)) {
					String configFileUri = getValue(nodeChild);
					try {

						String configFile = NorthStarUtils.read(configFileUri,servletContext);						
						XMLConverter xmlConverter = XMLConverter.getInstance();
						Document northstarConfigDocument = xmlConverter
								.convertStringToDocument(configFile);
						TelescopeConfigurator telescopeConfigurator = new TelescopeConfigurator();
						telescope
								.setTelescopeConfigurations(telescopeConfigurator
										.configure(northstarConfigDocument,
												servletContext));
					} catch (NullPointerException npe) {
						log.info("Cannot read : "
								+ configFileUri);
						return null;
					}
				}

			}
		}
		return telescope;
	}



	protected void parseEmails(Node emailsNode, Hashtable hashTable)
			throws IOException {
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < emailsNode.getChildNodes().getLength(); i++) {
			Node emailChild = emailsNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, emailChild
					.getClass())) {
				if (equal(emailChild, EMAIL)) {
					parseEmail(emailChild, hashTable);
				}

			}
		}
	}

	protected void parseEmail(Node emailNode, Hashtable hashTable)
			throws IOException {
		EmailConfiguration emailConfiguration = new EmailConfiguration();
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < emailNode.getChildNodes().getLength(); i++) {
			Node nodeChild = emailNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, TYPE)) {
					emailConfiguration.setKey(getValue(nodeChild));
				} else if (equal(nodeChild, URI)) {
					emailConfiguration.setUri(getValue(nodeChild));
					try {
						emailConfiguration.setMessage(NorthStarUtils.read(
								emailConfiguration.getUri(), servletContext));
					} catch (NullPointerException npe) {
						log.error("Cannot read : "
								+ emailConfiguration.getUri());
					}
				} else if (equal(nodeChild, BCC)) {
					emailConfiguration.setBcc(getValue(nodeChild));
				}
			}
		}
		if (!AstronValidator.isBlankOrNull(emailConfiguration.getKey())
				&& !AstronValidator.isBlankOrNull(emailConfiguration.getUri())) {
			hashTable.put(emailConfiguration.getKey(), emailConfiguration);
		}
	}

	/**
	 * The getValue method returns the value of an node
	 * 
	 * @param node
	 * @return
	 */
	protected String getValue(Node node) {
		String value = null;
		if (node.getFirstChild() != null) {
			value = node.getFirstChild().getNodeValue();
			if (log.isDebugEnabled()) {
				log.debug("Node: " + node.getNodeName() + " value: " + value);
			}
		}
		return value;
	}

	/**
	 * The equal method compares if an node has the given name
	 * 
	 * @param node
	 * @param nodeName
	 * @return
	 */
	protected boolean equal(Node node, String nodeName) {
		return node.getNodeName().equals(nodeName);
	}

	protected Date getDate(String value) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat();

		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		simpleDateFormat.applyPattern("dd-MM-yyyy");
		return simpleDateFormat.parse(value);
	}
}
