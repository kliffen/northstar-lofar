// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.upload;

// import java.util.ArrayList;
// import java.util.List;
// import nl.astron.useradministration.data.entities.User;

import java.util.ArrayList;
import java.util.List;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class UploadConfigForm extends ActionForm {
	private List userTelescopes = new ArrayList();
	private String selectedTelescope = null;
	private String currentConfigFileUrl = null;
	private String currentOptionsFileUrl = null;
	private String currentConfigFileDate = null;
	private String currentOptionsFileDate = null;
	
	private String previousConfigFileDate = null;
	private String previousOptionsFileDate = null;
	private String PreviousConfigFileLocation = null;
	private String PreviousOptionsFileLocation = null;
	
	private String newConfigFileName = null;
	private String newOptionsFileName = null;
	
	//private String configFile = null;
	protected FormFile optionsFile = null;
	protected FormFile configFile = null;
	private boolean configOk = false;
	private boolean optionsOk = false;
	private boolean hasMultipleAccess = false;
	
	public void setUserTelescopes(List userTelescopeList){
		this.userTelescopes = userTelescopeList;
	}
	
	public List getUserTelescopes(){
		return this.userTelescopes;
	}
	
	public void setSelectedTelescope(String selectedTelescopeLabel) {
        this.selectedTelescope = selectedTelescopeLabel;
    }
	
	 public String getSelectedTelescope() {
	        return selectedTelescope;
	    }
	 
	 public void setOptionsFile(FormFile lOptionsFile) {
	        this.optionsFile = lOptionsFile;
	    }
		
	 public FormFile getOptionsFile() {
	        return optionsFile;
	    }
	 
	 public void setConfigFile(FormFile lConfigFile) {
	        this.configFile = lConfigFile;
	    }
		
	 public FormFile getConfigFile() {
	        return configFile;
	    }
	 
	 public void setCurrentConfigFileUrl(String configFileUrl) {
	        this.currentConfigFileUrl = configFileUrl;
	    }
		
	 public String getCurrentConfigFileUrl() {
	        return currentConfigFileUrl;
	    }
	 

	 public void setNewConfigFileName(String configFileName) {
	        this.newConfigFileName = configFileName;
	    }
		
	 public String getNewConfigFileName() {
	        return newConfigFileName;
	    }
	 
	 public void setNewOptionsFileName(String OptionsFileName) {
	        this.newOptionsFileName = OptionsFileName;
	    }
		
	 public String getNewOptionsFileName() {
	        return newOptionsFileName;
	    }
	 
	 public void setCurrentOptionsFileUrl(String OptionsFileUrl) {
	        this.currentOptionsFileUrl = OptionsFileUrl;
	    }
	 
	 public String getCurrentConfigFileDate() {
	        return currentConfigFileDate;
	    }
	 
	 public void setCurrentConfigFileDate(String ConfigFileDate) {
	        this.currentConfigFileDate = ConfigFileDate;
	    }
	 
	 public String getCurrentOptionsFileDate() {
	        return currentOptionsFileDate;
	    }
	 
	 public void setCurrentOptionsFileDate(String OptionsFileDate) {
	        this.currentOptionsFileDate = OptionsFileDate;
	    }
	 
	 public String getCurrentOptionsFileUrl() {
	        return currentOptionsFileUrl;
	    }
	 
	 public void setHasMultipleAccess(boolean inp){
		 this.hasMultipleAccess = inp;
	 }
	 
	 public boolean getHasMultipleAccess(){
		 return hasMultipleAccess;
	 }
	 
	 public void setConfigOk(boolean inp){
		 this.configOk = inp;
	 }
	 
	 public boolean getConfigOk(){
		 return configOk;
	 }
	
	 public void setOptionsOk(boolean inp){
		 this.optionsOk = inp;
	 }
	 
	 public boolean getOptionsOk(){
		 return optionsOk;
	 }

	public String getPreviousConfigFileDate() {
		return previousConfigFileDate;
	}

	public void setPreviousConfigFileDate(String previousConfigFileDate) {
		this.previousConfigFileDate = previousConfigFileDate;
	}

	public String getPreviousOptionsFileDate() {
		return previousOptionsFileDate;
	}

	public void setPreviousOptionsFileDate(String previousOptionsFileDate) {
		this.previousOptionsFileDate = previousOptionsFileDate;
	}

	public String getPreviousConfigFileLocation() {
		return PreviousConfigFileLocation;
	}

	public void setPreviousConfigFileLocation(String previousConfigFileLocation) {
		PreviousConfigFileLocation = previousConfigFileLocation;
	}

	public String getPreviousOptionsFileLocation() {
		return PreviousOptionsFileLocation;
	}

	public void setPreviousOptionsFileLocation(String previousOptionsFileLocation) {
		PreviousOptionsFileLocation = previousOptionsFileLocation;
	}
}
