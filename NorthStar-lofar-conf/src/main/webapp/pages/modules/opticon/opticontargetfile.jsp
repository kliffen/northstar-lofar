<%@page import="eu.radionet.northstar.control.*" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />

<!-- This jsp handles uploading a target list -->
<html:form action="/processTargetFile" enctype="multipart/form-data" method="POST">
	<table width="100%">
		<!--  Help link in the top right corner -->
		<TR>
		<TD width="*">&nbsp;</TD>
		<TD class="help" width="60">
					<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="60" height="25">
							<TR>
								<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#sopticonuploadlist\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
				  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#opticonuploadlist\" target=\"help\">Help</a>"%></TD>
							</TR>
			  			</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />			
			</TD>
		</TR>
	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
				<jsp:include page="uploadformat.jsp"/>
			</td> 
		</tr>
		<!-- 
			The actual upload field
		 -->
		<tr>
			<td class="right">Upload targets file :&nbsp;</td>
			<td class="left"><html:file property="uploadFile" size="60" style="textarea"/>
			</td>
		</tr>
		<tr>
			<td colspan="2"><span class="error"><astron:errors property="uploadFile" ifnotexist=""/></span>&nbsp;</td>
		</tr>
		<!--  Cancel/OK buttons -->
		<tr>
			<td class="left"><html:submit property="saveButton" styleClass="list_upload" title="Upload">Upload</html:submit></td><td class="right">
				<html:cancel styleClass="list_decline_t" title="Cancel upload">Cancel</html:cancel></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	</table>
</html:form>

