
--
-- Table structure for table `northstar`.`thesis`
--

DROP TABLE IF EXISTS `thesis`;
CREATE TABLE `thesis` (
  `id` int(11) NOT NULL auto_increment,
  `additionalissues_id` int(11) default NULL,
  `indexid` int(11) default NULL,
  `student_ref` int(11) default NULL,
  `student_name` varchar(255) default NULL,
  `supervisor_ref` int(11) default NULL,
  `supervisor_name` varchar(255) default NULL,
  `expectedcompletiondate` datetime default NULL,
  `datarequired` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `member_student_FK` (`student_ref`),
  KEY `member_supervisor_FK` (`supervisor_ref`),
  KEY `additionalissues_thesis_IND` (`additionalissues_id`),
  KEY `additionalissues_index_thesis_IND` (`indexid`),
  CONSTRAINT `additionalissues_thesis_IND` FOREIGN KEY (`additionalissues_id`) REFERENCES `additionalissues` (`id`),
  CONSTRAINT `member_student_FK` FOREIGN KEY (`student_ref`) REFERENCES `member` (`id`),
  CONSTRAINT `member_supervisor_FK` FOREIGN KEY (`supervisor_ref`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

