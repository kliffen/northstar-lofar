// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.business.configuration.options.config.DependenciesType;
import eu.radionet.northstar.business.configuration.options.config.OptionsConfigurationType;
import eu.radionet.northstar.business.configuration.options.definition.DoubleType;
import eu.radionet.northstar.business.configuration.options.definition.FieldType;
import eu.radionet.northstar.business.configuration.options.definition.IntegerType;
import eu.radionet.northstar.business.configuration.options.definition.ValidationType;

public class OptionsDelegate {
	/**
	 * Retrieve the default option of a option list
	 * 
	 * @param name
	 *            name of the option list
	 * @param contextConfiguration
	 * @return value of the default option
	 */
	public static boolean isValid(String name, Double value,
			FieldsDefinitionType fieldsDefinitionType) {
		FieldType fieldType = (FieldType) fieldsDefinitionType.getFields().get(
				name);
		if (fieldType != null && value != null) {
			if (fieldType.getValidation() != null) {
				ValidationType validation = fieldType.getValidation();
				Double lowerBoundary = AstronConverter.toDouble(validation
						.getLowerBoundary());
				Double upperBoundary = AstronConverter.toDouble(validation
						.getUpperBoundary());
				if (lowerBoundary != null) {
					if (value.doubleValue() < lowerBoundary.doubleValue()) {
						return false;
					}
				}
				if (upperBoundary != null) {
					if (value.doubleValue() > upperBoundary.doubleValue()) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public static boolean isValid(String name, String value,
			DoubleType doubleType) {
		if (AstronValidator.isBlankOrNull(value)){
			return true;
		}		
		if (!AstronValidator.isDouble(value)){
			return false;
		}
		if (doubleType != null && value != null) {
			double doubleValue = AstronConverter.toDouble(value).doubleValue();
			if (doubleType.getValidation() != null) {
				ValidationType validation = doubleType.getValidation();
				Double lowerBoundary = AstronConverter.toDouble(validation
						.getLowerBoundary());
				Double upperBoundary = AstronConverter.toDouble(validation
						.getUpperBoundary());
				if (lowerBoundary != null) {
					if (doubleValue < lowerBoundary.doubleValue()) {
						return false;
					}
				}
				if (upperBoundary != null) {
					if (doubleValue > upperBoundary.doubleValue()) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public static boolean isValid(String name, String value,
			IntegerType integerType) {
		if (AstronValidator.isBlankOrNull(value)){
			return true;
		}	
		if (!AstronValidator.isInt(value)){
			return false;
		}
		if (integerType != null && value != null) {
			int intValue = AstronConverter.toInteger(value).intValue();
			if (integerType.getValidation() != null) {
				ValidationType validation = integerType.getValidation();
				Integer lowerBoundary = AstronConverter.toInteger(validation
						.getLowerBoundary());
				Integer upperBoundary = AstronConverter.toInteger(validation
						.getUpperBoundary());
				if (lowerBoundary != null) {
					if (intValue < lowerBoundary.intValue()) {
						return false;
					}
				}
				if (upperBoundary != null) {
					if (intValue > upperBoundary.intValue()) {
						return false;
					}
				}
			}
		}
		return true;
	}
	
	public static boolean allowedToDisplay(String name,
			FieldsDefinitionType fieldsDefinitionType) {
		if (fieldsDefinitionType != null) {
			FieldType fieldType = (FieldType) fieldsDefinitionType.getFields()
					.get(name);
			if (fieldType != null) {
				return true;
			}
		}
		return false;
	}
	public static boolean isRequired(String name,
			FieldsDefinitionType fieldsDefinitionType) {
		if (fieldsDefinitionType != null) {
			FieldType fieldType = (FieldType) fieldsDefinitionType.getFields()
					.get(name);
			if (fieldType != null) {
				return fieldType.isRequired();
			}
		}
		return false;
	}
	


	public static ConfigureOptionType getOption(String optionValue, List options) {
		Iterator optionsIterator = options.iterator();
		while (optionsIterator.hasNext()){
			ConfigureOptionType configureOptionType = (ConfigureOptionType) optionsIterator.next();
			if (configureOptionType != null && configureOptionType.getValue().equalsIgnoreCase(optionValue)){
				return configureOptionType;
			}
		}
		return null;
	}
	
	

	public static FieldType getFieldType(String name,
			FieldsDefinitionType fieldsDefinitionType) {
		if (fieldsDefinitionType != null) {
			return (FieldType) fieldsDefinitionType.getFields().get(name);
		}
		return null;

	}

	/**
	 * Retrieve the default option of a option list
	 * 
	 * @param name
	 *            name of the option list
	 * @param contextConfiguration
	 * @return value of the default option
	 */
	public static String getDefaultOption(String name,
			ContextType contextConfiguration) {
		if (contextConfiguration.getConfigurationType() != null) {
			OptionsConfigurationType optionsFieldConfiguration = (OptionsConfigurationType) contextConfiguration
					.getConfigurationType().getOptionsConfigurationTypes().get(
							name);
			if (optionsFieldConfiguration != null) {
				return optionsFieldConfiguration.getDefaultOption();
			}
		}
		return null;
	}

	/**
	 * Get label of a option
	 * 
	 * @param name
	 *            name of the optionlist
	 * @param value
	 *            value of a option
	 * @param contextConfiguration
	 * @return Label
	 */
	public static String getLabel(String name, String value,
			ContextType contextConfiguration) {
		if (contextConfiguration.getConfigurationType() != null) {
			OptionsConfigurationType optionsFieldConfiguration = (OptionsConfigurationType) contextConfiguration
					.getConfigurationType().getOptionsConfigurationTypes().get(
							name);
			if (optionsFieldConfiguration != null) {
				Iterator optionsIterator = optionsFieldConfiguration
						.getOptions().iterator();
				while (optionsIterator.hasNext()) {
					ConfigureOptionType optionFieldConfiguration = (ConfigureOptionType) optionsIterator
							.next();
					if (optionFieldConfiguration.getValue() != null
							&& value != null
							&& optionFieldConfiguration.getValue()
									.equals(value)) {
						if (optionFieldConfiguration.getLabel() != null) {
							return optionFieldConfiguration.getLabel();
						}
						return optionFieldConfiguration.getValue();
					}
				}
			}
		}
		return null;
	}

	/**
	 * Retrieve the option fiels
	 * 
	 * @param name
	 *            Name of the option list
	 * @param enteredValues
	 *            Values that are required
	 * @param contextConfiguration
	 * @return
	 */
	public static List getOptionsConfigurationTypes(String name,
			Map enteredValues, ContextType contextConfiguration) {
		List result = new ArrayList();
		if (contextConfiguration.getConfigurationType() != null) {
			OptionsConfigurationType optionsConfigurationType = (OptionsConfigurationType) contextConfiguration
					.getConfigurationType().getOptionsConfigurationTypes().get(
							name);
			if (optionsConfigurationType != null) {
				Iterator optionsIterator = optionsConfigurationType
						.getOptions().iterator();
				while (optionsIterator.hasNext()) {
					ConfigureOptionType optionFieldConfiguration = (ConfigureOptionType) optionsIterator
							.next();
					if (allowedToAddOption(optionFieldConfiguration
							.getDependenciesConfiguration(), enteredValues)) {
						result.add(optionFieldConfiguration);
					}
				}
			}
		}
		return result;
	}

	protected static boolean allowedToAddOption(
			DependenciesType dependenciesConfiguration, Map enteredValues) {
		boolean isOr = false;
		if (dependenciesConfiguration == null) {
			return true;
		}
		if (dependenciesConfiguration.getType().equals(DependenciesType.OR)) {
			isOr = true;
		}
		Iterator keys = dependenciesConfiguration.getItems().keySet()
				.iterator();
		while (keys.hasNext()) {
			String key = (String) keys.next();
			List enteredValuesList = (List) enteredValues.get(key);
			List requiredValues = (List) dependenciesConfiguration.getItems()
					.get(key);
			if (hasRequiredValues(isOr, enteredValuesList, requiredValues)) {
				if (isOr) {
					return true;
				}
			} else {
				if (!isOr) {
					return false;
				}
			}
		}
		Iterator andOrItemsIterator = dependenciesConfiguration.getAndOrItems()
				.iterator();
		while (andOrItemsIterator.hasNext()) {
			DependenciesType andOrConfiguration = (DependenciesType) andOrItemsIterator
					.next();
			if (allowedToAddOption(andOrConfiguration, enteredValues)) {
				if (isOr) {
					return true;
				}
			} else {
				if (!isOr) {
					return false;
				}
			}
		}
		/*
		 * if OR function, no required item is found
		 */
		if (isOr) {
			return false;
		} else {
			/*
			 * if AND function, all required items found
			 */
			return true;
		}

	}

	protected static boolean hasRequiredValues(boolean isOr,
			List enteredValues, List requiredValues) {
		if (enteredValues == null) {
			return false;
		}
		Iterator requiredValuesIterator = requiredValues.iterator();
		while (requiredValuesIterator.hasNext()) {
			String requiredValue = (String) requiredValuesIterator.next();
			Iterator enteredValuesIterator = enteredValues.iterator();
			boolean found = false;
			while (enteredValuesIterator.hasNext() && !found) {
				String enteredValue = (String) enteredValuesIterator.next();
				/*
				 * if required value is entered, set found = true
				 */
				if (enteredValue != null && enteredValue.equals(requiredValue)) {
					found = true;
					/*
					 * if OR function return true;
					 */
					if (isOr) {
						return true;
					}
				}
			}
			/*
			 * if required value is not found and AND function return false
			 */
			if (!found && !isOr) {
				return false;
			}
		}
		/*
		 * if OR function, no required item is found
		 */
		if (isOr) {
			return false;
		} else {
			/*
			 * if AND function, all required items found
			 */
			return true;
		}
	}
}
