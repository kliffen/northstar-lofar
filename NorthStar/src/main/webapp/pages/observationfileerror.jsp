<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>

<tiles:get name="errors" />
<html:form action="/processObservationFileWarning" method="POST">
<TABLE>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD>Some errors occurred while parsing file:</TD>
	</TR>
		<tr>
			<td colspan="2"><span class="error"><astron:errors property="uploadFile" ifnotexist=""/></span>&nbsp;</td>
		</tr>
	<TR>
		<TD align="center">
			<html:cancel tabindex="2">Ok</html:cancel>
		</TD>
	</TR>
</TABLE>

</html:form>