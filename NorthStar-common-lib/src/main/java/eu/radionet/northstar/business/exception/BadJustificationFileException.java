// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.exception;

public class BadJustificationFileException extends NorthStarException {
	private boolean scientificFile = false;

	private boolean technicalDetailsFile = false;

	private boolean figureFile = false;

	private boolean overall = false;

	public BadJustificationFileException(String message,
			boolean scientificFile, boolean technicalDetails,
			boolean figureFile, boolean overall) {
		super(message);
		this.scientificFile = scientificFile;
		this.technicalDetailsFile = technicalDetails;
		this.figureFile = figureFile;
		this.overall = overall;
	}

	public boolean isFigureFile() {
		return figureFile;
	}

	public boolean isScientificFile() {
		return scientificFile;
	}

	public boolean isTechnicalDetailsFile() {
		return technicalDetailsFile;
	}

	public boolean isOverall() {
		return overall;
	}
}
