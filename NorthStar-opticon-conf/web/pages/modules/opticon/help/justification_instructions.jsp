<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<html:html>
<head>
	<link rel=stylesheet href="<%=request.getContextPath()%>/css/proposalLayout.css" type="text/css">
    <title>Instructions Justifications</title>
</head>

<body>

<jsp:include page="../../../layout/box_header_noFullWidth.jsp" /> 
<pre>
The total LENGTH of the text for the justifications together is restricted
to 2 pages (3 for Large Proposals); the balance is left up to the
proposers. For figures and tables there may be 1 optional extra page
(2 for Large Proposals).

The font size should be no smaller than 12 points.

The proposal must be printable on A4 paper.

The uploaded documents will receive header and footer lines to identify
them properly within the proposal. Therefore, the area covered with
text and or figures should be at most:
  width = 595 points
  height = 842 points
Margins:
  left = 56 points
  right = 56 points
  top = 70 points
  bottom = 56 points

Appropriate uploadable ps files have been obtained with LaTeX using the
following commands:
% Example LaTeX commands to generate a NorthStar justification ps file
\documentclass[a4paper, 12pt]{article}
\oddsidemargin=-0.54cm
\evensidemargin=-0.54cm
\topmargin=-1.2cm
\textwidth=17cm
\textheight=25cm
\pagestyle{empty}
\begin{document}
% TEXT OF JUSTIFICATION HERE
\end{document}
This is an example only, and neither the NorthStar team nor the Time
Allocation Committee can take responsibility for correct operation or use
of external software such as LaTeX.

However, NorthStar allows Viewing of the full printable (pdf) version of
the proposal, with any uploaded files are already inserted; this same
format is also used for the review process. The pdf can be retrieved for
printing, but note that pdf viewers may need to be re-configured to print
on A4 paper, without scaling of the input.

Proposal documents which the review committee finds to be deficient,
unreasonably dense, or excessively long may be rejected !

</pre>
<hr/>
- <a href="" onclick="self.close()">close window</a> -

<jsp:include page="../../../layout/box_footer_noFullWidth.jsp" /> 


</body>
</html:html>