// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.business;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import nl.astron.util.XMLBuilder;

import org.w3c.dom.Element;

import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.ProposalXMLConverter;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.AdditionalIssues;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Resource;
import eu.radionet.northstar.data.entities.ResourceType;
import eu.radionet.northstar.data.entities.Target;
import eu.radionet.northstar.data.entities.Thesis;

/**
 * This class converts telescope specific data from the data layer into 
 * XML
 */
public class OpticonProposalXMLConverter extends ProposalXMLConverter {
    public static final String NORTHSTAR_OPTICON_NAMESPACE = "http://www.astro-opticon.org/NorthStar-Opticon";

    /*
     *  (non-Javadoc)
     * @see eu.radionet.northstar.business.ProposalXMLConverter#buildXml(nl.astron.util.XMLBuilder, eu.radionet.northstar.data.entities.Proposal)
     */
    public void buildXml(XMLBuilder xmlBuilder, Proposal proposal)
            throws DatabaseException {
        xmlBuilder.addNamespace("opticon", NORTHSTAR_OPTICON_NAMESPACE,
//              "file:///R:/NorthStar-opticon-lib/datamodel/OpticonProposal.xsd");
                "http://proposal.astron.nl/schemas/OpticonProposal.xsd");
        super.buildXml(xmlBuilder, proposal);
    }

    /*
     *  (non-Javadoc)
     * @see eu.radionet.northstar.business.ProposalXMLConverter#getCurrentNamespace()
     */
    protected String getCurrentNamespace() {
        return NORTHSTAR_OPTICON_NAMESPACE;
    }

    protected void addXmlObservation(Element parent, Observation observation,ObservingRequest observingRequest,
			int index) {
		Element observationElement = xmlBuilder.addIndexedElement(parent,
				getCurrentNamespace(), "observation", index);
		char chr = (char) (index+65);
		xmlBuilder.addTextElement(observationElement,
				"run",  String.valueOf(chr) );
		//addXmlTargets(observationElement, observation, observingRequest);
		xmlBuilder.addTextElement(observationElement,
				"requiredSchedConstraints", observation
						.getRequiredSchedConstraints());
		xmlBuilder.addTextElement(observationElement,
				"preferredSchedConstraints", observation
						.getPreferredSchedConstraints());

		/*
		 * this one must be implemented
		 */
		addXmlSpecificObservation(observationElement, observation,
				"specificObservation");
	}

    /**
     * converts the total time of the observations to XML
     * @see eu.radionet.northstar.business.ProposalXMLConverter#addXmlObservationAllocation(org.w3c.dom.Element, eu.radionet.northstar.data.entities.Observation, java.lang.String)
     */
    protected void addXmlObservationAllocation(Element parent,
            Observation observation, String name) {/*
    	OpticonObservation skeletonObservation = (OpticonObservation) observation;
		Element allocationElement = xmlBuilder.addElement(parent,
				getCurrentNamespace(), name);
		Double totalDuration = NorthStarDelegate.getResourceValue(
				skeletonObservation.getAllocations(), OpticonConstants.TOTAL_DURATION);

		addResource(allocationElement, "observingTime", AstronConverter.
                getHoursFromSeconds(totalDuration));
*/
    }

    /**
     * Converts the requested allocation time to XML
     * @see eu.radionet.northstar.business.ProposalXMLConverter#addXmlObservingRequestAllocation(org.w3c.dom.Element, eu.radionet.northstar.data.entities.ObservingRequest, java.lang.String)
     */
    protected void addXmlObservingRequestAllocation(Element parent,
            ObservingRequest observingRequest, String name) {
    	
    	OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
		Element allocationElement = xmlBuilder.addElement(parent,
				getCurrentNamespace(), name);

		Iterator<?> resourceIterator = opticonObservingRequest.getAllocations().keySet().iterator();
		while (resourceIterator.hasNext()) {
			String resourceName = (String) resourceIterator.next();
			Resource resource = (Resource) opticonObservingRequest.getAllocations().get(resourceName);
			addResource(allocationElement, resourceName, resource.getValue().toString(), resource.getType());
		}
    }

    /**
     * Converts telescope specific target information to XML
     * @see eu.radionet.northstar.business.ProposalXMLConverter#addXmlSpecificTarget(org.w3c.dom.Element, eu.radionet.northstar.data.entities.Target, java.lang.String)
     */
    protected void addXmlSpecificTarget(Element parent,List<?> observations,
            Target target, String name) {

    	OpticonTarget opticonTarget = (OpticonTarget) target;
    	
		FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		
        Element targetElement = xmlBuilder.addElement(parent,
                getCurrentNamespace(), name);
        if (!AstronValidator.isBlankOrNull(opticonTarget.getMoon()))
    		xmlBuilder.addTextElement(targetElement, "moon", opticonTarget.getMoon());
   		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING, fieldsDefinitionType))
		{
   			if (!AstronValidator.isBlankOrNull(opticonTarget.getSeeing()))
   				xmlBuilder.addTextElement(targetElement, "seeing", opticonTarget.getSeeing());
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING_RANGE, fieldsDefinitionType))
		{
			if (opticonTarget.getSeeingLower() != null)
				xmlBuilder.addTextElement(targetElement, "seeinglower", opticonTarget.getSeeingLower());
			if (opticonTarget.getSeeingUpper()!= null)
				xmlBuilder.addTextElement(targetElement, "seeingupper", opticonTarget.getSeeingUpper());
		}   		
        if (!AstronValidator.isBlankOrNull(opticonTarget.getWater()))
    		xmlBuilder.addTextElement(targetElement, "water", opticonTarget.getWater());
        if (!(opticonTarget.getSn() == null))
    		xmlBuilder.addTextElement(targetElement, "sn", opticonTarget.getSn());
        if (!(opticonTarget.getMagnitude() == null))
    		xmlBuilder.addTextElement(targetElement, "magnitude", opticonTarget.getMagnitude());
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX, fieldsDefinitionType))
		{            
	        if (!(opticonTarget.getFlux() == null))
	    		xmlBuilder.addTextElement(targetElement, "flux", opticonTarget.getFlux());
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX_FREQUENCY, fieldsDefinitionType))
		{            
	        if (!(opticonTarget.getFluxFrequency() == null))
	    		xmlBuilder.addTextElement(targetElement, "fluxfrequency", opticonTarget.getFluxFrequency());
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECTRAL_INDEX, fieldsDefinitionType))
		{            
	        if (!(opticonTarget.getSpectralIndex() == null))
	    		xmlBuilder.addTextElement(targetElement, "spectralindex", opticonTarget.getSpectralIndex());
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SKY_QUALITIES, fieldsDefinitionType))
		{
	        if (!AstronValidator.isBlankOrNull(opticonTarget.getSkyQuality()))
	    		xmlBuilder.addTextElement(targetElement, "skyquality", opticonTarget.getSkyQuality());
		}		
		if ((opticonTarget.getDiameter() != null))
    		xmlBuilder.addTextElement(targetElement, "diameter", opticonTarget.getDiameter());
		
   		xmlBuilder.addTextElement(targetElement, "opportunity", opticonTarget.isOpportunity());

   		String runs = fillSelectedRuns(observations, opticonTarget);
   		xmlBuilder.addTextElement(targetElement, "runs", runs);
   		
        if (!AstronValidator.isBlankOrNull(opticonTarget.getComments()))
    		xmlBuilder.addTextElement(targetElement, "comments", opticonTarget.getComments());
    }

    /**
     * Converts telescope specific observation information to XML
     * @see eu.radionet.northstar.business.ProposalXMLConverter#addXmlSpecificObservation(org.w3c.dom.Element, eu.radionet.northstar.data.entities.Observation, java.lang.String)
     */
    protected void addXmlSpecificObservation(Element parent,
            Observation observation, String name) {

    	OpticonObservation opticonObservation = (OpticonObservation) observation;
        Element observationElement = xmlBuilder.addElement(parent,
                getCurrentNamespace(), name);

		xmlBuilder.addTextElement(observationElement,
				"telescope", opticonObservation
						.getTelescopeConfiguration());
		xmlBuilder.addTextElement(observationElement, "moon",  opticonObservation.getObservationPhase());
		
		xmlBuilder.addTextElement(observationElement, "backupStrategy",  opticonObservation.getBackupStrategy());
		if(!AstronValidator.isBlankOrNull(opticonObservation.getCalibrationRequirements() )){
			xmlBuilder.addTextElement(observationElement, "calibrationRequirements",  opticonObservation.getCalibrationRequirements());
		}
		if(!AstronValidator.isBlankOrNull(opticonObservation.getObservationDates() )){
			xmlBuilder.addTextElement(observationElement, "observationDates",  opticonObservation.getObservationDates());
		}
		if(!AstronValidator.isBlankOrNull(opticonObservation.getObservationSeeing())){
			xmlBuilder.addTextElement(observationElement, "observationSeeing",  opticonObservation.getObservationSeeing());
		}
		if(!AstronValidator.isBlankOrNull(opticonObservation.getObservationWeather())){
			xmlBuilder.addTextElement(observationElement, "observationweather",  opticonObservation.getObservationWeather());
		}
		
		Double specifiedTime = NorthStarDelegate.getResourceValue(
				opticonObservation.getAllocations(),opticonObservation.getTelescopeConfiguration()+"_"+ OpticonConstants.REQUESTEDTIME );
		DecimalFormat twoPlaces = new DecimalFormat("0.0");
		List<String> names = new ArrayList<String>();
		List<String> values = new ArrayList<String>();
		names.add("name");
		names.add("unit");
		values.add("time");
		values.add(timeSuffix(opticonObservation));
		
		if(specifiedTime != null){
			String nights = twoPlaces.format(specifiedTime.doubleValue());
			xmlBuilder.addTextElementWithAttributes(observationElement, NORTHSTAR_OPTICON_NAMESPACE,
					"requestedTime", nights, names, values);
		}
		specifiedTime = NorthStarDelegate.getResourceValue(
				opticonObservation.getAllocations(),opticonObservation.getTelescopeConfiguration()+"_"+ OpticonConstants.MINIMUMTIME );
		if(specifiedTime != null){
			String nights = twoPlaces.format(specifiedTime.doubleValue());
			xmlBuilder.addTextElementWithAttributes(observationElement, NORTHSTAR_OPTICON_NAMESPACE,
					"minimumTime", nights, names, values);
		}
		
		if (opticonObservation.getInstrument() != null)
			addXmlInstrument(observationElement,
					opticonObservation.getInstrument(), "instrument");
    }
    
    private String timeSuffix(OpticonObservation observation) {
		// this is a very long routine just to add one little character for one telescope...
		OpticonInstrument instrument = observation.getInstrument();
		if(instrument==null || instrument.getName() ==null){
			return "";
		}
		Map enteredValues = new HashMap();
        enteredValues.put(OpticonConstants.TELESCOPE_CONFIGURATIONS,
                OptionsUtils.getList(observation.getTelescopeConfiguration()));

       
		 List configurations = OptionsDelegate.getOptionsConfigurationTypes(
	                OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredValues,
	                contextConfiguration);
	        ConfigureOptionType configureOptionType = (ConfigureOptionType) OptionsDelegate
	                .getOption(instrument.getName(), configurations);
	    if(configureOptionType == null){
	    	return "";
	    }
	        
		FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) configureOptionType.getSubfields();
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TELESCOPE_HOURS, fieldsDefinitionType)){
            return "hours";
            }else{
            	return "nights";
            }

	}

    /**
     * Converts telescope specific observing request information to XML
     * @see eu.radionet.northstar.business.ProposalXMLConverter#addXmlSpecificObservingRequest(org.w3c.dom.Element, eu.radionet.northstar.data.entities.ObservingRequest, java.lang.String)
     */
    protected void addXmlSpecificObservingRequest(Element parent,
            ObservingRequest observingRequest, String name) {

        OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
        Element observingRequestElement = xmlBuilder.addElement(parent,
                getCurrentNamespace(), name);

        
        Element targetRequestElement = xmlBuilder.addElement(parent,
		"targets");
        List<?> targets= observingRequest.getTargets();
        Iterator<?> tarit = targets.iterator();
        while (tarit.hasNext() ){
        	OpticonTarget target = (OpticonTarget) tarit.next();
        	
        	addXmlTarget(targetRequestElement,observingRequest.getObservations() , target);
        }
        
        if (opticonObservingRequest.getObservingMode()!=null)
        {
        	xmlBuilder.addTextElement(observingRequestElement,
        			"observingMode", opticonObservingRequest.getObservingMode());
        }
        xmlBuilder.addTextElement(observingRequestElement,
				"longTermProposal", opticonObservingRequest.isLongTermProposal());
		if (opticonObservingRequest.isLongTermProposal()){
			xmlBuilder.addTextElement(observingRequestElement,
				"longTermProposalDetails", opticonObservingRequest
						.getLongTermProposalSpecifics());
		}
		xmlBuilder.addTextElement(observingRequestElement,
				"largeProposal", opticonObservingRequest
						.isLargeProposal());
		if (!AstronValidator.isBlankOrNull(opticonObservingRequest.getAllocationJustification()))
		{
			xmlBuilder.addTextElement(observingRequestElement,
					"allocationJustification", opticonObservingRequest.getAllocationJustification());
		}
		
    }
    
    protected void addXmlTarget(Element parent, List<?> observations, Target target) 
	{
		Element targetElement = xmlBuilder.addElement(parent, getCurrentNamespace(), "target");
		xmlBuilder.addTextElement(targetElement, "fieldName", target
				.getFieldName());
		xmlBuilder.addTextElement(targetElement, "ra", target.getRa());
		xmlBuilder.addTextElement(targetElement, "dec", target.getDecl());
		addXmlEpoch(targetElement, target);
		
		/*
		 * this one must be implemented
		 */
		addXmlSpecificTarget(targetElement,observations,target,
				 "specificTarget");
		
		addXmlObservationAllocation(targetElement, target,
		"requestedAllocation");
	}


	protected void addXmlObservationAllocation(Element parent,
			Target target, String name)
	{
//		Element allocationElement = xmlBuilder.addElement(parent,
//				getCurrentNamespace(), name);

		Double totalDuration = NorthStarDelegate.getResourceValue(
				target.getAllocations(),
				OpticonConstants.TOTAL_DURATION);
		String time="0";
		if(totalDuration != null){
			time = AstronConverter.getHoursFromSeconds(totalDuration).toString();
		}
		NorthStarDelegate northStarDelegate;
		ResourceType resourceType = null;
		try {
			northStarDelegate = NorthStarDelegate.getInstance();
		    resourceType = northStarDelegate.getResourceType(OpticonConstants.TIME, OpticonConstants.SECONDS);
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		super.addResource(parent, "observingTime",time , resourceType);
		
	}

	protected void addResource(Element parent, String queue, String value,
			ResourceType resourceType) {
		List<String> names = new ArrayList<String>();
		names.add("queue");
		names.add("name");
		names.add("unit");
		List<String> values = new ArrayList<String>();
		values.add(queue);
		values.add(resourceType.getName());
		values.add(resourceType.getUnit());
		xmlBuilder.addTextElementWithAttributes(parent, NORTHSTAR_OPTICON_NAMESPACE,
				"resource", value, names, values);
	}

	protected void addXmlInstrument(Element parent,
			OpticonInstrument instrument, String name) {
	
        Element instrumentElement = xmlBuilder.addElementWithAttribute(parent,
                name, "name", instrument.getName());

    	if (instrument.getFilters()!= null && !instrument.getFilters().isEmpty())
    	{
    		Element filterElement = xmlBuilder.addElement(instrumentElement, "filters");
    		Iterator<?> filterIt = instrument.getFilters().iterator();
    		while (filterIt.hasNext())
    		{
    			xmlBuilder.addTextElement(filterElement, "filter", (String) filterIt.next());
    		}
    	}

        if (!AstronValidator.isBlankOrNull(instrument.getFilterDetails()))
        		xmlBuilder.addTextElement(instrumentElement, "filterDetails", instrument.getFilterDetails());
        if (!AstronValidator.isBlankOrNull(instrument.getFilterDetails()))
    		xmlBuilder.addTextElement(instrumentElement, "filterDetails", instrument.getFilterDetails());
        if (!AstronValidator.isBlankOrNull(instrument.getReadOut()))
    		xmlBuilder.addTextElement(instrumentElement, "readOut", instrument.getReadOut());
        if (!AstronValidator.isBlankOrNull(instrument.getMode()))
    		xmlBuilder.addTextElement(instrumentElement, "mode", instrument.getMode());
        if (!AstronValidator.isBlankOrNull(instrument.getOrderFilter()))
    		xmlBuilder.addTextElement(instrumentElement, "orderFilter", instrument.getOrderFilter());

        xmlBuilder.addTextElement(instrumentElement, "polarimetry", instrument.isPolarimetry());
        xmlBuilder.addTextElement(instrumentElement, "guideline", instrument.isGuideline());

        if (!AstronValidator.isBlankOrNull(instrument.getWavelength()))
    		xmlBuilder.addTextElement(instrumentElement, "wavelength", instrument.getWavelength());
        if (!(instrument.getOrder() == null))
    		xmlBuilder.addTextElement(instrumentElement, "order", instrument.getOrder());
        if (!AstronValidator.isBlankOrNull(instrument.getSlit()))
    		xmlBuilder.addTextElement(instrumentElement, "slit", instrument.getSlit());
        if (!(instrument.getSlitPositionAngle() == null))
    		xmlBuilder.addTextElement(instrumentElement, "slitPositionAngle", instrument.getSlitPositionAngle());
        if (!AstronValidator.isBlankOrNull(instrument.getGrating()))
    		xmlBuilder.addTextElement(instrumentElement, "grating", instrument.getGrating());
        if (!AstronValidator.isBlankOrNull(instrument.getGrism()))
    		xmlBuilder.addTextElement(instrumentElement, "grism", instrument.getGrism());
        if (!AstronValidator.isBlankOrNull(instrument.getComments()))
    		xmlBuilder.addTextElement(instrumentElement, "comments", instrument.getComments());

   		xmlBuilder.addTextElement(instrumentElement, "nonDefaultFrameExposureTime", instrument.isNonDefaultFrameExposureTime());

        if (!(instrument.getNonDefaultFrameExposureTimeDetails() == null))
    		xmlBuilder.addTextElement(instrumentElement, "nonDefaultFrameExposureTimeDetails", instrument.getNonDefaultFrameExposureTimeDetails());
        if (!(instrument.getFrameExposureTime() == null))
    		xmlBuilder.addTextElement(instrumentElement, "frameExposureTime", instrument.getFrameExposureTime());
        if (!AstronValidator.isBlankOrNull(instrument.getMicrostepping()))
    		xmlBuilder.addTextElement(instrumentElement, "microstepping", instrument.getMicrostepping());

   		xmlBuilder.addTextElement(instrumentElement, "coronagraphicMask", instrument.isCoronagraphicMask());

	}
	
	protected Element addXmlElement(Element root,
			AdditionalIssues additionalIssues) {
		Element additionalIssuesElement = xmlBuilder.addElement(root,
				"additionalIssues");
		if (additionalIssues.isLinkedProposals()) {
			xmlBuilder.addTextElement(additionalIssuesElement,
					"linkedProposals", additionalIssues
							.getLinkedProposalsSpecifics());
		}		
		if (additionalIssues.isLinkedProposalsElsewhere()) {
			xmlBuilder.addTextElement(additionalIssuesElement,
					"linkedProposalsElsewhere", additionalIssues
							.getLinkedProposalsElsewhereSpecifics());
		}		
		if (additionalIssues.isPreviousAllocations()) {
			xmlBuilder.addTextElement(additionalIssuesElement,
					"previousAllocations", additionalIssues
							.getPreviousAllocationsSpecifics());
		}
		if (additionalIssues.getTheses().size() > 0) {
			Element thesesElement = xmlBuilder.addElement(
					additionalIssuesElement, "theses");
			for (int i = 0; i < additionalIssues.getTheses().size(); i++) {

				Thesis thesis = (Thesis) additionalIssues.getTheses().get(i);
				addXmlElement(thesesElement, thesis, i);

			}
		}
		xmlBuilder.addTextElement(additionalIssuesElement, "additionalRemarks",
				additionalIssues.getAdditionalRemarks());
		return additionalIssuesElement;

	}
	
	private String fillSelectedRuns(List<?> observations,
			OpticonTarget opticonTarget) {
		String result="";
		if (observations == null){
			return "";
		}
		for(int i=0;i<observations.size();i++){
			OpticonObservation obs = (OpticonObservation) observations.get(i);
			List<?> obstargets= obs.getTargets();
			if(obstargets != null){ // new unstored observation
				Iterator<?> obsit = obstargets.iterator();
				while(obsit.hasNext() ){
					OpticonTarget obsTarget = (OpticonTarget) obsit.next();
					if(obsTarget.equals(opticonTarget)){
						char chr = (char) (i+65);
						result += String.valueOf(chr)+", " ;
					}
				}
			}
		}
		if(result.length() >2){
			result = result.substring(0, result.length()-2);
		}
		return result;
	}
	/*
	 * if (!AstronValidator.isBlankOrNull(opticonObservingRequest.getGrantNumber()))
		{
			xmlBuilder.addTextElement(observingRequestElement,
					"grantNumber", opticonObservingRequest.getGrantNumber());
		}
		if (!AstronValidator.isBlankOrNull(opticonObservingRequest.getTravel()))
		{
			xmlBuilder.addTextElement(observingRequestElement,
					"travel", opticonObservingRequest.getTravel());
		}
		if (!AstronValidator.isBlankOrNull(opticonObservingRequest.getOtherExpenditure()))
		{
			xmlBuilder.addTextElement(observingRequestElement,
					"otherExpenditure", opticonObservingRequest.getOtherExpenditure());
		}
		if (opticonObservingRequest.isOpticonFunding())
		{
			xmlBuilder.addTextElement(observingRequestElement,
					"opticonFunding", "yes");
		}

	 * 
	 */

	protected void addXmlSpecificTarget(Element parent, Target target,
			String name) {
		// TODO Auto-generated method stub
		
	}
}
