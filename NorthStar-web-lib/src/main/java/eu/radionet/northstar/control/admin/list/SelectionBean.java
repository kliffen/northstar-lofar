// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.list;

import java.io.Serializable;

public class SelectionBean implements Serializable {
	protected String[] selectedCategories = null;
	protected String[] selectedSemesters = null;
	protected String[] selectedStatusses = null;
	protected String[] selectedTelescopes = null;
	protected String[] selectedCommunities = null;
	protected String selectedYear = null;
	protected boolean forceProjectId = false;
	
	public String[] getSelectedCategories() {
		return selectedCategories;
	}
	public void setSelectedCategories(String[] selectedCategories) {
		this.selectedCategories = selectedCategories;
	}
	public String[] getSelectedSemesters() {
		return selectedSemesters;
	}
	public void setSelectedSemesters(String[] selectedSemesters) {
		this.selectedSemesters = selectedSemesters;
	}
	public String[] getSelectedStatusses() {
		return selectedStatusses;
	}
	public void setSelectedStatusses(String[] selectedStatusses) {
		this.selectedStatusses = selectedStatusses;
	}
	public String[] getSelectedTelescopes() {
		return selectedTelescopes;
	}
	public void setSelectedTelescopes(String[] selectedTelescopes) {
		this.selectedTelescopes = selectedTelescopes;
	}
	public String[] getSelectedCommunities() {
		return selectedCommunities;
	}
	public void setSelectedCommunities(String[] selectedCommunities) {
		this.selectedCommunities = selectedCommunities;
	}
	public String getSelectedYear() {
		return selectedYear;
	}
	public void setSelectedYear(String year) {
		this.selectedYear = year;
	}
	public boolean isForceProjectId() {
		return forceProjectId;
	}
	public void setForceProjectId(boolean forceProjectId) {
		this.forceProjectId = forceProjectId;
	}
}
