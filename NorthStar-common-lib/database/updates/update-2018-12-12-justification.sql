ALTER TABLE lofar_proposal_astron_nl_northstar.justification
ADD COLUMN ltarawstorage TINYINT(1)  NOT NULL DEFAULT 0 ,
ADD COLUMN ltarawstoragereason TEXT  DEFAULT NULL ;