# NorthStar Lofar configuration

## Bulding

## Settings up dev environment

Dependencies:
* Java 8 (although `pom.xml` specify `1.6`)
* Maven (confirmed to work with `3.6.3`)

Add the following to `~/.m2/settings.xml`
```xml
<settings>
  <mirrors>
    <mirror>
      <id>astron-nexus</id>
      <name>ASTRON Nexus</name>
      <url>https://support.astron.nl/nexus/content/groups/public/</url>
      <mirrorOf>*</mirrorOf>
    </mirror>
  </mirrors>
 
  <profiles>
    <profile>
      <id>astron</id>
      <repositories>
        <repository>
          <id>astron-nexus-snapshots</id>
          <name>Astron Nexus Snapshots Snapshots</name>
          <releases>
            <enabled>false</enabled>
          </releases>
          <snapshots>
            <enabled>true</enabled>           
          </snapshots>
          <url>https://support.astron.nl/nexus/content/repositories/snapshots/</url>
          <layout>default</layout>
        </repository>
        <repository>
          <id>astron-nexus-releases</id>
          <name>Astron Nexus Snapshots Releases</name>
          <releases>
            <enabled>true</enabled>
          </releases>
          <snapshots>
            <enabled>false</enabled>
          </snapshots>
          <url>https://support.astron.nl/nexus/content/repositories/releases/</url>
          <layout>default</layout>
        </repository>
      </repositories>
    </profile>
  </profiles>
 
  <activeProfiles>
    <activeProfile>astron</activeProfile>
  </activeProfiles>
   
</settings>
```

### Building NorthStar

Run the following command to build the package.
```bash
JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64/ mvn -U clean package
```
The JAVA_HOME variable may be omitted if Maven is using java 8 by default.
Alternatively, configure your IDE to use Java 8 and Maven.



