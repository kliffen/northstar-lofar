// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronValidator;
import nl.astron.util.XMLBuilder;

import org.w3c.dom.Element;

import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.ProposalXMLConverter;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Resource;
import eu.radionet.northstar.data.entities.ResourceType;
import eu.radionet.northstar.data.entities.Target;

/**
 * This class converts telescope specific data from the data layer into 
 * XML
 */
	
public class LofarProposalXMLConverter extends ProposalXMLConverter {
    public static final String NORTHSTAR_OPTICON_NAMESPACE = "http://www.astron.nl/NorthStar-Lofar";
    public static final String INTERFERO = "Interfero";
    
    /*
     *  (non-Javadoc)
     * @see eu.radionet.northstar.business.ProposalXMLConverter#buildXml(nl.astron.util.XMLBuilder, eu.radionet.northstar.data.entities.Proposal)
     */
    public void buildXml(XMLBuilder xmlBuilder, Proposal proposal)
            throws DatabaseException {
        xmlBuilder.addNamespace("lofar", NORTHSTAR_OPTICON_NAMESPACE,
//              "file:///R:/NorthStar-opticon-lib/datamodel/OpticonProposal.xsd");
                "http://proposal.astron.nl/schemas/LofarProposal.xsd");
        super.buildXml(xmlBuilder, proposal);
    }

    /*
     *  (non-Javadoc)
     * @see eu.radionet.northstar.business.ProposalXMLConverter#getCurrentNamespace()
     */
    protected String getCurrentNamespace() {
        return NORTHSTAR_OPTICON_NAMESPACE;
    }

    /**
     * converts the total time of the observations to XML
     * @see eu.radionet.northstar.business.ProposalXMLConverter#addXmlObservationAllocation(org.w3c.dom.Element, eu.radionet.northstar.data.entities.Observation, java.lang.String)
     */
    protected void addXmlObservationAllocation(Element parent,
            Observation observation, String name) {/*
    	OpticonObservation skeletonObservation = (OpticonObservation) observation;
		Element allocationElement = xmlBuilder.addElement(parent,
				getCurrentNamespace(), name);
		Double totalDuration = NorthStarDelegate.getResourceValue(
				skeletonObservation.getAllocations(), OpticonConstants.TOTAL_DURATION);

		addResource(allocationElement, "observingTime", AstronConverter.
                getHoursFromSeconds(totalDuration));
*/
    }

    /**
     * Converts the requested allocation time to XML
     * @see eu.radionet.northstar.business.ProposalXMLConverter#addXmlObservingRequestAllocation(org.w3c.dom.Element, eu.radionet.northstar.data.entities.ObservingRequest, java.lang.String)
     */
    protected void addXmlObservingRequestAllocation(Element parent,
            ObservingRequest observingRequest, String name) {
    	
    	OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
		
    	if(opticonObservingRequest.getAllocations().size()>0){
    		Element allocationElement = xmlBuilder.addElement(parent,
				getCurrentNamespace(), "requestedAllocation");
    	
			Iterator resourceIterator = opticonObservingRequest.getAllocations().keySet().iterator();
			while (resourceIterator.hasNext()) {
				String resourceName = (String) resourceIterator.next();
				Resource resource = (Resource) opticonObservingRequest.getAllocations().get(resourceName);
				addResource(allocationElement, resourceName, resource.getValue().toString(), resource.getType());
			}
    	}
		
    }

    /**
     * Converts telescope specific target information to XML
     * @see eu.radionet.northstar.business.ProposalXMLConverter#addXmlSpecificTarget(org.w3c.dom.Element, eu.radionet.northstar.data.entities.Target, java.lang.String)
     */
    protected void addXmlSpecificTarget(Element parent,
            Target target, String name) {

    	OpticonTarget opticonTarget = (OpticonTarget) target;
    	
		FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		
        Element targetElement = xmlBuilder.addElement(parent,
                getCurrentNamespace(), name);
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX, fieldsDefinitionType))
		{            
	        if (!(opticonTarget.getFlux() == null))
	    		xmlBuilder.addTextElement(targetElement, "flux", opticonTarget.getFlux());
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX_FREQUENCY, fieldsDefinitionType))
		{            
	        if (!(opticonTarget.getFluxFrequency() == null))
	    		xmlBuilder.addTextElement(targetElement, "fluxfrequency", opticonTarget.getFluxFrequency());
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECTRAL_INDEX, fieldsDefinitionType))
		{            
	        if (!(opticonTarget.getSpectralIndex() == null))
	    		xmlBuilder.addTextElement(targetElement, "spectralindex", opticonTarget.getSpectralIndex());
		}
        if (!AstronValidator.isBlankOrNull(opticonTarget.getComments()))
    		xmlBuilder.addTextElement(targetElement, "comments", opticonTarget.getComments());
        
        
        Element allocationElement = xmlBuilder.addElement(targetElement,
				getCurrentNamespace(), "requestedAllocation");
        Iterator resourceIterator = target.getAllocations().keySet().iterator();
		while (resourceIterator.hasNext()) {
			String resourceName = (String) resourceIterator.next();
			Resource resource = (Resource) target.getAllocations().get(resourceName);
			//super.addResource(allocationElement, "observingTime", resource.getValue().toString(), resource.getType());
			addResource(allocationElement, null, resource.getValue().toString(), resource.getType());
		}
        
    }

    /**
     * Converts telescope specific observation information to XML
     * @see eu.radionet.northstar.business.ProposalXMLConverter#addXmlSpecificObservation(org.w3c.dom.Element, eu.radionet.northstar.data.entities.Observation, java.lang.String)
     */
    protected void addXmlSpecificObservation(Element parent,
            Observation observation, String name) {

    	OpticonObservation opticonObservation = (OpticonObservation) observation;
        Element observationElement = xmlBuilder.addElement(parent,
                getCurrentNamespace(), name);

		xmlBuilder.addTextElement(observationElement,
				"telescope", opticonObservation
						.getTelescopeConfiguration());
		if (opticonObservation.getInstrument() != null)
			addXmlInstrument(observationElement,
					opticonObservation.getInstrument(), "instrument");
    }

    /**
     * Converts telescope specific observing request information to XML
     * @see eu.radionet.northstar.business.ProposalXMLConverter#addXmlSpecificObservingRequest(org.w3c.dom.Element, eu.radionet.northstar.data.entities.ObservingRequest, java.lang.String)
     */
    protected void addXmlSpecificObservingRequest(Element parent,
            ObservingRequest observingRequest, String name) {

		FieldsDefinitionType fieldsDefinitionType = 
				(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
			
        OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
        Element observingRequestElement = xmlBuilder.addElement(parent,
                getCurrentNamespace(), name);

        if (OptionsDelegate.allowedToDisplay(OpticonConstants.NIGHT_TIME, fieldsDefinitionType)) {
            xmlBuilder.addTextElement(observingRequestElement, "nightTime", opticonObservingRequest.isNightTime());
        }

        if (OptionsDelegate.allowedToDisplay(OpticonConstants.NOISE_LEVEL, fieldsDefinitionType)) {
        	if (opticonObservingRequest.getNoiseLevel() != null) {
                xmlBuilder.addTextElement(observingRequestElement, "noiseLevel", opticonObservingRequest.getNoiseLevel());
        	}
        }

        xmlBuilder.addTextElement(observingRequestElement, "proprietaryException", opticonObservingRequest.isProprietaryException());
        
       	if (!AstronValidator.isBlankOrNull(opticonObservingRequest.getProprietaryExceptionSpecifics()))
		{
			xmlBuilder.addTextElement(observingRequestElement,
					"proprietaryExceptionSpecifics", opticonObservingRequest.getProprietaryExceptionSpecifics() );
		}
		
    }

	protected void addXmlObservationAllocation(Element parent,
			Target target, String name)
	{
//		Element allocationElement = xmlBuilder.addElement(parent,
//				getCurrentNamespace(), name);

		
	}
	
	protected void addResource(Element parent, String queue, String value,
			ResourceType resourceType) {
		List names = new ArrayList();
		if(queue != null){
			names.add("queue");
		}
		names.add("name");
		names.add("unit");
		List values = new ArrayList();
		if(queue!=null){
			values.add(queue);
		}
		values.add(resourceType.getName());
		values.add(resourceType.getUnit());
		xmlBuilder.addTextElementWithAttributes(parent, NORTHSTAR_OPTICON_NAMESPACE,
				"resource", value, names, values);
	}

	protected void addXmlInstrument(Element parent,
			OpticonInstrument instrument, String name) {
		List attrNames = new ArrayList();
		List attrMhz= new ArrayList();
		attrNames.add("unit");
    	attrMhz.add("MHz");
    	
        Element instrumentElement = xmlBuilder.addElementWithAttribute(parent,
                name, "name", instrument.getName());
        if (!AstronValidator.isBlankOrNull(instrument.getClock() )){
    		xmlBuilder.addTextElement(instrumentElement, "clock", instrument.getClock());
        }
        if (!AstronValidator.isBlankOrNull(instrument.getOneFilter())){
    		xmlBuilder.addTextElement(instrumentElement, "filter", instrument.getOneFilter());
        }
        if (instrument.getFilterFrequency() != null && instrument.getFilterFrequency().intValue() > 0){
        	xmlBuilder.addTextElementWithAttributes( instrumentElement,
        											"",
        											"filterFrequency",
        											instrument.getFilterFrequency().toString(),
        											attrNames,
        											attrMhz);
        }
        if (instrument.getFilterBandwidth() != null && instrument.getFilterBandwidth().intValue() > 0){
    		//xmlBuilder.addTextElement(instrumentElement, "filterBandwidth", instrument.getFilterBandwidth().toString());
    		
    		xmlBuilder.addTextElementWithAttributes( instrumentElement,
					"",
					"filterBandwidth",
					instrument.getFilterBandwidth().toString(),
					attrNames,
					attrMhz);
        }
        if (!AstronValidator.isBlankOrNull(instrument.getFilterDetails() )){
    		xmlBuilder.addTextElement(instrumentElement, "filterContiguousCoverage", instrument.getFilterDetails().toString());
        }
        if (!AstronValidator.isBlankOrNull(instrument.getAntenna())){
        	
        	String antenna = stripAmount(instrument.getAntenna());
        	
    		xmlBuilder.addTextElement(instrumentElement, "antenna",antenna );
        }
       
        
        
        
    	xmlBuilder.addTextElement(instrumentElement, "piggyBack", instrument.isPiggyBack());
    	if (!AstronValidator.isBlankOrNull(instrument.getPiggyBackSpecifics()) && instrument.isPiggyBack()){
    		xmlBuilder.addTextElement(instrumentElement, "piggyBackSpecifics", instrument.getPiggyBackSpecifics());
        }
        
        // Post processing :
        
    	//processing mode
    	
    	xmlBuilder.addTextElement(instrumentElement, "beamConf", instrument.isBeamConf());
    	xmlBuilder.addTextElement(instrumentElement, "interferoConf", instrument.isInterferoConf());
    	xmlBuilder.addTextElement(instrumentElement, "tbbConf", instrument.isTbbConf());
    	xmlBuilder.addTextElement(instrumentElement, "otherConf", instrument.isOtherConf());
    	
        // only interferometer (UV calculation)  has averaging
        if(instrument.getName().contains("Interfero")){
	       	
        }
        
        if(instrument.getName().contains("TBB"))
        {
        	if (instrument.getTbbRequestedMinimumTime()!=null)
        	xmlBuilder.addTextElement(instrumentElement, "tbbRequestedMinimumTime", instrument.getTbbRequestedMinimumTime());
        	if(instrument.getTbbRequestedSATime()!=null)
        	xmlBuilder.addTextElement(instrumentElement, "tbbRequestedSATime", instrument.getTbbRequestedSATime());
        	if(instrument.getTbbTriggerLength()!=null)
        	xmlBuilder.addTextElement(instrumentElement, "tbbTriggerLength", instrument.getTbbTriggerLength());
        	if(instrument.getTbbTriggerRate()!=null)
        	xmlBuilder.addTextElement(instrumentElement, "tbbTriggerRate", instrument.getTbbTriggerRate());
        	if(!AstronValidator.isBlankOrNull(instrument.getTbbSelectedObservationId()))
        	xmlBuilder.addTextElement(instrumentElement, "tbbSelectedObservationId", instrument.getTbbSelectedObservationId());
        	if(!AstronValidator.isBlankOrNull(instrument.getTbbTriggerSourceId()))
        	xmlBuilder.addTextElement(instrumentElement, "tbbTriggerSourceId", instrument.getTbbTriggerSourceId());
        	if(instrument.getTbbExposureTime()!=null)
        	xmlBuilder.addTextElement(instrumentElement, "tbbExposureTime", instrument.getTbbExposureTime());
        }
        
        xmlBuilder.addTextElement(instrumentElement, "flagging", instrument.isFlagging());
        xmlBuilder.addTextElement(instrumentElement, "calibration", instrument.isCalibration());

        
        
        
        
        addXMLInstrumentSpecifics(instrumentElement,instrument);
   		

	}

	private String stripAmount(String antenna) {
		if(antenna == null){
			return null;
		}
		int end = antenna.indexOf(" (");
		if(end < 1){
			return antenna;
		}
		return antenna.substring(0, end);
	}

	private void addXMLInstrumentSpecifics(Element parent,
			OpticonInstrument instrument) {
		List attrNames = new ArrayList();
		List attrMhz= new ArrayList();
		attrNames.add("unit");
    	attrMhz.add("MHz");
    	List attrKhz= new ArrayList();
    	attrKhz.add("KHz");
    	List attrSeconds = new ArrayList();
    	attrKhz.add("seconds");

        Map enteredValues = new HashMap();
        enteredValues.put(OpticonConstants.TELESCOPE_CONFIGURATIONS,
                OptionsUtils.getList(instrument.getObservation().getTelescopeConfiguration()));
        List configurations = OptionsDelegate.getOptionsConfigurationTypes(
                OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredValues,
                contextConfiguration);
        ConfigureOptionType configureOptionType = (ConfigureOptionType) OptionsDelegate
                .getOption(instrument.getName(), configurations);
		FieldsDefinitionType fieldsDefinitionType = null;
        if (configureOptionType != null
                && configureOptionType.getSubfields() != null) {
            fieldsDefinitionType = (FieldsDefinitionType) configureOptionType
                    .getSubfields();
        }

		if(instrument.getName().equalsIgnoreCase("Tied Array")){
			xmlBuilder.addTypeAttributeToElement(parent, "lofar:TiedArrayInstrumentType");
			if (!AstronValidator.isBlankOrNull(instrument.getStation())){
				String station = stripAmount(instrument.getStation());
	        	xmlBuilder.addTextElement(parent, "stations", station);
	    	}
			if (!AstronValidator.isBlankOrNull(instrument.getCustomStationSpecifics() )){
	    		xmlBuilder.addTextElement(parent, "stationSpecifics", instrument.getCustomStationSpecifics());
	        }
			if (instrument.getBeams() != null && instrument.getBeams().intValue() > 0){
	    		xmlBuilder.addTextElement(parent, "beams", instrument.getBeams().toString());
	        }
			 if (instrument.getSamplerate() != null && instrument.getSamplerate().intValue() > 0){
		    		//xmlBuilder.addTextElement(instrumentElement, "sampleRate", instrument.getSamplerate().toString());
		    		xmlBuilder.addTextElementWithAttributes(parent,
															"",
															"sampleRate",
															instrument.getSamplerate().toString(),
															attrNames,
															attrMhz);
		        }
	       
		}
		else if(instrument.getName().contains("Beam Observation")){
			xmlBuilder.addTypeAttributeToElement(parent, "lofar:BeamInstrumentType");
			/*
			 		<xsd:element name="stations" type="StationSelectionType" minOccurs="0"></xsd:element>
					<xsd:element name="stationSpecifics" type="xsd:string" minOccurs="0"></xsd:element>
					<xsd:element name="beams" type="xsd:int" minOccurs="0"></xsd:element>
					<xsd:element name="sampleRate" type="FrequencyType" minOccurs="0"></xsd:element>
					<xsd:element name="cohstokes" type="xsd:boolean" minOccurs="0"></xsd:element>
					<xsd:element name="polarization" type="PolarizationSelectionType" minOccurs="0"></xsd:element>
					<xsd:element name="incohstokes" type="xsd:boolean" minOccurs="0"></xsd:element>
					<xsd:element name="incpolarization" type="PolarizationSelectionType" minOccurs="0"></xsd:element>
					<xsd:element name="channels" type="xsd:int" minOccurs="0"></xsd:element>
					<xsd:element name="intsteps" type="xsd:int" minOccurs="0"></xsd:element>
					<xsd:element name="complexvoltage" type="xsd:boolean" minOccurs="0"></xsd:element>
					<xsd:element name="flyseye" type="xsd:boolean" minOccurs="0"></xsd:element>
					<xsd:element name="rawvoltage" type="xsd:boolean" minOccurs="0"></xsd:element>
			 */
			if (!AstronValidator.isBlankOrNull(instrument.getStation())){
				String station = stripAmount(instrument.getStation());
	        	xmlBuilder.addTextElement(parent, "stations", station);
	    	}
			if (!AstronValidator.isBlankOrNull(instrument.getCustomStationSpecifics() )){
	    		xmlBuilder.addTextElement(parent, "stationSpecifics", instrument.getCustomStationSpecifics());
	        }
			if (!(instrument.getSubarrayPointings() == null)) {
				xmlBuilder.addTextElement(parent, "subarrayPointings", instrument.getSubarrayPointings());
			}
			if (fieldsDefinitionType != null && OptionsDelegate.allowedToDisplay(OpticonConstants.INSTRUMENT_STORE_RAW_DATA, fieldsDefinitionType)) {
				xmlBuilder.addTextElement(parent, "storeRawData", instrument.isStoreRawData());
			}
			
			if (instrument.getBeams() != null && instrument.getBeams().intValue() > 0){
	    		xmlBuilder.addTextElement(parent, "beams", instrument.getBeams().toString());
	        }
			if (instrument.getSamplerate() != null && instrument.getSamplerate().intValue() > 0){
		    		//xmlBuilder.addTextElement(instrumentElement, "sampleRate", instrument.getSamplerate().toString());
		    		xmlBuilder.addTextElementWithAttributes(parent,
															"",
															"sampleRate",
															instrument.getSamplerate().toString(),
															attrNames,
															attrMhz);
		    }
			xmlBuilder.addTextElement(parent, "cohstokes", instrument.isCohstokes());
	        if(instrument.isCohstokes() && instrument.getPolarization() != null){
	        	xmlBuilder.addTextElement(parent, "polarization", instrument.getPolarization().toString());
	        }
	        xmlBuilder.addTextElement(parent, "incohstokes", instrument.isIncohstokes() );
	        if(instrument.isIncohstokes() && instrument.getIncPolarization() != null){
	        	xmlBuilder.addTextElement(parent, "incpolarization", instrument.getIncPolarization().toString());
	        }
	        if(instrument.getChannels() != null && instrument.getChannels().intValue() > 0){
	        	xmlBuilder.addTextElement(parent, "channels", instrument.getChannels().toString());
	        }
	        if(instrument.getIntsteps() != null && instrument.getIntsteps().intValue() > 0){
	        	xmlBuilder.addTextElement(parent, "intsteps", instrument.getIntsteps().toString());
	        }
	        if (fieldsDefinitionType != null && OptionsDelegate.allowedToDisplay(OpticonConstants.INSTRUMENT_STORE_CORRELATED_VISIBILITIES, fieldsDefinitionType)) {
				xmlBuilder.addTextElement(parent, "storeUVData", instrument.isStoreUVData());
			}
			xmlBuilder.addTextElement(parent, "complexvoltage", instrument.isComplexVoltage());
			xmlBuilder.addTextElement(parent, "flyseye", instrument.isFlysEye());
			xmlBuilder.addTextElement(parent, "rawvoltage", instrument.isRawVoltage());
			
		}
		
		else if(instrument.getName().contains("Interferometer")){
			xmlBuilder.addTypeAttributeToElement(parent, "lofar:InterferoInstrumentType");
			if (!AstronValidator.isBlankOrNull(instrument.getStation())){
				String station = stripAmount(instrument.getStation());
	        	xmlBuilder.addTextElement(parent, "stations", station);
	    	}
			if (!AstronValidator.isBlankOrNull(instrument.getCustomStationSpecifics() )){
	    		xmlBuilder.addTextElement(parent, "stationSpecifics", instrument.getCustomStationSpecifics());
	        }
			if (!(instrument.getIntegrationTime() == null)){
	    		//xmlBuilder.addTextElement(instrumentElement, "integrationTime", instrument.getIntegrationTime());
	        	xmlBuilder.addTextElementWithAttributes(parent,
						"",
						"integrationTime",
						instrument.getIntegrationTime().toString(),
						attrNames,
						attrSeconds);
	        }
			if (!(instrument.getSubarrayPointings() == null)) {
				xmlBuilder.addTextElement(parent, "subarrayPointings", instrument.getSubarrayPointings());
			}
			if (!(instrument.getObservation().getNoiseLevel() == null)) {
				xmlBuilder.addTextElement(parent, "noiseLevel", instrument.getObservation().getNoiseLevel());
			}
			if (fieldsDefinitionType != null && OptionsDelegate.allowedToDisplay(OpticonConstants.INSTRUMENT_STORE_RAW_DATA, fieldsDefinitionType)) {
				xmlBuilder.addTextElement(parent, "storeRawData", instrument.isStoreRawData());
			}
					
			if (instrument.getPixelSizeX() != null && instrument.getPixelSizeX().intValue() > 0){
	    		xmlBuilder.addTextElement(parent, "pixelSizeX", instrument.getPixelSizeX().toString());
	        }
	        if (instrument.getPixelSizeY() != null && instrument.getPixelSizeY().intValue() >0 ){
	    		xmlBuilder.addTextElement(parent, "pixelSizeY", instrument.getPixelSizeY().toString());
	        }
	        
	        xmlBuilder.addTextElement(parent, "averaging", instrument.isAveraging());
	       	if(instrument.isAveraging()){
		       	if (instrument.getAveragingTime() != null && instrument.getAveragingTime().intValue() > 0){
		    		//xmlBuilder.addTextElement(parent, "averagingTime", instrument.getAveragingTime().toString());
		       		xmlBuilder.addTextElementWithAttributes( parent,
					"",
					"averagingTime",
					instrument.getAveragingTime().toString(),
					attrNames,
					attrSeconds);
		    		
		        }
		        if (instrument.getAveragingFrequency() != null && instrument.getAveragingFrequency().intValue() > 0){
			   		xmlBuilder.addTextElementWithAttributes( parent,
					"",
					"averagingFrequency",
					instrument.getAveragingFrequency().toString(),
					attrNames,
					attrKhz);
		        }
	       	}
		}
		else if(instrument.getName().equalsIgnoreCase("TBB (standalone)")){
			xmlBuilder.addTypeAttributeToElement(parent, "lofar:TransientBufferBoardInstrumentType");
			if (!AstronValidator.isBlankOrNull(instrument.getStation())){
				String station = stripAmount(instrument.getStation());
	        	xmlBuilder.addTextElement(parent, "stations", station);
	    	}
			if (!AstronValidator.isBlankOrNull(instrument.getCustomStationSpecifics() )){
	    		xmlBuilder.addTextElement(parent, "stationSpecifics", instrument.getCustomStationSpecifics());
	        }
			if (instrument.getEvents() != null && instrument.getEvents().intValue() > 0){
	    		xmlBuilder.addTextElement(parent, "events", instrument.getEvents().toString());
	        }
		}
		else if(instrument.getName().equalsIgnoreCase("TBB (piggyback)")){
			xmlBuilder.addTypeAttributeToElement(parent, "lofar:TransientBufferBoardPiggyInstrumentType");
			if (!AstronValidator.isBlankOrNull(instrument.getStation())){
				String station = stripAmount(instrument.getStation());
	        	xmlBuilder.addTextElement(parent, "stations", station);
	    	}
			if (!AstronValidator.isBlankOrNull(instrument.getCustomStationSpecifics() )){
	    		xmlBuilder.addTextElement(parent, "stationSpecifics", instrument.getCustomStationSpecifics());
	        }
			if (instrument.getEvents() != null && instrument.getEvents().intValue() > 0){
	    		xmlBuilder.addTextElement(parent, "events", instrument.getEvents().toString());
	        }
		}
		else if(instrument.getName().equalsIgnoreCase("Transient Buffer Board")){
			xmlBuilder.addTypeAttributeToElement(parent, "lofar:TransientBufferBoardInstrumentType");
			if (!AstronValidator.isBlankOrNull(instrument.getStation())){
				String station = stripAmount(instrument.getStation());
	        	xmlBuilder.addTextElement(parent, "stations", station);
	    	}
			if (!AstronValidator.isBlankOrNull(instrument.getCustomStationSpecifics() )){
	    		xmlBuilder.addTextElement(parent, "stationSpecifics", instrument.getCustomStationSpecifics());
	        }
			if (instrument.getEvents() != null && instrument.getEvents().intValue() > 0){
	    		xmlBuilder.addTextElement(parent, "events", instrument.getEvents().toString());
	        }
		}
		
		else if(instrument.getName().equalsIgnoreCase("Direct Data Storage")){
			xmlBuilder.addTypeAttributeToElement(parent, "lofar:DirectDataStorageInstrumentType");
			if (!AstronValidator.isBlankOrNull(instrument.getStation())){
				String station = stripAmount(instrument.getStation());
	        	xmlBuilder.addTextElement(parent, "stations", station);
	    	}
			if (!AstronValidator.isBlankOrNull(instrument.getCustomStationSpecifics() )){
	    		xmlBuilder.addTextElement(parent, "stationSpecifics", instrument.getCustomStationSpecifics());
	        }
			if (instrument.getBeams() != null && instrument.getBeams().intValue() > 0){
	    		xmlBuilder.addTextElement(parent, "beams", instrument.getBeams().toString());
	        }
			 if (instrument.getSamplerate() != null && instrument.getSamplerate().intValue() > 0){
		    		//xmlBuilder.addTextElement(instrumentElement, "sampleRate", instrument.getSamplerate().toString());
		    		xmlBuilder.addTextElementWithAttributes(parent,
															"",
															"sampleRate",
															instrument.getSamplerate().toString(),
															attrNames,
															attrMhz);
		        }
	       
		}
		
		else if(instrument.getName().equalsIgnoreCase("Non Standard")){
			xmlBuilder.addTypeAttributeToElement(parent, "lofar:NonStandardInstrumentType");
			if (!AstronValidator.isBlankOrNull(instrument.getComments())){
				xmlBuilder.addTextElement(parent, "specifics", instrument.getComments());
			}
		}
		
	}
}
