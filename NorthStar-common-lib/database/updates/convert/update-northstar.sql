ALTER TABLE semester ADD COLUMN semester VARCHAR(255) NOT NULL;
ALTER TABLE semester ADD COLUMN community VARCHAR(255) NOT NULL;
ALTER TABLE semester ADD COLUMN closed tinyint(1) NOT NULL default '0';
ALTER TABLE proposal ADD COLUMN semesterid int(11) NOT NULL;
ALTER TABLE proposal ADD COLUMN version VARCHAR(20) NOT NULL;
ALTER TABLE additionalissues ADD COLUMN data_analyse text default NULL;
ALTER TABLE semester MODIFY COLUMN telescope VARCHAR(20) NOT NULL;
ALTER TABLE observingrequest MODIFY COLUMN observingrequesttype VARCHAR(20) NOT NULL;
ALTER TABLE observation MODIFY COLUMN observationtype VARCHAR(20) NOT NULL;

ALTER TABLE justification ADD COLUMN `scientificfilepages` int(11) default NULL;
ALTER TABLE justification ADD COLUMN `technicaldetailsfilepages` int(11) default NULL;
ALTER TABLE justification ADD COLUMN `figurefilepages` int(11) default NULL;

ALTER TABLE semester ADD KEY `category_IND` (`categoryid`);
ALTER TABLE proposal ADD KEY `semester_IND` (`semesterid`);

#Set communities
UPDATE semester SET community='DUTCH'
WHERE telescope = 'WHT' OR telescope = 'INT' OR telescope = 'JCMT';
 
UPDATE semester SET community='ALL'
WHERE telescope = 'WSRT';

#Set semester
UPDATE semester SET semester = SUBSTRING(prefix,2,3);

#Assign proposal to semester
UPDATE proposal as prop, semester as sem , observingrequest as obs SET prop.semesterid=sem.id
WHERE obs.proposalid = prop.id AND obs.categoryid = sem.categoryid
AND prop.code like CONCAT(sem.prefix,'%');

#semester id must be 06B
UPDATE observingrequest o, proposal p SET p.semesterid = 20
WHERE o.observingrequesttype = 'WHT' AND p.id=o.proposalid AND p.semesterid = 0 ;

#semester id must be 06B
UPDATE observingrequest o, proposal p SET p.semesterid = 21
WHERE o.observingrequesttype = 'INT' AND p.id=o.proposalid AND p.semesterid = 0 ;

#semester id must be 06B
UPDATE observingrequest o, proposal p SET p.semesterid = 18
WHERE o.observingrequesttype = 'JCMT' AND p.id=o.proposalid AND p.semesterid = 0 ;

#semester id must be 06B
UPDATE observingrequest o, proposal p SET p.semesterid = 22
WHERE o.observingrequesttype = 'WSRT' AND p.id=o.proposalid AND p.semesterid = 0  AND o.categoryid=1;

#semester id must be 05B
UPDATE observingrequest o, proposal p SET p.semesterid = 5
WHERE o.observingrequesttype = 'WSRT' AND p.id=o.proposalid AND p.semesterid = 0  AND o.categoryid=2;

#semester id must be 05B
UPDATE observingrequest o, proposal p SET p.semesterid = 8
WHERE o.observingrequesttype = 'WSRT' AND p.id=o.proposalid AND p.semesterid = 0 AND o.categoryid=3;

UPDATE proposal p, semester s SET p.version = 'wht_1.0'
WHERE s.id=p.semesterid  AND s.telescope='WHT';
UPDATE proposal p, semester s SET p.version = 'int_1.0'
WHERE s.id=p.semesterid  AND s.telescope='INT';
UPDATE proposal p, semester s SET p.version = 'jcmt_1.0'
WHERE s.id=p.semesterid  AND s.telescope='JCMT';
UPDATE proposal p, semester s SET p.version = 'wsrt_1.0'
WHERE s.id=p.semesterid  AND s.telescope='WSRT';

UPDATE simplelinebackend s SET s.freqspecificationmode = 'skyFrequency'
WHERE  s.freqspecificationmode ='skyFrequencies';
UPDATE simplelinebackend s SET s.freqspecificationmode = 'restFrequency'
WHERE  s.freqspecificationmode ='velocities';

