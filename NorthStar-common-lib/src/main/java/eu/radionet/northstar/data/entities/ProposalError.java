// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProposalError.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: ProposalError.java,v 1.1 2006-05-02 10:01:10 verhoef Exp $
 *
 */
package eu.radionet.northstar.data.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * The ProposalError provides
 *
 * @author Bastiaan Verhoef
 *
 */
public class ProposalError implements Serializable {
    private String propertyName = null;
    private String description = null;
    private List errors = new ArrayList();

    public ProposalError(String propertyName, String description){
        this.propertyName = propertyName;
        this.description = description;
    }
    

    public String getDescription() {
        return description;
    }

    /**
     * @return Returns the errors.
     */
    public List getErrors() {
        return errors;
    }
    /**
     * @param errors The errors to set.
     */
    public void setErrors(List properties) {
        this.errors = properties;
    }
    public boolean isEmpty(){
        return errors.isEmpty();

    }
    public ProposalError getProposalError(String propertyName){
        ProposalError proposalError = null;
        for (int i = 0; i < errors.size();i++){
            proposalError = (ProposalError) errors.get(i);
            if (proposalError.getPropertyName().equals(propertyName)){
                return proposalError;
            }
            
        }
        return null;
    }

    /**
     * @return Returns the propertyName.
     */
    public String getPropertyName() {
        return propertyName;
    }
    /**
     * @param propertyName The propertyName to set.
     */
    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }
    /**
     * @param description The description to set.
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
