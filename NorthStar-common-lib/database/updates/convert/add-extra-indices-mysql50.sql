USE northstar;

CREATE INDEX type_observation ON observation (observationtype(10));
CREATE INDEX type_observingrequest ON observingrequest (observingrequesttype(10));