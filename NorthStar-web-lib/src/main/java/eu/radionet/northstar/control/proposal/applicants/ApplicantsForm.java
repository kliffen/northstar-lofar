// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.applicants;

import java.util.ArrayList;
import java.util.List;

import eu.radionet.northstar.control.proposal.ProposalForm;

public class ApplicantsForm extends ProposalForm {

    protected List memberBeans = null;
    protected String deleteMemberButton = null;
    protected String inviteMemberButton = null;	
    protected String reInviteMemberButton = null;		
    protected String moveUpMemberButton = null;
    protected String moveDownMemberButton = null;	
    protected String editMemberButton = null;

    protected String addMemberButton = null;
    protected int selectedMemberId = 0;
    protected String[] potentialObservers = null;

    protected String pi = null;

    protected String contactAuthor = null;
    protected boolean validData = true;
    
    protected String addApplicantsButton=null;
    protected String selectedProposal = null;
	protected List poposalListforApplicants = new ArrayList();

    /**
     * @return Returns the addMemberButton.
     */
    public String getAddMemberButton() {
        return addMemberButton;
    }

    /**
     * @param addMemberButton
     *            The addMemberButton to set.
     */
    public void setAddMemberButton(String addApplicantButton) {
        this.addMemberButton = addApplicantButton;
    }

    /**
     * @return Returns the correspondingAuthor.
     */
    public String getContactAuthor() {
        return contactAuthor;
    }

    /**
     * @param correspondingAuthor
     *            The correspondingAuthor to set.
     */
    public void setContactAuthor(String correspondingAuthor) {
        this.contactAuthor = correspondingAuthor;
    }

    /**
     * @return Returns the pi.
     */
    public String getPi() {
        return pi;
    }

    /**
     * @param pi
     *            The pi to set.
     */
    public void setPi(String pi) {
        this.pi = pi;
    }

    /**
     * @return Returns the potentialObservers.
     */
    public String[] getPotentialObservers() {
        return potentialObservers;
    }

    /**
     * @param potentialObservers
     *            The potentialObservers to set.
     */
    public void setPotentialObservers(String[] potentialObservers) {
        this.potentialObservers = potentialObservers;
    }

    /**
     * @return Returns the memberBeans.
     */
    public List getMemberBeans() {
        return memberBeans;
    }

    public void setDeleteMemberButton(int index, String value) {
        selectedMemberId = index;
        deleteMemberButton = value;
    }

    public String getDeleteMemberButton(int index) {
        return deleteMemberButton;
    }

    public void setMoveUpMemberButton(int index, String value) {
        selectedMemberId = index;
		moveUpMemberButton = value;
    }

    public String getMoveUpMemberButton(int index) {
        return moveUpMemberButton;
    }

    public void setMoveDownMemberButton(int index, String value) {
        selectedMemberId = index;
		moveDownMemberButton = value;
    }

    public String getMoveDownMemberButton(int index) {
        return moveDownMemberButton;
    }

    /**
     * @return Returns the selectedMemberId.
     */
    public int getSelectedMemberId() {
        return selectedMemberId;
    }

    public void setEditMemberButton(int index, String value) {
        selectedMemberId = index;
        editMemberButton = value;
    }

    public String getEditMemberButton(int index) {
        return editMemberButton;
    }


	public void setMemberBeans(List memberBeans) {
		this.memberBeans = memberBeans;
	}

	public String getInviteMemberButton(int index) {
		return inviteMemberButton;
	}

	public String getReInviteMemberButton(int index) {
		return reInviteMemberButton;
	}
    public void setInviteMemberButton(int index, String value) {
        selectedMemberId = index;
		inviteMemberButton = value;
    }
    public void setReInviteMemberButton(int index, String value) {
        selectedMemberId = index;
		reInviteMemberButton = value;
    }

	public boolean isValidData() {
		return validData;
	}

	public void setValidData(boolean invalidData) {
		this.validData = invalidData;
	}

	public String getAddApplicantsButton() {
		return addApplicantsButton;
	}

	public void setAddApplicantsButton(String addApplicantsButton) {
		this.addApplicantsButton = addApplicantsButton;
	}

	public String getSelectedProposal() {
		return selectedProposal;
	}

	public void setSelectedProposal(String selectedProposal) {
		this.selectedProposal = selectedProposal;
	}

	public List getPoposalListforApplicants() {
		return poposalListforApplicants;
	}

	public void setPoposalListforApplicants(List poposalListforApplicants) {
		this.poposalListforApplicants = poposalListforApplicants;
	}
	
	
}
