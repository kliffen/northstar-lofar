<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />
<!-- This jsp is used when you want to define a target or observation  -->
<html:form action="/processObservation" method="POST">
	<!-- The top part of the observation page it sets a help button in the top right corner -->	
	<table width="100%">
		<TR>
			<TD width="*">&nbsp;</TD>
			<TD class="help" width="60">
				<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
				<TABLE width="60" height="25">
					<TR>
			  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#opticonobservation\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
		  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#opticonobservation\" target=\"help\">Help</a>"%></TD>
	  				</TR>
	  			</TABLE>
			<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
			</TD>
		</TR>
	</table>

	<table align="center">
<tr>
	<td colspan="2">&nbsp;</td>
</tr>


<tr>
	<td class="header" colspan="2">Targets :</td>
</tr>

<tr>
	<td colspan="2"><tiles:get name="box_header_no_top" />
	<table width="100%">
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	<!-- If there are target/observations defined show some target properties in a tabel  -->
	<logic:notEmpty name="observationForm" property="targetSummaryValues">
	
	<TR>
		<TD colspan="2">
			<table class="projecttable"  align="center">
			<!-- write the column headers j should contain the number of target properties that are displayed. The total columns of this
				table should be j+3 (the proporties plus three columns containing buttons -->
			<% int j = 0; %>
				<TR>
					<logic:iterate id="targetSummaryLabel" name="observationForm" property="targetSummaryLabels">
						<TD  class="tableheader">&nbsp;<bean:write name="targetSummaryLabel" />&nbsp;</TD>
	 		<% j++; %>
					</logic:iterate>
					<!--  the last three burtton columns do not have a header -->
					<td class="tableheader" colspan="3">&nbsp;</td>
				</TR>
			<!-- 
				the following two variables are used to change the color of the table rows. Even rows have a different color than
				odd rows
			 -->
			<%
			 int i=0;
			 String styleClass = "";
			%>				
				<!--  Iterate though the defined targets. One target a row. -->
	 			<logic:iterate id="targetBean" name="observationForm" property="targetSummaryValues">
	 			<%
	 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
	 			%>
 	 			<TR>
 	 			<!-- iterate all the target properties and display them. (1st element has a different style) -->
 	 			<logic:iterate id="valueBean" indexId="valueBeanId" name="targetBean">
 	 				<logic:equal name="valueBean" property="display" value="true">
	 	 			<logic:equal name="valueBeanId" value="0">
						<TD class="tablefield_pri<%=styleClass%>">
						<!-- highlight target that is being edited -->
						<logic:equal name="observationForm" property="selectedTargetId" value="<%=""+(i-1)%>">
							<b><i>
								</logic:equal>
									<bean:write name="valueBean" property = "value"/>
								<logic:equal name="observationForm" property="selectedTargetId" value="<%=""+(i-1)%>">
							</i></b>
						</logic:equal>
						</TD>
	 	 			</logic:equal>
	 	 			<logic:notEqual name="valueBeanId" value="0">
	 	 				<TD class="tablefield<%=styleClass%>">
						<logic:equal name="observationForm" property="selectedTargetId" value="<%=""+(i-1)%>">
							<b><i>
						</logic:equal>
	 	 					<bean:write name="valueBean" property = "value"/>
						<logic:equal name="observationForm" property="selectedTargetId" value="<%=""+(i-1)%>">
							</i></b>
						</logic:equal>
	 	 				</TD>
	 	 			</logic:notEqual>
					</logic:equal>
 	 			</logic:iterate>
 	 			<!--  Display the edit buton -->
 	 			
				<TD class="tablefield<%=styleClass%>">
				
				<html:submit disabled="false"  indexed="true" property="editTargetButton" styleClass="list_edit" title="Edit this target/observation" onclick="setNorthStarCookie(getScrollCoordinates());">Edit</html:submit>  
				</TD>					
				<!--  when the maximum limit of target has not been reached show the copy button -->
				<logic:equal name="observationForm" property="maxNumberOfTargetsReached" value="true">
					<TD class="tablefield<%=styleClass%>"><html:submit disabled="true" indexed="true" property="copyTargetButton" styleClass="list_copy" title="Copy this target/observation" onclick="setNorthStarCookie(getScrollCoordinates());">Copy</html:submit></TD>
				</logic:equal>
				<logic:equal name="observationForm" property="maxNumberOfTargetsReached" value="false">
					<TD class="tablefield<%=styleClass%>"><html:submit disabled="false" indexed="true" property="copyTargetButton" styleClass="list_copy" title="Copy this target/observation" onclick="setNorthStarCookie(getScrollCoordinates());">Copy</html:submit></TD>
				</logic:equal>
				<!--  Display the delete button -->
				<TD class="tablefield<%=styleClass%>"><html:submit disabled="false" indexed="true" property="deleteTargetButton" styleClass="list_delete" title="Delete target/observation, no prompt!" onclick="setNorthStarCookie(getScrollCoordinates());">Delete</html:submit></TD>

	 			</TR>
	 			</logic:iterate>
	 			<tr><td colspan="<%=j+3 %>" class="spacer">&nbsp;</td></tr>	 				 			
			</table>
		</TD>
	</TR>
	<!-- If the maximum limit of target/observation has been reached display a message -->
	<tr>
		<td colspan="2" class="italic" align="center" >&nbsp;
		<logic:equal name="observationForm" property="maxNumberOfTargetsReached" value="true">
			The maximum number of targets is reached
		</logic:equal>
		</td>
	</tr>
	</logic:notEmpty>	
	<logic:empty name="observationForm" property="targetSummaryValues">
	<tr>
		<td colspan="2" class="bold" align="center" >&nbsp;
			List of targets is empty
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;
		</td>
	</tr>
	
	</logic:empty>
<tr>
	<logic:equal name="observationForm" property="selectedTargetId" value="-1">
		<td class="header" colspan="2">New Target :</td>
	</logic:equal>
	<logic:notEqual name="observationForm" property="selectedTargetId" value="-1">
	 	<html:hidden name="observationForm" property="targetId" />
		<td class="header" colspan="2">Edit Target :</td>
	</logic:notEqual>
</tr>		
 
<tr>
	<td colspan="2"><tiles:get name="box_header_no_top" />
	<table width="100%">
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<!--  
			Here you the fields for the target can be found: The name including a button to SIMBAD, the RA, dec, and Epoch	
		 -->
		<tr>
			<td class="right"><astron:label key="label.opticon.observation.fieldname" /></td> 
			<td class="left">
				<html:text property="fieldName" size="16" maxlength="255"/><span class="error">*</span>
				<html:submit property="resolveButton" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()"><astron:label key="label.opticon.observation.getsimbad" /></html:submit>&nbsp;<logic:messagesNotPresent property="fieldName"><astron:label key="label.opticon.observation.proposercheck" /></logic:messagesNotPresent><span class="error"><astron:errors property="fieldName"/></span>
			</td>
		</tr>
		<tr>
			<td class="right"><astron:label key="label.opticon.observation.rightascension" /></td>
			<td class="left">
				<html:text property="ra" size="11"/>
				<span class="courier">&nbsp;hh:mm[:ss.ss]&nbsp;</span>
				<span class="error">
					<!--  when the epoch is "Other System" the RA field is not mandatory. So no red star needs to be displayed -->
					<logic:equal name="observationForm" property="requiredTargetRa" value="false">
					   <astron:errors property="ra" ifnotexist=""/>
					</logic:equal>
					<logic:equal name="observationForm" property="epoch" value="Other system">
						<astron:errors property="ra" ifnotexist=""/>
					</logic:equal>
					<logic:equal name="observationForm" property="requiredTargetRa" value="true">
						<logic:notEqual name="observationForm" property="epoch" value="Other system">
							<astron:errors property="ra" ifnotexist="*"/>
						</logic:notEqual>
					</logic:equal>
				</span>
			</td>
		</tr>
		<tr>
			<td class="right"><astron:label key="label.opticon.observation.declination" /></td>
			<td class="left">
				<html:text property="dec" size="11" />
				<span class="courier">&nbsp;[+|-]dd:mm[:ss.s]&nbsp;</span>
				<span class="error">
					<!--  when the epoch is "Other System" the dec field is not mandatory. So no red star needs to be displayed -->
					<logic:equal name="observationForm" property="requiredTargetDec" value="false">
					    <astron:errors property="dec" ifnotexist=""/>
					</logic:equal>
					<logic:equal name="observationForm" property="epoch" value="Other system">
						<astron:errors property="dec" ifnotexist=""/>
					</logic:equal>
					<logic:equal name="observationForm" property="requiredTargetDec" value="true">
						<logic:notEqual name="observationForm" property="epoch" value="Other system">
							<astron:errors property="dec" ifnotexist="*"/>
						</logic:notEqual>
					</logic:equal>
				</span>
			</td>
		</tr>
		<tr>
			<td class="right"><astron:label key="label.opticon.observation.epoch" /></td>
			<td>
				<!--  
					A drop down box is implemented here. This is a way how to do it. But we strongly recommend to
					use the XML option system. Discussed in the opticonobservationgeneral.jsp	
				 -->
				<html:select property="epoch">
  					<html:option value="J2000"/>
  					<html:option value="B1950"/>
  					<html:option value="Other system"/>
				</html:select>
			</td>
		</tr>

		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
<logic:equal name="observationForm" property="displayTargetExposureTime" value="true">
	<tr>
		<td class="right"><astron:label key="label.opticon.observation.exposuretime" /></td>
		<td class="left"><html:text property="targetExposureTime" size="15" />
			<span class="error"><astron:errors property="totalDuration" ifnotexist="*" /></span>
			<astron:label key="label.opticon.observation.exposuretime.comment" />
		</td>
	</tr>
</logic:equal>
<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<logic:equal name="observationForm" property="displayTargetMoon" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.moon" /></td>
	<td class="left">
 		<logic:iterate id="moonOption" name="observationForm" property="moonOptions">
        	<html:radio property="selectedTargetMoon" idName="moonOption" value="value">
                 <bean:write name="moonOption" property="label"/>
         	</html:radio>
		 </logic:iterate>
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displaySeeingOptions" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.seeing" /></td>
	<td class="left">
 		<logic:iterate id="seeingOption" name="observationForm" property="seeingOptions">
        	<html:radio property="selectedTargetSeeing" idName="seeingOption" value="value">
                 <bean:write name="seeingOption" property="label"/>
         	</html:radio>
		 </logic:iterate>
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displaySeeingRange" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.seeing.specifics" /></td>
	<td class="left"><astron:label key="label.opticon.observation.lower" />
		<html:text property="targetSeeingLower" size="7"/>
		<astron:label key="label.opticon.observation.upper" />
		<html:text property="targetSeeingUpper" size="7"/>
		<logic:equal name="observationForm" property="requiredSeeingRange" value="true">
			<span class="error"><astron:errors property="targetSeeingUpper" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredSeeingRange" value="false">
			<span class="error"><astron:errors property="targetSeeingUpper" ifnotexist="" /></span>
		</logic:equal>				
	</td>	
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetWater" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.water" /></td>
	<td class="left">
 		<logic:iterate id="waterOption" name="observationForm" property="waterOptions">
        	<html:radio property="selectedTargetWater" idName="waterOption" value="value">
                 <bean:write name="waterOption" property="label"/>
         	</html:radio>
		 </logic:iterate>
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetSn" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.sn" /></td> 
	<td class="left">
		<html:text property="selectedTargetSn" size="16"/>
		<logic:equal name="observationForm" property="requiredSn" value="true">
			<span class="error"><astron:errors property="selectedTargetSn" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredSn" value="false">
			<span class="error"><astron:errors property="selectedTargetSn" ifnotexist="" /></span>
		</logic:equal>			
		<astron:label key="label.opticon.observation.sn.comment" />
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetMagnitude" value="true">
<tr>
	<td class="right">
		<astron:label key="label.opticon.observation.magnitude" />
	</td> 
	<td class="left">
		<html:text property="selectedTargetMagnitude" size="16"/><span class="error"><astron:errors property="selectedTargetMagnitude" ifnotexist="*"/></span>
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetFlux" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.flux" /></td> 
	<td class="left">
		<html:text property="targetFlux" size="16"/>
		<logic:equal name="observationForm" property="requiredTargetFlux" value="true">
			<span class="error"><astron:errors property="targetFlux" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetFlux" value="false">
			<span class="error"><astron:errors property="targetFlux" ifnotexist="" /></span>
		</logic:equal>			
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetSkyQuality" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.sky" /></td> 
	<td class="left">
		<bean:define id="skyQualities" name="observationForm" property="skyQualities" /> 
		<html:select property="selectedTargetSkyQuality">
			<html:options collection="skyQualities" property="value" labelProperty="label" />
		</html:select>
		<logic:equal name="observationForm" property="requiredTargetSkyQuality" value="true">
			<span class="error"><astron:errors property="targetSkyQuality" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetSkyQuality" value="false">
			<span class="error"><astron:errors property="targetSkyQuality" ifnotexist="" /></span>
		</logic:equal>			
	</td>
	
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetDiameter" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.diameter" /></td> 
	<td class="left">
		<html:text property="targetDiameter" size="16"/>
		<logic:equal name="observationForm" property="requiredTargetDiameter" value="true">
			<span class="error"><astron:errors property="targetDiameter" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetDiameter" value="false">
			<span class="error"><astron:errors property="targetDiameter" ifnotexist="" /></span>
		</logic:equal>	
		<astron:label key="label.opticon.observation.diameter.comment" />			
	</td>
	
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetStorage" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.datastorage" /></td> 
	<td class="left">
		<html:text property="targetStorage" size="16"/>
		<logic:equal name="observationForm" property="requiredTargetStorage" value="true">
			<span class="error"><astron:errors property="targetStorage" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetStorage" value="false">
			<span class="error"><astron:errors property="targetStorage" ifnotexist="" /></span>
		</logic:equal>				
	</td>
	
</tr>
</logic:equal>
<tr>
	<td class="right"><astron:label key="label.opticon.observation.comments" /></td> 
	<td class="left">
		<html:text property="selectedTargetComments" size="16"  maxlength="255"/>
	</td>
</tr>
<logic:equal name="observationForm" property="displayTargetOpportunity" value="true">
<tr>
	<td class="right" valign="top"><astron:label key="label.opticon.observation.opportunity" /></td>
	<td class="left"><html:radio styleClass="radio" name="observationForm" property="targetOpportunity" value="true" >Yes</html:radio>
					<html:radio styleClass="radio" name="observationForm" property="targetOpportunity" value="false" >No</html:radio></td>
</tr>							
</logic:equal>	
<tr>
	<td colspan="2">&nbsp;</td>
</tr>

<logic:equal name="targetsOnTabbedPage" value="true">
   <tr>
		<td class="right"><astron:label key="label.opticon.observation.runs" /></td> 
		<td class="left">
				<logic:iterate id="runItem" name="observationForm" property="observingRuns">
						<html:multibox property="selectedRuns" >
								<bean:write name="runItem" property="label" />
						</html:multibox>
						<bean:write name="runItem" property="value"/>
				</logic:iterate>
		</td>
	</tr>
	
  <tr>
	  <td colspan="2">&nbsp;</td>
  </tr>
</logic:equal>	
<logic:equal name="observationForm" property="autoCommit" value="false">
	<TR>
		<TD colspan="2" align="center">
		<logic:equal name="observationForm" property="maxNumberOfTargetsReached" value="true">
			<html:submit disabled="true"  property="newTargetButton" styleClass="list_newobs" title="New Target"  onclick="setNorthStarCookie(getScrollCoordinates())">Commit to list of targets</html:submit>&nbsp;
			<html:submit disabled="false"  property="clearTargetButton" styleClass="list_clearobs" title="Clear Target"  onclick="setNorthStarCookie(getScrollCoordinates())">Clear target form</html:submit>&nbsp;
             <html:submit disabled="true"  property="newTargetListButton" styleClass="list_uploadlist" title="Upload target list"  onclick="setNorthStarCookie(getScrollCoordinates())">Upload a target list</html:submit>    
		</logic:equal>
		<logic:equal name="observationForm" property="maxNumberOfTargetsReached" value="false">
			<html:submit disabled="false"  property="newTargetButton" styleClass="list_newobs" title="New Target"  onclick="setNorthStarCookie(getScrollCoordinates())">Commit to list of targets</html:submit>&nbsp;
			<html:submit disabled="false"  property="clearTargetButton" styleClass="list_clearobs" title="Clear Target"  onclick="setNorthStarCookie(getScrollCoordinates())">Clear target form</html:submit>&nbsp;
         <html:submit disabled="false"  property="newTargetListButton" styleClass="list_uploadlist" title="Upload target list"  onclick="setNorthStarCookie(getScrollCoordinates())">Upload a target list</html:submit>   
		</logic:equal>	
		</TD>
	</TR>	
</logic:equal>
					</table>
					<tiles:get name="box_footer" />			
				</td>
			</tr>		
					</table>
					<tiles:get name="box_footer" />			
				</td>
			</tr>	
			
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>			
<!-- HERE THE INSTRUMENT PAGE IS INCLUDED -->
	
	<logic:equal name="observationForm" property="showInstrumentBean" value="true">
	    <html:hidden name="observationForm" property="index" />
		<jsp:include page="instrument.jsp" />
					
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		
	</logic:equal> 
				
<!-- 

	BEGIN COMMON CODE
	
 -->
</table>

	<logic:equal name="targetsOnTabbedPage" value="true">
		<jsp:include page="/pages/include/storebuttons.jsp"/>
	</logic:equal>

<!-- Cancel and Ok button -->
<logic:notEqual name="targetsOnTabbedPage" value="true">
	<table width="100%">
		<tr>			
			<td class="left"><html:submit property="saveButton" title="Ok" styleClass="list_accept_yes">Ok</html:submit></td>
				<td class="right"><html:cancel styleClass="list_decline_t" title="Cancel target specification">Cancel</html:cancel></td>
		</tr>		
	</table>
</logic:notEqual>
</html:form>


<!-- 

	END COMMON CODE
	
 -->
