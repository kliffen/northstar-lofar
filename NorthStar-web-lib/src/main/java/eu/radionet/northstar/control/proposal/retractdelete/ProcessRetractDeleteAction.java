// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessRetractDeleteAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: ProcessRetractDeleteAction.java,v 1.2 2008-04-08 08:02:59 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.retractdelete;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;

/**
 * The ProcessRetractDeleteAction provides
 * 
 * @author Nico Vermaas
 *  
 */
public class ProcessRetractDeleteAction extends LockedAction {
    private Log log = LogFactory.getLog(this.getClass());


    public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        ActionMessages errors = new ActionMessages();
        RetractDeleteForm retractDeleteForm = (RetractDeleteForm) form;

        /*
         * retrieve own_useraccount info from session
         */
        UserAccount ownUserAccount = (UserAccount) session
                .getAttribute(Constants.OWN_USERACCOUNT);


        /*
         * if pressed on cancel
         */
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
        if (!isCancelled(request)) {

            if (retractDeleteForm.getRetractButton() != null) {
                try {
                    proposalDelegate.retractAndStore(proposalData
                            .getProposal(), ownUserAccount);
                } catch (DatabaseException de) {
                    log.error("Cannot retract and store proposal: "
                            + de.getMessage());
                    errors.add(Constants.ERROR, new ActionMessage(
                            "error.database.retract.proposal"));
                    saveErrors(request, errors);
                    return mapping.getInputForward();
                }
            } else if (retractDeleteForm.getDeleteButton() != null) {
                try {
                    proposalDelegate.delete(proposalData.getProposal(),session.getId(),ownUserAccount);
                } catch (DatabaseException de) {
                    log.error("Cannot delete proposal: " + de.getMessage());
                    errors.add(Constants.ERROR, new ActionMessage(
                            "error.database.delete.proposal"));
                    saveErrors(request, errors);
                    return mapping.getInputForward();
                }
                session.removeAttribute(Constants.PROPOSAL_DATA);
            }

            return mapping.findForward("success");
        }
        else {
			removeLock(request,ownUserAccount);
            return mapping.findForward("cancel");
        }

    }
}