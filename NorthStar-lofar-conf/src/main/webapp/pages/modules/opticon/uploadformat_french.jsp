Format reminder:<br/>
<pre class="courier">
Fieldname RA Dec Epoch [Exposure_time [Moon [Seeing [Water [S/N [Magnitude] [Comments]]]]]] 
</pre>
<p> An example:
<pre class="courier">
m35 06:45:59.93 -20:45:15.1 j2000 7200s dark   <0.5 dry 
m31 00:48:31.20 +12:09:00.0 j2000 11m   bright  0.8 
m32 00:42:41.87 +40:51:57.2 j2000
</pre>
</p>
<p>
Values you can use:
<ul>
	<li><i>Moon:</i> "dark", "firstq", "bright", "lastq"</li>
	<li><i>Seeing:</i> a number, "<0.5", "0.5-0.9", ">0.9"  (No spaces are allowed with the seeing field!)</li>
	<li><i>Water:</i> "dry", "average", "wet"</li>
</ul>
</p>
