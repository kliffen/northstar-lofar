<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>

<html:form action="/processObservationFileWarning" method="POST">
<TABLE>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<logic:equal name="warningForm" property="invalidTemplateObservation" value="true">
	<TR>
		<TD colspan="2">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img module="" page="/images/list/alert.gif"/></TD>
				<TD><PRE>
				
<b>WARNING:</b> Not all required template fields are filled!!
				</PRE>
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>	
	<TR>
		<TD>&nbsp;</TD>
	</TR>	
	</logic:equal>	

	<logic:equal name="warningForm" property="exceedMaxAllowedObservations" value="true">
	<TR>
		<TD colspan="2">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img module="" page="/images/list/alert.gif"/></TD>
				<TD><PRE>
				
<b>WARNING:</b> Maximum allowed observations exceed!! 
If you proceed, observations will be parsed,
until maximum allowed observations are reached!!	
				</PRE>
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>		
	</logic:equal>				
	<TR>
		<TD align="center">
			<html:submit property="saveButton" onclick="" tabindex="1">Proceed</html:submit>
			<html:cancel tabindex="2">Cancel</html:cancel>
		</TD>
	</TR>
</TABLE>

</html:form>