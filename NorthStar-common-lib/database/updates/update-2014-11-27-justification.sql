ALTER TABLE lofar_proposal_astron_nl_northstar.justification
ADD COLUMN combineddataproductrequest TINYINT(1)  NOT NULL DEFAULT 0 ,
ADD COLUMN combineddataproductrequestreason TEXT  DEFAULT NULL;
