<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processDeadLinePassed" method="GET">
<TABLE>
	
	<TR>
		<TD>
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				<TD valign='top'><PRE>
The deadline of your proposal has passed!!
Consider creating a new proposal for the next semester, when available.

</PRE>
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>	
	<TR>
		<TD class="header">The deadline of this semester is passed:</TD>
	</TR>
	<TR>
		<TD>
		<tiles:get name="box_header_no_top" />
			<table class="center" width="100%">
				<TR>
					<TD class="right">Semester:</TD>
					<TD class="bold" align="left"><bean:write name="deadLinePassedForm" property="currentDeadLine.semester"/></TD>
				</TR>
				<TR>
					<TD class="right">Community:</TD>
					<TD class="bold" align="left"><bean:write name="deadLinePassedForm" property="currentDeadLine.community"/></TD>
				</TR>
				<TR>
					<TD class="right">Telescope:</TD>
					<TD class="bold" align="left"><bean:write name="deadLinePassedForm" property="currentDeadLine.telescope"/></TD>
				</TR>
				<TR>
					<TD class="right">Category:</TD>
					<TD class="bold" align="left"><bean:write name="deadLinePassedForm" property="currentDeadLine.category"/></TD>
				</TR>
				<TR>
					<TD class="right">Deadline:</TD>
					<TD class="bold" align="left"><bean:write name="deadLinePassedForm" property="currentDeadLine.deadLine"/></TD>
				</TR>				
			</table>
		<tiles:get name="box_footer" />
		</TD>
	</TR>
	<logic:equal name="deadLinePassedForm" property="nextDeadLineNotExists" value="false">
	<TR>
		<TD>&nbsp;</TD>
	</TR>	
	<TR>
		<TD class="header">The deadline of next semester:</TD>
	</TR>
	<TR>
		<TD>
		<tiles:get name="box_header_no_top" />
			<table class="center" width="100%">
				<TR>
					<TD class="right">Semester:</TD>
					<TD class="bold" align="left"><bean:write name="deadLinePassedForm" property="nextDeadLine.semester"/></TD>
				</TR>
				<TR>
					<TD class="right">Community:</TD>
					<TD class="bold" align="left"><bean:write name="deadLinePassedForm" property="nextDeadLine.community"/></TD>
				</TR>
				<TR>
					<TD class="right">Telescope:</TD>
					<TD class="bold" align="left"><bean:write name="deadLinePassedForm" property="nextDeadLine.telescope"/></TD>
				</TR>
				<TR>
					<TD class="right">Category:</TD>
					<TD class="bold" align="left"><bean:write name="deadLinePassedForm" property="nextDeadLine.category"/></TD>
				</TR>
				<TR>
					<TD class="right">Deadline:</TD>
					<TD class="bold" align="left"><bean:write name="deadLinePassedForm" property="nextDeadLine.deadLine"/></TD>
				</TR>				
			</table>
		<tiles:get name="box_footer" />
		</TD>
	</TR>	
	</logic:equal>
	<TR>
		<TD>&nbsp;</TD>
	</TR>	
	</TABLE>
<TABLE width="100%">
	<TR>
<!-- Copying between semesters unreliable, no longer supported 
		<TD align="left"><html:submit property="submitButton" styleClass="list_accept_yes" title="Ok, add to next semester (if available)">Yes</html:submit></TD>
		<TD class="right"><html:cancel styleClass="list_decline_t" title="Do not add to next semester">No</html:cancel>
		</TD>
 -->		
		<TD class="center"><html:cancel styleClass="list_accept_yes" title="Return to proposal list">Ok</html:cancel>
		</TD>
	</TR>
</TABLE>
</html:form>
