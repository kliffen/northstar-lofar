<SUBJECT>Your %committee% technical review assignment</SUBJECT>
Dear %reviewerFirstName%,

As I hope you already know, you have been nominated as a technical 
referee for OPTICON trans-national proposals at your facility.  Could 
you please provide a brief technical report on the following proposals 
in the current cycle. You are invited to mark them as feasible or 
not, and to add any general comments or advice to the panel in the text 
box. 

Thanks for helping with the OPTICON common TAC review process.


YOUR ASSIGNED PROPOSALS:
------------------------
Technical reviewer (%reviewerCount% proposals):
%reviewerCodes%

Please enter your reviews online in %proposalreviewtool%:
%proposalreviewuri%

Check now, please, whether you have any conflict-of-interest. If so,
please contact me by e-mail directly. Affiliations at the
same institute are unavoidable.

You first have to be registered for the online review process for the
%committee%; you should have received an e-mail instructing you how
to register if that was still needed; if you have not done so already,
please follow the link in that mail to register now.


--> The schedule of the common TAC is very tight, please try to
submit your reviews in the next 3 weeks. <--

Further information can be found online.

Best regards,
%sender%

