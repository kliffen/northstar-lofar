// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control;

public final class Constants {
    public static final String ERROR = "error";
    public static final String WARNING = "warning";
	
    public static final String OWN_USERACCOUNT = "own_useraccount"; 
	public static final String ENABLE_PAGE_NUMBER_CONF = "enableConfPageNumber";
    public static final String CONFIRM = "confirm";
    public static final String REFRESH = "refresh";
    public static final String SUCCESS = "success";
    public static final String FAILURE = "failure";
    public static final String CANCEL = "cancel";
    public static final String DISCARD = "discard";
    public static final String SAVE_AND_EXIT = "saveAndExit";
    public static final String SAVE_AND_SUBMIT = "saveAndSubmit";
    public static final String SAVE_AND_VIEW = "saveAndView";
	
    public static final String PROPOSAL_DATA = "proposaldata";   
    public static final String NEW_PROPOSAL_DATA = "newproposaldata";  
    public static final String LOCKED_PROPOSAL = "lockedProposal";
    public static final String OBSERVATION = "observation";
    public static final String OBSERVATIONS = "observations";	
    public static final String UPLOAD_TARGET_LIST = "uploadTargetList";
    public static final String NEW_TARGETS = "newTargets";
	
    public static final String CURRENT_ACTION = "currentaction";
    public static final String ENTER_CLASS = "Entered this class";
    public static final String FORWARD = "forward";
    public static final String EPOCH_J2000 = "J2000";
	
    public static final String SCIENTIFIC = "scientific";
    public static final String TECHNICAL= "technical";
    public static final String FIGURE = "figure";
    public static final String ENVELOPE = "envelope";
    
	public static final String UPLOAD = "upload";  
    public static final String INTERNAL_ERROR_ACTION = "/internalError.do";
    public static final String MAX_OBSERVATIONS = "maxObservations";
    public static final String UV_RANGE_IMAGING = "uvRange";
    public static final String PIPELINE_STORAGE_FACTOR = "pipelineStorageFactor";
    public static final String BROWSER = "browser";
    public static final String GECKO = "gecko";
    public static final String BROWSER_PARAM = "browser";
    public static final String FORWARD_ACTION = "/forward.do";
	
    public static final String DATETIME_FORMAT = "yyyy/MM/dd HH:mm z"; 
    public static final String SEMESTER_DATETIME_FORMAT = "yyyy/MM/dd HH:mm:ss";  
    
   
    public static final String TIME_FORMAT = "HH:mm";  
    public static final String SHORT_TIME_FORMAT = "HH";
    public static final String DATE_FORMAT = "yyyy/MM";   
	
    public static final String NONE = " -- None Specified --";
	
	public static final String NONE_SPECIFIED_LABEL = "-- None Specified --";

	public static final String NONE_SPECIFIED_VALUE = "";
	public static final String SCIENTIFIC_REQUIRED_CATEGORIES = "scientificFileRequiredCategories";
	
	public static final String CHANGE_STATUS_BEAN = "changeStatusBean";
	public static final String OWN_PROPOSAL_SELECTION = "ownProposalSelection";
	public static final String ADMIN_PROPOSAL_SELECTION = "adminProposalSelection";
	public static final String ADMIN_SEMESTER_SELECTION = "adminSemesterSelection";
	public static final String TARGETS_ON_TABBED_PAGE = "targetsOnTabbedPage";

	public static final String MAX_SCIENTIFIC_PAGES = "maxScientificPages";
	public static final String MAX_TARGETS_SAME_RUN = "maxTargetsonSameRun";
	public static final String ENABLE_LINKED_PROPOSALS_THIS = "linkedProposalsThis";
	public static final String ENABLE_LINKED_PROPOSALS_OTHER = "linkedProposalsOther";
	public static final String ENABLE_PREVIOUS_ALLOCATIONS = "previousAllocations";
	public static final String ENABLE_RELATED_PUBLICATIONS = "relatedPublications";
	public static final String ENABLE_RELATED_PREVIOUS_INVOLVED_PROPOSAL = "relatedPreviousInvolvedProposal";
	public static final String ENABLE_GRANT_NUMBER = "grantNumber";
	public static final String ENABLE_SPONSORING = "sponsoring";
	public static final String ENABLE_NEW_OBSERVER_EXPERIENCE = "enableNewObserverExperience";
    public static final String ENABLE_ENVELOPE_SHEET = "enableEnvelopeSheet";
    public static final String ENABLE_FIRST_JUSTIFICATION = "enableFirstJustification";
	public static final String ENABLE_SECOND_JUSTIFICATION = "enableSecondJustification";
	public static final String ENABLE_FIGURES_FILE = "enableFiguresFile";
	public static final String ENABLE_TECHNICAL_JUSTIFICATION = "enableTechnicalJustification";	
	public static final String ENABLE_COMBINED_OUTPUT_SELECTION = "enableCombinedOutputProductSelection";	
	public static final String MAX_PUBLICATION_COUNT = "max-publication-count";
	public static final String MAX_RELATED_PREVIOUS_INVOLVED_PROPOSAL_TEXT_COUNT = "max-related-previous-involved-proposal-text-count";
	public static final String MAX_SCIENTIFIC_PAGECOUNT = "max-scientific-pages";
	public static final String MAX_TECHNICAL_PAGECOUNT =  "max-technical-pages";
	public static final String MAX_FIGURE_PAGECOUNT = "max-figure-pages";
	public static final String MAX_TOTAL_PAGECOUNT = "max-total-pages";
	public static final String ALLOWED_FIGURE_TYPES = "allowed-figure-file-types";
	public static final String ALLOWED_JUSTIFICATION_TYPES = "allowed-justification-file-types";
	
	public static final String LATEX_CLASS_FILE = "latexClassFile";
	public static final String LATEX_TEMPLATE_FILE = "latexTemplateFile";
	public static final String LATEX_REQUIRED_HEADER ="latexRequiredHeader";
	public static final String SINGLE_FILE_UPLOAD = "singleFileUPload";
	public static final String AUTO_TARGET_COMMIT = "autoTargetCommit";
	public static final String DISABLE_INVITATION_EMAIL_VALIDATION = "disableEmailValidation";
	public static final String TIME_GAP_PAGE_COUNT_INCREMENT = "time-gap-page-number";
	public static final String MINIMUM_JUSTIFICATION_PAGE_NUMBER = "min-justification-page-number";
	
	public static final String DISPLAY_OBSERVATION_STRATEGY = "displayObservationStrategy";
	public static final String DISPLAY_TECH_JUSTIFICATIONS="technicalJustifications";
	public static final String DISPLAY_TECH_NIGHT_TIME = "displayNightTime";
	public static final String DISPLAY_TECH_PARALLEL_OBSERVATION = "displayParallelObservation";
	
	public static final String DISPLAY_TECH_INTERNATIONAL_STATION = "displayInternationalStation";
	public static final String DISPLAY_TECH_MAX_DATA_RATE = "displayMaxDataRate";
	//public static final String DISPLAY_TECH_LTA_STOTAGE = "displayLtaStorage";
	public static final String DISPLAY_TECH_SENSITIVITY = "displaySensitivity";
	public static final String DISPLAY_TECH_OFFLINE_PROCESSING = "displayOfflineROProcessing";
	public static final String DISPLAY_TECH_EXTERNAL_PROCESSING = "displayExternalProcessing";
	public static final String DISPLAY_TECH_OTHER_SCHEDULING="displayotherSchedulingConstraints";
	public static final String DISPLAY_TECH_COMBINED_DATA_PRODUCT_REQUEST = "displayCombinedDataProductRequest";
	public static final String DISPLAY_TECH_CEP_REQUEST = "displayCepRequesting";
	
	public static final String DISPLAY_TECH_RO_PROCESSING = "displayROProcessing";
	public static final String DISPLAY_TECH_RO_PROCESSING_WITHOUT_SELFCAL = "displayROProcessingWithoutSelfCal";
	public static final String DISPLAY_TECH_FILLER_TIME = "displayFillerTime";
	public static final String DISPLAY_TECH_LTA_STORAGE = "displayLtaStorage";
	public static final String DISPLAY_TECH_LTA_STORAGE_ADVANCED = "displayLtaStorageAdvanced";
	public static final String DISPLAY_TECH_COOBSERVATION_TEAM = "displayCoObservationTeam";
}
