// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import javax.naming.NamingException;

import nl.astron.useradministration.data.entities.User;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.KeyGenerator;
import nl.astron.util.AstronValidator;
import nl.astron.util.exception.AstronMailException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.email.CollaborationEmailGenerator;
import eu.radionet.northstar.business.exception.AcceptanceOfInvitationRejectedException;
import eu.radionet.northstar.business.exception.ActiveParticipantException;
import eu.radionet.northstar.business.exception.InvalidUserException;
import eu.radionet.northstar.business.exception.NoInvitationFoundException;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.Repository;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.Thesis;
import eu.radionet.northstar.data.entities.collaboration.Invitation;
import eu.radionet.northstar.data.entities.collaboration.ProposalLock;

public class CollaborationDelegate {
	private Log log = LogFactory.getLog(this.getClass());

	private static CollaborationDelegate collaborationDelegate = null;

	private static CollaborationEmailGenerator collaborationEmailGenerator = null;

	private static Repository repository = null;

	private static NorthStarDelegate northStarDelegate = null;

	/**
	 * Default constructor
	 */
	private CollaborationDelegate() throws ConnectionException {
		repository = new Repository();
		collaborationEmailGenerator = new CollaborationEmailGenerator();
		northStarDelegate = NorthStarDelegate.getInstance();
	}

	/**
	 * The getInstance method provides
	 * 
	 * @return
	 * @throws ConnectionException
	 */
	public static CollaborationDelegate getInstance()
			throws ConnectionException {
		if (null == collaborationDelegate) {
			collaborationDelegate = new CollaborationDelegate();
		}
		return collaborationDelegate;
	}

	public void processAllAcceptedProposals(String sessionId, UserAccount userAccount) throws DatabaseException {
		List acceptedInvitations = repository.getAllAcceptedInvitations();
		Iterator acceptedInvitationsIterator = acceptedInvitations.iterator();
		while (acceptedInvitationsIterator.hasNext()){
			Invitation invitation = (Invitation) acceptedInvitationsIterator.next();
			if (!isLocked(invitation.getProposalId())){
				lockProposal(invitation.getProposalId(), userAccount,sessionId);
				unlockProposal(invitation.getProposalId(),userAccount,sessionId);
			}
		}
	}
	
	public void deleteInvalidProposalLock(Integer proposalId,
			UserAccount userAccount, String sessionId) throws DatabaseException {
		/*
		 * retrieve proposal lock
		 */
		ProposalLock proposalLock = repository.getProposalLock(proposalId,
				userAccount.getUser().getId(), sessionId);
		if (proposalLock != null) {
			repository.delete(proposalLock);
		}
	}

	/**
	 * The lockProposal method locks an proposal to an user
	 * 
	 * @param proposal
	 * @param userAccount
	 * @param sessionId
	 * @throws DatabaseException
	 */
	public void lockProposal(Integer proposalId, UserAccount userAccount,
			String sessionId) throws DatabaseException {
		/*
		 * looks if lock already exists
		 */
		ProposalLock oldProposalLock = repository
				.getValidProposalLock(proposalId);

		ProposalLock newProposalLock = new ProposalLock();
		/*
		 * fill lock
		 */
		newProposalLock.setProposalId(proposalId);
		newProposalLock.setUserId(userAccount.getUser().getId());
		newProposalLock.setSessionId(sessionId);
		Calendar calendar = Calendar.getInstance();
		newProposalLock.setValid(true);
		newProposalLock.setSince(calendar.getTime());
		newProposalLock.setLastAction(calendar.getTime());
		if (oldProposalLock != null) {
			oldProposalLock.setValid(false);
			oldProposalLock.setRemovedSince(newProposalLock.getSince());
			oldProposalLock.setRemoverId(newProposalLock.getUserId());
			repository.store(oldProposalLock);
		}
		/*
		 * store it
		 */
		repository.store(newProposalLock);
		log.info(LogMessage.getMessage(userAccount, "Locked proposal with id: "
				+ proposalId));
	}

	/**
	 * The unlockProposal method unlocks a proposal
	 * 
	 * @param proposal
	 * @param userAccount
	 * @param sessionId
	 * @throws DatabaseException
	 */
	public void unlockProposal(Integer proposalId, UserAccount userAccount,
			String sessionId) throws DatabaseException{
		/*
		 * retrieve proposal lock
		 */
		ProposalLock proposalLock = repository.getProposalLock(proposalId,
				userAccount.getUser().getId(), sessionId);
		/*
		 * if lock exists
		 */
		if (proposalLock != null) {
			/*
			 * and session id and user id are equal remove lock.
			 */
			if (proposalLock.getSessionId().equals(sessionId)
					&& userAccount.getUser().getId().intValue() == proposalLock
							.getUserId().intValue()) {
				/*
				 * try to add users that accept the invitation
				 */
				Proposal proposal = repository.getProposal(proposalLock
						.getProposalId());
				if (proposal != null && addRegisteredMembers(proposal, userAccount)) {
					repository.store(proposal);
				}
				repository.delete(proposalLock);
				log.info(LogMessage.getMessage(userAccount,
						"UnLocked proposal with id: " + proposalId));
			}

		}

	}

	/**
	 * The unlockProposals method unlock all proposals of this session
	 * 
	 * @param userAccount
	 * @param sessionId
	 * @throws DatabaseException
	 * @throws NamingException
	 * @throws AstronMailException
	 */
	public void unlockProposals(UserAccount userAccount, String sessionId)
			throws DatabaseException {
		/*
		 * retrieve proposal lock
		 */
		List proposalLocks = repository.getProposalLocks(userAccount.getUser()
				.getId(), sessionId);
		for (int i = 0; i < proposalLocks.size(); i++) {
			ProposalLock proposalLock = (ProposalLock) proposalLocks.get(i);
			/*
			 * if lock exists
			 */
			if (proposalLock != null) {
				/*
				 * and session id and user id are equal remove lock.
				 */
				if (proposalLock.getSessionId().equals(sessionId)
						&& userAccount.getUser().getId().intValue() == proposalLock
								.getUserId().intValue()) {
					/*
					 * try to add users that accept the invitation
					 */
					Proposal proposal = repository.getOwnProposal(proposalLock
							.getProposalId(), userAccount.getUser().getId());
					if (proposal != null) {
						addRegisteredMembers(proposal, userAccount);
					}
					repository.delete(proposalLock);
					log.info(LogMessage.getMessage(userAccount,
							"UnLocked proposal with id: "
									+ proposalLock.getProposalId()));
				}

			}
		}

	}

	/**
	 * The checkAndUpdateProposalLock method checks if proposal is lock by
	 * userAccount. If so it updates lastaction if not it returns false.
	 * 
	 * @param proposal
	 * @param userAccount
	 * @param sessionId
	 * @return
	 * @throws DatabaseException
	 */
	public boolean checkAndUpdateProposalLock(Integer proposalId,
			UserAccount userAccount, String sessionId) throws DatabaseException {
		if (proposalId == null) {
			return true;
		}
		ProposalLock proposalLock = repository.getValidProposalLock(proposalId);
		/*
		 * if lock exists
		 */
		if (proposalLock != null) {
			/*
			 * and session id and user id are equal update lock
			 */
			if (proposalLock.getSessionId().equals(sessionId)
					&& userAccount.getUser().getId().intValue() == proposalLock
							.getUserId().intValue() && proposalLock.isValid()) {
				/*
				 * update last action
				 */
				Calendar calendar = Calendar.getInstance();
				proposalLock.setLastAction(calendar.getTime());

				/*
				 * store it
				 */
				repository.store(proposalLock);
				return true;
			}
			/*
			 * proposal is locked by another session and/or user
			 */
			else {
				return false;
			}

		} else {
			// lockProposal(proposalId, userAccount, sessionId);
			return false;
		}
	}

	/**
	 * The proposalIsLockedByOthers method search if the proposal is locked by
	 * other users
	 * 
	 * @param proposalId
	 * @param userAccount
	 * @param sessionId
	 * @return
	 * @throws DatabaseException
	 */
	public boolean proposalIsLockedByOthers(Integer proposalId,
			UserAccount userAccount, String sessionId) throws DatabaseException {
		if (proposalId == null) {
			return true;
		}
		ProposalLock proposalLock = repository.getValidProposalLock(proposalId);
		/*
		 * if lock exists
		 */
		if (proposalLock != null) {
			/*
			 * and session id and user id are equal update lock
			 */
			if (proposalLock.getSessionId().equals(sessionId)
					&& userAccount.getUser().getId().intValue() == proposalLock
							.getUserId().intValue() && proposalLock.isValid()) {
				return false;
			}
			/*
			 * proposal is locked by another session and/or user
			 */
			else {
				return true;
			}

		} else {
			return false;
		}
	}

	/**
	 * The getProposalLock method retrieves proposal lock
	 * 
	 * @param proposalId
	 * @return
	 * @throws DatabaseException
	 */
	public ProposalLock getValidProposalLock(Integer proposalId)
			throws DatabaseException {
		return repository.getValidProposalLock(proposalId);
	}

	public ProposalLock getProposalLock(Integer proposalId, Integer userId,
			String sessionId) throws DatabaseException {
		return repository.getProposalLock(proposalId, userId, sessionId);
	}

	public List getValidProposalLocks() throws DatabaseException {
		return repository.getValidProposalLocks();
	}

	/**
	 * The isLocked method checks if the proposal is locked
	 * 
	 * @param proposalId
	 * @return
	 * @throws DatabaseException
	 */
	public boolean isLocked(Integer proposalId) throws DatabaseException {
		return getValidProposalLock(proposalId) != null;
	}

	public List getAcceptedInvitations(UserAccount userAccount)
			throws DatabaseException{
		return repository.getAcceptedInvitationsByUserId(userAccount.getUser()
				.getId());
	}

	public Invitation getAcceptedInvitation(Integer proposalId,
			UserAccount userAccount) throws DatabaseException
			 {
		return repository.getAcceptedInvitation(proposalId, userAccount
				.getUser().getId());
	}

	public Proposal getProposal(Integer proposalId, UserAccount userAccount)
			throws DatabaseException{
		Invitation invitation = repository.getAcceptedInvitation(proposalId,
				userAccount.getUser().getId());
		if (invitation != null) {
			return northStarDelegate.getProposal(invitation.getProposalId());
		}
		return null;
	}

	public void acceptInvitation(String invitationKey, UserAccount userAccount,
			String sessionId, String proposalId) throws DatabaseException,
			AcceptanceOfInvitationRejectedException {
		
		/*
		 * retrieve invitation
		 */
		Invitation invitation = repository.getInvitation(invitationKey);
		if (invitation == null) {
			Integer proposalInt = new Integer(proposalId);
			Proposal proposal = northStarDelegate.getProposal(proposalInt);
			// proposal has been deleted...
			if(proposal==null){
				log.warn("proposal with id: "+proposalId+" cannot be retrieved.");
				throw new NoInvitationFoundException("No invitation found");
			}
							
			Integer userId = userAccount.getUser().getId();
			Iterator memberIterator = proposal.getMembers().iterator();
					
			while (memberIterator.hasNext()) {
	            Member nextMember = (Member) memberIterator.next();
	            if (nextMember.getClass().equals(RegisteredMember.class)) {
	            	RegisteredMember registeredMember = (RegisteredMember) nextMember;
                    if (registeredMember.getUserId().compareTo(userId) == 0) {
                    	/*
                         * the user already is an active participant; 
                         */
                    	log.info(LogMessage.getMessage(userAccount,
    					"Acceptance rejected, because user already participant for proposal "+proposalInt.toString()));
                    	throw new ActiveParticipantException("No invitation found, user already participant");
                    }
	            	
	            }
	        }   
	        
			log.info(LogMessage.getMessage(userAccount,
					"Acceptance rejected, because no invitation found"));
			throw new NoInvitationFoundException("No invitation found");
		}
		Proposal proposal = northStarDelegate.getProposal(invitation
				.getProposalId());
		 ContextType contextConfiguration = null;
		 TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
			if (contextConfiguration == null){
				contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
				
			}
			FieldsDefinitionType fieldsDefinitionType = 
				(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		Member member = proposal.getMember(invitation.getMemberId());
		/*
		 * email from user that are logged in is equal to the invitation email,
		 * add it to the proposal.
		 */
		if (telescopeConfiguration.isDisableEmailValidation()){
		if (!userAccount.getUser().getEmail().equalsIgnoreCase(
				member.getEmail())) {

			log.info(LogMessage.getMessage(userAccount,
					"Emailadresses are different: invitation emailadress: "
							+ member.getEmail() + " user emailadress: "
							+ userAccount.getUser().getEmail()));

			throw new InvalidUserException("Emailadresses are different");
		}
		
		}
        Iterator memberIterator = proposal.getMembers().iterator();
        while (memberIterator.hasNext()) {
            Member nextMember = (Member) memberIterator.next();
            if (nextMember.getId().compareTo(member.getId()) != 0) {
                if (nextMember.getClass().equals(RegisteredMember.class)) {
                    RegisteredMember registeredMember = (RegisteredMember) nextMember;
                    if (registeredMember.getUserId().compareTo(userAccount.getUser().getId()) == 0) {
                        /*
                         * the user already is an active participant; delete invitation
                         */
                        repository.delete(invitation);
                        log.info(LogMessage.getMessage(userAccount,
                                "The user already is an active participant: username: "
                                        + userAccount.getUsername()));

                        throw new ActiveParticipantException("The user already is an active participant");
                   }
                }
            }
        }
		invitation.setUserId(userAccount.getUser().getId());
		repository.store(invitation);
		log.info(LogMessage.getMessage(userAccount,
				"Store request to add registered member of proposal with id: "
						+ proposal.getId()));
		/*
		 * it the proposal isn't locked, lock it an unlock it, so possible new
		 * members will added
		 */
		if (!this.isLocked(proposal.getId())) {
			Integer proposalInt = proposal.getId();
			this.lockProposal(proposalInt, userAccount, sessionId);
			this.unlockProposal(proposalInt, userAccount, sessionId);
		}
	}

	/**
	 * The addRegisteredMembers method
	 * 
	 * @param proposal
	 * @return
	 * @throws DatabaseException
	 * @throws AstronMailException
	 * @throws NamingException
	 */
	public boolean addRegisteredMembers(Proposal proposal, UserAccount userAccount)
			throws DatabaseException{
		boolean result = false;
		/*
		 * retrieve invitation
		 */
		List invitations = repository.getAcceptedInvitations(proposal.getId());
		for (int j = 0; j < invitations.size(); j++) {
			Invitation invitation = (Invitation) invitations.get(j);
			/*
			 * if invitation is associated to registered user
			 */
			if (invitation.getUserId() != null
					&& invitation.getUserId().intValue() > 0) {
				User user = northStarDelegate.getUser(invitation.getUserId());
				/*
				 * search for non registered member that belongs the invitation
				 */
				for (int i = 0; i < proposal.getMembers().size(); i++) {
					Member member = (Member) proposal.getMembers().get(i);
					/*
					 * if it is an non registered member
					 */
					if (member.getClass().equals(NonRegisteredMember.class)) {
						/*
						 * member id's are equal
						 */
						if (member.getId() != null && member.getId().equals(invitation.getMemberId())) {

								RegisteredMember registeredMember = new RegisteredMember();
								registeredMember.setRoles(member.getRoles());
								registeredMember.setUser(user);
                                //theses need to be updated too.
                                updateMembersInThesis(proposal, member, registeredMember);
								proposal.getMembers().remove(i);
								proposal.getMembers().add(i, registeredMember);
								log.info(LogMessage.getMessage(userAccount,
										"Add " + NorthStarDelegate.getName(user) + " as registered member to proposal with id: "
												+ proposal.getId()));
								/*
								 * collaborationEmailGenerator.sendAcceptedEmail(
								 * invitation, user,
								 * proposal.getContactAuthor().getUser());
								 */
								repository.delete(invitation);
								result = true;

						}
					}
				}

			}
		}

		return result;

	}
    
    /**
     * Updating the applicants in the thesis list when an applicant has accepted the invite.
     * @param proposal
     * @param oldMember
     * @param newMember
     */
    public void updateMembersInThesis(Proposal proposal, Member oldMember, Member newMember)
    {
        if (proposal.getAdditionalIssues() != null &&  proposal.getAdditionalIssues().getTheses()!= null)
        {
            List newList = new ArrayList();
            Iterator thesisIt = proposal.getAdditionalIssues().getTheses().iterator();
            while (thesisIt.hasNext())
            {
                Thesis thesis = (Thesis) thesisIt.next();
                if (thesis.getStudent() != null && thesis.getStudent().getId() == oldMember.getId())
                {
                    thesis.setStudent(newMember);
                    log.debug("Student replaced in thesis. " + thesis.getStudent().getName() + " becomes "
                            + newMember.getName());
                }
                if (thesis.getSupervisor() != null && thesis.getSupervisor().getId() == oldMember.getId())
                {
                    thesis.setSupervisor(newMember);
                    log.debug("Supervisor replaced in thesis. " + thesis.getSupervisor().getName() + " becomes "
                            + newMember.getName());
                }
                newList.add(thesis);
            }
            proposal.getAdditionalIssues().setTheses(newList);
        }
    }

	public void deleteInvitations(Integer proposalId) throws DatabaseException {
		List invitations = repository.getInvitations(proposalId);
		for (int i = 0; i < invitations.size(); i++) {
			Invitation invitation = (Invitation) invitations.get(i);

			/*
			 * collaborationEmailGenerator.sendDeleteEmail(invitation,
			 * proposal.getContactAuthor().getUser());
			 */
			repository.delete(invitation);
		}
	}

	public void deleteInvitation(Proposal proposal,
			NonRegisteredMember nonRegisteredMember, UserAccount userAccount)
			throws DatabaseException {
		Invitation invitation = repository.getInvitation(proposal.getId(),
				nonRegisteredMember.getId());
		if (invitation != null) {
			repository.delete(invitation);
			nonRegisteredMember.setRemoveInvitation(false);
		}
	}

	public void sendAndDeleteInvitations(Proposal proposal,
			UserAccount userAccount) throws DatabaseException, NamingException,
			AstronMailException {
		deleteInvitations(proposal, userAccount);
		for (int i = 0; i < proposal.getMembers().size(); i++) {
			Member member = (Member) proposal.getMembers().get(i);
			/*
			 * if it is an non registered member
			 */
			if (member.getClass().equals(NonRegisteredMember.class)) {
				NonRegisteredMember nonRegisteredMember = (NonRegisteredMember) member;
				if (nonRegisteredMember.isInvite()) {
					collaborationDelegate.invite(proposal, nonRegisteredMember,
							userAccount);

				}
			}
		}
	}

	public void deleteInvitations(Proposal proposal, UserAccount userAccount)
			throws DatabaseException {
		List invitations = repository.getInvitations(proposal.getId());
		List memberIds = new ArrayList();
		/*
		 * Iterate through members and look if invitation must be deleted
		 */
		Iterator memberIterator = proposal.getMembers().iterator();
		while (memberIterator.hasNext()) {
			Member member = (Member) memberIterator.next();
			/*
			 * if member is non registered member, it can be invited
			 */
			if (member.getClass().equals(NonRegisteredMember.class)) {

					NonRegisteredMember nonRegisteredMember = (NonRegisteredMember) member;
					/*
					 * if invitation must be removed, add it to the memberIds list.
					 */
					if (nonRegisteredMember.isRemoveInvitation()) {
						nonRegisteredMember.setRemoveInvitation(false);
						if (member.getId() != null){
							memberIds.add(member.getId());
					}

				}
			}
		}
		Iterator memberIdIterator = memberIds.iterator();


		/*
		 * iterate through invitations and look if it must be deleted
		 */
		for (int i = invitations.size() - 1; i >= 0; i--) {
			Invitation invitation = (Invitation) invitations.get(i);
			int memberInvitationId = invitation.getMemberId().intValue();
			boolean found = false;
			/*
			 * iterate through members
			 */
			while (memberIdIterator.hasNext() && !found) {
				Integer memberId = (Integer) memberIdIterator.next();
				if (memberInvitationId == memberId.intValue()) {
					found = true;
				}
			}
			/*
			 * remove invitation
			 */
			if (found) {
				repository.delete(invitation);
				invitations.remove(i);
			}
		}

		
		/*
		 * iterate through invitations and look if member that belongs to the invitation exists
		 */
		for (int i = invitations.size() - 1; i >= 0; i--) {
			Invitation invitation = (Invitation) invitations.get(i);
			int memberInvitationId = invitation.getMemberId().intValue();
			boolean found = false;
			/*
			 * iterate through members
			 */
			memberIterator = proposal.getMembers().iterator();
			while (memberIterator.hasNext()) {
				Member member = (Member) memberIterator.next();
				/*
				 * if member is non registered member, it can be invited
				 */
				if (member.getClass().equals(NonRegisteredMember.class)) {
						/*
						 * if invitation belongs to member
						 */
						if (member.getId()!= null && member.getId().intValue() == memberInvitationId) {
							found = true;
						}

					}
				}

			/*
			 * remove invitation
			 */
			if (!found) {
				repository.delete(invitation);
				invitations.remove(i);
			}
		}

	}

	public void deleteInvitationsOfProposal(Proposal proposal,
			UserAccount userAccount) throws DatabaseException {
		List invitations = repository.getInvitations(proposal.getId());
		for (int i = 0; i < invitations.size(); i++) {
			Invitation invitation = (Invitation) invitations.get(i);
			repository.delete(invitation);
		}

	}
	public void sendInvitationForViewing(Proposal proposal,
			UserAccount userAccount) throws DatabaseException, NamingException,
			AstronMailException {
		for (int i = 0; i < proposal.getMembers().size(); i++) {
			Member member = (Member) proposal.getMembers().get(i);
			/*
			 * if it is an non registered member
			 */
			if (member.getClass().equals(NonRegisteredMember.class)) {
				NonRegisteredMember nonRegisteredMember = (NonRegisteredMember) member;
				inviteForViewing(proposal, nonRegisteredMember, userAccount);
			}
		}
	}
	
	
	/**
	 * The invite method invite an persion by emailadres to work on the proposal
	 * 
	 * @param proposal
	 * @param nonRegisteredMember
	 * @throws AstronMailException
	 * @throws NamingException
	 * @throws DatabaseException
	 */
	public void invite(Proposal proposal,
			NonRegisteredMember nonRegisteredMember, UserAccount userAccount)
			throws AstronMailException, NamingException, DatabaseException {
		Invitation invitation = getNewInvitation(proposal, nonRegisteredMember);
		repository.store(invitation);
		collaborationEmailGenerator.sendInvitationEmail(proposal,
				nonRegisteredMember, invitation, userAccount);
		nonRegisteredMember.setInvite(false);
	}

	protected void inviteForViewing(Proposal proposal,
			NonRegisteredMember nonRegisteredMember, UserAccount userAccount)
			throws AstronMailException, NamingException, DatabaseException {
		Invitation invitation = getNewInvitation(proposal, nonRegisteredMember);
		repository.store(invitation);
		collaborationEmailGenerator.sendInvitationForViewingEmail(proposal,
				nonRegisteredMember, invitation, userAccount);
		nonRegisteredMember.setInvite(false);
	}
	
	public void sendInvitation(NonRegisteredMember nonRegisteredMember,UserAccount ownUser, String personalMessage, String forwardPath)
			throws AstronMailException, NamingException, DatabaseException {

		// first get a member.
		Invitation invitation = getNewInvitation(nonRegisteredMember);
		repository.store(invitation);
		forwardPath += "/setUpAcceptManageInvitation.do?key="+invitation.getAcceptCode();
		
		collaborationEmailGenerator.sendInvitationForNorthStarEmail(nonRegisteredMember, invitation, ownUser,personalMessage,forwardPath);
		nonRegisteredMember.setInvite(false);
	}

	/*
	 * public void inviteForViewing(Proposal proposal, UserAccount userAccount)
	 * throws AstronMailException, NamingException, DatabaseException { List
	 * invitations = repository.getInvitations(proposal.getId()); int
	 * invitationsIndex = 0; /* iterator through members
	 */
	/*
	 * Iterator membersIterator = proposal.getMembers().iterator(); while
	 * (membersIterator.hasNext()) { Member member = (Member)
	 * membersIterator.next(); if
	 * (NonRegisteredMember.class.equals(member.getClass())) {
	 * NonRegisteredMember nonRegisteredMember = (NonRegisteredMember) member;
	 * if (invitationsIndex < invitations.size()) { Invitation invitation =
	 * (Invitation) invitations .get(invitationsIndex); if
	 * (nonRegisteredMember.getId().intValue() == invitation
	 * .getMemberId().intValue()) { collaborationEmailGenerator
	 * .sendInvitationForViewingEmail(proposal, nonRegisteredMember, invitation,
	 * userAccount); invitationsIndex++; } else { inviteForViewing(proposal,
	 * nonRegisteredMember, userAccount); } } else { inviteForViewing(proposal,
	 * nonRegisteredMember, userAccount); } } } }
	 */

	/*
	 * public void reInvite(Proposal proposal, NonRegisteredMember
	 * nonRegisteredMember, UserAccount userAccount) throws AstronMailException,
	 * NamingException, DatabaseException { Invitation invitation =
	 * repository.getInvitation(proposal.getId(), nonRegisteredMember.getId());
	 * collaborationEmailGenerator.sendInvitationEmail(proposal,
	 * nonRegisteredMember, invitation, userAccount);
	 * nonRegisteredMember.setReInvite(false); }
	 */

	protected Invitation getNewInvitation(Proposal proposal,
			NonRegisteredMember nonRegisteredMember) throws DatabaseException {
		Invitation invitation = new Invitation();
		invitation.setProposalId(proposal.getId());
		invitation.setMemberId(nonRegisteredMember.getId());
		invitation.setAcceptCode(generateInvitationKey());
		Calendar calendar = Calendar.getInstance();
		invitation.setInvitationTime(calendar.getTime());
		return invitation;
	}
	
	protected Invitation getNewInvitation(NonRegisteredMember nonRegisteredMember) throws DatabaseException {
		if(nonRegisteredMember.getId() == null || nonRegisteredMember.getId().intValue() < 1){
			// create a new member
			repository.store(nonRegisteredMember);
		}
		
		Invitation invitation = new Invitation();
		invitation.setProposalId(new Integer(-1));
		invitation.setMemberId(nonRegisteredMember.getId());
		invitation.setAcceptCode(generateInvitationKey());
		Calendar calendar = Calendar.getInstance();
		invitation.setInvitationTime(calendar.getTime());
		return invitation;
	}

	public Invitation getInvitation(Proposal proposal,
			NonRegisteredMember nonRegisteredMember) throws DatabaseException {
		return repository.getInvitation(proposal.getId(), nonRegisteredMember
				.getId());
	}

	public List getInvitations(Proposal proposal) throws DatabaseException {
		return repository.getInvitations(proposal.getId());
	}

	public String generateInvitationKey() throws DatabaseException {
		KeyGenerator keyGenerator = new KeyGenerator();
		String publicKey = keyGenerator.getPassword(20);
		boolean unique = repository.isUniqueInvitationKey(publicKey);
		while (!unique) {
			publicKey = keyGenerator.getPassword(20);
			unique = repository.isUniqueInvitationKey(publicKey);
		}
		return publicKey;
	}

	public String getEmailName(User user) {
		String name = null;
		if (AstronValidator.isBlankOrNull(user.getTitle())) {
			name = user.getFirstName();
		} else {
			name = user.getTitle();
		}
		name += " " + user.getLastName();
		return name;
	}

	public Invitation getInvitation(String acceptcode) throws DatabaseException {
		return repository.getInvitation(acceptcode);
	}

	public NonRegisteredMember getMemberById(Integer memberId) throws DatabaseException {
		return repository.getMember(memberId);
	}

	public void acceptInvitation(Invitation invitation,
			NonRegisteredMember member, UserAccount ownUserAccount) throws DatabaseException {
		// convert usertelescope memberid to userid
		repository.convertUsertelescope(member.getId(),ownUserAccount.getId());
		
		// remove invitation and member
		repository.delete(invitation);
		repository.delete(member);
		
	}

	/*
	 * public User getContactAuthor(Proposal proposal) throws DatabaseException {
	 * for (int i = 0; i < proposal.getMembers().size(); i++) { if
	 * (RegisteredMember.class.equals(proposal.getMembers().get(i) .getClass())) {
	 * RegisteredMember member = (RegisteredMember) proposal
	 * .getMembers().get(i); if (member.isContactAuthor()) { return
	 * userManagement.getUser(member.getUserId()); } } } return null; }
	 */

}
