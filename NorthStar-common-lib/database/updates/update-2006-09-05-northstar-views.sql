# Only use after loading a dump on a different machine

DROP VIEW `invitationsview`;
CREATE VIEW `invitationsview` AS
SELECT p.code as proposal_code, i.invitationtime, m.name as name, m.affiliation as affiliation, m.email as email, i.id as invitationid,p.id as proposalid   FROM invitation i, nonregisteredmember m, proposal p
WHERE i.memberid=m.memberid AND i.proposalid = p.id ORDER BY p.code;

DROP VIEW `privileges`;
CREATE VIEW `privileges` AS
SELECT p.id as proposalid, r.userid  FROM proposal p, member m, registeredmember r
WHERE m.proposalid=p.id AND r.memberid=m.id;