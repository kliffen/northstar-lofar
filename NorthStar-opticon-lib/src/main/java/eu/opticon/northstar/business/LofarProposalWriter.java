// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SkeletonProposalWriter.java 
 *
 * Created on Mar 10, 2005
 *
 * Version $Id: OpticonProposalWriter.java,v 1.43 2008-08-15 10:07:31 boelen Exp $
 *
 */
package eu.opticon.northstar.business;

import java.awt.Color;
//import java.awt.TextArea;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;

import nl.astron.service.lofar.StorageCalculator;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import nl.astron.util.converters.StorageConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.LabelValueBean;
import org.apache.struts.util.MessageResources;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.BadPdfFormatException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

import eu.opticon.northstar.control.observingrequest.instrument.SetUpInstrumentBean;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.AstroCoordinate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.business.pdf.ProposalPdfPageEventHelper;
import eu.radionet.northstar.business.pdf.ProposalWriter;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.observingrequest.ValueBean;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.AdditionalIssues;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.Target;
import eu.radionet.northstar.data.entities.admin.AdminProposalSummary;

/**
 * Generates the telescope specific parts of the pdf view
 */
public class LofarProposalWriter extends ProposalWriter {

	private Log log = LogFactory.getLog(this.getClass());
	protected static final String WHITESPACE = " ";

	protected static final String COMMA = ", ";

	protected static final String DOUBLE_POINT = ": ";

	protected List applicants = new ArrayList();
	protected String community = null;
	
	protected MessageResources opticonLabels = null;
	
	public static final Font TITLE_REQUIRED_FONT = 
		FontFactory.getFont(FontFactory.HELVETICA, 12, Font.ITALIC, Color.RED);

	public static final Font TABLE_CONTENT_FONT_SMALL = FontFactory.getFont(
			FontFactory.HELVETICA, 6, Font.NORMAL);
	
	public static final Font TABLE_HEADER_FONT_SMALL = FontFactory.getFont(
			FontFactory.HELVETICA, 6, Font.BOLD, Color.WHITE);
	double totalProcessingTime =0;
	double targetProcessingTime =0;
	StorageCalculator sCalc = null;
	Map<String,Double> observationStorageMap = new HashMap();
	Map<String,Integer> observationSubbandMap = new HashMap();
	Map<String,Double> observationBFStorageMap = new HashMap();
	Map<String,Double> observationUVStorageMap = new HashMap();
	Map<String,Double> observationTBBStorageMap = new HashMap();
    Map<String, Double> observationDurationMap = new HashMap();
    Map<String, Boolean> observationVisitedMap = new HashMap();
	Map<String, Integer> observationStationMap = new HashMap();
	
	protected PdfPCell getOpticonTableTextCell(String text, boolean border) {
		PdfPCell cell = null;
		if (border) {
			cell = this.getTableCell();
		} else {
			cell = this.getBorderlessCell();
		}
		if (!AstronValidator.isBlankOrNull(text)) {
			cell.setPhrase(new Phrase(text, NORMAL_FONT));
		} else {
			cell.setPhrase(new Phrase(EMPTY_NOT_REQUIRED, TABLE_CONTENT_FONT));
		}
		return cell;
	}

	/**
	 * The getTableTitleTextCell generates tablecell with text
	 * 
	 * @param text
	 * @param border
	 * @return
	 */
	protected PdfPCell getOpticonTableTitleTextCell(String text, boolean border) 
	{
		PdfPCell cell = null;
		if (border) {
			cell = this.getTableCell();
		} else {
			cell = this.getBorderlessCell();
		}
		if (!AstronValidator.isBlankOrNull(text)) {
			cell.setPhrase(new Phrase(text, PROPOSAL_TITLE_FONT));
		} else {
			cell.setPhrase(new Phrase(EMPTY_NOT_REQUIRED, PROPOSAL_TITLE_FONT));
		}
		return cell;
	}
	
    /**
     * default constructor
     */
    public LofarProposalWriter() {
        super();
    }

	protected Paragraph getParagraphSmallTitle(String text) 
	{
		Paragraph title = new Paragraph(text, OTHER_SUB_TITLE_FONT);
		return title;
	}

	protected void updateHeaderImage(Proposal proposal, ServletContext servletContext)
		throws DocumentException, IOException 
	{
		/*
		 * put the telescopes in a hashmap, with the telescope as the key and
		 * the sum of nights as the value. (How to find the max value then [set
		 * of keys -> max funtion])?
		 */

		String maxTelescope = "";
		double maxTelescopeValue = 0;
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) proposal
				.getObservingRequest();
		// get the message so the translation table will be fillled.
		String message = NorthStarUtils.getLabel(servletContext, proposal, "label.opticon.targettable.targets");

		Iterator obsIt = opticonObservingRequest.getObservations().iterator();
		Set telescopeSet = new HashSet();
		
		// Sets only have unique values (removing double telescopes)
		while (obsIt.hasNext())
		{
			OpticonObservation observation = (OpticonObservation) obsIt.next();
			if (observation.getTelescopeConfiguration() != null)
			{
				telescopeSet.add(observation.getTelescopeConfiguration());
			}
		}
		Iterator telescopeIt = new ArrayList(telescopeSet).iterator();
		while (telescopeIt.hasNext())
		{
			// sum up the number of nights per telescope
			double result = 0;
			String telescopeName = (String) telescopeIt.next();
			Double nights = NorthStarDelegate.getResourceValue(
					opticonObservingRequest.getAllocations(), telescopeName
							+ '_' + OpticonConstants.BRIGHT);
			if (nights != null)
			{
				result += nights.doubleValue();
			}
			nights = NorthStarDelegate.getResourceValue(opticonObservingRequest
					.getAllocations(), telescopeName + '_'
					+ OpticonConstants.DARK);
			if (nights != null)
			{
				result += nights.doubleValue();
			}
			nights = NorthStarDelegate.getResourceValue(opticonObservingRequest
					.getAllocations(), telescopeName + '_'
					+ OpticonConstants.FIRST_Q);
			if (nights != null)
			{
				result += nights.doubleValue();
			}
			nights = NorthStarDelegate.getResourceValue(opticonObservingRequest
					.getAllocations(), telescopeName + '_'
					+ OpticonConstants.LAST_Q);
			if (nights != null)
			{
				result += nights.doubleValue();
			}
			
			if (result == maxTelescopeValue)
			{
				/*
				 *  the guard below is only possible when the maxTelescope and telescopeName
				 *  are both the same
				 */
				if (!telescopeName.equals(maxTelescope))
				{
					maxTelescope = "";
					maxTelescopeValue = result;
				}
			}
			else if (result > maxTelescopeValue)
			{
				maxTelescope = telescopeName;
				maxTelescopeValue = result;
			}
		}
		// disable theprefered telescope header function
		/*
		if (!maxTelescope.equals("") && !maxTelescope.equals("UKIRT"))
		{
			TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
			PdfUtils pdfUtils = new PdfUtils();
			
			String imageUri = telescopeConfiguration.getPdfHeaderImageUri();
			this.setHeaderImage(pdfUtils.getHeader(createNewHeader(maxTelescope,
					imageUri), servletContext));
			
		}
		*/
	}

	protected String createNewHeader(String prefix, String oldPath)
	{
		String[] parts = oldPath.split("/");
		String oldHeader = parts[parts.length-1];
		String newHeader = prefix + oldHeader;
		return oldPath.replaceAll(oldHeader, newHeader);
	}
	
	/**
	 * This methods overwrites the super one it is needed to add the extra pages
	 * after the science/technical case
	 * 
	 * @param proposal
	 * @param outputStream
	 * @throws DocumentException
	 * @throws IOException
	 */
	public void write(Proposal proposal, OutputStream outputStream, 
			ServletContext servletContext)
		throws DocumentException, IOException 
	{
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		contextPath = servletContext.getRealPath("/");
		// must be the same as the the label defined in the web xml.
		opticonLabels = (MessageResources) servletContext.getAttribute("org.apache.struts.action.MESSAGE/opticon"); 
		//servletContext.getAttribute("/opticon");
		
		/*
		 * Add the custom header here. When none of the telescope configs are used
		 * or the number of nights are the same the telescope used the "general" version
		 * otherwise it has the logo of the telescope with the most nights requested
		 */
		updateHeaderImage(proposal, servletContext);
		
		firstHeader = getFirstHeader(proposal);

		community = proposal.getSemester().getCommunity(); 
		applicants = proposal.getMembers();
		/*
		 * create the master document
		 */
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN);
		header = getHeader(proposal, document);

		/*
		 * instantiate pdf copy for merging pdf documents
		 */

		PdfCopy pdfCopy = new PdfCopy(document, outputStream);

		setMetaData(proposal, document);
		document.open();
		
		// get first and second page details.
		addPdf(pdfCopy, new PdfReader(getProposalPdf(proposal)));
		//addPdf(pdfCopy, new PdfReader(getTargetPage(proposal)));
		// add science / technical justification
		addPdf(pdfCopy, new PdfReader(getFiles(proposal)));
		// add poor weather backup and other info. 
		addPdf(pdfCopy, new PdfReader(getAdditionalPages(proposal)));
		if(proposal.getJustification().getEnvelopeSheet() != null && proposal.getJustification().getEnvelopeSheet().length() >1){
			addPdf(pdfCopy, new PdfReader(getEnvelopeSheet(proposal)));
		}
		
		
		/*
		 * add our generated pdf
		
		if (proposal.getObservingRequest().getObservations().size() > 0) {
			try {
				addPdf(pdfCopy, new PdfReader(
						getObservationsDetailsPdf(proposal
								.getObservingRequest().getObservations())));
			} catch (DocumentException de) {
				if (log.isDebugEnabled()) {
					// log.debug("Has no observation details");
				}
			}
		}
		*/
		document.close();

	}
	
	private byte[] getTargetPage(Proposal proposal) throws DocumentException {
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN + FIRST_HEADER_SIZE, BOTTOM_MARGIN);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		float firstHeaderSize = 0;
		firstHeaderSize = FIRST_HEADER_SIZE;

		// create pdf writer
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				byteArrayOutputStream);
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(firstHeader,
				header, FOOTER_POSITION, firstHeaderSize, HEADER_SIZE, this);

		pdfWriter.setPageEvent(pageEvent);
		/*
		 * this must before open, then all pages has footer. if after
		 * document.open, first page is without footer
		 */

		document.open();
		document.setMargins(LEFT_MARGIN, RIGHT_MARGIN, TOP_MARGIN,
				BOTTOM_MARGIN);
		// page 3 add targets...
		document.newPage();
		PdfPTable targetPageTable = getDefaultTable(1);
		addTargets(proposal,targetPageTable);
		
		document.add(targetPageTable);
		document.close();
		return byteArrayOutputStream.toByteArray();
	}

	private byte[] getEnvelopeSheet(Proposal proposal) throws BadPdfFormatException, DocumentException {
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN);
		
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				byteArrayOutputStream);
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(header,
				HEADER_SIZE);

		
		
		pdfWriter.setPageEvent(pageEvent);
		document.open();
		String scientificFileName = proposal.getJustification().getEnvelopeSheet();
		String footer = scientificFileName;
		try {
			NorthStarDelegate northStarDelegate = NorthStarDelegate.getInstance();
			//UserAccount ownUserAccount = (UserAccount) request.getSession.getAttribute(Constants.OWN_USERACCOUNT);
			
			RegisteredMember registeredMember = getContactAuthor(proposal
					.getMembers());
			boolean hasnopage = true;
			if(proposal.getJustification().getEnvelopeSheet() != null && proposal.getJustification().getEnvelopeSheet().length() >1){
				byte[] envelopeFile = northStarDelegate.getEnvelopeFile(registeredMember.getUserId() ,proposal.getJustification().getEnvelopeSheet());
				
				if(envelopeFile != null && envelopeFile.length > 10){
					PdfReader reader = new PdfReader(envelopeFile);
					
					addPdf(pdfWriter, reader,
						document, footer);
					hasnopage=false;
				}
				
				
			}
			if(hasnopage){
				document.add(new Paragraph(
						"No Scientific Justification File uploaded",
						HEADER_REQUIRED_FONT));
				PdfContentByte cb = pdfWriter.getDirectContent();
				PdfPTable footerTable = getFooter("");
				footerTable.setTotalWidth(document.right() - document.left());
				footerTable.writeSelectedRows(0, -1, document.left(),
						FOOTER_POSITION, cb);
				document.newPage();
			}
			/*
			 * disable intentional blank page for now
			
			if (reader.getNumberOfPages() == 1){
				document.add(new Paragraph(
						"This page is intentionally left blank",
						HEADER_NORMAL_FONT));
				PdfContentByte cb = pdfWriter.getDirectContent();
				PdfPTable footerTable = getFooter("");
				footerTable.setTotalWidth(document.right() - document.left());
				footerTable.writeSelectedRows(0, -1, document.left(),
						FOOTER_POSITION, cb);
				document.newPage();
			}*
			 */
		} catch (IOException bpfe) {
			log.error("Error importing envelope file : " + bpfe.getMessage());
			log.error(AstronConverter.stackTraceToString(bpfe));
		}
		
		document.close();
		return byteArrayOutputStream.toByteArray();
	}

	protected byte[] getProposalPdf(Proposal proposal) throws DocumentException {
		/*
		 * create new document
		 */
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN + FIRST_HEADER_SIZE, BOTTOM_MARGIN);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		float firstHeaderSize = 0;
		firstHeaderSize = FIRST_HEADER_SIZE;

		// create pdf writer
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				byteArrayOutputStream);
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(firstHeader,
				header, FOOTER_POSITION, firstHeaderSize, HEADER_SIZE, this);

		pdfWriter.setPageEvent(pageEvent);
		/*
		 * this must before open, then all pages has footer. if after
		 * document.open, first page is without footer
		 */

		document.open();
		document.setMargins(LEFT_MARGIN, RIGHT_MARGIN, TOP_MARGIN,
				BOTTOM_MARGIN);

		PdfPTable firstPageTable = getDefaultTable(1);

		// addTitle(proposal, firstPageTable);
		addSemester(proposal, firstPageTable);
		addAbstract(proposal, firstPageTable);
		
		PdfPCell cell = getBorderlessCell();
		
		firstPageTable.addCell(cell);
		//addRequestedTimeInfo(proposal.getObservingRequest(), firstPageTable);
		addContactAuthor(proposal, firstPageTable);
		addApplicants(proposal, firstPageTable);
		
		document.add(firstPageTable);
		FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		if(proposal.getJustification()!=null && OptionsDelegate.allowedToDisplay(Constants.ENABLE_FIRST_JUSTIFICATION ,fieldsDefinitionType))
		{
		// page 2
		document.newPage();
		PdfPTable secondPageTable = getDefaultTable(1);
		cell = getBorderlessCell();
		secondPageTable.addCell(cell);
		cell.addElement(getParagraphDefaultTitleWithValue("General Requirements", "", ""));
		secondPageTable.addCell(cell);
		addTechnicalJustification(proposal, secondPageTable);
		if(proposal.getJustification().getObservationStrategyText()!=null) {
			addObservationStrategy(proposal, secondPageTable);
		}
		document.add(secondPageTable);
		}

		// page 3
		document.newPage();
		PdfPTable thirdPageTable = getDefaultTable(1);
		// addObservationsSummaries(proposal, secondPageTable);

		int count = 0;
		Iterator obsIterator = proposal.getObservingRequest().getPipelines().iterator();
		while(obsIterator.hasNext()){
			//calculate the total processing time for all targets from all pipelines
			char chr = (char) (count+65);
			calculateTotalProcessingTime((Observation) obsIterator.next(), proposal, chr);
			count++;
		}	
		
		addTimeSummary(proposal, thirdPageTable);
		
		addObservingRequest(proposal.getObservingRequest(), thirdPageTable);
		
		cell = getBorderlessCell();
		cell.addElement(getParagraphDefaultTitle("Observation settings:"));
		thirdPageTable.addCell(cell);
		
		count = 0;
		obsIterator = proposal.getObservingRequest().getObservations().iterator();
		while (obsIterator.hasNext()){
			// for each observation, show instrument and target list.
			addObservationDetails((Observation) obsIterator.next(),proposal, thirdPageTable,count,false);
			count++;
		}
		
		cell = getBorderlessCell();
		cell.addElement(getParagraphDefaultTitle("Pipelines:"));
		thirdPageTable.addCell(cell);
		
		count =0;
		obsIterator = proposal.getObservingRequest().getPipelines().iterator();
		while (obsIterator.hasNext()){
			// for each observation, show instrument and target list.
			addObservationDetails((Observation) obsIterator.next(),proposal, thirdPageTable,count,true);
			count++;
		}
		
		document.add(thirdPageTable);
		
		//addContactAuthor(proposal, secondPageTable);
		// addAppendicesInfo(proposal, rootTable);
		
		document.close();
		return byteArrayOutputStream.toByteArray();
	}
	


	protected void addContactAuthor(List members, PdfPCell rootCell)
	 	throws DocumentException 
	{
		/* This method is not used for the french telescopes Contact Author
		 * should appear on thefirst page.*/
	}
	
	/**
	 * The addAdditionalIssues adds the additional issues
	 * 
	 * @param proposal
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addAdditionalIssues(Proposal proposal, PdfPTable rootTable)
                    throws DocumentException {
		if (proposal.getAdditionalIssues() == null) {
			/*
			 * make default additionalIssues
			 */
			proposal.setAdditionalIssues(new AdditionalIssues());
		}
		addTheses(proposal.getAdditionalIssues(), rootTable);
		FieldsDefinitionType fieldsDefinitionType = 
				(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		if(OptionsDelegate.allowedToDisplay(OpticonConstants.SELECT_PARENT,fieldsDefinitionType)){
			addParentProposal(proposal, rootTable);
		}
		//addLinkedProposals(proposal.getAdditionalIssues(), rootTable);
		//addLinkedProposalsElsewhere(proposal.getAdditionalIssues(), rootTable);
	}	
	
	private void addParentProposal(Proposal proposal, PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		if(proposal.getParentId() != null){
			NorthStarDelegate northStarDelegate = NorthStarDelegate.getInstance();
			Proposal parent= northStarDelegate.getProposal(proposal.getParentId());
			String title = parent.getJustification().getTitle();
			if(parent.getCode() != null){
				title= parent.getCode()+" "+title;
			}
			cell.addElement(getParagraphDefaultTitle("Parent proposal: "+title));
		}else{
			cell.addElement(getParagraphDefaultTitle("Parent proposal: No"));
		}
		rootTable.addCell(cell);
	}

	/**
	 * The getProposalPdf method generates pdf with proposal info
	 * 
	 * @param proposal
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected byte[] getAdditionalPages(Proposal proposal) throws DocumentException 
	{
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN);
		document.setMarginMirroring(true);
		/*
		 * output it to and byte array output stream
		 */
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		/*
		 * create pdf writer
		 */
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				byteArrayOutputStream);
		/*
		 * it must before open, then all pages has footer. if after
		 * document.open, first page is without footer
		 */
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(null, header,
				FOOTER_POSITION, HEADER_SIZE, HEADER_SIZE, this);

		pdfWriter.setPageEvent(pageEvent);
		document.open();

		// addFirstHeader(proposal, document);
		document.setMargins(LEFT_MARGIN, RIGHT_MARGIN, TOP_MARGIN,
				BOTTOM_MARGIN);
		/*
		 * create root table.
		 */
		PdfPTable pageTable = getDefaultTable(1);

		addAdditions(proposal, pageTable);
		if(proposal.getAdditionalIssues() != null){
			//addPreviousAllocations(proposal.getAdditionalIssues(), pageTable);
		}
		addAdditionalRemarks(proposal, pageTable);
		addProprietary(proposal,pageTable);
		if (proposal.getAdditionalIssues() == null) {
			proposal.setAdditionalIssues(new AdditionalIssues());
		}
		addAdditionalIssues(proposal, pageTable);
		addRelatedPublications(proposal.getAdditionalIssues(), pageTable);
		addRelatedPreviousInvolvedProposal(proposal.getAdditionalIssues(), pageTable);
		
		document.add(pageTable);
		

		// issue 7010
		PdfPTable targetPageTable = getDefaultTable(1);
		PdfPCell cell = getBorderlessCell();
		cell = getBorderlessCell();
		cell.addElement(getParagraphDefaultTitle("Targets:"));
		targetPageTable.addCell(cell);
		
		addTargets(proposal,targetPageTable);
		
		document.add(targetPageTable);
		// end issue 7010
		
		
	
		document.close();
		return byteArrayOutputStream.toByteArray();
	}	
	
	private void addProprietary(Proposal proposal, PdfPTable pageTable) {
		OpticonObservingRequest request = (OpticonObservingRequest) proposal.getObservingRequest();
		if(!AstronValidator.isBlankOrNull(request.getProprietaryExceptionSpecifics())){ 
			PdfPCell cell = getBorderlessCell();
			cell.addElement(getParagraphDefaultTitle("Exceptions to the 12 month proprietary period: "));
			cell.addElement(getParagraphNormal(request.getProprietaryExceptionSpecifics(), EMPTY_NOT_REQUIRED));
			pageTable.addCell(cell);
			
		}
	}
	
	protected void addAdditionalRemarks(Proposal proposal,
			PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		if (proposal.getAdditionalIssues() != null && !AstronValidator.isBlankOrNull(proposal.getAdditionalIssues().getAdditionalRemarks() )) {
			
			cell.addElement(getParagraphDefaultTitle("Additional remarks"));
			cell.addElement(getParagraphNormal( proposal.getAdditionalIssues().getAdditionalRemarks() , EMPTY_NOT_REQUIRED));

		} else {
			cell.addElement(getParagraphDefaultTitle("No additional remarks"));
		}
		rootTable.addCell(cell);

	}

	protected void addAdditions(Proposal proposal, PdfPTable rootTable) 
	{
		OpticonObservingRequest observingRequest = 
			(OpticonObservingRequest) proposal.getObservingRequest();
		FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		/* Re-purposed for LOFAR
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.ALLOCATION_JUSTIFICATION,
						fieldsDefinitionType))
		{
			PdfPCell cell = getTableCell();
			cell.setFixedHeight(200f);
			cell.addElement(getParagraphDefaultTitle("Justify the nights"));
			cell.addElement(getParagraphNormal(observingRequest
					.getAllocationJustification(), EMPTY));
			rootTable.addCell(cell);
		}
		*/
		
		/*
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.NEW_OBSERVER_EXPERIENCE,
						fieldsDefinitionType))
		{
			PdfPCell cell = getBorderlessCell();
			cell.addElement(getParagraphDefaultTitle("Observing experience of first time users"));
			cell.addElement(getParagraphNormal(observingRequest.getNewObserverExperience(), EMPTY_NOT_REQUIRED));
			rootTable.addCell(cell);
		}
		*/		
		if (OptionsDelegate.allowedToDisplay(
				OpticonConstants.GRANT_NUMBER, fieldsDefinitionType))
		{
			PdfPCell cell = getBorderlessCell();
	        PdfPTable table = getTableNoSplit(2);			
	        PdfPCell tableCell = getOpticonTableTitleTextCell("Current associate SFTC Grant number : ", false);
	        table.addCell(tableCell);
	        tableCell = getOpticonTableTextCell(observingRequest.getGrantNumber(), false);
	        table.addCell(tableCell);
	        cell.addElement(table);
	        rootTable.addCell(cell);
		}		
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TRAVEL, fieldsDefinitionType))
		{
			PdfPCell cell = getBorderlessCell();
			cell.addElement(getParagraphDefaultTitle("Non-standard travel and subsistence"));
			cell.addElement(getParagraphNormal(observingRequest.getTravel(), EMPTY_NOT_REQUIRED));
			rootTable.addCell(cell);
		}	
		/*
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.BACKUPSTRATEGY , fieldsDefinitionType))
		{
			// check nr of lines
			String paragraphText = observingRequest.getBackupStrategy();
			if (paragraphText != null && paragraphText.length() > 2){
				String[] paragraphlines = paragraphText.split("\n");

				int paragraphLength = paragraphlines.length;
				for (int i=0;i < paragraphlines.length ; i++ ){
					paragraphLength += Math.floor(paragraphlines[i].length()/80);
				}
			
				// TextArea paragraphText = new TextArea(observingRequest.getBackupStrategy(),80,  ;
				
				PdfPCell cell =getBorderlessCell();
				cell.addElement(getParagraphDefaultTitle("Backup strategy for poor weather conditions"));
	
				if(paragraphLength > 15 ){
					cell.addElement(getParagraph(paragraphText, "error", REQUIRED_FONT, REQUIRED_FONT,
							NORMAL_PARAGRAPH_INDENTATION_LEFT,
							NORMAL_PARAGRAPH_INDENTATION_RIGHT));
				}
				else{
					cell.addElement(getParagraphNormal(paragraphText , EMPTY_NOT_REQUIRED));
				}
				rootTable.addCell(cell);
			}
		}
		*/	
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.OTHER_EXPENDITURE, fieldsDefinitionType))
		{
			PdfPCell cell = getBorderlessCell();
			cell.addElement(getParagraphDefaultTitle("Any other expenditure"));
			cell.addElement(getParagraphNormal(observingRequest.getOtherExpenditure(), EMPTY_NOT_REQUIRED));
			rootTable.addCell(cell);
		}				
	}	
	
	
	
	public void fillHeaderText(Proposal proposal, PdfPTable headerTable) 
	{
		PdfPCell cell = getBorderlessCell();
		/* Community contains all the French TAC categories */
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) proposal.getObservingRequest();
		if (opticonObservingRequest.isLargeProposal()) 
		{
			cell.addElement(
				this.getParagraph("LARGE", EMPTY,HEADER_NORMAL_FONT, HEADER_REQUIRED_FONT, 0, 0, Paragraph.ALIGN_LEFT));
		}		
		headerTable.addCell(cell);
		// below is needed otherwise the logo will be small and next to the label above
		cell = getBorderlessCell();
		headerTable.addCell(cell);
		
	}
	
	protected void addObservationStrategy(Proposal proposal, PdfPTable rootTable) 
	{
		PdfPCell cell = getTableCell();
		// cell.setFixedHeight(200f);
		cell.setMinimumHeight(150f);
		cell.addElement(getParagraphDefaultTitle("Observation strategy"));
		String observationStrategyText = proposal.getJustification().getObservationStrategyText();
		cell.addElement(getParagraphNormal(observationStrategyText, EMPTY));
		rootTable.addCell(cell);
	}

	
	protected void addTechnicalJustification(Proposal proposal,
			PdfPTable rootTable) {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
    	ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
    	List technicalQuestions = OptionsDelegate.getOptionsConfigurationTypes(
        		Constants.DISPLAY_TECH_JUSTIFICATIONS, new HashMap(), contextConfiguration);
    	if (technicalQuestions != null && !technicalQuestions.isEmpty()) {
    		ConfigureOptionType optionFieldConfiguration =(ConfigureOptionType) technicalQuestions.get(0);
    		ConfigureOptionType techOptions = OptionsDelegate.getOption(optionFieldConfiguration.getValue(), technicalQuestions);
    		FieldsDefinitionType questions = techOptions.getSubfields();
    		
    		PdfPCell cell = getTableCell();
    		String text = null;
    		cell = getTableCell();
    		// cell.setFixedHeight(200f);
    		//cell.setMinimumHeight(150f);
    		cell.addElement(getParagraphDefaultTitleWithValue("Night Time : ",proposal.getJustification().isNighttime() ? "YES" :"NO", "NO"));
    		if(proposal.getJustification().isNighttime())
    		{
    		text = proposal.getJustification().getNightTimeReason();
    		cell.addElement(getParagraphNormal(text, EMPTY));
    		}
    		rootTable.addCell(cell);

    		 cell = getTableCell();
    		// cell.setFixedHeight(200f);
    		//cell.setMinimumHeight(150f);
    		cell.addElement(getParagraphDefaultTitleWithValue("Parallel observations planned with other observing facilities: ", proposal.getJustification().isParallelObservation() ?  "YES" :"NO", "NO"));
    		if(proposal.getJustification().isParallelObservation())
    		{
    		text =proposal.getJustification().getParallelObservationReason();
    		cell.addElement(getParagraphNormal(text, EMPTY));
    		}
    		rootTable.addCell(cell);
    		
    		cell = getTableCell();
    		// cell.setFixedHeight(200f);
    		//cell.setMinimumHeight(150f);
    		cell.addElement(getParagraphDefaultTitleWithValue("International Stations: ", proposal.getJustification().isInternationalStation()?  "YES" :"NO", "NO"));
    		if(proposal.getJustification().isInternationalStation())
    		{
    		if(proposal.getJustification().isInternationalStation())
    		   text = proposal.getJustification().getInternationalStationEssentialReason();
    		else
    			text = "Not Essential";
    		cell.addElement(getParagraphNormal(text, "Not Essential"));
    		}
    		rootTable.addCell(cell);
    			
    		
    		cell = getTableCell();
    		// cell.setFixedHeight(200f);
    		//cell.setMinimumHeight(150f);
    		cell.addElement(getParagraphDefaultTitleWithValue("Other scheduling constraints: ",proposal.getJustification().isOtherSchedulingConstraints() ?  "YES" :"NO", "NO"));
    		if(proposal.getJustification().isOtherSchedulingConstraints())
    		{
    		text =proposal.getJustification().getOtherSchedulingConstraintsReason();
    		cell.addElement(getParagraphNormal(text, EMPTY));
    		}
    		rootTable.addCell(cell);
    		
    		
    		cell = getTableCell();
    		// cell.setFixedHeight(200f);
    		//cell.setMinimumHeight(150f);
    		cell.addElement(getParagraphDefaultTitleWithValue("Combined data product request: ",proposal.getJustification().isCombinedDataProductRequest() ?  "YES" :"NO", "NO"));
    		if(proposal.getJustification().isCombinedDataProductRequest())
    		{
    		text =proposal.getJustification().getCombinedDataProductRequestReason();
    		cell.addElement(getParagraphNormal(text, EMPTY));
    		}
    		rootTable.addCell(cell);
    		
    		
    		cell = getTableCell();
    		// cell.setFixedHeight(200f);
    		//cell.setMinimumHeight(150f);
    		cell.addElement(getParagraphDefaultTitleWithValue("Sensitivity (mJy): ",proposal.getJustification().getSensitivity()!=null? String.valueOf(proposal.getJustification().getSensitivity()):"NO","NO"));
    		rootTable.addCell(cell);
    		
    		
    		
    		cell = getTableCell();
    		// cell.setFixedHeight(200f);
    		//cell.setMinimumHeight(150f);
    		cell.addElement(getParagraphDefaultTitleWithValue("Maximum data rate (GB/sec): ",proposal.getJustification().getMaxDataRate()!=null ? String.valueOf(proposal.getJustification().getMaxDataRate()) :"NO", "NO"));
    		rootTable.addCell(cell);
    		
    		
    		
    		
    		// cell.setFixedHeight(200f);
    		//cell.setMinimumHeight(150f);
    		 
    		if (	OptionsDelegate.allowedToDisplay(Constants.DISPLAY_TECH_RO_PROCESSING, questions) || 
    				OptionsDelegate.allowedToDisplay(Constants.DISPLAY_TECH_RO_PROCESSING_WITHOUT_SELFCAL, questions)) {
    			cell = getTableCell();
    			cell.addElement(getParagraphDefaultTitleWithValue("Processing offered by ASTRON: ", proposal.getJustification().isRoProcessing()?  "YES" :"NO", "NO"));
    			if(OptionsDelegate.allowedToDisplay(Constants.DISPLAY_TECH_RO_PROCESSING, questions) && proposal.getJustification().isRoProcessing()) {
    				if(proposal.getJustification().isDefaultROProcessing()) {
    					 text = proposal.getJustification().getRoProcessingReason();
    				} else {
    					text = " 'default' imaging pipeline offered by the RO Not Requested";
    				}
    				cell.addElement(getParagraphNormal(text, EMPTY));
    			}
    			rootTable.addCell(cell);
    		}

    		cell = getTableCell();
    		cell.addElement(getParagraphDefaultTitleWithValue("Storage in the LTA of raw data products: ", proposal.getJustification().isLtaRawStorage()?  "YES" :"NO", "NO"));
    		if(proposal.getJustification().isLtaRawStorage())
    		{
    		text =proposal.getJustification().getLtaRawStorageReason();
    		cell.addElement(getParagraphNormal(text, EMPTY));
    		}
    		rootTable.addCell(cell);

    		cell = getTableCell();
    		cell.addElement(getParagraphDefaultTitleWithValue("Storage requested for specific LTA site: ", proposal.getJustification().isLtaStorage()?  "YES" :"NO", "NO"));
    		if((proposal.getJustification().getLtaStorageLocation() != null) && (proposal.getJustification().getLtaStorageLocation().length() != 0)) {
    			cell.addElement(getParagraphDefaultTitleWithValue("The selected LTA storage location is: ", proposal.getJustification().getLtaStorageLocation(), ""));
    			
    		}
    		if(proposal.getJustification().isLtaStorage())
    		{
    		text =proposal.getJustification().getLtaStorageReason();
    		cell.addElement(getParagraphNormal(text, EMPTY));
    		}
    		rootTable.addCell(cell);
    	
    		
    	
    		cell = getTableCell();
    		cell.addElement(getParagraphDefaultTitleWithValue("Off-line data processing on ASTRON facilities (CEP3): ",proposal.getJustification().isOfflineROProcessing() ?  "YES" :"NO", "NO"));
    		if(proposal.getJustification().isOfflineROProcessing())
    		{
    		text =proposal.getJustification().getOfflineROProcessingReason();
    		cell.addElement(getParagraphNormal(text, EMPTY));
    		}
    		rootTable.addCell(cell);
    		
    		cell = getTableCell();
    		// cell.setFixedHeight(200f);
    		//cell.setMinimumHeight(150f);
    		cell.addElement(getParagraphDefaultTitleWithValue("External processing facilities: ",proposal.getJustification().isExternalProcessing() ?  "YES" :"NO", "NO"));
    		if(proposal.getJustification().isExternalProcessing())
    		{
    		text =proposal.getJustification().getExternalProcessingReason();
    		cell.addElement(getParagraphNormal(text, EMPTY));
    		}
    		rootTable.addCell(cell);
    		
    		cell = getTableCell();
    		cell.addElement(getParagraphDefaultTitleWithValue("Different CEP4 processing time: ",proposal.getJustification().isCepRequesting() ?  "YES" :"NO", "NO"));
    		if(proposal.getJustification().isCepRequesting())
    		{
    		text =proposal.getJustification().getCepRequestingReason();
    		cell.addElement(getParagraphNormal(text, EMPTY));
    		}
    		rootTable.addCell(cell);

    		if (OptionsDelegate.allowedToDisplay(Constants.DISPLAY_TECH_FILLER_TIME, questions)) {
    			cell = getTableCell();
    			cell.addElement(getParagraphDefaultTitleWithValue("Is user-shared support requested: ",
    					proposal.getJustification().isFillerTime() ? "YES" : "NO", "NO"));
    			if (proposal.getJustification().isFillerTime()) {
    				text = proposal.getJustification().getFillerTimeReason();
    				cell.addElement(getParagraphNormal(text, EMPTY));
    			}
    			rootTable.addCell(cell);
    		}

    		if (OptionsDelegate.allowedToDisplay(Constants.DISPLAY_TECH_COOBSERVATION_TEAM, questions)) {
    			cell = getTableCell();
    			cell.addElement(getParagraphDefaultTitleWithValue("Is co-observation requested: ",proposal.getJustification().isCoObservationTeam() ?  "YES" :"NO", "NO"));
    			rootTable.addCell(cell);
    		}

    	}
	}
	
	/*
	 *  Overwriting the super class. It needs a border and fixed size .
	 */
	protected void addAbstract(Proposal proposal, PdfPTable rootTable) 
	{
		PdfPCell cell = getTableCell();
		// cell.setFixedHeight(200f);
		cell.setMinimumHeight(150f);
		cell.addElement(getParagraphDefaultTitle("Abstract"));
		String abstractText = null;
		if (proposal.getJustification() != null) {
			abstractText = proposal.getJustification().getAbstractText();

		}
		cell.addElement(getParagraphNormal(abstractText, EMPTY));


		rootTable.addCell(cell);
	}
	
	protected void addSemester(Proposal proposal, PdfPTable rootTable) 
	{
		PdfPCell cell = getBorderlessCell();
        PdfPTable table = getTableNoSplit(3);

        PdfPCell tableCell = null;
        tableCell = getOpticonTableTitleTextCell("Semester : "+proposal.getSemester().getSemester(), false);
        table.addCell(tableCell);
		// cell.addElement(getParagraphDefaultTitle("Semester"));
        
        /*if (proposal.getCode() != null)
        {
            tableCell = getOpticonTableTitleTextCell("Reference : "+proposal.getCode(), false);
        }
        else
        {
        	tableCell = getOpticonTableTitleTextCell("Reference : N.A. ", false);
        }
           table.addCell(tableCell);
         */
        table.addCell(getOpticonTableTextCell(null,false));
        
		if (proposal.getJustification() != null) {
			
			OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) proposal.getObservingRequest();
			
			if (opticonObservingRequest.getCategory() != null)
			{
				tableCell = getOpticonTableTitleTextCell("Science Cat. : "+opticonObservingRequest.getCategory(), false);
			}
			else
			{
				TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
				contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
				FieldsDefinitionType fieldsDefinitionType = 
					(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
				
				if(OptionsDelegate.allowedToDisplay(OpticonConstants.SCIENCE_CATEGORY ,
						fieldsDefinitionType)){
					tableCell = getOpticonTableTitleTextCell("Science Cat. : N.A. ",false);
				}else{
					tableCell = getOpticonTableTitleTextCell(" ",false);
				}
			}
	        table.addCell(tableCell);
		}
		cell.addElement(table);
		rootTable.addCell(cell);
	}

	public void calculateTotalProcessingTime (Observation observation, Proposal proposal, char chr) {
		
		OpticonObservation opticonObservation = (OpticonObservation) observation;
		double totalTime = 0;
        String mode = "";
        HashMap processTimeResult =null;

        if(opticonObservation.getInstrument()!=null){
        	mode = opticonObservation.getInstrument().getProcessingMode();
        }

        List uvrangeList = new ArrayList();
		String uvRangeValue = new String();
		uvrangeList = OptionsUtils.getLabelValueBeans(Constants.UV_RANGE_IMAGING, new HashMap(), contextConfiguration);
		
		if (uvrangeList != null && uvrangeList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) uvrangeList.get(1);
			uvRangeValue =  urlValue.getValue(); 
		}
		double uvRange = 0;
		if (!AstronValidator.isBlankOrNull(uvRangeValue)) {
			uvRange = new Double(uvRangeValue).doubleValue();
		}
		 
		
		String baselineValue = new String();
		uvrangeList = OptionsUtils.getLabelValueBeans(OpticonConstants.PROCESSING_TIME_BASELINE_FACTOR, new HashMap(), contextConfiguration);
		
		if (uvrangeList != null && uvrangeList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) uvrangeList.get(1);
			baselineValue =  urlValue.getValue(); 
		}
		double baselineFactor =1d;
		if (!AstronValidator.isBlankOrNull(baselineValue)) {
			baselineFactor = new Double(baselineValue).doubleValue();
		}
		
        int totalStation =0;
        for(Target target : proposal.getObservingRequest().getTargets()){
			OpticonTarget optarget = (OpticonTarget) target;
			if(optarget != null && 
			   optarget.getObservingrun() != null && 
			   optarget.getPipeline() != null && optarget.getPipeline().equalsIgnoreCase(String.valueOf(chr))){

				int thisTargetSubband = SetUpInstrumentBean.convertSubbandListDescription(optarget.getSubbandList());
			
				Double targetDuration = NorthStarDelegate.getResourceValue(
						optarget.getAllocations(), OpticonConstants.TOTAL_DURATION);
				
				 processTimeResult = calculateProcessingTime(
							mode,
							opticonObservation.getInstrument()
									.getFlaggingStrategy(),
							opticonObservation.getInstrument().getAntenna() == null ? "": opticonObservation.getInstrument().getAntenna(),
							opticonObservation.getInstrument().isDemixing(),
							opticonObservation.getInstrument().getDemixingSources(),
							totalStation, thisTargetSubband,
							targetDuration!=null?targetDuration.doubleValue():0,
							opticonObservation.getInstrument().getFieldOfView()!=null?opticonObservation.getInstrument().getFieldOfView().doubleValue():0
									, uvRange,baselineFactor, opticonObservation.getInstrument().getProcessingMode());

					String totalprocessingtimeValue =String.valueOf(processTimeResult.get(OpticonConstants.PROCESSING_TIME_TOTAL));
					totalTime+=Double.parseDouble(totalprocessingtimeValue.replace(",",""));
			}
		}
        this.totalProcessingTime += totalTime;

		if(processTimeResult!=null)
			processTimeResult.put(OpticonConstants.PROCESSING_TIME_TOTAL, (int)totalProcessingTime);
	}
	
	protected void addTimeSummary(Proposal proposal, PdfPTable rootTable) 
	{
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) proposal.getObservingRequest();
        //double totalExpTime =0;
        // count totals:
		boolean enableBright=false;
		boolean enableGrey=false;
		boolean enableDark=false;
		boolean enableGlobal=false;
		boolean enableLongtermStorage=false;

        double totalBrightTime=0;
        double totalGreyTime=0;
        double totalDarkTime=0;
        double totalGlobalTime=0;
        
        double reqBrightTime=0;
        double reqGreyTime=0;
        double reqDarkTime=0;
        double reqGlobalTime=0;
        double processingTime =0; 
        double usefulBrightTime=0;
        double usefulGreyTime=0;
        double usefulDarkTime=0;
        double usefulGlobalTime=0;
        double totalProcessingTimeSpec =0; 
        double longTermStorage=0;
        
        Set telescopes = new HashSet();
        PdfPCell tableCell = null;
        
        List enabledTimes = OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.OBSERVATION_TIME, new HashMap(), contextConfiguration);
    	Iterator observationTimesIt = enabledTimes.iterator();
    	while (observationTimesIt.hasNext()){
    		LabelValueBean lvb = (LabelValueBean) observationTimesIt.next();
    		if(lvb.getValue().startsWith("Global")){
    			enableGlobal=true;
    		}
    		if(lvb.getValue().startsWith("Dark")){
    			enableDark=true;
    		}
    		if(lvb.getValue().startsWith("Grey")){
    			enableGrey=true;
    		}
    		if(lvb.getValue().startsWith("Bright")){
    			enableBright=true;
    		}
    	}
    	
           	
        Iterator targetsIt = opticonObservingRequest.getTargets().iterator();
    	while (targetsIt.hasNext())
    	{
    		OpticonTarget opticonTarget = (OpticonTarget) targetsIt.next();
    		if(opticonTarget.getAllocations() != null){
    			
    		
	    		Double exposureTime = NorthStarDelegate.getResourceValue(
						opticonTarget.getAllocations(),	OpticonConstants.BRIGHT);
	    		if (exposureTime != null){
	    			totalBrightTime += exposureTime.doubleValue();
	    		}
	    		exposureTime = NorthStarDelegate.getResourceValue(
						opticonTarget.getAllocations(),	OpticonConstants.GREY);
	    		if (exposureTime != null){
	    			totalGreyTime += exposureTime.doubleValue();
	    		}
	    		exposureTime = NorthStarDelegate.getResourceValue(
						opticonTarget.getAllocations(),	OpticonConstants.DARK);
	    		if (exposureTime != null){
	    			totalDarkTime += exposureTime.doubleValue();
	    		}
	    		exposureTime = NorthStarDelegate.getResourceValue(
						opticonTarget.getAllocations(),	OpticonConstants.GLOBAL);
	    		/*if (exposureTime != null && opticonTarget.isFirstInRun()){
	    			totalGlobalTime += exposureTime.doubleValue();
	    		}*/
	    		exposureTime = NorthStarDelegate.getResourceValue(
						opticonTarget.getAllocations(),	OpticonConstants.TOTAL_DURATION);
	    		if (exposureTime != null && opticonTarget.isFirstInRun()){
	    			totalGlobalTime += exposureTime.doubleValue();
	    		}
    		}
    	}
    	
	    Iterator observationsIt = opticonObservingRequest.getObservations().iterator();
     	while (observationsIt.hasNext())
     	{
	     		OpticonObservation opticonObservation = (OpticonObservation) observationsIt.next();

	    	telescopes.add(opticonObservation.getTelescopeConfiguration());
    	}
     	
     	/*
     	 *   Here is some lofar specific code to allow storage without any observations
     	 */
     	if(telescopes.size()==0){
     		Double globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                 "LOFAR_" + OpticonConstants.GLOBAL);
	    	if (globalTime != null){
	    		reqGlobalTime += globalTime.doubleValue();
	    	}
	    	
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	    			"LOFAR_" + OpticonConstants.LONGTERMSTORAGE );
	    	if (globalTime != null){
	    		longTermStorage += globalTime.doubleValue();
	    	}
	    	
     	}
    	
    	Iterator telescopeIt = new ArrayList(telescopes).iterator();
		while (telescopeIt.hasNext())
		{
			String telescopeName = (String) telescopeIt.next();
			
	    	Double globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.BRIGHT);
	    	
	    	if (globalTime != null){
	    		reqBrightTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.GREY);
	    	if (globalTime != null){
	    		reqGreyTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.DARK);
	    	if (globalTime != null){
	    		reqDarkTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.GLOBAL);
	    	if (globalTime != null){
	    		reqGlobalTime += globalTime.doubleValue();
	    	}
	    	
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.USEFULBRIGHT);
	    	if (globalTime != null){
	    		usefulBrightTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.USEFULGREY);
	    	if (globalTime != null){
	    		usefulGreyTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.USEFULDARK);
	    	if (globalTime != null){
	    		usefulDarkTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.USEFULGLOBAL);
	    	if (globalTime != null){
	    		usefulGlobalTime += globalTime.doubleValue();
	    	}
	    	
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.LONGTERMSTORAGE );
	    	if (globalTime != null){
	    		longTermStorage += globalTime.doubleValue();
	    	}
	    
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.TOTAL_PROCESSING_TIME );
	    	if (globalTime != null){
	    		processingTime += globalTime.doubleValue();
	    	}
	/*  kan weg
	    	totalAwardedTime += NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.AWARDED ).doubleValue();
	
	    	totalLongTime += NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.LONGTERM).doubleValue();
	*/
		}
		
		int tableCount=0;	
		if(enableBright) tableCount++;
		if(enableDark) tableCount++;
		if(enableGrey) tableCount++;
		if(enableGlobal) tableCount++;
    	
		if (contextConfiguration != null && tableCount > 0){
			FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
			
			PdfPCell cell = getBorderlessCell();
	        PdfPTable table = getTableNoSplit(tableCount);
	        if(enableBright){
	        	tableCell = getOpticonTableTitleTextCell("Bright: ",false);
	        	table.addCell(tableCell);
	        }
	        if(enableGrey){
	        	tableCell = getOpticonTableTitleTextCell("Grey: ",false);
	        	table.addCell(tableCell);
	        }
	        if(enableDark){
	        	tableCell = getOpticonTableTitleTextCell("Dark: ",false);
	        	table.addCell(tableCell);	
	        }
	        if(enableGlobal){
	        	tableCell = getOpticonTableTitleTextCell("Totals: ",false);
	        	table.addCell(tableCell);
	        }
			cell.addElement(table);
			rootTable.addCell(cell);
			
			
			cell = getBorderlessCell();
	        table = getTableNoSplit(tableCount);
	        if(enableBright){
	        	tableCell = getOpticonTableTitleTextCell("Exposure time: "+
	        		AstronConverter.getHoursFromSeconds(new Double(totalBrightTime))+"h " , false);
	        	table.addCell(tableCell);
	        }
	        if(enableGrey){
	        	tableCell = getOpticonTableTitleTextCell("Exposure time: "+
	        		AstronConverter.getHoursFromSeconds(new Double(totalGreyTime))+"h " , false);
	        	table.addCell(tableCell);
	        }
	        if(enableDark){
	        	tableCell = getOpticonTableTitleTextCell("Exposure time: "+
	        		AstronConverter.getHoursFromSeconds(new Double(totalDarkTime))+"h " , false);
	        	table.addCell(tableCell);
	        }
	        if(enableGlobal){
		        tableCell = getOpticonTableTitleTextCell("Exposure time: "+
		        		OpticonTargetUtils.getHoursFromSeconds(new Double(totalGlobalTime))+"h " , false);
		        table.addCell(tableCell);
		    }
	        cell.addElement(table);
			//rootTable.addCell(cell);
	        //AstronConverter.getMinutesFromSeconds(opticonObservation.getTotalObservationDuration())));
			// cell.addElement(getParagraphDefaultTitle("Semester"));
			//cell = getBorderlessCell();
	       table = getTableNoSplit(tableCount);
		
	       if(enableBright){
	    	   tableCell = getOpticonTableTextCell("Hours requested: "+ reqBrightTime+"h ", false);
	    	   table.addCell(tableCell);
	       }
	       if(enableGrey){
	        	tableCell = getOpticonTableTextCell("Hours requested: "+reqGreyTime+"h ", false);
	        	table.addCell(tableCell);
	       }
	       if(enableDark){
	    	   tableCell = getOpticonTableTextCell("Hours requested: "+reqDarkTime+"h ", false);
	    	   table.addCell(tableCell);
	       }
	       if(enableGlobal){
	    	   tableCell = getOpticonTableTextCell("Hours requested: "+ reqGlobalTime+"h ", false);
	    	   table.addCell(tableCell);
	       }
	       cell.addElement(table);
			//rootTable.addCell(cell);
		
		// display useful time only if is enable in the options file
			if (OptionsDelegate.allowedToDisplay(OpticonConstants.USEFUL_TIME,fieldsDefinitionType)){
			//	cell = getBorderlessCell();
		        table = getTableNoSplit(tableCount);
			
		       if(enableBright){
		        tableCell = getOpticonTableTextCell("Minimal useful requested: "+
		        		 usefulBrightTime+"h ", false);
		        table.addCell(tableCell);
		       }
		       if(enableGrey){		        
				tableCell = getOpticonTableTextCell("Minimal useful requested: "+
						 usefulGreyTime+"h ", false);
				table.addCell(tableCell);
		       }
		       if(enableDark){
				tableCell = getOpticonTableTextCell("Minimal useful requested: "+
						 usefulDarkTime+"h ", false);
				table.addCell(tableCell);
		       }
		       if(enableGlobal){
		    	   tableCell = getOpticonTableTextCell("Minimal useful requested: "+
						usefulGlobalTime+"h ", false);
		    	   table.addCell(tableCell);
		       }
			
		       if (OptionsDelegate.allowedToDisplay(OpticonConstants.ALLOCATION_JUSTIFICATION,
		    		   fieldsDefinitionType))
		       {
		    	   tableCell = getOpticonTableTextCell("Justification for minimal useful time:", false);
		    	   table.addCell(tableCell);
		    	   
		    	   tableCell = getTableCell();
		    	   tableCell.setFixedHeight(200f);
		    	   tableCell.addElement(getParagraphNormal(opticonObservingRequest
		    			   .getAllocationJustification(), EMPTY));
		    	   table.addCell(tableCell);
		       }

		       cell.addElement(table);
		       
			}
			
			if(OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_LONGTERM_STORAGE,
					fieldsDefinitionType)){
				
				
				double storage = calculateTotalStorage(opticonObservingRequest);
				BigDecimal bd = new BigDecimal(storage);
				bd = bd.setScale(2,BigDecimal.ROUND_UP);
				bd=OpticonTargetUtils.convertToTB(bd);
				
				
				tableCell = getOpticonTableTitleTextCell("Calculated storage: "+
						String.valueOf(bd.doubleValue()) +StorageConverter.UNIT_TERABYTE  , false);
		        table.addCell(tableCell);
		        
				
				table= getTableNoSplit(1);
				tableCell = getOpticonTableTextCell("Longterm storage requested: "+
						 longTermStorage+StorageConverter.UNIT_TERABYTE, false);
		    	   table.addCell(tableCell);
				cell.addElement(table);
			}
			
			if(OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_PROCESSING_TIME,
					fieldsDefinitionType)){
				
				totalProcessingTime = (double) Math.round(totalProcessingTime * 100.0) / 100.0;
				tableCell = getOpticonTableTitleTextCell("Calculated Processing time:"+
						String.valueOf(totalProcessingTime)+"h " , false);
		        table.addCell(tableCell);
		        
		        table= getTableNoSplit(1);
		        tableCell = getOpticonTableTextCell("Processing time requested: "+
		        		String.valueOf(processingTime)+"h ", false);
		    	   table.addCell(tableCell);
				
				cell.addElement(table);
			}
			
//			if(OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_POSTPROCESSING,
//					fieldsDefinitionType)){
//				double cepLoad = 0.0;
//				StorageCalculator sCalc = SetUpInstrumentBean.bindCalc(contextConfiguration);
//				if(sCalc != null){
//					try {
//						Iterator obsit = opticonObservingRequest.getObservations().iterator();
//						
//						while (obsit.hasNext()){
//						  OpticonObservation opticonObservation = (OpticonObservation) obsit.next();
//						  double dataSize = SetUpInstrumentBean.calculateStorage(opticonObservation, sCalc);
//						  cepLoad = cepLoad + SetUpInstrumentBean.calculateCepLoad(opticonObservation,contextConfiguration,dataSize);
//						}
//					}catch(ServiceException e){
//						log.error(e.getMessage() );
//					}
//				}else{
//					log.warn("could not bind to storageCalculator" );
//				}
//				
//				BigDecimal bd = new BigDecimal(cepLoad);
//			    bd = bd.setScale(0,BigDecimal.ROUND_UP);
//				table= getTableNoSplit(1);
//				tableCell = getOpticonTableTextCell("Estimated CEP load: "+
//						bd.toString()+" PU ", false);
//		    	   table.addCell(tableCell);
//				cell.addElement(table);
//			}
			
		if(OptionsDelegate.allowedToDisplay(OpticonConstants.NOISE_LEVEL,
					fieldsDefinitionType)){
				table= getTableNoSplit(1);
				tableCell = getOpticonTableTextCell("Required noise Level: " +
						opticonObservingRequest.getNoiseLevel().toString() + " J", false);
				table.addCell(tableCell);
				cell.addElement(table);
			}
		rootTable.addCell(cell);
			
		}
	}

	/**
     * Generates the additional information the observing request has next to the observations 
     * and the allocation times.
     *  @see eu.radionet.northstar.business.pdf.ProposalWriter#addObservingRequest(eu.radionet.northstar.data.entities.ObservingRequest,
     *      com.lowagie.text.pdf.PdfPTable)
     */
    protected void addObservingRequest(ObservingRequest observingRequest,
            PdfPTable rootTable) throws DocumentException {
        OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
        // if enabled
        if (contextConfiguration != null) // it happens
        {
        	addLongTermProposal(opticonObservingRequest, rootTable);
        	FieldsDefinitionType fieldsDefinitionType = 
				(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.REQUIRED_SCHEDULING_CONSTRAINTS ,
					fieldsDefinitionType)){
	        	addOverallSchedulingRequirements(opticonObservingRequest, rootTable);
	        }
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.PREFERRED_SCHEDULING_CONSTRAINTS ,
						fieldsDefinitionType)){	
	        	addOverallSchedulingPreferences(opticonObservingRequest, rootTable);
	        }
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_OBSERVING_DETAILS ,
					fieldsDefinitionType)){		
	        	addSchedulingDetails(opticonObservingRequest, rootTable);
	        }
	        
	        
        }
    }
    
  
    /**
     * Generates the total time of all the specified observations in the pdf
     * @see eu.radionet.northstar.business.pdf.ProposalWriter#addTotalRequestedTime(eu.radionet.northstar.data.entities.ObservingRequest, com.lowagie.text.pdf.PdfPTable)
     */
    protected void addTotalRequestedTime(ObservingRequest observingRequest,
            PdfPTable rootTable) 
    {
        OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
        // addScienceCategory(opticonObservingRequest, rootTable);
        
        PdfPCell cell = getBorderlessCell();
        //PdfPCell cell = getTableCell();
        //float spacing = 10;
        //cell.addElement(getSpacingParagraph(spacing));

        //OpticonProposalDelegate proposalDelegate = null;
		try
		{
			/*
			proposalDelegate = new OpticonProposalDelegate();
			Double nights = proposalDelegate.getTotalTime(opticonObservingRequest);
			if (nights != null)
			{
		        cell.addElement(getParagraphDefaultTitle("Total requested nights: " + AstronConverter.toString(nights) + " nights"));
				addTelescopeDetails(opticonObservingRequest, cell);
				
			}
			else
			{
				Paragraph p = getParagraphDefaultTitle("Total requested nights: ");
				p.add(new Phrase("EMPTY", TITLE_REQUIRED_FONT));
		        cell.addElement(p);
		        PdfPTable detailsTable = getTableNoSplit(1);
		        addSchedulingDetails(observingRequest, detailsTable); 
		        cell.addElement(detailsTable);
			}
			*/
			addTelescopeDetails(opticonObservingRequest, cell);
		}/* linked with proposaldelegate
		catch (ConnectionException e)
		{
			log.error(e.getStackTrace());
		}*/
		catch (DocumentException e)
		{
			log.error(e.getStackTrace());
		}		        		
        rootTable.addCell(cell);
        PdfPCell caCell = getBorderlessCell();
        try
		{
			super.addContactAuthor(applicants, caCell);
		}
		catch (DocumentException e)
		{
			log.error(e.getStackTrace());
		}
        rootTable.addCell(caCell);
        addPi(applicants, rootTable);

    }    

    
    protected void addPi(List members, PdfPTable rootTable)
	{
		PdfPCell cell = getBorderlessCell();
    	Member ca = getContactAuthor(members);
    	Member pi = getPi(members);
    	cell.addElement(getParagraphDefaultSubTitle("Pi"));
    	
    	if (pi.getId().intValue() == ca.getId().intValue())
    	{
    		cell.addElement(getParagraphNormal("is the same as the contact author", EMPTY));
    	}
    	else 
    	{
    		cell.addElement(getParagraphNormal(pi.getName() + " - " + pi.getAffiliation() + ", " + pi.getCountry(), EMPTY));
    	}
		rootTable.addCell(cell);
	}

    
    /**
     * The addLongTermProposal adds the long term proposal
     * 
     * @param opticonObservingRequest
     * @param rootTable
     * @throws DocumentException
     */
    protected void addLongTermProposal(
			OpticonObservingRequest opticonObservingRequest, PdfPTable rootTable)
	{
		PdfPCell cell = getBorderlessCell();
		/*
		 * if long term proposal
		 */
		if (opticonObservingRequest.isLongTermProposal())
		{
			// calculate total awarded times
			Set telescopes = new HashSet();
			double totalAwardedTime=0;
	        double totalLongTime=0;
	        
	        double totalAwardedDarkTime=0;
	        double totalLongDarkTime=0;
	        double totalAwardedGreyTime=0;
	        double totalLongGreyTime=0;
	        double totalAwardedBrightTime=0;
	        double totalLongBrightTime=0;
	        
			Iterator observationsIt = opticonObservingRequest.getObservations().iterator();
	    	while (observationsIt.hasNext())
	    	{
	    		OpticonObservation opticonObservation = (OpticonObservation) observationsIt.next();
	    		telescopes.add(opticonObservation.getTelescopeConfiguration());
	    	}
	    	Iterator telescopeIt = new ArrayList(telescopes).iterator();
			while (telescopeIt.hasNext())
			{
				String telescopeName = (String) telescopeIt.next();
				Double awardedTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
		                telescopeName + '_' + OpticonConstants.AWARDED );
				if (awardedTime != null){
					totalAwardedTime += awardedTime.doubleValue();
				}
				Double longTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
		                telescopeName + '_' + OpticonConstants.LONGTERM);
				if (longTime != null){
					totalLongTime += longTime.doubleValue();
				}
				
				Double awardedDarkTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
		                telescopeName + '_' + OpticonConstants.AWARDEDDARK );
				if (awardedDarkTime != null){
					totalAwardedDarkTime += awardedDarkTime.doubleValue();
				}
				Double longDarkTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
		                telescopeName + '_' + OpticonConstants.LONGTERMDARK);
				if (longDarkTime != null){
					totalLongDarkTime += longDarkTime.doubleValue();
				}
				Double awardedGreyTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
		                telescopeName + '_' + OpticonConstants.AWARDEDGREY );
				if (awardedGreyTime != null){
					totalAwardedGreyTime += awardedGreyTime.doubleValue();
				}
				Double longGreyTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
		                telescopeName + '_' + OpticonConstants.LONGTERMGREY);
				if (longGreyTime != null){
					totalLongGreyTime += longGreyTime.doubleValue();
				}
				Double awardedBrightTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
		                telescopeName + '_' + OpticonConstants.AWARDEDBRIGHT );
				if (awardedBrightTime != null){
					totalAwardedBrightTime += awardedBrightTime.doubleValue();
				}
				Double longBrightTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
		                telescopeName + '_' + OpticonConstants.LONGTERMBRIGHT);
				if (longBrightTime != null){
					totalLongBrightTime += longBrightTime.doubleValue();
				}
			}
	    	
			// place the times in in even spaced row.
			cell.addElement(getParagraphDefaultTitle("Is this a long term proposal: Yes"));
			
			cell.addElement(getParagraphNormal(opticonObservingRequest.getLongTermProposalSpecifics(), EMPTY));
			rootTable.addCell(cell);
		}
		
		
	}

    /**
	 * The addLongTermProposal adds the long term proposal
	 * 
	 * @param opticonObservingRequest
	 * @param rootTable
	 * @throws DocumentException
	 */
    protected void addScienceCategory(OpticonObservingRequest opticonObservingRequest,
            PdfPTable rootTable) {
        PdfPCell cell = getBorderlessCell();
        PdfPTable table = getTableNoSplit(4);

        FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();

        PdfPCell tableCell = null; 
        // TODO NIET VOOR UKIRT kan met options file
        //INVARIANT: Only two fields should be displayable.
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.SCIENCE_CATEGORY, fieldsDefinitionType))
		{
			tableCell = getOpticonTableTitleTextCell("Science category : ", false);
	        table.addCell(tableCell);
	        tableCell = getOpticonTableTextCell(community, false);
	        table.addCell(tableCell);
		}
		
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.OPTICON_FUNDING, fieldsDefinitionType))
		{
			tableCell = getOpticonTableTitleTextCell("Opticon Funding : ", false);
	        table.addCell(tableCell);
	        if (opticonObservingRequest.isOpticonFunding())
	        {
		        tableCell = getOpticonTableTextCell("yes", false);
	        }
	        else
	        {
		        tableCell = getOpticonTableTextCell("no", false);
	        }
	        table.addCell(tableCell);
		}
		/*if (OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVING_MODE, fieldsDefinitionType))
		{
			String observingModeLabel = 
					opticonLabels.getMessage("label.opticonobservingrequest.observingmode");
	        tableCell = getOpticonTableTitleTextCell(observingModeLabel, false);
	        table.addCell(tableCell);
	        tableCell = getOpticonTableTextCell(opticonObservingRequest.getObservingMode(), false);
	        table.addCell(tableCell);
		} */
		else 
		{
			table.addCell(getOpticonTableTextCell(null, false));
			table.addCell(getOpticonTableTextCell(null, false));
		}
        cell.addElement(table);
        rootTable.addCell(cell);
    }
 
    /**
     * Generates a table of observationdetails which will be displayed on the second page of the
     * pdf
     * @see eu.radionet.northstar.business.pdf.ProposalWriter#addObservationsList(eu.radionet.northstar.data.entities.Proposal,
     *      com.lowagie.text.pdf.PdfPTable)
     */
    protected void addObservationsList(List observations, OpticonObservingRequest opticonObservingRequest,
            PdfPCell observationsListCell) {
        int columns = 4;
        PdfPTable observationsListTable = getTableNoSplit(columns);
        /*
         * generates table header
         */

        observationsListTable.addCell(getTableHeaderCell("Number of targets"));
        observationsListTable.addCell(getTableHeaderCell("Telescope"));
        observationsListTable.addCell(getTableHeaderCell("Mode"));
        observationsListTable.addCell(getTableHeaderCell("Exposure (Hours)"));   
        /*
         * add table content
         */
        Iterator observationsIt = observations.iterator();
        while (observationsIt.hasNext())
        {
            OpticonObservation opticonObservation = (OpticonObservation) observationsIt.next();
            observationsListTable.addCell(getTableRequiredTextCell(
            		
            		opticonObservingRequest.getTargetsByObservationId(opticonObservation.getId()).size() + " targets"));
            observationsListTable.addCell(getTableRequiredTextCell(opticonObservation.getTelescopeConfiguration()));
            if (opticonObservation.getInstrument()==null) {
                observationsListTable.addCell(getTableRequiredTextCell(null));
            } else {
                observationsListTable.addCell(getTableRequiredTextCell(opticonObservation.getInstrument().getName()));
            }
            if(opticonObservation.getInstrument().getName().startsWith("TBB"))
            	observationsListTable.addCell(getTableNumbersCell(String.valueOf(opticonObservation.getInstrument().getTbbExposureTime())));
            else
            observationsListTable.addCell(getTableNumbersCell(OpticonTargetUtils
                            .getHoursFromSeconds(opticonObservation.getTotalObservationDuration())));
        }
        observationsListCell.addElement(observationsListTable);

    }

	protected void addChildProposals(Proposal proposal, PdfPCell telescopeDetailsCell)
	    	throws DocumentException
	    {
			NorthStarDelegate northStarDelegate = NorthStarDelegate.getInstance();
			List children = northStarDelegate.getChildsList(proposal);
			int columns = 2;
	        PdfPTable detailsTable = getTableNoSplit(columns);
	        detailsTable.setWidths(new float[] { 0.15f, 0.85f });
	        detailsTable.addCell(getTableHeaderCell("Code"));
	        detailsTable.addCell(getTableHeaderCell("Title"));
	        Iterator chilit = children.iterator();
	        while(chilit.hasNext()){
	        	AdminProposalSummary proposalSummary = (AdminProposalSummary) chilit.next();
	        	detailsTable.addCell(getTableNumbersCell(proposalSummary.getCode()));
	        	detailsTable.addCell(getTableNumbersCell(proposalSummary.getTitle()));
	        	
	        }
	        
	        telescopeDetailsCell.addElement(detailsTable);
	    }
        
    protected void addTelescopeDetails(OpticonObservingRequest opticonObservingRequest, PdfPCell telescopeDetailsCell)
    	throws DocumentException
    {
    	int columns = 2;// 6;
   
        PdfPTable detailsTable = getTableNoSplit(columns);
        detailsTable.setWidths(new float[] { 0.20f, 0.80f });
        detailsTable.addCell(getTableHeaderCell("Telescope"));
        /*detailsTable.addCell(getTableHeaderCell("Dark"));
        detailsTable.addCell(getTableHeaderCell("First"));
        detailsTable.addCell(getTableHeaderCell("Bright"));
        detailsTable.addCell(getTableHeaderCell("Last"));*/
        //detailsTable.addCell(getTableHeaderCell("Total hours"));
        //detailsTable.addCell(getTableHeaderCell("Observing mode"));
        detailsTable.addCell(getTableHeaderCell("Modes"));
        Iterator observationsIt = opticonObservingRequest.getObservations().iterator();
        
        /*
         * used to temporary store the instruments used belonging to one telescope
         */
        Map telescopeInstruments = new HashMap(); 
        //Map observingModes = new HashMap();
        // fill the Map
        while (observationsIt.hasNext())
        {
            OpticonObservation opticonObservation = (OpticonObservation) observationsIt.next();
            if (opticonObservation.getTelescopeConfiguration()!= null)
            {
            	String telescopeName = opticonObservation.getTelescopeConfiguration();
            	// if telescope is not in the set create an entry with a set of no instruments
            	if (!AstronValidator.isBlankOrNull(telescopeName) && !telescopeInstruments.containsKey(telescopeName))
            	{
            		telescopeInstruments.put(telescopeName, new HashSet());
            	}
            	if (opticonObservation.getInstrument() != null && 
            			!AstronValidator.isBlankOrNull(opticonObservation.getInstrument().getName()))
            	{
                	Set instruments = (Set) telescopeInstruments.get(telescopeName);
                	// The Set object prevent double entries
                	instruments.add(opticonObservation.getInstrument().getName());
            	}
            	
            }
        }
        // Use the map to fill the table cells
        Iterator telescopeIt = telescopeInstruments.keySet().iterator(); 
        while (telescopeIt.hasNext())
        {
        	String telescopeName = (String) telescopeIt.next();
        	detailsTable.addCell(getTableNumbersCell(telescopeName));
        	/*
        	detailsTable.addCell(getTableNumbersCell(
        			AstronConverter
                    .toString(NorthStarDelegate.getResourceValue(
                            opticonObservingRequest.getAllocations(),
                            telescopeName + '_' + OpticonConstants.DARK))));
        	detailsTable.addCell(getTableNumbersCell(
        			AstronConverter
                    .toString(NorthStarDelegate.getResourceValue(
                            opticonObservingRequest.getAllocations(),
                            telescopeName + '_' + OpticonConstants.FIRST_Q))));
        	detailsTable.addCell(getTableNumbersCell(
        			AstronConverter
                    .toString(NorthStarDelegate.getResourceValue(
                            opticonObservingRequest.getAllocations(),
                            telescopeName + '_' + OpticonConstants.BRIGHT))));
        	detailsTable.addCell(getTableNumbersCell(
        			AstronConverter
                    .toString(NorthStarDelegate.getResourceValue(
                            opticonObservingRequest.getAllocations(),
                            telescopeName + '_' + OpticonConstants.LAST_Q))));
           	detailsTable.addCell(getTableNumbersCell(
        			AstronConverter
                    .toString(NorthStarDelegate.getResourceValue(
                            opticonObservingRequest.getAllocations(),
                            telescopeName + '_' + OpticonConstants.GLOBAL)))); */
        	
        	
        	/* Get the instruments from the hash table and display them comma seperated */
        	List instruments = new ArrayList((Set) telescopeInstruments.get(telescopeName));
        	String instrumentString = "";
        	for (int i = 0; i < instruments.size(); i++)
        	{
        		if (i ==0)
        		{
        			instrumentString = (String) instruments.get(i);
        		}
        		else
        		{
        			instrumentString = instrumentString + ", " + (String) instruments.get(i);
        		}
        		
        	}
        	detailsTable.addCell(getTableNumbersCell(instrumentString));
        }
        // addSchedulingDetails(opticonObservingRequest, detailsTable);
        telescopeDetailsCell.addElement(detailsTable);

    }

    /**
     * The addObservation method add observation to root table
     * 
     * @param wsrtObservation
     * @param rootTable
     * @param  
     */
    protected void addObservationDetails(Observation observation, Proposal proposal,
            PdfPTable rootTable, int observationcount, boolean isPipeline) throws DocumentException {
    	// get all observations
    	
        OpticonObservation opticonObservation = (OpticonObservation) observation;
        // this is not the right way to get amount of options.
        //String[] tableHeaders = opticonLabels.getMessage("label.opticon.targettableheaders").split(";");
        
        FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
        
        List summaryColumns = createSummaryColumns(fieldsDefinitionType);
    	
        String[] tableHeaders = new String[summaryColumns.size()];
        Iterator columnIt = summaryColumns.iterator();
        int ci=0;
        while (columnIt.hasNext()){
        	String name = (String) columnIt.next();
        	//tableHeaders[ci] = opticonLabels.getMessage("label.opticon.targettable."+name);
        	try{
        		tableHeaders[ci] = NorthStarUtils.getLabel(null,proposal, "label.opticon.targettable."+name);//opticonLabels.getMessage("label.opticon.targettable."+name);
        	}catch(FileNotFoundException fe){
        		log.warn(" "+fe.getMessage());
        	}
        	ci++;
        }
        int columns = tableHeaders.length;
        
        /*
         * add table header
         */
        PdfPTable observationTable = getTableNoSplit(columns);
       // observationTable.setWidths(	new float[] { 0.08f, 0.08f, 0.08f, 0.07f, 0.07f, 0.07f, 0.07f, 0.07f, 0.07f, 0.05f, 0.065f, 0.065f, 0.075f, 0.08f});
        char chr = (char) (observationcount+65);
        if (!AstronValidator.isBlankOrNull(opticonObservation.getTelescopeConfiguration())) {

        	
        	
            PdfPCell telescopeCell = getTableHeaderCell("Observation: "+ String.valueOf(chr) );
            if(isPipeline){
            	telescopeCell = getTableHeaderCell("Pipeline: "+ String.valueOf(chr) );
            }
            telescopeCell.setColspan(columns);

            observationTable.addCell(telescopeCell);

            /*
             * Insert Telescope configuration
             */
            addInstrument(opticonObservation, observationTable, columns, isPipeline);

        } else {

            PdfPCell telescopeCell = getTableHeaderCell("Telescope: None Selected");
            telescopeCell.setColspan(columns);

            observationTable.addCell(telescopeCell);

            PdfPCell instrumentCell = getTableCell();
            instrumentCell.setColspan(columns);

            instrumentCell.addElement(getParagraphCellContentNormal(null, EMPTY));
            
            observationTable.addCell(instrumentCell);
        }

        /*
         * insert id /targets / runs / mode / time /storage
         */
        if(!isPipeline){
        	 List targetDurationMap = new ArrayList();
         	List targetSubbandMap = new ArrayList();
      		List targetThisObservation = new ArrayList();
	        String id = String.valueOf(chr);
	        String targets = "";
	        String runs = "";
	        String mode = opticonObservation.getInstrument().getName();
	        String time = "";
	        String subbands = "";
	        String storage = "";
	        String bfSize ="0.0";
	        String storeBF="";
	        String uvSize ="0.0";
	        String tbbSize ="0.0";
	        String storeUV="";
	        int targetCount = 0;
	        int runsCount = 0;
	        double totalDuration = 0d;
	        int targetSubbands = 0; int tbbsubbands =0;
	       
	        for(Target target : proposal.getObservingRequest().getTargets()){
				OpticonTarget optarget = (OpticonTarget) target;
				if(optarget.getObservingrun() != null && optarget.getObservingrun().equalsIgnoreCase(String.valueOf(chr) )){
					targetCount++;
					if(optarget.isFirstInRun()){
						runsCount++;
					}
					if(optarget.getAllocations() != null){
						Double targetDuration = NorthStarDelegate.getResourceValue(
								optarget.getAllocations(), OpticonConstants.TOTAL_DURATION);
						if (targetDuration != null && optarget.getObservingrun() != null && optarget.getObservingrun().equalsIgnoreCase(String.valueOf(chr))){
							if(optarget.isFirstInRun())
								totalDuration += targetDuration.doubleValue();	
							int thisTargetSuband= SetUpInstrumentBean.convertSubbandListDescription(optarget.getSubbandList());
							targetSubbands +=thisTargetSuband;
							targetDurationMap.add( targetDuration);
							targetSubbandMap.add(thisTargetSuband);
							targetThisObservation.add(optarget.getRunnumber());
						}
					}
				}
				
				if(opticonObservation.getInstrument().getName().startsWith("TBB")){
					if(optarget.getAllocations() != null){
						Double targetDuration = NorthStarDelegate.getResourceValue(
								optarget.getAllocations(), OpticonConstants.TOTAL_DURATION);
						if (targetDuration != null && optarget.getObservingrun() != null && optarget.getObservingrun().equalsIgnoreCase(opticonObservation.getInstrument().getTbbSelectedObservationId())){
							
							tbbsubbands += SetUpInstrumentBean.convertSubbandListDescription(optarget.getSubbandList());
							
						}
					}
				}
			}
	        targets = Integer.toString(targetCount) + " targets";
			runs = Integer.toString(runsCount) + " runs";
			subbands = Integer.toString(targetSubbands) + " subbands";
			time =OpticonTargetUtils.getHoursFromSeconds(Double.valueOf(totalDuration)).toString();
			if(opticonObservation.getInstrument() != null && opticonObservation.getInstrument().getName().equals(OpticonConstants.TBB_PIGGYBACK))
				time=opticonObservation.getInstrument().getTbbExposureTime().toString();
			HashMap storageDetails = null;
			if(opticonObservation.getInstrument().getName().equals(OpticonConstants.TBB_PIGGYBACK))
				storageDetails=calculateBFAndUVSize(opticonObservation,opticonObservation.getInstrument().getTbbExposureTime(), targetSubbands);
			
			else
				storageDetails=calculateBFAndUVSizePerTarget(opticonObservation, targetDurationMap, targetSubbandMap, targetThisObservation)  ;//calculateBFAndUVSize(opticonObservation, totalDuration, targetSubbands);
			double bfSizeVal =0.0;
			double uvSizeVal =0.0;
			double tbbSizeVal =0.0;
			double storageVal =0.0;
			if (storageDetails != null) {

				BigDecimal beam = (BigDecimal) storageDetails.get(OpticonConstants.BEAM_DATA_SIZE);
				
				if (beam != null){
					bfSizeVal =beam.doubleValue();
					bfSize = String.valueOf(OpticonTargetUtils.convertToTB(beam.setScale(2,BigDecimal.ROUND_UP)).doubleValue())+ StorageConverter.UNIT_TERABYTE;
				}	
				
				BigDecimal uv = (BigDecimal) storageDetails.get(OpticonConstants.CORRELATED_VISIBILITY_DATA_SIZE);
				
				if (uv != null){
					uvSizeVal =uv.doubleValue();
					uvSize = String.valueOf(OpticonTargetUtils.convertToTB(uv.setScale(2,BigDecimal.ROUND_UP)).doubleValue())+ StorageConverter.UNIT_TERABYTE;
				}
				
				BigDecimal tbb = (BigDecimal) storageDetails.get(OpticonConstants.TBB_DATA_SIZE);
				
				if (tbb != null){
					tbbSizeVal =tbb.doubleValue();
					 tbbSize= String.valueOf(OpticonTargetUtils.convertToTB(tbb.setScale(2,BigDecimal.ROUND_UP)).doubleValue())+ StorageConverter.UNIT_TERABYTE;
				}
				
				if(storageDetails.get(OpticonConstants.DATA_STORE_ALLOW)!=null ){
					if(((Boolean)( storageDetails.get(OpticonConstants.DATA_STORE_ALLOW))).booleanValue())
						storeBF="Yes";
					else
						storeBF="No";
				}
				
				if(storageDetails.get(OpticonConstants.UV_DATA_STORE_ALLOWE)!=null ){
					if(((Boolean)( storageDetails.get(OpticonConstants.UV_DATA_STORE_ALLOWE))).booleanValue())
						storeUV="Yes";
					else
						storeUV="No";
				}
				
				BigDecimal bd = (BigDecimal) storageDetails.get(OpticonConstants.TOTAL_DATA_SIZE);
				if(bd!=null){
				bd = bd.setScale(2,BigDecimal.ROUND_UP);
				bd=OpticonTargetUtils.convertToTB(bd);
				storage = String.valueOf(bd.doubleValue())+ StorageConverter.UNIT_TERABYTE;
				
				}
				
			}
			storageVal=calculateStorage(opticonObservation,totalDuration,targetSubbands);
			if(opticonObservation.getInstrument() != null && !opticonObservation.getInstrument().isStoreRawData() && 
					opticonObservation.getInstrument().getName()!= null && opticonObservation.getInstrument().getName().contains("Interfero") ){
				opticonObservation.getInstrument().setStoreRawData(true);
				storageVal = calculateStorage(opticonObservation,totalDuration,targetSubbands);
				opticonObservation.getInstrument().setStoreRawData(false);
			}
			HashMap dataStore = null;
			
			if(opticonObservation.getInstrument().getName().equals(OpticonConstants.TBB_PIGGYBACK))
				dataStore=calculateBFAndUVSize(opticonObservation,opticonObservation.getInstrument().getTbbExposureTime(), targetSubbands);
			else
				dataStore=calculateBFAndUVSizePerTarget(opticonObservation, targetDurationMap, targetSubbandMap, targetThisObservation)  ;
			
			storageVal = calculateStorage(dataStore, opticonObservation);
			observationBFStorageMap.put(String.valueOf(chr),bfSizeVal);
			observationUVStorageMap.put(String.valueOf(chr),uvSizeVal);
			observationTBBStorageMap.put(String.valueOf(chr),tbbSizeVal);
			observationStorageMap.put(String.valueOf(chr), storageVal);
			observationSubbandMap.put(String.valueOf(chr), targetSubbands);
			observationDurationMap.put(String.valueOf(chr),  totalDuration);
	       /* Iterator observingRequestIterator = proposal.getObservingRequest().getObservations().iterator();
	        while (observingRequestIterator.hasNext()) {
				OpticonObservation curObservation = (OpticonObservation) observingRequestIterator.next();	
				Double totalDuration = 0d;
				
	        }
	        */

			// get the station number
			String stationDescription = opticonObservation.getInstrument()
					.getStation();
			if(stationDescription!=null && stationDescription.startsWith("Custom")){
				String stationNumber = "";
				int lbegin = stationDescription.indexOf("(");
				int lend = stationDescription.indexOf(")");
				stationNumber = opticonObservation.getInstrument().getCustomStationSpecifics();
				if(!AstronValidator.isBlankOrNull(stationNumber)){
					lbegin = stationNumber.indexOf("(");
					lend = stationNumber.indexOf(")");
					if(lbegin >=0 && lend >=0){
						stationNumber=stationNumber.substring(lbegin+1,lend);
					} else {
						lbegin = stationNumber.indexOf("[");
						lend = stationNumber.indexOf("]");
						if(lbegin >=0 && lend >=0){
							stationNumber=stationNumber.substring(lbegin+1,lend);
							stationNumber =String.valueOf(stationNumber.split(",").length);
						}
					}
				}
				try {
					observationStationMap.put(String.valueOf(chr),Integer.parseInt(stationNumber));
					observationVisitedMap.put(String.valueOf(chr),false);
				} catch (NumberFormatException e) {
					
					e.printStackTrace();
				}
			}else{
				if (!AstronValidator.isBlankOrNull(stationDescription)) {
					int stationNumberStartIndex = stationDescription.indexOf('(');
					int stationNumberEndIndex = stationDescription.indexOf(')');
					String stationNumber = "";
					int coreStations = 0;
					int remoteStations = 0;
					int internationalStations = 0;
					if(!AstronValidator.isBlankOrNull(stationDescription) && stationNumberStartIndex>=0){
						stationNumber = stationDescription.substring(
								stationNumberStartIndex + 1, stationNumberEndIndex);
						String[] stationsArray = stationNumber.split(",");
				        String coreStationsString = stationsArray[0].trim();
				        String remoteStationsString = stationsArray[1].trim();
				        String internationalStationsString = stationsArray[2].trim();
				        try{
							coreStations = new Integer(coreStationsString).intValue();
						}catch(NumberFormatException e){
							log.warn(" "+coreStationsString+" is not an integer");
						}
				        try{
				        	remoteStations = new Integer(remoteStationsString).intValue();
						}catch(NumberFormatException e){
							log.warn(" "+remoteStationsString+" is not an integer");
						}
				        try{
				        	internationalStations = new Integer(internationalStationsString).intValue();
						}catch(NumberFormatException e){
							log.warn(" "+internationalStationsString+" is not an integer");
						}
				        
				        int stations = coreStations + remoteStations + internationalStations;
				        stationNumber = String.valueOf(stations);
					}
					else{
						String array[]=stationDescription.split(",");
						stationNumber=String.valueOf(array.length);
					}
					try {
						observationStationMap.put(String.valueOf(chr),Integer.parseInt(stationNumber));
						observationVisitedMap.put(String.valueOf(chr),false);
					} catch (NumberFormatException e) {
						
						e.printStackTrace();
					}
				}
			}
			rootTable.addCell(observationTable);
			
			String[] summaryTableHeaders = new String[11];
			observationTable = getTableNoSplit(summaryTableHeaders.length);
			summaryTableHeaders[0]="id:";
			summaryTableHeaders[1]="targets:";
			summaryTableHeaders[2]="runs:";
			summaryTableHeaders[3]="mode:";
			summaryTableHeaders[4]="time:";
			//summaryTableHeaders[5]="subbands:";
			summaryTableHeaders[5]="BF data:";	
			summaryTableHeaders[6]="store BF:";
			summaryTableHeaders[7]="UV data:";
			summaryTableHeaders[8]="store UV:";
			summaryTableHeaders[9]="TBB data:";
			summaryTableHeaders[10]="total storage:";		
	    	
	    	 for (int i = 0; i < summaryTableHeaders.length; i++)
	         {
	             observationTable.addCell(getTableHeaderCellSmall(summaryTableHeaders[i]));
	         }
			        
	    	 observationTable.addCell(getTableNumbersCellSmall(id));
	    	 observationTable.addCell(getTableNumbersCellSmall(targets));
	    	 observationTable.addCell(getTableNumbersCellSmall(runs));
	    	 observationTable.addCell(getTableNumbersCellSmall(mode));
	    	 observationTable.addCell(getTableNumbersCellSmall(time));
	    	 //observationTable.addCell(getTableNumbersCellSmall(subbands));
	    	 observationTable.addCell(getTableNumbersCellSmall(bfSize));
	    	 observationTable.addCell(getTableNumbersCellSmall(storeBF));
	    	 observationTable.addCell(getTableNumbersCellSmall(uvSize));
	    	 observationTable.addCell(getTableNumbersCellSmall(storeUV));
	    	 observationTable.addCell(getTableNumbersCellSmall(tbbSize));
	    	 observationTable.addCell(getTableNumbersCellSmall(storage));
	    	 
	    	 rootTable.addCell(observationTable);
        }else{
	        String id = String.valueOf(chr);
	        String targets = "0";
	        String avtime = "0";
	        String avfreq = "0";
	        String demix = "False";
	        String subbands = "0";
	        String storage = "0";
	        String poRatio="";
	        String processingTime="" ;
	        String mode = "";
	        int targetCount = 0;
	        int subbandCount = 0;
	        double pipelineAveraging = 0d;
	        double pipelineAveragingFreq = 0d;
	        double pipelineStorage = 0d;
	        HashMap processTimeResult =null;
			HashMap <String, Integer> poRatioList = new HashMap();
			
	        if(opticonObservation.getInstrument().getAveragingTime() != null){
				pipelineAveraging=opticonObservation.getInstrument().getAveragingTime().doubleValue();
				avtime=opticonObservation.getInstrument().getAveragingTime().toString();
			}
	        if(opticonObservation.getInstrument().getAveragingFrequency() != null){
	        	pipelineAveragingFreq=opticonObservation.getInstrument().getAveragingFrequency().doubleValue();
	        	avfreq=opticonObservation.getInstrument().getAveragingFrequency().toString();
	        }
	        if(opticonObservation.getInstrument().isDemixing()){
	        	demix = "True";
	        }
	        if(opticonObservation.getInstrument()!=null){
	        	mode = opticonObservation.getInstrument().getProcessingMode();
	        }

	        List uvrangeList = new ArrayList();
			String uvRangeValue = new String();
			uvrangeList = OptionsUtils.getLabelValueBeans(Constants.UV_RANGE_IMAGING, new HashMap(), contextConfiguration);
			
			if (uvrangeList != null && uvrangeList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) uvrangeList.get(1);
				uvRangeValue =  urlValue.getValue(); 
			}
			double uvRange = 0;
			if (!AstronValidator.isBlankOrNull(uvRangeValue)) {
				uvRange = new Double(uvRangeValue).doubleValue();
			}
			 
			
			String baselineValue = new String();
			uvrangeList = OptionsUtils.getLabelValueBeans(OpticonConstants.PROCESSING_TIME_BASELINE_FACTOR, new HashMap(), contextConfiguration);
			
			if (uvrangeList != null && uvrangeList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) uvrangeList.get(1);
				baselineValue =  urlValue.getValue(); 
			}
			double baselineFactor =1d;
			if (!AstronValidator.isBlankOrNull(baselineValue)) {
				baselineFactor = new Double(baselineValue).doubleValue();
			}
			
	        double observationDuration =0;
	        int totalStation =0;
	        double totalProcessingTime = 0;
	        for(Target target : proposal.getObservingRequest().getTargets()){
				OpticonTarget optarget = (OpticonTarget) target;
				if(optarget != null && 
				   optarget.getObservingrun() != null && 
				   optarget.getPipeline() != null && 
				   optarget.getPipeline().contains(String.valueOf(chr) )){
					// pipelinestorage is the sum data of the targets divided by integration time, 
					// each target generates the amount of raw data to the observation it belongs for its share of subbands... 
					double observationStorage = 0d; 
					if(optarget.getObservingrun() != null && optarget.getObservingrun().length() > 0 && observationStorageMap.get(optarget.getObservingrun()) != null){
						observationStorage = observationStorageMap.get(optarget.getObservingrun());
					}
					
					int thisTargetSubband = SetUpInstrumentBean.convertSubbandListDescription(optarget.getSubbandList());
					subbandCount += thisTargetSubband;
					int observationSubbands = 0;
					if(observationSubbandMap.get(optarget.getObservingrun()) != null){
						observationSubbands = observationSubbandMap.get(optarget.getObservingrun());
					}
					observationDuration+=observationDurationMap.get(optarget.getObservingrun());
					if(observationStationMap.get(optarget.getObservingrun())!=null && !observationVisitedMap.get(optarget.getObservingrun())){
						totalStation+=observationStationMap.get(optarget.getObservingrun());
						observationVisitedMap.put(optarget.getObservingrun(), true);
					}
					if(observationSubbands*pipelineAveraging > 0){
						double pipelineStorageTarget = (observationStorage*thisTargetSubband)/(observationSubbands*pipelineAveraging*pipelineAveragingFreq);//calculateStorage(opticonObservation,totalDuration,targetSubbands);
						pipelineStorage += pipelineStorageTarget;
						log.warn("["+optarget.getObservingrun()+","+observationStorage+","+
								 thisTargetSubband+","+observationSubbands+","+pipelineAveraging+","+pipelineAveragingFreq+","+pipelineStorageTarget+","+pipelineStorage+"]");
						log.warn("&&observationStorage1 :"+observationStorage);	
					}
					
					Double targetDuration = NorthStarDelegate.getResourceValue(
							optarget.getAllocations(), OpticonConstants.TOTAL_DURATION);
					 processTimeResult = calculateProcessingTime(
								mode,
								opticonObservation.getInstrument()
										.getFlaggingStrategy(),
								opticonObservation.getInstrument().getAntenna() == null ? "": opticonObservation.getInstrument().getAntenna(),
								opticonObservation.getInstrument().isDemixing(),
								opticonObservation.getInstrument().getDemixingSources(),
								totalStation, thisTargetSubband,
								targetDuration!=null?targetDuration.doubleValue():0,
								opticonObservation.getInstrument().getFieldOfView()!=null?opticonObservation.getInstrument().getFieldOfView().doubleValue():0
										, uvRange,baselineFactor, opticonObservation.getInstrument().getProcessingMode());
					 	String key =String.valueOf( processTimeResult.get(OpticonConstants.PROCESSING_TIME_RATIO));
					 	if (mode!=null && mode.equals(OpticonConstants.PROCESSING_MODE_CALIBRATION_AND_IMAGING)) 
					 		key =(String) processTimeResult.get(OpticonConstants.PROCESSING_TIME_RATIO_IMAGING);
						int value =0;
						if(key!=null && poRatioList.get(key) ==null)
							value =1;
						if(poRatioList.get(key)!=null){
							
							value = poRatioList.get(key).intValue()+1;
						}
						poRatioList.put(key, value);
						String totalprocessingtimeValue = String
								.valueOf(processTimeResult.get(OpticonConstants.PROCESSING_TIME_TOTAL));
						if (!AstronValidator.isBlankOrNull(totalprocessingtimeValue)
								&& !totalprocessingtimeValue.equals("null")){
							//targetProcessingTime += Double.parseDouble(totalprocessingtimeValue.replace(",","")); 
							totalProcessingTime+= Double.parseDouble(totalprocessingtimeValue.replace(",","")); 
							targetProcessingTime = Double.parseDouble(totalprocessingtimeValue.replace(",","")); 
						}

					//log.warn("set pipeline:"+chr+" target:"+ targetCount+" run:"+optarget.getObservingrun() +" subb:"+observationSubbands+" av:"+pipelineAveraging+ " storage:"+ observationStorage+" thissubb:"+thisTargetSubband);
					targetCount++;
				}
			}
	        targets = Integer.toString(targetCount) + " targets";
	        subbands = Integer.toString(subbandCount) + " subbands";
			BigDecimal bd = new BigDecimal(pipelineStorage*getPipelineStorageFactor(opticonObservation.getInstrument().getProcessingMode()));
			bd=OpticonTargetUtils.convertToTB(bd);
			bd = bd.setScale(2,BigDecimal.ROUND_UP);
			storage = String.valueOf(bd.doubleValue())+ StorageConverter.UNIT_TERABYTE;
			
			if (processTimeResult != null) {
				NumberFormat nf = NumberFormat.getInstance();
				nf.setMaximumFractionDigits(2);

				processTimeResult.put(OpticonConstants.PROCESSING_TIME_TOTAL, nf.format(totalProcessingTime));
			}
			
		
			String EMPTY_STRING =" ";
			String ratioDescription =EMPTY_STRING;
			if(poRatioList!=null)
			{
				for(String key : poRatioList.keySet())
				{
					ratioDescription+=key+"("+poRatioList.get(key)+")\n";
				}
			}	
			poRatio=String.valueOf(ratioDescription);
			if(processTimeResult!=null &&processTimeResult.get(OpticonConstants.PROCESSING_TIME_TOTAL)!=null ){
				processingTime=String.valueOf(processTimeResult.get(OpticonConstants.PROCESSING_TIME_TOTAL));
				
			}
			else
				processingTime=EMPTY_STRING;
			
				/* Iterator observingRequestIterator = proposal.getObservingRequest().getObservations().iterator();
	        while (observingRequestIterator.hasNext()) {
				OpticonObservation curObservation = (OpticonObservation) observingRequestIterator.next();	
				Double totalDuration = 0d;
				
	        }
	        */
			rootTable.addCell(observationTable);
			
			String[] summaryTableHeaders = new String[8];
			observationTable = getTableNoSplit(summaryTableHeaders.length);
			summaryTableHeaders[0]="id:";
			summaryTableHeaders[1]="targets:";
			summaryTableHeaders[2]="av. time:";
			summaryTableHeaders[3]="av. freq:";
			summaryTableHeaders[4]="demixing:";
			//summaryTableHeaders[5]="subbands:";	
			summaryTableHeaders[5]="storage:";	
			summaryTableHeaders[6]="p/o ratio:";	
			summaryTableHeaders[7]="processing time:";	
	    	
	    	 for (int i = 0; i < summaryTableHeaders.length; i++)
	         {
	             observationTable.addCell(getTableHeaderCellSmall(summaryTableHeaders[i]));
	         }
			        
	    	 observationTable.addCell(getTableNumbersCellSmall(id));
	    	 observationTable.addCell(getTableNumbersCellSmall(targets));
	    	 observationTable.addCell(getTableNumbersCellSmall(avtime));
	    	 observationTable.addCell(getTableNumbersCellSmall(avfreq));
	    	 observationTable.addCell(getTableNumbersCellSmall(demix));
	    	// observationTable.addCell(getTableNumbersCellSmall(subbands));
	    	 observationTable.addCell(getTableNumbersCellSmall(storage));
	    	 observationTable.addCell(getTableNumbersCellSmall(poRatio));
	    	 observationTable.addCell(getTableNumbersCellSmall(processingTime));
	    	 rootTable.addCell(observationTable);
        }
        /*
        PdfPCell cell = getTableCell();
        cell.setColspan(columns);
        if (!AstronValidator
                .isBlankOrNull(opticonObservation.getObservationDates())) {
        	cell.addElement(this.getParagraphCellContentTitleWithValue("preferred Dates: ",
        			opticonObservation.getObservationDates(),EMPTY_NOT_REQUIRED));
        }
        
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.REQUIRED_SCHEDULING_CONSTRAINTS ,
				fieldsDefinitionType))
        {
	        if (!AstronValidator.isBlankOrNull(opticonObservation.getRequiredSchedConstraints())) {
	            cell.addElement(this
	                            .getParagraphCellContentTitle("Required scheduling constraints"));
	            cell.addElement(this.getParagraphCellContentNormal(opticonObservation
	                    .getRequiredSchedConstraints(), EMPTY_NOT_REQUIRED));
	        } else {
	            cell.addElement(this
	                            .getParagraphCellContentTitle("No required scheduling constraints"));
	        }
        }
         * addPreferred Scheduling constraints
         
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.PREFERRED_SCHEDULING_CONSTRAINTS,
				fieldsDefinitionType))
        {
	        if (!AstronValidator.isBlankOrNull(opticonObservation
	                .getPreferredSchedConstraints())) {
	            cell.addElement(this
	                            .getParagraphCellContentTitle("Preferred scheduling constraints"));
	            cell.addElement(this.getParagraphCellContentNormal(opticonObservation
	                    .getPreferredSchedConstraints(), EMPTY_NOT_REQUIRED));
	
	        } else {
	            cell.addElement(this
	                            .getParagraphCellContentTitle("No preferred scheduling constraints"));
	        }
        }
        
        observationTable.addCell(cell);
        */
        
        /*
         * add observation to root table
         */
        //rootTable.addCell(observationTable);

    }
    
    @SuppressWarnings("unchecked")
	private void addTargets(Proposal proposal,
			PdfPTable targetPageTable) {

    	FieldsDefinitionType fieldsDefinitionType = 
    				(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
    	List summaryColumns = createSummaryColumns(fieldsDefinitionType);
    	
        String[] tableHeaders = new String[summaryColumns.size()];
        Iterator columnIt = summaryColumns.iterator();
        int ci=0;
        while (columnIt.hasNext()){
        	String name = (String) columnIt.next();
        	//tableHeaders[ci] = opticonLabels.getMessage("label.opticon.targettable."+name);
        	try{
        		tableHeaders[ci] = NorthStarUtils.getLabel(null,proposal, "label.opticon.targettable."+name);//opticonLabels.getMessage("label.opticon.targettable."+name);
        	}catch(FileNotFoundException fe){
        		log.warn(" "+fe.getMessage());
        	}
        	ci++;
        }
        int columns = tableHeaders.length;
        
        /*
         * add table header
         */
        PdfPTable observationTable = getTableNoSplit(columns);

    	
    	 for (int i = 0; i < columns; i++)
         {
             observationTable.addCell(getTableHeaderCellSmall(tableHeaders[i]));
         }
    	/* 
         * fill the table rows with observation detais
         */
         List targets =  proposal.getObservingRequest().getTargets();
    	 Collections.sort(targets, new Comparator<OpticonTarget>() {
		        public int compare(OpticonTarget o1, OpticonTarget o2) {
		        		if(o1==null ||  o2 ==null){
		        			return 0;
		        		}
		        		if(o1.getRunnumber() == null){
		        			o1.setRunnumber("1");
		        		}
		        		if(o2.getRunnumber() == null){
		        			o2.setRunnumber("1");
		        		}
		        		if(o1.isFirstInRun() && new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber())) == 0){
		        			return -1;
		        		}
		        		if(o2.isFirstInRun() && new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber())) == 0){
		        			return 1;
		        		}
		        		return new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber()));
		            }
		        });
    	List<Target> ttargets = targets; 
        for (Target target : ttargets)
        {
        	OpticonTarget opticonTarget = (OpticonTarget) target;
        	
        	if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SELECT_RUNNUMBER, fieldsDefinitionType)){
	        	if(opticonTarget.isFirstInRun())
        		observationTable.addCell(getTableNumbersCellSmall(opticonTarget.getRunnumber() ));
	        	else
	        		observationTable.addCell(getTableNumbersCellSmall(" "));
	        }
	        /*
	         * add field name
	         */
	        observationTable.addCell(getTableNumbersCellSmall(opticonTarget
	                .getFieldName()));
	        /*
	         * add ra & dec and other target information
	         */
	        if (opticonTarget.getRa() != null
	                && opticonTarget.getDecl() != null) {
	            AstroCoordinate coordinate = new AstroCoordinate(opticonTarget
	                    .getRa().doubleValue(), opticonTarget.getDecl()
	                    .doubleValue());
	            observationTable
	                    .addCell(getTableNumbersCellSmall(coordinate.RAString()));
	            observationTable
	                    .addCell(getTableNumbersCellSmall(coordinate.DecString()));
	        } else {
	            observationTable.addCell(getTableNumbersCellSmall(null));
	            observationTable.addCell(getTableNumbersCellSmall(null));
	        }
	        observationTable.addCell(getTableNumbersCellSmall(opticonTarget
	                .getEpoch()));
			Double totalDuration = NorthStarDelegate.getResourceValue(
					opticonTarget.getAllocations(),
					OpticonConstants.TOTAL_DURATION);
			if(opticonTarget.isFirstInRun())
			observationTable
						.addCell(getTableNumbersCellSmall(OpticonTargetUtils
	                        .getHoursFromSeconds(totalDuration)));
			else
				observationTable.addCell(getTableNumbersCellSmall(""));
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_MOON, fieldsDefinitionType))
    		{ 
	        	observationTable
                	.addCell(getTableNumbersCellSmall(opticonTarget.getMoon()));
    		}
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING, fieldsDefinitionType))
    		{
    			observationTable
                	.addCell(getTableNumbersCellSmall(opticonTarget.getSeeing()));
    			observationTable.addCell(getTableCell());
    		}
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING_RANGE, fieldsDefinitionType))
    		{
    			observationTable
    				.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getSeeingLower())));
    			observationTable
            		.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getSeeingUpper())));
    		}
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_WATER, fieldsDefinitionType))
    		{
    			observationTable
    				.addCell(getTableNumbersCellSmall(opticonTarget.getWater()));
    		}
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SN, fieldsDefinitionType))
    		{ 
    			observationTable
    			 .addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getSn())));
    		}
            if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_MAGNITUDE, fieldsDefinitionType))
    		{ 
            	observationTable
					.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getMagnitude())));
    		}
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX, fieldsDefinitionType))
    		{    
    			String params = "";
    			if(opticonTarget.getFlux()!=null){
    				params=AstronConverter.toString(opticonTarget.getFlux())+"Jy";
    			}
    			if(opticonTarget.getFluxFrequency() != null){
    				params += ", "+AstronConverter.toString(opticonTarget.getFluxFrequency())+"MHz";
    			}
    			if(opticonTarget.getSpectralIndex() != null){
    				params += ", "+AstronConverter.toString(opticonTarget.getSpectralIndex());
    			}
    			
    			observationTable
    				.addCell(getTableNumbersCellSmall(params ));
    		}
    		/*if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX_FREQUENCY, fieldsDefinitionType))
    		{            
    			observationTable
    				.addCell(getTableNumbersCellSmall()));
    		}
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECTRAL_INDEX, fieldsDefinitionType))
    		{            
    			observationTable
    				.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getSpectralIndex())));
    		}*/
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_DIAMETER, fieldsDefinitionType))
    		{            
    			observationTable
    				.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getDiameter())));
    		}
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SKY_QUALITIES, fieldsDefinitionType))
    		{            
    			observationTable
    				.addCell(getTableNumbersCellSmall(opticonTarget.getSkyQuality()));
    		}
    		
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SUBBAND_LIST, fieldsDefinitionType)){
    			observationTable
				.addCell(getTableNumbersCellSmall(opticonTarget.getSubbandList() ));
	        }
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_TOTAL_SUBBAND_LIST, fieldsDefinitionType)){
    			observationTable
				.addCell(getTableNumbersCellSmall(opticonTarget.getTotalSubbandList()));
	        }   		
    		
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_STORAGE, fieldsDefinitionType)){
	        	observationTable
				.addCell(getTableNumbersCellSmall("0" ));
	        }
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_RUNS, fieldsDefinitionType)){
	        	observationTable
				.addCell(getTableNumbersCellSmall(opticonTarget.getObservingrun()));
	        }
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_CALIBRATION_BEAM, fieldsDefinitionType)){
	        	if(opticonTarget.isCalibrationBeam()){
	        		observationTable.addCell(getTableNumbersCellSmall("Y" ));
	        	}else{
	        		observationTable.addCell(getTableNumbersCellSmall("N" ));
	        	}
				
	        }
	        
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SINGLE_PER_RUN, fieldsDefinitionType)){
	        	observationTable
				.addCell(getTableNumbersCellSmall(opticonTarget.getObservingrun()));
	        }
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SELECT_PIPELINE, fieldsDefinitionType)){
	        	observationTable
				.addCell(getTableNumbersCellSmall(opticonTarget.getPipeline()));
	        }
		
    		
    		
    		/*
    		if (OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_POSTPROCESSING , fieldsDefinitionType))
    		{      
    			observationTable
				.addCell(getTableInnerHeaderCell("Post Processing: "));
            	
    			PdfPCell postCell = getTableCell();
            	
            	//postCell.addElement(this.getParagraphCellContentTitle("Configuration comments"));
            	if(instrument.isCalibration()){
            		postCell.addElement(this
                            .getParagraphCellContentTitleWithValue(
                                    "Calibration : ", "yes", EMPTY));
                } else {
                	postCell.addElement(this
                            .getParagraphCellContentTitleWithValue(
                                    "Calibration : ", "no", EMPTY));
            	}
            	if(instrument.isFlagging()){
            		postCell.addElement(this
                            .getParagraphCellContentTitleWithValue(
                                    "Flagging : ", "yes", EMPTY));
                } else {
                	postCell.addElement(this
                            .getParagraphCellContentTitleWithValue(
                                    "Flagging : ", "no", EMPTY));
            	}
            		
            	cell.addElement(postCell);
    			observationTable
    				.addCell(getTableNumbersCellSmall(opticonTarget.getSkyQuality()));
    			
    		}
    		*/
    		observationTable.addCell(getTableNumbersCellSmall(opticonTarget.getComments()));
      }

        /*
         * add observation to root table
         */
        targetPageTable.addCell(observationTable);
		
	}


    protected void addFilterDetails(OpticonInstrument instrument, PdfPCell cell)
    	throws DocumentException 
    {
    	//invariant: instrument != null
		List configurations = OptionsDelegate.getOptionsConfigurationTypes(
				OpticonConstants.INSTRUMENT_FILTER, new HashMap(), contextConfiguration);
		boolean isAllowedToDisplay = false;
		Iterator filtersIt = instrument.getFilters().iterator();
		while (filtersIt.hasNext() && !isAllowedToDisplay)
		{
			ConfigureOptionType configureOptionType = 
				(ConfigureOptionType) OptionsDelegate.getOption((String) filtersIt.next(), 
									configurations);	
			if (configureOptionType != null	&& configureOptionType.getSubfields() != null) 
			{
				FieldsDefinitionType fieldsDefinitionType = 
					(FieldsDefinitionType) configureOptionType.getSubfields();
				isAllowedToDisplay = OptionsDelegate.allowedToDisplay(
						OpticonConstants.INSTRUMENT_FILTER_DETAILS,	fieldsDefinitionType);			
			}
		}
		if (isAllowedToDisplay)
		{
            cell.addElement(this.getParagraphCellContentTitleWithValue(
                    "Filter details: ", instrument.getFilterDetails(), EMPTY));
		}
	}
    
    protected void addInstrument(OpticonObservation opticonObservation,
            PdfPTable observationTable, int columns,boolean isPipeline) throws DocumentException {
        PdfPCell observingModeCell = getTableCell();
        observingModeCell.setColspan(columns);
        PdfPTable observingModeTable = getTableNoSplit(1);
        
        /*
         * if backend exist
         */
        if (opticonObservation.getInstrument() != null) {

        	OpticonInstrument instrument = opticonObservation.getInstrument();
        	
        	PdfPCell cell = getTableInnerHeaderCell(instrument
                    .getName()+ " mode : ");
        	if(instrument.getName().contains("Beam") ){
        		observingModeTable = getTableNoSplit(2);
        		cell.setColspan(2);
        	}
        	 
        	observingModeTable.addCell(cell);
            cell = getTableCell();
            PdfPCell cell2 = getTableCell();
            PdfPTable postProcTable = getTableNoSplit(1);
            Map enteredValues = new HashMap();
            enteredValues.put(OpticonConstants.TELESCOPE_CONFIGURATIONS,
                    OptionsUtils.getList(opticonObservation.getTelescopeConfiguration()));
            List configurations = OptionsDelegate.getOptionsConfigurationTypes(
                    OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredValues,
                    contextConfiguration);
            
            String name = instrument.getName();
            String[] names= name.split("-");
            List<ConfigureOptionType>  cType = new ArrayList<ConfigureOptionType>();
            for (String instName : names){
            	 ConfigureOptionType configureOptionType = (ConfigureOptionType) OptionsDelegate
                         .getOption(instName, configurations);
            	 cType.add(configureOptionType);
            }
            FieldsDefinitionType fieldsDefinitionType = new FieldsDefinitionType();
            for (ConfigureOptionType configureOptionType :cType ) {
				if (configureOptionType != null
						&& configureOptionType.getSubfields() != null) {
				
					fieldsDefinitionType.add(configureOptionType.getSubfields());
				}
            }
					// for lofar
					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.INSTRUMENT_STATION,
							fieldsDefinitionType)) {
						cell.addElement(this
								.getParagraphCellContentTitleWithValue(
										"Stations: ", instrument.getStation(),
										EMPTY));
						if (!AstronValidator.isBlankOrNull(instrument
								.getCustomStationSpecifics())) {
							cell.addElement(this.getParagraphCellContentTitleWithValue(
									"Station specifics: ",
									instrument.getCustomStationSpecifics(),
									EMPTY));
						}
					}

					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.INSTRUMENT_CLOCK,
							fieldsDefinitionType)) {
						cell.addElement(this
								.getParagraphCellContentTitleWithValue(
										"Clock speed: ", instrument.getClock(),
										EMPTY));
					}

					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.INSTRUMENT_ONE_FILTER,
							fieldsDefinitionType)) {
						cell.addElement(this
								.getParagraphCellContentTitleWithValue(
										"Filter: ", instrument.getOneFilter(),
										EMPTY));
					}

					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.INSTRUMENT_ANTENNA,
							fieldsDefinitionType)) {
						cell.addElement(this
								.getParagraphCellContentTitleWithValue(
										"Antenna: ", instrument.getAntenna(),
										EMPTY));
					}

					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.INSTRUMENT_SUBARRAY_POINTINGS,
							fieldsDefinitionType)) {
						if (instrument.getSubarrayPointings() == null) {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Subarray Pointings: ", "Default",
											EMPTY));
						} else {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Subarray Pointings: ", instrument
													.getSubarrayPointings()
													.toString(), EMPTY));
						}
					}

					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.OBSERVATION_NOISE_LEVEL,
							fieldsDefinitionType)) {
						if (opticonObservation.getNoiseLevel() != null) {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Required noise level (J): ",
											opticonObservation.getNoiseLevel()
													.toString(), EMPTY));
						}
						
						if(instrument.isInterferoConf())
						{
							if (instrument.getFrequencyChannels() != null) {
								cell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"Channels per subband : ",
												instrument.getFrequencyChannels().toString(),
												EMPTY));
							}
						}
						else if (instrument.getChannels() != null) {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Channels per subband : ",
											instrument.getChannels().toString(),
											EMPTY));
						} else {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Channels per subband : ", null,
											EMPTY));
						}
					}

					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.INSTRUMENT_INTEGRATION_TIME,
							fieldsDefinitionType)
							&& instrument.getIntegrationTime() != null) {
						cell.addElement(this
								.getParagraphCellContentTitleWithValue(
										"Integration time: ", instrument
												.getIntegrationTime()
												.toString(), EMPTY));
					}
					
					/*   if (OptionsDelegate.allowedToDisplay(
					           OpticonConstants.INSTRUMENT_STORE_RAW_DATA , fieldsDefinitionType)) {
					   	if (instrument.isStoreRawData()) {
					           cell.addElement(this.getParagraphCellContentTitleWithValue(
					                   "Keep raw observation data: ", "Yes", EMPTY));
					   	} else {
					           cell.addElement(this.getParagraphCellContentTitleWithValue(
					                   "Keep raw observation data: ", "No", EMPTY));
					   	}
					   }*/
					
					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.INSTRUMENT_COMMENTS,
							fieldsDefinitionType)) {
						if (!AstronValidator.isBlankOrNull(instrument
								.getComments())) {
							cell.addElement(this
									.getParagraphCellContentTitle("Configuration comments"));
							cell.addElement(this.getParagraphCellContentNormal(
									instrument.getComments(),
									EMPTY_NOT_REQUIRED));

						} else {
							cell.addElement(this
									.getParagraphCellContentTitle("No configuration comments "));
						}
					}
					
					 if(instrument.getName().contains("TBB"))
					 {
						 
						 if (instrument.getTbbTriggerLength()!= null) {
								cell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"TBB Trigger Length : ",
										instrument.getTbbTriggerLength().toString()+ "Sec.",
												EMPTY));
							} 	
						 if (instrument.getTbbTriggerRate()!= null) {
								cell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"TBB Trigger Rate : ",
										instrument.getTbbTriggerRate().toString()+ " per hour",
												EMPTY));
							} 	
						  
							 
					 }
					FieldsDefinitionType targetsDefinitionType = (FieldsDefinitionType) contextConfiguration
							.getFieldsDefinitionType();
					/*	if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.DISPLAY_PIGGYBACK_MODE,
							fieldsDefinitionType)) {
						if (!AstronValidator.isBlankOrNull(instrument
								.getPiggyBackSpecifics())) {
							PdfPTable piggyTable = getTableNoSplit(1);

							piggyTable
									.addCell(getTableInnerHeaderCell("Piggy Back mode: "));
							PdfPCell piggyCell = getTableCell();

							piggyCell.addElement(this
									.getParagraphCellContentTitleWithValue("",
											instrument.getPiggyBackSpecifics(),
											EMPTY_NOT_REQUIRED));
							piggyTable.addCell(piggyCell);
							cell.addElement(piggyTable);
						}
						
						
						if (instrument.getTbbExposureTime() != null) {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"TBB Exposure Time : ",
									instrument.getTbbExposureTime().toString(),
											EMPTY));
						} 

					}*/
					if (instrument.getName().contains("Beam")) {
						cell2.addElement(this
								.getParagraphCellContentTitle("Coherent sum of stations (CS) : "
										+ instrument.isCohstokes()));

						cell2.addElement(this
								.getParagraphCellContentTitle("Fly's Eye (FE) : "
										+ instrument.isFlysEye()));

						if (instrument.isCohstokes() || instrument.isFlysEye()) {
							cell2.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Polarizations (CS/FE) : ",
											instrument.getPolarization(),
											EMPTY_NOT_REQUIRED));
						} else {
							cell2.addElement(this
									.getParagraphCellContentTitle("."));
						}

						cell2.addElement(this
								.getParagraphCellContentTitle("Incoherent sum of stations : "
										+ instrument.isIncohstokes()));
						if (instrument.isIncohstokes()) {
							cell2.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Polarizations (Incoherent) : ",
											instrument.getIncPolarization(),
											EMPTY_NOT_REQUIRED));
						} else {
							cell2.addElement(this
									.getParagraphCellContentTitle("."));
						}
						
						int tiedArrayBeams = 0;
						if (instrument.isCohstokes() && instrument.getCohTaBeams() != null) {
							tiedArrayBeams = instrument.getCohTaBeams();
//						} else if (instrument.isIncohstokes() && instrument.getInCohTaBeams() != null) {
//							tiedArrayBeams = instrument.getInCohTaBeams();
						} else if (instrument.isIncohstokes()) {
							tiedArrayBeams = 1; // HAH 20160825 Quick fix. For incoherent Beams there can only be one beam (info Luciano Cerrigone/Richard Fallows)
						}

						if (tiedArrayBeams != 0) {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Tied array beams (Manual) : ", String.valueOf(tiedArrayBeams),
											EMPTY));
						} else {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Tied array beams (Manual) : ", "0", EMPTY));
						}
						
						int tiedArrayRings = 0;
						if (instrument.getRings() != null) {
							tiedArrayRings = instrument.getRings();
						};
						
						if (tiedArrayRings != 0) {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Tied array rings : ", String.valueOf(tiedArrayRings),
											EMPTY));
						} else {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Tied array rings : ", "0", EMPTY));
						}
						
						if (instrument.getIntsteps() != null) {
							cell2.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Coherent Time integration factor : ",
											instrument.getIntsteps().toString(),
											EMPTY));
						} else {
							cell2.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Coherent Time integration factor : ",
											"0", EMPTY));
						}
						if (instrument.getChannels() != null) {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Coherent Channels per subband : ",
											instrument.getChannels().toString(),
											EMPTY));
						} else {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Coherent Channels per subband : ",
											"0", EMPTY));
						}

						if (instrument.getIncohIntsteps() != null) {
							cell2.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Incoherent Time integration factor : ",
											instrument.getIncohIntsteps()
													.toString(), EMPTY));
						} else {
							cell2.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Incoherent Time integration factor : ",
											"0", EMPTY));
						}
						if (instrument.getIncchannels() != null) {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Incoherent Channels per subband : ",
											instrument.getIncchannels()
													.toString(), EMPTY));
						} else {
							cell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Incoherent Channels per subband : ",
											"0", EMPTY));
						}

						/*cell2.addElement(this
								.getParagraphCellContentTitle(
										"Correlated visibilities :  "+instrument.isCorrelatedVisibilities() ));*/

						//                	cell2.addElement(this
						//                			.getParagraphCellContentTitle(
						//                					"Complex Voltage : "+instrument.isComplexVoltage() ));

						//cell.addElement(this
								//.getParagraphCellContentTitle("Raw Voltage : "
										//+ instrument.isRawVoltage()));

						

					}
					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.DISPLAY_POSTPROCESSING,
							targetsDefinitionType)) {

						PdfPCell postCell = getTableInnerHeaderCell("Post Processing: ");
						if (instrument.getName().contains("Beam")) {
							postProcTable = getTableNoSplit(2);
							postCell.setColspan(2);
						}
						postProcTable.addCell(postCell);
						postCell = getTableCell();
						postCell.setColspan(2);
						if (instrument.isAveraging()) {
							if (instrument.getAveragingTime() != null) {
								postCell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"Averaging time : ", instrument
														.getAveragingTime()
														.toString(), EMPTY));
							}
							if (instrument.getAveragingFrequency() != null) {
								postCell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"Averaging frequency : ",
												instrument
														.getAveragingFrequency()
														.toString(), EMPTY));
							}
						}
						if (instrument.getName() != null
								&& instrument.getName().contains("Interfero")) {
							if (instrument.isCalibration()) {
								postCell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"Calibration : ", "yes", EMPTY));
							} else {
								postCell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"Calibration : ", "no", EMPTY));
							}
							if (instrument.isFlagging()) {
								postCell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"Flagging : ", "yes", EMPTY));
							} else {
								postCell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"Flagging : ", "no", EMPTY));
							}
						}
						if (instrument.isImaging()) {
							postCell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Field size : ",
											instrument.getFieldSizeX()
													+ " x "
													+ instrument
															.getFieldSizeY(),
											EMPTY));
							postCell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"Pixel size : ",
											instrument.getPixelSizeX()
													+ " x "
													+ instrument
															.getPixelSizeY(),
											EMPTY));

						}

						if (instrument.getName() != null
								&& instrument.getName().contains("Beam")) {
							if (instrument.isCalibration()) {
								postCell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"Rfi checker : ", "yes", EMPTY));
							} else {
								postCell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"Rfi checker : ", "no", EMPTY));
							}
							if (instrument.isFlagging()) {
								postCell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"FoldAll : ", "yes", EMPTY));
							} else {
								postCell.addElement(this
										.getParagraphCellContentTitleWithValue(
												"FoldAll : ", "no", EMPTY));
							}
						}

						if (instrument.isSummaryPlots()) {
							postCell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"SummaryPlots : ", "yes", EMPTY));
						} else {
							postCell.addElement(this
									.getParagraphCellContentTitleWithValue(
											"SummaryPlots : ", "no", EMPTY));
						}

						postProcTable.addCell(postCell);
						//cell.addElement(postProcTable);
					}

				
			
			// PIPELINE
            if(isPipeline){
            	cell.addElement(this
            			.getParagraphCellContentTitleWithValue(
                                "Processing mode : ", instrument.getProcessingMode(), EMPTY));
            	if(!instrument.getProcessingMode().equalsIgnoreCase(OpticonConstants.PROCESSING_MODE_PULSAR)){
	            	cell.addElement(this
	            			.getParagraphCellContentTitleWithValue(
	                                "Flagging strategy : ", instrument.getFlaggingStrategy(), EMPTY));
            	}
            	if(instrument.getAveragingTime() != null){
            		cell.addElement(this
            			.getParagraphCellContentTitleWithValue(
                                "Averaging time steps : ", String.valueOf(instrument.getAveragingTime().doubleValue()), EMPTY));
            	}
            	if(instrument.getAveragingFrequency() != null){
            		cell.addElement(this
            			.getParagraphCellContentTitleWithValue(
                                "Averaging freq steps : ", instrument.getAveragingFrequency().toString(), EMPTY));
            	}
            	if(!instrument.getProcessingMode().equalsIgnoreCase(OpticonConstants.PROCESSING_MODE_PULSAR)){
            		cell.addElement(this
                			.getParagraphCellContentTitle(
                                    "Demixing : " + instrument.isDemixing()));
            	}
            	if(instrument.isDemixing()){
            		if(instrument.getDemixingTime() != null && instrument.getDemixingFrequency() != null){
                		cell.addElement(this
                    			.getParagraphCellContentTitleWithValue(
                                        "Demixing time steps : ", instrument.getDemixingTime().toString(), EMPTY));
                		cell.addElement(this
                    			.getParagraphCellContentTitleWithValue(
                                        "Demixing freq steps : ", instrument.getDemixingFrequency().toString(), EMPTY));
                	}
            		if(instrument.getDemixingSources() != null){
            			String sources="";
                		for(String source : instrument.getDemixingSources()){
                			sources +=", "+source;
                		}
                		cell.addElement(this
                    			.getParagraphCellContentTitleWithValue(
                                        "Demixing sources : ", sources, EMPTY));
            		}
            	}
            	if(instrument.getProcessingMode() != null && instrument.getProcessingMode().equalsIgnoreCase("Calibration + imaging")){
            		if(instrument.getSubbandsPerImage() != null && instrument.getFieldOfView() != null){
                		cell.addElement(this
                    			.getParagraphCellContentTitleWithValue(
                                        "Subbands per image : ", instrument.getSubbandsPerImage().toString(), EMPTY));
                		cell.addElement(this
                    			.getParagraphCellContentTitleWithValue(
                                        "Field of View : ", instrument.getFieldOfView().toString(), EMPTY));
            		}
            	}
            	
            	/**
				 * Pulsar pipeline parameters.
				 */
				if(!AstronValidator.isBlankOrNull(instrument.getProcessingMode()) && instrument.getProcessingMode().trim().equalsIgnoreCase(OpticonConstants.PROCESSING_MODE_PULSAR)){
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Skip RFI check : ", instrument.isSkipRFI()? "YES" : "NO", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Skip folding : ", instrument.isSkipFolding()? "YES" : "NO", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Skip pdmp : ", instrument.isSkipPdmp()? "YES" : "NO", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Skip dspsr: ", instrument.isSkipDspsr()? "YES" : "NO", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Skip prepfold : ", instrument.isSkipPrepfold()? "YES" : "NO", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Single pulse analysis : ", instrument.isSinglePulse()? "YES" : "NO", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"RRATs analysis : ", instrument.isRratsAnalysis()? "YES" : "NO", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Skip dynamic average : ", instrument.isSkipDynamicAverage()? "YES" : "NO", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Length of subintegration (sec) : ", instrument.getSubintegrationLength() != null ? String.valueOf(instrument.getSubintegrationLength()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Convert HDF5 32-bit raw data to 8-bit : ", instrument.isConvertRawData()? "YES" : "NO", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Clip threshold (in units of sigma) : ", instrument.getThreshold() != null ? String.valueOf(instrument.getThreshold()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Sigma limit in conversion from raw HDF5 to PSRFITS : ", instrument.getSigmaLimit() != null ? String.valueOf(instrument.getSigmaLimit()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Number of blocks read at once in conversion to PSRFITS : ", instrument.getNumberOfBlocks() != null ? String.valueOf(instrument.getNumberOfBlocks()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Prepfold options : ", !AstronValidator.isBlankOrNull(instrument.getPrepfoldOptions()) ? String.valueOf(instrument.getPrepfoldOptions()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Prepsubband options : ", !AstronValidator.isBlankOrNull(instrument.getPresubbandOptions()) ? String.valueOf(instrument.getPresubbandOptions()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"RFIfind options : ", !AstronValidator.isBlankOrNull(instrument.getRfifind()) ? String.valueOf(instrument.getRfifind()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Dspsr options : ", !AstronValidator.isBlankOrNull(instrument.getDspsrOptions()) ? String.valueOf(instrument.getDspsrOptions()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Digifil options : ", !AstronValidator.isBlankOrNull(instrument.getDigifilOptions()) ? String.valueOf(instrument.getDigifilOptions()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Prepdata options : ", !AstronValidator.isBlankOrNull(instrument.getPredataOptions()) ? String.valueOf(instrument.getPredataOptions()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Extra options to convert from raw HDF5 to PSRFITS : ", !AstronValidator.isBlankOrNull(instrument.getBf2fitsOptions()) ? String.valueOf(instrument.getBf2fitsOptions()) : "EMPTY", EMPTY));
					cell.addElement(this
							.getParagraphCellContentTitleWithValue(
									"Pulsar : ", !AstronValidator.isBlankOrNull(instrument.getPulsarDesc()) ? String.valueOf(instrument.getPulsarDesc()) : "EMPTY", EMPTY));
					
				}
            	
            	if(instrument.getProcessingMode() != null && ( instrument.getProcessingMode().equalsIgnoreCase("Pulsar pipeline")
            			|| instrument.getProcessingMode().equalsIgnoreCase(OpticonConstants.PROCESSING_MODE_USER_SPECIFIED))){
            		if (!AstronValidator
                            .isBlankOrNull(instrument.getComments())) {
                        cell
                                .addElement(this
                                        .getParagraphCellContentTitle("Configuration comments"));
                        cell.addElement(this.getParagraphCellContentNormal(
                                instrument.getComments(), EMPTY_NOT_REQUIRED));

                    } else {
                        cell
                                .addElement(this
                                        .getParagraphCellContentTitle("No configuration comments "));
                    }
            		
            	}

            }
            
            // FIXME brrrr 
            if(instrument.getName().contains("Beam") ){
            	observingModeTable.addCell(cell);
            	observingModeTable.addCell(cell2);
            	cell = getTableCell();
            	cell.setColspan(2);
            }
            cell.addElement(postProcTable);
            observingModeTable.addCell(cell);
            
        }
        /*
         * if not
         */
        else {
            observingModeTable
                    .addCell(getTableInnerHeaderCell("Observing mode details"));
            observingModeTable.addCell(getParagraphCellContentNormal(null,
                    EMPTY));
        }
        observingModeCell.addElement(observingModeTable);
        observationTable.addCell(observingModeCell);

    }

	protected PdfPCell getTableHeaderCellSmall(String text) {
		PdfPCell cell = new PdfPCell();
		cell.setPhrase(new Phrase(text, TABLE_HEADER_FONT_SMALL));
		cell.setBackgroundColor(Color.DARK_GRAY);
		return cell;
	}
	
	
	private double calculateTotalStorage(OpticonObservingRequest observingRequest){
		double observationStorage = 0d;
		
		Map<String,Double> observationStorageMap = new HashMap();
		Map<String,Integer> observationSubbandMap = new HashMap();
		
		int observationcount=0;
		int pipelinecount=0;
		// loop through all observations and get their duration and storage
		for(Observation observation : observingRequest.getObservations()){
			int targetSubbands = 0;
			double observationDuration = 0d;
			char chr = (char) (observationcount+65);
			List targetDurationMap = new ArrayList();
			List targetSubbandMap =new ArrayList();
			List targetThisObservation = new ArrayList();
			OpticonObservation opticonObservation = (OpticonObservation) observation;
			for(Target target : observingRequest.getTargets()){
				OpticonTarget optarget = (OpticonTarget) target;
				Double targetDuration = NorthStarDelegate.getResourceValue(
						optarget.getAllocations(), OpticonConstants.TOTAL_DURATION);
				if (targetDuration != null && optarget.getObservingrun() != null && optarget.getObservingrun().equalsIgnoreCase(String.valueOf(chr))){
					if(optarget.isFirstInRun())
						observationDuration += targetDuration.doubleValue();	
					int targetSubband= SetUpInstrumentBean.convertSubbandListDescription(optarget.getSubbandList());
					 targetSubbands +=targetSubband;
					 targetDurationMap.add(targetDuration);
					 targetSubbandMap.add(targetSubband);
					 targetThisObservation.add(optarget.getRunnumber());
				
				}
	
				
			}
			
			double storage=0;
			
			
			if(opticonObservation.getInstrument() != null && !opticonObservation.getInstrument().isStoreRawData() && 
					opticonObservation.getInstrument().getName()!= null && opticonObservation.getInstrument().getName().contains("Interfero") ){
				opticonObservation.getInstrument().setStoreRawData(true);
				storage = calculateStorage(opticonObservation,observationDuration,targetSubbands);
				opticonObservation.getInstrument().setStoreRawData(false);
			}
			
			//observationStorageMap.put(String.valueOf(chr),storage); //this is done after some line below.
			observationSubbandMap.put(String.valueOf(chr), targetSubbands);
			HashMap dataStore = null;
			
			if(opticonObservation.getInstrument().getName().equals(OpticonConstants.TBB_PIGGYBACK))
				dataStore=calculateBFAndUVSize(opticonObservation,opticonObservation.getInstrument().getTbbExposureTime(), targetSubbands);
			else
				dataStore=calculateBFAndUVSizePerTarget(opticonObservation, targetDurationMap, targetSubbandMap, targetThisObservation)  ;
			
			storage = calculateStorage(dataStore, opticonObservation);
			observationStorageMap.put(String.valueOf(chr), storage);
			if(dataStore!=null)
				observationStorage+= ((BigDecimal) dataStore.get(OpticonConstants.TOTAL_DATA_SIZE)).doubleValue();
			else
				observationStorage+=storage;
			observationcount++;
		}
		
		//calculate pipeline
		for(Observation pipeline : observingRequest.getPipelines()){
			double pipelineAveraging = 0d;
			double pipelineAveragingFreq=0d;
			double pipelineStorage = 0d;
			char chr = (char) (pipelinecount+65);
			OpticonObservation opticonObservation = (OpticonObservation) pipeline;
			if(opticonObservation.getInstrument().getAveragingTime() != null){
				pipelineAveraging=opticonObservation.getInstrument().getAveragingTime().doubleValue();
			}
			if(opticonObservation.getInstrument().getAveragingFrequency()!= null){
				pipelineAveragingFreq=opticonObservation.getInstrument().getAveragingFrequency().doubleValue();
			}
			int targetCount=0;
			for(Target target : observingRequest.getTargets()){
				OpticonTarget optarget = (OpticonTarget) target;
				//if(optarget.getPipeline() != null && optarget.getPipeline().equalsIgnoreCase(String.valueOf(chr) )){
				if (optarget != null && optarget.getObservingrun() != null && optarget.getPipeline() != null
						&& optarget.getPipeline().equalsIgnoreCase(String.valueOf(chr))) {
					// pipelinestorage is the sum data of the targets divided by integration time, 
					// each target generates the amount of raw data to the observation it belongs for its share of subbands... 
					double thisObservationStorage = observationStorageMap.get(optarget.getObservingrun()).doubleValue();
					if (optarget.getObservingrun() != null && optarget.getObservingrun().length() > 0
							&& observationStorageMap.get(optarget.getObservingrun()) != null) {
						thisObservationStorage = observationStorageMap.get(optarget.getObservingrun());
					}
					int thisTargetSubband = SetUpInstrumentBean.convertSubbandListDescription(optarget.getSubbandList());
					int observationSubbands = observationSubbandMap.get(optarget.getObservingrun());
					
					if(observationSubbands*pipelineAveraging > 0){
						double pipelineStorageTarget= (thisObservationStorage*thisTargetSubband)/(observationSubbands*pipelineAveraging*pipelineAveragingFreq);//calculateStorage(opticonObservation,totalDuration,targetSubbands);
						pipelineStorage+=pipelineStorageTarget;
						 log.warn("["+optarget.getObservingrun()+","+observationStorage+","+
								 thisTargetSubband+","+observationSubbands+","+pipelineAveraging+","+pipelineAveragingFreq+","+pipelineStorageTarget+","+pipelineStorage+"]");
										
					}
					targetCount++;
				}
			}
			
			pipelinecount++;
			double pipelineStorageFactor=getPipelineStorageFactor(opticonObservation.getInstrument().getProcessingMode());
			pipelineStorage=pipelineStorage*pipelineStorageFactor;
			log.warn("total storage for:"+String.valueOf(chr) +" is:"+ pipelineStorage);
			log.warn("observationStorage is : "+observationStorage);
			//BigDecimal pipelinebd = new BigDecimal(pipelineStorage);
			observationStorage+=pipelineStorage;
		}
		
		
		return observationStorage+0.01;
	}
	
	
	private double calculateStorage(HashMap dataStore, OpticonObservation opticonObservation) {
		double storage = 0.0;
		if (dataStore != null) {

			BigDecimal beam = (BigDecimal) dataStore.get(OpticonConstants.BEAM_DATA_SIZE);
			if (beam != null) {
				storage += beam.doubleValue();
			}
		} 
		
		BigDecimal uv = new BigDecimal(0);
		if (dataStore != null) {
			uv = (BigDecimal) dataStore.get(OpticonConstants.CORRELATED_VISIBILITY_DATA_SIZE);
			if (uv != null) {
				storage += uv.doubleValue();
			} 
		}
		BigDecimal tbb = new BigDecimal(0);
		if (dataStore != null) {
			tbb = (BigDecimal) dataStore.get(OpticonConstants.TBB_DATA_SIZE);
			if (tbb != null) {
				storage += tbb.doubleValue();
			}
		}
		BigDecimal totalData = new BigDecimal(0);

		if (dataStore != null)
			totalData = (BigDecimal) dataStore.get(OpticonConstants.TOTAL_DATA_SIZE);
		if (totalData.doubleValue() > 0.0)
			storage = totalData.doubleValue();
		return storage;
	}

	private double getPipelineStorageFactor(String processingMode) {
		double pipelineStorageFactor = 0;
		if(processingMode != null && !processingMode.equalsIgnoreCase("")){
			try {
				File fXmlFile = new File(contextPath+OpticonConstants.CONFIG_FILE_LOCATION);
				if(fXmlFile.exists()){
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					org.w3c.dom.Document doc = dBuilder.parse(fXmlFile);
					doc.getDocumentElement().normalize();
					
					NodeList nListPipelineStorageFactor = doc.getElementsByTagName("PipelineStorageFactor");
					
					for (int temp = 0; temp < nListPipelineStorageFactor.getLength(); temp++) {
						Node nNode = nListPipelineStorageFactor.item(temp);
						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElement = (Element) nNode;
							double factor = eElement.getTextContent()!= null ? Double.parseDouble(eElement.getTextContent()) : 0;
							String processingModeStr = eElement.getAttribute("processingMode") != null ? eElement.getAttribute("processingMode") : "";
							String[] processingModeArray = processingModeStr.split(",");
							for (String finalProcessingMode : processingModeArray) {
								if(processingMode.equalsIgnoreCase(finalProcessingMode.trim())){
									pipelineStorageFactor = factor;
								}
							}
						}
					}
				}
			} catch (RemoteException e1) {
				log.error(e1.getMessage());
			} catch (ParserConfigurationException e1) {
				log.error(e1.getMessage());
			} catch (SAXException e1) {
				log.error(e1.getMessage());
			} catch (IOException e1) {
				log.error(e1.getMessage());
			}
		}
		
		if(pipelineStorageFactor == 0){
			List pipelineStorageFactorList = new ArrayList();
			String pipelineStorageFactorValue = new String();
			pipelineStorageFactorList = OptionsUtils.getLabelValueBeans(Constants.PIPELINE_STORAGE_FACTOR, new HashMap(), contextConfiguration);
			
			if (pipelineStorageFactorList != null && pipelineStorageFactorList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) pipelineStorageFactorList.get(1);
				pipelineStorageFactorValue =  urlValue.getValue(); 
			}
			if (!AstronValidator.isBlankOrNull(pipelineStorageFactorValue)) {
				pipelineStorageFactor = new Double(pipelineStorageFactorValue).doubleValue();
			}
		}
		
		return pipelineStorageFactor;
	}

	private double calculateStorage(OpticonObservation opticonObservation,
			Double totalDuration, int subbands) {
		
		double result = 0;
		if(sCalc == null){
			log.warn("binding calculator");
			sCalc = SetUpInstrumentBean.bindCalc(contextConfiguration);
		}
		if(sCalc != null){
			try {
				result = SetUpInstrumentBean.calculateStorage(opticonObservation,totalDuration.doubleValue(),subbands,sCalc).doubleValue();
				
			} catch (ServiceException e) {
				log.warn(e.getMessage());
				//throw new InvalidConfigurationException(e);
			}
		}else{
			log.warn("could not bind calculator");
		}
		
		return result;
	}
	
	private HashMap calculateBFAndUVSize(
			OpticonObservation opticonObservation, Double totalDuration,
			int subbands) {

		HashMap result = new HashMap();
		if(totalDuration==null)
			totalDuration=0d;
		if (sCalc == null) {
			log.warn("binding calculator");
			sCalc = SetUpInstrumentBean.bindCalc(contextConfiguration);
		}
		if (sCalc != null) {
			try {
				result = SetUpInstrumentBean.calculateBFAndUVSize(
						opticonObservation, totalDuration.doubleValue(),
						subbands, sCalc);

			} catch (ServiceException e) {
				log.warn(e.getMessage());
				// throw new InvalidConfigurationException(e);
			}
		}else{
			log.warn("could not bind calculator");
		}
		
		return result;
	}
	
	/**
	 * The addOverallSchedulingRequirements adds Overall scheduling requirements
	 * 
	 * @param observingRequest
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addSchedulingDetails(
			ObservingRequest observingRequest, PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		if (!AstronValidator.isBlankOrNull(observingRequest
				.getOverallRequiredSchedConstraints())) {

			cell
					.addElement(getParagraphDefaultTitle("Overall scheduling details"));
			cell.addElement(getParagraphNormal(observingRequest
					.getOverallRequiredSchedConstraints(), EMPTY_NOT_REQUIRED));

		} else {
			cell
					.addElement(getParagraphDefaultTitle("No overall scheduling details"));
		}
		rootTable.addCell(cell);

	}

	
	
	/**
	 * The getTableNumbersCell method generates tablecell with numbers
	 * 
	 * @param numbers
	 * @return
	 */
	protected PdfPCell getTableNumbersCellSmall(String numbers) {
		PdfPCell cell = new PdfPCell();
		if (!AstronValidator.isBlankOrNull(numbers)) {
			cell.setPhrase(new Phrase(numbers, TABLE_CONTENT_FONT_SMALL));
		} else {
			cell.setPhrase(new Phrase(EMPTY_NOT_REQUIRED, TABLE_CONTENT_FONT_SMALL));
		}
		return cell;
	}
	
	
	private List createSummaryColumns(FieldsDefinitionType fieldsDefinitionType){
		
        List summaryColumns = new ArrayList();
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SELECT_RUNNUMBER, fieldsDefinitionType)){
        	summaryColumns.add("Run#");
        }
        summaryColumns.add("Field");
        summaryColumns.add("Ra");
        summaryColumns.add("Dec");
        summaryColumns.add("Epoch");
        summaryColumns.add("Exposure");
        //if (opticonObservationForm.isDisplayTargetExposureTime()){
        //	summaryColumns.add("Exposure");
        //}
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_MOON, fieldsDefinitionType))
		{ 
        	summaryColumns.add("Moon");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING, fieldsDefinitionType)){
        	summaryColumns.add("Seeing");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING_RANGE, fieldsDefinitionType)){
        	summaryColumns.add("SeeingLow");
        	summaryColumns.add("SeeingUp");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_WATER, fieldsDefinitionType)){
        	summaryColumns.add("Water");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SN, fieldsDefinitionType)){
        	summaryColumns.add("SN");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_MAGNITUDE, fieldsDefinitionType)){
        	summaryColumns.add("Magnitude");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX, fieldsDefinitionType)){
        	summaryColumns.add("FluxParams");
        }
        /*if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX_FREQUENCY, fieldsDefinitionType)){
        	summaryColumns.add("FluxFrequency");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECTRAL_INDEX, fieldsDefinitionType)){
        	summaryColumns.add("SpectralIndex");
        }*/
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SKY_QUALITIES, fieldsDefinitionType)){
        	summaryColumns.add("Sky");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_DIAMETER, fieldsDefinitionType)){
        	summaryColumns.add("Diameter");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SUBBAND_LIST, fieldsDefinitionType)){
        	summaryColumns.add("Subbands");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_TOTAL_SUBBAND_LIST, fieldsDefinitionType)){
        	summaryColumns.add("TotalSubbands");
        }        
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_STORAGE, fieldsDefinitionType)){
        	summaryColumns.add("Storage");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_RUNS, fieldsDefinitionType)){
        	summaryColumns.add("Runs");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_CALIBRATION_BEAM, fieldsDefinitionType)){
        	summaryColumns.add("Calibr.");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SINGLE_PER_RUN, fieldsDefinitionType)){
        	summaryColumns.add("Obs.");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SELECT_PIPELINE, fieldsDefinitionType)){
        	summaryColumns.add("Pipe.");
        }
        summaryColumns.add("Comments");
        return summaryColumns;
	}
	
	private HashMap calculateProcessingTime(String mode, String flaggingStrategy,
			String antenna, boolean isDemixing, List<String> demixingSources, int totalStation,
			int totalTargetSubband, double totalObservingDuration, double fieldOfView, double uvRange, double baselineFactor, String processingMode) {

		HashMap result = new HashMap();
		
		if(sCalc == null){
			log.warn("binding calculator");
			sCalc = SetUpInstrumentBean.bindCalc(contextConfiguration);
		}
		if(sCalc != null){
			try {
				result = SetUpInstrumentBean.calculateProcessingTime(mode,flaggingStrategy,antenna,isDemixing,demixingSources,totalStation, totalTargetSubband,totalObservingDuration, fieldOfView,uvRange,sCalc, baselineFactor, contextPath, processingMode );
				
			} catch (ServiceException e) {
				log.warn(e.getMessage());
				//throw new InvalidConfigurationException(e);
			}
		}else{
			log.warn("could not bind calculator");
		}
		
		return result;
		
	}
	
	private HashMap calculateBFAndUVSizePerTarget(
			OpticonObservation opticonObservation,
			List targetDurationMap,
			List targetSubbandMap, List targetThisObservation) {
		// TODO Auto-generated method stub
		HashMap finalResult = new HashMap();
		BigDecimal beam = new BigDecimal(0);
		BigDecimal uv = new BigDecimal(0);
		BigDecimal tbb = new BigDecimal(0);
		BigDecimal total = new BigDecimal(0);

		int i=0;
		while (i<targetThisObservation.size()){
			double totalDuration =(Double) targetDurationMap.get(i);
			int subbands=(Integer) targetSubbandMap.get(i);
			HashMap perTargetDataSize = calculateBFAndUVSize(opticonObservation, totalDuration, subbands);
			if(perTargetDataSize!=null){
				BigDecimal thisBeam =(BigDecimal) perTargetDataSize.get(OpticonConstants.BEAM_DATA_SIZE);
			if(thisBeam!=null)
				beam= beam.add(thisBeam) ;
			BigDecimal thisUv =(BigDecimal) perTargetDataSize.get(OpticonConstants.CORRELATED_VISIBILITY_DATA_SIZE);
			if(thisUv!=null)
				uv= uv.add(thisUv) ;
			
			BigDecimal thisTbb =(BigDecimal) perTargetDataSize.get(OpticonConstants.TBB_DATA_SIZE);
			if(thisTbb!=null)
				tbb= tbb.add(thisTbb) ;
			
			
			BigDecimal thistotal = (BigDecimal) perTargetDataSize.get(OpticonConstants.TOTAL_DATA_SIZE);
			if(thistotal!=null)
			total= total.add(thistotal) ;
			
			
			finalResult.put(OpticonConstants.DATA_STORE_ALLOW, perTargetDataSize.get(OpticonConstants.DATA_STORE_ALLOW));
			finalResult.put(OpticonConstants.DATA_STORE_MODE, perTargetDataSize.get(OpticonConstants.DATA_STORE_MODE));
			finalResult.put(OpticonConstants.UV_DATA_STORE_ALLOWE, perTargetDataSize.get(OpticonConstants.UV_DATA_STORE_ALLOWE));
			}
			i++;
			}
		
		finalResult.put(OpticonConstants.BEAM_DATA_SIZE, beam);
		finalResult.put(OpticonConstants.CORRELATED_VISIBILITY_DATA_SIZE, uv);
		finalResult.put(OpticonConstants.TBB_DATA_SIZE, tbb);
		finalResult.put(OpticonConstants.TOTAL_DATA_SIZE, total);
		
		
		return finalResult;
	}
}