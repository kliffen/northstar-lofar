// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.deadlinepassed;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.SemesterBean;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.Semester;

public class ProcessDeadLinePassedAction extends LockedAction {

	private Log log = LogFactory.getLog(this.getClass());

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		ActionMessages errors = new ActionMessages();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		/*
		 * if not pressed on cancel, it must be an submit
		 */
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		if (!isCancelled(request)) {
			try {
				Semester semester = northStarDelegate
						.getNextSemester(proposalData.getProposal());
				if (semester != null) {
					proposalData.getProposal().setSemester(semester);
					proposalData.getProposal().setCode(null);
					proposalData.setProposalBean(new SemesterBean(semester));
					proposalDelegate.storeWithoutSendingMails(proposalData.getProposal(),
							ownUserAccount, session.getId());
				}
			} catch (DatabaseException de) {
				log.error(LogMessage.getMessage(ownUserAccount,
						"Cannot store proposal: " + de.getMessage(), de));
				errors.add(Constants.ERROR, new ActionMessage(
						"error.database.store.proposal"));
				saveErrors(request, errors);
				return mapping.getInputForward();
			}
			proposalData.setIgnoreDeadLineWarning(false);
			return proposalData.getSuccessWarningForward();
		}else {
			return proposalData.getCancelWarningForward();
		}


	}
}
