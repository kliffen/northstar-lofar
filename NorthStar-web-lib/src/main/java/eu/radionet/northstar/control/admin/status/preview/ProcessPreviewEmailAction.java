// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.status.preview;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.admin.collaboration.AdminLockedAction;
import eu.radionet.northstar.control.admin.status.ChangeStatusBean;
import eu.radionet.northstar.control.util.ParameterAppender;

public class ProcessPreviewEmailAction extends AdminLockedAction {
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		PreviewEmailForm previewEmailForm = (PreviewEmailForm) form;
		HttpSession session = request.getSession();
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (isCancelled(request)) {
			return ParameterAppender.append(mapping.findForward(Constants.DISCARD),
					"proposalId", previewEmailForm.getProposalId());
		}
		
		if(previewEmailForm.getNextButton() != null ){
			ChangeStatusBean changeStatusBean = (ChangeStatusBean) session.getAttribute(Constants.CHANGE_STATUS_BEAN);
			String message = AstronConverter.wrap(changeStatusBean.getStatusEmail().getMessage(), 75);
			String subject = changeStatusBean.getStatusEmail().getSubject();
			proposalDelegate.changeStatus(changeStatusBean.getNewStatusId(),lockedProposal,subject, message, ownUserAccount);
			removeLock(lockedProposal.getId(), request, ownUserAccount);
			session.removeAttribute(Constants.CHANGE_STATUS_BEAN);
			
		}else{
			ChangeStatusBean changeStatusBean = (ChangeStatusBean) session.getAttribute(Constants.CHANGE_STATUS_BEAN);
			proposalDelegate.changeStatusWithoutMail(changeStatusBean.getNewStatusId(),lockedProposal, ownUserAccount);
			removeLock(lockedProposal.getId(), request, ownUserAccount);
			session.removeAttribute(Constants.CHANGE_STATUS_BEAN);
			
		}
		return mapping.findForward(Constants.SUCCESS);
	}
}
