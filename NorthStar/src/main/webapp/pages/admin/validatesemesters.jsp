<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />

<html:form action="/processValidateSemesters" method="POST">

<TABLE>

	<tr>
		<td class="right">Server time: &nbsp;</td>
		<td class="left"><bean:write name="validateSemestersForm" property="serverTime"/></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<tr>
		<td class="right">Server time (UTC): &nbsp;</td>
		<td class="left"><bean:write name="validateSemestersForm" property="serverUTCTime"/></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
	</tr>	
	<tr>
		<td class="right">Time to validate(yyyy/MM/dd HH:mm:ss):&nbsp;</td>
		<td class="left"><html:text property="validateTime" tabindex="2"  maxlength="255"/><span class="error"><astron:errors property="validateTime" ifnotexist="*"/></span></td>
	</tr>
	<tr>
		<td>&nbsp;</td>
		<td align="left"><html:submit property="validateButton" >Validate semesters</html:submit>
	</td>
	<tr>
		<td>Next semesters:</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>		
	<TR>
		<TD colspan="2">
			<table align="center" class="projecttable">
				<TR>
					<td class="tableheader">&nbsp;Semester&nbsp;</td>
					<td class="tableheader">&nbsp;Telescope&nbsp;</td>					
					<td class="tableheader">&nbsp;Category&nbsp;</td>
     				<td class="tableheader">&nbsp;Start Time&nbsp;</td>
     				<td class="tableheader">&nbsp;End Time&nbsp;</td>
     				<td class="tableheader">&nbsp;Dead Line&nbsp;</td>
					<td class="tableheader">&nbsp;Last assigned code&nbsp;</td>     				
     				<td class="tableheader">&nbsp;Errors&nbsp;</td>
				</TR>
			<%
			int i=0;
			String styleClass = "";
			%>
	 			<logic:iterate id="semesterBean" name="validateSemestersForm" property="semesterBeans">
	 			<%
	 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
	 			%>
 	 			<TR>
					<TD class="tablefield_pri<%=styleClass%>" valign="top"><bean:write name="semesterBean" property="semesterCode"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="semesterBean" property="telescope"/></TD>					
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="semesterBean" property="category"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="semesterBean" property="startTime"/></TD>					
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="semesterBean" property="endTime"/></TD>
					<TD class="tablefield<%=styleClass%>"  valign="top"><bean:write name="semesterBean" property="deadLineTime"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="semesterBean" property="lastAssignedCode"/></TD>

					<TD class="tablefield<%=styleClass%>" valign="top"><span class="error"><bean:write name="semesterBean" property="error"/></span></TD>

	 			</TR>

	 			</logic:iterate>
				<tr><td colspan="8" class="spacer">&nbsp;</td></tr>
			</table>
		</TD>
	</TR>

	

	
</TABLE>

</html:form>

