// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessUploadAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: ProcessUploadAction.java,v 1.20 2008-12-03 11:49:43 boelen Exp $
 *
 */
package eu.radionet.northstar.control.proposal.upload;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import nl.astron.util.tar.TarEntry;
import nl.astron.util.tar.TarInputStream;
import nl.astron.util.tar.TarOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.LabelValueBean;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;

import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.exception.BadJustificationFileException;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.util.PdfUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.FigureFile;
import eu.radionet.northstar.data.entities.Justification;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ScientificFile;
import eu.radionet.northstar.data.entities.TechnicalDetailsFile;

/**
 * The ProcessUploadAction provides
 * 
 * @author Bastiaan Verhoef
 * 
 */
public class ProcessUploadAction extends LockedAction {
	private Log log = LogFactory.getLog(this.getClass());
	//private byte[] justificationFile = null;
	//private Integer justificationFilePages = null;
	//private ProposalData proposalData = null;
	
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		
		UploadForm uploadForm = (UploadForm) form;
    	ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);

		/*
		 * if not pressed on cancel, it must be an submit
		 */
		if (!isCancelled(request)) {
			proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposalData.getProposal());
			TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
			ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());
	    	FieldsDefinitionType fieldsDefinitionType = 
				(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
	 
			if (proposalData.getProposal().getJustification() == null) {
				proposalData.getProposal().setJustification(new Justification());
			}
			
			if (OptionsDelegate.allowedToDisplay(Constants.SINGLE_FILE_UPLOAD , fieldsDefinitionType)){
				return processSingleFileButtons(uploadForm,proposalData,mapping,request,response);
	    	}else{
	    		return processMultiFileButtons(uploadForm,proposalData,mapping,request,response);
	    	}
				
		}
		
		//cancel has been pressed...
		return mapping.findForward("success");

	}
		
	private ActionForward processSingleFileButtons(UploadForm uploadForm,
			ProposalData proposalData, ActionMapping mapping, HttpServletRequest request, HttpServletResponse response) throws IOException, DocumentException, DatabaseException {
		HttpSession session = request.getSession();
		Integer justificationFilePages = new Integer(0);
		// here the file is converted to pdf and the pages are being counted.
		justificationFilePages = getJustificationFilePages(uploadForm, session,response,proposalData.getProposal());
		if (justificationFilePages.intValue() < 0 ){
			return null;
		}
		byte[] justificationFile = uploadForm.getJustificationFile().getFileData();//getByteArray(uploadForm.getJustificationFile().get );
					
		ActionMessages errors = new ActionMessages();

		errors = validate(uploadForm, mapping, request);
		if (errors.size() > 0) {
			saveErrors(request, errors);
			return mapping.getInputForward();
		}
		Justification justification = proposalData.getProposal()
		.getJustification();
		if (uploadForm.getKindUploadFile().equals(Constants.SCIENTIFIC)) {
			
			justification.setScientificFileName(uploadForm
					.getJustificationFile().getFileName());
			justification.setScientificFileSize(new Integer(uploadForm
					.getJustificationFile().getFileSize()));
			Calendar UTCCal = new GregorianCalendar(TimeZone
					.getTimeZone("UTC"));
			justification.setScientificFileDate(UTCCal.getTime());
			justification.setScientificFilePages(justificationFilePages);
			ScientificFile scientificFile = proposalDelegate
					.getScientificFile(justification);
			if (scientificFile == null) {
				scientificFile = new ScientificFile();
			}
			scientificFile.setUploadFile(justificationFile);
			proposalData.setScientificFile(scientificFile);
		} else if (uploadForm.getKindUploadFile().equals(
				Constants.TECHNICAL)) {

			justification.setTechnicalDetailsFileName(uploadForm
					.getJustificationFile().getFileName());
			justification.setTechnicalDetailsFileSize(new Integer(
					uploadForm.getJustificationFile().getFileSize()));
			Calendar UTCCal = new GregorianCalendar(TimeZone
					.getTimeZone("UTC"));
			justification.setTechnicalDetailsFileDate(UTCCal.getTime());
			justification
					.setTechnicalDetailsFilePages(justificationFilePages);
			TechnicalDetailsFile technicalDetailsFile = proposalDelegate
					.getTechnicalDetailsFile(justification);
			if (technicalDetailsFile == null) {
				technicalDetailsFile = new TechnicalDetailsFile();
			}
			technicalDetailsFile.setUploadFile(justificationFile);

			proposalData.setTechnicalDetailsFile(technicalDetailsFile);
			proposalData.setDeleteTechnicalDetailsFile(false);

		} else if (uploadForm.getKindUploadFile().equals(Constants.FIGURE)) {
			String fileName = uploadForm.getJustificationFile().getFileName();
			FigureFile figureFile = proposalDelegate.getFigureFile(justification);
			if (figureFile == null) {
				figureFile = new FigureFile();
			}
			// single file don't need tarred figures
			//}else{
			justification.setFigureFileName(uploadForm.getJustificationFile().getFileName());
			justification.setFigureFileSize(new Integer(uploadForm.getJustificationFile().getFileSize()));
			figureFile.setUploadFile(justificationFile);
		
			Calendar UTCCal = new GregorianCalendar(TimeZone
					.getTimeZone("UTC"));
			justification.setFigureFileDate(UTCCal.getTime());
			justification.setFigureFilePages(justificationFilePages);
			proposalData.setFigureFile(figureFile);

			// nv050405, bugfix
			proposalData.setDeleteFigureFile(false);
		}
		return mapping.findForward("success");

	}


	private ActionForward processMultiFileButtons(UploadForm uploadForm,
			ProposalData proposalData, ActionMapping mapping, HttpServletRequest request, HttpServletResponse response) throws IOException, DatabaseException, InvalidConfigurationException {
		HttpSession session = request.getSession();
		if(uploadForm.getAttachFileButton() != null){
			Justification justification = proposalData.getProposal().getJustification();
			String fileName = uploadForm.getJustificationFile().getFileName();
			if(fileName.endsWith(".tex") || fileName.endsWith(".pdf") || fileName.endsWith(".PDF")){
				justification.setScientificFileName(uploadForm
						.getJustificationFile().getFileName());
				justification.setScientificFileSize(new Integer(uploadForm
						.getJustificationFile().getFileSize()));
				Calendar UTCCal = new GregorianCalendar(TimeZone
						.getTimeZone("UTC"));
				justification.setScientificFileDate(UTCCal.getTime());
				//justification.setScientificFilePages(justificationFilePages);
				ScientificFile scientificFile = proposalDelegate.getScientificFile(justification);
				if (scientificFile == null) {
					scientificFile = new ScientificFile();
				}
				scientificFile.setUploadFile(uploadForm.getJustificationFile().getFileData());
				proposalData.setScientificFile(scientificFile);
				
			}else if (fileName.length() > 4){
				byte[] justificationFile = uploadForm.getJustificationFile().getFileData();
				
				FigureFile figureFile = proposalData.getFigureFile();
				if(figureFile==null){
					// also check database...
					figureFile = proposalDelegate.getFigureFile(justification);
				}
				
				// FIXME ugly check if figure file is empty
				if(!fileName.endsWith(".tar")){
					if (figureFile == null) {
						figureFile = createnewFile(fileName,  justificationFile, justification);
					}else{
							if(justification.getFigureFileName() == null){
								figureFile = createnewFile(fileName,  justificationFile, justification);
							}else{
								// append figure file
								byte[] tarFile = appendFigureFile(justificationFile,figureFile,fileName);
								figureFile.setUploadFile(tarFile);
							}
					}
					
					
					
				}else{
					// just overwrite the tar file
					justification.setFigureFileName("figures.tar");//uploadForm.getJustificationFile().getFileName()
					justification.setFigureFileSize(new Integer(uploadForm.getJustificationFile().getFileSize()));
					figureFile.setUploadFile(justificationFile);
				}
				
				Calendar UTCCal = new GregorianCalendar(TimeZone
						.getTimeZone("UTC"));
				justification.setFigureFileDate(UTCCal.getTime());
				//justification.setFigureFilePages(justificationFilePages);
				proposalData.setFigureFile(figureFile);

				// nv050405, bugfix
 				proposalData.setDeleteFigureFile(false);

			}else{
				// errors.add pls attach a valid file
				return mapping.findForward("refresh");
			}
			session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
			return mapping.findForward("refresh");
			
		}

		if(uploadForm.getDeleteFileButton() != null){
			if(uploadForm.getSelectedFile() != null){
				String selectedFile = uploadForm.getSelectedFile();
				if(selectedFile.endsWith(".tex")){
					proposalData.getProposal().getJustification().setScientificFileDate(null);
					proposalData.getProposal().getJustification().setScientificFileName(null);
					proposalData.getProposal().getJustification().setScientificFilePath(null);
					proposalData.getProposal().getJustification().setScientificFileSize(null);
					proposalData.setScientificFile(null);
				}else{
					// remove the file from the tar...
					FigureFile figureFile = removeFigureFile(proposalData,selectedFile);
					if(figureFile != null){
						proposalData.setFigureFile(figureFile);
					}else{
						// if it is the last figure file, remove the empty tar.
						uploadForm.setJustificationFile(null);
						proposalData.setFigureFile(null);
						proposalData.getProposal().getJustification().setFigureFileDate(null);
						proposalData.getProposal().getJustification().setFigureFileName(null);
						proposalData.getProposal().getJustification().setFigureFilePath(null);
						proposalData.getProposal().getJustification().setFigureFileSize(null);
					}
				}
			}else{
				// wrong: please select a file for removal
				/*
				ActionMessages errors = new ActionMessages();
				errors.add("justificationFile", new ActionMessage(
				"error.select.file"));
				saveErrors(request, errors);
				return mapping.getInputForward();
				 */
			}
			session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
			return mapping.findForward("refresh");
		}

		if(uploadForm.getPreviewFileButton() != null){
			PdfUtils pdfUtils = new PdfUtils();
			byte[] bo = pdfUtils.getPreviewPdf(proposalData,request);
			if(bo==null){
				String convertErrors = pdfUtils.getConvertErrors();
				bo=convertErrors.getBytes();
				response.setContentType("text/html");
			}else{
				response.setContentType("application/pdf");
				response.setHeader("Content-disposition", "attachment; filename=justification_preview.pdf");
				if (getBrowser(request).equals("msie")) {
					response.setHeader("Cache-Control", null);
					response.setHeader("Pragma", null);
				}
			}
			
			response.setContentLength(bo.length);
			//response.setHeader("Content-disposition", "attachment; filename=latex_output.log");
			//response.setHeader("Cache-Control", "max-age=300");

			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(bo);
			outStream.flush();
			return null;
			
		}
		
		return mapping.findForward("success");
		
	}


	private FigureFile createnewFile(String fileName, byte[] justificationFile, Justification justification) throws IOException {
		// create a new tar file
		FigureFile figureFile = new FigureFile();
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		TarOutputStream tout = new TarOutputStream(bout);
		
		// create tar of single image file.
		TarEntry tarEntry = new TarEntry(fileName);
		tarEntry.setSize(justificationFile.length);
		tout.putNextEntry(tarEntry);
		tout.write(justificationFile);
		tout.closeEntry();
		
		tout.flush();
		tout.close();
		
		justification.setFigureFileName("figures.tar");
		justification.setFigureFileSize(new Integer(bout.size()));
		Calendar UTCCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		justification.setFigureFileDate(UTCCal.getTime());
		
		figureFile.setUploadFile(bout.toByteArray());
		return figureFile;
		
	}


	private byte[] appendFigureFile(byte[] justificationFile,
			FigureFile figureFile, String fileName) {
		InputStream inp = new ByteArrayInputStream(figureFile.getUploadFile());
		TarInputStream tis = new TarInputStream(inp);
		
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		TarOutputStream tout = new TarOutputStream(bout);
		TarEntry ter;
		
		try {
			while( (ter=tis.getNextEntry()) != null){
				// create new entry
				TarEntry tarEntry = new TarEntry(ter.getName());
				tarEntry.setSize(ter.getSize());
				tout.putNextEntry(tarEntry);
				
				// get file from the old archive
				if (!ter.isDirectory()){
					ByteArrayOutputStream lout = new ByteArrayOutputStream();
					tis.copyEntryContents(lout);
					// write to the new one
					tout.write(lout.toByteArray());
				}
				tout.closeEntry();
			}
			// now append final file:
			
			TarEntry tarEntry = new TarEntry(fileName);
			tarEntry.setSize(justificationFile.length);
			tout.putNextEntry(tarEntry);
			
			// write to the new one
			tout.write(justificationFile);
			tout.closeEntry();
			
			tout.flush();
			tout.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			log.warn("file not found ");
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			log.warn("IO error");
			e.printStackTrace();
		}
		return bout.toByteArray();
	}


	private FigureFile removeFigureFile(ProposalData proposalData,String selectedFile) {
		FigureFile figureFile=proposalData.getFigureFile();
		if (figureFile==null){
				figureFile = proposalDelegate.getFigureFile(proposalData.getProposal().getJustification());
				// no figurefile in session, and no figurefile in database, so it is already gone.
				if(figureFile==null){
					return null;
				}
		}
		// create input stream for reading the archive
		InputStream inp = new ByteArrayInputStream(figureFile.getUploadFile());
		TarInputStream tis = new TarInputStream(inp);
		TarEntry ter;
		
		//create outputstream for writing the new one
		ByteArrayOutputStream bout = new ByteArrayOutputStream();
		TarOutputStream tout = new TarOutputStream(bout);
		boolean found = false;
		int figureCount=0;
		try {
			while( (ter=tis.getNextEntry()) != null){
				if(ter.getName().equalsIgnoreCase(selectedFile)){
					found=true;
					// and do not add to the tar.
				}else{
					// create new entry
					TarEntry tarEntry = new TarEntry(ter.getName());
					tarEntry.setSize(ter.getSize());
					tout.putNextEntry(tarEntry);
					ByteArrayOutputStream lout = new ByteArrayOutputStream();
					tis.copyEntryContents(lout);
					// get file from the old archive
					/*File file = ter.getFile();
					InputStream is = new FileInputStream(file);
					byte fileContent[] = new byte[(int)file.length()];
					is.read(fileContent);
					is.close();
					*/
					// write to the new one
					
					tout.write(lout.toByteArray());
					tout.closeEntry();
					figureCount++;
				}
			}
			tout.flush();
			tout.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		if (found && figureCount >0){
			figureFile.setUploadFile(bout.toByteArray());
			return figureFile;
		}
		return null;
	}
	/*
	 * Validate checks the ohter pages for length, and overall length, 
	 */
	private ActionMessages validate(UploadForm uploadForm,
			ActionMapping mapping, HttpServletRequest request)
			throws DatabaseException, FileNotFoundException, IOException, DocumentException {
		
		ActionMessages errors = new ActionMessages();
		HttpSession session = request.getSession();
    	ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		Justification justification = proposalData.getProposal().getJustification();
		Integer scientificFilePages = null;
		Integer technicalDetailsPages = null;
		Integer figureFilePages = null;
		String sessionId = request.getSession().getId();
		
		checkForOldData(session, proposalData);
		try {
			if (uploadForm.getKindUploadFile().equals(Constants.SCIENTIFIC)) {
				scientificFilePages = getJustificationFilePages(uploadForm,	session, null, null); // should be overloaded
				technicalDetailsPages = justification
						.getTechnicalDetailsFilePages();
				figureFilePages = justification.getFigureFilePages();
				proposalDelegate.checkJustificationFiles(proposalData
						.getProposal(), scientificFilePages,
						technicalDetailsPages, figureFilePages);
				
			} else if (uploadForm.getKindUploadFile().equals(
					Constants.TECHNICAL)) {
				scientificFilePages = justification.getScientificFilePages();
				technicalDetailsPages = getJustificationFilePages(uploadForm,	session, null, null);
				figureFilePages = justification.getFigureFilePages();
				proposalDelegate.checkJustificationFiles(proposalData
						.getProposal(), scientificFilePages,
						technicalDetailsPages, figureFilePages);

			} 
			else if (uploadForm.getKindUploadFile().equals(Constants.FIGURE)) {
				scientificFilePages = justification.getScientificFilePages();
				technicalDetailsPages = justification.getTechnicalDetailsFilePages();
				// here figurefilepages are counted the normal way
				figureFilePages = getJustificationFilePages(uploadForm,	session, null, null);
				// scientific pages have been uploaded once
				if (scientificFilePages != null && scientificFilePages.intValue() > 0){
					PdfUtils pdfUtils = new PdfUtils();
					Proposal proposal = proposalData.getProposal();
					String figureName = uploadForm.getJustificationFile().getFileName();
					byte[] figureFile = uploadForm.getJustificationFile().getFileData(); 
					
					if (figureName != null && figureName.endsWith(".tar")){
						// fetch scientific file from session
						ScientificFile scientificFile = proposalData.getScientificFile();
						if (proposalData.getScientificFile() == null){
							// fetch scientific file from database
							
								scientificFile = proposalDelegate.getScientificFile(justification);
							
						}
						
						//getByteArray(uploadForm.getJustificationFile().getInputStream());
						if (scientificFile != null && figureFile != null ){
							scientificFilePages = new Integer(pdfUtils.getNumberLatexPages(scientificFile.getUploadFile(), 
										figureFile, proposal, session.getServletContext()));
							
						}
					}
				}
				proposalDelegate.checkJustificationFiles(proposalData
						.getProposal(), scientificFilePages,
						technicalDetailsPages, figureFilePages);

			}
		} catch (BadJustificationFileException bjfe) {
			if (bjfe.isOverall()) {
				errors.add("justificationFile", new ActionMessage(
						"error.exceed.maximum.pages.overall"));
			} else if (bjfe.isScientificFile()) {
				errors.add("justificationFile", new ActionMessage(
						"error.exceed.maximum.pages.scientificfile"));
			} else if (bjfe.isTechnicalDetailsFile()) {
				errors.add("justificationFile", new ActionMessage(
						"error.exceed.maximum.pages.technicaldetailsfile"));
			} else if (bjfe.isFigureFile()) {
				errors.add("justificationFile", new ActionMessage(
						"error.exceed.maximum.pages.figurefile"));
			}
		}
		return errors;
	}

	/*
	 * checkForOldData removes the stored page numbers if the files are empty.
	 * or recalculates them if there are files but no number of pages.
	 */
	private void checkForOldData(HttpSession session, ProposalData proposalData) throws DatabaseException,
			IOException, DocumentException {
		PdfUtils pdfUtils = new PdfUtils();
		Justification justification = proposalData.getProposal()
				.getJustification();
		if (justification.getScientificFileName() == null && justification.getScientificFilePages() != null){
			justification.setScientificFilePages(null);
		}
		if (justification.getTechnicalDetailsFileName() == null && justification.getTechnicalDetailsFilePages() != null){
			justification.setTechnicalDetailsFilePages(null);
		}
		if (justification.getFigureFileName() == null && justification.getFigureFilePages() != null){
			justification.setFigureFilePages(null);
		}
		if (justification.getScientificFilePages() == null
				&& !AstronValidator.isBlankOrNull(justification
						.getScientificFileName())) {
			ScientificFile scientificFile = proposalDelegate
					.getScientificFile(justification);
			if (scientificFile != null) {
				String fileType = justification.getScientificFileName();
				fileType = fileType.substring(fileType.lastIndexOf('.'));
				justification.setScientificFilePages(
						new Integer(pdfUtils.getNumberOfPages(scientificFile.getUploadFile(), 
							fileType, session)));
			} else {
				justification.setScientificFilePages(new Integer(0));
			}
		}
		if (justification.getTechnicalDetailsFilePages() == null
				&& !AstronValidator.isBlankOrNull(justification
						.getTechnicalDetailsFileName())) {
			TechnicalDetailsFile technicalDetailsFile = proposalDelegate
					.getTechnicalDetailsFile(justification);
			if (technicalDetailsFile != null) {
				String fileType = justification.getScientificFileName();
				fileType = fileType.substring(fileType.lastIndexOf('.'));
				justification.setTechnicalDetailsFilePages(new Integer(pdfUtils
						.getNumberOfPages(technicalDetailsFile.getUploadFile(),
								fileType, session)));
			} else {
				justification.setTechnicalDetailsFilePages(new Integer(0));
			}
		}
		if (justification.getFigureFilePages() == null
				&& !AstronValidator.isBlankOrNull(justification
						.getFigureFileName())) {
			FigureFile figureFile = proposalDelegate
					.getFigureFile(justification);
			if (figureFile != null) {
				String fileType = justification.getScientificFileName();
				fileType = fileType.substring(fileType.lastIndexOf('.'));
				justification.setFigureFilePages(new Integer(pdfUtils.getNumberOfPages(
						figureFile.getUploadFile(), fileType, session)));
			} else {
				justification.setFigureFilePages(new Integer(0));
			}
		}
	}

	private byte[] getByteArray(BufferedInputStream inputStream) throws IOException {
		int byteChar = inputStream.read();

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		while (byteChar != -1) {
			out.write(byteChar);
			byteChar = inputStream.read();
		}
		return out.toByteArray();
	}
	
	
	protected Integer getJustificationFilePages(UploadForm uploadForm,  HttpSession session, 
			HttpServletResponse response, Proposal proposal)
	throws IOException, DocumentException
	{
		PdfUtils pdfUtils = new PdfUtils();
		
		if(uploadForm.getJustificationFile()==null)
			return (new Integer(0));
		
			// uploadForm.getJustificationFile().getContentType();
		byte[] justificationFile = uploadForm.getJustificationFile().getFileData(); //getByteArray(uploadForm.getJustificationFile().getInputStream());
		String latexError = null;
		// fetch defined header from options file.
		
		if(proposal==null && session != null){
			ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
			proposal = proposalData .getProposal();
		}
	
		String fileType = uploadForm.getJustificationFile().getFileName();
		if(fileType == null){
			log.warn("no filename given");
			return (new Integer(0));
		}
		fileType = fileType.substring(fileType.lastIndexOf('.'));
		
		// check if a required header is defined
    	TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		String definedHeader = null;
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		List urlList = new ArrayList();
		String url = new String();
		urlList = OptionsUtils.getLabelValueBeans(Constants.LATEX_REQUIRED_HEADER, new HashMap(), contextConfiguration);
		
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			url =  urlValue.getValue(); 
		} 
		
		definedHeader = url;
				
		if (definedHeader != null && fileType.equalsIgnoreCase("tex")){
			String testHeader =definedHeader.replaceAll("\\s", "");
			
			// check header
			// maybe also check file length when reading file into a string.
			String texFile = new String(justificationFile);
			if (texFile.length() > 4000){
				texFile = texFile.substring(0, 4000);
			}
			// remove special characters (spaces, returns etc.)
			texFile = texFile.replaceAll("\\s", "");
			if (!texFile.startsWith(testHeader)){
				latexError = "incorrect latex header \nrequired header is: \n\n";
				latexError += definedHeader;
				latexError += "\n\nsupplied header was: \n\n";
				latexError += new String(justificationFile);
			}
		}
		// convert to pdf and count pages...
		int calcFilePages = pdfUtils.getNumberOfPages(justificationFile, uploadForm.getJustificationFile().getFileName(), session);
		
		if(calcFilePages < 0 || latexError != null){
			if (latexError == null){
				latexError = pdfUtils.getConvertErrors();
			}
			else{
				calcFilePages = -1;
			}
			
			byte[] bo = latexError.getBytes();
			
			response.setContentLength(bo.length);
			response.setContentType("text/html");

			//response.setHeader("Content-disposition", "attachment; filename=latex_output.log");
			//response.setHeader("Cache-Control", "max-age=300");

			ServletOutputStream outStream = response.getOutputStream();
			outStream.write(bo);
			outStream.flush();
		}
		
		return new Integer(calcFilePages);

	}
	
	protected String getBrowser(HttpServletRequest request) {

		String useragent = request.getHeader("User-Agent").toLowerCase();
		if (useragent.matches(".*msie.*")) {
			return "msie";
		} else if (useragent.matches(".*gecko.*")) {
			return "gecko";
		} else {
			return "other";
		}

	}
}

