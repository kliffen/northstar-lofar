
DROP TABLE IF EXISTS `resource`;
DROP TABLE IF EXISTS `resourcetype`;
DROP TABLE IF EXISTS `observationallocation`;
DROP TABLE IF EXISTS `observingrequestallocation`;
CREATE TABLE `resourcetype` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `unit` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;


CREATE TABLE `resource` (
  `id` int(11) NOT NULL auto_increment,
  `resourcetypeid` int(11) NOT NULL,
  `value` double NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `resourcetype_resource_IND` (`resourcetypeid`),
  CONSTRAINT `resourcetype_resource_FK` FOREIGN KEY (`resourcetypeid`) REFERENCES `resourcetype` (`id`)
) TYPE=InnoDB;

CREATE TABLE `observationallocation` (
  `id` int(11) NOT NULL auto_increment,
  `resourceid` int(11) default NULL,
  `observationid` int(11) default NULL,
  `allocationkey` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observation_observationallocation_IND` (`observationid`),
  KEY `resource_observationallocation_IND` (`resourceid`),
  KEY `observation_key_observationallocation_IND` (`allocationkey`),
  CONSTRAINT `observation_observationallocation_FK` FOREIGN KEY (`observationid`) REFERENCES `observation` (`id`),
  CONSTRAINT `resource_observationallocation_FK` FOREIGN KEY (`resourceid`) REFERENCES `resource` (`id`) ON DELETE CASCADE
) TYPE=InnoDB;

CREATE TABLE `observingrequestallocation` (
  `id` int(11) NOT NULL auto_increment,
  `resourceid` int(11) default NULL,
  `observingrequestid` int(11) default NULL,
  `allocationkey` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observingrequest_observingrequestallocation_IND` (`observingrequestid`),
  KEY `resource_observingrequestallocation_IND` (`resourceid`),
  KEY `observingrequest_key_observingrequestallocation_IND` (`allocationkey`),
  CONSTRAINT `observingrequest_observingrequestallocation_FK` FOREIGN KEY (`observingrequestid`) REFERENCES `observingrequest` (`id`),
  CONSTRAINT `resource_observingrequestallocation_FK` FOREIGN KEY (`resourceid`) REFERENCES `resource` (`id`) ON DELETE CASCADE
) TYPE=InnoDB;

CREATE TABLE `optioncollection` (
  `id` int(11) NOT NULL auto_increment,
  `key` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;

CREATE TABLE `options` (
  `id` int(11) NOT NULL auto_increment,
  `optioncollectionid` int(11) default NULL,
  `indexid` int(11) default NULL,
   `value` varchar(255) default NULL,
  `label` varchar(255) default NULL,

  PRIMARY KEY  (`id`),
  KEY `optioncollection_options_IND` (`optioncollectionid`),
  KEY `optioncollection_index_options_IND` (`indexid`),
  CONSTRAINT `optioncollection_options_FK` FOREIGN KEY (`optioncollectionid`) REFERENCES `optioncollection` (`id`)
) TYPE=InnoDB;

INSERT INTO `resourcetype` (`id`,`name`,`unit`) VALUES (1,'time','hours'),(2,'time','minutes'),(3,'time','seconds');
 
 