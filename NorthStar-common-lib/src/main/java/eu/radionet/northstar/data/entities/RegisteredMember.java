// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * RegisteredMember.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: RegisteredMember.java,v 1.1 2006-05-02 10:01:10 verhoef Exp $
 *
 */
package eu.radionet.northstar.data.entities;

import java.util.Iterator;

import nl.astron.useradministration.data.entities.User;
import nl.astron.useradministration.data.entities.UserAffiliation;
import nl.astron.util.AstronValidator;

/**
 * The RegisteredMember provides
 * 
 * @author Bastiaan Verhoef
 */
public class RegisteredMember extends Member {
	protected Integer userId = null;

	protected User user = null;


	public String getName() {
		if (user != null) {
			String name = null;
			if (AstronValidator.isBlankOrNull(user.getTitle())) {
				name = user.getFirstName();
			} else {
				name = user.getTitle() + " " + user.getFirstName();
			}
			name += " " + user.getLastName();
			return name;
		}
		return null;
	}

	public String getAffiliation() {
		if (user != null) {
			Iterator userAffiliations = user.getUserAffiliations().iterator();
			while (userAffiliations.hasNext()) {
				UserAffiliation userAffiliation = (UserAffiliation) userAffiliations
						.next();
				if (userAffiliation.isPreferred()) {
					String nameDepartment = userAffiliation.getAffiliation()
							.getName();
					if (!AstronValidator.isBlankOrNull(userAffiliation
							.getAffiliation().getDepartment())) {
						nameDepartment += " ("
								+ userAffiliation.getAffiliation()
										.getDepartment() + ")";
					}
					return nameDepartment;
				}
			}
		}
		return null;

	}

	public String getEmail() {
		if (user != null) {
			return user.getEmail().trim();
		} else {
			return null;
		}
	}
	
	public String getLastName(){
		 if (user != null) {
			 String name = user.getLastName();
			 return name;
		 }
		 return null;
    }
		
	

	public String getCountry() {
		if (user != null) {
			Iterator userAffiliations = user.getUserAffiliations().iterator();
			while (userAffiliations.hasNext()) {
				UserAffiliation userAffiliation = (UserAffiliation) userAffiliations
						.next();
				if (userAffiliation.isPreferred()) {
					return userAffiliation.getAffiliation().getCountry();
				}
			}
		}
		return null;
	}

	/**
	 * @return Returns the userId.
	 */
	public Integer getUserId() {
		return userId;
	}

	/**
	 * @param userId
	 *            The userId to set.
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public void setUser(User user) {
		this.user = user;
		this.userId = user.getId();
	}

	/**
	 * @return Returns the user.
	 */
	public User getUser() {
		return user;
	}


}