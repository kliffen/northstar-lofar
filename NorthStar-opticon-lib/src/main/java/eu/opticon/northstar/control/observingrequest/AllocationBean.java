// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest;

import java.io.Serializable;

public class AllocationBean implements Serializable 
{
	protected String id = null;
	protected String telescope = null;
	
	protected String firstQTime = null;
	protected String lastQTime = null;
	protected String greyTime = null;
	protected String darkTime = null;
	protected String brightTime = null;
	protected String globalTime = null;
	protected String awardedTime = null;
	protected String longTermTime = null;
	
	protected String awardedDarkTime = null;
	protected String usefulDarkTime = null;
	protected String longTermDarkTime = null;
	protected String awardedGreyTime = null;
	protected String usefulGreyTime = null;
	protected String longTermGreyTime = null;
	protected String awardedBrightTime = null;
	protected String longTermBrightTime = null;
	protected String usefulBrightTime = null;
	protected String awardedGlobalTime = null;
	protected String longTermGlobalTime = null;
	protected String usefulGlobalTime = null;

	// these fields are the hint fields, derived from the observations
	protected String obsFirstQTime = null;
	protected String obsLastQTime = null;
	protected String obsGreyTime = null;
	protected String obsDarkTime = null;
	protected String obsBrightTime = null;
	protected String obsGlobalTime = null;
	protected String totalProcessingTime =null;
	protected String obsLongTermStorage = null;
	protected String longTermStorage = null;
	
	public String getLongTermStorage() {
		return longTermStorage;
	}
	public void setLongTermStorage(String longTermStorage) {
		this.longTermStorage = longTermStorage;
	}
	
	public String getBrightTime()
	{
		return brightTime;
	}
	public void setBrightTime(String brightTime)
	{
		this.brightTime = brightTime;
	}
	public String getDarkTime()
	{
		return darkTime;
	}
	public void setDarkTime(String darkTime)
	{
		this.darkTime = darkTime;
	}
	public String getFirstQTime()
	{
		return firstQTime;
	}
	public void setFirstQTime(String firstQTime)
	{
		this.firstQTime = firstQTime;
	}
	public String getGreyTime()
	{
		return greyTime;
	}
	public void setGreyTime(String greyTime)
	{
		this.greyTime = greyTime;
	}
	public String getId()
	{
		return id;
	}
	public void setId(String id)
	{
		this.id = id;
	}
	public String getLastQTime()
	{
		return lastQTime;
	}
	public void setLastQTime(String lastQTime)
	{
		this.lastQTime = lastQTime;
	}
	public String getTelescope()
	{
		return telescope;
	}
	public void setTelescope(String telescope)
	{
		this.telescope = telescope;
	}
	public String getObsBrightTime()
	{
		return obsBrightTime;
	}
	public void setObsBrightTime(String obsBrightTime)
	{
		this.obsBrightTime = obsBrightTime;
	}
	public String getObsDarkTime()
	{
		return obsDarkTime;
	}
	public void setObsDarkTime(String obsDarkTime)
	{
		this.obsDarkTime = obsDarkTime;
	}
	public String getObsFirstQTime()
	{
		return obsFirstQTime;
	}
	public void setObsFirstQTime(String obsFirstQTime)
	{
		this.obsFirstQTime = obsFirstQTime;
	}
	public String getObsLastQTime()
	{
		return obsLastQTime;
	}
	public void setObsLastQTime(String obsLastQTime)
	{
		this.obsLastQTime = obsLastQTime;
	}
	public String getGlobalTime()
	{
		return globalTime;
	}
	public void setGlobalTime(String globalTime)
	{
		this.globalTime = globalTime;
	}
	public String getObsGlobalTime()
	{
		return obsGlobalTime;
	}
	public void setObsGlobalTime(String obsGlobalTime)
	{
		this.obsGlobalTime = obsGlobalTime;
	}
	public String getAwardedTime()
	{
		return awardedTime;
	}
	public void setAwardedTime(String awardedTime)
	{
		this.awardedTime = awardedTime;
	}
	public String getLongTermTime()
	{
		return longTermTime;
	}
	public void setLongTermTime(String longTermTime)
	{
		this.longTermTime = longTermTime;
	}
	public String getObsGreyTime()
	{
		return obsGreyTime;
	}
	public void setObsGreyTime(String obsGreyTime)
	{
		this.obsGreyTime = obsGreyTime;
	}
	public String getAwardedDarkTime()
	{
		return awardedDarkTime;
	}
	public void setAwardedDarkTime(String awardedDarkTime)
	{
		this.awardedDarkTime = awardedDarkTime;
	}
	public String getLongTermDarkTime()
	{
		return longTermDarkTime;
	}
	public void setLongTermDarkTime(String longTermDarkTime)
	{
		this.longTermDarkTime = longTermDarkTime;
	}
	public String getAwardedGreyTime()
	{
		return awardedGreyTime;
	}
	public void setAwardedGreyTime(String awardedGreyTime)
	{
		this.awardedGreyTime = awardedGreyTime;
	}
	public String getLongTermGreyTime()
	{
		return longTermGreyTime;
	}
	public void setLongTermGreyTime(String longTermGreyTime)
	{
		this.longTermGreyTime = longTermGreyTime;
	}
	public String getAwardedBrightTime()
	{
		return awardedBrightTime;
	}
	public void setAwardedBrightTime(String awardedBrightTime)
	{
		this.awardedBrightTime = awardedBrightTime;
	}
	public String getLongTermBrightTime()
	{
		return longTermBrightTime;
	}
	public void setLongTermBrightTime(String longTermBrightTime)
	{
		this.longTermBrightTime = longTermBrightTime;
	}
	public String getUsefulDarkTime() {
		return usefulDarkTime;
	}
	public void setUsefulDarkTime(String usefulDarkTime) {
		this.usefulDarkTime = usefulDarkTime;
	}
	public String getUsefulGreyTime() {
		return usefulGreyTime;
	}
	public void setUsefulGreyTime(String usefulGreyTime) {
		this.usefulGreyTime = usefulGreyTime;
	}
	public String getUsefulBrightTime() {
		return usefulBrightTime;
	}
	public void setUsefulBrightTime(String usefulBrightTime) {
		this.usefulBrightTime = usefulBrightTime;
	}

	public String getAwardedGlobalTime() {
		return awardedGlobalTime;
	}
	public void setAwardedGlobalTime(String awardedGlobalTime) {
		this.awardedGlobalTime = awardedGlobalTime;
	}
	public String getLongTermGlobalTime() {
		return longTermGlobalTime;
	}
	public void setLongTermGlobalTime(String longTermGlobalTime) {
		this.longTermGlobalTime = longTermGlobalTime;
	}
	public String getUsefulGlobalTime() {
		return usefulGlobalTime;
	}
	public void setUsefulGlobalTime(String usefulGlobalTime) {
		this.usefulGlobalTime = usefulGlobalTime;
	}
	public String getObsLongTermStorage() {
		return obsLongTermStorage;
	}
	public void setObsLongTermStorage(String obsLongTermStorage) {
		this.obsLongTermStorage = obsLongTermStorage;
	}
	public String getTotalProcessingTime() {
		return totalProcessingTime;
	}
	public void setTotalProcessingTime(String totalProcessingTime) {
		this.totalProcessingTime = totalProcessingTime;
	}

}
