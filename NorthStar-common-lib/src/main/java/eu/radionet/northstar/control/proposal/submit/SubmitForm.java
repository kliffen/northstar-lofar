// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * UploadForm.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: SubmitForm.java,v 1.1 2006-05-02 10:01:10 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.submit;

import org.apache.struts.action.ActionForm;

import eu.radionet.northstar.data.entities.ProposalError;

/**
 * The UploadForm provides
 *
 * @author Bastiaan Verhoef
 *
 */
public class SubmitForm extends ActionForm {
    private ProposalError proposalErrors = null;
    private String proposalCode = null;
    private String proposalTitle = null;
    private String proposalCategory = null;
    private String telescope = null;

    /**
     * @return Returns the proposalErrors.
     */
    public ProposalError getProposalErrors() {
        return proposalErrors;
    }
    /**
     * @param proposalErrors The proposalErrors to set.
     */
    public void setProposalErrors(ProposalError proposalErrors) {
        this.proposalErrors = proposalErrors;
    }


    /**
     * @return Returns the proposalCode.
     */
    public String getProposalCode() {
        return proposalCode;
    }
    /**
     * @param proposalCode The proposalCode to set.
     */
    public void setProposalCode(String proposalCode) {
        this.proposalCode = proposalCode;
    }
    /**
     * @return Returns the proposalTitle.
     */
    public String getProposalTitle() {
        return proposalTitle;
    }
    /**
     * @param proposalTitle The proposalTitle to set.
     */
    public void setProposalTitle(String proposalTitle) {
        this.proposalTitle = proposalTitle;
    }
    /**
     * @return Returns the proposalCategory.
     */
    public String getProposalCategory() {
        return proposalCategory;
    }
    /**
     * @param proposalCategory The proposalCategory to set.
     */
    public void setProposalCategory(String proposalCategory) {
        this.proposalCategory = proposalCategory;
    }
    /**
     * @return Returns the telescope.
     */
    public String getTelescope() {
        return telescope;
    }
    /**
     * @param telescope The telescope to set.
     */
    public void setTelescope(String telescope) {
        this.telescope = telescope;
    }


}
