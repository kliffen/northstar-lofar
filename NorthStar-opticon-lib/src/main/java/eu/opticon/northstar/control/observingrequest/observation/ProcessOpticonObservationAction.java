// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest.observation;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.opticon.northstar.business.OpticonConstants;
import eu.opticon.northstar.business.OpticonTargetUtils;
import eu.opticon.northstar.control.observingrequest.instrument.ProcessInstrumentBean;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.AstroCoordinate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.exception.ParseTargetListException;
import eu.radionet.northstar.control.proposal.observingrequest.observation.ObservationForm;
import eu.radionet.northstar.control.proposal.observingrequest.observation.ProcessObservationAction;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ResourceType;

public class ProcessOpticonObservationAction extends ProcessObservationAction {
    protected Observation fillObservation(HttpServletRequest request,Observation observation, ObservationForm observationForm) throws DatabaseException{
        if (observation == null){
            observation = new OpticonObservation();
        }
        /*
		 * check if max number of observation has been reached or exceeded
		 */
        OpticonObservation opticonObservation = (OpticonObservation) observation;
        OpticonObservationForm opticonObservationForm = (OpticonObservationForm) observationForm;
        if(opticonObservation.getInstrument() == null){
        	opticonObservation.setInstrument(new OpticonInstrument());
        }
     
         if(opticonObservationForm.getShowInstrumentBean() != null && opticonObservationForm.getShowInstrumentBean().equalsIgnoreCase("true")){
	    	ProcessInstrumentBean.processForm(request, opticonObservation);
	    	
		}
	    
//         if (opticonObservationForm.getObservationNoiseLevel() != null &&
//        		 AstronValidator.isPositiveDouble(opticonObservationForm.getObservationNoiseLevel())) {
//        	 opticonObservation.setNoiseLevel(new Double(opticonObservationForm.getObservationNoiseLevel()));
//         }
        	 
	    return super.fillObservation(request, observation, observationForm);

    }
    
	public void fillInstrument(OpticonObservation opticonObservation,
			OpticonObservationForm opticonObservationForm) throws DatabaseException 
	{
		
		
	}
	
    /**
	 * @param observationForm
	 * @param observation
	 * @param mapping : Add this parameter to handle error message 
	 * @throws DatabaseException
	 */
    public void addOrChangeTarget(ObservationForm observationForm,HttpServletRequest request, ActionMapping mapping)
    	throws DatabaseException
    {
    	OpticonObservationForm opticonObservationForm = 
    		(OpticonObservationForm) observationForm;
    	OpticonTarget opticonTarget = new OpticonTarget();
    	List<OpticonTarget> tempTargets = opticonObservationForm.getTempTargets();
		ActionMessages errors=validate(opticonObservationForm, mapping, request);
		// this is where the target is added, to the list
    	opticonTarget.setFieldName(observationForm.getFieldName());
    	if ((observationForm.getRa() != null) && observationForm.getDec() != null) 
    	try{
    		if (observationForm.getRa().length() != 0){
    			AstroCoordinate raDec = new AstroCoordinate(observationForm.getRa(), observationForm.getDec());
    			opticonTarget.setRa(new Double(raDec.RA()));
    			opticonTarget.setDecl(new Double(raDec.Dec()));
    		}
    	}catch (Exception e) 
		{
			// this should not happen here; do nothing
    		log.warn("impossible error occured");
    		throw new DatabaseException(e);
    		//e.printStackTrace();
		}
    	opticonTarget.setEpoch(observationForm.getEpoch());
    	
    	/*String newRunnumber = "0";
    	List<LabelValueBean> runnumbers = opticonObservationForm.getRunnumbers();
    	for (LabelValueBean lvb : runnumbers){
    		if(lvb.getLabel().contains("new")){
    			newRunnumber = lvb.getValue();
    		}
    	}
    	
    	//if(runHasNoTime(opticonObservationForm.getTempTargets(),opticonObservationForm.getSelectedRunnumber())){
				//(opticonObservationForm.getSelectedRunnumber() != null && opticonObservationForm.getSelectedRunnumber().equalsIgnoreCase(newRunnumber))){*/
    	Double totalDuration = AstronConverter.getDoubleSecondsFromMinutes(opticonObservationForm.getTargetExposureTime());
    	
    	boolean isExistingRun = false;
		for(OpticonTarget target : tempTargets){
			if(target.getRunnumber() != null && target.getRunnumber().equalsIgnoreCase(opticonObservationForm.getSelectedRunnumber())){
				isExistingRun = true;
				
				if(opticonObservationForm.getTargetExposureTime()==null )
		    	{
					totalDuration = NorthStarDelegate.getResourceValue(
							target.getAllocations(), OpticonConstants.TOTAL_DURATION);
		    	}
			}
		}
		
    	//if(opticonObservationForm.isTargetFirstInRun()){
	    	
	    	
			ResourceType resourceType = northStarDelegate.getResourceType(OpticonConstants.TIME, OpticonConstants.SECONDS);
			Map allocations = opticonTarget.getAllocations();
			NorthStarDelegate.setResourceValue(allocations, OpticonConstants.TOTAL_DURATION, totalDuration,	resourceType); 
			
			if (opticonObservationForm.getSelectedObservationTime() != null){ // if no time is selected
				if(opticonObservationForm.getSelectedObservationTime().startsWith(OpticonConstants.DARK) ){
					NorthStarDelegate.setResourceValue(allocations,OpticonConstants.DARK , totalDuration,	resourceType); 
				}
				if(opticonObservationForm.getSelectedObservationTime().startsWith(OpticonConstants.GREY) ){
					NorthStarDelegate.setResourceValue(allocations,OpticonConstants.GREY , totalDuration,	resourceType); 
				}
				if(opticonObservationForm.getSelectedObservationTime().startsWith(OpticonConstants.BRIGHT) ){
					NorthStarDelegate.setResourceValue(allocations,OpticonConstants.BRIGHT , totalDuration,	resourceType); 
				}
				if(opticonObservationForm.getSelectedObservationTime().startsWith(OpticonConstants.FIRST_Q ) ){
					NorthStarDelegate.setResourceValue(allocations,OpticonConstants.FIRST_Q , totalDuration,	resourceType); 
				}
				if(opticonObservationForm.getSelectedObservationTime().startsWith(OpticonConstants.LAST_Q ) ){
					NorthStarDelegate.setResourceValue(allocations,OpticonConstants.LAST_Q , totalDuration,	resourceType); 
				}
			}
    	//}
    	
		opticonTarget.setMoon(opticonObservationForm.getSelectedTargetMoon() );
		opticonTarget.setSeeing(opticonObservationForm.getSelectedTargetSeeing());
		opticonTarget.setSeeingLower(AstronConverter.toDouble(opticonObservationForm.getTargetSeeingLower()));
		opticonTarget.setSeeingUpper(AstronConverter.toDouble(opticonObservationForm.getTargetSeeingUpper()));
		opticonTarget.setWater(opticonObservationForm.getSelectedTargetWater());
		opticonTarget.setSn(AstronConverter.toDouble(opticonObservationForm.getSelectedTargetSn()));
		opticonTarget.setMagnitude(AstronConverter.toDouble(opticonObservationForm.getSelectedTargetMagnitude()));
		opticonTarget.setFlux(AstronConverter.toDouble(opticonObservationForm.getTargetFlux() ));
		opticonTarget.setFluxFrequency(AstronConverter.toDouble(opticonObservationForm.getTargetFluxFrequency() ));
		opticonTarget.setSpectralIndex(AstronConverter.toDouble(opticonObservationForm.getTargetSpectralIndex() ));
		if(opticonObservationForm.getTargetDiameter() != null && opticonObservationForm.getTargetDiameter().equalsIgnoreCase("point")){
			opticonTarget.setDiameter(new Double(0d));
		}else{
			opticonTarget.setDiameter(AstronConverter.toDouble(opticonObservationForm.getTargetDiameter() ));
		}
		opticonTarget.setSkyQuality(opticonObservationForm.getSelectedTargetSkyQuality());
		opticonTarget.setComments(opticonObservationForm.getSelectedTargetComments());
		opticonTarget.setOpportunity(opticonObservationForm.isTargetOpportunity() );
		opticonTarget.setCentralFrequency(AstronConverter.toDouble(opticonObservationForm.getTargetCentralFrequency()));
		opticonTarget.setBandwidth(AstronConverter.toDouble(opticonObservationForm.getTargetBandwidth()));
		opticonTarget.setSubbandList(opticonObservationForm.getTargetSubbandList());
		try {
			opticonTarget.setTotalSubbandList(String.valueOf(OpticonTargetUtils.getNumberofSubBands(opticonObservationForm.getTargetSubbandList())));
		} catch (ParseTargetListException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		opticonTarget.setSpecifySubbandList(opticonObservationForm.isTargetSpecifySubbandList());
		/*if(opticonObservationForm.isTargetSpecifySubbandList() != true){*/
			String bandlist = "";
			if(opticonTarget.getCentralFrequency() != null && opticonTarget.getBandwidth() != null){
				 bandlist = CalculateSubbandListAction.calculate(0, opticonTarget.getCentralFrequency().doubleValue(), opticonTarget.getBandwidth().doubleValue());
				opticonTarget.setSubbandList(bandlist);
				opticonObservationForm.setTargetSubbandList(bandlist);
				try {
					opticonObservationForm.setTargetTotalSubbandList(String.valueOf(OpticonTargetUtils.getNumberofSubBands(bandlist)));
				} catch (ParseTargetListException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			/*else { // This else is not necessary, already done above
				bandlist =opticonObservationForm.getTargetSubbandList();
				opticonTarget.setSubbandList(bandlist); //TODO : some validation for proper input format

				opticonTarget.setTotalSubbandList(sumSubbandList(bandlist));
			}*/
		/*}*/
		/*	String concatPipeline = "";
			for(String subString:opticonObservationForm.getSelectedPipeline()) {
				concatPipeline = concatPipeline + ";" + subString;
			}*/
		//opticonTarget.setPipeline(opticonObservationForm.getSelectedPipeline());
		//opticonTarget.setPipeline(opticonObservationForm.getSelectedPipeline()!=null?String.join(";", opticonObservationForm.getSelectedPipeline()):null);
		if (opticonObservationForm.getSelectedPipeline() != null) {
			String tmp = null;
			for(String pipeline: opticonObservationForm.getSelectedPipeline()) {
				if (tmp == null) {
					tmp = pipeline;
				} else {
					tmp = tmp + ";" + pipeline;	
				}				
			}
			opticonTarget.setPipeline(tmp);
		} else {
			opticonTarget.setPipeline(null);
		}
		opticonTarget.setCalibrationBeam(opticonObservationForm.isCalibrationBeam() );
		
		//if(opticonObservationForm.getSelectedRunnumber() == null ||
		//		(opticonObservationForm.getSelectedRunnumber() != null && opticonObservationForm.getSelectedRunnumber().equalsIgnoreCase(newRunnumber))){
		
		// set observingrun the same as the others in the same run
		String observingRun = opticonObservationForm.getSelectedObservingRun();
		for(OpticonTarget target : tempTargets){
			if(target.getRunnumber() != null && 
					target.getRunnumber().equalsIgnoreCase(opticonObservationForm.getSelectedRunnumber())){
				if(opticonObservationForm.isTargetFirstInRun()){
					target.setObservingrun(observingRun);
				}else if(target.getObservingrun() != null &&  target.getObservingrun() != ""){
						observingRun = target.getObservingrun();
				}
					
			}
		}
		
		opticonTarget.setObservingrun(observingRun);
		opticonTarget.setFirstInRun(opticonObservationForm.isTargetFirstInRun() );
		if(opticonObservationForm.getTempTargets().size() == 0 || observationForm.getSelectedTargetId() == 0 || !isExistingRun){
			opticonTarget.setFirstInRun(true);
		}
		opticonTarget.setRunnumber(opticonObservationForm.getSelectedRunnumber());
		
		
		//FIXME this is an ugly solution to check if all the boxes are unselected...
		String value  = request.getParameter("selectedRuns"); 
		if (value != null){
			opticonTarget.setSelectedRuns(OptionsUtils.getList(opticonObservationForm.getSelectedRuns()));
		}else{
			opticonTarget.setSelectedRuns(null);
		}
		
		int targetIndex = observationForm.getSelectedTargetId();
		if(AstronValidator.isBlankOrNull(opticonObservationForm.getTargetId())){
			targetIndex=-1;
		}
   	    boolean exceedMaximumSubbands= validateTargetSubband(opticonTarget,opticonObservationForm,errors); 
   	  	
   	     if ( observationForm.getResolveButton()==null && !opticonObservationForm.isAutoCommit()){
			    
   	     if(!exceedMaximumSubbands)	{	
 			errors.add(OpticonConstants.TARGET_SUBBAND_LIST,new ActionMessage ("error.exceed.maximum.subband.list", opticonObservationForm.getSelectedRunnumber()));
    	    }
   	    if((opticonObservationForm.getTargetExposureTime() == null || opticonObservationForm.getTargetExposureTime() == "") && opticonTarget.isFirstInRun()) {
			    	errors.add(OpticonConstants.TARGET_EXPOSURE_TIME, new ActionMessage("errors.required","Exposure Time"));
   	    	}
	
   	    	if(errors.size() >0){
   	 	    saveErrors(request, errors);
   	 		throw new DatabaseException();
   	 	}
   	     }
		   
    	if (targetIndex >= 0){
    		/* it is an edit, the old row in the table need to replaced with new values. */
    		//opticonObservationForm.getTargetSummaryValues().set(targetIndex, targetValues);
    		opticonTarget.setId(new Integer(opticonObservationForm.getTargetId()));
    		opticonObservationForm.getTempTargets().set(targetIndex, opticonTarget);
		}else{
			
			/* keep target id at null...
			 * It is an new target, it needs to be added to the table
    		//opticonObservationForm.getTargetSummaryValues().add(targetValues);
			// maybe this is needed for 4tab proposals, 5tab will give hibernate store errors
			ProposalData proposalData = (ProposalData) request.getSession().getAttribute(Constants.PROPOSAL_DATA);
			TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
			
			if(!telescopeConfiguration.isTargetsOnTabbedPage()){
				opticonTarget.setId(new Integer(-1)); 
			}
			 */
			opticonObservationForm.getTempTargets().add(opticonTarget);
			
		}
    	
    	// reorder target list
        Collections.sort(opticonObservationForm.getTempTargets(), new Comparator<OpticonTarget>() {

        public int compare(OpticonTarget o1, OpticonTarget o2) {
	        	if(o1==null ||  o2 ==null){
	    			return 0;
	    		}
	    		if(o1.getRunnumber() == null){
	    			o1.setRunnumber("1");
	    		}
	    		if(o2.getRunnumber() == null){
	    			o2.setRunnumber("1");
	    		}
	        	if(o1.isFirstInRun() && new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber())) == 0){
	    			return -1;
	    		}
	        	if(o2.isFirstInRun() && new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber())) == 0){
	    			return 1;
	    		}
                return new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber()));
            }
        });
        
        
    	
    }
	//validate targets and check if the maximum target in same run is reached or not   
	private boolean validateTargetSubband(OpticonTarget opticonTarget, OpticonObservationForm opticonObservationForm, ActionMessages errors) {
		boolean validTotalSubBandLList=true; 
		List temptargets= new ArrayList();
		
		if(opticonObservationForm.isRequiredTargetSubbandList()){
			if(AstronValidator.isBlankOrNull(opticonTarget.getSubbandList())){
				errors.add(OpticonConstants.TARGET_SUBBAND_LIST,
						new ActionMessage ("errors.required", "Target SubBand List"));
				return false;
			}
		}
				
		if(opticonTarget.getId() ==null && opticonObservationForm.getSelectedTargetId()<0){
			temptargets.add(opticonTarget);
		
		}
		else if(opticonObservationForm.getSelectedTargetId()>=0) {
			OpticonTarget editOpticonTarget= (OpticonTarget) opticonObservationForm.getTempTargets().get(opticonObservationForm.getSelectedTargetId());
			//Subband list is resetting here. Thats why while copying SubbandList it gets empty.
			editOpticonTarget.setSubbandList(opticonObservationForm.getTargetSubbandList());
			editOpticonTarget.setTotalSubbandList(opticonObservationForm.getTargetTotalSubbandList());
			opticonObservationForm.getTempTargets().set(opticonObservationForm.getSelectedTargetId(), editOpticonTarget);
		}
			temptargets.addAll(opticonObservationForm.getTempTargets());
		
		try {

				validTotalSubBandLList = OpticonTargetUtils
					.validateTotalSubBandListForSameRunnumber(
							opticonTarget
									.getRunnumber(),
									temptargets, opticonObservationForm.getMaxNumberOfTargetsonSameRunReached());
				if(opticonObservationForm.getSelectedTargetId()>0 && !validTotalSubBandLList){
					opticonObservationForm.getTempTargets().set(opticonObservationForm.getSelectedTargetId(), opticonTarget);
				}		
			
		} catch (ParseTargetListException e) {
			errors.add("targetSubbandList",
					new ActionMessage ("error.exceed.maximum.subband.list", opticonObservationForm.getSelectedRunnumber()));
					
		}
	
		return validTotalSubBandLList;
	}

	private boolean runHasNoTime(List<OpticonTarget> tempTargets, String selectedRunnumber) {
		boolean result = true;
		for(OpticonTarget target : tempTargets){
			if(target.getRunnumber() != null && target.getRunnumber().equalsIgnoreCase(selectedRunnumber)){
				Map allocations = target.getAllocations();
				Double value = NorthStarDelegate.getResourceValue(allocations, OpticonConstants.TOTAL_DURATION);
				if(value != null && value.doubleValue() > 0){
					result = false;
				}
			}
		}
		return result;
	}

	protected ActionMessages validate(ObservationForm observationForm,
			ActionMapping mapping, HttpServletRequest request) {
		
		ActionMessages errors = super.validate(observationForm,mapping,request);
		
		if (!AstronValidator.isBlankOrNull(observationForm.getRa()  )) {
			if (!AstronValidator.isRa(observationForm.getRa())) {
				errors
						.add("ra", new ActionMessage(
								"error.nora.observation.ra"));
			}

		}
		if (!AstronValidator.isBlankOrNull(observationForm.getDec())) {
			if (!AstronValidator.isDec(observationForm.getDec())) {
				errors.add("dec", new ActionMessage(
						"error.nodec.observation.dec"));
			}

		}
		if (AstronValidator.isBlankOrNull(observationForm.getRa())
				&& !AstronValidator.isBlankOrNull(observationForm.getDec())
				|| !AstronValidator.isBlankOrNull(observationForm.getRa())
				&& AstronValidator.isBlankOrNull(observationForm.getDec())) {
			errors.add("dec", new ActionMessage(
					"error.novalidradec.observation"));
		}
		
		return errors;
		
	}

	private void checkDouble(String value, String name,
			ActionMessages errors) {
		if(!AstronValidator.isBlankOrNull(value)){
			try{
				if(Double.valueOf(value).doubleValue() <= 0d ){
					errors.add(name, new ActionMessage(
					"error.invalid.double"));
				}
			}catch(NumberFormatException e){
				errors.add(name, new ActionMessage(
				"error.invalid.double"));
			}
		}
	}
//change the return type to boolean it will return false if the targets for same run is exceed and the copy operation will be failed. 
	protected boolean copyTarget(ObservationForm observationForm, HttpServletRequest request, ActionMapping mapping) {
		OpticonTarget target = (OpticonTarget) observationForm.getTempTargets().get(observationForm.getSelectedTargetId());
		String subBandList = target.getSubbandList();
		OpticonTarget nwTarget = new OpticonTarget();
		
		nwTarget.setFieldName(target.getFieldName());
		nwTarget.setRa(target.getRa());
		nwTarget.setDecl(target.getDecl());
		nwTarget.setEpoch(target.getEpoch());
		nwTarget.setSelectedRuns(target.getSelectedRuns());
		
		nwTarget.setMoon(target.getMoon());
		nwTarget.setSeeing(target.getSeeing());
		nwTarget.setWater(target.getWater());
		nwTarget.setSn(target.getSn());
		nwTarget.setMagnitude(target.getMagnitude());
		nwTarget.setComments(target.getComments());
		nwTarget.setFlux(target.getFlux());
		nwTarget.setFluxFrequency(target.getFluxFrequency());
		nwTarget.setSpectralIndex(target.getSpectralIndex());
		nwTarget.setSeeingUpper(target.getSeeingUpper());
		nwTarget.setSeeingLower(target.getSeeingLower());
		nwTarget.setSkyQuality(target.getSkyQuality());
		nwTarget.setDiameter(target.getDiameter());
		nwTarget.setStorage(target.getStorage());
		nwTarget.setOpportunity(target.isOpportunity());
		nwTarget.setCentralFrequency(target.getCentralFrequency());
		nwTarget.setBandwidth(target.getBandwidth());
		nwTarget.setSubbandList(target.getSubbandList());
		nwTarget.setTotalSubbandList(target.getTotalSubbandList());
		nwTarget.setSpecifySubbandList(target.isSpecifySubbandList());
		nwTarget.setCalibrationBeam(target.isCalibrationBeam());
		nwTarget.setRunnumber(target.getRunnumber() );
		nwTarget.setAllocations(target.getAllocations());
		nwTarget.setFirstInRun(false);
		
    	// hashmap cloning
    	//Map allocs = new HashMap(target.getAllocations());
    	//nwTarget.setAllocations(allocs);
		ActionMessages errors= validate(observationForm, mapping, request);
		boolean validCopyTarget =validateTargetSubband(nwTarget, (OpticonObservationForm)observationForm,errors);
		
		if (validCopyTarget){
		 	observationForm.getTempTargets().add(nwTarget);
	   }
		else{
			errors.add(OpticonConstants.TARGET_SUBBAND_LIST, new ActionMessage ("error.exceed.maximum.subband.list",target.getRunnumber()) );
			saveErrors(request, errors);
		}
    	observationForm.setSelectedTargetId(-1);
    	target.setSubbandList(subBandList);
    	
    	return validCopyTarget;
	}
	
	private void checkInteger(String value, String name, ActionMessages errors) {

		if(!AstronValidator.isBlankOrNull(value)){
			try{
				if(Integer.valueOf(value).intValue() <= 0 ){
					errors.add(name, new ActionMessage(
					"error.invalid.integer"));
				}
			}catch(NumberFormatException e){
				errors.add(name, new ActionMessage(
				"error.invalid.integer"));
			}
		}
		
	}
	
	protected void deleteTarget(ObservationForm observationForm) {
		if(observationForm.getTempTargets() != null && observationForm.getTempTargets().size() > observationForm.getSelectedTargetId()){
		
			OpticonTarget target = (OpticonTarget) observationForm.getTempTargets().get(observationForm.getSelectedTargetId());
			
	        if(!target.isFirstInRun() ){
	        	observationForm.getTempTargets().remove(observationForm.getSelectedTargetId());
	        }else{ //this else block is commented as delete button will delete only one target at a  time @mansura
				//delete all targets with the same runnumber
				String deletenr = target.getRunnumber();
				for(int i=0;i<observationForm.getTempTargets().size();i++) {  
					 OpticonTarget element = (OpticonTarget) observationForm.getTempTargets().get(i);  
		             if(element != null && deletenr.equalsIgnoreCase(element.getRunnumber()))  
		             {  
		            	 observationForm.getTempTargets().remove(i);
		            	 i--;
		            }  
			     }
				if(observationForm.getTempTargets() != null && observationForm.getTempTargets().size() > 0){
				OpticonTarget element = (OpticonTarget) observationForm.getTempTargets().get(0);
					 if(element != null && deletenr.equalsIgnoreCase(element.getRunnumber()))  
		             {  
		            	 observationForm.getTempTargets().remove(0);
		             }
				}
			}
		}
		observationForm.setSelectedTargetId(-1);
	}

	protected void clearTargetFields(ObservationForm observationForm)
	{
		OpticonObservationForm opticonObservationForm = (OpticonObservationForm) observationForm;
		opticonObservationForm.setFieldName(null);
		opticonObservationForm.setRa(null);
		opticonObservationForm.setDec(null);
		opticonObservationForm.setEpoch(null);
		opticonObservationForm.setTargetExposureTime(null);
		opticonObservationForm.setSelectedTargetMoon("bright");
		opticonObservationForm.setSelectedTargetSeeing(">0.9");
		opticonObservationForm.setSelectedTargetWater("don't care");
		opticonObservationForm.setSelectedTargetSn(null);
		opticonObservationForm.setSelectedTargetMagnitude(null);
		opticonObservationForm.setSelectedTargetComments(null);
		opticonObservationForm.setTargetFlux(null);
		opticonObservationForm.setTargetFluxFrequency(null);
		opticonObservationForm.setTargetSpectralIndex(null);
		opticonObservationForm.setTargetSeeingLower(null);
		opticonObservationForm.setTargetSeeingUpper(null);
		opticonObservationForm.setSelectedTargetSkyQuality(null);
		opticonObservationForm.setTargetDiameter(null);
		opticonObservationForm.setSelectedRuns(null);
		opticonObservationForm.setTargetOpportunity(false);
		opticonObservationForm.setTargetCentralFrequency(null);
		opticonObservationForm.setTargetBandwidth(null);
		opticonObservationForm.setTargetSubbandList(null);
		opticonObservationForm.setTargetTotalSubbandList(null);
		opticonObservationForm.setTargetSpecifySubbandList(false);
		opticonObservationForm.setCalibrationBeam(false);
		opticonObservationForm.setTargetFirstInRun(false);
	}	
}
