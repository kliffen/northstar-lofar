// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.RegisteredMember;

public class SetUpProposalAction extends LockedAction {

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ProposalForm proposalForm = (ProposalForm) form;
		HttpSession session = request.getSession();
		UserAccount ownUserAccount = (UserAccount) session
		.getAttribute(Constants.OWN_USERACCOUNT);
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);

		Proposal proposal = proposalData.getProposal();
		proposalData.setForwardUriAfterSubmit(request.getServletPath());
		RegisteredMember registeredMember = (RegisteredMember) proposal.getContactAuthor();
		if (registeredMember.getUser().getId().intValue() == ownUserAccount.getUser().getId().intValue()){
			proposalForm.setUserIsContactAuthor(true);
		}
		return mapping.findForward(Constants.SUCCESS);
	}

}
