// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business;

import java.util.Hashtable;

import eu.radionet.northstar.business.configuration.ConfigurationUtil;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Semester;

public class ProposalDelegateFactory {
	private static Hashtable proposalDelegates = new Hashtable();

	/*
	 * public static ProposalDelegate getProposalDelegate(
	 * TelescopeConfiguration telescopeConfiguration) throws
	 * InvalidConfigurationException { ProposalDelegate proposalDelegate =
	 * (ProposalDelegate) proposalDelegates
	 * .get(telescopeConfiguration.getName()); if (proposalDelegate == null) {
	 * ConfigurationUtil configurationUtil = ConfigurationUtil .getInstance();
	 * proposalDelegates.put(telescopeConfiguration.getName(), configurationUtil
	 * .getProposalDelegate(telescopeConfiguration)); return (ProposalDelegate)
	 * proposalDelegates .get(telescopeConfiguration.getName()); } return
	 * proposalDelegate; }
	 */

	public static ProposalDelegate getProposalDelegate(Proposal proposal)
			throws InvalidConfigurationException {
		ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();

		String community = proposal.getSemester().getCommunity();
		String telescope = proposal.getSemester().getTelescope();
		String version = proposal.getVersion();

		Hashtable communities = (Hashtable) proposalDelegates.get(telescope);
		/*
		 * if the selected telescope is not found
		 */
		if (communities == null) {

			ProposalDelegate proposalDelegate = configurationUtil
					.getProposalDelegate(proposal);
			/*
			 * create versions hashtable
			 */
			Hashtable versions = new Hashtable();
			/*
			 * store proposaldelegate
			 */
			versions.put(version, proposalDelegate);
			/*
			 * create communities hashtable
			 */
			communities = new Hashtable();
			/*
			 * store versions and communities
			 */
			communities.put(community, versions);
			proposalDelegates.put(telescope, communities);
			return proposalDelegate;

		} else {
			Hashtable versions = (Hashtable) communities.get(community);
			if (versions == null) {
				ProposalDelegate proposalDelegate = configurationUtil
						.getProposalDelegate(proposal);
				/*
				 * create versions hashtable
				 */
				versions = new Hashtable();
				/*
				 * store proposaldelegate
				 */
				versions.put(version, proposalDelegate);
				communities.put(community, versions);
				return proposalDelegate;
			} else {
				/*
				 * retrieve
				 */
				ProposalDelegate proposalDelegate = (ProposalDelegate) versions
						.get(version);
				if (proposalDelegate == null) {
					proposalDelegate = configurationUtil
							.getProposalDelegate(proposal);
					versions.put(version, proposalDelegate);
					return proposalDelegate;
				} else {
					return proposalDelegate;
				}
			}

		}

	}

	public static ProposalDelegate getProposalDelegate(Semester semester)
			throws InvalidConfigurationException {
		ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
		TelescopeConfiguration config = NorthStarConfiguration.getTelescopeConfiguration(semester, NorthStarDelegate.getCurrentDate()); 

		String community = semester.getCommunity();
		String telescope = semester.getTelescope();
		String version = config.getVersion();

		Hashtable communities = (Hashtable) proposalDelegates.get(telescope);
		/*
		 * if the selected telescope is not found
		 */
		if (communities == null) {

			ProposalDelegate proposalDelegate = configurationUtil.getProposalDelegate(config);
			/*
			 * create versions hashtable
			 */
			Hashtable versions = new Hashtable();
			/*
			 * store proposaldelegate
			 */
			versions.put(version, proposalDelegate);
			/*
			 * create communities hashtable
			 */
			communities = new Hashtable();
			/*
			 * store versions and communities
			 */
			communities.put(community, versions);
			proposalDelegates.put(telescope, communities);
			return proposalDelegate;

		} else {
			Hashtable versions = (Hashtable) communities.get(community);
			if (versions == null) {
				ProposalDelegate proposalDelegate = configurationUtil.getProposalDelegate(config);
				/*
				 * create versions hashtable
				 */
				versions = new Hashtable();
				/*
				 * store proposaldelegate
				 */
				versions.put(version, proposalDelegate);
				communities.put(community, versions);
				return proposalDelegate;
			} else {
				/*
				 * retrieve
				 */
				ProposalDelegate proposalDelegate = (ProposalDelegate) versions
						.get(version);
				if (proposalDelegate == null) {
					proposalDelegate = configurationUtil.getProposalDelegate(config);
					versions.put(version, proposalDelegate);
					return proposalDelegate;
				} else {
					return proposalDelegate;
				}
			}

		}

	}

}
