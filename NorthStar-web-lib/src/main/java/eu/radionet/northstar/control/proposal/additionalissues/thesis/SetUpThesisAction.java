// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SetUpApplicantAction.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: SetUpThesisAction.java,v 1.5 2008-04-08 08:03:00 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.additionalissues.thesis;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.AdditionalIssues;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Thesis;

/**
 * The SetUpThesisAction provides
 * 
 * @author Hanno Holties
 *  
 */
public class SetUpThesisAction extends LockedAction {


	
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        ThesisForm thesisForm = (ThesisForm) form;
		HttpSession session = request.getSession();
        ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
        Proposal proposal = proposalData.getProposal();
        /*
         * iterator through members
         */
        Iterator membersIterator = proposal.getMembers().iterator();
        List memberBeans = new ArrayList();
        
        int i = 0;
        while (membersIterator.hasNext()) {
            MemberBean memberBean = new MemberBean();
            memberBean.setId(new Integer(i));

            Member member = (Member) membersIterator.next();
            memberBean.setMember(member);

            i++;

            memberBeans.add(memberBean);

        }
        thesisForm.setMemberBeans(memberBeans);
        /*
         * TODO: change this to a xml option configuration.
         */
        thesisForm.initStudentLevels();
        
        if (!AstronValidator.isBlankOrNull(thesisForm.getThesisId())) {
			AdditionalIssues additionalIssues = proposalData
                .getProposal().getAdditionalIssues();
            Thesis thesis = (Thesis) additionalIssues.getTheses()
                .get(AstronConverter.toInteger(thesisForm.getThesisId()).intValue());
            
            if (thesis.getStudent() != null) {
                /*
                 * iterator through members
                 */
                Iterator memberBeansIterator = memberBeans.iterator();
                
                while (memberBeansIterator.hasNext()) {
                    MemberBean memberBean = (MemberBean) memberBeansIterator.next();
                    if (memberBean.getMember().equals(thesis.getStudent())) {
                        thesisForm.setStudentRef(memberBean.getId());
                    }
                }
                thesisForm.setStudentApplicant(true);
            } else {
                thesisForm.setStudentApplicant(false);
            }
            thesisForm.setStudentName(thesis.getStudentName());
            thesisForm.setStudentLevel(thesis.getStudentLevel());
            if (thesis.getSupervisor() != null) {
                Iterator memberBeansIterator = memberBeans.iterator();
                
                while (memberBeansIterator.hasNext()) {
                    MemberBean memberBean = (MemberBean) memberBeansIterator.next();
                    if (memberBean.getMember().equals(thesis.getSupervisor())) {
                        thesisForm.setSupervisorRef(memberBean.getId());
                    }
                }
                thesisForm.setSupervisorApplicant(true);
            } else {
                thesisForm.setSupervisorApplicant(false);
            }
            thesisForm.setSupervisorName(thesis.getSupervisorName());
            thesisForm.setExpectedCompletionDate(AstronConverter.toDateString(thesis.getExpectedCompletionDate(),Constants.DATE_FORMAT));
            thesisForm.setDataRequired(thesis.isDataRequired());
        }

        return mapping.findForward(Constants.SUCCESS);

    }
}