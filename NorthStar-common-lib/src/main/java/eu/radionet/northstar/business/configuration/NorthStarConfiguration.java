// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.configuration;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import eu.radionet.northstar.data.WebDAVConfiguration;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Semester;

public class NorthStarConfiguration {

	private static String name = null;
    private static String bannerImageUri = null;
	private static String welcomeUri = null;
	private static String welcomeMessage = null;
    private static String northstarHelpUri = null;
	private static String emailSender = null;
	private static String maintenanceAccount = null;
	private static String administratorEmail = null;
	private static Set mappingFiles = new HashSet();
	private static NorthStarConfiguration configuration = null;
	private static CollaborationConfiguration collaborationConfiguration = null;
	private static Hashtable emailConfigurations = new Hashtable();
	private static List telescopes = new ArrayList();
	private static List communityConfigurations = new ArrayList();
	private static WebDAVConfiguration webDAVConfiguration = null;
	private static Map commonOptions = new HashMap();
	private static boolean restrictUserTelescope = false;
	private static String labelsFile = null;

	private NorthStarConfiguration(){
		
	}
	public static NorthStarConfiguration getInstance(){
		if (configuration == null){
			configuration = new NorthStarConfiguration();
		}
		return configuration;
	}
	public static String getName() {
		return name;
	}
	public static List getTelescopeNames(){
		List telescopeNames = new ArrayList();
		Iterator telescopeIterator = telescopes.iterator();
		while (telescopeIterator.hasNext()){
			Telescope telescope = (Telescope) telescopeIterator.next();
			telescopeNames.add(telescope.getName());
		}
		return telescopeNames;
	}
	
	public static Telescope getTelescope(String name){
		Iterator telescopeIterator = telescopes.iterator();
		while (telescopeIterator.hasNext()){
			Telescope telescope = (Telescope) telescopeIterator.next();
			if (telescope.getName().equals(name)){
				return telescope;
				
			}
		}
		return null;
	}


	public void setName(String input) {
		name = input;
	}



	public CollaborationConfiguration getCollaborationConfiguration() {
		return collaborationConfiguration;
	}


	public void setCollaborationConfiguration(
			CollaborationConfiguration input) {
		collaborationConfiguration = input;
	}




	public static String getEmailSender() {
		return emailSender;
	}
	
	public static boolean getRestrictUserTelescope(){
		return restrictUserTelescope;
	}
	
	public void setRestrictUserTelescope(boolean RestrictUserTelescope){
		NorthStarConfiguration.restrictUserTelescope = RestrictUserTelescope;
		restrictUserTelescope = RestrictUserTelescope;
	}


	public void setEmailSender(String input) {
		emailSender = input;
	}
	/**
     * @return Returns the bannerImageUri.
     */
    public static String getBannerImageUri() {
        return bannerImageUri;
    }
    /**
     * @param bannerImageUri The bannerImageUri to set.
     */
    public void setBannerImageUri(String bannerImageUri) {
        NorthStarConfiguration.bannerImageUri = bannerImageUri;
    }
    public static String getWelcomeUri() {
		return welcomeUri;
	}
	public void setWelcomeUri(String welcomeUri) {
		NorthStarConfiguration.welcomeUri = welcomeUri;
	}
	public static String getWelcomeMessage() {
		return welcomeMessage;
	}
	public void setWelcomeMessage(String welcomeMessage) {
		NorthStarConfiguration.welcomeMessage = welcomeMessage;
	}
	/**
     * @return Returns the northstarHelpUri.
     */
    public static String getNorthstarHelpUri() {
        return northstarHelpUri;
    }
    /**
     * @param northstarHelpUri The northstarHelpUri to set.
     */
    public void setNorthstarHelpUri(String northstarHelpUri) {
        NorthStarConfiguration.northstarHelpUri = northstarHelpUri;
    }
    public static TelescopeConfiguration getTelescopeConfiguration(Proposal proposal){
		Iterator telescopeIterator = NorthStarConfiguration
		.getTelescopes().iterator();
		while (telescopeIterator.hasNext()){
			Telescope telescope = (Telescope) telescopeIterator.next();
			/*
			 * if telescope belongs to semester
			 */
			if (proposal.getSemester().getTelescope().equals(telescope.getName())){
				Iterator telescopeConfigurationsIterator = telescope
				.getTelescopeConfigurations().iterator();
				while (telescopeConfigurationsIterator.hasNext()){
					TelescopeConfiguration telescopeConfiguration = (TelescopeConfiguration) telescopeConfigurationsIterator
					.next();
					/*
					 * if community are the same
					 */
					if (proposal.getSemester().getCommunity().equals(telescopeConfiguration.getCommunity())){
						if (proposal.getVersion().equals(telescopeConfiguration.getVersion())){
							return telescopeConfiguration;
						}
					}
				}
				
			}
		}
		return null;
	}
	public static TelescopeConfiguration getTelescopeConfiguration(Semester semester, Calendar currentDate){
		Iterator telescopeIterator = NorthStarConfiguration
		.getTelescopes().iterator();
		while (telescopeIterator.hasNext()){
			Telescope telescope = (Telescope) telescopeIterator.next();
			/*
			 * if telescope belongs to semester
			 */
			if (semester.getTelescope().equals(telescope.getName())){
				Iterator telescopeConfigurationsIterator = telescope
				.getTelescopeConfigurations().iterator();
				while (telescopeConfigurationsIterator.hasNext()){
					TelescopeConfiguration telescopeConfiguration = (TelescopeConfiguration) telescopeConfigurationsIterator
					.next();
					/*
					 * if community are the same
					 */
					if (semester.getCommunity().equals(telescopeConfiguration.getCommunity())){
						Date startTime = telescopeConfiguration.getStartTime();
						/*
						 * if start time is before current time
						 */
						if (startTime != null && startTime.before(currentDate.getTime())){
							Date endTime = telescopeConfiguration.getEndTime();
							if (endTime != null){
								if (endTime.after(currentDate.getTime())){
									return telescopeConfiguration;
								}
							}else {
								return telescopeConfiguration;
							}
						}
					}
				}

			}
		}
		return null;
	}
/*	public static TelescopeConfiguration getTelescopeConfiguration(
			Proposal proposal) {
		if (proposal.getObservingRequests().size() == 0){
			return null;
		}
		Iterator telescopeConfigurationsIterator = NorthStarConfiguration
				.getTelescopeConfigurations().iterator();
		while (telescopeConfigurationsIterator.hasNext()) {
			TelescopeConfiguration telescopeConfiguration = (TelescopeConfiguration) telescopeConfigurationsIterator
					.next();
			if (proposal.getObservingRequest().getClass().equals(
					telescopeConfiguration.getObservingRequestClass())) {
				return telescopeConfiguration;
			}
		}
		return null;
	}
	public static TelescopeConfiguration getTelescopeConfiguration(
			ObservingRequest observingRequest) {
		Iterator telescopeConfigurationsIterator = NorthStarConfiguration
				.getTelescopeConfigurations().iterator();
		while (telescopeConfigurationsIterator.hasNext()) {
			TelescopeConfiguration telescopeConfiguration = (TelescopeConfiguration) telescopeConfigurationsIterator
					.next();
			if (observingRequest.getClass().equals(
					telescopeConfiguration.getObservingRequestClass())) {
				return telescopeConfiguration;
			}
		}
		return null;
	}
	public static TelescopeConfiguration getTelescopeConfiguration(
			TargetListParser targetListParser) {
		Iterator telescopeConfigurationsIterator = NorthStarConfiguration
				.getTelescopeConfigurations().iterator();
		while (telescopeConfigurationsIterator.hasNext()) {
			TelescopeConfiguration telescopeConfiguration = (TelescopeConfiguration) telescopeConfigurationsIterator
					.next();
			if (targetListParser.getClass().equals(
					telescopeConfiguration.getTargetListParserClass())) {
				return telescopeConfiguration;
			}
		}
		return null;
	}
	public static Class getObservingRequest(
			String name) {
		Iterator telescopeConfigurationsIterator = NorthStarConfiguration
				.getTelescopeConfigurations().iterator();
		while (telescopeConfigurationsIterator.hasNext()) {
			TelescopeConfiguration telescopeConfiguration = (TelescopeConfiguration) telescopeConfigurationsIterator
					.next();
			if (name.equals(
					telescopeConfiguration.getName())) {
				return telescopeConfiguration.getObservingRequestClass();
			}
		}
		return null;
	}*/
	public static String getAdministratorEmail() {
		return administratorEmail;
	}
	public void setAdministratorEmail(String administratorEmail) {
		NorthStarConfiguration.administratorEmail = administratorEmail;
	}
	public static Set getMappingFiles() {
		return mappingFiles;
	}
	public static void setMappingFiles(Set mappingFiles) {
		NorthStarConfiguration.mappingFiles = mappingFiles;
	}
	public static List getCommunityConfigurations() {
		return communityConfigurations;
	}
	public static void setCommunityConfigurations(List communityConfigurations) {
		NorthStarConfiguration.communityConfigurations = communityConfigurations;
	}
	public static String getCommunityDescription(String name){
		Iterator iterator = communityConfigurations.iterator();
		while (iterator.hasNext()){
			CommunityConfiguration communityConfiguration = (CommunityConfiguration) iterator.next();
			if (communityConfiguration.getName().equals(name)){
				return communityConfiguration.getDescription();
			}
		}
		return null;
	}
	public void addCommunityConfiguration(
			CommunityConfiguration communityConfiguration) {
		communityConfigurations.add(communityConfiguration);
	}
	public static List getTelescopes() {
		return telescopes;
	}
	public static void setTelescopes(List telescopes) {
		NorthStarConfiguration.telescopes = telescopes;
	}
	public static Hashtable getEmailConfigurations() {
		return emailConfigurations;
	}
	public static void setEmailConfigurations(Hashtable emailConfigurations) {
		NorthStarConfiguration.emailConfigurations = emailConfigurations;
	}
	public void addEmailConfiguration(String type, EmailConfiguration emailConfiguration){
		emailConfigurations.put(type,emailConfiguration);
	}
	public static EmailConfiguration getEmailConfiguration(String type){
		return (EmailConfiguration) emailConfigurations.get(type);
	}
	public static String getMaintenanceAccount() {
		return maintenanceAccount;
	}
	public void setMaintenanceAccount(String maintenanceAccount) {
		NorthStarConfiguration.maintenanceAccount = maintenanceAccount;
	}
	public static WebDAVConfiguration getWebDAVConfiguration() {
		return webDAVConfiguration;
	}
	public static void setWebDAVConfiguration(
			WebDAVConfiguration webDAVConfiguration) {
		NorthStarConfiguration.webDAVConfiguration = webDAVConfiguration;
	}
	public static Map getCommonOptions() {
		return commonOptions;
	}
	public void setCommonOptions(Map commonOptions) {
		NorthStarConfiguration.commonOptions = commonOptions;
	}
	public static String getLabelsFile() {
		return labelsFile;
	}
	public void setLabelsFile(String labelsFile) {
		NorthStarConfiguration.labelsFile = labelsFile;
	}
	
}
