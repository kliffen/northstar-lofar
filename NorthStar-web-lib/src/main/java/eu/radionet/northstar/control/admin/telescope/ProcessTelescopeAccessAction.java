// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.telescope;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.control.ActionServlet;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.UserTelescope;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.useradministration.data.entities.User;

public class ProcessTelescopeAccessAction extends Action {

	private NorthStarDelegate northStarDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (form != null){
			TelescopeAccessForm telescopeAccessForm = (TelescopeAccessForm) form;
			
			String Operation = telescopeAccessForm.getOperation();
			// remove button is pressed
			if (Operation.equalsIgnoreCase("remove")){
				String userId = telescopeAccessForm.getSelectedUser();
				try {
					removeUserTelescope(Integer.valueOf(userId),telescopeAccessForm.getSelectedTelescope());
		        } catch (ConnectionException ce) {
		            return (mapping.findForward("fatalError"));
		        }
			}
			// add button is pressed
			if (Operation.equalsIgnoreCase("adduser")){
				if (!telescopeAccessForm.getSelectedTelescope().equalsIgnoreCase("")){
					return (mapping.findForward("adduser"));
				}
			}
			// invite button is pressed
			if (Operation.equalsIgnoreCase("invite")){
				if (!telescopeAccessForm.getSelectedTelescope().equalsIgnoreCase("")){
					return ParameterAppender.append(mapping.findForward("inviteuser"),
							"selectedTelescope", telescopeAccessForm.getSelectedTelescope());
					//return (mapping.findForward("inviteuser"));
				}
			}
		}
	
		return mapping.findForward(Constants.REFRESH);
	}
	
	private void removeUserTelescope(Integer userId, String telescopeName) 
				throws DatabaseException{
		northStarDelegate = NorthStarDelegate.getInstance();
        // get access rights belonging to the userid
		if (userId != null){
	        List UserTelescopeList = northStarDelegate.getTelescopeAccessRights(userId );
	        // check if a telescope matches
	        Iterator UTit = UserTelescopeList.iterator();
	        while (UTit.hasNext())
	        {
	        	UserTelescope uTelescope = (UserTelescope) UTit.next();
	        	// only if it matches delete the found object
	        	if(uTelescope.getTelescope().equalsIgnoreCase(telescopeName) ){
	        		northStarDelegate.deleteUserTelescope(uTelescope);
	        		// success
	        	}
	        }
		}           
	}

}
