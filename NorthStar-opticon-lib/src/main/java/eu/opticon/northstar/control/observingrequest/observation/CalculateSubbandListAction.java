package eu.opticon.northstar.control.observingrequest.observation;

import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.astron.util.AstronValidator;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CalculateSubbandListAction extends Action{
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		response.setContentType("text/xml");  
        response.setHeader("Cache-Control", "no-cache");  
        
		//ActionForward forward = new ActionForward();
		PrintWriter writer = response.getWriter();
		
		double centralFrequency=0;
		double bandwidth=0;
		int clock = 0;
		
		String frequency= request.getParameter("frequency");
		String bandwidthparam= request.getParameter("bandwidth");
		try{
			centralFrequency = Double.parseDouble(frequency);
			if(AstronValidator.isBlankOrNull(bandwidthparam))
				writer.println("");	
			else{
			bandwidth = Double.parseDouble(bandwidthparam);
			
			writer.println(calculate(clock,centralFrequency,bandwidth));
			}
		}catch(Exception ex){
			writer.println("invalid numbers");
		}	
		
		return null;
	}
	
	public static String calculate(int clock,double centralFrequency,double bandwidth){
		double subbandwidth = 0.1953125; // at 200MHz (160 not supported yet, there are 5.12 subbands in a MHz 
		double startfrequency = centralFrequency - (bandwidth / 2);
		double endfrequency = centralFrequency + (bandwidth / 2);
		int startBandNr = 0;
		int endBandNr = 0;
		/* maximum rating, got them from wilfred
		110-190 filter
		mode        SB     freq
		HBA start 12   102.344
		HBA eind 499   197.461
		
		210-250 filter
		mode        SB     freq
		HBA start  52   210.156
		HBA end  255   249.805
		
		10-90/30-90 filter
		mode        SB     freq
		LBA start  12   2.344
		LBA end  499   97.461
		
		set max bandwidth to 95.3 which will create 488 subbands
		 */
		if(bandwidth <= 0 || bandwidth > 94.92 || centralFrequency < 2 || centralFrequency > 250){
			return "invalid frequency or bandwidth.";
		}
		
		// calculate low band
		if(centralFrequency < 98){
			
		}
		// calculate mid band
		else if(centralFrequency > 102 && centralFrequency < 198){
			startfrequency = startfrequency - 100;
			endfrequency = endfrequency - 100;
		}
		// calculate high band
		else if(centralFrequency > 210 ){
			startfrequency = startfrequency - 200;
			endfrequency = endfrequency - 200;
		}else{
			return("invalid central frequency.");
		}
		
		startBandNr = (int) Math.floor(startfrequency / subbandwidth);
		endBandNr = (int) Math.ceil(endfrequency /  subbandwidth);
		
		if(startBandNr < 1 || endBandNr > 512){
			return("frequency / bandwith out of range.");
		}
		
		String result = Integer.valueOf(startBandNr).toString() + "-" + Integer.valueOf(endBandNr).toString();
		return result;
	}

}
