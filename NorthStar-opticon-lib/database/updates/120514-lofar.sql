ALTER TABLE `lofar_proposal_astron_nl_northstar`.`opticontarget` ADD COLUMN `flux_frequency` DOUBLE  DEFAULT NULL AFTER `flux`,
 ADD COLUMN `spectral_index` DOUBLE  DEFAULT NULL AFTER `flux_frequency`;

ALTER TABLE `lofar_proposal_astron_nl_northstar`.`opticoninstrument` ADD COLUMN `complex_voltage` TINYINT(1)  NOT NULL DEFAULT 0 AFTER `incohstokes`,
 ADD COLUMN `flys_eye` TINYINT(1)  NOT NULL DEFAULT 0 AFTER `complex_voltage`,
 ADD COLUMN `raw_voltage` TINYINT(1)  NOT NULL DEFAULT 0 AFTER `flys_eye`,
 ADD COLUMN `subarray_pointings` INTEGER  DEFAULT NULL AFTER `direct_datastorage`,
 ADD COLUMN `store_raw_data` TINYINT(1)  NOT NULL DEFAULT 0 AFTER `incpolarization`;

ALTER TABLE `lofar_proposal_astron_nl_northstar`.`opticonobservingrequest` ADD COLUMN `night_time` TINYINT(1)  NOT NULL DEFAULT 0 AFTER `requestednights`,
 ADD COLUMN `noise_level` DOUBLE  DEFAULT NULL AFTER `night_time`;

ALTER TABLE `lofar_proposal_astron_nl_northstar`.`opticonobservation` ADD COLUMN `noise_level` DOUBLE  DEFAULT NULL AFTER `calibrationrequirements`;
