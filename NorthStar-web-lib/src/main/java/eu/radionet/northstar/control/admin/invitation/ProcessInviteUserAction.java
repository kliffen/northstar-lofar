// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessApplicantAction.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: ProcessApplicantAction.java,v 1.3 2008-04-08 08:02:58 smit Exp $
 *
 */
package eu.radionet.northstar.control.admin.invitation;

import java.util.Locale;
import java.util.MissingResourceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronValidator;
import nl.astron.util.exception.AstronMailException;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.UserTelescope;

/**
 * The ProcessApplicantAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessInviteUserAction extends Action {

	private CollaborationDelegate collaborationDelegate = null;
	private NorthStarDelegate northstarDelegate = null;
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		InviteUserForm inviteUserForm = (InviteUserForm) form;

		/*
		 * if pressed on Discard changes
		*/ 
		if (isCancelled(request)) {
			return mapping.findForward(Constants.SUCCESS);
		}
		
		if(inviteUserForm.getOkButton() != null ){
	       	return mapping.findForward(Constants.SUCCESS);
	    }

		ActionMessages errors = validate(inviteUserForm);
		if(!errors.isEmpty()){
			saveErrors(request, errors);
			return mapping.findForward(Constants.REFRESH);
		}
		
        NonRegisteredMember nonRegisteredMember = new NonRegisteredMember();
		nonRegisteredMember.setName(inviteUserForm.getName());
		nonRegisteredMember.setAffiliation(inviteUserForm.getAffiliation());
		nonRegisteredMember.setEmail(inviteUserForm.getEmail().trim());
		nonRegisteredMember.setCountry(inviteUserForm.getSelectedCountry());
		
		 // now the country name is in ISO format.
        String countr = inviteUserForm.getSelectedCountry();
        Locale[] localit = Locale.getAvailableLocales();
         
        if (inviteUserForm.getInviteButton() != null){
        	// send invitation email
        	UserAccount ownUserAccount = (UserAccount) session
			.getAttribute(Constants.OWN_USERACCOUNT);
        	collaborationDelegate = CollaborationDelegate.getInstance();
        	
        	String serverPath = request.getScheme() + "://"+ request.getServerName();
			if (request.getScheme().equals("http") && request.getServerPort() != 80) {
				serverPath += ":" + request.getServerPort();
			} else if (request.getScheme().equals("https") || request.getServerPort() != 443) {
				serverPath += ":" + request.getServerPort();
			}
			
			String forwardPath = serverPath	+ "/useradministration/user/forward.do?forward=" + serverPath;
			forwardPath = forwardPath + "/proposal" ;
			String message = inviteUserForm.getPersonalMessage();
			
			try{
        		collaborationDelegate.sendInvitation(nonRegisteredMember,ownUserAccount,message,forwardPath);
        	}catch(AstronMailException e){
        		// sending failed
        		inviteUserForm.setInvitationOk(null);
        		inviteUserForm.setInviteButton(null);
        		errors.add("sendemail", new ActionMessage("error.send.submit.proposal"));
        		saveErrors(request, errors);
        		return mapping.findForward(Constants.REFRESH);
        	}
        	
        	if(inviteUserForm.getSelectedTelescope() != null){
	        	UserTelescope userTelescope = new UserTelescope();
				userTelescope.setTelescope(inviteUserForm.getSelectedTelescope());
				userTelescope.setMemberId(Integer.valueOf(nonRegisteredMember.getId() * -1));
				if(northstarDelegate ==null){
					northstarDelegate = NorthStarDelegate.getInstance(); 
				}
				northstarDelegate.addUserTelescope(userTelescope);

        	}
        	
        	inviteUserForm.setInvitationOk("OK");
        	inviteUserForm.setInviteButton(null);
        	return mapping.findForward(Constants.REFRESH);
        }
        
        for(int i=0; i < localit.length; i++){
        	Locale locale = localit[i]; 
        	try{
	       		if (locale.getISO3Country().equalsIgnoreCase(countr)) {
	       			nonRegisteredMember.setCountry(locale.getDisplayCountry());
	       		}
        	}catch(MissingResourceException e){
        		//log.warn("Couldn't find 3-letter country code for "+locale.getDisplayCountry());
        	}
        }
		
		return mapping.findForward(Constants.REFRESH);
	}

	private ActionMessages validate(InviteUserForm inviteUserForm) {
		ActionErrors errors = new ActionErrors();
        
        if (AstronValidator.isBlankOrNull(inviteUserForm.getName() )) {
            errors.add("name", new ActionMessage(
                    "error.required.applicant.name"));
        } 
        if (AstronValidator.isBlankOrNull(inviteUserForm.getAffiliation())) {
            errors.add("affiliation", new ActionMessage(
                    "error.required.applicant.affiliation"));
        }    
        if (AstronValidator.isBlankOrNull(inviteUserForm.getSelectedCountry())) {
            errors.add("country", new ActionMessage(
                    "error.required.applicant.country"));
        }
        if (AstronValidator.isBlankOrNull(inviteUserForm.getEmail().trim())) {
            errors.add("email", new ActionMessage(
                    "error.required.applicant.email"));
        }        
        else if (!AstronValidator.isEmail(inviteUserForm.getEmail().trim())) {
            errors.add("email", new ActionMessage(
                    "error.invalid.applicant.email"));
        } 

        return errors;

	}

}
