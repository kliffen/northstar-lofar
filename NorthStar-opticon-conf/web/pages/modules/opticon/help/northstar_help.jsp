<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<html:html>
<head>
	<link rel=stylesheet href="<%=request.getContextPath()%>/css/proposalLayout.css" type="text/css">
    <title>NorthStar Help File</title>
</head>

<body>

<jsp:include page="../../../layout/box_header_noFullWidth.jsp" />
<A name="index"></a>
<h2>Index of the NorthStar Help Page:</h2>
<ul>
<li><a href="#general">About the Northstar proposal web application</a>
<li><a href="#browser">Browser behaviour</a>
<li><a href="#proposallist">Proposal list</a>
<li><a href="#applicants">Applicants</a>
	<ul>
		<li><a href="#applicant">Add/Edit Applicant</a>
	</ul>
<li><a href="#justification">Justification</a>
<li><a href="#opticonobservingrequest">Observing Request</a>
	<ul>
		<li><a href="#opticonobservation">Observation</a>
		<li><a href="#opticonuploadlist">Upload Target List</a>

	</ul>
<li><a href="#additionalissues">Additional Information</a>
</ul>
<hr/>
- <a href="#index">index</a> - <a href="" onclick="self.close()">close window</a> -
<A name="general"></a>
<hr/>
<h3>About the Northstar proposal web application&nbsp;:&nbsp;</h3>
<pre>

 FIRST TIME USERS are STRONGLY encouraged to use the "Help" button
 on each page for instructions and help specific to that page.
 
 MORE HELP for the preparation of proposals in NorthStar can be found by
 scrolling down from the Read-me-First section, or by using the links
 provided in the "Help Index" section at the top of every page in
 NorthStar.

 SAVING AN INTERMEDIATE VERSION frequently while you are filling out
 the proposal form is highly recommended, to avoid loss of work should
 the network connection be lost or the server time out. Press the
 relevant Save button. The proposal will appear in your proposal list
 below, and an Edit button is enabled to allow further modifications
 at any time until submission.

 USING ONLY A SINGLE BROWSER WINDOW/SESSION per proposal is required,
 in order to avoid divergence of edit actions. Remember: actions are
 only stored after one of the Save buttons is pressed !

 THE SAFARI BROWSER for Macintosh should be avoided since some
 NorthStar features have known problems under that system.
 Alternatives for Macintosh (for example FireFox) exist and have no
 reported problems.

 SUBMISSION of proposals can only be done by the "Contact Author".

 RETRACTION of a Regular Proposal is possible for the Contact Author
 only, until the deadline has elapsed. A retracted proposal may be
 modified and then submitted, or deleted; in either case, no previous
 versions of a retracted proposal will ever be admitted for review.

 THE CONTACT AUTHOR must be available should the need arise for
 interaction with the Committee or the Observatory on processing or
 assessment of the proposal. It is orginally the person creating the
 proposal, but others can be invited to become "Active Participants", and
 any of these can then get the Contact Author role.

 THE FORMAL PI can be indicated to be any (other) person in the list of
 applicants, in case that is desirable for proper credit or funding
 eligibility, for example.

 COLLABORATIVE PROPOSAL PREPARATION (Viewing, Editing, and Uploading of
 external files), can be carried out on an equal footing by all "Active
 Participants". Checks to avoid and resolve simultaneous modifications
 are in place, but collaboration in good faith is of course assumed.

 ACTIVE PARTICIPANTS are added upon Acceptance of an e-mail Invitation,
 sent in NorthStar by an existing Active Participant (originally, only
 the creator of the proposal). All Active Participants must have, or
 create, their own NorthStar user account. The email address used
 in the proposal must correspond to the email address of the user
 account. Detailed instructions are available from the Applicants page
 on the proposal form.

 NON_ACTIVE PARTICIPANTS can be added, but will not immediately receive
 a notification and will not be able to View or Edit the proposal. At any
 time a non-active participant can be converted to an active one by
 sending an email Invitation using the Invite button on the form. Upon
 submission of the proposal all participants, active and non-active, will
 receive an email notification and invitation to View the proposal.
 
 THE PROPOSAL FORMAT consists of filling out the form, and uploading
 one or more separate latex format Science and Technical
 Justification files. Detailed instructions are available from the
 Justification page of the proposal form.

- Credits
 This web application has been developed as a tool to prepare and
 submit observation proposals for (radio-) and then (optical-) telescopes. 
 It has been  developed as
 part of the Radionet "Synergy" activity and further developed by 
the Opticon Access program, are both funded by the EU FP6 and FP7 programme. 

 The proposal tool has been developed at ASTRON by the Radio Observatory
 Division's software group. It has been specifically adapted for the
 OPTICON Trans National Access Programme.

</pre>
<hr/>
- <a href="#index">index</a> - <a href="" onclick="self.close()">close window</a> -
<A name="browser"></a>
<hr/>
<h3>Browser behaviour&nbsp;:&nbsp;</h3>
<pre>
      - Browser versions
        The proposal tool has been developed as a web application to allow everybody
        with access to a personal computer with internet connectivity, on which a recent
        web browser has been installed, to work with the application. Although care is
        taken not to rely on browser specific features the tool does not support all/old
        browser versions.

        The proposal web application has been tested with the following browsers:

        - MS Internet Explorer 6.0
        - Netscape 7.1
        - Mozilla Firefox 3.5

        If you experience problems that could be related to a specific browser type, please first
        try one of the (freely available) browsers mentioned above. We do appreciate reports on
        browser specific problems to help us make the tool support more browsers. 
        

      - Cookies
        The proposal web application uses cookies for maintaining the session state (i.e. to track
        whether a user has logged in). No other information is saved or retrieved using cookies
        and the lifespan of the cookies is for the duration of the session only. <b>Cookies should
        be accepted by the browser.</b>

      - Javascript
        The proposal web application uses javascript for site navigation and validation. Care is
        taken not to use browser specific features, but <b>javascript must be enabled in the browser or
        else you will not be able to fully navigate through the application.</b>
        
      - Browser navigation
        The proposal web application provides buttons and links for its navigation. Some (links) will open
        a new browser window. Most will open new pages in the same browser window. Although
        they are not disabled, <b>use of the standard browser navigation options (e.g. "Back", "Forward",
        "Open in new window") while working with the web application is strongly discouraged</b> since it
        can have unexpected side effects (such as overwriting entries with old information). Similarly,
        working in two simultaneous sessions (e.g. by logging in twice) may give rise to unpredictable
        results.
        
      - Session timeout
        The server automatically diposes of sessions that have been idle for a long period. This
        prevents sessions that have not been closed in a controlled way from claiming server
        resources indefinitely. The timeout period for editing of proposals is currently set at a
        generous eight hours so that users do not easily lose (unsaved) session information
        when they are distracted from working on a proposal. If the session is timed out anyway,
        the user will be redirected to the login screen upon the first attempt to (re-) load a
        Proposal URL. <b>All information since the last "Save" will then be lost!</b>
        
        For the proposal overview page, the timeout interval is set at half an hour.
        
        <b>IMPORTANT NOTICE</b>
        Although we allow long lasting sessions, internet connections depend on a
        large number of linked elements, many of which are out of control of the user (client) and the
        service provider (server). Therefore we strongly encourage users to:
        
        1) While working on a proposal: save the session regularly by hitting the "Save and
        Continue" or the "Save and Exit" button at the bottom of the screen.
        
        2) If discontinuing work with the tool for a prolonged period: log out by clicking on the
        appropriate link on the "Proposal List" page.
        
        In this way you can prevent loss of (a large amount of) information in case of network/connection
        failure and also prevent unnecessary use of server resources that might degrade server performance.

</pre>
<hr/>
- <a href="#index">index</a> - <a href="" onclick="self.close()">close window</a> -
<A name="proposallist"></a>
<hr/>
<h3>Proposal list&nbsp;:&nbsp;</h3>
<pre>
  THE CONTACT AUTHOR is responsible for Submission, possible
  Retraction, and any communication which might be needed during the
  review process. The creator of the first draft will orginally be the
  Contact Author; this can later be changed to another Active
  Participant (see below).

  THE PI might be another team member, in case this is desirable for
  proper credit or funding eligibility, for example.

  COLLABORATION facilities are offered by Northstar for the preparation
  of proposals: a proposal will appear in the Proposal List of all
  Active Participants.

  ACTIVE PARTICIPANTS have accepted an Invitation from an Active
  Participant (originally only the creator of the proposal, of
  course). See the Help section on Applicants for details. Viewing,
  Editing, and Saving, are open to all Active Participants on
  an equal footing; there are appropriate locking facilities to avoid
  and resolve multiple simultaneous actions, but collaboration in good
  faith is of course assumed.

  NON_ACTIVE PARTICIPANTS can be added, but will not immediately receive
  a notification and will not be able to View or Edit the proposal. At any
  time a non-active participant can be converted to an active one by
  sending an email Invitation using the Invite button on the form. Upon
  submission of the proposal all participants, active and non-active, will
  receive an email notification and invitation to View the proposal.
 
  VIEWING presents the pdf version of the full proposal as it has last
  been saved, including uploaded files in their final placement, exactly
  as it will be used for the review process. Required Fields which have
  not yet been entered will appear with a marker text in red. The pdf can
  be retrieved for printing, but note that pdf viewers and printer drivers
  may scale the document or alter e.g. the fonts before printing, unless
  certain options are (un-) set. Documents will be reviewed printed on
  A4 paper and without scaling of the input.

  EDITING of all fields remains possible at any time until
  submission. There are 5 tabbed pages to be completed, each with multiple
  fields; some are context-dependent, and further fields may open up as
  choices are made.

  SAVING AN INTERMEDIATE VERSION can be done at any intermediate stage
  of preparation. It is highly recommended to do this frequently while
  you are filling out the proposal form, to avoid loss of work should
  the network connection be lost or the server time out. Press the
  relevant Save button. After the first Save the proposal will appear
  in the Proposal List, and the Edit button is enabled to allow further
  modifications at any time until submission.

  DELETING a proposal is a permanent action. Confirmation of the action
  will be requested, but afterwards the proposal cannot be
  retrieved. After Submission, proposals cannot be deleted. Only the
  current Contact Author is allowed to delete the proposal.

  SUBMISSION of a fully prepared proposal must be done by the Contact
  Author. There will be a verification that all required fields have an
  entry, but validation of the contents of the fields is not at a very
  advanced level, and the Contact Author is responsible for checking
  the validity of the entries before submission. After confirmation,
  the proposal will be entered into the permanent Observatory
  records.

  CONFIRMATION OF SUBMISSION, with the assigned Project ID, will be
  sent by e-mail to ALL applicants. Applicants who are not yet Active
  will also receive an Invitation to become an Active Participant, as
  they should have the opportunity to View the proposal. After a proposal
  is submitted, Editing and Deleting are no longer possible.

  RETRACTION of Regular proposals is possible until a deadline has
  passed, and must be done by the Contact Author. After that, the
  proposal can either be edited and submitted again, or be deleted. The
  originally assigned project id will remain in use. After retraction,
  the proposal will NOT be considered for allocation unless and until
  the current Contact Author submits it again, in time for the
  deadline. Urgent or Service proposals for some telescopes CANNOT be
  retracted or revised, since they trigger human action immediately
  upon submission. Retracted Regular proposals cannot be changed to the
  Urgent or Service categories, because they have already received
  a permanent project id in the Regular category.

  SIMILAR PROPOSALS can be generated by completing one first, and then 
  using the COPY function on the list of proposals. The particulars are 
  copied across to the  extent that this is possible.

</pre>
<hr/>
- <a href="#index">index</a> - <a href="" onclick="self.close()">close window</a> -
<A name="applicants"></a>
<hr/>
<h3>Applicants&nbsp;:&nbsp;</h3>
<pre>
  For each applicant, supply the person's name, affiliation (institute
  name), country of affiliation, and e-mail address.

  Applicants, if any, who are willing to come to the telescope for the
  observations, and/or who have relevant experience, can be indicated by
  checking the appropriate box.

  The creator of the proposal is automatically listed as an applicant first,
  and his/her contact information is already known.

  Shuffling the order of applicants is possible by pressing up/down arrow
  buttons appearing alongside their names.

  There must always be 1 Contact Author, responsible for submission and any
  communication which might be needed during the review process The creator
  of the first draft will orginally be the Contact Author; this can later be
  changed to another Active Participant (see below).

  Another team member can be labeled as the formal PI, in case this is
  desirable for proper credit or funding eligibility, for example.

  Northstar offers collaboration facilities in preparing proposals:

  Active Participants are those applicants registered as sharing in the
  online generation of a specific proposal, which appears in their personal
  proposal list. They can all view and potentially edit all aspects of that
  proposal online on an equal footing; there are appropriate locking
  facilities to avoid and resolve multiple simultaneous editing actions, but
  collaboration in good faith is of course assumed. Submission and
  retraction of the proposal remain the exclusive responsibility of the
  Contact Author.

  Persons listed as applicants can become Active Participants through
  invitation by an Active Participant (originally only the creator of the
  proposal, of course). Non-active participants will not immediately receive
  a notification and will not be able to View or Edit the proposal. Upon
  submission of the proposal all participants, active and non-active, will
  receive an email notification and invitation to View the proposal.

  The Invite function sends, upon the next "save", an e-mail to a specific
  e-mail address, and this address must correspond to the one listed for
  this person's NorthStar account; the invitation therefore expires if the
  e-mail address is edited. Should the mail be lost, or if it needs to be
  sent to another e-mail address, re-invitation is possible.

  Acceptance of an invitation requires the new Active Participant to have an
  existing NorthStar user account, and involves going to a specific online
  link, given in the e-mail invitation. Persons who do not yet have a
  NorthStar user account can still follow the link, and must then first use
  the "Register as New User" function from the NorthStar login screen to
  which they will be taken.
     
</pre>
<hr/>
- <a href="#index">index</a> - <a href="" onclick="self.close()">close window</a> -
<A name="applicant"></a>
<hr/>
<h3>Add/Edit Applicant&nbsp;:&nbsp;</h3>
<pre>
  Specify details of an applicant. All fields are required.
  
   - Name
     The name of the applicant.

   - Affiliation
     The name of the institute that the applicant is affiliated with.

   - Email
     The email address of the applicant (validation requires a plausible
     email address).

   - Country
     The country of the institute that the applicant is affiliated with.
     
  If the box "Invite as Active Participant" is checked, an e-mail invitation
  will be sent. If the invitation is accepted, this gives both View and Edit
  rights on the entire proposal to that person, on an equal footing with
  the rights of the person issuing the invitation. There are checks to
  avoid and resolve possible conflicting simultaneous Edit actions, but good
  faith collaboration is assumed. Only the Contact Author has Submit/Retract
  privileges, but all Active Participants are able to designate any of
  the Active Participants to become the Contact Author via the radio
  button selectors on the Applicants page.
  
  If the box "Invite as Active Participant" is left unchecked, the applicant
  will not receive any notification and will not be able to View or Edit the
  proposal.
</pre>
<hr/>
- <a href="#index">index</a> - <a href="" onclick="self.close()">close window</a> -
<A name="justification"></a>
<hr/>
<h3>Justification&nbsp;:&nbsp;</h3>
<pre>
  Enter a concise TITLE (recommend not more than 150 characters)

  Enter an ABSTRACT in plain text form. This should not exceed 180
  words.

  Scientific and Technical JUSTIFICATIONS should be prepared offline,
  on your own computer, and then uploaded. In order to control 
  font sizes and page length, you are required to to prepare your 
  proposal in latex, and submit the latex file. The latex file will be 
  compiled automatically by NorthStar. If the latex fails to compile,
  then the latex error message will be shown.   
   
  Before submission, the  entire proposal, including the uploaded 
  file(s) in their final  placement, can be viewed as a pdf file, which 
  will also be used during the review process.

  The justification pages may contain figures or tables, but these may also
  be uploaded as an optional additional file (page limits may apply,
  detailed <%= "<a href=\"" + request.getContextPath() + "/justificationInstructions.do\">INSTRUCTIONS</a>" %> are available from the Justification page of the
  proposal form).

  NorthStar accepts images in postscript format or encapsulated postscript
  (.ps, .eps) It is also possible to upload a .tar file with multiple images 
  in it.   
  The "Instructions for preparation" button on the "Justification" web page 
  contains further instructions, and also a template to use Latex to 
  generate the page with the appropriate format for uploading. The latex
  class file is also available for download but doesn't have to be uploaded. 
  All file SIZES are RESTRICTED to 10 Mbytes.

  To UPLOAD a file, use the BROWSE button to select the file on your drive,
  then press ATTACH. If the type and size are correct, then the file will be
  added to the list of files. Use the preview button to see how the page will
  look like. 

  A mandatory file can only be REPLACED by a new version, by simply using the
  upload function again.

  To VIEW or DOWNLOAD a file you have uploaded, click on its name.
  Viewing the entire proposal as a pdf document to inspect its appearance
  is also possible. The pdf can be retrieved for printing, but note that pdf
  viewers and printer drivers may scale the document, or alter e.g. the fonts,
  before printing unless certain options are (un-) set. Documents will be
  reviewed printed on A4 paper and without scaling of the input.

  Proposal documents which the review committee finds
  to be deficient, unreasonably dense, or excessively long may be
  rejected !

</pre>
<hr/>
- <a href="#index">index</a> - <a href="" onclick="self.close()">close window</a> -
<A name="opticonobservingrequest"></a>
<hr/>
<h3>Observing request&nbsp;:&nbsp;</h3>
<pre>
  The Observing Request page allows you to select the science category of 
the proposal. If your proposal is a Long term proposal (to run over 
more than one semester).

The selection of a telescope/instrument configuration is done via the 
button "Specify a new Observing run". Once a telescope & instrument has been
selected, a link to the instrument documentation page will appear with several
configuration options. If fields are missing or incomplete, then use the 
configuration comments field to specify them. If filters are requested, then
it is possible to select several filters by checking them. (instead of 
specifying of filling in a new observing run for each filter.)

In the run details section, specify the time request and scheduling 
constraints. The total time is the number of nights (or hours in some occasions)
including calibration and overheads. The minimum useful time is the minimum of
nights/hours required to provide any results. After that, there are some selection
boxes for time/weather conditions. Periods to be avoided, sequential scheduling
etc.. can be defined in the boxes below that.

The button commit observing run is intended to fill in another observing run.
The last box: "overall scheduling constraints" is intended for providing general 
constraints for the whole proposal (periods to be avoided, sequential 
scheduling, etc...) Specific scheduling constraints for individual observing runs
have to be entered when you fill in the observing run and 
will appear on page 5 within the Observation details per telescope/instrument. 



</pre>
<hr/>
- <a href="#index">index</a> - <a href="" onclick="self.close()">close window</a> -
<A name="opticonobservation"></a>
<hr/>
<h3>Target List &nbsp;:&nbsp;</h3>
<pre>
  the target list page allows you to formulate a list of targets which will be
  used by one or more runs.
  Fields to fill in for targets are self-explanatory; those which have to be 
  filled in are marked with a red asterix (upon saving/viewing the proposal, 
  those fields will be highlighted if not filled in properly), the others are
  optional. The field "Comments" can be used to enter more parameters, 
  like a redshift for example, or an indication of variability, or moving etc..
  
  Targets have to be connected to runs. Each run has a letter assigned,  
   A, B, C etc.. just like the filters, you can check several observing runs for
  a single target.
  
  Only when the commit to list of targets button is pressed, will the target be
  added to the list. Also when editing an existing target, if the commit button
  is not pressed and there is a page switch, then the new data will be lost.

  Lists of targets can be uploaded from a file. The required format for that 
  file can be seen when hitting the button " Upload a target list" .

  It is possible to upload a batch file containing the targets to 
  be observed. This should be a plain text file. Each line should 
  contain information about the target to be observed. Each item on 
  this line should be separated by one or more spaces.

  You must provide a list of your proposed targets in the format:
  
  Fieldname RA Dec Epoch [Exposure_time [Runs [S/N [Red Magn. 
    [Infrared Magn. [Diameter [Comments]]]]]]]]]
  
  An example:

  m35 06:45:59.93 -20:45:15.1 j2000 720s A,B 5 4 4 0 this is an example 
  "hd 39801" 05:55:10.31 +07:24:25.4 j2000 1400s  A,B 7
  m31 00:48:31.20 +12:09:00.0 j2000 91s   C  
  m32 00:42:41.87 +40:51:57.2 j2000

  - Target Identifier
    This should be a unique name, e.g. IC1613
    (Avoid 'blanks' in the target name!)

	Other details can be found on the upload page itself.
</pre>
<hr/>
- <a href="#index">index</a> - <a href="" onclick="self.close()">close window</a> -
<A name="additionalissues"></a>
<hr/>
<h3>Additional Information&nbsp;:&nbsp;</h3>
<pre>
   Enter other information relevant for assessment here.

  - Linked or joint proposals   -Yes -No
     Are you proposing to other telescopes for the same, or linked,
     projects

     - Details
       List all telescopes for which linked propoals will be
       submitted.


   - Proposals elsewhere
     Briefly describe related proposals recently submitted or soon to
     be submitted elsewhere.

   - Previous allocations 
     If this box is checked, a window opens to provide more details. 
     Describe status of recent related and unrelated allocations
     received for this telescope. Specify project id's. Summarise the 
     results obtained, the problems encountered, if any. 
     List any relevant papers published.

   - Programme is part of a PhD thesis   -Yes -No
      - Name of the student
      - Name of the supervisor
      - Envisaged thesis completion date
      - Data required for completion of the thesis -Yes -No

   - Relevant publications
     Optional references to papers of particular relevance in assessing
     this application.

   - Additional Remarks
     Any other information pertinent for assessment and scheduling.

</pre>
<hr/>
- <a href="#index">index</a> - <a href="" onclick="self.close()">close window</a> -
<hr/>
<jsp:include page="../../../layout/box_footer_noFullWidth.jsp" />

</body>
</html:html>
