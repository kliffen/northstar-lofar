// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.semester;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;

import nl.astron.util.AstronValidator;
import eu.radionet.northstar.control.Constants;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class SemesterForm extends ActionForm {
	protected String semesterId = null;

	protected String prefix = null;

	protected String startTime = null;

	protected String endTime = null;

	protected String deadLineTime = null;
	
	protected String deadLineDelay = null;

	protected String category = null;

	protected String lastAssignedNumber = "0";

	protected String telescope = null;

	protected String semester = null;

	protected String community = null;

	protected List telescopes = new ArrayList();

	protected List categories = new ArrayList();

	protected Set communities = new TreeSet();

	protected String storeButton = null;
	
	protected List parents = new ArrayList();
	protected String selectedParentId = null;
	
	protected String communityLabel = null;
	
	protected boolean immediate =false;
	protected String availableTime = null;

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getDeadLineTime() {
		return deadLineTime;
	}

	public void setDeadLineTime(String deadLineTime) {
		this.deadLineTime = deadLineTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getSemesterId() {
		return semesterId;
	}

	public void setSemesterId(String id) {
		this.semesterId = id;
	}

	public String getLastAssignedNumber() {
		return lastAssignedNumber;
	}

	public void setLastAssignedNumber(String lastAssignedNumber) {
		this.lastAssignedNumber = lastAssignedNumber;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getTelescope() {
		return telescope;
	}

	public void setTelescope(String telescope) {
		this.telescope = telescope;
	}

	public List getTelescopes() {
		return telescopes;
	}

	public void setTelescopes(List telescopes) {
		this.telescopes = telescopes;
	}

	public List getCategories() {
		return categories;
	}

	public void setCategories(List categories) {
		this.categories = categories;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {

		SimpleDateFormat inputDateFormat = new SimpleDateFormat();
		inputDateFormat.applyPattern(Constants.SEMESTER_DATETIME_FORMAT);
		inputDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		ActionErrors errors = new ActionErrors();
		if (storeButton != null) {
			try {

				inputDateFormat.parse(startTime);

			} catch (ParseException pe) {
				errors.add("startTime", new ActionMessage("error.dateformat.semester"));
			}
			try {

				inputDateFormat.parse(endTime);

			} catch (ParseException pe) {
				errors.add("endTime", new ActionMessage("error.dateformat.semester"));
			}
			try {

				inputDateFormat.parse(availableTime);

			} catch (ParseException pe) {
				errors.add("availableTime", new ActionMessage("error.dateformat.semester"));
			}
			try {

				inputDateFormat.parse(deadLineTime);

			} catch (ParseException pe) {
				errors.add("deadLineTime",
						new ActionMessage("error.dateformat"));
			}
			if (!AstronValidator.isPositiveInt(lastAssignedNumber)) {
				errors.add("lastAssignedNumber", new ActionMessage(
						"error.noint.positive"));
			}
			if (AstronValidator.isBlankOrNull(prefix)) {
				errors.add("prefix", new ActionMessage("error.field.required"));
			}
			if (AstronValidator.isBlankOrNull(semester)) {
				errors.add("semester",
						new ActionMessage("error.field.required"));
			} 
	
		}
		return errors;
	}

	public String getSemester() {
		return semester;
	}

	public void setSemester(String semester) {
		this.semester = semester;
	}

	public Set getCommunities() {
		return communities;
	}

	public void setCommunities(Set communities) {
		this.communities = communities;
	}

	public String getCommunity() {
		return community;
	}

	public void setCommunity(String community) {
		this.community = community;
	}

	public String getStoreButton() {
		return storeButton;
	}

	public void setStoreButton(String storeButton) {
		this.storeButton = storeButton;
	}

	public String getCommunityLabel() {
		return communityLabel;
	}

	public void setCommunityLabel(String communityLabel) {
		this.communityLabel = communityLabel;
	}

	public String getAvailableTime() {
		return availableTime;
	}

	public void setAvailableTime(String availableTime) {
		this.availableTime = availableTime;
	}

	public boolean isImmediate() {
		return immediate;
	}

	public void setImmediate(boolean immediate) {
		this.immediate = immediate;
	}

	public String getDeadLineDelay() {
		return deadLineDelay;
	}

	public void setDeadLineDelay(String deadLineDelay) {
		this.deadLineDelay = deadLineDelay;
	}

	public String getSelectedParentId() {
		return selectedParentId;
	}

	public void setSelectedParentId(String selectedParentId) {
		this.selectedParentId = selectedParentId;
	}

	public List getParents() {
		return parents;
	}

	public void setParents(List parents) {
		parents = parents;
	}

	

	
}