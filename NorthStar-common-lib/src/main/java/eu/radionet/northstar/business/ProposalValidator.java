// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.apache.struts.util.LabelValueBean;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import nl.astron.util.Utils;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.business.configuration.options.definition.FieldType;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.AdditionalIssues;
import eu.radionet.northstar.data.entities.Justification;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalError;
import eu.radionet.northstar.data.entities.Target;
import eu.radionet.northstar.data.entities.Thesis;

public class ProposalValidator {
	protected ContextType contextConfiguration = null;
	public ProposalValidator(){
	}
	public ProposalValidator(ContextType contextConfiguration){
		this.contextConfiguration = contextConfiguration;
	}
	
	public ProposalError validate(Proposal proposal) {
		ProposalError error = new ProposalError("proposal", "Proposal");
		
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		if (this.contextConfiguration == null){
			this.contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
			
		}
		FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		ProposalError errorChild = null;
		if (proposal.getJustification() != null) { 
			if (OptionsDelegate.allowedToDisplay(Constants.ENABLE_FIRST_JUSTIFICATION , fieldsDefinitionType))
			{
				errorChild = validate(proposal.getJustification(),telescopeConfiguration,true);
			}
			else
			{
				errorChild = validate(proposal.getJustification(),telescopeConfiguration,false);
			}
			if (!errorChild.isEmpty()) {
				error.getErrors().add(errorChild);
			}		
			if(OptionsDelegate.allowedToDisplay(Constants.ENABLE_PAGE_NUMBER_CONF , fieldsDefinitionType)){
				
				if(proposal.getSemester().getCategory().getCode().equals("envelope_sheet")||proposal.getSemester().getCategory().getCode().equals("ddt")||proposal.getSemester().getCategory().getCode().equals("progress_report"))
					errorChild =	validatePageNumber(proposal.getJustification(),proposal.getObservingRequest(),true);
				else	
					errorChild = validatePageNumber(proposal.getJustification(),proposal.getObservingRequest(),false);
			 }
			if (!errorChild.isEmpty()) {
				error.getErrors().add(errorChild);
			}
		} else {
			Justification justificationNew = new Justification();
			error.getErrors().add(validate(justificationNew,telescopeConfiguration,true));
		}
		if (proposal.getAdditionalIssues() != null) {
			errorChild = validate(proposal.getAdditionalIssues());
			if (!errorChild.isEmpty()) {
				error.getErrors().add(errorChild);
			}
		} else {
			AdditionalIssues additionalIssuesNew = new AdditionalIssues();
			errorChild = validate(additionalIssuesNew);
			if (!errorChild.isEmpty()) {
				error.getErrors().add(errorChild);
			}
		}
		List urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_OBSERVATIONS, new HashMap(), contextConfiguration);
		int maxObs =0;
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			String Url =  urlValue.getValue(); 
			maxObs = AstronConverter.toInteger(Url).intValue();
				
			
		}
		
		if(maxObs > 0){
			errorChild = validate(proposal.getObservingRequest());
			 Double reqTime = NorthStarDelegate.getResourceValue(
					 proposal.getObservingRequest().getAllocations(),telescopeConfiguration+"_"+ "requestedTime" );
			 Double minTime = NorthStarDelegate.getResourceValue(
					 proposal.getObservingRequest().getAllocations(),telescopeConfiguration+"_"+ "minimumTime" );
			if(minTime!=null && reqTime!=null){
				if(minTime>reqTime)
					errorChild=	new ProposalError("minimumTime","Minimum Time");
			}
			 if (!errorChild.isEmpty()) {
				error.getErrors().add(errorChild);
			}
		}
		return error;
	}
	

	public ProposalError validatePageNumber(Justification justification,
			ObservingRequest observingRequest, boolean b) {
		
		ProposalError error = new ProposalError("justification","Justification");
			
		return error;
	}
	public ProposalError validate(AdditionalIssues additionalIssues) {
        ProposalError error = new ProposalError("additionalIssues","Additional information");
        if (additionalIssues.isLinkedProposals()) {
            if (AstronValidator.isBlankOrNull(additionalIssues.getLinkedProposalsSpecifics())){
                error.getErrors().add(new ProposalError("linkedProposalsSpecifics","Details linked proposals"));
            }
        }
        if (additionalIssues.isLinkedProposalsElsewhere()) {
            if (AstronValidator.isBlankOrNull(additionalIssues.getLinkedProposalsElsewhereSpecifics())){
                error.getErrors().add(new ProposalError("linkedProposalsElsewhereSpecifics","Details linked proposals elsewhere"));
            }
        }
        if (additionalIssues.isPreviousAllocations()) {
            if (AstronValidator.isBlankOrNull(additionalIssues.getPreviousAllocationsSpecifics())){
                error.getErrors().add(new ProposalError("previousAllocationsSpecifics","Details previous allocations"));
            }
        }
        
        if (additionalIssues.isEnableRelatedPublications()) {
            if (AstronValidator.isBlankOrNull(additionalIssues.getRelatedPublications())){
                error.getErrors().add(new ProposalError("relatedPublicationsText","Related Publication"));
            }
        }
        
        if (additionalIssues.isEnablePreviousInvolvedProposal()) {
        	if (AstronValidator.isBlankOrNull(additionalIssues.getRelatedPreviousInvolvedProposal())){
        		error.getErrors().add(new ProposalError("relatedPreviousInvolvedProposalText","Related previously involved proposals"));
        	}
        }
        
        
        if (additionalIssues.getTheses().size() > 0) {
            for (int i = 0; i < additionalIssues.getTheses().size(); i++) {
                Thesis thesis = (Thesis) additionalIssues.getTheses().get(i);

                if (AstronValidator.isBlankOrNull(thesis.getStudentName())){
                    error.getErrors().add(new ProposalError("studentName","Name of student"));
                }
                if (AstronValidator.isBlankOrNull(thesis.getStudentLevel())){
                    error.getErrors().add(new ProposalError("studentLevel","Level of student"));
                }
                if (AstronValidator.isBlankOrNull(thesis.getSupervisorName())){
                    error.getErrors().add(new ProposalError("supervisorName","Name of supervisor"));
                }
                if (thesis.getExpectedCompletionDate()==null){
                    error.getErrors().add(new ProposalError("expectedCompletionDate","Expected completion date"));
                }
            }
        }
        
        if(additionalIssues.isEnableRelatedPublications())
        {
        	 if (AstronValidator.isBlankOrNull(additionalIssues.getRelatedPublications())){
                 error.getErrors().add(new ProposalError("relatedPublications","Related Publication is a mandatory Field"));
             }
        }
        
        if (additionalIssues.isEnablePreviousInvolvedProposal()) 
        {
        	if (AstronValidator.isBlankOrNull(additionalIssues.getRelatedPreviousInvolvedProposal())){
        		error.getErrors().add(new ProposalError("relatedPreviousInvolvedProposal","Related previously involved proposal is a mandatory Field"));
        	}
        }
        return error;
    }
    
    public ProposalError validate(Justification justification, TelescopeConfiguration telescopeConfiguration, boolean scientificRequired) {
        ProposalError error = new ProposalError("justification","Justification");
        if (AstronValidator.isBlankOrNull(justification.getTitle())) {
            error.getErrors().add(new ProposalError("title","Title"));
        }else{
        	if (justification.getTitle().length() > telescopeConfiguration.getMaxCharsForTitle() ) {
        		error.getErrors().add(new ProposalError("title","Title"));
        	}
        }
        if (AstronValidator.isBlankOrNull(justification.getAbstractText())){
            error.getErrors().add(new ProposalError("abstractText","Abstract"));
        }else{
        	Integer abstractCount = Utils.countWords(justification.getAbstractText());
        	if (abstractCount.intValue() >  telescopeConfiguration.getMaxWordsForAbstract() ){
        		error.getErrors().add(new ProposalError("abstractText","Abstract"));
        	}
        }
        
		List technicalQuestions = OptionsDelegate.getOptionsConfigurationTypes(
				Constants.DISPLAY_TECH_JUSTIFICATIONS, new HashMap(), contextConfiguration);
		if (technicalQuestions != null && technicalQuestions.size() > 0) {
			ConfigureOptionType optionFieldConfiguration = (ConfigureOptionType) technicalQuestions.get(0);
			ConfigureOptionType techOptions = OptionsDelegate.getOption(optionFieldConfiguration.getValue(),
					technicalQuestions);
			FieldsDefinitionType questions = techOptions.getSubfields();
			if (OptionsDelegate.allowedToDisplay(Constants.DISPLAY_OBSERVATION_STRATEGY, questions)) {
				if ((justification.getObservationStrategyText() == null)
						|| (justification.getObservationStrategyText().isEmpty())) {
					error.getErrors().add(new ProposalError("observationStrategy", "Observation Strategy required"));
				} else if ((justification.getObservationStrategyText().trim().split("\\s+").length) > 300){
					error.getErrors().add(new ProposalError("observationStrategy", "Observation Strategy too long"));
				}
			}
		}
        
        // this part checks if a scientific file has been uploaded..  
        if(scientificRequired){
          if (AstronValidator.isBlankOrNull(justification.getScientificFileName())){
            error.getErrors().add(new ProposalError("scientificFileName","Scientific justification file"));
          }
        }
         
        return error;

    }
    public ProposalError validate(ObservingRequest observingRequest) {
        ProposalError error = new ProposalError("observingRequest",
                "Observing Request");
        for (int i = 0; i < observingRequest.getObservations().size(); i++) {
            Observation observation = (Observation) observingRequest.getObservations().get(i);
            ProposalError errorChild = validate(observation, observingRequest);
            if (!errorChild.isEmpty()) {
                error.getErrors().add(errorChild);
            }
        }
        return error;
    }
    
    public ProposalError validate(Observation observation,ObservingRequest observingRequest) 
    {
        ProposalError error = new ProposalError("observation", "Observation");
        if (observation.getTargets() != null){
	        Iterator targetIt = observation.getTargets().iterator();
	        while (targetIt.hasNext())
	        {
	            ProposalError errorChild = validate((Target) targetIt.next());
	            if (!errorChild.isEmpty()) 
	            {
	                error.getErrors().add(errorChild);
	            }
	        }
        }else{
        	observation.setTargets(new ArrayList());
        }
        return error;   	
    }	 
    	
    public ProposalError validate(Target target) {
        ProposalError error = new ProposalError("Target", "Target ("
                + target.getFieldName() + ")");
        if (AstronValidator.isBlankOrNull(target.getFieldName())) {
            error.getErrors().add(new ProposalError("fieldName", "Fieldname"));
        }
        if (target.getEpoch() != null && target.getEpoch().equals("Other system")) {
            if (target.getDecl() == null && target.getRa() != null) {
                error.getErrors().add(new ProposalError("decl", "Dec"));
            }
            if (target.getDecl() != null && target.getRa() == null) {
                error.getErrors().add(new ProposalError("ra", "Ra"));
            }
        } else {
            if (target.getDecl() == null) {
                error.getErrors().add(new ProposalError("decl", "Dec"));
            }
            if (target.getRa() == null) {
                error.getErrors().add(new ProposalError("ra", "Ra"));
            }
        }

        return error;
    }
}
