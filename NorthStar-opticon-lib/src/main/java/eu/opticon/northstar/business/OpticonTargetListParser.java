// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Vector;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.AstroCoordinate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.exception.NorthStarException;
import eu.radionet.northstar.business.exception.ParseTargetListException;
import eu.radionet.northstar.business.targetlist.TargetFileBean;
import eu.radionet.northstar.business.targetlist.TargetListParser;
import eu.radionet.northstar.control.proposal.observingrequest.ValueBean;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ResourceType;
import eu.radionet.northstar.data.entities.Target;

/**
 * This class parses a telescope specified target list to observation objects
 * In this skeleton example it parses a list using the following scheme:
 * <blockquote>Fieldname RA Dec Epoch [Exposure_time]</blockquote>
 */
public class OpticonTargetListParser extends TargetListParser {

	/*
	 * List of telescope specific optional headers
	 */
	protected static final String EXPOSURE_TIME = "exposure_time";
	protected static final String MOON = "moon";
	protected static final String SEEING = "seeing";
	protected static final String SEEING_LOWER = "seeing_lower";
	protected static final String SEEING_UPPER = "seeing_upper";
	protected static final String WATER = "water";
	protected static final String SN = "S/N";
	protected static final String MAGNITUDE = "magnitude";
	protected static final String FLUX = "flux";
	protected static final String SKY_QUALITY = "sky_quality";
	protected static final String COMMENTS = "comments";

	private String[] optionalHeaders = new String[] {EXPOSURE_TIME, MOON, SEEING_LOWER, SEEING_UPPER, WATER, SN, MAGNITUDE, FLUX, SKY_QUALITY, COMMENTS  };
	protected NorthStarDelegate northStarDelegate = null;

	public List parseTargetListFile(InputStream targetListFile, List list, int maxTargetonSameRun)
	throws IOException, ParseTargetListException, DatabaseException {
		boolean oldFormat = false;
		lineNumber = 1;
		List targets = new ArrayList();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				targetListFile));
		String line = br.readLine();
		String splitword = "[ \\t\\x0B]+";
		if (line.matches(".*;.*")) {
			splitword = ";";
			oldFormat = true;
		}
		// spaces and tabs
		String[] header = null;
		/*
		 * parse al the lines
		 */
		while (line != null && line.trim().length() > 0) {
			String[] pieces = line.trim().split(splitword);
			OpticonTarget target = null;
			/*
			 * if there is an valid header
			 */
			if (lineNumber == 1 && oldFormat && isHeader(pieces)) {
				header = pieces;
			} else {
				target = createTarget(pieces);
				targets.add(target);
				//targets.add(createObservation(targetFileBean));
			}
			line = br.readLine();
			lineNumber++;
		}

		return targets;
	}


	public List parseTargetListFile(InputStream targetListFile,
			Observation templateObservation, List list, int maxTargetOnSameRun) throws IOException,
			ParseTargetListException, DatabaseException, NorthStarException {
		boolean oldFormat = false;
		lineNumber = 1;
		List targets = new ArrayList();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				targetListFile));
		String line = br.readLine();
		String splitword = "[ \\t\\x0B]+";
		if (line.matches(".*;.*")) {
			splitword = ";";
			oldFormat = true;
		}
		String[] header = null;
		/*
		 * parse al the lines
		 */
		while (line != null && line.trim().length() > 0) {
			String[] pieces = line.trim().split(splitword);
			OpticonTarget target = null;
			/*
			 * if there is an valid header
			 */
			if (lineNumber == 1 && oldFormat && isHeader(pieces)) {
				header = pieces;
			} else {
				target = createTarget(pieces);
				targets.add(target);
			}
			line = br.readLine();
			lineNumber++;
		}

		return targets;
	}

	public List parseTargetListFile(InputStream targetListFile,
			Target templateTarget, List list, int maxTargetOnSameRun) throws IOException,
			ParseTargetListException, DatabaseException, NorthStarException {
		boolean oldFormat = false;
		lineNumber = 1;
		List targets = new Vector();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				targetListFile));
		String line = br.readLine();
		String splitword = "[ \\t\\x0B]+";
		if (line.matches(".*;.*")) {
			splitword = ";";
			oldFormat = true;
		}
		String[] header = null;
		/*
		 * parse al the lines
		 */
		while (line != null && line.trim().length() > 0) {
			String[] pieces = line.trim().split(splitword);
			OpticonTarget target = null;
			/*
			 * if there is an valid header
			 */
			if (lineNumber == 1 && oldFormat && isHeader(pieces)) {
				header = pieces;
			} else {
				target = createTarget(pieces);
				targets.add(target);
			}
			line = br.readLine();
			lineNumber++;
		}

		return targets;
	}	


	private OpticonTarget createTarget(String[] pieces) throws ParseTargetListException {
		OpticonTarget target = new OpticonTarget();

		if(pieces[0].trim().startsWith("\"")){
			pieces = createTargetName(pieces);
		}
		
		if (pieces.length >= 4) 
		{
			target.setFieldName(pieces[0].trim());
			if (!AstronValidator.isRa(pieces[1].trim())) 
			{
				throw new ParseTargetListException("error.parser.notra", lineNumber);
			} 
			else if (!AstronValidator.isDec(pieces[2].trim())) 
			{
				throw new ParseTargetListException("error.parser.notdec",lineNumber);
			} 
			else 
			{
				try {
					AstroCoordinate raDec = new AstroCoordinate(pieces[1].trim(), pieces[2].trim());
					target.setRa(new Double(raDec.RA()));
					target.setDecl(new Double(raDec.Dec()));
				} catch (Exception e) {
					// should not happen
				}
			}
			if (isEpoch(pieces[3].trim())) 
			{
				target.setEpoch(convertEpoch(pieces[3].trim()));
			} 
			else 
			{
				throw new ParseTargetListException("error.parser.notepoch",	lineNumber);
			}
			if (pieces.length > 4)
			{	
				if (isTime(pieces[4].trim()))
				{
					Integer targetTime = timeToSeconds(pieces[4].trim());
					Double totalDuration = new Double(targetTime.doubleValue());
					ResourceType resourceType =null;
					try {
						northStarDelegate = NorthStarDelegate.getInstance();
						resourceType = northStarDelegate.getResourceType(OpticonConstants.TIME, OpticonConstants.SECONDS);
					} catch (DatabaseException e) {
						log.error("db error");
					}
					Map allocations = new HashMap();
					NorthStarDelegate.setResourceValue(allocations, OpticonConstants.TOTAL_DURATION, totalDuration,	resourceType); 
					target.setAllocations(allocations);
				}
				else
				{
					throw new ParseTargetListException("error.parser.nottime",	lineNumber);
				}
				if (pieces.length > 5)
				{
					if (isRuns(pieces[5].trim())){
						target.setSelectedRuns(convertRuns(pieces[5].trim()));
						if (pieces.length > 6)
						{
							if (AstronValidator.isPositiveDouble(pieces[6].trim())) 
							{
								target.setSn(AstronConverter.toDouble(pieces[6].trim()));
								
								if (pieces.length > 7)
								{
									if (AstronValidator.isPositiveDouble(pieces[7].trim())) 
									{
										target.setMagnitude(AstronConverter.toDouble(pieces[7].trim()));
										if (pieces.length > 8)
										{
											if (AstronValidator.isPositiveDouble(pieces[8].trim())) 
											{
												target.setFlux(AstronConverter.toDouble(pieces[8].trim()));
												
												if (pieces.length > 9)
												{
													if (AstronValidator.isPositiveDouble(pieces[9].trim()) ||
														pieces[9].trim().equalsIgnoreCase("0")) 
													{
														target.setDiameter(AstronConverter.toDouble(pieces[9].trim()));
														if (pieces.length > 10)
														{
															target.setComments(concatListFromIndex(pieces, 10));
														}
													}
													else
													{
														throw new ParseTargetListException("error.parser.notdiameter",	lineNumber);
													}		    	
												}
											}
											else
											{
												throw new ParseTargetListException("error.parser.notflux",	lineNumber);
											}	
										}
									}
									else
									{
										throw new ParseTargetListException("error.parser.notmagnitude",	lineNumber);
									}		    	    
								}
							}
							else
							{
								throw new ParseTargetListException("error.parser.notsn",	lineNumber);
							}	
						}
					}
					else
					{
						throw new ParseTargetListException("error.parser.notrun",	lineNumber);
					}
					
				}

			} 
		}else 
		{
			throw new ParseTargetListException("error.parser.notenoughfields", lineNumber);
		}


		return target;
	}


	private String[] createTargetName(String[] pieces) throws ParseTargetListException {
		String name="";
		int i=0;
		while(!pieces[i].trim().endsWith("\"") && i < pieces.length -1){
			name+=pieces[i]+" ";
			i++;
		}
		name+=pieces[i];
		// no closing quotes
		if(i== pieces.length -1){
			throw new ParseTargetListException("error.parser.noquotes",lineNumber);
		}
		name=name.substring(1);
		name=name.substring(0, name.length()-1);
		String[] result = new String[pieces.length-i] ;
		result[0]=name;
		for(int j=1;j<pieces.length-i;j++){
			result[j]=pieces[j+i];
		}
		
		return result;
		
		
	}


	private List convertRuns(String runs) {
		String[] list = runs.split(",");
		List result = new ArrayList();
		if (list != null && list.length > 0) {
			for (int i = 0; i < list.length; i++) {
				String value = list[i].trim();
				// only add single characters
				if(value.length()==1){
					result.add(value);
				}
			}
		}
		return result;
	}


	private boolean isRuns(String runs) {
		if(runs.length() > 1){
			String[] list = runs.split(",");
			if (list.length==1){
				return false;
			}
		}
		return true;
	}


	/**
	 * telescope specific  targetlist parser
	 * @param proposalDelegate
	 */
	public OpticonTargetListParser(ProposalDelegate proposalDelegate) {
		super(proposalDelegate);
	}

	/**
	 * parses the headers
	 * @see eu.radionet.northstar.business.targetlist.TargetListParser#parseHeader(java.util.List)
	 */
	protected List parseHeader(List possibleHeaders) throws ParseTargetListException
	{
		for (int i = 0; i < optionalHeaders.length; i++){
			boolean found = false;
			for (int j = possibleHeaders.size() - 1; j >= 0 && !found;j--){
				String possibleHeader = (String )possibleHeaders.get(j);
				if (possibleHeader.equalsIgnoreCase(optionalHeaders[i])){
					found = true;
					possibleHeaders.remove(j);
				}
			}
		}
		return super.parseHeader(possibleHeaders);
	}

	/**
	 * Converts a row of the target list into a Java bean
	 * @see eu.radionet.northstar.business.targetlist.TargetListParser#createTargetFileBean(java.lang.String[])
	 */
	protected TargetFileBean createTargetFileBean(String[] pieces)
	throws ParseTargetListException 
	{

		TargetFileBean targetFileBean = new TargetFileBean();

		if (pieces.length >= 4) 
		{
			targetFileBean.getFields().put(FIELD_NAME, pieces[0].trim());
			if (!AstronValidator.isRa(pieces[1].trim())) 
			{
				throw new ParseTargetListException("error.parser.notra", lineNumber);
			} 
			else if (!AstronValidator.isDec(pieces[2].trim())) 
			{
				throw new ParseTargetListException("error.parser.notdec",lineNumber);
			} 
			else 
			{
				targetFileBean.getFields().put(RA, pieces[1].trim());
				targetFileBean.getFields().put(DEC, pieces[2].trim());
			}
			if (isEpoch(pieces[3].trim())) 
			{
				targetFileBean.getFields().put(EPOCH, pieces[3].trim());
			} 
			else 
			{
				throw new ParseTargetListException("error.parser.notepoch",	lineNumber);
			}
			if (pieces.length > 4)
			{	
				if (isTime(pieces[4].trim()))
				{
					targetFileBean.getFields().put(EXPOSURE_TIME, pieces[4].trim());
				}
				else
				{
					throw new ParseTargetListException("error.parser.nottime",	lineNumber);
				}
				if (pieces.length > 5)
				{
					if (isMoon(pieces[5].trim())) 
					{
						targetFileBean.getFields().put(MOON, pieces[5].trim());
						if (pieces.length > 6)
						{
							if (isSeeingRange(pieces[6].trim())) 
							{
								String[] seeingValues = pieces[6].trim().split("-");
								targetFileBean.getFields().put(SEEING_LOWER, seeingValues[0]);
								targetFileBean.getFields().put(SEEING_UPPER, seeingValues[1]);
								if (pieces.length > 7)
								{
									if (isWater(pieces[7].trim())) 
									{
										targetFileBean.getFields().put(WATER, pieces[7].trim());
										if (pieces.length > 8)
										{
											if (AstronValidator.isPositiveDouble(pieces[8].trim())) 
											{
												targetFileBean.getFields().put(SN, pieces[8].trim());
												if (pieces.length > 9)
												{
													if (AstronValidator.isPositiveDouble(pieces[9].trim())) 
													{
														targetFileBean.getFields().put(MAGNITUDE, pieces[9].trim());
														if (pieces.length > 10)
														{
															if (AstronValidator.isPositiveDouble(pieces[10].trim())) 
															{
																targetFileBean.getFields().put(FLUX, pieces[10].trim());
																if (pieces.length > 11)
																{
																	if (isSkyQuality(pieces[11].trim())) 
																	{
																		targetFileBean.getFields().put(SKY_QUALITY, pieces[11].trim());
																		if (pieces.length > 12)
																		{
																			targetFileBean.getFields().put(COMMENTS, concatListFromIndex(pieces, 12));
																		}
																	}
																	else
																	{
																		throw new ParseTargetListException("error.parser.notskyquality",	lineNumber);
																	}		    	
																}
															}
															else
															{
																throw new ParseTargetListException("error.parser.notflux",	lineNumber);
															}	
														}
													}
													else
													{
														throw new ParseTargetListException("error.parser.notmagnitude",	lineNumber);
													}		    	    
												}
											}
											else
											{
												throw new ParseTargetListException("error.parser.notsn",	lineNumber);
											}	
										}	    	    	    	        
									}
									else
									{
										throw new ParseTargetListException("error.parser.notwater",	lineNumber);
									}	
								}
							}
							else 
							{
								throw new ParseTargetListException("error.parser.notseeing",lineNumber);
							}	
						}

					} 
					else 
					{
						throw new ParseTargetListException("error.parser.notdec",	lineNumber);
					}

				}
			}	
		} 
		else 
		{
			throw new ParseTargetListException("error.parser.notenoughfields", lineNumber);
		}
		return targetFileBean;
	}

	/**
	 * Creates the observation object out of a bean representing a row
	 * in the target list.
	 * @see eu.radionet.northstar.business.targetlist.TargetListParser#createObservation(eu.radionet.northstar.business.targetlist.TargetFileBean)
	 */
	protected List createTargetValues(TargetFileBean targetFileBean)
	throws ParseTargetListException, DatabaseException 
	{
		List targetValues = new ArrayList();
		if (!targetFileBean.getFields().isEmpty()) 
		{
			Map fields = targetFileBean.getFields();
			/*
			 * retrieve fields
			 */
			String fieldName = (String) fields.get(FIELD_NAME);
			String ra = (String) fields.get(RA);
			String dec = (String) fields.get(DEC);
			String epoch = (String) fields.get(EPOCH);
			if (AstronValidator.isBlankOrNull(fieldName)) {
				throw new ParseTargetListException(
						"error.parser.fieldname.mandatory", lineNumber);
			}
			if (!AstronValidator.isRa(ra)) {
				throw new ParseTargetListException("error.parser.notra",
						lineNumber);
			}
			if (!AstronValidator.isDec(dec)) {
				throw new ParseTargetListException("error.parser.notdec",
						lineNumber);
			}
			if (AstronValidator.isBlankOrNull(epoch) && isEpoch(epoch)) {
				throw new ParseTargetListException("error.parser.notepoch",
						lineNumber);
			}

			targetValues.add(new ValueBean(fieldName));
			targetValues.add(new ValueBean(ra));
			targetValues.add(new ValueBean(dec));
			targetValues.add(new ValueBean(convertEpoch(epoch)));
			String exposureTime = (String) fields.get(EXPOSURE_TIME);
			if (!AstronValidator.isBlankOrNull(exposureTime)) 
			{
				targetValues.add(new ValueBean(timeToHours(exposureTime).toString()));
			}
			else
			{
				targetValues.add(new ValueBean(null));
			}
			targetValues.add(new ValueBean(convertMoon((String) fields.get(MOON))));
			targetValues.add(new ValueBean(null, false)); // SEEING needs to be added while not used.
			targetValues.add(new ValueBean((String) fields.get(SEEING_LOWER)));
			targetValues.add(new ValueBean((String) fields.get(SEEING_UPPER)));
			targetValues.add(new ValueBean(convertWater((String) fields.get(WATER))));
			targetValues.add(new ValueBean((String) fields.get(SN)));
			targetValues.add(new ValueBean((String) fields.get(MAGNITUDE)));
			targetValues.add(new ValueBean((String) fields.get(FLUX)));
			targetValues.add(new ValueBean(convertSkyQuality((String) fields.get(SKY_QUALITY))));
			targetValues.add(new ValueBean((String) fields.get(COMMENTS)));
		} 
		return targetValues;
	}



	/*
	 *  (non-Javadoc)
	 * @see eu.radionet.northstar.business.targetlist.TargetListParser#createObservation(eu.radionet.northstar.business.targetlist.TargetFileBean, eu.radionet.northstar.data.entities.Observation)
	 */
	protected List createTargetValues(TargetFileBean targetFileBean, Target templateTarget)  
	throws ParseTargetListException, DatabaseException
	{
		List targetValues =  createTargetValues(targetFileBean);
		OpticonTarget opticonTemplateTarget = (OpticonTarget) templateTarget;
		if (((ValueBean) targetValues.get(4)).getValue() == null && opticonTemplateTarget.getAllocations() != null)
		{
			Double totalExposureTime = NorthStarDelegate.getResourceValue(
					opticonTemplateTarget.getAllocations(),	OpticonConstants.TOTAL_DURATION);
			targetValues.set(4, new ValueBean(AstronConverter.getMinutesFromSeconds(totalExposureTime)));
		}
		if (((ValueBean) targetValues.get(5)).getValue()== null)
		{
			targetValues.set(5, new ValueBean(opticonTemplateTarget.getMoon()));
		}
		if (((ValueBean) targetValues.get(6)).getValue()== null)
		{
			targetValues.set(6, new ValueBean(opticonTemplateTarget.getSeeing(),false));
		}
		if (((ValueBean) targetValues.get(7)).getValue()== null)
		{
			targetValues.set(7, new ValueBean(AstronConverter.toString(opticonTemplateTarget.getSeeingLower())));
		}
		if (((ValueBean) targetValues.get(8)).getValue()== null)
		{
			targetValues.set(8, new ValueBean(AstronConverter.toString(opticonTemplateTarget.getSeeingUpper())));
		}
		if (((ValueBean) targetValues.get(9)).getValue()== null)
		{
			targetValues.set(9, new ValueBean(opticonTemplateTarget.getWater()));
		}
		if (((ValueBean) targetValues.get(10)).getValue()== null)
		{
			targetValues.set(10, 
					new ValueBean(AstronConverter.toString(opticonTemplateTarget.getSn())));
		}
		if (((ValueBean) targetValues.get(11)).getValue()== null)
		{
			targetValues.set(11, 
					new ValueBean(AstronConverter.toString(opticonTemplateTarget.getMagnitude())));

		}
		if (((ValueBean) targetValues.get(12)).getValue()== null)
		{
			targetValues.set(12, 
					new ValueBean(AstronConverter.toString(opticonTemplateTarget.getFlux())));

		}
		if (((ValueBean) targetValues.get(13)).getValue()== null)
		{
			targetValues.set(13, new ValueBean(opticonTemplateTarget.getSkyQuality()));
		}
		if (((ValueBean) targetValues.get(14)).getValue()== null)
		{
			targetValues.set(14, new ValueBean(opticonTemplateTarget.getComments()));
		}
		return targetValues;
	}


	/**
	 * Sets the Resource  for all allocations in the observation
	 * @param observation
	 * @param exposureTime
	 * @throws DatabaseException
	 */
	protected void setTotalDuration(OpticonTarget target, Integer exposureTime) 
	throws DatabaseException
	{
		if (exposureTime != null)
		{
			NorthStarDelegate northStarDelegate = NorthStarDelegate.getInstance();
			Double value = new Double(exposureTime.doubleValue());
			ResourceType resourceType = northStarDelegate.getResourceType("time","seconds");
			Map allocations = target.getAllocations();
			NorthStarDelegate.setResourceValue(
					allocations, OpticonConstants.TOTAL_DURATION, value,resourceType);
		}
	}


	protected Observation createObservation(
			TargetFileBean targetFileBean, Observation observation) throws ParseTargetListException, DatabaseException
			{
		//not used
		return null;
			}

	protected Observation createObservation(TargetFileBean targetFileBean) throws ParseTargetListException, DatabaseException
	{
		//not used
		return null;
	}	

	protected boolean isTime(String time)
	{
		boolean result = false;
		if (time.endsWith("s") || time.endsWith("m") || time.endsWith("h"))
		{
			result = AstronValidator.isInt(time.substring(0, time.length()-1));
		} 
		return result;
	}

	protected boolean isMoon(String moon)
	{
		boolean result = false;
		result = moon.equalsIgnoreCase("dark") 
		|| moon.equalsIgnoreCase("firstq") 
		|| moon.equalsIgnoreCase("lastq") 
		|| moon.equalsIgnoreCase("bright");
		return result;
	}

	protected boolean isSkyQuality(String skyQuality)
	{
		boolean result = false;
		result = skyQuality.equalsIgnoreCase("photometric") 
		|| skyQuality.equalsIgnoreCase("thin") 
		|| skyQuality.equalsIgnoreCase("thick");
		return result;
	}

	protected String convertSkyQuality(String input)
	{
		String output = "photometric";
		if (input == null)
		{
			return null;
		}
		if (input.equalsIgnoreCase("thin"))
		{
			output = "thin cirrus";
		}
		if (input.equalsIgnoreCase("thick"))
		{
			output = "thick cirrus";
		}
		return output;
	}

	protected String convertWater(String input)
	{
		String output = "dry (0.09)";
		if (input == null)
		{
			return null;
		}
		if (input.equalsIgnoreCase("dontcare"))
		{
			output = "don't care";
		}
		if (input.equalsIgnoreCase("average"))
		{
			output = "average (0.15)";
		}
		return output;
	}

	protected String convertMoon(String input)
	{
		String output = "dark";
		if (input == null)
		{
			return null;
		}
		if (input.equalsIgnoreCase("bright"))
		{
			output = "bright";
		}
		if (input.equalsIgnoreCase("firstq"))
		{
			output = "first quarter";
		}
		if (input.equalsIgnoreCase("lastq"))
		{
			output = "last quarter";
		}
		return output;
	}



	/*
	protected void setSeeing(OpticonTarget opticonTarget, String seeing)
	{
		if (seeing!= null)
		{
			if (seeing.equalsIgnoreCase("<0.5")	
					|| seeing.equalsIgnoreCase(">0.9"))
			{		if (((ValueBean) targetValues.get(13)).getValue()== null)
		{
			targetValues.set(10, new ValueBean(opticonTemplateTarget.getComments()));
		}

				opticonTarget.setSeeing(seeing);
			}
			else if (seeing.equalsIgnoreCase("0.5-0.9"))
			{
				opticonTarget.setSeeing("0.5 - 0.9");
			}
			else if (AstronValidator.isPositiveDouble(seeing))
			{
				double seeingDouble = AstronConverter.toDouble(seeing).doubleValue();
				if (seeingDouble < 0.5)
				{
					opticonTarget.setSeeing("<0.5");
				}
				else if (seeingDouble > 0.9)
				{
					opticonTarget.setSeeing(">0.9");
				}
				else
				{
					opticonTarget.setSeeing("0.5 - 0.9");
				}
			}
		}
	}
	 */

	protected boolean isSeeingRange(String seeing)
	{
		boolean result = false;
		String[] twoDoubles = seeing.split("-");
		if (twoDoubles.length == 2)
		{
			result = AstronValidator.isPositiveDouble(twoDoubles[0]) && AstronValidator.isPositiveDouble(twoDoubles[1]);
		}
		return result;

	}

	protected boolean isSeeing(String seeing)
	{
		boolean result = false;
		result = AstronValidator.isPositiveDouble(seeing) 
		|| seeing.equalsIgnoreCase("<0.5")
		|| seeing.equalsIgnoreCase("0.5-0.9")
		|| seeing.equalsIgnoreCase(">0.9");
		return result;
	}

	protected boolean isWater(String water)
	{
		boolean result = false;
		result = water.equalsIgnoreCase("dontcare") 
		|| water.equalsIgnoreCase("dry") 
		|| water.equalsIgnoreCase("average");
		return result;
	}

	protected String concatListFromIndex(String[] pieces, int index)
	{
		String result = pieces[index].trim();
		if (pieces.length > index+1)
		{
			for (int i = index+1; i < pieces.length; i++)
			{
				result = result + ' ' + pieces[i].trim();
			}
		}
		return result;
	}

}

