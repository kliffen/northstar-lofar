// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ObservingRequest.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: ObservingRequest.java,v 1.2 2006-06-08 10:22:14 verhoef Exp $
 *
 */
package eu.radionet.northstar.data.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * The ObservingRequest provides
 * 
 * @author Bastiaan Verhoef
 *  
 */
public class ObservingRequest implements Serializable {

    protected Integer id = null;

    protected String overallRequiredSchedConstraints = null;

    protected String overallPreferredSchedConstraints = null;

    protected List<Observation> observations = new ArrayList();
    
    protected List<Observation> pipelines = new ArrayList();
    protected List<Observation> tbbObservations = new ArrayList();
    
    protected Map allocations = new HashMap();
    
    protected List<Target> targets = new ArrayList();
    
   // protected List targetsPerObservation = new ArrayList();

    /**
     * @return Returns the id.
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            The id to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * @return Returns the observations.
     */
    public List<Observation> getObservations() {
        return observations;
    }

    /**
     * @param observations
     *            The observations to set.
     */
    public void setObservations(List observations) {
        this.observations = observations;
    }

    /**
     * @return Returns the overallPreferredSchedConstraints.
     */
    public String getOverallPreferredSchedConstraints() {
        return overallPreferredSchedConstraints;
    }

    /**
     * @param overallPreferredSchedConstraints
     *            The overallPreferredSchedConstraints to set.
     */
    public void setOverallPreferredSchedConstraints(
            String overallPreferredSchedConstraints) {
        this.overallPreferredSchedConstraints = overallPreferredSchedConstraints;
    }

    /**
     * @return Returns the overallRequiredSchedConstraints.
     */
    public String getOverallRequiredSchedConstraints() {
        return overallRequiredSchedConstraints;
    }

    /**
     * @param overallRequiredSchedConstraints
     *            The overallRequiredSchedConstraints to set.
     */
    public void setOverallRequiredSchedConstraints(
            String overallRequiredSchedConstraints) {
        this.overallRequiredSchedConstraints = overallRequiredSchedConstraints;
    }


    


	public Map getAllocations() {
		return allocations;
	}

	public void setAllocations(Map allocations) {
		this.allocations = allocations;
	}

	public List<Target> getTargets() {
		return targets;
	}

	public void setTargets(List targets) {
		this.targets = targets;
	}


	public List getTargetsByObservationId(Integer observationId) {
		if(observationId ==null){
			return null;
		}
		return getTargetsByObservationId(observationId.intValue());
	}
	
	public List getTargetsByObservationId(int observationId) {
		if (observations == null){
			return null;
		}
		Iterator it = observations.iterator();
		while(it.hasNext()){
			Observation tob =(Observation) it.next();
			if (tob.getId().intValue() == observationId){
				return tob.getTargets();
			}
		}
		return null;
	}

	public List<Observation> getPipelines() {
		return pipelines;
	}

	public void setPipelines(List pipelines) {
		this.pipelines = pipelines;
	}

	public List<Observation> getTbbObservations() {
		return tbbObservations;
	}

	public void setTbbObservations(List<Observation> tbbObservations) {
		this.tbbObservations = tbbObservations;
	}

	   
}