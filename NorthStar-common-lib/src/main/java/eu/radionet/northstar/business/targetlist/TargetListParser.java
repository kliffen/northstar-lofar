// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Feb 28, 2005
 *
 *
 */
package eu.radionet.northstar.business.targetlist;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.radionet.northstar.business.AstroCoordinate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.exception.NorthStarException;
import eu.radionet.northstar.business.exception.ParseTargetListException;
import eu.radionet.northstar.control.proposal.observingrequest.ValueBean;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Target;

/**
 * @author Anton Smit
 */
public abstract class TargetListParser {
	protected int lineNumber = 0;

	protected final static String FIELD_NAME = "fieldname";

	protected final static String RA = "ra";

	protected final static String DEC = "dec";

	protected final static String EPOCH = "epoch";

	protected final static String EPOCH_B1950 = "B1950";

	protected final static String EPOCH_J2000 = "J2000";

	protected final static String EPOCH_OTHER = "other";

	protected final static String EPOCH_OTHER_SYSTEM = "Other system";

	private String[] requiredHeaders = new String[] { FIELD_NAME, RA, DEC,
			EPOCH };

	protected Log log = LogFactory.getLog(this.getClass());

	protected ProposalDelegate proposalDelegate = null;

	public TargetListParser(ProposalDelegate proposalDelegate) {
		this.proposalDelegate = proposalDelegate;
	}

	public List parseTargetListFile(InputStream targetListFile, List list, int maxTargetonSameRun)
			throws IOException, ParseTargetListException, DatabaseException {
		boolean oldFormat = false;
		lineNumber = 1;
		List observations = new Vector();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				targetListFile));
		String line = br.readLine();
		String splitword = "[ \\t\\x0B]+";
		if (line.matches(".*;.*")) {
			splitword = ";";
			oldFormat = true;
		}
		// spaces and tabs
		String[] header = null;
		/*
		 * parse al the lines
		 */
		while (line != null && line.trim().length() > 0) {
			String[] pieces = line.trim().split(splitword);
			TargetFileBean targetFileBean = null;
			/*
			 * if there is an valid header
			 */
			if (lineNumber == 1 && oldFormat && isHeader(pieces)) {
				header = pieces;
			} else {
				if (header != null) {
					targetFileBean = createTargetFileBean(header, pieces);
				} else {
					targetFileBean = createTargetFileBean(pieces);
				}
				observations.add(createObservation(targetFileBean));
			}
			line = br.readLine();
			lineNumber++;
		}

		return observations;
	}

	public List parseTargetListFile(InputStream targetListFile,
			Observation templateObservation, List list, int maxTargetOnSameRun) throws IOException,
			ParseTargetListException, DatabaseException, NorthStarException {
		boolean oldFormat = false;
		lineNumber = 1;
		List observations = new Vector();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				targetListFile));
		String line = br.readLine();
		String splitword = "[ \\t\\x0B]+";
		if (line.matches(".*;.*")) {
			splitword = ";";
			oldFormat = true;
		}
		if (line.matches(".*,.*")) {
			splitword = ",";
		}
		String[] header = null;
		/*
		 * parse al the lines
		 */
		while (line != null && line.trim().length() > 0) {
			String[] pieces = line.trim().split(splitword);
			
			TargetFileBean targetFileBean = null;
			/*
			 * if there is an valid header
			 */
			if (lineNumber == 1 && oldFormat && isHeader(pieces)) {
				header = pieces;
			} else {
				if (header != null) {
					targetFileBean = createTargetFileBean(header, pieces);
				} else {
					targetFileBean = createTargetFileBean(pieces);
				}
				observations.add(createObservation(targetFileBean,
						templateObservation));
			}
			line = br.readLine();
			lineNumber++;
		}

		return observations;
	}

	public List parseTargetListFile(InputStream targetListFile,
			Target templateTarget, List list, int maxTargetOnSameRun) throws IOException,
			ParseTargetListException, DatabaseException, NorthStarException {
		boolean oldFormat = false;
		lineNumber = 1;
		List targets = new Vector();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				targetListFile));
		String line = br.readLine();
		String splitword = "[ \\t\\x0B]+";
		if (line.matches(".*;.*")) {
			splitword = ";";
			oldFormat = true;
		}
		String[] header = null;
		/*
		 * parse al the lines
		 */
		while (line != null && line.trim().length() > 0) {
			String[] pieces = line.trim().split(splitword);
			TargetFileBean targetFileBean = null;
			/*
			 * if there is an valid header
			 */
			if (lineNumber == 1 && oldFormat && isHeader(pieces)) {
				header = pieces;
			} else {
				if (header != null) {
					targetFileBean = createTargetFileBean(header, pieces);
				} else {
					targetFileBean = createTargetFileBean(pieces);
				}
				targets.add(createTargetValues(targetFileBean,
						templateTarget));
			}
			line = br.readLine();
			lineNumber++;
		}

		return targets;
	}	
	
	
	protected boolean isHeader(String[] possibleHeaders)
			throws ParseTargetListException {
		List headers = new ArrayList();
		for (int i = 0; i < possibleHeaders.length; i++) {
			headers.add(possibleHeaders[i]);
		}
		List remainingHeaders = parseHeader(headers);
		if (remainingHeaders.size() > 0) {
			throw new ParseTargetListException("error.parser.wrongheader",
					remainingHeaders, lineNumber);
		}
		return true;
	}

	protected List parseHeader(List possibleHeaders)
			throws ParseTargetListException {

		for (int i = 0; i < requiredHeaders.length; i++) {
			boolean found = false;
			for (int j = possibleHeaders.size() - 1; j >= 0 && !found; j--) {
				String possibleHeader = (String) possibleHeaders.get(j);
				if (possibleHeader.equalsIgnoreCase(requiredHeaders[i])) {
					found = true;
					possibleHeaders.remove(j);
				}
			}
			if (!found) {
				throw new ParseTargetListException(
						"error.parser.required.header", requiredHeaders[i],
						lineNumber);
			}
		}
		return possibleHeaders;
	}

	/**
	 * The createTargetFileBean method creates an targetFileBean with hashtable
	 * fields
	 * 
	 * @param header
	 * @param values
	 * @return
	 * @throws ParseTargetListException
	 */
	protected TargetFileBean createTargetFileBean(String[] header,
			String[] values) throws ParseTargetListException {
		TargetFileBean targetFileBean = new TargetFileBean();
		Hashtable fields = new Hashtable();
		if (values.length > header.length) {
			throw new ParseTargetListException("error.parser.toomanyfields",
					lineNumber);
		}
		for (int i = 0; i < header.length; i++) {
			if (i < values.length) {
				fields.put(header[i].toLowerCase(), values[i]);
			}
		}
		targetFileBean.setFields(fields);
		return targetFileBean;
	}

	protected abstract TargetFileBean createTargetFileBean(String[] pieces)
			throws ParseTargetListException;

	protected abstract Observation createObservation(
			TargetFileBean targetFileBean) throws ParseTargetListException, DatabaseException;

	/*
	protected abstract Target createTarget(
			TargetFileBean targetFileBean) throws ParseTargetListException, DatabaseException;

	*/
	protected abstract Observation createObservation(
			TargetFileBean targetFileBean, Observation templateObservation)
			throws ParseTargetListException, DatabaseException,
			NorthStarException;	
	/*
	protected abstract Target createTarget(
			TargetFileBean targetFileBean, Target templateTarget)
			throws ParseTargetListException, DatabaseException,
			NorthStarException;
*/
	protected abstract List createTargetValues(
			TargetFileBean targetFileBean, Target templateTarget)
			throws ParseTargetListException, DatabaseException,
			NorthStarException;

	
	/*protected void fillObservation (Observation observation, TargetFileBean targetFileBean)
		throws ParseTargetListException 
	{
		Target target = new Target();
		fillTarget(target,targetFileBean);
		observation.getTargets().add(target);
	}
	*/
	
	protected void fillObservingRequest (ObservingRequest observingRequest, TargetFileBean targetFileBean)
	throws ParseTargetListException 
	{
		Target target = new Target();
		fillTarget(target,targetFileBean);
		observingRequest.getTargets().add(target);
	}
	
	protected void fillTarget(Target target,
			TargetFileBean targetFileBean) throws ParseTargetListException {

		if (!targetFileBean.getFields().isEmpty()) {
			Hashtable fields = targetFileBean.getFields();
			/*
			 * retrieve fields
			 */
			String fieldName = (String) fields.get(FIELD_NAME);
			String ra = (String) fields.get(RA);
			String dec = (String) fields.get(DEC);
			String epoch = (String) fields.get(EPOCH);
			/*
			 * check fields
			 */
			if (AstronValidator.isBlankOrNull(fieldName)) {
				throw new ParseTargetListException(
						"error.parser.fieldname.mandatory", lineNumber);
			}
			if (!AstronValidator.isRa(ra)) {
				throw new ParseTargetListException("error.parser.notra",
						lineNumber);
			}
			if (!AstronValidator.isDec(dec)) {
				throw new ParseTargetListException("error.parser.notdec",
						lineNumber);
			}
			if (AstronValidator.isBlankOrNull(epoch) && isEpoch(epoch)) {
				throw new ParseTargetListException("error.parser.notepoch",
						lineNumber);
			}
		
			target.setFieldName(fieldName);
			try {
				AstroCoordinate raDec = new AstroCoordinate(ra, dec);
				target.setRa(new Double(raDec.RA()));
				target.setDecl(new Double(raDec.Dec()));
			} catch (Exception e) {
				// should not happen
			}

			target.setEpoch(convertEpoch(epoch));

		} else {
			target.setFieldName(targetFileBean.getFieldName());
			target.setRa(targetFileBean.getRa());
			target.setDecl(targetFileBean.getDec());
			target.setEpoch(convertEpoch(targetFileBean.getEpoch()));

		}
	} // TODO these two methods merge to one

	protected boolean isEpoch(String epoch) {
		return (epoch.equalsIgnoreCase(EPOCH_J2000)
				|| epoch.equalsIgnoreCase(EPOCH_B1950) || epoch
				.equalsIgnoreCase(EPOCH_OTHER));
	}

	protected String convertEpoch(String input) {
		if (input.equalsIgnoreCase(EPOCH_B1950)) {
			return EPOCH_B1950;
		} else if (input.equalsIgnoreCase(EPOCH_J2000)) {
			return EPOCH_J2000;
		} else {
			return EPOCH_OTHER_SYSTEM;
		}
	}

	protected Integer timeToSeconds(String time)
			throws ParseTargetListException {
		Integer seconds = null;
		if (time.endsWith("s")) {
			seconds = AstronConverter.toInteger(time.substring(0,
					time.length() - 1));
		} else if (time.endsWith("m")) {
			seconds = AstronConverter.getSecondsFromMinutes(time.substring(0,
					time.length() - 1));
		} else if (time.endsWith("h")) {
			seconds = AstronConverter.getSecondsFromHours(time.substring(0, time
					.length() - 1));
		} else // no unit, make it seconds
		{
			throw new ParseTargetListException("error.parser.nounittime",
					lineNumber);
			// seconds = converter.toInteger(time);
		}
		//FIXME there is a wrong conversion somewhere
		return seconds;

	}
	
	protected Integer timeToMinutes(String time) throws ParseTargetListException 
	{
		Integer minutes = null;
		if (time.endsWith("s")) 
		{
			minutes = AstronConverter.getMinutesFromSeconds(time.substring(0, time.length()-1));
		} 
		else if (time.endsWith("m")) 
		{
			minutes = AstronConverter.toInteger(time.substring(0, time.length()-1));
		} 
		else if (time.endsWith("h")) 
		{
			minutes = AstronConverter.getMinutesFromHours(time.substring(0, time.length()-1));
		} 
		else // no unit, make it seconds
		{
			throw new ParseTargetListException("error.parser.nounittime",	lineNumber);
		}
		return minutes;
	}
	
	protected Double timeToHours(String time) throws ParseTargetListException {
		Double hours = null;
		if (time.endsWith("h")) {
			hours = AstronConverter.toDouble(time.substring(0,
					time.length() - 1));
		} else if (time.endsWith("m")) {
			hours = AstronConverter.getHoursFromMinutes(time.substring(0,
					time.length() - 1));
		} else if (time.endsWith("s")) {
			hours = AstronConverter.getHoursFromSeconds(time.substring(0, time
					.length() - 1));
		} else // no unit, make it seconds
		{
			throw new ParseTargetListException("error.parser.nounittime",
					lineNumber);
			// seconds = converter.toInteger(time);
		}
		return hours;

	}

	/**
	 * TODO: This function is also defined in process/setUpObservation!
	 * @param targetValues
	 * @param index
	 * @return
	 */
	protected String targetValue(List targetValues, int index)
	{
		if (targetValues.get(index)== null)
		{
			return null;
		}
		else
		{
			return ((ValueBean) targetValues.get(index)).getValue();
		}
	}
	
}