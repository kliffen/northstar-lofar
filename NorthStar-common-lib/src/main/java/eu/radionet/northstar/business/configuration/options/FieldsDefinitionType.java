// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.configuration.options;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

public class FieldsDefinitionType {
	protected Map fields = new HashMap();

	public Map getFields() {
		return fields;
	}

	public void setFields(Map fieldConfigurations) {
		this.fields = fieldConfigurations;
	}

	public void add(FieldsDefinitionType f) {
		
		Iterator iterator = f.getFields().entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry mapEntry = (Map.Entry) iterator.next();
			if(!fields.containsKey(mapEntry.getKey()))
			 fields.put(mapEntry.getKey(), mapEntry.getValue());
		}
		//fields.putAll(f.getFields());
		setFields(fields);
	}
	

	public void remove(FieldsDefinitionType subfields) {
		Iterator iter = subfields.getFields().entrySet().iterator();
		while (iter.hasNext()) {
		    Map.Entry entry = (Entry) iter.next();
		    fields.remove(entry.getKey());
		    }
		setFields(fields);
		}
}
