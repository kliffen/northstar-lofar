// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.observingrequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProcessProposalAction;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Target;

public class ProcessObservingRequestAction extends ProcessProposalAction {
	protected Log log = LogFactory.getLog(this.getClass());
	protected ContextType contextConfiguration = null;
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ObservingRequestForm observingRequestForm = (ObservingRequestForm) form;
		HttpSession session = request.getSession();
		ActionMessages errors = new ActionMessages();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}

		/*
		 * if pressed on Discard changes
		 */
		if (isCancelled(request)) {
			return cancel(mapping,request, ownUserAccount);
		}
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
		contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());

		/*
		 * validate fields that has configurable validation
		 */
		errors = validateConfigurable(observingRequestForm,
				mapping, request);
		if (errors.size() > 0) {
			saveErrors(request, errors);
			return mapping.findForward(Constants.REFRESH);
		}
		proposalData.getProposal().setParentId(AstronConverter.toInteger(observingRequestForm.getSelectedParent()));
		

		ObservingRequest observingRequest = proposalData.getProposal()
				.getObservingRequest();
		fillObservingRequest(request, observingRequest, observingRequestForm);
		
		if (getErrors(request).size() > 0) {
			//saveErrors(request, errors);
			return mapping.findForward(Constants.REFRESH);
		}
		session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
		/*
		 * if forward in form is set, forward to an other pages. (See jsp's for
		 * more info)
		 */
		if (!AstronValidator.isBlankOrNull(observingRequestForm.getForward())) {
			return mapping.findForward(observingRequestForm.getForward());
		}
		ActionForward forward = processGeneralButtons(observingRequestForm,
				mapping, request, errors, proposalData, ownUserAccount);
		if (forward != null) {
			return forward;
		}
		forward = processOtherButtons (observingRequestForm,mapping);
		if (forward != null){
			return forward;
		}
		//
		// check number of observations according to the options file 
		//
    
        boolean maxNumberOfObservationsReached = false;
        ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());
		
		List urlList = new ArrayList();
		String Url = new String();
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_OBSERVATIONS, new HashMap(), contextConfiguration);
		
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue(); 
		
			if (proposalData.getProposal().getObservingRequest().getObservations().size() >= AstronConverter.toInteger(Url).intValue()){
				maxNumberOfObservationsReached = true;
			}
		}
		if (observingRequestForm.getNewObservationButton() != null && !maxNumberOfObservationsReached) {
			if( telescopeConfiguration.isTargetsOnTabbedPage()){
				storeObservation(observingRequestForm,observingRequest,proposalData,true);
				return ParameterAppender.append(mapping.findForward("refresh"),
					"index", "new");
			}else{
				return ParameterAppender.append(mapping.findForward("observation"),
						"index", "new");
			}
		} 
		if (observingRequestForm.getNewPipelineButton() != null) {
			if( telescopeConfiguration.isTargetsOnTabbedPage()){
				storeObservation(observingRequestForm,observingRequest,proposalData,false);
				return ParameterAppender.append(mapping.findForward("refresh"),
					"index", "newPipeline");
			}else{
				return ParameterAppender.append(mapping.findForward("observation"),
						"index", "newpipeline");
			}
		} 
		
		/*
		* if pressend on delete observation / pipeline
		*/
		else if (observingRequestForm.getDeleteObservationButton(0) != null) {
			List targets =  updateTargetCoupling(observingRequest,observingRequestForm.getSelectedObservationId(),-1);
			observingRequest.setTargets(targets);
			
			observingRequest.getObservations().remove(observingRequestForm.getSelectedObservationId());
			observingRequestForm.setSelectedObservation(null);
			session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
			return mapping.findForward("refresh");
		}else if (observingRequestForm.getDeletePipelineButton(0) != null) {
			int observationId = observingRequestForm.getSelectedPipelineId();//getCorrectPipelineId(observingRequestForm.getSelectedObservationId(),observingRequest.getObservations());
			List targets = updateTargetCoupling(observingRequest,-1,observationId);
			observingRequest.setTargets(targets);
			
			observingRequest.getPipelines().remove(observationId);
			observingRequestForm.setSelectedPipeline(null);
			session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
			return mapping.findForward("refresh");
		}  else if (observingRequestForm.getCopyObservationButton(0) != null) {
			Observation observation = (Observation) observingRequest
					.getObservations().get(
							observingRequestForm.getSelectedObservationId());
			try {
				if(telescopeConfiguration.isTargetsOnTabbedPage()){
					// don't copy target, they are referenced from the list.
					boolean includeTargets = false;
					proposalDelegate.copyObservation(proposalData.getProposal(),observation,includeTargets);
				}else{
					// also copy targets..
					boolean includeTargets = true;
					proposalDelegate.copyObservation(proposalData.getProposal(),observation,includeTargets);
				}
				
			} 
			catch (Exception e) {
				log.error(LogMessage.getMessage(ownUserAccount, "Cannot copy observation: " + e.getMessage(), e));
				errors.add(Constants.ERROR, new ActionMessage(
						"error.database.copy.observation"));
				saveErrors(request, errors);
				return mapping.findForward("refresh");
			}
			session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
			return mapping.findForward("refresh");
		}else if (observingRequestForm.getCopyPipelineButton(0) != null) {
			Observation observation = (Observation) observingRequest
					.getPipelines().get(
							observingRequestForm.getSelectedPipelineId());
			try {
				if(telescopeConfiguration.isTargetsOnTabbedPage()){
					// don't copy target, they are referenced from the list.
					boolean includeTargets = false;
					proposalDelegate.copyPipeline(proposalData.getProposal(),observation,includeTargets);
				}
			} 
			catch (Exception e) {
				log.error(LogMessage.getMessage(ownUserAccount, "Cannot copy observation: " + e.getMessage(), e));
				errors.add(Constants.ERROR, new ActionMessage(
						"error.database.copy.observation"));
				saveErrors(request, errors);
				return mapping.findForward("refresh");
			}
			session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
			return mapping.findForward("refresh");
		}
		/*
		 * if pressed on edit observation
		 */
		else if (observingRequestForm.getEditObservationButton(0) != null) {
			// refresh the page if the observation is on the same page or something..
			if( telescopeConfiguration.isTargetsOnTabbedPage() ){
				storeObservation(observingRequestForm,observingRequest,proposalData,true);
				return ParameterAppender.append(mapping.findForward("refresh"),
						"index", observingRequestForm.getSelectedObservationId()
								+ "");
			}else{
				return ParameterAppender.append(mapping.findForward("observation"),
					"index", observingRequestForm.getSelectedObservationId()
							+ "");
			}
		}
		
		else if (observingRequestForm.getEditPipelineButton(0) != null) {
			// refresh the page if the observation is on the same page or something..
			if( telescopeConfiguration.isTargetsOnTabbedPage() ){
				storeObservation(observingRequestForm,observingRequest,proposalData,false);
				return ParameterAppender.append(mapping.findForward("refresh"),
						"index", observingRequestForm.getSelectedPipelineId()
								+ "");
			}else{
				return ParameterAppender.append(mapping.findForward("observation"),
					"index", observingRequestForm.getSelectedPipelineId()
							+ "");
			}
		}
		/*
		 * if pressed on upload observation/target list
		 */
		else if (observingRequestForm.getNewObservationFileButton() != null) {

			return mapping.findForward("observationfile");

		}
		/*
		 * if commit the edited instrument
		 */
		if (observingRequestForm.getCommitObservationButton() != null) {
			storeObservation(observingRequestForm,observingRequest,proposalData,true);
			//observingRequestForm.setSelectedObservation("-1"); //doesn't work
			return ParameterAppender.append(mapping.findForward("refresh"),
					"index", "-1");
		}
		
	/*	if(observingRequestForm.getCancelObservationButton()!=null ){
			//
			if(observingRequest.getObservations().size()<observingRequestForm.getSelectedObservationId())
				observingRequest.getObservations().remove(observingRequestForm.getSelectedObservationId());
			observingRequestForm.setSelectedObservationId(-1);
			observingRequestForm.setSelectedObservation(null);
			return mapping.findForward("cancel");	
		}*/
		/*
		 * if commit the edited instrument
		 */
		if (observingRequestForm.getCommitPipelineButton() != null) {
			storeObservation(observingRequestForm,observingRequest,proposalData,false);
			//observingRequestForm.setSelectedObservation("-1"); //doesn't work
			return ParameterAppender.append(mapping.findForward("refresh"),
					"index", "-1");
		}
		
		/*if(observingRequestForm.getCancelPipelineButton()!=null){
		//	observingRequest.getPipelines().remove(observingRequestForm.getSelectedPipelineId());
			observingRequestForm.setSelectedPipeline(null);
			return mapping.findForward("observingRequest");
		}*/
		/*
		 * if edit on an instrument
		 */
		if( telescopeConfiguration.isTargetsOnTabbedPage() ){
				return ParameterAppender.append(mapping.findForward("refresh"),
						"index", observingRequestForm.getSelectedObservationId()
								+ "");
		}
		
		return mapping.getInputForward();
	}

	protected List updateTargetCoupling(ObservingRequest observingRequest,
			int selectedObservationId,int selectedPipelineId) {
		
		return observingRequest.getTargets();
	}

	protected int getCorrectPipelineId(int selectedObservationId,	List observations) {
		return -1;
	}

	private void storeObservation(ObservingRequestForm observingRequestForm, ObservingRequest observingRequest, ProposalData proposalData, boolean isObservation) {
		// if an observation was open for editing, store it in the database
		String index= observingRequestForm.getSelectedObservation();
		if(!AstronValidator.isBlankOrNull(index)){
			int obsIndex = new Integer(index).intValue();
			// if it was a pipeline, get the correct pipeline id
			/*if(!isObservation ){
				obsIndex = getCorrectPipelineId(obsIndex,observingRequest.getObservations());
			}else{
				obsIndex = getCorrectObservationId(obsIndex,observingRequest.getObservations());
			}
			*/
			Observation observation = null;
			if(isObservation){
				 observation  = (Observation) observingRequest.getObservations().get(obsIndex);
				// TODO fix this later on
				//FIXME test if it is really needed?
				 if(observation.getId() != null){
					// remove from list because it will be added later on
					//observingRequest.getObservations().remove(obsIndex);
					 observation.setObservationKind(null);
					 proposalDelegate.addChangeObservation( proposalData
						.getProposal(), observation, isObservation);
				 }
			}else{
				observation  = (Observation) observingRequest.getPipelines().get(obsIndex);
				if(observation.getId() != null){
					// remove from list because it will be added later on
					//observingRequest.getPipelines().remove(obsIndex);
					observation.setObservationKind("pipeline");
					proposalDelegate.addChangeObservation( proposalData
							.getProposal(), observation, isObservation);
				}
				
			}
			
		}
		
	}
	
	protected int getCorrectObservationId(int obsIndex, List observations) {
		return -1;
	}

	protected void fillObservingRequest(HttpServletRequest request,
			ObservingRequest observingRequest,
			ObservingRequestForm observingRequestForm) throws DatabaseException {
		observingRequest
				.setOverallPreferredSchedConstraints(observingRequestForm
						.getOverallPreferredSchedConstraints());
		observingRequest
				.setOverallRequiredSchedConstraints(observingRequestForm
						.getOverallRequiredSchedConstraints());
	}
	
	protected ActionForward processOtherButtons(ObservingRequestForm observingRequestForm,ActionMapping mapping){
		return null;
	}
	
	protected ActionMessages validateConfigurable(ObservingRequestForm observingRequestForm,
			ActionMapping mapping, HttpServletRequest request) {
		ActionMessages errors = new ActionMessages();
		
		return errors;
	}


}
