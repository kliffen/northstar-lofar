Format reminder:<br/>
<pre class="courier">
Fieldname RA Dec Epoch [Calibrator [Exposure_time [Flux [Ref_freq [Spectral_index [Subband list [Run# [Observation [Pipeline [Comments]]]]]]]]]]
</pre>
Only Fieldname RA Dec Epoch are required, when entering other fields, all fields before it have to be filled in. <br>
<p> An example:
<pre class="courier">
m31,00:48:31.20,+12:09:00.0,j2000,N,11m 
"hd 39801",05:55:10.31,+07:24:25.4,j2000,Y,7200s,1,100,,12-120,1,A,A,on different days
m32,00:42:41.87,+40:51:57.2,j2000
</pre>
</p>

