-- MySQL dump 10.10
--
-- Host: localhost    Database: northstar
-- ------------------------------------------------------
-- Server version	5.0.15-nt-max

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `opticonobservation`
--

DROP TABLE IF EXISTS `opticonobservation`;
CREATE TABLE `opticonobservation` (
  `id` int(11) NOT NULL auto_increment,
  `observationid` int(11) default NULL,
  `telescopeconfiguration` varchar(255) default NULL,
  `instrumentconfiguration` varchar(255) default NULL,
  `time` varchar(255) default NULL,
  `weather` varchar(255) default NULL,
  `mode` varchar(255) default NULL,
  `seeing` varchar(255) default NULL,
  `preferredDates` varchar(255) default NULL,
  `backupstrategy` text default NULL,
  `calibrationrequirements` text default NULL,  
    PRIMARY KEY  (`id`),
  UNIQUE KEY `opticonobservation_UNIQ` (`observationid`),
  CONSTRAINT `observation_opticonobservation_FK` FOREIGN KEY (`observationid`) REFERENCES `observation` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `opticonobservingrequest`
--

DROP TABLE IF EXISTS `opticonobservingrequest`;
CREATE TABLE `opticonobservingrequest` (
  `id` int(11) NOT NULL auto_increment,
  `observingrequestid` int(11) default NULL,
  `long_term_proposal` tinyint(1) NOT NULL default '0',   
  `long_term_proposal_specifics` text default NULL,
  `large_proposal` tinyint(1) NOT NULL default '0',   
  `observing_mode` varchar(100) default NULL,   
  `category` varchar(255) default NULL,  
  `allocation_justification` text default NULL,  
  `new_observer_experience` text default NULL,  
  `travel` text default NULL,  
  `other_expenditure` text default NULL,  
  `data_reduction` tinyint(1) NOT NULL default '0',
  `data_reduction_specifics` text,
  `proprietary_exception` tinyint(1) NOT NULL default '0',   
  `proprietary_exception_specifics` text default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `opticonobservingrequest_UNIQ` (`observingrequestid`),
  CONSTRAINT `observingrequest_opticonobservingrequest_FK` FOREIGN KEY (`observingrequestid`) REFERENCES `observingrequest` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `opticontarget`;
CREATE TABLE `opticontarget` (
  `id` int(11) NOT NULL auto_increment,
  `targetid` int(11) default NULL,
  `moon` varchar(255) default NULL,  
  `seeing` varchar(255) default NULL,  
  `water` varchar(255) default NULL,  
  `sn` double default NULL,  
  `magnitude` double default NULL,  
  `comments` varchar(255) default NULL,  
  `flux` double default NULL,  
  `seeing_upper` double default NULL,  
  `seeing_lower` double default NULL,  
  `diameter` double default NULL, 
  `sky_quality` varchar(255) default NULL,  
  `storage` double default NULL,
  `opportunity` tinyint(1) NOT NULL default 0,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `opticontarget_UNIQ` (`targetid`),
  CONSTRAINT `target_opticontarget_FK` FOREIGN KEY (`targetid`) REFERENCES `target` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `opticoninstrument`;
CREATE TABLE `opticoninstrument` (
  `id` int(11) NOT NULL,
  `name` varchar(255) default NULL,  
  `filter` varchar(255) default NULL, 
  `filter_details` text default NULL,  
  `obs_mode` varchar(255) default NULL,  
  `order_filter` varchar(255) default NULL,  
  `read_out` varchar(255) default NULL,  
  `polarimetry` tinyint(1) NOT NULL default '0',
  `guideline` tinyint(1) NOT NULL default '0',
  `wavelength` varchar(255) default NULL,  
  `ordervalue` int(11) default NULL,
  `slit` varchar(255) default NULL,
  `slit_position_angle`  double  default NULL,   
  `grating` varchar(255) default NULL,  
  `grism` varchar(255) default NULL,    
  `ccd` varchar(255) default NULL,  
  `comments` text default NULL,   
  `non_default_frame_exposuretime` tinyint(1) NOT NULL default '0',   
  `non_default_frame_exposuretime_details` text default NULL,   
  `frame_exposuretime`  double  default NULL, 
  `microstepping` varchar(255) default NULL,  
  `coronagraphic_mask` tinyint(1) NOT NULL default '0', 
  `telescope_scheduling` varchar(255) default NULL,
  `station` varchar(255) default NULL,
  `custom_station_specifics` varchar(255) default NULL,
  `clock` varchar(255) default NULL,
  `antenna` varchar(255) default NULL,
  `integrationtime` varchar(255) default NULL,
  `averaging` tinyint(1) NOT NULL default '0',
  `flagging` tinyint(1) NOT NULL default '0',
  `calibration` tinyint(1) NOT NULL default '0',
  `imaging` tinyint(1) NOT NULL default '0',
  `summary_plots` tinyint(1) NOT NULL default '0',
  `one_filter` varchar(255) default NULL,
  `averaging_frequency` double default NULL,
  `averaging_time` double default NULL,
  `polarization_amount` int(11) default NULL,
  `fieldsize_x` int(11) default NULL,
  `fieldsize_y` int(11) default NULL,
  `pixelsize_x` int(11) default NULL,
  `pixelsize_y` int(11) default NULL,
  `filter_frequency` int(11) default NULL,
  `filter_bandwidth` int(11) default NULL,
  `filter_contiguous_coverage` tinyint(1) NOT NULL default '0',
  `piggyback` tinyint(1) NOT NULL default '0',   
  `piggyback_specifics` text default NULL,
  `direct_datastorage` tinyint(1) NOT NULL default '0', 
  `beams` int(11) default NULL,
  `events` int(11) default NULL,
  `samplerate` int(11) default NULL,
  `frequency_channels` int(11) default NULL,
  `cohstokes` tinyint(1) NOT NULL default '0',
  `polarization` varchar(255) default NULL,
  `incohstokes` tinyint(1) NOT NULL default '0',
  `incpolarization` varchar(255) default NULL,
  `channels` int(11) default NULL,
  `intsteps` int(11)default NULL,
  PRIMARY KEY  (`id`)
) TYPE=InnoDB;

DROP TABLE IF EXISTS `opticoninstrumentfilters`;
CREATE TABLE `opticoninstrumentfilters` (
  `id` int(11) NOT NULL auto_increment,
  `instrumentid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `value` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  INDEX  `instrument_opticonfilters_IND` (`instrumentid`),
  INDEX  `instrument_index_opticonfilters_IND` (`indexid`),
  CONSTRAINT `instrument_opticonfilters_FK` FOREIGN KEY (`instrumentid`) REFERENCES `opticoninstrument` (`id`) ON DELETE CASCADE
) TYPE=InnoDB;

DROP TABLE IF EXISTS `opticoninstrumentfiltersw2`;
CREATE TABLE `opticoninstrumentfiltersw2` (
  `id` int(11) NOT NULL auto_increment,
  `instrumentid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `value` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  INDEX  `instrument_opticonfiltersw2_IND` (`instrumentid`),
  INDEX  `instrument_index_opticonfiltersw2_IND` (`indexid`),
  CONSTRAINT `instrument_opticonfiltersw2_FK` FOREIGN KEY (`instrumentid`) REFERENCES `opticoninstrument` (`id`) ON DELETE CASCADE
) TYPE=InnoDB;

DROP TABLE IF EXISTS `opticoninstrumentgrismw`;
CREATE TABLE `opticoninstrumentgrismw` (
  `id` int(11) NOT NULL auto_increment,
  `instrumentid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `value` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  INDEX  `instrument_opticongrismw_IND` (`instrumentid`),
  INDEX  `instrument_index_opticongrismw_IND` (`indexid`),
  CONSTRAINT `instrument_opticongrismw_FK` FOREIGN KEY (`instrumentid`) REFERENCES `opticoninstrument` (`id`) ON DELETE CASCADE
) TYPE=InnoDB;

DROP TABLE IF EXISTS `opticoninstrumentslitw`;
CREATE TABLE `opticoninstrumentslitw` (
  `id` int(11) NOT NULL auto_increment,
  `instrumentid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `value` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  INDEX  `instrument_opticonslitw_IND` (`instrumentid`),
  INDEX  `instrument_index_opticonslitw_IND` (`indexid`),
  CONSTRAINT `instrument_opticonslitw_FK` FOREIGN KEY (`instrumentid`) REFERENCES `opticoninstrument` (`id`) ON DELETE CASCADE
) TYPE=InnoDB;

DROP TABLE IF EXISTS `opticoninstrumentgratingw`;
CREATE TABLE `opticoninstrumentgratingw` (
  `id` int(11) NOT NULL auto_increment,
  `instrumentid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `value` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  INDEX  `instrument_opticongratingw_IND` (`instrumentid`),
  INDEX  `instrument_index_opticongratingw_IND` (`indexid`),
  CONSTRAINT `instrument_opticongratingw_FK` FOREIGN KEY (`instrumentid`) REFERENCES `opticoninstrument` (`id`) ON DELETE CASCADE
) TYPE=InnoDB;

DROP TABLE IF EXISTS `targets_per_observation`;
CREATE TABLE  `targets_per_observation` (
  `targetid` int(11) NOT NULL,
  `observationid` int(11) NOT NULL,
  `indexid` int(11) NOT NULL,
  PRIMARY KEY  (`targetid`,`observationid`)
) TYPE=InnoDB;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;




