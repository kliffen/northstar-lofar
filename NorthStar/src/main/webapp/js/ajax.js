function calculateSubbandList(myform){
	var frequency = myform.targetCentralFrequency.value;
	var bandwidth = myform.targetBandwidth.value;
	
	var xmlhttp;
	if (window.XMLHttpRequest)
	  {// code for IE7+, Firefox, Chrome, Opera, Safari
	  xmlhttp=new XMLHttpRequest();
	  }
	xmlhttp.onreadystatechange=function()
	  {
	  if (xmlhttp.readyState==4 && xmlhttp.status==200)
	    {
	    //document.getElementsByName("textsubbandResult").value=xmlhttp.responseText;
		  var targetsubband = document.getElementsByName("targetSubbandList")[0];
		  targetsubband.value=xmlhttp.responseText;
		  }
	  }
	xmlhttp.open("GET","calculateSubbandList.do?frequency=" + frequency+ "&bandwidth="+bandwidth ,true);
	xmlhttp.send();
}

function showPopup(myform){
	 
	 var ispb = document.getElementsByName("enablePiggyBack")[0];
	 	
     if(ispb.checked ){
    	 
    	 $( "#dialog-confirm" ).dialog({
    	      modal: true,
      	      buttons: {
    	        "Yes": function() {
    	          $( this ).dialog( "close" );
    	          myform.submit();
    	        },
    	        "No": function() {
    	         ispb.checked =false;
    	          $( this ).dialog( "close" );
    	          myform.submit();
    	        }
    	      }
    	    });
    	 
                 $("#dialog-confirm").dialog('open');
                 return false;
             }
     else{
    	 myform.submit();
     }
}

function showPopupForIntegrationTime(myform, integrationTime){
	var ispb = 1;
    if(integrationTime <= 0){
	 $( "#dialog-confirm-integration" ).dialog({
	      modal: true,
	 	      buttons: {
	        "Yes": function() {
	          $( this ).dialog( "close" );
	          $('#processObservingReq').append('<input type="hidden" title="commit observation" class="list_newobs" id="commitObservationButton" value="Commit Observation specification" name="commitObservationButton">');
	          myform.submit();
	        },
	        "No": function() {
	          $( this ).dialog( "close" );
	        }
	      }
	  });
	  $("#dialog-confirm-integration").dialog('open');
	  return false;
    }else if(integrationTime > 2){
    	$( "#dialog-confirm-integration" ).dialog({
     	      modal: true,
       	      buttons: {
     	        "Yes": function() {
     	          $( this ).dialog( "close" );
     	          $('#processObservingReq').append('<input type="hidden" title="commit observation" class="list_newobs" id="commitObservationButton" value="Commit Observation specification" name="commitObservationButton">');
     	          myform.submit();
     	        },
     	        "No": function() {
     	          $( this ).dialog( "close" );
     	        }
     	      }
     	    });
          $("#dialog-confirm-integration").dialog('open');
          return false;
    }else{
    	$('#processObservingReq').append('<input type="hidden" title="commit observation" class="list_newobs" id="commitObservationButton" value="Commit Observation specification" name="commitObservationButton">');
    	myform.submit();
    	return false;
    }
}


function validateObservingRuns(myform){
    var runs = myform.selectedRunnumber;
    var runlabel = runs.options[runs.selectedIndex].innerHTML;
    var etimebox = document.getElementsByName("targetExposureTime")[0];
    var obselect = document.getElementsByName("selectedObservingRun")[0];
    var isPrimary = document.getElementsByName("targetFirstInRun")[0];

    if (runlabel.length > 4){
            etimebox.disabled = false;
            obselect.disabled = false;
            isPrimary.value = true;
    }else{
            etimebox.value="";
            etimebox.disabled = true;
            obselect.options[0].selected = true;
            obselect.disabled = true;
            isPrimary.value = false;
    }


}

function showPopupForDeleteAllConfirm(myform){
	 $( "#dialog-confirm-delete-all-targets" ).dialog({
	      modal: true,
	 	      buttons: {
	        "Yes": function() {
	          $( this ).dialog( "close" );
	          myform.submit();
	        },
	        "No": function() {
	          $( this ).dialog( "close" );
	        }
	      }
	  });
	  $("#dialog-confirm-integration").dialog('open');
	  return false;
}

