// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProposalWriter.java 
 *
 * Created on Mar 7, 2005
 *
 * Version $Id: DocumentWriter.java,v 1.6 2008-07-21 09:21:04 boelen Exp $
 *
 */
package eu.radionet.northstar.business.pdf;

import java.awt.Color;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import nl.astron.util.AstronValidator;

import com.lowagie.text.Chunk;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;

/**
 * The ProposalWriter provides general pdf support
 * 
 * @author Bastiaan Verhoef
 * 
 */
public abstract class DocumentWriter {

	public static final Font TITLE_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 12, Font.ITALIC);

	public static final Font TITLE_VALUE_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 12, Font.NORMAL);

	public static final Font TITLE_VALUE_REQUIRED_FONT = FontFactory
			.getFont(FontFactory.HELVETICA, 12, Font.BOLD, Color.RED);

	public static final Font OTHER_SUB_TITLE_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 10, Font.ITALIC);

	public static final Font DEFAULT_SUB_TITLE_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 10, Font.BOLDITALIC);

	public static final Font PROPOSAL_TITLE_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 10, Font.BOLD);

	public static final Font NORMAL_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 10, Font.NORMAL);

	public static final Font REQUESTED_TIME_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 14, Font.BOLD);

	public static final Font REQUIRED_REQUESTED_TIME_FONT = FontFactory
			.getFont(FontFactory.HELVETICA, 14, Font.BOLD, Color.RED);

	public static final Font REQUESTED_TIME_TABLE_HEADER_FONT = FontFactory
			.getFont(FontFactory.HELVETICA, 12, Font.BOLD, Color.WHITE);

	public static final Font REQUESTED_TIME_TABLE_CONTENT_FONT = FontFactory
			.getFont(FontFactory.HELVETICA, 12, Font.BOLD);

	public static final Font REQUESTED_TIME_TABLE_CONTENT__REQUIRED_FONT = FontFactory
			.getFont(FontFactory.HELVETICA, 10, Font.BOLD, Color.RED);

	public static final Font REQUIRED_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 10, Font.BOLD, Color.RED);

	public static final Font TABLE_HEADER_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 8, Font.BOLD, Color.WHITE);

	public static final Font TABLE_INNER_HEADER_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 8, Font.BOLDITALIC);

	public static final Font TABLE_CONTENT_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 8, Font.NORMAL);

	public static final Font TABLE_CONTENT_TITLE_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 8, Font.BOLDITALIC);

	public static final Font TABLE_CONTENT_REQUIRED_FONT = FontFactory
			.getFont(FontFactory.HELVETICA, 8, Font.BOLD, Color.RED);

	public static final Font FOOTER = FontFactory.getFont(
			FontFactory.HELVETICA, 8, Font.ITALIC);

	public static final Font FOOTER_REQUIRED = FontFactory.getFont(
			FontFactory.HELVETICA, 8, Font.ITALIC, Color.RED);

	public static final Font HEADER_NORMAL_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 14, Font.BOLD);

	public static final Font HEADER_BIG_ITALIC_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 20, Font.BOLDITALIC);

	public static final Font HEADER_BIG_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 20, Font.BOLD);

	public static final Font HEADER_SMALL_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 10, Font.BOLD);

	public static final Font HEADER_SMALL_REQUIRED_FONT = FontFactory
			.getFont(FontFactory.HELVETICA, 10, Font.BOLD, Color.RED);

	public static final Font HEADER_REQUIRED_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 14, Font.BOLD, Color.RED);

	public static final Font TEXT_JUSTIFICATION_FONT = FontFactory.getFont(
			FontFactory.HELVETICA, 11, Font.NORMAL);

	public static final String NOT_SPECIFIED = "NOT SPECIFIED";

	public static final String EMPTY = "EMPTY";

	public static final String NONE = "-- NONE --";

	public static final String EMPTY_NOT_REQUIRED = "";

	public static final float NORMAL_PARAGRAPH_INDENTATION_LEFT = 15;

	public static final float NORMAL_PARAGRAPH_INDENTATION_RIGHT = 7;

	public static final float HEADER_TITLE_MAX_LENGTH = 450f;

	public static final float LEFT_MARGIN = 56;

	public static final float RIGHT_MARGIN = 56;

	public static final float TOP_MARGIN = 70;

	public static final float BOTTOM_MARGIN = 56;

	public static final float FOOTER_POSITION = 56;

	public static final float HEADER_SIZE = 30;

	public static final float FIRST_HEADER_SIZE = 150;


	/**
	 * The getBorderlessCell method generates cell without border
	 * 
	 * @return
	 */
	protected PdfPCell getBorderlessCell() {
		PdfPCell cell = new PdfPCell();
		cell.setBorder(0);
		return cell;
	}

	/**
	 * The getTableCell method generates cell with border
	 * 
	 * @return
	 */
	protected PdfPCell getTableCell() {
		PdfPCell cell = new PdfPCell();
		return cell;
	}

	/**
	 * The getTableHeaderCell method generates cell with table header layout
	 * 
	 * @param text
	 * @return
	 */
	protected PdfPCell getTableHeaderCell(String text) {
		PdfPCell cell = new PdfPCell();
		cell.setPhrase(new Phrase(text, TABLE_HEADER_FONT));
		cell.setBackgroundColor(Color.DARK_GRAY);
		return cell;
	}

	protected PdfPCell getRequestedTimeTableHeaderCell(String text) {
		PdfPCell cell = new PdfPCell();
		cell.setPhrase(new Phrase(text, REQUESTED_TIME_TABLE_HEADER_FONT));
		cell.setBackgroundColor(Color.DARK_GRAY);
		return cell;
	}

	protected PdfPCell getTableInnerHeaderCell(String text) {
		PdfPCell cell = new PdfPCell();
		cell.setPhrase(new Phrase(text, TABLE_INNER_HEADER_FONT));
		cell.setBackgroundColor(Color.LIGHT_GRAY);
		return cell;
	}

	/**
	 * The getTableRequiredTextCell method generates tablecell with required
	 * text
	 * 
	 * @param text
	 * @return
	 */
	protected PdfPCell getTableRequiredTextCell(String text) {
		return getTableRequiredTextCell(text, true);

	}

	/**
	 * The getTableRequiredTextCell method generates tablecell with required
	 * text
	 * 
	 * @param text
	 * @param border
	 * @return
	 */
	protected PdfPCell getTableRequiredTextCell(String text, boolean border) {
		PdfPCell cell = null;
		if (border) {
			cell = this.getTableCell();
		} else {
			cell = this.getBorderlessCell();
		}

		if (!AstronValidator.isBlankOrNull(text)) {
			cell.setPhrase(new Phrase(text, TABLE_CONTENT_FONT));
		} else {
			cell.setPhrase(new Phrase(EMPTY, TABLE_CONTENT_REQUIRED_FONT));
		}
		return cell;
	}

	/**
	 * The getTableTextCell method generates tablecell with text
	 * 
	 * @param text
	 * @return
	 */
	protected PdfPCell getTableTextCell(String text, boolean border) {
		PdfPCell cell = null;
		if (border) {
			cell = this.getTableCell();
		} else {
			cell = this.getBorderlessCell();
		}
		if (!AstronValidator.isBlankOrNull(text)) {
			cell.setPhrase(new Phrase(text, TABLE_CONTENT_FONT));
		} else {
			cell.setPhrase(new Phrase(EMPTY_NOT_REQUIRED, TABLE_CONTENT_FONT));
		}
		return cell;
	}

	/**
	 * The getTableTitleTextCell generates tablecell with text
	 * 
	 * @param text
	 * @param border
	 * @return
	 */
	protected PdfPCell getTableTitleTextCell(String text, boolean border) {
		PdfPCell cell = null;
		if (border) {
			cell = this.getTableCell();
		} else {
			cell = this.getBorderlessCell();
		}
		if (!AstronValidator.isBlankOrNull(text)) {
			cell.setPhrase(new Phrase(text, TABLE_CONTENT_TITLE_FONT));
		} else {
			cell.setPhrase(new Phrase(EMPTY_NOT_REQUIRED, TABLE_CONTENT_FONT));
		}
		return cell;
	}

	/**
	 * The getTableTextCell method geneates tablecell with text
	 * 
	 * @param text
	 * @return
	 */
	protected PdfPCell getTableTextCell(String text) {
		return getTableTextCell(text, true);

	}

	/**
	 * The getTableTitleTextCell method geneates tablecell with title text
	 * 
	 * @param text
	 * @return
	 */
	protected PdfPCell getTableTitleTextCell(String text) {
		return getTableTitleTextCell(text, true);
	}

	/**
	 * The getTableRequiredNumbersCell method generates tablecell with required
	 * numbers
	 * 
	 * @param numbers
	 * @return
	 */
	protected PdfPCell getTableRequiredNumbersCell(String numbers) {
		return getTableRequiredNumbersCell(numbers, true);
	}

	protected PdfPCell getTableRequiredNumbersCell(String numbers,
			boolean border) {
		PdfPCell cell = null;
		if (border) {
			cell = this.getTableCell();
		} else {
			cell = this.getBorderlessCell();
		}
		if (!AstronValidator.isBlankOrNull(numbers)) {
			cell.setPhrase(new Phrase(numbers, TABLE_CONTENT_FONT));
		} else {
			cell.setPhrase(new Phrase(NOT_SPECIFIED,
					TABLE_CONTENT_REQUIRED_FONT));
		}
		return cell;
	}

	protected PdfPCell getRequestedTimeTableRequiredNumbersCell(String numbers) {
		return getRequestedTimeTableRequiredNumbersCell(numbers, true);
	}

	protected PdfPCell getRequestedTimeTableRequiredNumbersCell(String numbers,
			boolean border) {
		PdfPCell cell = null;
		if (border) {
			cell = this.getTableCell();
		} else {
			cell = this.getBorderlessCell();
		}
		if (!AstronValidator.isBlankOrNull(numbers)) {
			cell.setPhrase(new Phrase(numbers,
					REQUESTED_TIME_TABLE_CONTENT_FONT));
		} else {
			cell.setPhrase(new Phrase(NOT_SPECIFIED,
					REQUESTED_TIME_TABLE_CONTENT__REQUIRED_FONT));
		}
		return cell;
	}

	/**
	 * The getTableNumbersCell method generates tablecell with numbers
	 * 
	 * @param numbers
	 * @return
	 */
	protected PdfPCell getTableNumbersCell(String numbers) {
		PdfPCell cell = new PdfPCell();
		if (!AstronValidator.isBlankOrNull(numbers)) {
			cell.setPhrase(new Phrase(numbers, TABLE_CONTENT_FONT));
		} else {
			cell.setPhrase(new Phrase(EMPTY_NOT_REQUIRED, TABLE_CONTENT_FONT));
		}
		return cell;
	}

	protected PdfPCell getRequestedTimeTableNumbersCell(String numbers) {
		PdfPCell cell = new PdfPCell();
		if (!AstronValidator.isBlankOrNull(numbers)) {
			cell.setPhrase(new Phrase(numbers,
					REQUESTED_TIME_TABLE_CONTENT_FONT));
		} else {
			cell.setPhrase(new Phrase(EMPTY_NOT_REQUIRED,
					REQUESTED_TIME_TABLE_CONTENT_FONT));
		}
		return cell;
	}

	/**
	 * The getDefaultTable method generates default table
	 * 
	 * @param cells
	 * @return
	 */
	protected PdfPTable getDefaultTable(int cells) {
		PdfPTable table = new PdfPTable(cells);
		table.setWidthPercentage(100);
		table.setSplitLate(true);
		table.setSplitRows(true);
		return table;
	}

	protected PdfPTable getDirectSplitTable(int cells) {
		PdfPTable table = new PdfPTable(cells);
		table.setWidthPercentage(100);
		table.setSplitLate(false);
		table.setSplitRows(true);
		return table;
	}

	/**
	 * The getTableNoSplit
	 * 
	 * @param cells
	 * @return
	 */
	protected PdfPTable getTableNoSplit(int cells) {
		PdfPTable table = new PdfPTable(cells);
		table.setWidthPercentage(100);
		return table;
	}

	/**
	 * The getParagraphDefaultTitle method generates paragraph with default
	 * title layout
	 * 
	 * @param text
	 * @return
	 */
	protected Paragraph getParagraphDefaultTitle(String text) {
		Paragraph title = new Paragraph(text, TITLE_FONT);
		title.setSpacingAfter(2);
		return title;
	}

	protected Paragraph getParagraphDefaultTitleWithValue(String title,
			String value, String error) {
		Paragraph titleParagraph = new Paragraph(title, TITLE_FONT);
		Chunk titleValue = null;
		if (!AstronValidator.isBlankOrNull(value)) {

			titleValue = new Chunk(value, TITLE_VALUE_FONT);
		} else {
			titleValue = new Chunk(error, TITLE_VALUE_REQUIRED_FONT);
		}
		titleParagraph.add(titleValue);
		titleParagraph.setSpacingAfter(2);
		return titleParagraph;
	}

	protected Paragraph getParagraphDefaultTitleWithValues(String title,
			List values, String separator, String error) {
		Paragraph titleParagraph = new Paragraph(title, TITLE_FONT);
		Chunk titleValue = null;
		if (values.size() > 0) {
			Iterator iterator = values.iterator();
			while (iterator.hasNext()) {
				String value = (String) iterator.next();
				if (!AstronValidator.isBlankOrNull(value)) {
					if (iterator.hasNext()){
						value=value+separator;
					}
					titleValue = new Chunk(value, TITLE_VALUE_FONT);
				} else {
					if (iterator.hasNext()){
						error=error+separator;
					}
					titleValue = new Chunk(error, TITLE_VALUE_REQUIRED_FONT);
				}
				titleParagraph.add(titleValue);
			}
		} else {
			titleValue = new Chunk(error, TITLE_VALUE_REQUIRED_FONT);
			titleParagraph.add(titleValue);
		}
		titleParagraph.setSpacingAfter(2);
		return titleParagraph;
	}

	protected Paragraph getSpacingParagraph(float spacing) {
		Paragraph spacingParagraph = new Paragraph("", TITLE_FONT);
		spacingParagraph.setSpacingAfter(spacing);
		return spacingParagraph;
	}

	protected void addParagraphDefaultTitleWithSubTitle(String titleText,
			String subTitleText, PdfPCell cell) {
		Paragraph title = new Paragraph(titleText, TITLE_FONT);
		cell.addElement(title);
		Paragraph subTitle = new Paragraph(subTitleText, OTHER_SUB_TITLE_FONT);
		cell.addElement(subTitle);
		subTitle.setSpacingAfter(2);

	}

	/**
	 * The getParagraphDefaultSubTitle method generates paragraph with default
	 * subtitle layout
	 * 
	 * @param text
	 * @return
	 */
	protected Paragraph getParagraphDefaultSubTitle(String text) {
		Paragraph title = new Paragraph(text, DEFAULT_SUB_TITLE_FONT);
		title.setSpacingAfter(1);
		return title;
	}

	/**
	 * The getParagraphNormal method provides generates paragraph with default
	 * normal layout
	 * 
	 * @param text
	 * @param error
	 * @return
	 */
	protected Paragraph getParagraphNormal(String text, String error) {
		Paragraph myParagraph = getParagraph(text, error, NORMAL_FONT, REQUIRED_FONT,
				NORMAL_PARAGRAPH_INDENTATION_LEFT,
				NORMAL_PARAGRAPH_INDENTATION_RIGHT);
		return myParagraph;
	}
	
	protected Paragraph getParagraphNormal(List texts, String separator, String error) {
		return getParagraph(texts,separator, error, NORMAL_FONT, REQUIRED_FONT,
				NORMAL_PARAGRAPH_INDENTATION_LEFT,
				NORMAL_PARAGRAPH_INDENTATION_RIGHT);
	}

	protected Paragraph getParagraphRequestedTime(String text, String error) {
		return getParagraph(text, error, REQUESTED_TIME_FONT,
				REQUIRED_REQUESTED_TIME_FONT,
				NORMAL_PARAGRAPH_INDENTATION_LEFT,
				NORMAL_PARAGRAPH_INDENTATION_RIGHT);
	}

	protected Paragraph getParagraph(String text, String error,
			Font normalFont, Font errorFont, float indentationLeft,
			float indentationRight, int alignment) {
		Paragraph result = null;
		if (!AstronValidator.isBlankOrNull(text)) {

			result = new Paragraph(text, normalFont);
		} else {
			result = new Paragraph(error, errorFont);
		}
		result.setIndentationLeft(indentationLeft);
		result.setIndentationRight(indentationRight);
		result.setAlignment(alignment);
		return result;
	}
	protected Paragraph getParagraph(List texts, String separator, String error,
			Font normalFont, Font errorFont, float indentationLeft,
			float indentationRight, int alignment) {
		Paragraph result = new Paragraph();
		Chunk contentValue = null;
		if (texts.size() > 0) {
			Iterator iterator = texts.iterator();
			while (iterator.hasNext()) {
				String value = (String) iterator.next();
				if (!AstronValidator.isBlankOrNull(value)) {
					if (iterator.hasNext()){
						value=value+separator;
					}
					contentValue = new Chunk(value, normalFont);
				} else {
					if (iterator.hasNext()){
						error=error+separator;
					}
					contentValue = new Chunk(error, errorFont);
				}
				result.add(contentValue);
			}
		} else {
			contentValue = new Chunk(error, errorFont);
			result.add(contentValue);
		}
		result.setIndentationLeft(indentationLeft);
		result.setIndentationRight(indentationRight);
		result.setAlignment(alignment);
		return result;
	}	

	protected Chunk getChunk(String text, String error, Font normalFont,
			Font errorFont) {
		Chunk result = null;
		if (!AstronValidator.isBlankOrNull(text)) {

			result = new Chunk(text, normalFont);
		} else {
			result = new Chunk(error, errorFont);
		}
		return result;
	}

	protected Paragraph getParagraph(String text, String error,
			Font normalFont, Font errorFont, float indentationLeft,
			float indentationRight) {
		return getParagraph(text, error, normalFont, errorFont,
				indentationLeft, indentationRight, Paragraph.ALIGN_LEFT);

	}
	protected Paragraph getParagraph(List texts, String separator, String error,
			Font normalFont, Font errorFont, float indentationLeft,
			float indentationRight) {
		return getParagraph(texts, separator, error, normalFont, errorFont,
				indentationLeft, indentationRight, Paragraph.ALIGN_LEFT);

	}
	/**
	 * The getParagraphCellContentTitle method generates paragraph with default
	 * celltitle layout
	 * 
	 * @param text
	 * @return
	 */
	protected Paragraph getParagraphCellContentTitle(String text) {
		Paragraph title = new Paragraph(text, TABLE_CONTENT_TITLE_FONT);
		title.setSpacingAfter(2);
		return title;
	}

	protected Paragraph getParagraphCellContentTitleWithValue(String title,
			String value, String error) {
		Paragraph titleParagraph = new Paragraph(title,
				TABLE_CONTENT_TITLE_FONT);
		Chunk titleValue = null;
		if (!AstronValidator.isBlankOrNull(value)) {

			titleValue = new Chunk(value, TABLE_CONTENT_FONT);
		} else {
			titleValue = new Chunk(error, TABLE_CONTENT_REQUIRED_FONT);
		}
		titleParagraph.add(titleValue);
		titleParagraph.setSpacingAfter(2);
		return titleParagraph;
	}

	/**
	 * The getParagraphCellContentNormal method generates paragraph with default
	 * cellnormal layout
	 * 
	 * @param text
	 * @param error
	 * @return
	 */
	protected Paragraph getParagraphCellContentNormal(String text, String error) {

		if (!AstronValidator.isBlankOrNull(text)) {

			return new Paragraph(text, TABLE_CONTENT_FONT);
		} else {
			return new Paragraph(error, TABLE_CONTENT_REQUIRED_FONT);
		}
	}

	public static Integer getPdfSize(byte[] pdf) throws IOException {
		try{
			PdfReader pdfReader = new PdfReader(pdf);
			return new Integer(pdfReader.getNumberOfPages());
		}catch(Exception e){
			return new Integer(-1);
		}
		
	}

}