// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.email;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.TimeZone;

import javax.naming.NamingException;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.exception.AstronMailException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.radionet.northstar.business.configuration.EmailConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.data.Constants;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalStatus;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.admin.StatusEmail;

public abstract class TelescopeEmailGenerator extends EmailGenerator {
	private Log log = LogFactory.getLog(this.getClass());

	protected static final String PROPOSAL_CODE = "%proposalcode%";

	protected static final String NAME = "%name%";

	protected static final String DATE = "%date%";

	private static final String CONTACT_AUTHOR = "%contactauthor%";

	protected static final String COMMITTEE = "%committee%";

	protected static final String TELESCOPE = "%telescope%";

	protected static final String APPLICANTS = "%applicants%";

	protected static final String PROPOSAL_TITLE = "%proposaltitle%";

	protected static final String PROPOSAL_CATEGORY = "%proposalcategory%";

	private static String RETRACTED = "retracted";

	private static String SUBMITTED = "submitted";

	private static String IN_PREPARATION = "in_preparation";

	private static String UNDER_REVIEW = "under_review";

	private static String ACCEPTED = "accepted";

	private static String REJECTED = "rejected";

	public abstract StatusEmail sendSubmittedEmail(
			TelescopeConfiguration telescopeConfiguration, Proposal proposal,
			UserAccount userAccount) throws AstronMailException, NamingException;

	public abstract StatusEmail sendRetractedEmail(
			TelescopeConfiguration telescopeConfiguration, Proposal proposal,
			UserAccount userAccount) throws AstronMailException, NamingException;

	public StatusEmail sendStatusChangeEmail(
			TelescopeConfiguration telescopeConfiguration, Proposal proposal,
			String subject, String message, UserAccount userAccount)
			throws AstronMailException, NamingException {
		EmailConfiguration emailConfiguration = null;
		int statusId = proposal.getCurrentStatus().getStatus().getId()
				.intValue();
		if (statusId == Constants.IN_PREPARATION) {
			emailConfiguration = this.getInPreparationEmailConfiguration(
					proposal, telescopeConfiguration);
		} else if (statusId == Constants.SUBMITTED) {
			emailConfiguration = this.getSubmittedEmailConfiguration(proposal,
					telescopeConfiguration);
		} else if (statusId == Constants.UNDER_REVIEW) {
			emailConfiguration = this.getUnderReviewEmailConfiguration(
					proposal, telescopeConfiguration);
		} else if (statusId == Constants.ACCEPTED) {
			emailConfiguration = this.getAcceptedEmailConfiguration(proposal,
					telescopeConfiguration);
		} else if (statusId == Constants.REJECTED) {
			emailConfiguration = this.getAcceptedEmailConfiguration(proposal,
					telescopeConfiguration);
		}
		StatusEmail statusEmail = super.sendStatusChangeEmail(proposal,
				emailConfiguration, subject, message);
		RegisteredMember contactAuthor = proposal.getContactAuthor();
		log.info("Username: " + userAccount.getUsername()
				+ " - Send status change mail for proposal "
				+ proposal.getCode() + " with id: " + proposal.getId()
				+ " send to : " + getEmailName(contactAuthor.getUser()) + "("
				+ contactAuthor.getEmail() + ")");
		return statusEmail;
	}

	protected StatusEmail sendSubmittedEmail(
			TelescopeConfiguration telescopeConfiguration,
			EmailConfiguration emailConfiguration,
			EmailConfiguration emailConfigurationOfOtherMembers,
			String message, String messageToOthers, Proposal proposal,
			UserAccount userAccount) throws AstronMailException, NamingException {

		message = message.replaceAll(NAME, getEmailName(userAccount.getUser()));
		message = message.replaceAll(PROPOSAL_CODE, proposal.getCode());

		ProposalStatus proposalStatus = proposal.getCurrentStatus();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm z");
		Calendar UTCCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		UTCCal.setTime(proposalStatus.getStatusTime());
		simpleDateFormat.setCalendar(UTCCal);

		message = message.replaceAll(DATE, simpleDateFormat.format(UTCCal
				.getTime()));
		message = message.replaceAll(TELESCOPE, proposal.getSemester()
				.getTelescope());
		message = message.replaceAll(PROPOSAL_CATEGORY, proposal.getSemester()
				.getCategory().getCode());
		message = message.replaceAll(COMMITTEE, telescopeConfiguration
				.getCommittee());
		message = message.replaceAll(PROPOSAL_TITLE, proposal
				.getJustification().getTitle());

		StatusEmail statusEmail = super.sendGeneratedStatusChangeEmail(proposal,
				emailConfiguration, message);
		log.info("Send submission mail for proposal " + proposal.getCode()
				+ " with id: " + proposal.getId() + " send to : "
				+ getEmailName(userAccount.getUser()) + "("
				+ userAccount.getUsername() + " - "
				+ userAccount.getUser().getEmail() + ")");

		sendSubmittedEmailForOtherMembers(telescopeConfiguration,
				emailConfigurationOfOtherMembers, messageToOthers, proposal);
		return statusEmail;

	}

	protected StatusEmail sendRetractedEmail(
			TelescopeConfiguration telescopeConfiguration,
			EmailConfiguration emailConfiguration,
			EmailConfiguration emailConfigurationOfOtherMembers,
			String message, String messageToOthers, Proposal proposal,
			UserAccount userAccount) throws AstronMailException, NamingException {
		message = message.replaceAll(NAME, getEmailName(userAccount.getUser()));
		message = message.replaceAll(PROPOSAL_CODE, proposal.getCode());

		ProposalStatus proposalStatus = proposal.getCurrentStatus();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm z");
		Calendar UTCCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		UTCCal.setTime(proposalStatus.getStatusTime());
		simpleDateFormat.setCalendar(UTCCal);

		message = message.replaceAll(DATE, simpleDateFormat.format(UTCCal
				.getTime()));
		message = message.replaceAll(TELESCOPE, proposal.getSemester()
				.getTelescope());
		message = message.replaceAll(COMMITTEE, telescopeConfiguration
				.getCommittee());
		message = message.replaceAll(PROPOSAL_TITLE, proposal
				.getJustification().getTitle());
		StatusEmail statusEmail = super.sendGeneratedStatusChangeEmail(proposal,
				emailConfiguration, message);
		log.info("Send retraction mail for proposal " + proposal.getCode()
				+ " with id: " + proposal.getId() + " send to : "
				+ getEmailName(userAccount.getUser()) + "("
				+ userAccount.getUsername() + " - "
				+ userAccount.getUser().getEmail() + ")");

		sendRetractedEmailForOtherMembers(telescopeConfiguration,
				emailConfigurationOfOtherMembers, messageToOthers, proposal);
		return statusEmail;

	}

	protected void sendSubmittedEmailForOtherMembers(
			TelescopeConfiguration telescopeConfiguration,
			EmailConfiguration emailConfiguration, String messageToOthers,
			Proposal proposal) throws AstronMailException, NamingException {
		String resultMessageToOthers = getSubmittedEmailForOtherMembers(
				telescopeConfiguration, emailConfiguration, messageToOthers,
				proposal);
		Iterator membersIterator = proposal.getMembers().iterator();
		while (membersIterator.hasNext()) {
			Member member = (Member) membersIterator.next();
			if (!member.isContactAuthor()
					&& member.getClass().equals(RegisteredMember.class)) {
				sendEmailForOtherMember(telescopeConfiguration,
						emailConfiguration, resultMessageToOthers, member);
			}
		}
	}

	protected void sendRetractedEmailForOtherMembers(
			TelescopeConfiguration telescopeConfiguration,
			EmailConfiguration emailConfiguration, String messageToOthers,
			Proposal proposal) throws AstronMailException, NamingException {
		String resultMessageToOthers = getRetractedEmailForOtherMembers(
				telescopeConfiguration, emailConfiguration, messageToOthers,
				proposal);
		Iterator membersIterator = proposal.getMembers().iterator();
		while (membersIterator.hasNext()) {
			Member member = (Member) membersIterator.next();
			if (!member.isContactAuthor()) {
				sendEmailForOtherMember(telescopeConfiguration,
						emailConfiguration, resultMessageToOthers, member);
			}
		}
	}

	protected String getSubmittedEmailForOtherMembers(
			TelescopeConfiguration telescopeConfiguration,
			EmailConfiguration emailConfiguration, String message,
			Proposal proposal) {
		String result = message.replaceAll(PROPOSAL_CODE, proposal.getCode());

		ProposalStatus proposalStatus = proposal.getCurrentStatus();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm z");
		Calendar UTCCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		UTCCal.setTime(proposalStatus.getStatusTime());
		simpleDateFormat.setCalendar(UTCCal);

		result = result.replaceAll(DATE, simpleDateFormat.format(UTCCal
				.getTime()));
		result = result.replaceAll(TELESCOPE, proposal.getSemester()
				.getTelescope());
		result = result.replaceAll(COMMITTEE, telescopeConfiguration
				.getCommittee());
		result = result.replaceAll(PROPOSAL_TITLE, proposal.getJustification()
				.getTitle());
		/*
		 * String applicants = null; Iterator membersIterator =
		 * proposal.getMembers().iterator(); while (membersIterator.hasNext()){
		 * Member member = (Member) membersIterator.next(); if (applicants !=
		 * null){ applicants += "\n"; }else { applicants = ""; } applicants +=
		 * member.getName(); if (member.isPi() || member.isContactAuthor()){
		 * applicants += "("; if (member.isPi()){ applicants += "PI"; if
		 * (member.isContactAuthor()){ applicants += ", Contact Author"; } }else
		 * if (member.isContactAuthor()){ applicants += "Contact Author"; }
		 * applicants += ")"; } } result = result.replaceAll(APPLICANTS,
		 * applicants);
		 */
		return result;
	}

	protected String getRetractedEmailForOtherMembers(
			TelescopeConfiguration telescopeConfiguration,
			EmailConfiguration emailConfiguration, String messageToOthers,
			Proposal proposal) {
		String result = messageToOthers.replaceAll(PROPOSAL_CODE, proposal
				.getCode());

		ProposalStatus proposalStatus = proposal.getCurrentStatus();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm z");
		Calendar UTCCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		UTCCal.setTime(proposalStatus.getStatusTime());
		simpleDateFormat.setCalendar(UTCCal);

		result = result.replaceAll(DATE, simpleDateFormat.format(UTCCal
				.getTime()));
		result = result.replaceAll(TELESCOPE, proposal.getSemester()
				.getTelescope());
		result = result.replaceAll(COMMITTEE, telescopeConfiguration
				.getCommittee());
		result = result.replaceAll(PROPOSAL_TITLE, proposal.getJustification()
				.getTitle());
		RegisteredMember contactAuthor = proposal.getContactAuthor();
		result = result.replaceAll(CONTACT_AUTHOR, getEmailName(contactAuthor
				.getUser()));
		return result;

	}

	protected void sendEmailForOtherMember(
			TelescopeConfiguration telescopeConfiguration,
			EmailConfiguration emailConfiguration, String message, Member member)
			throws AstronMailException, NamingException {
		if (member.getClass().equals(RegisteredMember.class)) {
			RegisteredMember registeredMember = (RegisteredMember) member;
			message = message.replaceAll(NAME, getEmailName(registeredMember
					.getUser()));
		} else {
			message = message.replaceAll(NAME, member.getName());
		}
		super.sendEmail(emailConfiguration, message, member.getEmail());

	}

	protected EmailConfiguration getSubmittedEmailConfiguration(
			Proposal proposal, TelescopeConfiguration telescopeConfiguration) {
		return telescopeConfiguration.getEmailConfiguration(SUBMITTED);
	}

	protected EmailConfiguration getRetractedEmailConfiguration(
			Proposal proposal, TelescopeConfiguration telescopeConfiguration) {
		return telescopeConfiguration.getEmailConfiguration(RETRACTED);
	}

	protected EmailConfiguration getInPreparationEmailConfiguration(
			Proposal proposal, TelescopeConfiguration telescopeConfiguration) {
		return telescopeConfiguration.getEmailConfiguration(IN_PREPARATION);
	}

	protected EmailConfiguration getUnderReviewEmailConfiguration(
			Proposal proposal, TelescopeConfiguration telescopeConfiguration) {
		return telescopeConfiguration.getEmailConfiguration(UNDER_REVIEW);
	}

	protected EmailConfiguration getAcceptedEmailConfiguration(
			Proposal proposal, TelescopeConfiguration telescopeConfiguration) {
		return telescopeConfiguration.getEmailConfiguration(ACCEPTED);
	}

	protected EmailConfiguration getRejectedEmailConfiguration(
			Proposal proposal, TelescopeConfiguration telescopeConfiguration) {
		return telescopeConfiguration.getEmailConfiguration(REJECTED);
	}

}
