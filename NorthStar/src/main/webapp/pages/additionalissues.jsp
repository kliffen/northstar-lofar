<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />

<html:form action="/processAdditionalIssues" focus="addThesisButton" method="POST">

	<table width="100%">
		<TR>
			<td width="*">&nbsp;</TD>
			<TD class="help" width="60">
<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="60" height="25">
							<TR>
					  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#additionalissues\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
				  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#additionalissues\" target=\"help\">Help</a>"%></TD>
			  				</TR>
			  			</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />	</TD>
		</TR>
	</table>
<table align="center">
	<TR>
		<TD class="header" colspan="2"><astron:label key="label.additionalissues.studentinformation"/> </TD>
	</TR>
	<TR>
		<TD colspan="2">
		<tiles:get name="box_header_no_top" />
			<table class="center" width="100%">
				<logic:notEmpty name="additionalIssuesForm" property="thesisBeans">
				<tr>
					<td>&nbsp;</td>
				</tr>
				<TR>
					<TD>
						<table align="center" class="projecttable">
							<TR>
							    <td class="tableheader"><astron:label key="label.additionalissues.student"/></td>
							    <td class="tableheader"><astron:label key="label.additionalissues.level"/></td>
							    <td class="tableheader"><astron:label key="label.additionalissues.applicant"/></td>
							    <td class="tableheader"><astron:label key="label.additionalissues.supervisor"/></td>
			     				<td class="tableheader"><astron:label key="label.additionalissues.applicant"/></td>
			     				<td class="tableheader" nowrap="true"><astron:label key="label.additionalissues.completiondate"/></td>
			     				<td class="tableheader" nowrap="true"><astron:label key="label.additionalissues.studentdata"/></td>
			     				<td class="tableheader" colspan="2">&nbsp;</td>
							</TR>
							<%
						int i=0;
						String styleClass = "";
						
						%>
				 			<logic:iterate id="thesis" name="additionalIssuesForm" property="thesisBeans">
				 				<%
				 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
								
				 			%>
				 
			 	 			<TR height="28">
								<TD class="tablefield_pri<%=styleClass%> "  valign="top"><bean:write name="thesis" property="studentName"/></TD>
								<TD class="tablefield<%=styleClass%> "  valign="top"><bean:write name="thesis" property="studentLevel"/></TD>
								<TD class="tablefield<%=styleClass%> "  valign="top" align="center">
									<logic:notEqual name="thesis" property="studentApplicant" value="false">Yes</logic:notEqual>
									<logic:equal name="thesis" property="studentApplicant" value="false">No</logic:equal>
								</TD>
								<TD class="tablefield<%=styleClass%> "  valign="top"><bean:write name="thesis" property="supervisorName"/></TD>
								<TD class="tablefield<%=styleClass%> "  valign="top" align="center">
									<logic:notEqual name="thesis" property="supervisorApplicant" value="false">Yes</logic:notEqual>
									<logic:equal name="thesis" property="supervisorApplicant" value="false">No</logic:equal>
								</TD>
								<TD class="tablefield<%=styleClass%>"  valign="top" align="center"><bean:write name="thesis" property="expectedCompletionDate"/></TD>
								<TD class="tablefield<%=styleClass%> "  valign="top" align="center">
									<logic:equal name="thesis" property="dataRequired" value="true">Yes</logic:equal>
									<logic:notEqual name="thesis" property="dataRequired" value="true">No</logic:notEqual>
								</TD>
								<TD class="tablefield<%=styleClass%>" valign="top">
									<html:submit indexed="true" property="editThesisButton" styleClass="list_edit" title="Edit thesis" onclick="setNorthStarCookie(getScrollCoordinates());">Edit</html:submit>
								</TD>
								<TD class="tablefield<%=styleClass%>" valign="top">
									<html:submit indexed="true" property="deleteThesisButton" styleClass="list_delete" title="Delete thesis" onclick="setNorthStarCookie(getScrollCoordinates());">Delete</html:submit>
								</TD>
				 			</TR>
				 			</logic:iterate>
				 			<tr><td colspan="8" class="spacer">&nbsp;</td></tr>
						</table>
					</TD>
				</TR>
				</logic:notEmpty>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<TR>
					<TD><html:submit property="addThesisButton" styleClass="list_newthesis" title="New Thesis"><astron:label key="label.additionalissues.addnewstudent"/></html:submit>
					</TD>
				</TR>	
			</table>
		<tiles:get name="box_footer" />
		</TD>
	</TR>
	
	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<TR>
		<TD class="header" colspan="2"><astron:label key="label.additionalissues.publicationinformation"/> </TD>
		</TR>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<logic:equal name="additionalIssuesForm" property="enableLinkedProposalsThis" value="true">
			<tr>
				<td class="right" valign="top"><astron:label key="label.additionalissues.linkedproposals.thistac" />  &nbsp;</td>
				<td class="left"><html:radio styleClass="radio" property="linkedProposals" value="true"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
								<html:radio styleClass="radio" property="linkedProposals" value="false"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio></td>
			</tr>
	
			<logic:equal name="additionalIssuesForm" property="linkedProposals" value="true">
			<tr>
				<td class="right" valign="top"><astron:label key="label.additionalissues.details" /></td>
				<td class="left"><html:textarea property="linkedProposalsSpecifics" cols="60" rows="4"/><span class="error">*</span></td>
			</tr>
			</logic:equal>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</logic:equal>
		<logic:equal name="additionalIssuesForm" property="enableLinkedProposalsOther" value="true">
			<tr>
				<td class="right" valign="top"><astron:label key="label.additionalissues.linkedproposals.othertac" />  :&nbsp;</td>
				<td class="left"><html:radio styleClass="radio" property="linkedProposalsElsewhere" value="true"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
								<html:radio styleClass="radio" property="linkedProposalsElsewhere" value="false"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio></td>
			</tr>
					
			<logic:equal name="additionalIssuesForm" property="linkedProposalsElsewhere" value="true">
			<tr>
				<td class="right" valign="top"><astron:label key="label.additionalissues.details" /></td>
				<td class="left"><html:textarea property="linkedProposalsElsewhereSpecifics" cols="60" rows="4"/><span class="error">*</span></td>
			</tr>
			</logic:equal>		
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</logic:equal>
		<logic:equal name="additionalIssuesForm" property="enablePreviousAllocations" value="true">
			<tr>
				<td class="right" valign="top"><astron:label key="label.additionalissues.previousallocations" /> &nbsp;</td>
				<td class="left"><html:radio styleClass="radio" property="previousAllocations" value="true"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
								<html:radio styleClass="radio" property="previousAllocations" value="false"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio></td>
			</tr>
			<logic:equal name="additionalIssuesForm" property="previousAllocations" value="true">
			<tr>
				<td class="right" valign="top">
					<astron:label key="label.additionalissues.previousallocationdetails" /> 
				</td>
				<td class="left"><html:textarea property="previousAllocationsSpecifics" cols="75" rows="4"/><span class="error">*</span></td>
			</tr>
			</logic:equal>	
				
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</logic:equal>
		<logic:equal name="additionalIssuesForm" property="enableNewObserverExperience" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.observerexperience" /><!-- Observing experience of <br/>users new to this telescope:&nbsp;--></td>
			<td class="left">
				<html:textarea property="newObserverExperience" cols="60" rows="4"/>
			</td>
		</tr>	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
		</logic:equal>
			<logic:equal name="additionalIssuesForm" property="enableRelatedPublications" value="true">
			<%-- <tr>
				<td class="right" valign="top"><astron:label key="label.additionalissues.relatedpublications" /></td>
				<td class="left"><html:radio styleClass="radio" property="relatedPublications" value="true"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
								<html:radio styleClass="radio" property="relatedPublications" value="false"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio></td>
			</tr>
			<logic:equal name="additionalIssuesForm" property="relatedPublications" value="true"> --%>
			<html:hidden property="maxPublicationCount"/>
			<tr>
				<td class="right" valign="top"><astron:label key="label.additionalissues.addpublications" /> <bean:write name="additionalIssuesForm" property="maxPublicationCount"/>) :&nbsp; <br/>
				<astron:label key="label.additionalissues.relatedpublicationspecifics" />
				</td>
				<td class="left"><html:textarea property="relatedPublicationsText" cols="75" rows="4" onkeydown="CountRows(document.forms[0].relatedPublicationsText,document.forms[0].maxPublicationCount.value,70);" onkeyup="CountRows(document.forms[0].relatedPublicationsText,document.forms[0].maxPublicationCount.value,70);" /><span class="error">*</span></td>
			</tr>
										
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
		</logic:equal>
		
		<!-- issue 7006  -->
		<logic:equal name="additionalIssuesForm" property="enablePreviousInvolvedProposal" value="true">						
			<tr>
				<td class="right" valign="top"><astron:label key="label.additionalissues.addpreviously" /> <br/>
				<astron:label key="label.additionalissues.relatedpublicationspecifics.previously" />
				</td>
				<td class="left"><html:textarea property="relatedPreviousInvolvedProposalText" cols="75" rows="4" onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].relatedPreviousInvolvedProposalText,document.forms[0].maxRelatedPreviousInvolvedProposalTextCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
  				(Words entered: <html:text property="maxRelatedPreviousInvolvedProposalTextCount" size="2" disabled="disabled" styleClass="count"/>)<span class="error"><span class="error">*</span></td> 
			</tr>
		
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>		
		</logic:equal>
		<!-- end issue 7006  -->		
		
		<logic:equal name="additionalIssuesForm" property="enableGrantNumber" value="true">
			<tr>
			<td class="right" valign="top"><astron:label key="label.additionalissues.grantnumber" /> &nbsp;</td>
			<td class="left"><html:text property="grantNumber" size="12"/></td>
		</tr>	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
		</logic:equal>
		<logic:equal name="additionalIssuesForm" property="displaySponsoring" value="true">
		<tr>
			<td class="right"><astron:label key="label.additionalissues.sponsoring" /><!-- Do you seek to carry out this project using reserved access rights  :&nbsp;--></td>
			<td class="left">
				<html:radio property="enableSponsoring" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
				<html:radio property="enableSponsoring" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
		</logic:equal>	
		<logic:equal name="additionalIssuesForm" property="enableSponsoring" value="true">
		<tr>
			<td class="right"> <astron:label key="label.additionalissues.sponsoring.specifics" /> <!-- list one or more organisations from which you are requesting sponsorship (e.g. GLOW) :&nbsp;--></td>
			<td class="left"><html:textarea property="sponsorDetails" cols="60" rows="4"/></td>
				
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</logic:equal>
	<logic:equal name="additionalIssuesForm" property="displayOpticonSupport" value="true">
		<tr>
			<td class="right"><astron:label key="label.additionalissues.sponsoring.opticon" /> <!-- opticon support--></td>
			<logic:equal name="additionalIssuesForm" property="displayOpticonSupport" value="true">
			<td class="left"><astron:label key="label.additionalissues.opticonsupport.yes" /></td>
			</logic:equal>	
			<logic:equal name="additionalIssuesForm" property="displayOpticonSupport" value="false">
			<td class="left"><astron:label key="label.additionalissues.opticonsupport.no" /></td>
			</logic:equal>	
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</logic:equal>
		<tr>
			<td class="right" valign="top"><astron:label key="label.additionalissues.additionalremarks" /> </td>
			<td class="left"><html:textarea property="additionalRemarks" cols="75" rows="5"/></td>
		</tr>		
	</table><br/>
		<bean:define id="userIsContactAuthor" name="additionalIssuesForm" property="userIsContactAuthor"/>
<%@ include file="include/storebuttons.jsp" %>		
</html:form>
