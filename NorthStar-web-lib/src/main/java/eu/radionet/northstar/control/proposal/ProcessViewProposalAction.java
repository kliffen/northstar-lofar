// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessDownloadAction.java 
 *
 * Created on Feb 9, 2005
 *
 * Version $Id: ProcessViewProposalAction.java,v 1.5 2006-06-27 12:44:51 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.util.PdfUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.WebDAVConfiguration;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The ProcessDownloadAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessViewProposalAction extends Action {
	private Log log = LogFactory.getLog(this.getClass());


	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/*
		 * retrieve proposal data from session with information
		 */
		String proposalIdString = request.getParameter("proposalId");
		HttpSession session = request.getSession();
		Proposal proposal = null;
		UserAccount ownUserAccount = (UserAccount) session
		.getAttribute(Constants.OWN_USERACCOUNT);
		if (AstronValidator.isPositiveInt(proposalIdString)){
			Integer proposalId = AstronConverter.toInteger(proposalIdString);
			NorthStarDelegate northStarDelegate = NorthStarDelegate.getInstance();
			proposal = northStarDelegate.getOwnProposal(proposalId,
					ownUserAccount);
			if (proposal == null){
				CollaborationDelegate collaborationDelegate = CollaborationDelegate.getInstance();
				proposal = collaborationDelegate.getProposal(proposalId,
						ownUserAccount);
			}
		}else {
		/*
		 * retrieve proposal data from session with information
		 */
		ProposalData proposalData = (ProposalData) session
				.getAttribute(Constants.PROPOSAL_DATA);
		proposal = proposalData.getProposal();
		}


		boolean isStatusUnderReviewOrHigher = NorthStarDelegate
				.isStatusUnderReviewOrHigher(proposal);
		if (isStatusUnderReviewOrHigher) {
			String url = WebDAVConfiguration.getRedirectUrl()
					+ WebDAVConfiguration.getPdfLocation(proposal);
			response.sendRedirect(url);
			return null;
		}
		PdfUtils pdfUtils = new PdfUtils();
		byte[] content = pdfUtils.getProposalPdf(proposal,
				request);

		/*
		 * document.open(); document.add(new Paragraph("Hello world"));
		 * document.close();
		 */
		response.setContentType("application/pdf");

		String fileName = null;
		if (!AstronValidator.isBlankOrNull(proposal.getCode())) {
			fileName = AstronConverter.removeNonWordCharacters(proposal.getCode()) + ".pdf";
		} else {
			fileName = "proposal_with_no_code.pdf";
		}
		response.setHeader("Content-disposition", "attachment; filename="
				+ fileName);
		if (getBrowser(request).equals("msie")) {
			response.setHeader("Cache-Control", null);
			response.setHeader("Pragma", null);
		}
		response.setContentLength(content.length);
		try {
			response.getOutputStream().write(content);
			response.getOutputStream().flush();
			response.getOutputStream().close();
		} catch (IOException io) {
			log.warn(LogMessage.getMessage(ownUserAccount,
					"Exception while downloading pdf "
							+ io.getClass().getName() + " " + io.getMessage()));
		}
		return null;
	}
	protected String getBrowser(HttpServletRequest request) {

		String useragent = request.getHeader("User-Agent").toLowerCase();
		if (useragent.matches(".*msie.*")) {
			return "msie";
		} else if (useragent.matches(".*gecko.*")) {
			return "gecko";
		} else {
			return "other";
		}

	}

}
