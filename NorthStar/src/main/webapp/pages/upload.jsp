<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />
<html:form action="/processUpload" enctype="multipart/form-data" method="POST">

<html:hidden property="kindUploadFile"/>
<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
<table width="100%">
	<TR>
		<td width="*">&nbsp;</TD>
		<TD class="help" width="220">
	
			<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
				<TABLE width="220" height="25">
					<TR>
			  			<TD width='20' height="25"><%= "<a title=\"Opens the instructions for preparation of justification files\" href=\"" + request.getContextPath() + "/justificationInstructions.do\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
		  				<TD width="200" height="25"><%= "<a title=\"Opens the instructions for preparation of justification files\" href=\"" + request.getContextPath() + "/justificationInstructions.do\" target=\"help\">Instructions for preparation</a>"%></TD>
	  				</TR>
	  			</TABLE>
			<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />		
		</TD>
		<TD class="help" width="60">
			<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
				<TABLE width="60" height="25">
					<TR>
			  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#justification\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
		  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#justification\" target=\"help\">Help</a>"%></TD>
	  				</TR>
	  			</TABLE>
			<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />		
		</TD>
	</TR>
</table>

<table align="center">
	<TR>
		<TD colspan="2">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				<TD valign='top'>
				<logic:notEmpty name="uploadForm" property="uploadMessage">
						<pre><bean:write name="uploadForm" property="uploadMessage"
							filter="false" /></pre> 
				</logic:notEmpty>
				<logic:empty name="uploadForm" property="uploadMessage">
 					Note: the file size of the uploaded file cannot exceed 10MB
				</logic:empty>
				</TD>
			</TR>
					<TR>
						<td></td>
						<td colspan="4" class="italic" align="left">
						<logic:equal name="uploadForm" property="variableMaxPageNumber" value="true">
								<astron:label key="label.justification.pagelimit.rule" />
						</logic:equal>
						</td>
					</TR>
				</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<logic:equal name="uploadForm" property="showSingleFileUpload" value="true">
		<tr>
				<logic:equal name="uploadForm" property="kindUploadFile" value="scientific">
				<td class="right">Upload Scientific Justification File :&nbsp;</td>
				</logic:equal>
				<logic:equal name="uploadForm" property="kindUploadFile" value="technical">
				<td class="right">Upload Technical Justification File :&nbsp;</td>
				</logic:equal>
				<logic:equal name="uploadForm" property="kindUploadFile" value="figure">
				<td class="right">Upload Figure(s) File :&nbsp;</td>
				</logic:equal>
				<td class="left"><html:file property="justificationFile" size="50" /></td>
			</tr>
			<tr>
				<td  colspan="2"><span class="error"><astron:errors property="justificationFile" ifnotexist=""/></span>&nbsp;</td>
			</tr>
		</table>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />	
		
		
		
		<table width="100%">
	
		
			<tr>
				<td class="left"><html:submit property="uploadFileButton" styleClass="list_upload" title="Upload">Upload</html:submit></td><td class="right">
					<html:cancel styleClass="list_decline_t" title="Cancel upload">Cancel</html:cancel></td>
			</tr>
		
		</table>
		
	</logic:equal>
	<logic:equal name="uploadForm" property="showSingleFileUpload" value="false">
		<tr>
			<td class="right">Upload Files :&nbsp;</td>
			<td class="left"> &nbsp; </td></tr>
			<tr><td class="right">
			<html:file property="justificationFile" size="37" /> &nbsp; </td>
			<td class="left"><html:submit property="attachFileButton" styleClass="list_upload" title="Upload">Attach</html:submit> 
			</td></tr>
		</tr>
		<tr>
			<td  colspan="2"><span class="error"><astron:errors property="justificationFile" ifnotexist=""/></span>&nbsp;</td>
		</tr>
		<tr><td class="right">
			<!-- onchange="document.forms[0].submit()" -->
			<bean:define id="files" name="uploadForm" property="fileList"/> 
			<html:select property="selectedFile" size="10" style="width: 400px">
				<html:options collection="files" property="value" labelProperty="label"/>
			</html:select> &nbsp; 
		</td><td class="left" valign="top">
			<html:submit property="deleteFileButton" styleClass="list_delete_file" title="Delete selected file"> Delete</html:submit>
		</td></tr>
		<tr>
			<td  colspan="2"> &nbsp;</td>
		</tr>
	</table>
	<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />	
	<table width="100%">
		<tr>
			<td class="left"><html:submit property="previewFileButton" styleClass="list_saveandview" title="Upload">Preview</html:submit>
			</td><td class="right">
				<html:cancel styleClass="list_decline_t" title="Cancel upload">Finish</html:cancel></td>
		</tr>
	
	</table>
	</logic:equal>

</html:form>

