DROP TABLE IF EXISTS `statusemail`;
CREATE TABLE `statusemail` (
  `id` int(11) NOT NULL auto_increment,
  `proposalstatusid` int(11) NOT NULL,  
  `proposalid` int(11) NOT NULL,
  `oldstatus` varchar(20) NOT NULL,
  `newstatus` varchar(20) NOT NULL,
  `subject` varchar(255) default NULL,
  `sender` varchar(255) default NULL,
  `torecipients` varchar(255) default NULL,    
  `ccrecipients` varchar(255) default NULL,     
  `bccrecipients` varchar(255) default NULL,  
  `message` text default NULL,
  `generatedmail` tinyint(1) NOT NULL default '0',  
  PRIMARY KEY  (`id`),
  KEY `proposal_statusemail_IND` (`proposalid`),
  CONSTRAINT `proposal_statusemail_FK` FOREIGN KEY (`proposalid`) REFERENCES `proposal` (`id`) ON DELETE CASCADE
) TYPE=InnoDB;


 