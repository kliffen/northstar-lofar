// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Feb 13, 2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package eu.radionet.northstar.business;

import java.math.BigDecimal;

import nl.astron.util.AstronValidator;

/**
 * @author Holties
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AstroCoordinate {

	private double ra = 0; //default zero; internal variable represents arcsec
	private double dec = 0; //default zero; internal variable represents arcsec
	
	/**
	 * Constructor initializing in degrees.
	 * No range checking. Instead transform to following range:
	 * -90 <= ra <= 90
	 * 0 <= ha < 360
	 * 
	 * @param ra right ascenscion in degrees
	 */
	public AstroCoordinate(double ra, double dec) {
		super();
		this.ra = ra*240;
		this.dec = dec*3600;
	}
	
    public AstroCoordinate(double ra, double dec, boolean degrees) {
        super();
        if (degrees) {
            new AstroCoordinate(ra, dec);
        } else {
            this.ra = ra;
            this.dec = dec;
        }
    }
    
	/**
	 * Constructor
	 * @param dd
	 */
	public AstroCoordinate(int ra_h, int ra_m, double ra_s, int dec_d, int dec_m, double dec_s) {
		super();

		this.ra = ra_h*3600+ra_m*60+ra_s;
		this.dec = dec_d*3600+dec_m*60*dec_s;
	}
	
	/**
	 * Constructor
	 * @param raString
	 * @param decString
	 */
	public AstroCoordinate(String raString, String decString) throws Exception {
		super();
		
		try {
			setRA(raString);
			setDec(decString);
		} catch (Exception e) {
			ra = 0;
			dec = 0;
			throw e;
		}

	}

	public void setRA(double ra) {
		this.ra = ra*240;
	}
	
	public void setDec(double dec) {
		this.dec = dec*3600;
	}
	
	public void setRA(String raString) throws Exception {
		
        if (!AstronValidator.isBlankOrNull(raString)) {
    		int sign = 1;
    		Integer hh = null;
    		Integer mm = null;
    		Double ss = null;

    		if (raString.charAt(0) == '+') {
        		raString = raString.substring(1);
        	} else if (raString.charAt(0) == '-') {
        		raString = raString.substring(1);
        		sign = -1;
        	} 
        	String[] hhmmssString = raString.split(":");
        	if ((hhmmssString.length == 1) && (AstronValidator.isDouble(raString))) {
            	/*
            	 * no colons: assume degrees
            	 */
                    this.ra = new Double(raString).doubleValue()*240*sign;

        	} else if ((hhmmssString.length <= 3) && (AstronValidator.isInt(hhmmssString[0]))) {
            	/*
            	 * convert to hours and look for minutes
            	 */
        		hh = new Integer(hhmmssString[0]);
        		
        		if ((hhmmssString.length >= 2) && (AstronValidator.isInt(hhmmssString[1]))) {
                	/*
                	 * convert to minutes and look for seconds
                	 */
            		mm = new Integer(hhmmssString[1]);

            		if ((hhmmssString.length >=3) && (AstronValidator.isDouble(hhmmssString[2]))) {
                    	/*
                    	 * convert to degrees and transform
                    	 */
                		ss = new Double(hhmmssString[2]);
                		
            		} else if (hhmmssString.length == 2) {
                		ss = new Double(0);
    
            		} else {
                    	Exception e = new Exception("invalid format");
                    	throw e;
            		}
            	} else {
                	Exception e = new Exception("invalid format");
                	throw e;
        		}
        	} else {
            	Exception e = new Exception("invalid format");
            	throw e;
            }

        	if ((hh != null)&&(mm!=null)&&(ss!=null)) {

        		double raTemp = 0;

           		raTemp += hh.doubleValue()*3600;
            	raTemp += mm.doubleValue()*60;
            	raTemp += ss.doubleValue();

    			this.ra = raTemp*sign;
        	}
        }
	}

	public void setDec(String decString) throws Exception {
		
        if (!AstronValidator.isBlankOrNull(decString)) {
    		int sign = 1;
    		Integer dd = null;
    		Integer mm = null;
    		Double ss = null;

    		
    		if (decString.charAt(0) == '+') {
        		decString = decString.substring(1);
        	} else if (decString.charAt(0) == '-') {
        		decString = decString.substring(1);
        		sign = -1;
        	} 
        	
        	String[] ddmmssString = decString.split(":");
        	if ((ddmmssString.length == 1) && (AstronValidator.isDouble(decString))) {
            	/*
            	 * no colons: assume degrees
            	 */
                    this.dec = new Double(decString).doubleValue()*3600*sign;

        	} else if ((ddmmssString.length <= 3) && (AstronValidator.isInt(ddmmssString[0]))) {
            	/*
            	 * convert to degrees and look for minutes
            	 */
        		dd = new Integer(ddmmssString[0]);
        		
        		if ((ddmmssString.length >= 2) && (AstronValidator.isInt(ddmmssString[1]))) {
                	/*
                	 * convert to minutes and look for seconds
                	 */
            		mm = new Integer(ddmmssString[1]);

            		if ((ddmmssString.length >=3) && (AstronValidator.isDouble(ddmmssString[2]))) {
                    	/*
                    	 * convert to seconds and transform
                    	 */
                		ss = new Double(ddmmssString[2]);
 
            		} else if (ddmmssString.length == 2) {
                		ss = new Double(0);
    
            		} else {
                    	Exception e = new Exception("invalid format");
                    	throw e;
            		}
            	} else {
                	Exception e = new Exception("invalid format");
                	throw e;
        		}
        	} else {
            	Exception e = new Exception("invalid format");
            	throw e;
            }

        	if ((dd != null)&&(mm!=null)&&(ss!=null)) {
           		
            	double decTemp = 0;

           		decTemp += dd.doubleValue()*3600;
            	decTemp += mm.doubleValue()*60;
            	decTemp += ss.doubleValue();

    			this.dec = decTemp*sign;
        	}
        }
	}

	private void transform() {
		
		double dec_abs = StrictMath.abs(this.dec);
		int sign = 1;
		if (this.dec < 0) sign = -1;
		
		int nFullCircle = (int) (dec_abs/(360*3600.0));
		dec_abs = dec_abs - nFullCircle*360*3600;
		// now: -360 < dec < 360
		if (dec_abs > 180*3600) {
			dec_abs = 360*3600 - dec_abs;
			sign = -sign;
		}
		// now: -180 <= dec <= 180
		if (dec_abs > 90*3600) {
			dec_abs = 180*3600 - dec_abs;
			this.ra = this.ra + 180*3600;
		}
		// now: -90 <= dec <= 90
		this.dec = sign*dec_abs;
		
		// transform ra range
		nFullCircle = (int) (this.ra/(360*240.0));
		this.ra = this.ra - nFullCircle*360*240;
		// now: -180 <= ra < 180

		if (this.ra < 0) {
			this.ra = this.ra + 360*240;
		}
		//now: 0 <= ra < 360
		
	}
	
	public double RA() {
		transform();
		return this.ra/240.0;
	}
	
	public double Dec() {
		transform();
		return this.dec/3600.0;
	}
	
    public double RA_sec() {
        transform();
        return this.ra;
    }
    
    public double Dec_sec() {
        transform();
        return this.dec;
    }
    
	public String RAString(int decimals, boolean trim_zeros) {
		String raString = "";
		
		transform();

		int hh = (int) this.ra/3600;
		if (hh < 10) {
			raString += "0";
		}
		raString += hh + ":";
		int mm = (int) (this.ra - 3600*hh)/60;
		if (mm < 10) {
			raString += "0";
		}
		raString += mm + ":";
		BigDecimal ss = new BigDecimal(this.ra - 3600*hh - 60*mm);
		if (ss.doubleValue() < 10) {
			raString += "0";
		}
		raString += ss.setScale(decimals, BigDecimal.ROUND_HALF_UP).toString();
		
        if (trim_zeros) {
            while (raString.endsWith("0")) {
                raString = raString.substring(0, raString.length()-1);
            }

            if (raString.endsWith(".")) {
                raString = raString.substring(0, raString.length()-1);
            }
        }

        return raString;
	}
	
    
    public String RAString() {
        /*
         * Default precission for Right Ascension is 2 decimals; keep trailing zeros
         */
        return RAString(2, false);
    }
    
	public String DecString(int decimals, boolean trim_zeros) {
		String decString = "+";
		
		transform();
		
		double dec_abs = StrictMath.abs(this.dec);
		if (this.dec < 0) {
			decString = "-";
		}
		
		int dd = (int) dec_abs/3600;
		if (dd < 10) {
			decString += "0";
		}
		decString += dd + ":";
		int mm = (int) (dec_abs - 3600*dd)/60;
		if (mm < 10) {
			decString += "0";
		}
		decString += mm + ":";
		BigDecimal ss = new BigDecimal(dec_abs - 3600*dd - 60*mm);
		if (ss.doubleValue() < 10) {
			decString += "0";
		}
		decString += ss.setScale(decimals, BigDecimal.ROUND_HALF_UP).toString();

        if (trim_zeros) {
            while (decString.endsWith("0")) {
                decString = decString.substring(0, decString.length()-1);
            }

            if (decString.endsWith(".")) {
                decString = decString.substring(0, decString.length()-1);
            }
        }

        return decString;
	}
    
    public String DecString() {
        /*
         * Default precission for declination is 1 decimal; keep trailing zeros
         */
        return DecString(1, false);
    }
}
