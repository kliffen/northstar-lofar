// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.semester.list;

import java.io.Serializable;

public class SemesterBean implements Serializable {
	protected String id = null;
	protected String prefix = null;
	protected String startTime = null;
	protected String endTime = null;
	protected String deadLineTime = null;
	protected String category = null;
	protected String lastAssignedNumber = null;
	protected String telescope = null;
	protected boolean allowedToDelete = false;
	protected boolean available = false;
	protected boolean immediate = false;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getDeadLineTime() {
		return deadLineTime;
	}
	public void setDeadLineTime(String deadLineTime) {
		this.deadLineTime = deadLineTime;
	}
	public String getEndTime() {
		return endTime;
	}
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getLastAssignedNumber() {
		return lastAssignedNumber;
	}
	public void setLastAssignedNumber(String lastAssignedNumber) {
		this.lastAssignedNumber = lastAssignedNumber;
	}
	public String getPrefix() {
		return prefix;
	}
	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	public String getTelescope() {
		return telescope;
	}
	public void setTelescope(String telescope) {
		this.telescope = telescope;
	}
	public boolean isAllowedToDelete() {
		return allowedToDelete;
	}
	public void setAllowedToDelete(boolean allowedToDelete) {
		this.allowedToDelete = allowedToDelete;
	}
	public boolean isAvailable() {
		return available;
	}
	public void setAvailable(boolean available) {
		this.available = available;
	}
	public boolean isImmediate() {
		return immediate;
	}
	public void setImmediate(boolean immediate) {
		this.immediate = immediate;
	}
}
