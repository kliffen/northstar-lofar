// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessUploadAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: ProcessOpticonSubmitAction.java,v 1.3 2008-04-25 09:41:39 smit Exp $
 *
 */
package eu.opticon.northstar.control.proposal.submit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.opticon.northstar.business.OpticonProposalDelegate;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.submit.ProcessSubmitAction;
import eu.radionet.northstar.control.proposal.submit.SubmitForm;

/**
 * The ProcessUploadAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessOpticonSubmitAction extends ProcessSubmitAction 
{

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception 
	{
			// here was some code which made the proposal codes add twice...
			//
			return super.lockedExecute(mapping, form, request, response);
	}
	
	

}