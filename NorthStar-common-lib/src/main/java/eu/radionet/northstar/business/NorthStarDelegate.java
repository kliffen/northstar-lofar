// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.business.UserManagement;
import nl.astron.useradministration.data.entities.User;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.useradministration.data.entities.UserAccountSummary;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.radionet.northstar.business.configuration.ConfigurationUtil;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.data.Repository;
import eu.radionet.northstar.data.entities.Category;
import eu.radionet.northstar.data.entities.Justification;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalStatus;
import eu.radionet.northstar.data.entities.ProposalSummary;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.Resource;
import eu.radionet.northstar.data.entities.ResourceType;
import eu.radionet.northstar.data.entities.Role;
import eu.radionet.northstar.data.entities.ScientificFile;
import eu.radionet.northstar.data.entities.Semester;
import eu.radionet.northstar.data.entities.Status;
import eu.radionet.northstar.data.entities.admin.AdminProposalSummary;
import eu.radionet.northstar.data.entities.admin.StatusEmail;
import eu.radionet.northstar.data.entities.UserTelescope;

public class NorthStarDelegate {
	private Log log = LogFactory.getLog(this.getClass());

	private static NorthStarDelegate proposalsDelegate = null;

	protected static ConfigurationUtil configurationUtil = null;

	// private static CollaborationDelegate collaborationDelegate = null;

	// private static DelegateUtils delegateUtils = null;
	private static UserManagement userManagement = null;

	private static Repository repository = null;

	/**
	 * Default constructor
	 */
	private NorthStarDelegate() throws ConnectionException {
		repository = new Repository();
		configurationUtil = ConfigurationUtil.getInstance();
		userManagement = new UserManagement();
		// delegateUtils = DelegateUtils.getInstance();
	}

	public static Calendar getCurrentDate() {
		Calendar currentDate = new GregorianCalendar();
		return currentDate;
	}

	/**
	 * The getInstance method provides
	 * 
	 * @return
	 * @throws ConnectionException
	 */
	public static NorthStarDelegate getInstance() throws ConnectionException {
		if (null == proposalsDelegate) {
			proposalsDelegate = new NorthStarDelegate();
		}
		return proposalsDelegate;
	}

	public ResourceType getResourceType(String name, String unit)
			throws DatabaseException {
		return repository.getResourceType(name, unit);
	}

	public List getProposals(String[] statusses, String[] categories,
			String[] telescopes, String[] semesters, String[] communities)
			throws DatabaseException {
		return retrieveUserInfo(repository.getProposals(statusses, categories,
				telescopes, semesters, communities));
	}

	public List getProposals(String[] statusses, String[] categories,
			String telescope, String[] semesters, String community)
			throws DatabaseException {
		return retrieveUserInfo(repository.getProposals(statusses, categories,
				new String[] { telescope }, semesters,
				new String[] { community }));
	}

	public List getAdminProposalSummaries(boolean b, String[] statusses, String[] categories,
			String telescope, String[] semesters, String community)
			throws DatabaseException {
		List results = new ArrayList();
		List proposalSummaries = repository.getProposalSummaries(b,statusses, categories,
				new String[] { telescope }, semesters,
				new String[] { community });
		Iterator iterator = proposalSummaries.iterator();
		while (iterator.hasNext()) {
			Object[] object = (Object[]) iterator.next();
			AdminProposalSummary proposalSummary = new AdminProposalSummary();
			proposalSummary.setId((Integer) object[0]);
			proposalSummary.setCode((String) object[1]);
			proposalSummary.setCurrentStatus((ProposalStatus) object[2]);
			proposalSummary.setSemester((Semester) object[3]);
			Justification justification = (Justification) object[4];
			if (justification != null){
				proposalSummary.setTitle(justification.getTitle());
			}
			List members = repository.getPiAndContactAuthor(proposalSummary.getId());
			Iterator memberIterator = members.iterator();
			while (memberIterator.hasNext()){
				Member member = (Member) memberIterator.next();
				if (member.isContactAuthor()){
					proposalSummary.setContactAuthor(retrieveUserInfo(member));
				}
				if (member.isPi()){
					proposalSummary.setPi(retrieveUserInfo(member));
				}
			}
			results.add(proposalSummary);
		}
		return results;

	}
	public Member retrieveUserInfo(Member member) throws DatabaseException {
		/*
		 * if member is registered member retrieve user information and add it
		 * to registered member
		 */
		if (member.getClass().equals(RegisteredMember.class)) {
			RegisteredMember registeredMember = (RegisteredMember) member;
			User user = userManagement.getUser(registeredMember.getUserId());
			registeredMember.setUser(user);
		}

		return member;
	}

	public StatusEmail getStatusEmailByProposalStatusId(Integer proposalStatusId)
			throws DatabaseException {
		return repository.getStatusEmailByProposalStatusId(proposalStatusId);
	}

	public List getProposalsByIds(String[] ids) throws DatabaseException {
		return retrieveUserInfo(repository.getProposalsByIds(ids));
	}

	public List getCategories() throws DatabaseException {
		return repository.getCategories();
	}

	public Category getDefaultCategory() throws DatabaseException {
		return repository.getDefaultCategory();
	}

	public List getStatusses() throws DatabaseException {
		return repository.getStatusses();
	}

	public List getSemesters() throws DatabaseException {
		return repository.getSemesters();
	}

	public List getNotClosedSemesters() throws DatabaseException {
		return repository.getNotClosedSemesters();
	}

	public List getSemesters(String name) throws DatabaseException {
		return repository.getSemesters(name, getCurrentDate());
	}

	public List getAvailableSemesters(String name) throws DatabaseException {
		return repository.getAvailableSemesters(name, getCurrentDate());
	}

	public Semester getSemester(Integer id) throws DatabaseException {
		return repository.getSemester(id);
	}

	public void storeSemester(Semester semester) throws DatabaseException {
		repository.store(semester);
	}

	public void storeStatusEmail(StatusEmail statusEmail)
			throws DatabaseException {
		repository.store(statusEmail);
	}

	public void deleteSemester(Semester semester) throws DatabaseException {
		repository.delete(semester);
	}

	public Category getCategory(Integer id) throws DatabaseException {
		return repository.getCategory(id);
	}

	public Status getStatus(Integer id) throws DatabaseException {
		if (id != null) {
			return repository.getStatus(id.intValue());
		}
		return null;
	}

	public List getSemesters(String[] categoriesIds, String[] telescopes,
			String[] communities, Calendar date) throws DatabaseException {
		return repository.getSemesters(categoriesIds, telescopes, communities,
				date);
	}

	/**
	 * The getOwnProposals method retrieve all own proposals
	 * 
	 * @param userAccount
	 * @return List of proposals
	 * @throws DatabaseException
	 */
	public List getOwnProposals(String[] selectedTelescopes,
			UserAccount userAccount) throws DatabaseException {
		return retrieveUserInfo(repository.getOwnProposals(selectedTelescopes,
				userAccount.getUser().getId()));
	}


	
	public List getOwnProposals(boolean showReviewedProposals,
			String telescope, UserAccount userAccount) throws DatabaseException {
		List statusCodes = new ArrayList();
		statusCodes.add(Constants.STATUS_IN_PREPARATION);
		statusCodes.add(Constants.STATUS_SUBMITTED);
		statusCodes.add(Constants.STATUS_UNDER_REVIEW);
		if (showReviewedProposals) {
			statusCodes.add(Constants.STATUS_REJECTED);
			statusCodes.add(Constants.STATUS_ACCEPTED);
			statusCodes.add(Constants.STATUS_INVALID);
		}
		return retrieveUserInfo(repository.getOwnProposals(telescope,
				statusCodes, userAccount.getUser().getId()));
	}

	public List getSubmittedProposals() throws DatabaseException {
		return retrieveUserInfo(repository.getSubmittedProposals());
	}

	public Proposal getOwnProposal(Integer proposalId, UserAccount userAccount)
			throws DatabaseException {
		List members = repository.getRegisteredMembers(userAccount.getUser()
				.getId());
		if (members.size() > 0) {
			return retrieveUserInfo(repository.getOwnProposal(proposalId,
					members));
		}
		return null;
		// return getNewProposal(userAccount);
	}

	public Proposal getProposal(Integer proposalId) throws DatabaseException {

		return retrieveUserInfo(repository.getProposal(proposalId));

	}

	public List getSubmittedProposalsBySemesterId(Integer semesterId)
			throws DatabaseException {
		return retrieveUserInfo(repository
				.getSubmittedProposalsBySemesterId(semesterId));
	}

	/**
	 * The getNewProposal method creates an new Proposal
	 * 
	 * @param userAccount
	 * @return
	 * @throws DatabaseException
	 */
	public Proposal getNewProposal(UserAccount userAccount, Semester semester)
			throws DatabaseException, InvalidConfigurationException {
		List roles = repository.getDefaultRoles();
		Status status = repository.getDefaultStatus();
		Proposal proposal = new Proposal();
		proposal.setSemester(semester);
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(semester, NorthStarDelegate
						.getCurrentDate());
		/*
		 * retrieve version
		 */
		// this is an ugly assertion that telescopeconfiguration does exist
		proposal.setVersion(telescopeConfiguration.getVersion());
		ObservingRequest observingRequest = configurationUtil
				.getObservingRequest(telescopeConfiguration);

		proposal.addObservingRequest(observingRequest);

		RegisteredMember registeredMember = new RegisteredMember();
		registeredMember.setUser(userAccount.getUser());
		for (int i = 0; i < roles.size(); i++) {
			registeredMember.addRole((Role) roles.get(i));
		}
		proposal.addChangeMember(registeredMember);
		proposal
				.setCurrentProposalStatus(status, userAccount.getUser().getId());
		return proposal;
	}

	/*
	 * public String getName(User user) { String title = ""; if (user != null) {
	 * if (user.getTitle() != null) { title = user.getTitle(); } return
	 * user.getLastName() + ", " + title + " " + user.getFirstName(); } else {
	 * return null; } }
	 */
	public static String getName(User user) {
		if (user != null) {
			String name = null;
			if (AstronValidator.isBlankOrNull(user.getTitle())) {
				name = user.getFirstName();
			} else {
				name = user.getTitle() + " " + user.getFirstName();
			}
			name += " " + user.getLastName();
			return name;
		}
		return null;
	}

	/**
	 * The retrieveUserInfo method retrieves user information
	 * 
	 * @param proposals
	 * @return
	 * @throws DatabaseException
	 */
	protected List retrieveUserInfo(List proposals) throws DatabaseException {
		for (int i = 0; i < proposals.size(); i++) {
			Proposal proposal = (Proposal) proposals.get(i);
			proposals.set(i, retrieveUserInfo(proposal));
		}
		return proposals;
	}

	/**
	 * The retrieveUserInfo method retrieves user information
	 * 
	 * @param proposal
	 * @return Proposal
	 * @throws DatabaseException
	 */
	protected Proposal retrieveUserInfo(Proposal proposal)
			throws DatabaseException {
		if (proposal != null) {
			Iterator members = proposal.getMembers().iterator();
			while (members.hasNext()) {
				Member member = (Member) members.next();
				/*
				 * if member is registered member retrieve user information and
				 * add it to registered member
				 */
				if (member!=null && member.getClass()!=null && member.getClass().equals(RegisteredMember.class)) {
					RegisteredMember registeredMember = (RegisteredMember) member;
					User user = userManagement.getUser(registeredMember
							.getUserId());
					registeredMember.setUser(user);
				}

			}
		}
		return proposal;
	}

	/**
	 * The getUserAccountByName method retrieves userAccount by userame
	 * 
	 * @param username
	 * @return UserAccount
	 * @throws DatabaseException
	 */
	public UserAccount getUserAccountByName(String username)
			throws DatabaseException {
		
		return userManagement.getUserAccount(username);
	}

	public User getUser(Integer userId) throws DatabaseException {
		return userManagement.getUser(userId);
	}

	/**
	 * isStatusSubmitted is a helper function that returns true if the status of
	 * the proposal is 'submitted'
	 * 
	 * @param proposal
	 * @return true/false
	 * @author Nico Vermaas
	 * @since 22 feb 2005
	 */
	public boolean isStatusSubmitted(Proposal proposal) {
		return proposal.getCurrentStatus().getStatus().getCode()
				.equalsIgnoreCase(Constants.STATUS_SUBMITTED);
	}

	/**
	 * isStatusInPreparation is a helper function that returns true if the
	 * status of the proposal is 'in preparation'
	 * 
	 * @param proposal
	 * @return true/false
	 * @author Bastiaan Verhoef
	 * @since 20 feb 2006
	 */
	public boolean isStatusInPreparation(Proposal proposal) {
		return proposal.getCurrentStatus().getStatus().getCode()
				.equalsIgnoreCase(Constants.STATUS_IN_PREPARATION);
	}

	/**
	 * isStatusUnderReview is a helper function that returns true if the status
	 * of the proposal is 'under review'
	 * 
	 * @param proposal
	 * @return true/false
	 * @throws DatabaseException
	 * @author Nico Vermaas
	 * @since 22 feb 2005
	 */
	public boolean isStatusUnderReview(Proposal proposal) {
		return proposal.getCurrentStatus().getStatus().getCode()
				.equalsIgnoreCase(Constants.STATUS_UNDER_REVIEW);
	}

	public boolean isAccepted(Proposal proposal) {
		log.debug(proposal.getCurrentStatus().getStatus().getCode() + " - "
				+ Constants.STATUS_ACCEPTED);
		return proposal.getCurrentStatus().getStatus().getCode()
				.equalsIgnoreCase(Constants.STATUS_ACCEPTED);
	}

	public boolean isRejected(Proposal proposal) {
		return proposal.getCurrentStatus().getStatus().getCode()
				.equalsIgnoreCase(Constants.STATUS_REJECTED);
	}

	public static boolean isStatusUnderReviewOrHigher(Proposal proposal) {
		String statusString = proposal.getCurrentStatus().getStatus().getCode();
		return statusString.equalsIgnoreCase(Constants.STATUS_UNDER_REVIEW)
				|| statusString.equalsIgnoreCase(Constants.STATUS_ACCEPTED)
				|| statusString.equalsIgnoreCase(Constants.STATUS_REJECTED);
	}

	public boolean isDeadLinePassed(Proposal proposal) {
		Date currentDate = getCurrentDate().getTime();
		Date deadLine = proposal.getSemester().getDeadLine();
		deadLine.setTime(deadLine.getTime()+(proposal.getSemester().getDeadLineDelay()*1000));
		return currentDate.after(deadLine);
	}

	public boolean hasUnderReviewStatus(Proposal proposal) {
		Iterator iterator = proposal.getStatusHistory().iterator();
		while (iterator.hasNext()) {
			ProposalStatus proposalStatus = (ProposalStatus) iterator.next();
			String status = proposalStatus.getStatus().getCode();
			if (status.equalsIgnoreCase(Constants.STATUS_UNDER_REVIEW)) {
				return true;
			}
		}
		return false;
	}

	public Semester getNextSemester(Proposal proposal) throws DatabaseException {
		List semesters = repository.getNextSemesters(proposal.getSemester(),
				getCurrentDate());
		if (semesters.size() == 1) {
			return (Semester) semesters.get(0);
		} else if (semesters.size() == 0) {
			return null;
		} else {
			return (Semester) semesters.get(0);
		}
	}

	public boolean isDeprecated(Proposal proposal) {
		Date currentDate = getCurrentDate().getTime();
		TelescopeConfiguration telecopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		if (telecopeConfiguration.getEndTime() != null) {
			return currentDate.after(telecopeConfiguration.getEndTime());
		} else {
			return false;
		}
	}

	/**
	 * isCategoryUrgent is a helper function that returns true if the category
	 * of the proposal is 'urgent'
	 * 
	 * @param proposal
	 * @return true/false
	 * @throws DatabaseException
	 * @author Nico Vermaas
	 * @since 22 feb 2005
	 */
	/*
	 * public boolean isCategoryUrgent(Proposal proposal) { return
	 * proposal.getSemester().getCategory().getCode().equalsIgnoreCase(
	 * Constants.CATEGORY_URGENT); }
	 */
	public boolean isImmediate(Proposal proposal) {
		return proposal.getSemester().isImmediate();
	}

	/*
	 * public boolean isCategoryRegular(Proposal proposal) { return
	 * proposal.getSemester().getCategory().getCode().equalsIgnoreCase(
	 * Constants.CATEGORY_REGULAR); }
	 */

	public User getContactAuthorAsUser(Proposal proposal)
			throws DatabaseException {
		User user = null;
		RegisteredMember ca = (RegisteredMember) proposal.getContactAuthor();
		if (ca.getUser() == null) {
			user = userManagement.getUser(ca.getUserId());
		} else {
			user = ca.getUser();
		}
		return user;
	}

	public void logStatusChange(Proposal proposal, UserAccount userAccount,
			Status currentStatus, Status nextStatus) {
		if (log.isInfoEnabled()) {
			String logString = "Status of proposal ";
			if (!AstronValidator.isBlankOrNull(proposal.getCode())) {
				logString += "with code: " + proposal.getCode() + " ";
			}
			logString += "changed by " + userAccount.getUsername() + " from "
					+ currentStatus.getCode() + " to " + nextStatus.getCode();
			log.info(logString);
		}
	}

	public List getAvailableSemesters(String telescope, String community)
			throws DatabaseException {
		return repository.getAvailableSemesters(telescope, community,
				getCurrentDate());
	}

	public List getAvailableSemesters(String telescope, String community,
			String category) throws DatabaseException {
		return repository.getAvailableSemesters(telescope, community, category,
				getCurrentDate());
	}

	public Semester getAvailableSemester(String telescope, String community,
			String semester, String category) throws DatabaseException {
		return repository.getAvailableSemester(telescope, community, semester,
				category, getCurrentDate());
	}

	public Semester getSemester(String telescope, String community,
			String semester, String category) throws DatabaseException {
		return repository.getSemester(telescope, community, semester, category,
				getCurrentDate());
	}

	public boolean semesterAlreadyExist(String telescope, String community,
			String semester, String category) throws DatabaseException {
		return repository.getSemester(telescope, community, semester, category) != null;

	}

	public static void setResourceValue(Map allocations, String key,
			Double resourceValue, ResourceType resourceType) {
		if (resourceValue != null) {
			Resource resource = (Resource) allocations.get(key);
			if (resource == null) {
				resource = new Resource();
				resource.setType(resourceType);
			}
			resource.setValue(resourceValue);
			allocations.put(key, resource);
		} else {
			allocations.remove(key);
		}
	}
	
	public static List getUserAccountSummaries(boolean sort){
		
		try {
			return userManagement.getUserAccountSummaries(sort);
		} catch (DatabaseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static Double getResourceValue(Map allocations, String key) {
		Resource resource = (Resource) allocations.get(key);
		if (resource == null) {
			return null;
		}
		return resource.getValue();
	}
	
	public List getUsersTelescope(String Telescope)throws DatabaseException{
		return repository.getUsersTelescope(Telescope);
		//getUsersTelescope(Telescope);
	}
	
	public List getUsersNotTelescope(String Telescope)throws DatabaseException{
		List admins = userManagement.getAdmins();
		
		if (admins != null && Telescope != null && Telescope != ""){
			return repository.getUsersNotTelescope(Telescope,admins);
		}
		else {
			return null;
		}
			
		//getUsersTelescope(Telescope);
	}
	
	public List getTelescopeAccessRights(Integer userId)throws DatabaseException{
		return repository.getTelescopeAccessRights(userId);
	}
	
	public void deleteUserTelescope(UserTelescope userTelescope)
	throws DatabaseException {
		repository.delete(userTelescope);
	}
	
	public void addUserTelescope(UserTelescope userTelescope)throws DatabaseException{
		repository.store(userTelescope);
	}

	public List getMembersNotProposal() throws DatabaseException {
		List members = repository.getMembersNotProposal();
		return members;
	}

	public List getParentList(Semester semester) {
		List results = new ArrayList();
		if(semester.getParentId() != null){
			AdminProposalSummary proposalSummary = new AdminProposalSummary();
			proposalSummary.setTitle("- None Specified -");
			results.add(proposalSummary);
//			List summaries = repository.getProposalsSummaries(semester.getParentId());
			String[] statusses = {"accepted"};
			List summaries = repository.getProposalsSummaries(statusses, semester.getParentId());
			Iterator iterator = summaries.iterator();
			while (iterator.hasNext()) {
				Object[] object = (Object[]) iterator.next();
				proposalSummary = new AdminProposalSummary();
				proposalSummary.setId((Integer) object[0]);
				proposalSummary.setCode((String) object[1]);
				proposalSummary.setTitle((String) object[2]);
				
				//proposalSummary.setSemester((Semester) object[3]);
				
				results.add(proposalSummary);
			}
			
		}
		
		return results;
	}

	public List getChildsList(Proposal proposal) {
		List results = new ArrayList();
		
		
		if(proposal.getId() != null){
			List summaries = repository.getProposalChildSummaries(proposal.getId());
			Iterator iterator = summaries.iterator();
			while (iterator.hasNext()) {
				Object[] object = (Object[]) iterator.next();
				AdminProposalSummary proposalSummary = new AdminProposalSummary();
				proposalSummary.setId((Integer) object[0]);
				proposalSummary.setCode((String) object[1]);
				String title = (String) object[2];
				if (title.length() > 80){
					title=title.substring(0, 77);
					title=title+"...";
				}
				proposalSummary.setTitle(title);
				//proposalSummary.setSemester((Integer) object[3]);
				
				results.add(proposalSummary);
			}
		}
		return results;
	}

	public List<AdminProposalSummary> getEnvelopeSheetProposalSummaries(UserAccount ownUserAccount) {
		List<AdminProposalSummary> results = new ArrayList();
		List summaries = repository.getEnvelopeSheetProposalSummaries(ownUserAccount.getUser().getId());
		Iterator iterator = summaries.iterator();
		while (iterator.hasNext()) {
			Object[] object = (Object[]) iterator.next();
			AdminProposalSummary proposalSummary = new AdminProposalSummary();
			proposalSummary.setId((Integer) object[0]);
			proposalSummary.setCode((String) object[1]);
			String title = (String) object[2];
			if (title!=null && title.length() > 40){
				title=title.substring(0, 37);
				title=title+"...";
			}
			String code = "NOT_SUBMITTED";
			if(proposalSummary.getCode() != null){
				code =proposalSummary.getCode() +": ";
			}
			proposalSummary.setTitle(code + title);
			//proposalSummary.setSemester((Integer) object[3]);
			
			results.add(proposalSummary);
		}
		
		return results;
	}

	public byte[] getEnvelopeFile(UserAccount ownUserAccount, String title){
		return(this.getEnvelopeFile(ownUserAccount.getUser().getId(),title));
	}
	
	public byte[] getEnvelopeFile(Integer userId, String title){
		if(AstronValidator.isPositiveInt(title)){
			// id is proposal code
			Integer justificationId = repository.getJustificationId(Integer.valueOf(title));
			ScientificFile file = repository.getScientificFile(justificationId );
			if(file !=null)
			return file.getUploadFile();
		}else{
			List summaries = repository.getEnvelopeSheetProposalSummaries(userId);
			Integer proposalid = null;
			Iterator iterator = summaries.iterator();
			while (iterator.hasNext()) {
				Object[] object = (Object[]) iterator.next();
				String code = (String) object[1];
				//log.warn("comapring: " + code + " with "+ title);
				if(code != null && code.length() > 1) {
					if(title.startsWith((String) object[1])){
						proposalid = (Integer) object[0];
					}
				} 
			}
			if(proposalid != null){
				Integer justificationId = repository.getJustificationId(proposalid);
				ScientificFile file = repository.getScientificFile(justificationId);
				if(file !=null)
				return file.getUploadFile();
			}
		}
		//Integer justificationId = 
		//repository.getEnvelopeSheetJustificationId(ownUserAccount.getUser().getId(),title);
		
		return null;
		//return repository.getEnvelopeSheetFile(ownUserAccount.getUser().getId(),title);
	}
	
}
