<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />
<html:form action="/processDeleteThesis" method="POST">

<html:hidden property="thesisId"/>
<table width="100%">
	<TR>
		<TD width="*">&nbsp;</TD>
		<TD class="help" width="60">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="60" height="25">
							<TR>
					  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#thesis\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
				  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#thesis\" target=\"help\">Help</a>"%></TD>
			  				</TR>
			  			</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
					</TD>
	</TR>
	</table>
<table>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="right">Student:&nbsp;</td>
		<td class="bold" align="left"><bean:write name="thesisForm" property="studentName"/></td>
	</tr>
	<tr>
		<td class="right">Supervisor:&nbsp;</td>
		<td class="bold" align="left"><bean:write name="thesisForm" property="supervisorName"/></td>
	</tr>
	<tr>
		<td class="right">Completion date:&nbsp;</td>
		<td  class="bold" align="left"><bean:write name="thesisForm" property="expectedCompletionDate"/></td>
	</tr>
	<tr>
		<td class="right">Data required:&nbsp;</td>
		<td  class="bold" align="left"><bean:write name="thesisForm" property="dataRequired"/></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>		
	<tr>	
		<TD colspan="2">
		<tiles:get name="box_header" />
			<TABLE width="100%">
				<TR>
					<TD>
Are you sure you want to remove this thesis from the proposal?<br>
					</TD>
				</TR>
 			</TABLE>
		<tiles:get name="box_footer" />
		</TD>
	</TR>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>		
</table>
<table width="100%">
	<tr>
		<td align="left"><html:submit property="addThesisButton"  tabindex="5" styleClass="list_accept_yes">Ok</html:submit>
 		</td>
		<td align="right"><html:cancel styleClass="list_decline_t">Cancel</html:cancel></TD>
	</tr>
</table>

</html:form>