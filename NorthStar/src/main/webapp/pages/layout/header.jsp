<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<TABLE width="100%">

<TR>
	<TD class="center"><img src="<%=request.getContextPath()%><bean:write name="proposaldata" property="headerImageUri"/>" /></TD>
	<TD class="help">
		<TABLE>
			<TR>
				<TD>
				<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="100" height="25">
							<TR>
					  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
				  				<TD width="80" height="25"><%= "<a title=\"Opens the help index\" href=\"" + request.getContextPath() + "/proposalHelp.do\" target=\"help\">Help Index</a>"%></TD>
			  				</TR>  				
			  			</TABLE>
				<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />	
				</TD>
			</TR>
			<TR>
				<TD>
				<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="100" height="25">
							<TR>
								<TD class="right"><astron:label key="label.header.community"/></TD>	
								<TD class="bold" align="left" >
									<bean:write  name="proposaldata" property="proposalBean.community" />
								</TD>			  				
							</TR> 
							<TR>
								<TD class="right"><astron:label key="label.header.category"/></TD>	
								<TD class="bold" align="left" >
									<bean:write  name="proposaldata" property="proposalBean.category" />
								</TD>			  				
							</TR> 								
							<TR>
								<TD class="right"><astron:label key="label.header.semester"/></TD>	
								<TD class="bold" align="left" >
									<bean:write  name="proposaldata" property="proposalBean.semester" />
								</TD>			  				
							</TR> 							 				
			  			</TABLE>
				<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />	
				</TD>
			</TR>			
		</TABLE>
	</TD>
</TR>

<TR>
	<TD colspan="2">
	<div class="aTabMenu">
	<ul>
		<logic:equal name="currentaction" value="/setUpApplicants.do">
			<li id="current"><html:link href="javascript:submitforward('applicants')"><span>Applicants</span></html:link></li>
		</logic:equal>
		<logic:notEqual name="currentaction" value="/setUpApplicants.do">
			<li><html:link href="javascript:submitforward('applicants')"><span>Applicants</span></html:link></li>
		</logic:notEqual>

		<logic:equal name="currentaction" value="/setUpJustification.do">
			<li id="current"><html:link href="javascript:submitforward('justification')"><span>Justification</span></html:link></li>
		</logic:equal>
		<logic:notEqual name="currentaction" value="/setUpJustification.do">
			<li><html:link href="javascript:submitforward('justification')"><span>Justification</span></html:link></li>
		</logic:notEqual>
		<logic:equal name="currentaction" value="/setUpObservingRequest.do">
			<li id="current"><html:link href="javascript:submitforward('observingRequest')"><span>Observing Request</span></html:link></li>
		</logic:equal>
		<logic:notEqual name="currentaction" value="/setUpObservingRequest.do">
			<li><html:link href="javascript:submitforward('observingRequest')"><span>Observing Request</span></html:link></li>
		</logic:notEqual>
		<logic:equal name="targetsOnTabbedPage" value="true">
			<logic:equal name="currentaction" value="/setUpObservation.do">
				<li id="current"><html:link href="javascript:submitforward('observation')"><span>Target List</span></html:link></li>
			</logic:equal>
			<logic:equal name="currentaction" value="/opticon/setUpObservation.do">
				<li id="current"><html:link href="javascript:submitforward('observation')"><span>Target List</span></html:link></li>
			</logic:equal>
			<logic:notEqual name="currentaction" value="/setUpObservation.do">
				<logic:notEqual name="currentaction" value="/opticon/setUpObservation.do">
					<li><html:link href="javascript:submitforward('observation')"><span>Target List</span></html:link></li>
				</logic:notEqual>	
			</logic:notEqual>
			
		</logic:equal>
		
		
		<logic:equal name="currentaction" value="/setUpAdditionalIssues.do">
			<li id="current"><html:link href="javascript:submitforward('additionalIssues')"><span>Additional information</span></html:link></li>
		</logic:equal>
		<logic:notEqual name="currentaction" value="/setUpAdditionalIssues.do">
			<li><html:link href="javascript:submitforward('additionalIssues')"><span>Additional information</span></html:link></li>
		</logic:notEqual>

	</ul>
	</div>
	</TD>
</TR>
</TABLE>
