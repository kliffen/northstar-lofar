// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.data.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class OpticonInstrument implements Serializable {
	protected Integer id = null;
	protected String name = null;
	protected List filters = new ArrayList();
	protected List filtersw2 = new ArrayList();
	protected List grismw = new ArrayList();
	protected List slitw = new ArrayList();
	protected List gratingw = new ArrayList();
	protected String filterDetails = null;
	protected String readOut = null;
	protected String mode = null;
	protected String CCD = null;
	protected String orderFilter = null;
	
	protected boolean polarimetry = false;
	protected boolean guideline = false;
	protected String wavelength = null;
	protected Integer order = null;
	protected String slit = null;
	protected Double slitPositionAngle = null;
	protected String grating = null;
	protected String grism = null;
	
	protected String comments = null;
	protected boolean nonDefaultFrameExposureTime = false;
	protected String nonDefaultFrameExposureTimeDetails = null;
	protected Double frameExposureTime = null;
	protected String microstepping = null;
	protected boolean coronagraphicMask = false;
	protected OpticonObservation observation = null;
	protected String telescopeScheduling = null;
	
	// for lofar
	protected String station = null;
	protected String singleStationSpecifics = null;
	protected String customStationSpecifics = null;
	protected Double filterFrequency = null;
	protected Double filterBandwidth = null;
	protected boolean filterContiguousCoverage = true;
	protected String clock = null;
	protected String oneFilter = null;
	protected String antenna = null;
	protected Integer subarrayPointings = null;
	protected Double integrationTime = null;
	protected boolean averaging = false;
	protected boolean flagging = false;
	protected boolean calibration = false;
	protected boolean imaging = false;
	protected boolean summaryPlots = false;
	protected String polarization = null;
	protected String incPolarization = null;
	protected boolean storeRawData = false;
	protected boolean storeUVData = false;
	protected Integer fieldSizeX = null;
	protected Integer pixelSizeX = null;
	protected Integer fieldSizeY = null;
	protected Integer pixelSizeY = null;
	protected boolean piggyBack = false;
	protected String piggyBackSpecifics = null;
	protected boolean directDataStorage = false;
	
	protected Integer frequencyChannels = null;
	protected Integer beams = null;
	protected boolean cohstokes = false;
	protected boolean incohstokes = false;
	protected boolean complexVoltage = false;
	protected boolean flysEye = false;
	protected boolean rawVoltage = false;
	protected Integer channels = null;
	protected Integer incchannels = null;
	protected Integer intsteps = null;
	protected Integer  incohIntsteps = null;
	protected Integer events = null;
	protected Integer samplerate = null;
	
	// lofar
	protected String processingMode = null;
	protected Integer subbandsPerImage = null;
	protected Double fieldOfView = null;
	protected Double averagingFrequency = null;
	protected Double averagingTime = null;
	protected boolean demixing = false;
	protected Double demixingFrequency = null;
	protected Double demixingTime = null;
	protected String flaggingStrategy = null;
	protected List<String> demixingSources = new ArrayList();
	protected boolean correlatedVisibilities = false;
	protected Integer cohTaBeams = null;
	protected Integer inCohTaBeams = null;
	protected Integer rings = null;

	protected boolean beamConf;
	protected boolean interferoConf;
	protected boolean tbbConf;
	protected boolean otherConf;
	protected List <String> selectedInstConfs = new ArrayList<String>();
	protected List <String> deSelectedInstConfs = new ArrayList<String>();
	/***
	 * fields for TBB
	 */
    protected Double tbbRequestedMinimumTime = null;
	protected Double tbbRequestedSATime = null;
	protected Double tbbTriggerLength = null;
	protected Double tbbTriggerRate = null;
	protected String tbbSelectedObservationId = null;
	protected String tbbTriggerSourceId=null;
	protected Double tbbExposureTime =null;
	/**
	 * fields for pulsar pipeline parameters.
	 */
	protected boolean skipRFI = false;
	protected boolean skipFolding = false;
	protected boolean skipPdmp = false;
	protected boolean skipDspsr = false;
	protected boolean skipPrepfold = false;
	protected boolean singlePulse = false;
	protected boolean rratsAnalysis = false;
	protected boolean skipDynamicAverage = false;
	protected Double subintegrationLength = null;
	protected boolean convertRawData = false;
	protected Double threshold = new Double(5);
	protected Double sigmaLimit = new Double(3);
	protected Double numberOfBlocks = new Double(100);
	protected String prepfoldOptions = null;
	protected String presubbandOptions = null;
	protected String rfifind = null;protected String dspsrOptions = null;
	protected String digifilOptions = null;
	protected String predataOptions = null;
	protected String bf2fitsOptions = null;
	protected String pulsarDesc = null;
	
	public OpticonInstrument() {
		
	}
	
	public OpticonInstrument(String name) {
		this.name =name;
	}
	public Integer getCohTaBeams() {
		return cohTaBeams;
	}
	public void setCohTaBeams(Integer cohTaBeams) {
		this.cohTaBeams = cohTaBeams;
	}
	public Integer getInCohTaBeams() {
		return inCohTaBeams;
	}
	public void setInCohTaBeams(Integer inCohTaBeams) {
		this.inCohTaBeams = inCohTaBeams;
	}
	public Integer getRings() {
		return rings;
	}
	public void setRings(Integer rings) {
		this.rings = rings;
	}

	
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public boolean isCoronagraphicMask() {
		return coronagraphicMask;
	}
	public void setCoronagraphicMask(boolean coronagraphicMask) {
		this.coronagraphicMask = coronagraphicMask;
	}
	public Double getFrameExposureTime() {
		return frameExposureTime;
	}
	public void setFrameExposureTime(Double frameExposureTime) {
		this.frameExposureTime = frameExposureTime;
	}
	public String getGrating() {
		return grating;
	}
	public void setGrating(String grating) {
		this.grating = grating;
	}
	public String getGrism() {
		return grism;
	}
	public void setGrism(String grism) {
		this.grism = grism;
	}
	public String getMicrostepping() {
		return microstepping;
	}
	public void setMicrostepping(String microstepping) {
		this.microstepping = microstepping;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Integer getOrder() {
		return order;
	}
	public void setOrder(Integer order) {
		this.order = order;
	}
	public boolean isPolarimetry() {
		return polarimetry;
	}
	public void setPolarimetry(boolean polarimetry) {
		this.polarimetry = polarimetry;
	}
	public boolean isGuideline() {
		return guideline;
	}
	public void setGuideline(boolean guideline) {
		this.guideline = guideline;
	}
	public String getSlit() {
		return slit;
	}
	public void setSlit(String slitWidth) {
		this.slit = slitWidth;
	}
	public boolean isNonDefaultFrameExposureTime()
	{
		return nonDefaultFrameExposureTime;
	}
	public void setNonDefaultFrameExposureTime(boolean nonDefaultFrameExposureTime)
	{
		this.nonDefaultFrameExposureTime = nonDefaultFrameExposureTime;
	}
	public String getNonDefaultFrameExposureTimeDetails()
	{
		return nonDefaultFrameExposureTimeDetails;
	}
	public void setNonDefaultFrameExposureTimeDetails(
			String nonDefaultFrameExposureTimeDetails)
	{
		this.nonDefaultFrameExposureTimeDetails = nonDefaultFrameExposureTimeDetails;
	}
	public String getWavelength() {
		return wavelength;
	}
	public void setWavelength(String wavelength) {
		this.wavelength = wavelength;
	}
	public OpticonObservation getObservation() {
		return observation;
	}
	public void setObservation(OpticonObservation observation) {
		this.observation = observation;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getSlitPositionAngle() {
		return slitPositionAngle;
	}
	public void setSlitPositionAngle(Double slitPositionAngle) {
		this.slitPositionAngle = slitPositionAngle;
	}

	public String getMode()
	{
		return mode;
	}
	public void setMode(String mode)
	{
		this.mode = mode;
	}
	public String getReadOut()
	{
		return readOut;
	}
	public void setReadOut(String secondFilter)
	{
		this.readOut = secondFilter;
	}
	public String getOrderFilter()
	{
		return orderFilter;
	}
	public void setOrderFilter(String orderFilter)
	{
		this.orderFilter = orderFilter;
	}
	public String getFilterDetails()
	{
		return filterDetails;
	}
	public void setFilterDetails(String filterDetails)
	{
		this.filterDetails = filterDetails;
	}
	public List getFilters()
	{
		return filters;
	}
	public void setFilters(List filters)
	{
		this.filters = filters;
	}
	public String getStation() {
		return station;
	}
	public void setStation(String station) {
		this.station = station;
	}
	public String getClock() {
		return clock;
	}
	public void setClock(String clock) {
		this.clock = clock;
	}
	public String getAntenna() {
		return antenna;
	}
	public void setAntenna(String antenna) {
		this.antenna = antenna;
	}
	public final Integer getSubarrayPointings() {
		return subarrayPointings;
	}
	public final void setSubarrayPointings(Integer subarrayPointings) {
		this.subarrayPointings = subarrayPointings;
	}
	public final boolean isComplexVoltage() {
		return complexVoltage;
	}
	public final void setComplexVoltage(boolean complexVoltage) {
		this.complexVoltage = complexVoltage;
	}
	public boolean isAveraging() {
		return averaging;
	}
	public void setAveraging(boolean averaging) {
		this.averaging = averaging;
	}
	public boolean isFlagging() {
		return flagging;
	}
	public void setFlagging(boolean flagging) {
		this.flagging = flagging;
	}
	public boolean isCalibration() {
		return calibration;
	}
	public void setCalibration(boolean calibration) {
		this.calibration = calibration;
	}
	public Double getIntegrationTime() {
		return integrationTime;
	}
	public void setIntegrationTime(Double integrationTime) {
		this.integrationTime = integrationTime;
	}
	public boolean isImaging() {
		return imaging;
	}
	public void setImaging(boolean imaging) {
		this.imaging = imaging;
	}
	public String getCustomStationSpecifics() {
		return customStationSpecifics;
	}
	public void setCustomStationSpecifics(String customStationSpecifics) {
		this.customStationSpecifics = customStationSpecifics;
	}
	public String getOneFilter() {
		return oneFilter;
	}
	public void setOneFilter(String oneFilter) {
		this.oneFilter = oneFilter;
	}
	public void setFieldSizeX(Integer fieldSizeX) {
		this.fieldSizeX = fieldSizeX;
	}
	public void setPixelSizeX(Integer pixelSizeX) {
		this.pixelSizeX = pixelSizeX;
	}
	public void setFieldSizeY(Integer fieldSizeY) {
		this.fieldSizeY = fieldSizeY;
	}
	public void setPixelSizeY(Integer pixelSizeY) {
		this.pixelSizeY = pixelSizeY;
	}
	public Integer getFieldSizeX() {
		return fieldSizeX;
	}
	public Integer getPixelSizeX() {
		return pixelSizeX;
	}
	public Integer getFieldSizeY() {
		return fieldSizeY;
	}
	public Integer getPixelSizeY() {
		return pixelSizeY;
	}
	public void setAveragingFrequency(Double averagingFrequency) {
		this.averagingFrequency = averagingFrequency;
	}
	public void setAveragingTime(Double averagingTime) {
		this.averagingTime = averagingTime;
	}
	
	public Double getAveragingFrequency() {
		return averagingFrequency;
	}
	public Double getAveragingTime() {
		return averagingTime;
	}
	public String getSingleStationSpecifics() {
		return singleStationSpecifics;
	}
	public void setSingleStationSpecifics(String singleStationSpecifics) {
		this.singleStationSpecifics = singleStationSpecifics;
	}
	public Double getFilterFrequency() {
		return filterFrequency;
	}
	public void setFilterFrequency(Double filterFrequency) {
		this.filterFrequency = filterFrequency;
	}
	public Double getFilterBandwidth() {
		return filterBandwidth;
	}
	public void setFilterBandwidth(Double filterBandwidth) {
		this.filterBandwidth = filterBandwidth;
	}
	public boolean isFilterContiguousCoverage() {
		return filterContiguousCoverage;
	}
	public void setFilterContiguousCoverage(boolean filterContiguousCoverage) {
		this.filterContiguousCoverage = filterContiguousCoverage;
	}
	public boolean isPiggyBack() {
		return piggyBack;
	}
	public void setPiggyBack(boolean piggyBack) {
		this.piggyBack = piggyBack;
	}
	public String getPiggyBackSpecifics() {
		return piggyBackSpecifics;
	}
	public void setPiggyBackSpecifics(String piggyBackSpecifics) {
		this.piggyBackSpecifics = piggyBackSpecifics;
	}
	public boolean isDirectDataStorage() {
		return directDataStorage;
	}
	public void setDirectDataStorage(boolean directDataStorage) {
		this.directDataStorage = directDataStorage;
	}
	public Integer getFrequencyChannels() {
		return frequencyChannels;
	}
	public void setFrequencyChannels(Integer frequencyChannels) {
		this.frequencyChannels = frequencyChannels;
	}
	public Integer getBeams() {
		return beams;
	}
	public void setBeams(Integer beams) {
		this.beams = beams;
	}
	public Integer getEvents() {
		return events;
	}
	public void setEvents(Integer events) {
		this.events = events;
	}
	public Integer getSamplerate() {
		return samplerate;
	}
	public void setSamplerate(Integer samplerate) {
		this.samplerate = samplerate;
	}
	public String getTelescopeScheduling() {
		return telescopeScheduling;
	}
	public void setTelescopeScheduling(String telescopeScheduling) {
		this.telescopeScheduling = telescopeScheduling;
	}
	public List getFiltersw2() {
		return filtersw2;
	}
	public void setFiltersw2(List filtersw2) {
		this.filtersw2 = filtersw2;
	}
	public List getGrismw() {
		return grismw;
	}
	public void setGrismw(List grismw) {
		this.grismw = grismw;
	}
	public List getSlitw() {
		return slitw;
	}
	public void setSlitw(List slitw) {
		this.slitw = slitw;
	}
	public List getGratingw() {
		return gratingw;
	}
	public void setGratingw(List gratingw) {
		this.gratingw = gratingw;
	}
	public String getCCD() {
		return CCD;
	}
	public void setCCD(String cCD) {
		CCD = cCD;
	}
	public boolean isCohstokes() {
		return cohstokes;
	}
	public void setCohstokes(boolean cohstokes) {
		this.cohstokes = cohstokes;
	}
	public boolean isIncohstokes() {
		return incohstokes;
	}
	public void setIncohstokes(boolean incohstokes) {
		this.incohstokes = incohstokes;
	}
	public final boolean isFlysEye() {
		return flysEye;
	}
	public final void setFlysEye(boolean flysEye) {
		this.flysEye = flysEye;
	}
	public final boolean isRawVoltage() {
		return rawVoltage;
	}
	public final void setRawVoltage(boolean rawVoltage) {
		this.rawVoltage = rawVoltage;
	}
	public Integer getChannels() {
		return channels;
	}
	public void setChannels(Integer channels) {
		this.channels = channels;
	}
	public Integer getIntsteps() {
		return intsteps;
	}
	public void setIntsteps(Integer intsteps) {
		this.intsteps = intsteps;
	}
	public String getPolarization() {
		return polarization;
	}
	public void setPolarization(String polarization) {
		this.polarization = polarization;
	}
	public String getIncPolarization() {
		return incPolarization;
	}
	public void setIncPolarization(String incPolarization) {
		this.incPolarization = incPolarization;
	}
	public final boolean isStoreRawData() {
		return storeRawData;
	}
	public final void setStoreRawData(boolean storeRawData) {
		this.storeRawData = storeRawData;
	}
	public boolean isSummaryPlots() {
		return summaryPlots;
	}
	public void setSummaryPlots(boolean summaryPlots) {
		this.summaryPlots = summaryPlots;
	}
	public String getProcessingMode() {
		return processingMode;
	}
	public void setProcessingMode(String processingMode) {
		this.processingMode = processingMode;
	}
	public Integer getSubbandsPerImage() {
		return subbandsPerImage;
	}
	public void setSubbandsPerImage(Integer subbandsPerImage) {
		this.subbandsPerImage = subbandsPerImage;
	}
	public Double getFieldOfView() {
		return fieldOfView;
	}
	public void setFieldOfView(Double fieldOfView) {
		this.fieldOfView = fieldOfView;
	}
	public Double getDemixingFrequency() {
		return demixingFrequency;
	}
	public void setDemixingFrequency(Double demixingFrequency) {
		this.demixingFrequency = demixingFrequency;
	}
	public Double getDemixingTime() {
		return demixingTime;
	}
	public void setDemixingTime(Double demixingTime) {
		this.demixingTime = demixingTime;
	}
	public String getFlaggingStrategy() {
		return flaggingStrategy;
	}
	public void setFlaggingStrategy(String flaggingStrategy) {
		this.flaggingStrategy = flaggingStrategy;
	}
	public boolean isDemixing() {
		return demixing;
	}
	public void setDemixing(boolean demixing) {
		this.demixing = demixing;
	}
	public List<String> getDemixingSources() {
		return demixingSources;
	}
	public void setDemixingSources(List demixingSources) {
		this.demixingSources = demixingSources;
	}
	public boolean isCorrelatedVisibilities() {
		return correlatedVisibilities;
	}
	public void setCorrelatedVisibilities(boolean correlatedVisibilities) {
		this.correlatedVisibilities = correlatedVisibilities;
	}
	public boolean isStoreUVData() {
		return storeUVData;
	}
	public void setStoreUVData(boolean storeUVData) {
		this.storeUVData = storeUVData;
	}
	
	
	public Double getTbbRequestedMinimumTime() {
		return tbbRequestedMinimumTime;
	}
	public void setTbbRequestedMinimumTime(Double tbbRequestedMinimumTime) {
		this.tbbRequestedMinimumTime = tbbRequestedMinimumTime;
	}
	public Double getTbbRequestedSATime() {
		return tbbRequestedSATime;
	}
	public void setTbbRequestedSATime(Double tbbRequestedSATime) {
		this.tbbRequestedSATime = tbbRequestedSATime;
	}
	
	public Double getTbbTriggerLength() {
		return tbbTriggerLength;
	}
	public void setTbbTriggerLength(Double tbbTriggerLength) {
		this.tbbTriggerLength = tbbTriggerLength;
	}
	
	public String getTbbSelectedObservationId() {
		return tbbSelectedObservationId;
	}
	public void setTbbSelectedObservationId(String tbbSelectedObservationId) {
		this.tbbSelectedObservationId = tbbSelectedObservationId;
	}
	public String getTbbTriggerSourceId() {
		return tbbTriggerSourceId;
	}
	public void setTbbTriggerSourceId(String tbbTriggerSourceId) {
		this.tbbTriggerSourceId = tbbTriggerSourceId;
	}
	public Double getTbbExposureTime() {
		return tbbExposureTime;
	}
	public void setTbbExposureTime(Double tbbExposureTime) {
		this.tbbExposureTime = tbbExposureTime;
	}
	public Double getTbbTriggerRate() {
		return tbbTriggerRate;
	}
	public void setTbbTriggerRate(Double tbbTriggerRate) {
		this.tbbTriggerRate = tbbTriggerRate;
	}




	public boolean isBeamConf() {
		return beamConf;
	}


	public boolean isInterferoConf() {
		return interferoConf;
	}


	public boolean isTbbConf() {
		return tbbConf;
	}


	public boolean isOtherConf() {
		return otherConf;
	}


	public void setBeamConf(boolean beamConf) {
		this.beamConf = beamConf;
	}


	public void setInterferoConf(boolean interferoConf) {
		this.interferoConf = interferoConf;
	}


	public void setTbbConf(boolean tbbConf) {
		this.tbbConf = tbbConf;
	}


	public void setOtherConf(boolean otherConf) {
		this.otherConf = otherConf;
	}
	public List<String> getSelectedInstConfs() {
		return selectedInstConfs;
	}
	public void setSelectedInstConfs(List<String> selectedInstConfs) {
		this.selectedInstConfs = selectedInstConfs;
	}

	public List<String> getDeSelectedInstConfs() {
		return deSelectedInstConfs;
	}

	public void setDeSelectedInstConfs(List<String> deSelectedInstConfs) {
		this.deSelectedInstConfs = deSelectedInstConfs;
	}

	public Integer getIncchannels() {
		return incchannels;
	}

	public Integer getIncohIntsteps() {
		return incohIntsteps;
	}

	public void setIncchannels(Integer incchannels) {
		this.incchannels = incchannels;
	}

	public void setIncohIntsteps(Integer incohIntsteps) {
		this.incohIntsteps = incohIntsteps;
	}
	
	public boolean isSkipRFI() {
		return skipRFI;
	}

	public void setSkipRFI(boolean skipRFI) {
		this.skipRFI = skipRFI;
	}

	public boolean isSkipFolding() {
		return skipFolding;
	}

	public void setSkipFolding(boolean skipFolding) {
		this.skipFolding = skipFolding;
	}

	public boolean isSkipPdmp() {
		return skipPdmp;
	}

	public void setSkipPdmp(boolean skipPdmp) {
		this.skipPdmp = skipPdmp;
	}

	public boolean isSkipDspsr() {
		return skipDspsr;
	}

	public void setSkipDspsr(boolean skipDspsr) {
		this.skipDspsr = skipDspsr;
	}

	public boolean isSkipPrepfold() {
		return skipPrepfold;
	}

	public void setSkipPrepfold(boolean skipPrepfold) {
		this.skipPrepfold = skipPrepfold;
	}

	public boolean isSinglePulse() {
		return singlePulse;
	}

	public void setSinglePulse(boolean singlePulse) {
		this.singlePulse = singlePulse;
	}

	public boolean isRratsAnalysis() {
		return rratsAnalysis;
	}

	public void setRratsAnalysis(boolean rratsAnalysis) {
		this.rratsAnalysis = rratsAnalysis;
	}
	
	public boolean isSkipDynamicAverage() {
		return skipDynamicAverage;
	}

	public void setSkipDynamicAverage(boolean skipDynamicAverage) {
		this.skipDynamicAverage = skipDynamicAverage;
	}

	public Double getSubintegrationLength() {
		return subintegrationLength;
	}

	public void setSubintegrationLength(Double subintegrationLength) {
		this.subintegrationLength = subintegrationLength;
	}

	public boolean isConvertRawData() {
		return convertRawData;
	}

	public void setConvertRawData(boolean convertRawData) {
		this.convertRawData = convertRawData;
	}

	public Double getThreshold() {
		return threshold;
	}

	public void setThreshold(Double threshold) {
		this.threshold = threshold;
	}

	public Double getSigmaLimit() {
		return sigmaLimit;
	}

	public void setSigmaLimit(Double sigmaLimit) {
		this.sigmaLimit = sigmaLimit;
	}

	public Double getNumberOfBlocks() {
		return numberOfBlocks;
	}

	public void setNumberOfBlocks(Double numberOfBlocks) {
		this.numberOfBlocks = numberOfBlocks;
	}

	public String getPrepfoldOptions() {
		return prepfoldOptions;
	}

	public void setPrepfoldOptions(String prepfoldOptions) {
		this.prepfoldOptions = prepfoldOptions;
	}

	public String getPresubbandOptions() {
		return presubbandOptions;
	}

	public void setPresubbandOptions(String presubbandOptions) {
		this.presubbandOptions = presubbandOptions;
	}

	public String getRfifind() {
		return rfifind;
	}

	public void setRfifind(String rfifind) {
		this.rfifind = rfifind;
	}

	public String getDspsrOptions() {
		return dspsrOptions;
	}

	public void setDspsrOptions(String dspsrOptions) {
		this.dspsrOptions = dspsrOptions;
	}

	public String getDigifilOptions() {
		return digifilOptions;
	}

	public void setDigifilOptions(String digifilOptions) {
		this.digifilOptions = digifilOptions;
	}

	public String getPredataOptions() {
		return predataOptions;
	}

	public void setPredataOptions(String predataOptions) {
		this.predataOptions = predataOptions;
	}

	public String getBf2fitsOptions() {
		return bf2fitsOptions;
	}

	public void setBf2fitsOptions(String bf2fitsOptions) {
		this.bf2fitsOptions = bf2fitsOptions;
	}
	
	public String getPulsarDesc() {
		return pulsarDesc;
	}

	public void setPulsarDesc(String pulsarDesc) {
		this.pulsarDesc = pulsarDesc;
	}
}
