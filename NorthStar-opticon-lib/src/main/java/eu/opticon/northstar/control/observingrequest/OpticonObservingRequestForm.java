// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import eu.radionet.northstar.control.proposal.observingrequest.ObservingRequestForm;
import nl.astron.util.AstronValidator;

/**
 * This class contains telescope specific form data of the observing
 * request tab
 */
public class OpticonObservingRequestForm extends ObservingRequestForm {
	
	
	// note: the instrument bean itself is included via request parameters
	protected String showInstrumentBean=null;
	protected List allocationBeans = new ArrayList();
	protected boolean largeProposal = false;
	protected boolean multipleTelescopes = false;
	
	protected boolean displayDark = false;
	protected boolean displayGrey = false;
	protected boolean displayBright = false;
	protected boolean displayGlobal = false;

	protected boolean displayNoiseLevel = false;
	protected String noiseLevel = null;
	protected boolean displayNightTime = false;
	protected boolean nightTime = false;
	
	protected boolean displayLongTermProposal = false;
	protected boolean longTermProposal = false;
    protected String longTermProposalSpecifics = null;
    protected boolean displayLargeProposal = false;
    protected boolean displayRequestedTime = false;
	protected boolean displayTotalTime = false;
    protected boolean displayUseResources = false;
    
   	protected String selectedObservingMode = null;
    protected List observingModes = new ArrayList();
	protected boolean displayObservingMode = false;
	protected boolean displayScienceCategory = false;
	protected List scienceCategories = new ArrayList();
    protected String category = null;
    
    protected String allocationJustification = null;
    protected boolean displayAllocationJustification = false;
    
    //protected String newObserverExperience = null;
    protected boolean displayNewObserverExperience = false;
    protected boolean requiredNewObserverExperience = false;
    protected String travel = null;
    protected boolean displayTravel = false;
    protected String otherExpenditure = null;
    protected boolean displayOtherExpenditure = false;
    protected boolean displayUsefulTime = false;
    protected boolean displayAwardedTime = false;
    protected boolean displayLongTermTime = false;
    protected boolean displayLongTermStorage = false;
    protected boolean displayProcessingTime =false;
    protected String specifiedStorage = null;
    protected String specifiedProcessingTime =null;
   
	protected boolean displayProprietaryException = false;
    protected boolean enableProprietaryException = false;
    protected String proprietaryExceptionSpecifics = null;
    protected boolean displayObservingDetails = false; 
    protected boolean displayAllocations = false;
    protected List observationAllocations = new ArrayList();
    protected boolean displayCepLoad = false;
    protected String specifiedCepLoad = null;

 // pipeline
    protected String[] pipelineSummaryLabels = new String[0];
    protected List pipelineSummaryValues = new ArrayList();
    // this is set to enable the commit pipeline button when a pipeline instument is selected
    protected boolean processingPipeline = false;
    protected boolean variableMaxPageNumber =false;
    
/*
     * the sum of the observation times specified by the proposers. This is not a field in 
     * the form but is calculated
     */
	protected String totalSpecifiedTime = null;
	
    /** 
     * Validates if the filled in fields are in the correct format. (No strings where numbers 
     * are expected etc.) The error messages can be found in 
     * eu.institute.northstar.skeleton.control.application.properties
     * @see org.apache.struts.action.ActionForm#validate(org.apache.struts.action.ActionMapping, javax.servlet.http.HttpServletRequest)
     */
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {

        ActionErrors errors = super.validate(mapping,request);
       
        Map<String, String[]> parameters = request.getParameterMap();
       
        /*for(String parameter : parameters.keySet()) {
        	System.out.println(parameter + " " + parameters.get(parameter)[0]);
        }*/
        if(parameters.get("cohstokes") != null && parameters.get("cohstokes")[0].equals("true")) {
        	if (	parameters.get("cohTaBeams") != null && 
        			parameters.get("cohTaBeams")[0].length() != 0 &&
        			!AstronValidator.isPositiveInt(parameters.get("cohTaBeams")[0])
        			) {
        		errors.add("cohTaBeams", new ActionMessage("error.noint.positive") );
        	}
        	if (	parameters.get("intsteps") != null &&
        			parameters.get("intsteps")[0].length() != 0 &&
        			//!AstronValidator.isPositiveInt(parameters.get("intsteps")[0]))
        			AstronValidator.isInt(parameters.get("intsteps")[0]) && new Integer(parameters.get("intsteps")[0]).intValue() <=0)
        			{
        		errors.add("intsteps", new ActionMessage("error.noint.positive") );
        	}
        	if (	parameters.get("rings") != null && 
        			parameters.get("rings")[0].length() != 0 &&
        			!AstronValidator.isPositiveInt(parameters.get("rings")[0])) {
        		errors.add("rings", new ActionMessage("error.noint.positive") );
        	}
        }
       
        if(parameters.get("incohstokes") != null && parameters.get("incohstokes")[0].equals("true")) {
        	if (	parameters.get("incohIntsteps") != null &&
        			parameters.get("incohIntsteps")[0].length() != 0 &&
        			AstronValidator.isInt(parameters.get("incohIntsteps")[0]) && new Integer(parameters.get("incohIntsteps")[0]).intValue() <=0)
        			{
        		errors.add("incohIntsteps", new ActionMessage("error.noint.positive") );
        	}
        }
        
        if(parameters.get("tbbConf") != null && parameters.get("tbbConf")[0].equals("on")) {
        	if (parameters.get("tbbTriggerLength") != null && parameters.get("tbbTriggerLength")[0].length() != 0 && !AstronValidator.isPositiveFloat(parameters.get("tbbTriggerLength")[0])) {
        		errors.add("tbbTriggerLength", new ActionMessage("error.nofloat.positive") );
        	}
        	if (parameters.get("tbbTriggerRate") != null && parameters.get("tbbTriggerRate")[0].length() != 0 && !AstronValidator.isPositiveFloat(parameters.get("tbbTriggerRate")[0])) {
        		errors.add("tbbTriggerRate", new ActionMessage("error.nofloat.positive") );
        	}
        }

        
        

        
        return errors;
        
    }

    public final String getNoiseLevel() {
		return noiseLevel;
	}

	public final void setNoiseLevel(String noiseLevel) {
		this.noiseLevel = noiseLevel;
	}

	public final boolean isNightTime() {
		return nightTime;
	}

	public final void setNightTime(boolean nightTime) {
		this.nightTime = nightTime;
	}

	public final boolean isDisplayNoiseLevel() {
		return displayNoiseLevel;
	}

	public final void setDisplayNoiseLevel(boolean displayNoiseLevel) {
		this.displayNoiseLevel = displayNoiseLevel;
	}

	public final boolean isDisplayNightTime() {
		return displayNightTime;
	}

	public final void setDisplayNightTime(boolean displayNightTime) {
		this.displayNightTime = displayNightTime;
	}

	public boolean isDisplayTotalTime() {
		return displayTotalTime;
	}
    
	public boolean isMultipleTelescopes() {
		return multipleTelescopes;
	}

	public void setMultipleTelescopes(boolean multipleTelescopes) {
		this.multipleTelescopes = multipleTelescopes;
	}

	public void setDisplayTotalTime(boolean displayTotalTime) {
		this.displayTotalTime = displayTotalTime;
	}
	

    public boolean isDisplayUseResources() {
		return displayUseResources;
	}

	public void setDisplayUseResources(boolean displayResources) {
		this.displayUseResources = displayResources;
	}

	/**
     * @return Returns the totalSpecifiedTime.
     */
    public String getTotalSpecifiedTime()
    {
        return totalSpecifiedTime;
    }

    /**
     * @param totalSpecifiedTime The totalSpecifiedTime to set.
     */
    public void setTotalSpecifiedTime(String totalSpecifiedTime)
    {
        this.totalSpecifiedTime = totalSpecifiedTime;
    }

    /**
     * @return Returns the category.
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category The category to set.
     */
    public void setCategory(String category) {
        this.category = category;
    }
    
    public List getScienceCategories() {
        return scienceCategories;
    }

    public void setScienceCategories(List categories) {
        this.scienceCategories = categories;
    }

    /**
     * @return Returns the longTermProposal.
     */
    public boolean isLongTermProposal() {
        return longTermProposal;
    }

    /**
     * @param longTermProposal The longTermProposal to set.
     */
    public void setLongTermProposal(boolean longTermProposal) {
        this.longTermProposal = longTermProposal;
    }

	public String getLongTermProposalSpecifics()
	{
		return longTermProposalSpecifics;
	}

	public void setLongTermProposalSpecifics(String longTermProposalSpecifics)
	{
		this.longTermProposalSpecifics = longTermProposalSpecifics;
	}

	public List getAllocationBeans()
	{
		return allocationBeans;
	}
	
	public void setAllocationBeans(List allocationBeans)
	{
		this.allocationBeans = allocationBeans;
	}

	public AllocationBean getAllocationBean(int index)
	{
		resizeAllocationBeans(index);
		return (AllocationBean) allocationBeans.get(index);
	}

    private void resizeAllocationBeans(int index) 
    {
        int newSize = index + 1;
        if (allocationBeans.size() < newSize) 
        {
            for (int i = allocationBeans.size(); allocationBeans.size() < newSize; i++)
            {
            	allocationBeans.add(new AllocationBean());
            }
        }
    }

	public boolean isLargeProposal() {
		return largeProposal;
	}


	public void setLargeProposal(boolean largeProposal) {
		this.largeProposal = largeProposal;
	}

	public String getAllocationJustification()
	{
		return allocationJustification;
	}

	public void setAllocationJustification(String allocationJustification)
	{
		this.allocationJustification = allocationJustification;
	}

	public List getObservationAllocations()
	{
		return observationAllocations;
	}

	public void setObservationAllocations(List observationAllocations)
	{
		this.observationAllocations = observationAllocations;
	}

	public boolean isDisplayObservingMode()
	{
		return displayObservingMode;
	}

	public void setDisplayObservingMode(boolean displayObservingMode)
	{
		this.displayObservingMode = displayObservingMode;
	}
	
	public boolean isDisplayScienceCategory()
	{
		return displayScienceCategory;
	}

	public void setDisplayScienceCategory(boolean displayScienceCategory)
	{
		this.displayScienceCategory = displayScienceCategory;
	}
	
	
	public List getObservingModes()
	{
		return observingModes;
	}

	public void setObservingModes(List observingModes)
	{
		this.observingModes = observingModes;
	}

	public String getSelectedObservingMode()
	{
		return selectedObservingMode;
	}

	public void setSelectedObservingMode(String selectedObservingMode)
	{
		this.selectedObservingMode = selectedObservingMode;
	}

	public boolean isDisplayAllocationJustification()
	{
		return displayAllocationJustification;
	}

	public void setDisplayAllocationJustification(
			boolean displayAllocationJustification)
	{
		this.displayAllocationJustification = displayAllocationJustification;
	}

	public boolean isDisplayNewObserverExperience()
	{
		return displayNewObserverExperience;
	}

	public void setDisplayNewObserverExperience(boolean displayNewObserverExperience)
	{
		this.displayNewObserverExperience = displayNewObserverExperience;
	}

	public boolean isDisplayLongTermStorage() {
		return displayLongTermStorage;
	}

	public void setDisplayLongTermStorage(boolean displayLongTermStorage) {
		this.displayLongTermStorage = displayLongTermStorage;
	}

	public boolean isDisplayOtherExpenditure()
	{
		return displayOtherExpenditure;
	}

	public void setDisplayOtherExpenditure(boolean displayOtherExpenditure)
	{
		this.displayOtherExpenditure = displayOtherExpenditure;
	}

	public boolean isDisplayTravel()
	{
		return displayTravel;
	}

	public void setDisplayTravel(boolean displayTravel)
	{
		this.displayTravel = displayTravel;
	}

	public String getOtherExpenditure()
	{
		return otherExpenditure;
	}

	public void setOtherExpenditure(String otherExpenditure)
	{
		this.otherExpenditure = otherExpenditure;
	}

	public String getTravel()
	{
		return travel;
	}

	public void setTravel(String travel)
	{
		this.travel = travel;
	}

	public boolean isRequiredNewObserverExperience()
	{
		return requiredNewObserverExperience;
	}

	public void setRequiredNewObserverExperience(
			boolean requiredNewObserverExperience)
	{
		this.requiredNewObserverExperience = requiredNewObserverExperience;
	}

	public boolean isDisplayUsefulTime() {
		return displayUsefulTime;
	}

	public void setDisplayUsefulTime(boolean enableUsefulTime) {
		this.displayUsefulTime = enableUsefulTime;
	}

	public boolean isDisplayAwardedTime() {
		return displayAwardedTime;
	}

	public void setDisplayAwardedTime(boolean displayAwardedTime) {
		this.displayAwardedTime = displayAwardedTime;
	}

	public boolean isDisplayLongTermTime() {
		return displayLongTermTime;
	}

	public void setDisplayLongTermTime(boolean displayLongTermTime) {
		this.displayLongTermTime = displayLongTermTime;
	}
	
	 public boolean isDisplayDark() {
			return displayDark;
	}

	public void setDisplayDark(boolean displayDark) {
		this.displayDark = displayDark;
	}

	public boolean isDisplayGrey() {
		return displayGrey;
	}

	public void setDisplayGrey(boolean displayGrey) {
		this.displayGrey = displayGrey;
	}

	public boolean isDisplayBright() {
		return displayBright;
	}

	public void setDisplayBright(boolean displayBright) {
		this.displayBright = displayBright;
	}

	public boolean isDisplayGlobal() {
		return displayGlobal;
	}

	public void setDisplayGlobal(boolean displayGlobal) {
		this.displayGlobal = displayGlobal;
	}
	
    public boolean isDisplayLargeProposal() {
		return this.displayLargeProposal;
	}

	public void setDisplayLargeProposal(boolean displayLargeProposal) {
		this.displayLargeProposal = displayLargeProposal;
	}

	public String getSpecifiedStorage() {
		return specifiedStorage;
	}

	public void setSpecifiedStorage(String specifiedStorage) {
		this.specifiedStorage = specifiedStorage;
	}

	public String getShowInstrumentBean() {
		return showInstrumentBean;
	}

	public void setShowInstrumentBean(String showInstrumentBean) {
		this.showInstrumentBean = showInstrumentBean;
	}

	public boolean isDisplayProprietaryException() {
		return displayProprietaryException;
	}

	public void setDisplayProprietaryException(boolean displayProprietaryException) {
		this.displayProprietaryException = displayProprietaryException;
	}

	public boolean isEnableProprietaryException() {
		return enableProprietaryException;
	}

	public void setEnableProprietaryException(boolean enableProprietaryException) {
		this.enableProprietaryException = enableProprietaryException;
	}

	public String getProprietaryExceptionSpecifics() {
		return proprietaryExceptionSpecifics;
	}

	public void setProprietaryExceptionSpecifics(
			String proprietaryExceptionSpecifics) {
		this.proprietaryExceptionSpecifics = proprietaryExceptionSpecifics;
	}

	public boolean isDisplayRequestedTime() {
		return displayRequestedTime;
	}

	public void setDisplayRequestedTime(boolean displayRequestedTime) {
		this.displayRequestedTime = displayRequestedTime;
	}

	public boolean isDisplayObservingDetails() {
		return displayObservingDetails;
	}

	public void setDisplayObservingDetails(boolean displayObservingDetails) {
		this.displayObservingDetails = displayObservingDetails;
	}

	public boolean isDisplayAllocations() {
		return displayAllocations;
	}

	public void setDisplayAllocations(boolean displayAllocations) {
		this.displayAllocations = displayAllocations;
	}

	public boolean isDisplayLongTermProposal() {
		return displayLongTermProposal;
	}

	public void setDisplayLongTermProposal(boolean displayLongTermProposal) {
		this.displayLongTermProposal = displayLongTermProposal;
	}

	public boolean isDisplayCepLoad() {
		return displayCepLoad;
	}

	public void setDisplayCepLoad(boolean displayCepLoad) {
		this.displayCepLoad = displayCepLoad;
	}

	public String getSpecifiedCepLoad() {
		return specifiedCepLoad;
	}

	public void setSpecifiedCepLoad(String specifiedCepLoad) {
		this.specifiedCepLoad = specifiedCepLoad;
	}

	public String[] getPipelineSummaryLabels() {
		return pipelineSummaryLabels;
	}

	public void setPipelineSummaryLabels(String[] pipelineSummaryLabels) {
		this.pipelineSummaryLabels = pipelineSummaryLabels;
	}

	public List getPipelineSummaryValues() {
		return pipelineSummaryValues;
	}

	public void setPipelineSummaryValues(List pipelineSummaryValues) {
		this.pipelineSummaryValues = pipelineSummaryValues;
	}

		public boolean isProcessingPipeline() {
		return processingPipeline;
	}

	public void setProcessingPipeline(boolean processingPipeline) {
		this.processingPipeline = processingPipeline;
	}

	public boolean isVariableMaxPageNumber() {
		return variableMaxPageNumber;
	}

	public void setVariableMaxPageNumber(boolean variableMaxPageNumber) {
		this.variableMaxPageNumber = variableMaxPageNumber;
	}

	public boolean isDisplayProcessingTime() {
		return displayProcessingTime;
	}

	public void setDisplayProcessingTime(boolean displayProcessingTime) {
		this.displayProcessingTime = displayProcessingTime;
	}
	
	 public String getSpecifiedProcessingTime() {
			return specifiedProcessingTime;
		}

		public void setSpecifiedProcessingTime(String specifiedProcessingTime) {
			this.specifiedProcessingTime = specifiedProcessingTime;
		}

}
