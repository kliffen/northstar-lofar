<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processInvalidConfiguration" method="POST">
<TABLE>
	
	<TR>
		<TD>
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				<TD valign='top'><PRE>
The telescope of this proposal has a new configuration!!
Do you want to convert this proposal to the new configuration?
Note: Only with the new configuration, you can submit the proposal!!

</PRE>
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>

	</TABLE>
<TABLE width="100%">
	<TR>
		<TD align="left"><html:submit property="submitButton" styleClass="list_accept_yes" title="Ok, submit this proposal">Ok</html:submit></TD>
		
		<TD class="right"><html:cancel styleClass="list_decline_t" title="Cancel submit">Cancel</html:cancel>
		</TD>
	</TR>
</TABLE>
</html:form>
