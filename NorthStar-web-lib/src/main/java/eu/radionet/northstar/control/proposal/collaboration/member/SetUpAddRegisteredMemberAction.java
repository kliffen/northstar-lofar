// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.collaboration.member;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.exception.ActiveParticipantException;
import eu.radionet.northstar.business.exception.InvalidUserException;
import eu.radionet.northstar.business.exception.NoInvitationFoundException;
import eu.radionet.northstar.control.Constants;

public class SetUpAddRegisteredMemberAction extends Action {
	private Log log = LogFactory.getLog(this.getClass());

	private CollaborationDelegate collaborationDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AddRegisteredMemberForm addRegisteredMemberForm = (AddRegisteredMemberForm) form;
		HttpSession session = request.getSession();

		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
		
		if(addRegisteredMemberForm.getPid() == null){
			addRegisteredMemberForm.setInvalidUrl(true);
			return mapping.findForward(Constants.FAILURE);
		}else if(!AstronValidator.isPositiveInt(addRegisteredMemberForm.getPid()) ){
			addRegisteredMemberForm.setInvalidUrl(true);
			return mapping.findForward(Constants.FAILURE);	
		}

		collaborationDelegate = CollaborationDelegate.getInstance();
		try {
			collaborationDelegate.acceptInvitation(addRegisteredMemberForm
					.getKey(), ownUserAccount, session.getId(),addRegisteredMemberForm.getPid());

		} catch (NoInvitationFoundException nife) {
			addRegisteredMemberForm.setNoInvitation(true);
			return mapping.findForward(Constants.FAILURE);
		} catch (InvalidUserException nife) {
			addRegisteredMemberForm.setNoInvitation(true);
			return mapping.findForward(Constants.FAILURE);
        } catch (ActiveParticipantException nife) {
            addRegisteredMemberForm.setInvalidUser(true);
            return mapping.findForward(Constants.FAILURE);
		}
		return mapping.findForward(Constants.SUCCESS);

	}
}
