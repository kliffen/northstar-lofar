// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.configuration;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Semester;

public class TelescopeConfiguration {
	private String strutsModule = null;
	private String headerImageUri = null;
	private String pdfHeaderImageUri = null;
	private String telescopeAstronomer = null;
	private String director = null;
	private String committee = null;
	private Class emailGeneratorClass = null;
	private Class proposalDelegateClass = null;
	private Class pdfWriterClass = null;
	private Class xmlWriterClass = null;
	private Class proposalValidatorClass = null;
	private Class targetListParserClass = null;
	private Class observingRequestClass = null;
	private Class observationClass = null;
    private List otherFileConfigurations = new ArrayList();
    private Map optionsContexts = new HashMap();
	private Date startTime = null;
	private Date endTime = null;
	private String version = null;
	private String community = null;
	private int maxWordsForAbstract = 100;
	private int maxWordsForTech = 60;
	private int maxCharsForTitle = 150;
/*	private int maxScientificPages = 2;
	private int maxTechnicalPages = 1;
	private int maxFigurePages = 1;
	private int maxTotalPages = 4;
	private String allowedJustificationFileTypes = ".pdf";
	private String allowedFigureFileTypes = ".pdf";
	*/
	private boolean enableGrantNumber = false;
	private boolean enableSponsoring = false;
	private boolean enableNewObserverExperience = false;
	private boolean disableEmailValidation=false;
	//private String latexClassFile = null;
	//private String latexTemplateFile = null;
	//private String latexRequiredHeader = null;
	private String labelsFile = null;
	//private Integer maxPublicationCount = null;
	private boolean targetsOnTabbedPage = false;
	private String preferredCountries = null;
	private String tacCountry = null;
	
	private Hashtable emailConfigurations = new Hashtable();
	public void addEmailConfiguration(String type, EmailConfiguration emailConfiguration){
		emailConfigurations.put(type,emailConfiguration);
	}
	public EmailConfiguration getEmailConfiguration(String type){
		return (EmailConfiguration) emailConfigurations.get(type);
	}
	public Hashtable getEmailConfigurations(){
		return emailConfigurations;
	}

	public Class getObservingRequestClass() {
		return observingRequestClass;
	}

	public void setObservingRequestClass(Class observingRequest) {
		this.observingRequestClass = observingRequest;
	}

	public boolean isEnableNewObserverExperience() {
		return enableNewObserverExperience;
	}

	public void setEnableNewObserverExperience(boolean enableNewObserverExperience) {
		this.enableNewObserverExperience = enableNewObserverExperience;
	}

	public String getCommittee() {
		return committee;
	}
	public String getDirector() {
		return director;
	}
	public Class getEmailGeneratorClass() {
		return emailGeneratorClass;
	}
	public String getTelescopeAstronomer() {
		return telescopeAstronomer;
	}
	public void setCommittee(String committee) {
		this.committee = committee;
	}
	public void setDirector(String director) {
		this.director = director;
	}
	public void setEmailConfigurations(Hashtable emailConfigurations) {
		this.emailConfigurations = emailConfigurations;
	}
	public void setEmailGeneratorClass(Class emailGenerator) {
		this.emailGeneratorClass = emailGenerator;
	}
	public void setTelescopeAstronomer(String telescopeAstronomer) {
		this.telescopeAstronomer = telescopeAstronomer;
	}
	public String getStrutsModule() {
		return strutsModule;
	}
	public void setStrutsModule(String strutsModule) {
		this.strutsModule = strutsModule;
	}
	public String getHeaderImageUri() {
		return headerImageUri;
	}
	public void setHeaderImageUri(String headerImageUri) {
		this.headerImageUri = headerImageUri;
	}
	public Class getPdfWriterClass() {
		return pdfWriterClass;
	}
	public Class getTargetListParserClass() {
		return targetListParserClass;
	}
	public void setPdfWriterClass(Class pdfWriter) {
		this.pdfWriterClass = pdfWriter;
	}
	public void setTargetListParserClass(Class targetListParser) {
		this.targetListParserClass = targetListParser;
	}
	public void setPdfHeaderImageUri(String pdfHeaderImageUri) {
		this.pdfHeaderImageUri = pdfHeaderImageUri;
	}
	public String getPdfHeaderImageUri() {
		return pdfHeaderImageUri;
	}

	public void setProposalDelegateClass(Class proposalDelegateClass) {
		this.proposalDelegateClass = proposalDelegateClass;
	}
	public Class getProposalDelegateClass() {
		return proposalDelegateClass;
	}
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public Date getEndTime() {
		return endTime;
	}
	public void setEndTime(Date endTime) {
		this.endTime = endTime;
	}
	public Date getStartTime() {
		return startTime;
	}
	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public List getOtherFileConfigurations() {
		return otherFileConfigurations;
	}
	public void setOtherFileConfigurations(List otherFileConfigurations) {
		this.otherFileConfigurations = otherFileConfigurations;
	}
	public Map getOptionsContexts() {
		return optionsContexts;
	}
	public void setOptionsContexts(Map optionsContexts) {
		this.optionsContexts = optionsContexts;
	}
	public ContextType getContextConfiguration(Semester semester){
		return (ContextType) optionsContexts.get(semester.getCategory().getCode());
	}
	public ContextType getContextConfiguration(Proposal proposal){
		return (ContextType) optionsContexts.get(proposal.getSemester().getCategory().getCode());
	}
	public ContextType getContextConfiguration(String category){
		return (ContextType) optionsContexts.get(category);
	}
	
	public Class getProposalValidatorClass() {
		return proposalValidatorClass;
	}
	public void setProposalValidatorClass(Class proposalValidatorClass) {
		this.proposalValidatorClass = proposalValidatorClass;
	}
	public Class getXmlWriterClass() {
		return xmlWriterClass;
	}
	public void setXmlWriterClass(Class xmlWriterClass) {
		this.xmlWriterClass = xmlWriterClass;
	}
	public Class getObservationClass() {
		return observationClass;
	}
	public void setObservationClass(Class observationClass) {
		this.observationClass = observationClass;
	}

	public int getMaxCharsForTitle() {
		return maxCharsForTitle;
	}
	public void setMaxCharsForTitle(int maxCharactersForTitle) {
		this.maxCharsForTitle = maxCharactersForTitle;
	}
	public int getMaxWordsForAbstract() {
		return maxWordsForAbstract;
	}
	public void setMaxWordsForAbstract(int maxWordsForAbstract) {
		this.maxWordsForAbstract = maxWordsForAbstract;
	}
	
	public boolean isEnableGrantNumber() {
		return enableGrantNumber;
	}
	public void setEnableGrantNumber(boolean enableGrantNumber) {
		this.enableGrantNumber = enableGrantNumber;
	}
	public String getLabelsFile() {
		return labelsFile;
	}
	public void setLabelsFile(String labelsFile) {
		this.labelsFile = labelsFile;
	}
	public boolean isEnableSponsoring() {
		return enableSponsoring;
	}
	public void setEnableSponsoring(boolean enableSponsoring) {
		this.enableSponsoring = enableSponsoring;
	}
	public boolean isTargetsOnTabbedPage() {
		return targetsOnTabbedPage;
	}
	public void setTargetsOnTabbedPage(boolean targetsOnTabbedPage) {
		this.targetsOnTabbedPage = targetsOnTabbedPage;
	}
	public String getPreferredCountries() {
		return preferredCountries;
	}
	public void setPreferredCountries(String preferredCountries) {
		this.preferredCountries = preferredCountries;
	}
	public String getTacCountry() {
		return tacCountry;
	}
	public void setTacCountry(String tacCountry) {
		this.tacCountry = tacCountry;
	}
	public boolean isDisableEmailValidation() {
		return disableEmailValidation;
	}
	public void setDisableEmailValidation(boolean disableEmailValidation) {
		this.disableEmailValidation = disableEmailValidation;
	}
	public int getMaxWordsForTech() {
		return maxWordsForTech;
	}
	public void setMaxWordsForTech(int maxWordsForTech) {
		this.maxWordsForTech = maxWordsForTech;
	}

}
