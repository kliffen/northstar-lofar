// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.data.entities.admin;

import java.io.Serializable;

public class StatusEmail implements Serializable {
	protected Integer id = null;
	protected Integer proposalId = null;
	protected String subject  = null;
	protected String message = null;
	protected Integer proposalStatusId = null;
	protected String toRecipients = null;
	protected String ccRecipients = null;
	protected String bccRecipients = null;
	protected String sender = null;
	protected boolean generatedMail = false;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public Integer getProposalId() {
		return proposalId;
	}
	public void setProposalId(Integer proposalId) {
		this.proposalId = proposalId;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public Integer getProposalStatusId() {
		return proposalStatusId;
	}
	public void setProposalStatusId(Integer proposalStatusId) {
		this.proposalStatusId = proposalStatusId;
	}
	public String getSender() {
		return sender;
	}
	public void setSender(String sender) {
		this.sender = sender;
	}
	public String getBccRecipients() {
		return bccRecipients;
	}
	public void setBccRecipients(String bccRecipients) {
		this.bccRecipients = bccRecipients;
	}
	public String getCcRecipients() {
		return ccRecipients;
	}
	public void setCcRecipients(String ccRecipients) {
		this.ccRecipients = ccRecipients;
	}
	public String getToRecipients() {
		return toRecipients;
	}
	public void setToRecipients(String toRecipients) {
		this.toRecipients = toRecipients;
	}
	public boolean isGeneratedMail() {
		return generatedMail;
	}
	public void setGeneratedMail(boolean generatedMail) {
		this.generatedMail = generatedMail;
	}


}
