// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Dec 16, 2004
 *
 *
 */
package eu.opticon.northstar.data.entities;

import java.util.Iterator;

import eu.opticon.northstar.business.OpticonConstants;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.data.entities.Observation;

/**
 * @author Anton Smit
 */
public class OpticonObservation extends Observation
{
    protected String telescopeConfiguration = null;
    
	protected OpticonInstrument instrument = null;
	protected String observationPhase = null;
	protected String observationWeather = null;
	protected String observationMode = null;
	protected String observationSeeing = null;
	protected String observationDates = null;
	protected String backupStrategy = null;
	protected String calibrationRequirements = null;
	protected Double noiseLevel = null;
	//protected boolean isProcessingPipeline = false;   
	
	public Double getTotalObservationDuration()
	{
		
		double duration = 0;
		if (targets==null){
			return new Double(0.0);
		}
		Iterator targetsIt = targets.iterator();
		while (targetsIt.hasNext())
		{
			OpticonTarget target = (OpticonTarget) targetsIt.next();
			//FIXME compiner problems?
			Double targetDuration = NorthStarDelegate.getResourceValue(
					target.getAllocations(), OpticonConstants.TOTAL_DURATION);
			if (targetDuration != null)
			{
				duration += targetDuration.doubleValue();				
			}
			
		}
		
		return new Double(duration);
	}
	
	public boolean isProcessingPipeline(){
		if(this.instrument != null && this.instrument.getName() != null 
				&& this.instrument.getName().equalsIgnoreCase("pipeline")){
			return true;
		}
		
		return false;
	}

	public String getTelescopeConfiguration()
	{
		return telescopeConfiguration;
	}


	public void setTelescopeConfiguration(String telescopeConfiguration)
	{
		this.telescopeConfiguration = telescopeConfiguration;
	}

	public OpticonInstrument getInstrument()
	{
		return instrument;
	}

	public void setInstrument(OpticonInstrument instrument)
	{
		this.instrument = instrument;
	}
    
	public String getObservationWeather()
	{
		return observationWeather;
	}

	public void setObservationWeather(String observationWeather)
	{
		this.observationWeather = observationWeather;
	}

	public String getObservationMode()
	{
		return observationMode;
	}

	public void setObservationMode(String observationMode)
	{
		this.observationMode = observationMode;
	}

	public String getObservationSeeing()
	{
		return observationSeeing;
	}

	public void setObservationSeeing(String observationSeeing)
	{
		this.observationSeeing = observationSeeing;
	}

	public String getObservationDates()
	{
		return observationDates;
	}

	public void setObservationDates(String observationDates)
	{
		this.observationDates = observationDates;
	}

	public String getBackupStrategy() {
		return backupStrategy;
	}

	public void setBackupStrategy(String backupStrategy) {
		this.backupStrategy = backupStrategy;
	}

	public String getObservationPhase() {
		return observationPhase;
	}

	public void setObservationPhase(String observationPhase) {
		this.observationPhase = observationPhase;
	}

	public String getCalibrationRequirements() {
		return calibrationRequirements;
	}

	public void setCalibrationRequirements(String calibrationRequirements) {
		this.calibrationRequirements = calibrationRequirements;
	}

	public final Double getNoiseLevel() {
		return noiseLevel;
	}

	public final void setNoiseLevel(Double noiseLevel) {
		this.noiseLevel = noiseLevel;
	}
	
}
