// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.util;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import javax.servlet.ServletContext;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.radionet.northstar.business.configuration.EmailConfiguration;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.control.Constants;

public class TelescopeConfigurator {
	protected final static String NORTHSTAR_TELESCOPE_CONFIGURATIONS = "northstar-telescope-configurations";

	protected final static String NORTHSTAR_TELESCOPE_CONFIGURATION = "northstar-telescope-configuration";

	protected static final String NAME = "name";

	protected static final String WELCOME_URI = "welcome-uri";

	protected static final String DESCRIPTION = "description";

	protected static final String EMAIL_SENDER = "emailsender";

	protected static final String ADMINISTRATOR_EMAIL = "administrator-email";

	protected static final String MAPPING_FILES = "mapping-files";

	protected static final String MAPPING_FILE = "mapping-file";

	protected static final String OTHER_FILES = "other-files";

	protected static final String OTHER_FILE = "other-file";

	protected static final String PARSER = "parser";

	protected static final String FILE = "file";

	protected static final String TELESCOPES = "telescopes";

	protected static final String TELESCOPE = "telescope";

	protected static final String OBSERVING_REQUEST = "observingrequest";

	protected static final String OBSERVATION = "observation";

	protected static final String PDF_WRITER = "pdfwriter";

	protected static final String XML_WRITER = "xmlwriter";

	protected static final String PROPOSAL_VALIDATOR = "proposalvalidator";

	protected static final String TARGET_LIST_PARSER = "targetlistparser";

	protected static final String EMAILS = "emails";

	protected static final String EMAIL = "email";

	protected static final String TYPE = "type";

	protected static final String URI = "uri";

	protected static final String BCC = "bcc";

	protected static final String VERSION = "version";

	protected static final String START_TIME = "starttime";

	protected static final String END_TIME = "endtime";

	protected static final String COMMUNITY = "community";

	protected static final String COLLABORATION = "collaboration";

	protected static final String PROPOSAL_DELEGATE = "proposaldelegate";

	protected static final String PROPOSAL_CONVERTER = "telescopeconverter";

	protected static final String TELESCOPE_ASTRONOMER = "telescopeastronomer";

	protected static final String DIRECTOR = "director";

	protected static final String EMAIL_GENERATOR = "emailgenerator";

	protected static final String COMMITTEES = "committees";

	protected static final String COMMITTEE = "committee";

	protected static final String STRUTS_MODULE = "struts-module";

	protected static final String HEADER_IMAGE_URI = "header-image";

	protected static final String PDF_HEADER_IMAGE_URI = "pdf-header-image";

	protected static final String OPTION_VALIDATION_FILE = "option-validation-file";

	protected static final String MAX_WORDS_FOR_ABSTRACT = "max-words-for-abstract";
	protected static final String MAX_WORDS_FOR_TECH="max-words-for-tech";
	protected static final String MAX_CHARS_FOR_TITLE = "max-chars-for-title";

/*	protected static final String MAX_SCIENTIFIC_PAGES = "max-scientific-pages";
	protected static final String MAX_TECHNICAL_PAGES = "max-technical-pages";
	protected static final String MAX_FIGURE_PAGES = "max-figure-pages";
	protected static final String MAX_TOTAL_PAGES = "max-total-pages";
	
	protected static final String ALLOWED_JUSTIFICATION_FILE_TYPES ="allowed-justification-file-types";
	protected static final String ALLOWED_FIGURE_FILE_TYPES = "allowed-figure-file-types";
	
	protected static final String MAX_PUBLICATION_COUNT = "max-publication-count";
	*/
	protected static final String LATEX_CLASS_FILE = "latexClassFile";
	protected static final String LATEX_TEMPLATE_FILE = "latexTemplateFile";
	//protected static final String LATEX_REQUIRED_HEADER = "latexRequiredHeader";
	
	protected static final String ENABLE_GRANT_NUMBER = "enable-grant-number";
	protected static final String ENABLE_OPTICON_SUPPORT = "enable-opticon-support";
	protected static final String ENABLE_SPONSORING = "enable-sponsoring";
	protected static final String ENABLE_NEW_OBSERVER_EXPERIENCE = "enable-new-observer-experience";
	protected static final String LABELS_FILE = "labels-file";
	protected static final String TARGETS_ON_TABBED_PAGE = "targets-on-tabbed-page";
	protected static final String PREFERRED_COUNTRIES = "preferred-countries";
	protected static final String TAC_COUNTRY = "tac-country";
	
	protected ServletContext servletContext = null;

	private Log log = LogFactory.getLog(this.getClass());

	public List configure(Document document, ServletContext servletContext)
			throws Exception {
		this.servletContext = servletContext;
		/*
		 * search for project
		 */
		NodeList elements = document
				.getElementsByTagName(NORTHSTAR_TELESCOPE_CONFIGURATIONS);
		for (int i = 0; i < elements.getLength(); i++) {
			Node node = (Node) elements.item(i);
			return parseRootNode(node);
		}
		return null;
	}

	protected List parseRootNode(Node northStarNode) throws Exception {
		List configurations = new ArrayList();

		for (int i = 0; i < northStarNode.getChildNodes().getLength(); i++) {
			Node nodeChild = northStarNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, NORTHSTAR_TELESCOPE_CONFIGURATION)) {
					configurations
							.add(parseNorthStarTelescopeConfigurationNode(nodeChild));
				}
			}

		}
		return configurations;

	}

	protected TelescopeConfiguration parseNorthStarTelescopeConfigurationNode(
			Node telescopeNode) throws Exception {
		TelescopeConfiguration telescopeConfiguration = new TelescopeConfiguration();
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < telescopeNode.getChildNodes().getLength(); i++) {
			Node nodeChild = telescopeNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, START_TIME)) {
					String dateString = getValue(nodeChild);
					try {

						telescopeConfiguration
								.setStartTime(getDate(dateString));
					} catch (ParseException pe) {
						log.error("Cannot parse : " + dateString);
					}
				} else if (equal(nodeChild, END_TIME)) {
					String dateString = getValue(nodeChild);
					try {

						telescopeConfiguration.setEndTime(getDate(dateString));
					} catch (ParseException pe) {
						log.error("Cannot parse : " + dateString);
					}
				} else if (equal(nodeChild, COMMUNITY)) {
					telescopeConfiguration.setCommunity(getValue(nodeChild));
				} else if (equal(nodeChild, VERSION)) {
					telescopeConfiguration.setVersion(getValue(nodeChild));
				} else if (equal(nodeChild, PDF_WRITER)) {
					telescopeConfiguration.setPdfWriterClass(Class
							.forName(getValue(nodeChild)));
				} else if (equal(nodeChild, XML_WRITER)) {
					telescopeConfiguration.setXmlWriterClass(Class
							.forName(getValue(nodeChild)));
				} else if (equal(nodeChild, PROPOSAL_VALIDATOR)) {
					telescopeConfiguration.setProposalValidatorClass(Class
							.forName(getValue(nodeChild)));
				} else if (equal(nodeChild, TARGET_LIST_PARSER)) {
					telescopeConfiguration.setTargetListParserClass(Class
							.forName(getValue(nodeChild)));
				} else if (equal(nodeChild, OBSERVING_REQUEST)) {
					telescopeConfiguration.setObservingRequestClass(Class
							.forName(getValue(nodeChild)));
				} else if (equal(nodeChild, OBSERVATION)) {
					telescopeConfiguration.setObservationClass(Class
							.forName(getValue(nodeChild)));
				} else if (equal(nodeChild, PROPOSAL_DELEGATE)) {
					telescopeConfiguration.setProposalDelegateClass(Class
							.forName(getValue(nodeChild)));
				} else if (equal(nodeChild, MAPPING_FILES)) {
					parseMappingFiles(nodeChild, NorthStarConfiguration
							.getMappingFiles());
				}else if (equal(nodeChild, OTHER_FILES)) {
					parseOtherFiles(nodeChild, telescopeConfiguration);
				} else if (equal(nodeChild, TELESCOPE_ASTRONOMER)) {
					telescopeConfiguration
							.setTelescopeAstronomer(getValue(nodeChild));
				} else if (equal(nodeChild, DIRECTOR)) {
					telescopeConfiguration.setDirector(getValue(nodeChild));
				} else if (equal(nodeChild, STRUTS_MODULE)) {
					telescopeConfiguration.setStrutsModule(getValue(nodeChild));
			/*	} else if (equal(nodeChild, MAX_SCIENTIFIC_PAGES)) {
					Integer maxScientificPages = AstronConverter
							.toInteger(getValue(nodeChild));
					if (maxScientificPages != null) {
						telescopeConfiguration.setMaxScientificPages(
								maxScientificPages.intValue());
					}
				} else if (equal(nodeChild, MAX_TECHNICAL_PAGES)) {
					Integer maxTechnicalPages = AstronConverter
							.toInteger(getValue(nodeChild));
					if (maxTechnicalPages != null) {
						telescopeConfiguration
								.setMaxTechnicalPages(maxTechnicalPages
										.intValue());
					}
				} else if (equal(nodeChild, MAX_FIGURE_PAGES)) {
					Integer maxFigurePages = AstronConverter
							.toInteger(getValue(nodeChild));
					if (maxFigurePages != null) {
						telescopeConfiguration
								.setMaxFigurePages(maxFigurePages.intValue());
					}
				} else if (equal(nodeChild, MAX_TOTAL_PAGES)) {
					Integer maxTotalPages = AstronConverter
							.toInteger(getValue(nodeChild));
					if (maxTotalPages != null) {
						telescopeConfiguration
								.setMaxTotalPages(maxTotalPages.intValue());
					}
					*/
				}else if (equal(nodeChild, MAX_WORDS_FOR_ABSTRACT)) {
					Integer maxWordsForAbstract = new Integer(getValue(nodeChild));
					if (maxWordsForAbstract != null) {
						telescopeConfiguration
								.setMaxWordsForAbstract(maxWordsForAbstract
										.intValue());
					}
				/*
				}  else if (equal(nodeChild, ALLOWED_FIGURE_FILE_TYPES)) {
						telescopeConfiguration.setAllowedFigureFileTypes(getValue(nodeChild));
				} else if (equal(nodeChild, ALLOWED_JUSTIFICATION_FILE_TYPES)) {
					telescopeConfiguration.setAllowedJustificationFileTypes(getValue(nodeChild));
				} else if (equal(nodeChild, MAX_CHARS_FOR_TITLE)) {
					Integer maxCharsForTitle = AstronConverter
							.toInteger(getValue(nodeChild));
					if (maxCharsForTitle != null) {
						telescopeConfiguration
								.setMaxCharsForTitle(maxCharsForTitle
										.intValue());
					}
					*/	
				} 
				else if (equal(nodeChild, MAX_WORDS_FOR_TECH)) {
					Integer maxWordsForTech = new Integer(getValue(nodeChild));
					if (maxWordsForTech != null) {
						telescopeConfiguration
								.setMaxWordsForTech(maxWordsForTech
										.intValue());
					}	
				}
				
				else if (equal(nodeChild, HEADER_IMAGE_URI)) {
					telescopeConfiguration
							.setHeaderImageUri(getValue(nodeChild));
				} else if (equal(nodeChild, PDF_HEADER_IMAGE_URI)) {
					telescopeConfiguration
							.setPdfHeaderImageUri(getValue(nodeChild));
				} else if (equal(nodeChild, COMMITTEE)) {
					telescopeConfiguration.setCommittee(getValue(nodeChild));
				} else if (equal(nodeChild, EMAIL_GENERATOR)) {
					telescopeConfiguration.setEmailGeneratorClass(Class
							.forName(getValue(nodeChild)));
				} else if (equal(nodeChild, EMAILS)) {
					parseEmails(nodeChild, telescopeConfiguration
							.getEmailConfigurations());
				}else if (equal(nodeChild, ENABLE_GRANT_NUMBER)) {
					telescopeConfiguration.setEnableGrantNumber(true);
				}else if (equal(nodeChild, ENABLE_SPONSORING)) {
					telescopeConfiguration.setEnableSponsoring(true);
				}else if (equal(nodeChild, ENABLE_NEW_OBSERVER_EXPERIENCE)) {
					telescopeConfiguration.setEnableNewObserverExperience(true);
				} else if (equal(nodeChild, LABELS_FILE)) {
					telescopeConfiguration.setLabelsFile(getValue(nodeChild));
				} else if (equal(nodeChild, TARGETS_ON_TABBED_PAGE)) {
						telescopeConfiguration.setTargetsOnTabbedPage(true);
				} else if (equal(nodeChild, Constants.DISABLE_INVITATION_EMAIL_VALIDATION)) {
					telescopeConfiguration.setDisableEmailValidation(true);
				}	else if (equal(nodeChild, PREFERRED_COUNTRIES)) {
					telescopeConfiguration.setPreferredCountries(getValue(nodeChild));
				} else if (equal(nodeChild, TAC_COUNTRY)) {
					telescopeConfiguration.setTacCountry(getValue(nodeChild));
				} else if (equal(nodeChild, OPTION_VALIDATION_FILE)) {
					try {
						String configFileUri = getValue(nodeChild);
						String configFile = NorthStarUtils.read(configFileUri,servletContext);		
						XMLConverter xmlConverter = XMLConverter.getInstance();
						Document optionConfigDocument = xmlConverter
								.convertStringToDocument(configFile);
						OptionsConfigurator optionsConfigurator = new OptionsConfigurator();
						telescopeConfiguration
								.setOptionsContexts(optionsConfigurator
										.configure(optionConfigDocument,servletContext));
					} catch (NullPointerException npe) {
						return null;
					}
				}

			}
		}
		return telescopeConfiguration;
	}

	protected void parseMappingFiles(Node mappingFilesNode, Set mappingFiles) {
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < mappingFilesNode.getChildNodes().getLength(); i++) {
			Node mappingFileChild = mappingFilesNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class,
					mappingFileChild.getClass())) {
				if (equal(mappingFileChild, MAPPING_FILE)) {
					mappingFiles.add(getValue(mappingFileChild));
				}

			}
		}
	}

	protected void parseOtherFiles(Node otherFilesNode,
			TelescopeConfiguration telescopeConfiguration) throws Exception {
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < otherFilesNode.getChildNodes().getLength(); i++) {
			Node otherFileChild = otherFilesNode.getChildNodes().item(i);
			if (AstronValidator.implementsInterface(Element.class, otherFileChild
					.getClass())) {
				if (equal(otherFileChild, OTHER_FILE)) {
					Class parser = null;
					String fileName = null;
					for (int j = 0; j < otherFileChild.getChildNodes()
							.getLength(); j++) {
						Node parserOrFileChild = otherFileChild.getChildNodes()
								.item(j);
						if (AstronValidator.implementsInterface(Element.class,
								parserOrFileChild.getClass())) {
							if (equal(parserOrFileChild, PARSER)) {
								parser = Class
										.forName(getValue(parserOrFileChild));
							}
							if (equal(parserOrFileChild, FILE)) {
								fileName = getValue(parserOrFileChild);
							}
						}
					}
					Constructor sourceConstructor = parser.getConstructor(new Class[] {});
					OtherFilesConfigurator otherFilesConfigurator = (OtherFilesConfigurator) sourceConstructor
							.newInstance(new Object[] {});
					String otherFile = NorthStarUtils.read(fileName,servletContext);		
					XMLConverter xmlConverter = XMLConverter.getInstance();
					Document otherFilesDocument = xmlConverter
							.convertStringToDocument(otherFile);
					telescopeConfiguration.getOtherFileConfigurations().add(
							otherFilesConfigurator
									.configure(otherFilesDocument));
				}

			}
		}
	}

	protected void parseEmails(Node emailsNode, Hashtable hashTable)
			throws IOException {
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < emailsNode.getChildNodes().getLength(); i++) {
			Node emailChild = emailsNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, emailChild
					.getClass())) {
				if (equal(emailChild, EMAIL)) {
					parseEmail(emailChild, hashTable);
				}

			}
		}
	}

	protected void parseEmail(Node emailNode, Hashtable hashTable)
			throws IOException {
		EmailConfiguration emailConfiguration = new EmailConfiguration();
		/*
		 * iterate through the children
		 */
		for (int i = 0; i < emailNode.getChildNodes().getLength(); i++) {
			Node nodeChild = emailNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, TYPE)) {
					emailConfiguration.setKey(getValue(nodeChild));
				} else if (equal(nodeChild, URI)) {
					emailConfiguration.setUri(getValue(nodeChild));
					try {
						emailConfiguration.setMessage(NorthStarUtils.read(
								emailConfiguration.getUri(), servletContext));
					} catch (NullPointerException npe) {
						log.error("Cannot read : "
								+ emailConfiguration.getUri());
					}
				} else if (equal(nodeChild, BCC)) {
					emailConfiguration.setBcc(getValue(nodeChild));
				}
			}
		}
		if (!AstronValidator.isBlankOrNull(emailConfiguration.getKey())) {
			hashTable.put(emailConfiguration.getKey(), emailConfiguration);
		}
	}

	/**
	 * The getValue method returns the value of an node
	 * 
	 * @param node
	 * @return
	 */
	protected String getValue(Node node) {
		String value = null;
		if (node.getFirstChild() != null) {
			value = node.getFirstChild().getNodeValue();
			if (log.isDebugEnabled()) {
				log.debug("Node: " + node.getNodeName() + " value: " + value);
			}
		}
		return value;
	}

	/**
	 * The equal method compares if an node has the given name
	 * 
	 * @param node
	 * @param nodeName
	 * @return
	 */
	protected boolean equal(Node node, String nodeName) {
		return node.getNodeName().equals(nodeName);
	}

	protected Date getDate(String value) throws ParseException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat();

		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		simpleDateFormat.applyPattern("dd/MM/yyyy HH:mm:ss");
		return simpleDateFormat.parse(value);
	}
}
