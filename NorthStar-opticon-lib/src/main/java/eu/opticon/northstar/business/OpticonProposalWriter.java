// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SkeletonProposalWriter.java 
 *
 * Created on Mar 10, 2005
 *
 * Version $Id: OpticonProposalWriter.java,v 1.43 2008-08-15 10:07:31 boelen Exp $
 *
 */
package eu.opticon.northstar.business;

import java.awt.Color;
//import java.awt.TextArea;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletContext;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.LabelValueBean;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.AstroCoordinate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.business.pdf.DocumentWriter;
import eu.radionet.northstar.business.pdf.ProposalPdfPageEventHelper;
import eu.radionet.northstar.business.pdf.ProposalWriter;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.business.util.PdfUtils;

import eu.radionet.northstar.control.proposal.observingrequest.ValueBean;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.AdditionalIssues;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * Generates the telescope specific parts of the pdf view
 */
public class OpticonProposalWriter extends ProposalWriter {

	private Log log = LogFactory.getLog(this.getClass());
	protected static final String WHITESPACE = " ";
	protected static final String COMMA = ", ";
	protected static final String DOUBLE_POINT = ": ";

	protected List applicants = new ArrayList();
	//protected String community = null;
	// the overflow values are to overflow the others to the last page.
	protected int applicantOverflow=14;
	protected int observationOverflow = 14;
	protected int targetOverflow=28;
	
	protected int schedulingOverflow = 8;
	protected static final int LINE_LENGTH=80;
	// A4 size = 595,842
	protected float APPLICANTS_HEIGHT = 365.0f;
	protected float OBSERVATION_HEIGHT = 200.0f;
	protected float TARGETS_HEIGHT = 290.0f;
	
	public static final Font TITLE_REQUIRED_FONT = 
		FontFactory.getFont(FontFactory.HELVETICA, 12, Font.ITALIC, Color.RED);

	public static final Font TABLE_CONTENT_FONT_SMALL = FontFactory.getFont(
			FontFactory.HELVETICA, 6, Font.NORMAL);
	
	public static final Font TABLE_HEADER_FONT_SMALL = FontFactory.getFont(
			FontFactory.HELVETICA, 6, Font.BOLD, Color.WHITE);
	
    /**
     * default constructor
     */
    public OpticonProposalWriter() {
        super();
    }

		
	/**
	 * This methods overwrites the super one it is needed to add the extra pages
	 * after the science/technical case
	 * 
	 * @param proposal
	 * @param outputStream
	 * @throws DocumentException
	 * @throws IOException
	 */
	public void write(Proposal proposal, OutputStream outputStream, 
			ServletContext servletContext)
		throws DocumentException, IOException 
	{
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		
		/*
		 * Add the custom header here. When none of the telescope configs are used
		 * or the number of nights are the same the telescope used the "general" version
		 * otherwise it has the logo of the telescope with the most nights requested
		 */
		updateHeaderImage(proposal, servletContext);
		// get the message so the translation table will be fillled.
		String message = NorthStarUtils.getLabel(servletContext, proposal, "label.opticon.targettable.targets");
		
		firstHeader = getFirstHeader(proposal);

		applicants = proposal.getMembers(); // for addTotalRequestedTime()
		/*
		 * create the master document
		 */
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN);
		header = getHeader(proposal, document);

		/*
		 * instantiate pdf copy for merging pdf documents
		 */

		PdfCopy pdfCopy = new PdfCopy(document, outputStream);

		setMetaData(proposal, document);
		document.open();
		
		//TODO This is ugly, this function determines how many observations and targets fit on the page
		computeOverflowcount(proposal);
		
		// get first and second page details.
		addPdf(pdfCopy, new PdfReader(getProposalFirstPage(proposal)));
		
		byte[] secondPage =getProposalSecondPage(proposal);
		
		addPdf(pdfCopy, new PdfReader(secondPage));
		// add science / technical justification
		addPdf(pdfCopy, new PdfReader(getFiles(proposal)));
		// add poor weather backup and other info. 
		addPdf(pdfCopy, new PdfReader(getAdditionalPages(proposal)));
		
		document.close();

	}
	
	private void computeOverflowcount(Proposal proposal) throws IOException {
		// the applicants
		// the observations
		byte[] countOverflowPage= getApplicantSize(proposal);
		int tries = 0;
		int count = DocumentWriter.getPdfSize(countOverflowPage).intValue();
		// tries should actually be: max allowed observations-15
		while(count >1&&tries<20){
			applicantOverflow--;
			countOverflowPage= getApplicantSize(proposal);
			count = DocumentWriter.getPdfSize(countOverflowPage).intValue();
			tries++;
		}
		
		// the observations
		countOverflowPage= getObservationSize(proposal);
		tries = 0;
		count = DocumentWriter.getPdfSize(countOverflowPage).intValue();
		// tries should actually be: max allowed observations-15
		while(count >1&&tries<20){
			observationOverflow--;
			countOverflowPage= getObservationSize(proposal);
			count = DocumentWriter.getPdfSize(countOverflowPage).intValue();
			tries++;
		}
		// and now for the targets:
		countOverflowPage= getTargetSize(proposal);
		tries = 0;
		count = DocumentWriter.getPdfSize(countOverflowPage).intValue();
		// tries should actually be: max allowed targets-20
		while(count >1&&tries<30){
			targetOverflow--;
			countOverflowPage= getTargetSize(proposal);
			count = DocumentWriter.getPdfSize(countOverflowPage).intValue();
			tries++;
		}
		
	}
	
	private byte[] getApplicantSize(Proposal proposal) {
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN); // a4size = 842 
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		// create pdf writer
		PdfWriter pdfWriter;
		try {
			pdfWriter = PdfWriter.getInstance(document,byteArrayOutputStream);
			PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(null,
				header, FOOTER_POSITION, 0, HEADER_SIZE, this);
			pdfWriter.setPageEvent(pageEvent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();
		PdfPTable countPageTable = getDefaultTable(1);
		try {
			// first i create a fixed table length 496 
			PdfPCell cell = getBorderlessCell();
	    	cell.addElement(getParagraphDefaultSubTitle(" length test "));
	    	float otherHeight = PageSize.A4.getHeight() - APPLICANTS_HEIGHT -TOP_MARGIN -BOTTOM_MARGIN;
	    	cell.setFixedHeight(otherHeight);
	    	countPageTable.addCell(cell);
	    	
	    	List telescopeConfigurations = OptionsUtils.getLabelValueBeans(
	        		OpticonConstants.TELESCOPE_CONFIGURATIONS, new HashMap(), contextConfiguration);
			
    		cell = getBorderlessCell();
    		cell.addElement(getParagraphDefaultTitle("Telescopes"));
    		addTelescopeDetails((OpticonObservingRequest) proposal.getObservingRequest(), cell);
    		countPageTable.addCell(cell);
		
	    	addApplicants(proposal,countPageTable,0,false);
			int applicantcount = proposal.getMembers().size();
			if(applicantcount > applicantOverflow){//add the warning line
				cell= getBorderlessCell();
				cell.addElement(getParagraphNormal("Applicants are continued on the last page", EMPTY));
				countPageTable.addCell(cell);
			}
			addContactAuthor(proposal, countPageTable);
			
			document.add(countPageTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
		return byteArrayOutputStream.toByteArray();
	}

	private byte[] getObservationSize(Proposal proposal) {
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN); // a4size = 842 
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		// create pdf writer
		PdfWriter pdfWriter;
		try {
			pdfWriter = PdfWriter.getInstance(document,byteArrayOutputStream);
			PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(null,
				header, FOOTER_POSITION, 0, HEADER_SIZE, this);
			pdfWriter.setPageEvent(pageEvent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();
		PdfPTable countPageTable = getDefaultTable(1);
		try {
			// first i create a fixed table length 496 
			PdfPCell cell = getBorderlessCell();
	    	cell.addElement(getParagraphDefaultSubTitle(" length test "));
	    	float otherHeight = PageSize.A4.getHeight() - OBSERVATION_HEIGHT -TOP_MARGIN -BOTTOM_MARGIN;
	    	cell.setFixedHeight(otherHeight);
	    	countPageTable.addCell(cell);
			addObservations(proposal,countPageTable,0,false);
			
			document.add(countPageTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
		return byteArrayOutputStream.toByteArray();
	}


	private byte[] getTargetSize(Proposal proposal) {
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN); // a4size = 842 
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		// create pdf writer
		PdfWriter pdfWriter;
		try {
			pdfWriter = PdfWriter.getInstance(document,byteArrayOutputStream);
			PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(null,
				header, FOOTER_POSITION, 0, HEADER_SIZE, this);
			pdfWriter.setPageEvent(pageEvent);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.open();
		PdfPTable countPageTable = getDefaultTable(1);
		try {
			// first i create a fixed table length 496 because  
			PdfPCell cell = getBorderlessCell();
	    	cell.addElement(getParagraphDefaultSubTitle(" target length test "));
	    	float otherHeight = PageSize.A4.getHeight() - TARGETS_HEIGHT -TOP_MARGIN -BOTTOM_MARGIN;
	    	cell.setFixedHeight(otherHeight);
	    	countPageTable.addCell(cell);
			
	    	addTargets(proposal,countPageTable,0,false);
			
			document.add(countPageTable);
		} catch (DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		document.close();
		return byteArrayOutputStream.toByteArray();
	}


	protected Paragraph getParagraphSmallTitle(String text) 
	{
		Paragraph title = new Paragraph(text, OTHER_SUB_TITLE_FONT);
		return title;
	}

	protected void updateHeaderImage(Proposal proposal, ServletContext servletContext)
		throws DocumentException, IOException 
	{
		/*
		 * put the telescopes in a hashmap, with the telescope as the key and
		 * the sum of nights as the value. (How to find the max value then [set
		 * of keys -> max funtion])?
		 */

		String maxTelescope = "";
		double maxTelescopeValue = 0;
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) proposal
				.getObservingRequest();
		Iterator obsIt = opticonObservingRequest.getObservations().iterator();
		Set telescopeSet = new HashSet();
		// Sets only have unique values (removing double telescopes)
		while (obsIt.hasNext())
		{
			OpticonObservation observation = (OpticonObservation) obsIt.next();
			if (observation.getTelescopeConfiguration() != null)
			{
				telescopeSet.add(observation.getTelescopeConfiguration());
			}
		}
		Iterator telescopeIt = new ArrayList(telescopeSet).iterator();
		while (telescopeIt.hasNext())
		{
			// sum up the number of nights per telescope
			double result = 0;
			String telescopeName = (String) telescopeIt.next();
			Double nights = NorthStarDelegate.getResourceValue(
					opticonObservingRequest.getAllocations(), telescopeName
							+ '_' + OpticonConstants.BRIGHT);
			if (nights != null)
			{
				result += nights.doubleValue();
			}
			nights = NorthStarDelegate.getResourceValue(opticonObservingRequest
					.getAllocations(), telescopeName + '_'
					+ OpticonConstants.DARK);
			if (nights != null)
			{
				result += nights.doubleValue();
			}
			nights = NorthStarDelegate.getResourceValue(opticonObservingRequest
					.getAllocations(), telescopeName + '_'
					+ OpticonConstants.FIRST_Q);
			if (nights != null)
			{
				result += nights.doubleValue();
			}
			nights = NorthStarDelegate.getResourceValue(opticonObservingRequest
					.getAllocations(), telescopeName + '_'
					+ OpticonConstants.LAST_Q);
			if (nights != null)
			{
				result += nights.doubleValue();
			}
			// OHP has multiple telescope but have one logo
			if (telescopeName.startsWith("OHP"))
			{
				telescopeName = "OHP";
			}
			if (result == maxTelescopeValue)
			{
				/*
				 *  the guard below is only possible when the maxTelescope and telescopeName
				 *  are both OHP
				 */
				if (!telescopeName.equals(maxTelescope))
				{
					maxTelescope = "";
					maxTelescopeValue = result;
				}
			}
			else if (result > maxTelescopeValue)
			{
				maxTelescope = telescopeName;
				maxTelescopeValue = result;
			}
		}
		// disable theprefered telescope header function
		/*
		if (!maxTelescope.equals("") && !maxTelescope.equals("UKIRT"))
		{
			TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
			PdfUtils pdfUtils = new PdfUtils();
			
			String imageUri = telescopeConfiguration.getPdfHeaderImageUri();
			this.setHeaderImage(pdfUtils.getHeader(createNewHeader(maxTelescope,
					imageUri), servletContext));
			
		}
		*/
	}

	protected String createNewHeader(String prefix, String oldPath)
	{
		String[] parts = oldPath.split("/");
		String oldHeader = parts[parts.length-1];
		String newHeader = prefix + oldHeader;
		return oldPath.replaceAll(oldHeader, newHeader);
	}
	
	protected byte[] getProposalFirstPage(Proposal proposal) throws DocumentException {
		/*
		 * create new document
		 */
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN + FIRST_HEADER_SIZE, BOTTOM_MARGIN);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		float firstHeaderSize = 0;
		firstHeaderSize = FIRST_HEADER_SIZE;

		// create pdf writer
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				byteArrayOutputStream);
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(firstHeader,
				header, FOOTER_POSITION, firstHeaderSize, HEADER_SIZE, this);

		pdfWriter.setPageEvent(pageEvent);
		/*
		 * this must before open, then all pages has footer. if after
		 * document.open, first page is without footer
		 */

		document.open();
		document.setMargins(LEFT_MARGIN, RIGHT_MARGIN, TOP_MARGIN,
				BOTTOM_MARGIN);
		
		PdfPTable firstPageTable = getDefaultTable(1);

		// addTitle(proposal, firstPageTable);
		addSemester(proposal, firstPageTable);
		addAbstract(proposal, firstPageTable);
		
		PdfPCell cell = getBorderlessCell();
		
		List telescopeConfigs = OptionsUtils.getLabelValueBeans(
        		OpticonConstants.TELESCOPE_CONFIGURATIONS, new HashMap(), contextConfiguration);
		if(telescopeConfigs != null && telescopeConfigs.size() > 2){
			cell.addElement(getParagraphDefaultTitle("Telescopes"));
			addTelescopeDetails((OpticonObservingRequest) proposal.getObservingRequest(), cell);
			firstPageTable.addCell(cell);
		}else{
			APPLICANTS_HEIGHT= 320f;
		}
		//addRequestedTimeInfo(proposal.getObservingRequest(), firstPageTable);
		int applicantcount = proposal.getMembers().size();
		
		addApplicants(proposal, firstPageTable,0,true);
		
		if(applicantcount > applicantOverflow){//add the warning line
			cell= getBorderlessCell();
			cell.addElement(getParagraphNormal("Applicants are continued on the last page", EMPTY));
			firstPageTable.addCell(cell);
		}
		addContactAuthor(proposal, firstPageTable);
		document.add(firstPageTable);
		document.close();
		return (byteArrayOutputStream.toByteArray());
	}
	
	private byte[] getProposalSecondPage(Proposal proposal) throws DocumentException {
		int count=0;
		
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		// create pdf writer
		PdfWriter pdfWriter = PdfWriter.getInstance(document,byteArrayOutputStream);
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(null,
				header, FOOTER_POSITION, 0, HEADER_SIZE, this);

		pdfWriter.setPageEvent(pageEvent);

		document.open();
		PdfPTable secondPageTable = getDefaultTable(1);
		// addObservationsSummaries(proposal, secondPageTable);
		//addTimeSummary(proposal, secondPageTable);
		addObservingRequest(proposal.getObservingRequest(), secondPageTable);

		int observationcount = addObservations(proposal,secondPageTable,0,true);
		if(observationcount >= observationOverflow ){
			PdfPCell cell= getBorderlessCell();
			cell.addElement(getParagraphNormal("Observing runs are continued on the last page", EMPTY));
			secondPageTable.addCell(cell);
		}

		addTargets(proposal,secondPageTable,0,true);
		int targetCount=proposal.getObservingRequest().getTargets().size();
		if(targetCount > targetOverflow ){
			PdfPCell cell= getBorderlessCell();
			cell.addElement(getParagraphNormal("Targets are continued on the last page", EMPTY));
			secondPageTable.addCell(cell);
		}
		
		document.add(secondPageTable);
		document.close();
		
		return byteArrayOutputStream.toByteArray();

	}

	

	private int addObservations(Proposal proposal, PdfPTable secondPageTable, int startCount, boolean fixed) throws DocumentException {
		
		// get header names 
		String[] tableHeaders=new String[10];
		try {
			tableHeaders[0] = NorthStarUtils.getLabel(null,proposal, "label.opticon.observationtable.run");
			tableHeaders[1] = NorthStarUtils.getLabel(null,proposal, "label.opticon.observationtable.telescope");
			tableHeaders[2] = NorthStarUtils.getLabel(null,proposal, "label.opticon.observationtable.instrument");
			tableHeaders[3] = NorthStarUtils.getLabel(null,proposal, "label.opticon.observationtable.nights");
			tableHeaders[4] = NorthStarUtils.getLabel(null,proposal, "label.opticon.observationtable.moon");
			tableHeaders[5] = NorthStarUtils.getLabel(null,proposal, "label.opticon.observationtable.weather");
			tableHeaders[6] = NorthStarUtils.getLabel(null,proposal, "label.opticon.observationtable.mode");
			tableHeaders[7] = NorthStarUtils.getLabel(null,proposal, "label.opticon.observationtable.seeing");
			tableHeaders[8] = NorthStarUtils.getLabel(null,proposal, "label.opticon.observationtable.config");
			tableHeaders[9] = NorthStarUtils.getLabel(null,proposal, "label.opticon.observationtable.details");
			
		} catch (FileNotFoundException fe) {
			fe.printStackTrace();
		}
        
        // create header
		int columns = tableHeaders.length;
        PdfPTable observationTable = getTableNoSplit(columns);
        // widths for run, telescope, instrument, nights, moon, weather, mode, seeing, config, details
        observationTable.setWidths(new int[] { 3, 8, 10, 8, 4, 5 , 5, 5, 23,22});
        
        PdfPCell cell = getBorderlessCell();
		cell.addElement(getParagraphDefaultTitle("Observing runs"));
        cell.setColspan(columns);
        observationTable.addCell(cell);
        
        for (int i = 0; i < columns; i++){
        	observationTable.addCell(getTableHeaderCellSmall(tableHeaders[i]));
        }
		
		// add observation details
		Iterator obsIterator = proposal.getObservingRequest().getObservations().iterator();
		int observationcount = 0;
		int endCount = observationOverflow;
		if(startCount > 0){
			endCount = proposal.getObservingRequest().getObservations().size()+1;
		}
		while (obsIterator.hasNext()){
			// for each observation, show instrument and target list.
			OpticonObservation observation = (OpticonObservation) obsIterator.next();
			if(observationcount >= startCount && observationcount < endCount){
				char chr = (char) (observationcount+65);
				String observationRun= " "+String.valueOf(chr);
				observationTable.addCell(getTableNumbersCellSmall(observationRun));
				//addObservationDetails(proposal,observation, observationId,secondPageTable);
				observationTable.addCell(getTableNumbersCellSmall(observation.getTelescopeConfiguration()));
				observationTable.addCell(getTableNumbersCellSmall(observation.getInstrument().getName()));
				Double specifiedTime = NorthStarDelegate.getResourceValue(
						observation.getAllocations(),observation.getTelescopeConfiguration()+"_"+ OpticonConstants.REQUESTEDTIME );
				
								
				String suffix = timeSuffix(observation);
				String nights = "0";
	        	if (specifiedTime != null){
	        		DecimalFormat twoPlaces = new DecimalFormat("0");
	        		
	        		nights = twoPlaces.format(specifiedTime.doubleValue())+suffix;
		        	specifiedTime = NorthStarDelegate.getResourceValue(
							observation.getAllocations(),observation.getTelescopeConfiguration()+"_"+ OpticonConstants.MINIMUMTIME );
		        	if (specifiedTime != null){
		        		nights += " ("+twoPlaces.format(specifiedTime.doubleValue())+suffix+") ";
		        	}
	        	}
	        	observationTable.addCell(getTableNumbersCellSmall(nights));
	        	observationTable.addCell(getTableNumbersCellSmall(observation.getObservationPhase()));

	        	if(observation.getObservationWeather() != null){
	        		String weather = observation.getObservationWeather();
	        		if(weather.startsWith("pho")) weather = "Pm";
	        		if(weather.startsWith("lig")) weather = "Lc";
	        		if(weather.startsWith("clear")) weather = "Clr";
	        		observationTable.addCell(getTableNumbersCellSmall(weather));
	        	}else{
	        		observationTable.addCell(getTableNumbersCellSmall(" "));
	        	}
	        	observationTable.addCell(getTableNumbersCellSmall(observation.getObservationMode()));
	        	observationTable.addCell(getTableNumbersCellSmall(observation.getObservationSeeing()));
	        	String instrumentConfiguration = getShortInstrumentDetails(observation);
	        	
	        	
	        	observationTable.addCell(getTableNumbersCellSmall(instrumentConfiguration));
	        	if(!AstronValidator.isBlankOrNull(observation.getInstrument().getComments() )){
	        		observationTable.addCell(getTableNumbersCellSmall(observation.getInstrument().getComments() 
	        					+ " " + observation.getRequiredSchedConstraints()));
	        	}else{
	        		observationTable.addCell(getTableNumbersCellSmall(observation.getRequiredSchedConstraints()));
	        	}
	        	
				
			}
			observationcount++;
		}
		
		cell = getBorderlessCell();
		if(fixed){
			cell.setFixedHeight(OBSERVATION_HEIGHT );
		}
		cell.addElement(observationTable);
		
		secondPageTable.addCell(cell);
		// blankline
		//cell = getBorderlessCell();		
		//secondPageTable.addCell(cell);
		
		//PdfPCell cell = getTableCell();
		//cell.addElement(observationTable);
		//secondPageTable.addCell(cell);
		
		return observationcount;
	}

	private String timeSuffix(OpticonObservation observation) {
		// this is a very long routine just to add one little character for one telescope...
		OpticonInstrument instrument = observation.getInstrument();
		if(instrument==null || instrument.getName() ==null){
			return "";
		}
		Map enteredValues = new HashMap();
        enteredValues.put(OpticonConstants.TELESCOPE_CONFIGURATIONS,
                OptionsUtils.getList(observation.getTelescopeConfiguration()));

       
		 List configurations = OptionsDelegate.getOptionsConfigurationTypes(
	                OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredValues,
	                contextConfiguration);
	        ConfigureOptionType configureOptionType = (ConfigureOptionType) OptionsDelegate
	                .getOption(instrument.getName(), configurations);
	    if(configureOptionType == null){
	    	return "";
	    }
	        
		FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) configureOptionType.getSubfields();
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TELESCOPE_HOURS, fieldsDefinitionType)){
            return "h";
            }else{
            	return "n";
            }

	}


	private String getShortInstrumentDetails(OpticonObservation observation) {
		StringBuffer shortDetails = new StringBuffer();
		Map enteredValues = new HashMap();
        enteredValues.put(OpticonConstants.TELESCOPE_CONFIGURATIONS,
                OptionsUtils.getList(observation.getTelescopeConfiguration()));

        OpticonInstrument instrument = observation.getInstrument();
        List configurations = OptionsDelegate.getOptionsConfigurationTypes(
                OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredValues,
                contextConfiguration);
        ConfigureOptionType configureOptionType = (ConfigureOptionType) OptionsDelegate
                .getOption(instrument.getName(), configurations);

        if (configureOptionType != null
                && configureOptionType.getSubfields() != null) {
            FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) configureOptionType
                    .getSubfields();
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_FRAME_EXPOSURE_TIME_DETAILS,
                    fieldsDefinitionType)) {
                if (instrument.isNonDefaultFrameExposureTime()) {
                     shortDetails.append("Frame Exp Time: ");
                     shortDetails.append(instrument.getNonDefaultFrameExposureTimeDetails());
                     shortDetails.append(" ");
                }
            }
            if (OptionsDelegate.allowedToDisplay(OpticonConstants.INSTRUMENT_FILTER, fieldsDefinitionType)){
            	String filterStr = "";
            	if (instrument.getFilters()!= null){
            		Iterator filterIt = instrument.getFilters().iterator();
            		while (filterIt.hasNext()){
            			if (filterStr == ""){
            				filterStr = (String) filterIt.next();
            			}else{
            				filterStr += ", " + (String) filterIt.next();
            			}
            		}
            	}
            	
            	shortDetails.append("Filter: ");
            	shortDetails.append(filterStr);
            	shortDetails.append(" ");
            }
            
            if (OptionsDelegate.allowedToDisplay(OpticonConstants.INSTRUMENT_FILTERW2, fieldsDefinitionType)){
            	String filterStr = "";
            	if (instrument.getFiltersw2()!= null){
            		Iterator filterIt = instrument.getFiltersw2().iterator();
            		while (filterIt.hasNext()){
            			if (filterStr == ""){
            				filterStr = (String) filterIt.next();
            			}else{
            				filterStr += ", " + (String) filterIt.next();
            			}
            		}
            	}
             	shortDetails.append("Filterw2: ");
            	shortDetails.append(filterStr);
            	shortDetails.append(" ");
            }
            
            
            if (OptionsDelegate.allowedToDisplay(OpticonConstants.INSTRUMENT_GRATINGW, fieldsDefinitionType)){
            	String gratStr = "";
            	if (instrument.getGratingw()!= null){
            		Iterator gratIt = instrument.getGratingw().iterator();
            		while (gratIt.hasNext()){
            			if (gratStr == ""){
            				gratStr = (String) gratIt.next();
            			}else{
            				gratStr += ", " + (String) gratIt.next();
            			}
            		}
            	}
             	shortDetails.append("Gratings: ");
            	shortDetails.append(gratStr);
            	shortDetails.append(" ");
            }
            
            if (OptionsDelegate.allowedToDisplay(OpticonConstants.INSTRUMENT_GRISMW, fieldsDefinitionType)){
            	String grismStr = "";
            	if (instrument.getGrismw()!= null){
            		Iterator grismIt = instrument.getGrismw().iterator();
            		while (grismIt.hasNext()){
            			if (grismStr == ""){
            				grismStr = (String) grismIt.next();
            			}else{
            				grismStr += ", " + (String) grismIt.next();
            			}
            		}
            	}
             	shortDetails.append("Grisms: ");
            	shortDetails.append(grismStr);
            	shortDetails.append(" ");
            }
            
            if (OptionsDelegate.allowedToDisplay(OpticonConstants.INSTRUMENT_SLITW, fieldsDefinitionType)){
            	String slitStr = "";
            	if (instrument.getSlitw()!= null){
            		Iterator slitIt = instrument.getSlitw().iterator();
            		while (slitIt.hasNext()){
            			if (slitStr == ""){
            				slitStr = (String) slitIt.next();
            			}else{
            				slitStr += ", " + (String) slitIt.next();
            			}
            		}
            	}
             	shortDetails.append("Slits: ");
            	shortDetails.append(slitStr);
            	shortDetails.append(" ");
            }
            
            shortDetails.append(addFilterDetails(instrument));
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_MODE, fieldsDefinitionType)) {
            	shortDetails.append("Observing mode: ");
            	shortDetails.append(instrument.getMode());
            	shortDetails.append(" ");
            }
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_POLARIMETRY,
                    fieldsDefinitionType)) {
                if (instrument.isPolarimetry()) {
                	shortDetails.append("Polarimetry ");
                }
               
            }
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_GUIDELINE,
                    fieldsDefinitionType)) {
                if (instrument.isGuideline()) {
                	shortDetails.append("no Guideline "); // not accepted
                }
            }
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_WAVELENGTH,
                    fieldsDefinitionType)) {
            	shortDetails.append("Wavelength: ");
            	shortDetails.append(instrument.getWavelength());
            	shortDetails.append(" ");
            }
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_ORDER, fieldsDefinitionType)) {
            	shortDetails.append("Order: ");
            	shortDetails.append(AstronConverter.toString(instrument.getOrder()));
            	shortDetails.append(" ");
            }
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_SLIT, fieldsDefinitionType)) {
            	shortDetails.append("Slit: ");
            	shortDetails.append(instrument.getSlit());
            	shortDetails.append(" ");
            }
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_SLIT_POSITION_ANGLE,
                    fieldsDefinitionType)) {
            	shortDetails.append("Slit position angle: ");
            	shortDetails.append(AstronConverter.toString(instrument
                                .getSlitPositionAngle()));
            	shortDetails.append(" ");
            }
            if (OptionsDelegate
                    .allowedToDisplay(OpticonConstants.INSTRUMENT_GRATING,
                            fieldsDefinitionType)) {
            	shortDetails.append("Grating: ");
            	shortDetails.append(instrument.getGrating());
            	shortDetails.append(" ");
            }
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_ORDER_FILTER, fieldsDefinitionType)) {
            	shortDetails.append("Order separation filter: ");
            	shortDetails.append(instrument.getOrderFilter());
            	shortDetails.append(" ");
            }
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_READOUT, fieldsDefinitionType)) {
            	shortDetails.append("Read-out mode : ");
            	shortDetails.append(instrument.getReadOut());
            	shortDetails.append(" ");
            }                
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_GRISM, fieldsDefinitionType)) {
            	shortDetails.append("Grism: ");
            	shortDetails.append(instrument.getGrism());
            	shortDetails.append(" ");
            }
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_MICROSTEPPING,
                    fieldsDefinitionType)) {
            	shortDetails.append("Stepping: ");
            	shortDetails.append(instrument.getMicrostepping());
            	shortDetails.append(" ");
            }

            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.INSTRUMENT_CORONAGRAPHIC_MASK,
                    fieldsDefinitionType)) {
                if (instrument.isCoronagraphicMask()) {
                	shortDetails.append("Coronagraphic Mask");
                	shortDetails.append(" ");
                }
            }
            
            if (OptionsDelegate.allowedToDisplay(
                    OpticonConstants.TELESCOPE_SCHEDULING,
                    fieldsDefinitionType)) {
            	shortDetails.append("Scheduling: ");
            	shortDetails.append(instrument.getTelescopeScheduling());
            	shortDetails.append(" ");
            }
        }
        return shortDetails.toString();
	}

	protected byte[] getProposalPdf(Proposal proposal) throws DocumentException {
		/*
		 * create new document
		 */
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN + FIRST_HEADER_SIZE, BOTTOM_MARGIN);

		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		float firstHeaderSize = 0;
		firstHeaderSize = FIRST_HEADER_SIZE;

		// create pdf writer
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				byteArrayOutputStream);
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(firstHeader,
				header, FOOTER_POSITION, firstHeaderSize, HEADER_SIZE, this);

		pdfWriter.setPageEvent(pageEvent);
		/*
		 * this must before open, then all pages has footer. if after
		 * document.open, first page is without footer
		 */

		document.open();
		document.setMargins(LEFT_MARGIN, RIGHT_MARGIN, TOP_MARGIN,
				BOTTOM_MARGIN);
		
		PdfPTable firstPageTable = getDefaultTable(1);

		// addTitle(proposal, firstPageTable);
		addSemester(proposal, firstPageTable);
		addAbstract(proposal, firstPageTable);
		
		PdfPCell cell = getBorderlessCell();
		cell.addElement(getParagraphDefaultTitle("Telescopes"));
		addTelescopeDetails((OpticonObservingRequest) proposal.getObservingRequest(), cell);
		firstPageTable.addCell(cell);
		//addRequestedTimeInfo(proposal.getObservingRequest(), firstPageTable);
		
		addApplicants(proposal, firstPageTable);
		addContactAuthor(proposal, firstPageTable);
		
		// page 2
		document.add(firstPageTable);
		document.newPage();
		PdfPTable secondPageTable = getDefaultTable(1);
		// addObservationsSummaries(proposal, secondPageTable);
		addTimeSummary(proposal, secondPageTable);
		addObservingRequest(proposal.getObservingRequest(), secondPageTable);
		
		Iterator obsIterator = proposal.getObservingRequest().getObservations().iterator();
		int observationcount =0;
		while (obsIterator.hasNext()){
			// for each observation, show instrument and target list.
			char chr = (char) (observationcount+65);
			String observationId= String.valueOf(chr);
			observationcount++;
			
			addObservationDetails(proposal,(Observation) obsIterator.next(), observationId,secondPageTable);
		}
		addTargets(proposal,secondPageTable,0,false);
		
		document.add(secondPageTable);
		document.close();
		
		return byteArrayOutputStream.toByteArray();
	}
	
	private void addTargets(Proposal proposal, PdfPTable rootTable,int startCount, boolean fixed) {
		FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		
		List summaryColumns = createSummaryColumns(fieldsDefinitionType);
    	
        String[] tableHeaders = new String[summaryColumns.size()];
        Iterator columnIt = summaryColumns.iterator();
        int ci=0;
        while (columnIt.hasNext()){
        	String name = (String) columnIt.next();
        	try{
        		tableHeaders[ci] = NorthStarUtils.getLabel(null,proposal, "label.opticon.targettable."+name);//opticonLabels.getMessage("label.opticon.targettable."+name);
        	}catch(FileNotFoundException fe){
        		log.warn(" "+fe.getMessage());
        	}
        	ci++;
        }
        int columns = tableHeaders.length;
        
        /*
         * add table header
         */
        PdfPTable observationTable = getTableNoSplit(columns);
        PdfPCell cell = getBorderlessCell();
		cell.addElement(getParagraphDefaultTitle("Targets"));
        cell.setColspan(columns);
        observationTable.addCell(cell);
        
        for (int i = 0; i < columns; i++)
        {
            observationTable.addCell(getTableHeaderCellSmall(tableHeaders[i]));
        }
		/* 
         * fill the table rows with target detais
         */
        int targetCount=0;
        int endCount = targetOverflow;
		if(startCount > 0){
			endCount = proposal.getObservingRequest().getTargets().size()+1;
		}
        Iterator targetsIt =  proposal.getObservingRequest().getTargets().iterator();
        while (targetsIt.hasNext())
        {
         	OpticonTarget opticonTarget = (OpticonTarget) targetsIt.next();
         	if(targetCount >= startCount && targetCount < endCount){
         		addTargetDetails(observationTable,opticonTarget,proposal);
	         }
         	targetCount++;
        }
		
        // adding it all up to for a nice table///
        cell = getBorderlessCell();
        if(fixed){
        	cell.setFixedHeight(TARGETS_HEIGHT);
        }
		cell.addElement(observationTable);
		rootTable.addCell(cell);
		// blankline
		//cell = getBorderlessCell();
		//rootTable.addCell(cell);
	}
	
	/*
	private void addOverflowedTargets(Proposal proposal, PdfPTable pageTable) {
		FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		
		List summaryColumns = createSummaryColumns(fieldsDefinitionType);
    	
        String[] tableHeaders = new String[summaryColumns.size()];
        Iterator columnIt = summaryColumns.iterator();
        int ci=0;
        while (columnIt.hasNext()){
        	String name = (String) columnIt.next();
        	try{
        		tableHeaders[ci] = NorthStarUtils.getLabel(null,proposal, "label.opticon.targettable."+name);//opticonLabels.getMessage("label.opticon.targettable."+name);
        	}catch(FileNotFoundException fe){
        		log.warn(" "+fe.getMessage());
        	}
        	ci++;
        }
        int columns = tableHeaders.length;
        PdfPTable observationTable = getTableNoSplit(columns);
        for (int i = 0; i < columns; i++){
            observationTable.addCell(getTableHeaderCellSmall(tableHeaders[i]));
        }
		
        int targetCount=0;
        Iterator targetsIt =  proposal.getObservingRequest().getTargets().iterator();
        while (targetsIt.hasNext())
        {
         	OpticonTarget opticonTarget = (OpticonTarget) targetsIt.next();
         	if(targetCount >= targetOverflow){
         		// add the targets
         		addTargetDetails(observationTable,opticonTarget,proposal);
	         }
         	targetCount++;
        }
		
        // adding it all up to for a nice table///
        PdfPTable extraTable = getTableNoSplit(1);
        
		PdfPCell telescopeCell = getTableHeaderCell("Targets ");
		extraTable.addCell(telescopeCell);
        
		PdfPCell cell = getTableCell();
		cell.addElement(observationTable);
		extraTable.addCell(cell);
		// blankline
		cell = getBorderlessCell();		
		pageTable.addCell(cell);
		cell = getTableCell();
		cell.addElement(extraTable);
		pageTable.addCell(cell);

		
	}
*/
	protected void addApplicants(Proposal proposal, PdfPTable rootTable, int startCount, boolean fixed)
	throws DocumentException {
		int columns = 6;
		
		PdfPCell applicantsCell = getBorderlessCell();
		applicantsCell.addElement(getParagraphDefaultTitle("Applicants"));
		
		PdfPTable applicantsTable = getTableNoSplit(columns);
		applicantsTable.setWidths(new int[] { 30, 20, 25, 15, 3, 8 });
		/*
		 * add table header
		 */
		applicantsTable.addCell(getTableHeaderCell("Name"));
		applicantsTable.addCell(getTableHeaderCell("Affiliation"));
		applicantsTable.addCell(getTableHeaderCell("Email"));
		applicantsTable.addCell(getTableHeaderCell("Country"));
		applicantsTable.addCell(getTableHeaderCell(""));
		applicantsTable.addCell(getTableHeaderCell("Potential observer"));
		/*
		 * add applicants to table
		 */
		int applicantCount = 0;
		int endCount = proposal.getMembers().size();
		if(startCount == 0 && proposal.getMembers().size() > applicantOverflow){
			endCount = applicantOverflow;
		}
		for (int i = startCount; i < endCount; i++) {
			Member member = (Member) proposal.getMembers().get(i);
			applicantsTable.addCell(getTableRequiredTextCell(member.getName()
					.trim()));
			applicantsTable.addCell(getTableRequiredTextCell(member
					.getAffiliation()));
			applicantsTable
					.addCell(getTableRequiredTextCell(member.getEmail()));
			applicantsTable.addCell(getTableRequiredTextCell(member
					.getCountry()));
			if (member.isPi()) {
				applicantsTable.addCell(getTableTextCell("Pi"));
			} else {
				applicantsTable.addCell(getTableTextCell(null));
			}
		
			if (member.isPotentialObserver()) {
				applicantsTable.addCell(getTableTextCell("Yes"));
			} else {
				applicantsTable.addCell(getTableTextCell(null));
			}
		}
		
		applicantsCell.addElement(applicantsTable);
		addContactAuthor(proposal.getMembers(),applicantsCell);
		rootTable.addCell(applicantsCell);
	
	}


	private void addTargetDetails(PdfPTable observationTable,
			OpticonTarget opticonTarget,Proposal proposal) {
		FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();

		/*
         * add field name
         */
        observationTable.addCell(getTableNumbersCellSmall(opticonTarget
                .getFieldName()));
        /*
         * add ra & dec and other target information
         */
        if (opticonTarget.getRa() != null
                && opticonTarget.getDecl() != null) {
            AstroCoordinate coordinate = new AstroCoordinate(opticonTarget
                    .getRa().doubleValue(), opticonTarget.getDecl()
                    .doubleValue());
            observationTable
                    .addCell(getTableNumbersCellSmall(coordinate.RAString()));
            observationTable
                    .addCell(getTableNumbersCellSmall(coordinate.DecString()));
        } else {
            observationTable.addCell(getTableNumbersCellSmall(null));
            observationTable.addCell(getTableNumbersCellSmall(null));
        }
        observationTable.addCell(getTableNumbersCellSmall(opticonTarget
                .getEpoch()));
		Double totalDuration = NorthStarDelegate.getResourceValue(
				opticonTarget.getAllocations(),
				OpticonConstants.TOTAL_DURATION);
        observationTable
                .addCell(getTableNumbersCellSmall(AstronConverter
                        .getHoursFromSeconds(totalDuration)));
        // add observing runs
        String runs=fillSelectedRuns(proposal.getObservingRequest().getObservations(),opticonTarget);
        
        
        observationTable.addCell(getTableNumbersCellSmall(runs));
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_MOON, fieldsDefinitionType))
		{ 
        	observationTable
            	.addCell(getTableNumbersCellSmall(opticonTarget.getMoon()));
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING, fieldsDefinitionType))
		{
			observationTable
            	.addCell(getTableNumbersCellSmall(opticonTarget.getSeeing()));
			observationTable.addCell(getTableCell());
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING_RANGE, fieldsDefinitionType))
		{
			observationTable
				.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getSeeingLower())));
			observationTable
        		.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getSeeingUpper())));
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_WATER, fieldsDefinitionType))
		{
			observationTable
				.addCell(getTableNumbersCellSmall(opticonTarget.getWater()));
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SN, fieldsDefinitionType))
		{ 
			observationTable
			 .addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getSn())));
		}
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_MAGNITUDE, fieldsDefinitionType))
		{ 
        	observationTable
				.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getMagnitude())));
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX, fieldsDefinitionType))
		{            
			observationTable
				.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getFlux())));
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX_FREQUENCY, fieldsDefinitionType))
		{            
			observationTable
				.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getFluxFrequency())));
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECTRAL_INDEX, fieldsDefinitionType))
		{            
			observationTable
				.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getSpectralIndex())));
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_DIAMETER, fieldsDefinitionType))
		{            
			observationTable
				.addCell(getTableNumbersCellSmall(AstronConverter.toString(opticonTarget.getDiameter())));
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SKY_QUALITIES, fieldsDefinitionType))
		{            
			observationTable
				.addCell(getTableNumbersCellSmall(opticonTarget.getSkyQuality()));
		}
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_OPPORTUNITY, fieldsDefinitionType))
		{            
			if(opticonTarget.isOpportunity()){
				observationTable
					.addCell(getTableNumbersCellSmall("yes"));
			}else{
				observationTable
				.addCell(getTableNumbersCellSmall(" "));
			}
		}
		/*
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_POSTPROCESSING , fieldsDefinitionType))
		{      
			observationTable
			.addCell(getTableInnerHeaderCell("Post Processing: "));
        	
			PdfPCell postCell = getTableCell();
        	
        	//postCell.addElement(this.getParagraphCellContentTitle("Configuration comments"));
        	if(instrument.isCalibration()){
        		postCell.addElement(this
                        .getParagraphCellContentTitleWithValue(
                                "Calibration : ", "yes", EMPTY));
            } else {
            	postCell.addElement(this
                        .getParagraphCellContentTitleWithValue(
                                "Calibration : ", "no", EMPTY));
        	}
        	if(instrument.isFlagging()){
        		postCell.addElement(this
                        .getParagraphCellContentTitleWithValue(
                                "Flagging : ", "yes", EMPTY));
            } else {
            	postCell.addElement(this
                        .getParagraphCellContentTitleWithValue(
                                "Flagging : ", "no", EMPTY));
        	}
        		
        	cell.addElement(postCell);
			observationTable
				.addCell(getTableNumbersCellSmall(opticonTarget.getSkyQuality()));
			
		}
		*/
        
	    observationTable.addCell(getTableNumbersCellSmall(opticonTarget.getComments()));

		
	}

	private String fillSelectedRuns(List observations,
			OpticonTarget opticonTarget) {
		String result="";
		if (observations == null){
			return "";
		}
		for(int i=0;i<observations.size();i++){
			OpticonObservation obs = (OpticonObservation) observations.get(i);
			List obstargets= obs.getTargets();
			if(obstargets != null){ // new unstored observation
				Iterator obsit = obstargets.iterator();
				while(obsit.hasNext() ){
					OpticonTarget obsTarget = (OpticonTarget) obsit.next();
					if(obsTarget.equals(opticonTarget)){
						char chr = (char) (i+65);
						result += String.valueOf(chr)+" " ;
					}
				}
			}
		}
		return result;
	}

	protected void addContactAuthor(List members, PdfPCell rootCell)
	 	throws DocumentException 
	{
		/* This method is not used for the french telescopes Contact Author
		 * should appear on thefirst page.*/
	}
	
	/**
	 * The addAdditionalIssues adds the additional issues
	 * 
	 * @param proposal
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addAdditionalIssues(Proposal proposal, PdfPTable rootTable)
                    throws DocumentException {
		if (proposal.getAdditionalIssues() == null) {
			/*
			 * make default additionalIssues
			 */
			proposal.setAdditionalIssues(new AdditionalIssues());
		}
		addTheses(proposal.getAdditionalIssues(), rootTable);
		addLinkedProposals(proposal.getAdditionalIssues(), rootTable);
		addLinkedProposalsElsewhere(proposal.getAdditionalIssues(), rootTable);
		
	}		
	/**
	 * The getProposalPdf method generates pdf with proposal info
	 * 
	 * @param proposal
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected byte[] getAdditionalPages(Proposal proposal) throws DocumentException 
	{
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN);
		document.setMarginMirroring(true);
		/*
		 * output it to and byte array output stream
		 */
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		/*
		 * create pdf writer
		 */
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				byteArrayOutputStream);
		/*
		 * it must before open, then all pages has footer. if after
		 * document.open, first page is without footer
		 */
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(null, header,
				FOOTER_POSITION, HEADER_SIZE, HEADER_SIZE, this);

		pdfWriter.setPageEvent(pageEvent);
		document.open();

		// addFirstHeader(proposal, document);
		document.setMargins(LEFT_MARGIN, RIGHT_MARGIN, TOP_MARGIN,
				BOTTOM_MARGIN);
		/*
		 * create root table.
		 */
		PdfPTable pageTable = getDefaultTable(1);
		addAdditionalIssues(proposal, pageTable);
		addAdditions(proposal, pageTable);
		addPreviousAllocations(proposal.getAdditionalIssues(), pageTable);
		addAdditionalRemarks(proposal.getAdditionalIssues(), pageTable);
		addRelatedPublications(proposal.getAdditionalIssues(), pageTable);
		addRelatedPreviousInvolvedProposal(proposal.getAdditionalIssues(), pageTable);
		addObservationInfo(proposal,pageTable);
		
		
		if(proposal.getMembers().size() > applicantOverflow){
			addApplicants(proposal, pageTable,applicantOverflow,false);
		}
		
		OpticonObservingRequest observingRequest = (OpticonObservingRequest) proposal.getObservingRequest();
		if(observingRequest.getObservations().size() >= observationOverflow){
			addObservations(proposal,pageTable,observationOverflow,false);
		}
		if(observingRequest.getTargets().size() > targetOverflow){
			addTargets(proposal,pageTable,targetOverflow,false);
		}
		if (observingRequest.isLongTermProposal() && !AstronValidator.isBlankOrNull(observingRequest.getLongTermProposalSpecifics()))
		{
			String[] longtermSpecifics= splitLongtermSpecifics( observingRequest.getLongTermProposalSpecifics());
			if(longtermSpecifics[1]!=null){
				PdfPCell cell= getBorderlessCell();
				cell.addElement(getParagraphDefaultTitle("Long term proposal specifics continued from page 2 : "));
				cell.addElement(getParagraphNormal(longtermSpecifics[1] , EMPTY));
				pageTable.addCell(cell);
			}
		}

		// add overflowed scheduling constraints
		if(!AstronValidator.isBlankOrNull( proposal.getObservingRequest().getOverallRequiredSchedConstraints() )){
			String[] schedulingConstraints = splitSchedulingConstraints(proposal.getObservingRequest().getOverallRequiredSchedConstraints() );
			if(schedulingConstraints[1] != null ){
				PdfPCell cell= getBorderlessCell();
				cell.addElement(getParagraphDefaultTitle("Scheduling constraints continued from page 2 : "));
				cell.addElement(getParagraphNormal(schedulingConstraints[1] , EMPTY));
				pageTable.addCell(cell);
			}
		}
		
		document.add(pageTable);
	
		document.close();
		return byteArrayOutputStream.toByteArray();
	}	
	
	private void addObservationInfo(Proposal proposal, PdfPTable pageTable) {
		Iterator obsIterator = proposal.getObservingRequest().getObservations().iterator();
		int observationcount =0;
		PdfPCell cell = getBorderlessCell();
		cell.addElement(getParagraphDefaultTitle("Observing run info : "));
		
		while (obsIterator.hasNext()){
			// for each observation, show instrument and target list.
			char chr = (char) (observationcount+65);
			String observationRun= String.valueOf(chr);
			OpticonObservation observation = (OpticonObservation) obsIterator.next();
			
			
			if(!AstronValidator.isBlankOrNull(observation.getBackupStrategy()) && !AstronValidator.isBlankOrNull(observation.getCalibrationRequirements())){
				String observationInfo = "Run: "+observationRun;
				observationInfo += " calibration info: "+observation.getCalibrationRequirements();
				cell.addElement(getParagraphNormal(observationInfo, EMPTY));
				observationInfo = "            backup strategy: "+observation.getBackupStrategy();
				cell.addElement(getParagraphNormal(observationInfo, EMPTY));
			}else if(!AstronValidator.isBlankOrNull(observation.getCalibrationRequirements())){
				String observationInfo = "Run: "+observationRun;
				observationInfo += " calibration info: "+observation.getCalibrationRequirements();
				cell.addElement(getParagraphNormal(observationInfo, EMPTY));
			}else if(!AstronValidator.isBlankOrNull(observation.getBackupStrategy())){
				String observationInfo = "Run: "+observationRun;
				observationInfo += " backup strategy: "+observation.getBackupStrategy();
				cell.addElement(getParagraphNormal(observationInfo, EMPTY));
			}
			observationcount++;
		}
		pageTable.addCell(cell);
	}

	/*
	private void addOverflowedObservations(Proposal proposal,
			PdfPTable pageTable) throws DocumentException {
		Iterator obsIterator = proposal.getObservingRequest().getObservations().iterator();
		int overflowcount = observationOverflow -1;
		int observationcount =0;
		while (obsIterator.hasNext()){
			// for each observation, show instrument and target list.
			OpticonObservation observation = (OpticonObservation) obsIterator.next();
			if(observationcount >= overflowcount){
				char chr = (char) (observationcount+65);
				String observationId= String.valueOf(chr);
				addObservationDetails(proposal,observation, observationId,pageTable);
			}
			observationcount++;
		}
		
	}
*/
	protected void addAdditions(Proposal proposal, PdfPTable rootTable) 
	{
		OpticonObservingRequest observingRequest = 
			(OpticonObservingRequest) proposal.getObservingRequest();
		FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.ALLOCATION_JUSTIFICATION,
						fieldsDefinitionType))
		{
			PdfPCell cell = getTableCell();
			cell.setFixedHeight(200f);
			cell.addElement(getParagraphDefaultTitle("Justify the nights"));
			cell.addElement(getParagraphNormal(observingRequest
					.getAllocationJustification(), EMPTY));
			rootTable.addCell(cell);
		}
		
		/*
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.NEW_OBSERVER_EXPERIENCE,
						fieldsDefinitionType))
		{
			PdfPCell cell = getBorderlessCell();
			cell.addElement(getParagraphDefaultTitle("Observing experience of first time users"));
			cell.addElement(getParagraphNormal(observingRequest.getNewObserverExperience(), EMPTY_NOT_REQUIRED));
			rootTable.addCell(cell);
		}
		*/		
		if (OptionsDelegate.allowedToDisplay(
				OpticonConstants.GRANT_NUMBER, fieldsDefinitionType))
		{
			PdfPCell cell = getBorderlessCell();
			cell.addElement(getParagraphDefaultTitle("Current associated SFTC Grant number : "));
			cell.addElement(getParagraphNormal(proposal.getAdditionalIssues().getGrantNumber(), EMPTY_NOT_REQUIRED));
	        rootTable.addCell(cell);
		}		
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TRAVEL, fieldsDefinitionType))
		{
			PdfPCell cell = getBorderlessCell();
			cell.addElement(getParagraphDefaultTitle("Non-standard travel and subsistence"));
			cell.addElement(getParagraphNormal(observingRequest.getTravel(), EMPTY_NOT_REQUIRED));
			rootTable.addCell(cell);
		}	
				if (OptionsDelegate.allowedToDisplay(OpticonConstants.OTHER_EXPENDITURE, fieldsDefinitionType))
		{
			PdfPCell cell = getBorderlessCell();
			cell.addElement(getParagraphDefaultTitle("Any other expenditure"));
			cell.addElement(getParagraphNormal(observingRequest.getOtherExpenditure(), EMPTY_NOT_REQUIRED));
			rootTable.addCell(cell);
		}				
	}	
	
	
	
	public void fillHeaderText(Proposal proposal, PdfPTable headerTable) 
	{
		PdfPCell cell = getBorderlessCell();
		/* Community contains all the French TAC categories */
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) proposal.getObservingRequest();
		if (opticonObservingRequest.isLargeProposal()) 
		{
			cell.addElement(
				this.getParagraph("LARGE", EMPTY,HEADER_NORMAL_FONT, HEADER_REQUIRED_FONT, 0, 0, Paragraph.ALIGN_LEFT));
		}		
		headerTable.addCell(cell);
		// below is needed otherwise the logo will be small and next to the label above
		cell = getBorderlessCell();
		headerTable.addCell(cell);
		
	}
	
	/*
	 *  Overwriting the super class. It needs a border and fixed size .
	 */
	protected void addAbstract(Proposal proposal, PdfPTable rootTable) 
	{
		PdfPCell cell = getTableCell();
		// cell.setFixedHeight(200f);
		cell.setMinimumHeight(150f);
		cell.addElement(getParagraphDefaultTitle("Abstract"));
		String abstractText = null;
		if (proposal.getJustification() != null) {
			abstractText = proposal.getJustification().getAbstractText();

		}
		cell.addElement(getParagraphNormal(abstractText, EMPTY));


		rootTable.addCell(cell);
	}
	
	protected PdfPCell getOpticonTableTextCell(String text, boolean border) {
		PdfPCell cell = null;
		if (border) {
			cell = this.getTableCell();
		} else {
			cell = this.getBorderlessCell();
		}
		if (!AstronValidator.isBlankOrNull(text)) {
			cell.setPhrase(new Phrase(text, NORMAL_FONT));
		} else {
			cell.setPhrase(new Phrase(EMPTY_NOT_REQUIRED, TABLE_CONTENT_FONT));
		}
		return cell;
	}

	/**
	 * The getTableTitleTextCell generates tablecell with text
	 * 
	 * @param text
	 * @param border
	 * @return
	 */
	protected PdfPCell getOpticonTableTitleTextCell(String text, boolean border) 
	{
		PdfPCell cell = null;
		if (border) {
			cell = this.getTableCell();
		} else {
			cell = this.getBorderlessCell();
		}
		if (!AstronValidator.isBlankOrNull(text)) {
			cell.setPhrase(new Phrase(text, PROPOSAL_TITLE_FONT));
		} else {
			cell.setPhrase(new Phrase(EMPTY_NOT_REQUIRED, PROPOSAL_TITLE_FONT));
		}
		return cell;
	}

	
	protected void addSemester(Proposal proposal, PdfPTable rootTable) 
	{
		PdfPCell cell = getBorderlessCell();
        PdfPTable table = getTableNoSplit(3);

        PdfPCell tableCell = null;
        tableCell = getOpticonTableTitleTextCell("Semester : "+proposal.getSemester().getSemester(), false);
        table.addCell(tableCell);
		// cell.addElement(getParagraphDefaultTitle("Semester"));
        
        /*if (proposal.getCode() != null)
        {
            tableCell = getOpticonTableTitleTextCell("Reference : "+proposal.getCode(), false);
        }
        else
        {
        	tableCell = getOpticonTableTitleTextCell("Reference : N.A. ", false);
        }
           table.addCell(tableCell);
         */
        table.addCell(getOpticonTableTextCell(null,false));
        
		if (proposal.getJustification() != null) {
			
			OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) proposal.getObservingRequest();
			
			if (opticonObservingRequest.getCategory() != null)
			{
				tableCell = getOpticonTableTitleTextCell("Science Cat. : "+opticonObservingRequest.getCategory(), false);
			}
			else
			{
				TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
				contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
				FieldsDefinitionType fieldsDefinitionType = 
					(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
				
				if(OptionsDelegate.allowedToDisplay(OpticonConstants.SCIENCE_CATEGORY ,
						fieldsDefinitionType)){
					tableCell = getOpticonTableTitleTextCell("Science Cat. : N.A. ",false);
				}else{
					tableCell = getOpticonTableTitleTextCell(" ",false);
				}
			}
	        table.addCell(tableCell);
		}
		cell.addElement(table);
		rootTable.addCell(cell);
	}
	
	protected void addTimeSummary(Proposal proposal, PdfPTable rootTable) 
	{
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) proposal.getObservingRequest();
        //double totalExpTime =0;
        // count totals:
		boolean enableBright=false;
		boolean enableGrey=false;
		boolean enableDark=false;
		boolean enableGlobal=false;
		//boolean enableLongtermStorage=false;

        double totalBrightTime=0;
        double totalGreyTime=0;
        double totalDarkTime=0;
        double totalGlobalTime=0;
        
        double reqBrightTime=0;
        double reqGreyTime=0;
        double reqDarkTime=0;
        double reqGlobalTime=0;
         
        double usefulBrightTime=0;
        double usefulGreyTime=0;
        double usefulDarkTime=0;
        double usefulGlobalTime=0;
        
        double longTermStorage=0;
        
        Set telescopes = new HashSet();
        PdfPCell tableCell = null;
        
        List enabledTimes = OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.OBSERVATION_TIME, new HashMap(), contextConfiguration);
    	Iterator observationTimesIt = enabledTimes.iterator();
    	while (observationTimesIt.hasNext()){
    		LabelValueBean lvb = (LabelValueBean) observationTimesIt.next();
    		if(lvb.getValue().startsWith("Global")){
    			enableGlobal=true;
    		}
    		if(lvb.getValue().startsWith("Dark")){
    			enableDark=true;
    		}
    		if(lvb.getValue().startsWith("Grey")){
    			enableGrey=true;
    		}
    		if(lvb.getValue().startsWith("Bright")){
    			enableBright=true;
    		}
    	}
    	
           	
        Iterator targetsIt = opticonObservingRequest.getTargets().iterator();
    	while (targetsIt.hasNext())
    	{
    		OpticonTarget opticonTarget = (OpticonTarget) targetsIt.next();
    		
    		Double exposureTime = NorthStarDelegate.getResourceValue(
					opticonTarget.getAllocations(),	OpticonConstants.BRIGHT);
    		if (exposureTime != null){
    			totalBrightTime += exposureTime.doubleValue();
    		}
    		exposureTime = NorthStarDelegate.getResourceValue(
					opticonTarget.getAllocations(),	OpticonConstants.GREY);
    		if (exposureTime != null){
    			totalGreyTime += exposureTime.doubleValue();
    		}
    		exposureTime = NorthStarDelegate.getResourceValue(
					opticonTarget.getAllocations(),	OpticonConstants.DARK);
    		if (exposureTime != null){
    			totalDarkTime += exposureTime.doubleValue();
    		}
    		exposureTime = NorthStarDelegate.getResourceValue(
					opticonTarget.getAllocations(),	OpticonConstants.GLOBAL);
    		if (exposureTime != null){
    			totalGlobalTime += exposureTime.doubleValue();
    		}
    	}
    	
	    Iterator observationsIt = opticonObservingRequest.getObservations().iterator();
     	while (observationsIt.hasNext())
     	{
	     	OpticonObservation opticonObservation = (OpticonObservation) observationsIt.next();
	    	telescopes.add(opticonObservation.getTelescopeConfiguration());
    	}
    	
    	Iterator telescopeIt = new ArrayList(telescopes).iterator();
		while (telescopeIt.hasNext())
		{
			String telescopeName = (String) telescopeIt.next();
			
	    	Double globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.BRIGHT);
	    	if (globalTime != null){
	    		reqBrightTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.GREY);
	    	if (globalTime != null){
	    		reqGreyTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.DARK);
	    	if (globalTime != null){
	    		reqDarkTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.GLOBAL);
	    	if (globalTime != null){
	    		reqGlobalTime += globalTime.doubleValue();
	    	}
	    	
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.USEFULBRIGHT);
	    	if (globalTime != null){
	    		usefulBrightTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.USEFULGREY);
	    	if (globalTime != null){
	    		usefulGreyTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.USEFULDARK);
	    	if (globalTime != null){
	    		usefulDarkTime += globalTime.doubleValue();
	    	}
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.USEFULGLOBAL);
	    	if (globalTime != null){
	    		usefulGlobalTime += globalTime.doubleValue();
	    	}
	    	
	    	globalTime = NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.LONGTERMSTORAGE );
	    	if (globalTime != null){
	    		longTermStorage += globalTime.doubleValue();
	    	}
	/*  kan weg
	    	totalAwardedTime += NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.AWARDED ).doubleValue();
	
	    	totalLongTime += NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
	                telescopeName + '_' + OpticonConstants.LONGTERM).doubleValue();
	*/
		}
		
    	
		if (contextConfiguration != null){
			FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
			int tableCount=0;	
			if(enableBright) tableCount++;
			if(enableDark) tableCount++;
			if(enableGrey) tableCount++;
			if(totalGlobalTime >0) tableCount++;
			
			PdfPCell cell = getBorderlessCell();
			PdfPTable table = null;
			if (tableCount > 0){
		        table = getTableNoSplit(tableCount);
		        if(enableBright){
		        	tableCell = getOpticonTableTitleTextCell("Bright: ",false);
		        	table.addCell(tableCell);
		        }
		        if(enableGrey){
		        	tableCell = getOpticonTableTitleTextCell("Grey: ",false);
		        	table.addCell(tableCell);
		        }
		        if(enableDark){
		        	tableCell = getOpticonTableTitleTextCell("Dark: ",false);
		        	table.addCell(tableCell);	
		        }
		        if(totalGlobalTime >0){
		        	tableCell = getOpticonTableTitleTextCell("Total: ",false);
		        	table.addCell(tableCell);
		        }
				cell.addElement(table);
				rootTable.addCell(cell);
				
			}
			cell = getBorderlessCell();
	        // Do not show if value is blank or zero
	        if(totalBrightTime>0 || totalBrightTime >0 || totalBrightTime >0 || totalGlobalTime >0){
	        	table = getTableNoSplit(tableCount);
		        if(enableBright){
		        	if(totalBrightTime > 0){
		        		tableCell = getOpticonTableTitleTextCell("Exposure time: "+
		        		AstronConverter.getHoursFromSeconds(new Double(totalBrightTime))+" " , false);
		        	}else{
		        		tableCell = getOpticonTableTextCell(" \n", false);
		        	}
		        	table.addCell(tableCell);
		        }
		        if(enableGrey){
		        	if(totalGreyTime > 0){
		        		tableCell = getOpticonTableTitleTextCell("Exposure time: "+
		        		AstronConverter.getHoursFromSeconds(new Double(totalGreyTime))+" " , false);
		        	}else{
		        		tableCell = getOpticonTableTextCell(" . ", false);
		        	}
		        	table.addCell(tableCell);
		        }
		        if(enableDark){
		        	if( totalDarkTime > 0){
		        	tableCell = getOpticonTableTitleTextCell("Exposure time: "+
		        		AstronConverter.getHoursFromSeconds(new Double(totalDarkTime))+" " , false);
		        	}else{
		        		tableCell = getOpticonTableTextCell(" . ", false);
		        	}
		        	table.addCell(tableCell);
		        }
	        
	        
		        if(totalGlobalTime >0){
			        tableCell = getOpticonTableTitleTextCell("Exposure time: "+
			        		AstronConverter.getHoursFromSeconds(new Double(totalGlobalTime))+" " , false);
			        table.addCell(tableCell);
			    }
		        cell.addElement(table);
	        }
			
			//rootTable.addCell(cell);
	        //AstronConverter.getMinutesFromSeconds(opticonObservation.getTotalObservationDuration())));
			// cell.addElement(getParagraphDefaultTitle("Semester"));
			//cell = getBorderlessCell();
	       if (tableCount > 0){
		       table = getTableNoSplit(tableCount);
			
		       if(enableBright){
		    	   tableCell = getOpticonTableTextCell("Nights requested: "+Integer.toString((int) reqBrightTime)+" ", false);
		    	   table.addCell(tableCell);
		       }
		       if(enableGrey){
		        	tableCell = getOpticonTableTextCell("Nights requested: "+Integer.toString((int) reqGreyTime)+" ", false);
		        	table.addCell(tableCell);
		       }
		       if(enableDark){
		    	   tableCell = getOpticonTableTextCell("Nights requested: "+Integer.toString((int) reqDarkTime)+" ", false);
		    	   table.addCell(tableCell);
		       }
		       if(totalGlobalTime >0){
		    	   if(enableGlobal){
		    	   tableCell = getOpticonTableTextCell("Nights requested: "+Integer.toString((int) reqGlobalTime)+" ", false);
		    	   }else{
		    		   tableCell = getOpticonTableTextCell(" . ", false);
		    	   }
		    	   table.addCell(tableCell);
		       }
		       cell.addElement(table);
				//rootTable.addCell(cell);
			
			// display useful time only if is enable in the options file
				if (OptionsDelegate.allowedToDisplay(OpticonConstants.USEFUL_TIME,fieldsDefinitionType)){
				//	cell = getBorderlessCell();
			        table = getTableNoSplit(tableCount);
				
			       if(enableBright){
			        tableCell = getOpticonTableTextCell("Minimal useful requested: "+
			        		Integer.toString((int) usefulBrightTime)+" ", false);
			        table.addCell(tableCell);
			       }
			       if(enableGrey){		        
					tableCell = getOpticonTableTextCell("Minimal useful requested: "+
							Integer.toString((int) usefulGreyTime)+" ", false);
					table.addCell(tableCell);
			       }
			       if(enableDark){
					tableCell = getOpticonTableTextCell("Minimal useful requested: "+
							Integer.toString((int) usefulDarkTime)+" ", false);
					table.addCell(tableCell);
			       }
			       if(totalGlobalTime >0){
			    	   if(enableGlobal){
			    		   tableCell = getOpticonTableTextCell("Minimal useful requested: "+
							Integer.toString((int) usefulGlobalTime)+" ", false);
			    	   }else{
			    		   tableCell = getOpticonTableTextCell(" . ", false);
			    	   }
			    	   table.addCell(tableCell);
			       }
			       cell.addElement(table);
				
				}
				
				if(OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_LONGTERM_STORAGE,
						fieldsDefinitionType)){
					
					table= getTableNoSplit(1);
					tableCell = getOpticonTableTextCell("Longterm storage requested: "+
							Integer.toString((int) longTermStorage)+" GB ", false);
			    	   table.addCell(tableCell);
					cell.addElement(table);
				}
	       }
		rootTable.addCell(cell);
			
		}
	}
    /**
     * Generates the additional information the observing request has next to the observations 
     * and the allocation times.
     *  @see eu.radionet.northstar.business.pdf.ProposalWriter#addObservingRequest(eu.radionet.northstar.data.entities.ObservingRequest,
     *      com.lowagie.text.pdf.PdfPTable)
     */
    protected void addObservingRequest(ObservingRequest observingRequest,
            PdfPTable rootTable) throws DocumentException {
        OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
        // if enabled
        if (contextConfiguration != null) // it happens
        {
        	PdfPCell lcell = getBorderlessCell();
        	PdfPTable ftable = getTableNoSplit(1);
        	
        	addLongTermProposal(opticonObservingRequest, ftable);
        	FieldsDefinitionType fieldsDefinitionType = 
				(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.REQUIRED_SCHEDULING_CONSTRAINTS ,
					fieldsDefinitionType))
	        {
	        	addOverallSchedulingRequirements(opticonObservingRequest, ftable);
	        }
	        if (OptionsDelegate.allowedToDisplay(OpticonConstants.PREFERRED_SCHEDULING_CONSTRAINTS ,
						fieldsDefinitionType))
	        {	
	        	addOverallSchedulingPreferences(opticonObservingRequest, ftable);
	        }
	        
	        lcell.addElement(ftable);
	        lcell.setFixedHeight(200f);
	        rootTable.addCell(lcell);
	        
	        
        }
    }
    
  
    /**
     * Generates the total time of all the specified observations in the pdf
     * @see eu.radionet.northstar.business.pdf.ProposalWriter#addTotalRequestedTime(eu.radionet.northstar.data.entities.ObservingRequest, com.lowagie.text.pdf.PdfPTable)
     */
    protected void addTotalRequestedTime(ObservingRequest observingRequest,
            PdfPTable rootTable) 
    {
        OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
        // addScienceCategory(opticonObservingRequest, rootTable);
        
        PdfPCell cell = getBorderlessCell();
        //PdfPCell cell = getTableCell();
        //float spacing = 10;
        //cell.addElement(getSpacingParagraph(spacing));

        //OpticonProposalDelegate proposalDelegate = null;
		try
		{
			/*
			proposalDelegate = new OpticonProposalDelegate();
			Double nights = proposalDelegate.getTotalTime(opticonObservingRequest);
			if (nights != null)
			{
		        cell.addElement(getParagraphDefaultTitle("Total requested nights: " + AstronConverter.toString(nights) + " nights"));
				addTelescopeDetails(opticonObservingRequest, cell);
				
			}
			else
			{
				Paragraph p = getParagraphDefaultTitle("Total requested nights: ");
				p.add(new Phrase("EMPTY", TITLE_REQUIRED_FONT));
		        cell.addElement(p);
		        PdfPTable detailsTable = getTableNoSplit(1);
		        addSchedulingDetails(observingRequest, detailsTable); 
		        cell.addElement(detailsTable);
			}
			*/
			addTelescopeDetails(opticonObservingRequest, cell);
		}/* linked with proposaldelegate
		catch (ConnectionException e)
		{
			log.error(e.getStackTrace());
		}*/
		catch (DocumentException e)
		{
			log.error(e.getStackTrace());
		}		        		
        rootTable.addCell(cell);
        cell = getBorderlessCell();
        try
		{
			super.addContactAuthor(applicants, cell);
		}
		catch (DocumentException e)
		{
			log.error(e.getStackTrace());
		}
        rootTable.addCell(cell);
        addPi(applicants, rootTable);

    }    
    
    protected void addOverallSchedulingRequirements(
			ObservingRequest observingRequest, PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		if (!AstronValidator.isBlankOrNull(observingRequest
				.getOverallRequiredSchedConstraints())) {

			cell.addElement(getParagraphDefaultTitle("Overall scheduling requirements"));
			String constraints = observingRequest.getOverallRequiredSchedConstraints();
			
			// this function ensures that the constraints are being split at the same character.
			String regions[] = splitSchedulingConstraints(constraints);
			// this part gets the 0 region, the last page gets the 1 region
			cell.addElement(getParagraphNormal(regions[0], EMPTY_NOT_REQUIRED));

		} else {
			cell.addElement(getParagraphDefaultTitle("No overall scheduling requirements"));
		}
		rootTable.addCell(cell);

	}


    private String[] splitSchedulingConstraints(String constraints) {
    	
    	String result[]= new String[2];
    	String lines[] = constraints.split("\\r?\\n");
		int lineCount = lines.length;
		for (int i=0;i<lines.length;i++){
			if(lines[i].length()>LINE_LENGTH){
				lineCount+=lines[i].length()/LINE_LENGTH;
			}
		}
		 
		if(lineCount > schedulingOverflow){
			// get a new array with lines of max LINE_LENGTH characters
			List newList = new ArrayList();
			lineCount = lines.length;
			for (int i=0;i<lines.length;i++){
				String curline = lines[i];
				while(curline.length()>LINE_LENGTH){
					newList.add(curline.substring(0, LINE_LENGTH));
					curline = curline.substring(LINE_LENGTH);
				}
				newList.add(curline);
			}
			// split the new array at 8 lines
			StringBuffer contained=new StringBuffer();
			StringBuffer overflow=new StringBuffer();
			for (int i=0;i<newList.size();i++){
				if(i < schedulingOverflow){
					contained.append(newList.get(i));
					contained.append("\n");
				}else{
					overflow.append(newList.get(i));
					overflow.append("\n");
				}
				
			}
			contained.append(" the scheduling requirements are continued on the last page ");
			result[0]=contained.toString();
			result[1]=overflow.toString();
			
			return result;
		}else{
			result[0]=constraints;
			result[1]=null;
			return result;
		}
		
	}


	protected void addPi(List members, PdfPTable rootTable)
	{
		PdfPCell cell = getBorderlessCell();
    	Member ca = getContactAuthor(members);
    	Member pi = getPi(members);
    	cell.addElement(getParagraphDefaultSubTitle("Pi"));
    	
    	if (pi.getId().intValue() == ca.getId().intValue())
    	{
    		cell.addElement(getParagraphNormal("is the same as the contact author", EMPTY));
    	}
    	else 
    	{
    		cell.addElement(getParagraphNormal(pi.getName() + " - " + pi.getAffiliation() + ", " + pi.getCountry(), EMPTY));
    	}
		rootTable.addCell(cell);
	}

    
    /**
     * The addLongTermProposal adds the long term proposal
     * 
     * @param opticonObservingRequest
     * @param rootTable
     * @throws DocumentException
     */
    protected void addLongTermProposal(
			OpticonObservingRequest opticonObservingRequest, PdfPTable rootTable)
	{
		PdfPCell cell = getBorderlessCell();
		/*
		 * if long term proposal
		 */
		if (opticonObservingRequest.isLongTermProposal())
		{
			cell.addElement(getParagraphDefaultTitle("Is this a long term proposal: Yes"));
			String[] results= splitLongtermSpecifics( opticonObservingRequest.getLongTermProposalSpecifics());
			cell.addElement(getParagraphNormal(results[0], EMPTY));
			
			String lines[] = results[0].split("\\r?\\n");
			schedulingOverflow=schedulingOverflow-lines.length;
		}
		else
		{
			cell.addElement(getParagraphDefaultTitle("Is this a long term proposal: No"));
		}
		rootTable.addCell(cell);
	}

    private String[] splitLongtermSpecifics(String longTermProposalSpecifics) {
    	String[] result = new String[2];
    	String lines[] = longTermProposalSpecifics.split("\\r?\\n");
		int lineCount = lines.length;
		for (int i=0;i<lines.length;i++){
			if(lines[i].length()>LINE_LENGTH){
				lineCount+=lines[i].length()/LINE_LENGTH;
			}
		}
		if(lineCount > 4){
			// split the text...
			List newList = new ArrayList();
			lineCount = lines.length;
			for (int i=0;i<lines.length;i++){
				String curline = lines[i];
				while(curline.length()>LINE_LENGTH){
					newList.add(curline.substring(0, LINE_LENGTH));
					curline = curline.substring(LINE_LENGTH);
				}
				newList.add(curline);
			}
			// split the new array at 5 lines
			StringBuffer contained=new StringBuffer();
			StringBuffer overflow=new StringBuffer();
			for (int i=0;i<newList.size();i++){
				if(i < 4){
					contained.append(newList.get(i));
					contained.append("\n");
				}else{
					overflow.append(newList.get(i));
					overflow.append("\n");
				}
			}
			contained.append(" the long term specifics are continued on the last page ");
			result[0]=contained.toString();
			result[1]=overflow.toString();
			
			lineCount=5;
		}else{
			result[0]=longTermProposalSpecifics;
			result[1]=null;
		}
		
		return result;
	}


	/**
     * Generates a table of observationdetails which will be displayed on the second page of the
     * pdf
     * @see eu.radionet.northstar.business.pdf.ProposalWriter#addObservationsList(eu.radionet.northstar.data.entities.Proposal,
     *      com.lowagie.text.pdf.PdfPTable)
     */
    protected void addObservationsList(List observations, OpticonObservingRequest opticonObservingRequest,
            PdfPCell observationsListCell) {
        int columns = 4;
        PdfPTable observationsListTable = getTableNoSplit(columns);
        /*
         * generates table header
         */

        observationsListTable.addCell(getTableHeaderCell("Number of targets"));
        observationsListTable.addCell(getTableHeaderCell("Telescope"));
        observationsListTable.addCell(getTableHeaderCell("Instrument"));
        observationsListTable.addCell(getTableHeaderCell("Exposure (mins.)"));   
        /*
         * add table content
         */
        Iterator observationsIt = observations.iterator();
        while (observationsIt.hasNext())
        {
            OpticonObservation opticonObservation = (OpticonObservation) observationsIt.next();
            observationsListTable.addCell(getTableRequiredTextCell(
            		
            		opticonObservingRequest.getTargetsByObservationId(opticonObservation.getId()).size() + " targets"));
            observationsListTable.addCell(getTableRequiredTextCell(opticonObservation.getTelescopeConfiguration()));
            if (opticonObservation.getInstrument()==null) {
                observationsListTable.addCell(getTableRequiredTextCell(null));
            } else {
                observationsListTable.addCell(getTableRequiredTextCell(opticonObservation.getInstrument().getName()));
            }
            observationsListTable.addCell(getTableNumbersCell(AstronConverter
                            .getHoursFromSeconds(opticonObservation.getTotalObservationDuration())));
        }
        observationsListCell.addElement(observationsListTable);

    }

	protected void addSchedulingDetails(
			ObservingRequest observingRequest, PdfPTable detailsTable) 
	{
		PdfPCell cell = getTableCell();
		cell.setColspan(6);
		cell.setFixedHeight(100f);
		if (!AstronValidator.isBlankOrNull(
				observingRequest.getOverallRequiredSchedConstraints())) 
		{

			cell.addElement(getParagraphSmallTitle("Details, including preferred dates and periods to be avoided:"));
			cell.addElement(getParagraphNormal(
					observingRequest.getOverallRequiredSchedConstraints(), EMPTY_NOT_REQUIRED));

		} 
		else 
		{
			cell.addElement(getParagraphSmallTitle("No details given"));
		}
		detailsTable.addCell(cell);

	}
        
    protected void addTelescopeDetails(OpticonObservingRequest opticonObservingRequest, PdfPCell telescopeDetailsCell)
    	throws DocumentException
    {
    	int columns = 3;// 6;
   
        PdfPTable detailsTable = getTableNoSplit(columns);
        //detailsTable.setWidths(new float[] { 0.20f, 0.10f, 0.10f, 0.10f, 0.10f, 0.40f });
        detailsTable.addCell(getTableHeaderCell("Telescope"));
        /*detailsTable.addCell(getTableHeaderCell("Dark"));
        detailsTable.addCell(getTableHeaderCell("First"));
        detailsTable.addCell(getTableHeaderCell("Bright"));
        detailsTable.addCell(getTableHeaderCell("Last"));*/
        //detailsTable.addCell(getTableHeaderCell("Total hours"));
        detailsTable.addCell(getTableHeaderCell("Observing mode"));
        detailsTable.addCell(getTableHeaderCell("Instruments"));
        Iterator observationsIt = opticonObservingRequest.getObservations().iterator();
        
        /*
         * used to temporary store the instruments used belonging to one telescope
         */
        Map telescopeInstruments = new HashMap(); 
        Map observingModes = new HashMap();
        // fill the Map
        while (observationsIt.hasNext())
        {
            OpticonObservation opticonObservation = (OpticonObservation) observationsIt.next();
            if (opticonObservation.getTelescopeConfiguration()!= null)
            {
            	String telescopeName = opticonObservation.getTelescopeConfiguration();
            	// if telescope is not in the set create an entry with a set of no instruments
            	if (!AstronValidator.isBlankOrNull(telescopeName) && !telescopeInstruments.containsKey(telescopeName))
            	{
            		telescopeInstruments.put(telescopeName, new HashSet());
            		
            	}
            	if (opticonObservation.getInstrument() != null && 
            			!AstronValidator.isBlankOrNull(opticonObservation.getInstrument().getName()))
            	{
                	Set instruments = (Set) telescopeInstruments.get(telescopeName);
                	// The Set object prevent double entries
                	instruments.add(opticonObservation.getInstrument().getName());
            	}
            	if (opticonObservation.getObservationMode() != null)
        		{
        			String modes = (String) observingModes.get(telescopeName);
        			if(modes != null && modes.indexOf(opticonObservation.getObservationMode()) < 0 ){
        				modes += ", "+opticonObservation.getObservationMode();
        				observingModes.put(telescopeName, modes);
        			}else{
        				observingModes.put(telescopeName, opticonObservation.getObservationMode());
        			}
        		}
            	
            }
        }
        // Use the map to fill the table cells
        Iterator telescopeIt = telescopeInstruments.keySet().iterator(); 
        while (telescopeIt.hasNext())
        {
        	String telescopeName = (String) telescopeIt.next();
        	detailsTable.addCell(getTableNumbersCell(telescopeName));
        	/*
        	detailsTable.addCell(getTableNumbersCell(
        			AstronConverter
                    .toString(NorthStarDelegate.getResourceValue(
                            opticonObservingRequest.getAllocations(),
                            telescopeName + '_' + OpticonConstants.DARK))));
        	detailsTable.addCell(getTableNumbersCell(
        			AstronConverter
                    .toString(NorthStarDelegate.getResourceValue(
                            opticonObservingRequest.getAllocations(),
                            telescopeName + '_' + OpticonConstants.FIRST_Q))));
        	detailsTable.addCell(getTableNumbersCell(
        			AstronConverter
                    .toString(NorthStarDelegate.getResourceValue(
                            opticonObservingRequest.getAllocations(),
                            telescopeName + '_' + OpticonConstants.BRIGHT))));
        	detailsTable.addCell(getTableNumbersCell(
        			AstronConverter
                    .toString(NorthStarDelegate.getResourceValue(
                            opticonObservingRequest.getAllocations(),
                            telescopeName + '_' + OpticonConstants.LAST_Q))));
           	detailsTable.addCell(getTableNumbersCell(
        			AstronConverter
                    .toString(NorthStarDelegate.getResourceValue(
                            opticonObservingRequest.getAllocations(),
                            telescopeName + '_' + OpticonConstants.GLOBAL)))); */
        	
        	String observingMode = (String) observingModes.get(telescopeName);
        	detailsTable.addCell(getTableNumbersCell( observingMode ));
        	
        	/* Get the instruments from the hash table and display them comma seperated */
        	List instruments = new ArrayList((Set) telescopeInstruments.get(telescopeName));
        	String instrumentString = "";
        	for (int i = 0; i < instruments.size(); i++)
        	{
        		if (i ==0)
        		{
        			instrumentString = (String) instruments.get(i);
        		}
        		else
        		{
        			instrumentString = instrumentString + ", " + (String) instruments.get(i);
        		}
        		
        	}
        	detailsTable.addCell(getTableNumbersCell(instrumentString));
        }
        // addSchedulingDetails(opticonObservingRequest, detailsTable);
        telescopeDetailsCell.addElement(detailsTable);

    }

    /**
     * The addObservation method add observation to root table
     * @param observationId 
     * 
     * @param wsrtObservation
     * @param rootTable
     */
    protected void addObservationDetails(Proposal proposal,Observation observation, 
            String observationId, PdfPTable rootTable) throws DocumentException {
    	// get all observations
    	ObservingRequest observingRequest = proposal.getObservingRequest(); 
        OpticonObservation opticonObservation = (OpticonObservation) observation;
        // this is not the right way to get amount of options.
        //String[] tableHeaders = opticonLabels.getMessage("label.opticon.targettableheaders").split(";");
        
        FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
        
        int columns = 2;
        
        PdfPTable observationTable = getTableNoSplit(columns);
        
        if (!AstronValidator.isBlankOrNull(opticonObservation.getTelescopeConfiguration())) {

        	Double specifiedTime = NorthStarDelegate.getResourceValue(
					opticonObservation.getAllocations(),opticonObservation.getTelescopeConfiguration()+"_"+ OpticonConstants.REQUESTEDTIME );
            String nights = "0";
        	if (specifiedTime != null){
        		nights = specifiedTime.toString();
        	}
        	String instrument=" not selected ";
        	if(opticonObservation.getInstrument() != null){
        		instrument=opticonObservation.getInstrument().getName();
        	}
        	PdfPCell telescopeCell = getTableHeaderCell(" Run: "+observationId+"   Telescope: "+opticonObservation.getTelescopeConfiguration()+
        		"   Instrument: "+instrument +"   Nights: "+nights);
          
            telescopeCell.setColspan(columns);
            observationTable.addCell(telescopeCell);

            /*
             * Insert Telescope configuration
             */
            PdfPTable leftTable = getTableNoSplit(columns);
            addInstrument(opticonObservation, leftTable, columns);
            
            PdfPCell ncell = getBorderlessCell();
            ncell.addElement(leftTable);
            
            observationTable.addCell(ncell);

        } else {

            PdfPCell telescopeCell = getTableHeaderCell("Telescope: None Selected");
            telescopeCell.setColspan(columns);
            observationTable.addCell(telescopeCell);

            PdfPCell instrumentCell = getTableCell();

            instrumentCell.addElement(getParagraphCellContentNormal(null, EMPTY));
            
            observationTable.addCell(instrumentCell);
        }

        /*
         * insert scheduling comments
         */
        PdfPTable rightTable = getTableNoSplit(1);
        rightTable.addCell(getTableInnerHeaderCell("run details"));
        PdfPCell ncell = getBorderlessCell();
       
        if (!AstronValidator
                .isBlankOrNull(opticonObservation.getObservationPhase())) {
        	ncell.addElement(this.getParagraphCellContentTitleWithValue("Time period: ",
        	 opticonObservation.getObservationPhase(),EMPTY_NOT_REQUIRED));
        	//rightTable.addCell(ncell);
        	//ncell = getBorderlessCell();
        }
        
        if (!AstronValidator
                .isBlankOrNull(opticonObservation.getObservationWeather())) {
        	ncell.addElement(this.getParagraphCellContentTitleWithValue("Weather: ",
        			opticonObservation.getObservationWeather(),EMPTY_NOT_REQUIRED));
        	//rightTable.addCell(ncell);
        	//ncell = getBorderlessCell();
        }
        
        if (!AstronValidator
                .isBlankOrNull(opticonObservation.getObservationMode())) {
        	ncell.addElement(this.getParagraphCellContentTitleWithValue("Mode: ",
        			opticonObservation.getObservationMode(),EMPTY_NOT_REQUIRED));
        	//rightTable.addCell(ncell);
        	//ncell = getBorderlessCell();
         }

        if (!AstronValidator
                .isBlankOrNull(opticonObservation.getObservationSeeing())) {
        	ncell.addElement(this.getParagraphCellContentTitleWithValue("Seeing: ",
        			opticonObservation.getObservationSeeing(),EMPTY_NOT_REQUIRED));
        	
        }
        rightTable.addCell(ncell);
        // add remainder.
       // ncell=getTableCell();
        //rightTable.addCell(ncell);
        
        PdfPCell cell = getBorderlessCell();
        //cell.setColspan(columns);
        if (!AstronValidator
                .isBlankOrNull(opticonObservation.getObservationDates())) {
        	cell.addElement(this.getParagraphCellContentTitleWithValue("preferred Dates: ",
        			opticonObservation.getObservationDates(),EMPTY_NOT_REQUIRED));
        }
        
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.REQUIRED_SCHEDULING_CONSTRAINTS ,
				fieldsDefinitionType))
        {
	        if (!AstronValidator.isBlankOrNull(opticonObservation.getRequiredSchedConstraints())) {
	            cell.addElement(this
	                            .getParagraphCellContentTitle("Required scheduling constraints"));
	            cell.addElement(this.getParagraphCellContentNormal(opticonObservation
	                    .getRequiredSchedConstraints(), EMPTY_NOT_REQUIRED));
	        } else {
	            cell.addElement(this
	                            .getParagraphCellContentTitle("No required scheduling constraints"));
	        }
        }
        /*
         * addPreferred Scheduling constraints
         */
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.PREFERRED_SCHEDULING_CONSTRAINTS,
				fieldsDefinitionType))
        {
	        if (!AstronValidator.isBlankOrNull(opticonObservation
	                .getPreferredSchedConstraints())) {
	            cell.addElement(this
	                            .getParagraphCellContentTitle("Preferred scheduling constraints"));
	            cell.addElement(this.getParagraphCellContentNormal(opticonObservation
	                    .getPreferredSchedConstraints(), EMPTY_NOT_REQUIRED));
	
	        } else {
	            cell.addElement(this
	                            .getParagraphCellContentTitle("No preferred scheduling constraints"));
	        }
        }
        rightTable.addCell(cell);
        ncell=getTableCell();
        ncell.addElement(rightTable);
        
        observationTable.addCell(ncell);
        
        
        /*
         * add observation to root table
         */
   	 	PdfPCell blankline = getBorderlessCell();

        rootTable.addCell(blankline);
        
        rootTable.addCell(observationTable);
       
        rootTable.addCell(blankline);

    }
    protected void addFilterDetails(OpticonInstrument instrument, PdfPCell cell)
    	throws DocumentException 
    {
    	//invariant: instrument != null
		List configurations = OptionsDelegate.getOptionsConfigurationTypes(
				OpticonConstants.INSTRUMENT_FILTER, new HashMap(), contextConfiguration);
		boolean isAllowedToDisplay = false;
		Iterator filtersIt = instrument.getFilters().iterator();
		while (filtersIt.hasNext() && !isAllowedToDisplay)
		{
			ConfigureOptionType configureOptionType = 
				(ConfigureOptionType) OptionsDelegate.getOption((String) filtersIt.next(), 
									configurations);	
			if (configureOptionType != null	&& configureOptionType.getSubfields() != null) 
			{
				FieldsDefinitionType fieldsDefinitionType = 
					(FieldsDefinitionType) configureOptionType.getSubfields();
				isAllowedToDisplay = OptionsDelegate.allowedToDisplay(
						OpticonConstants.INSTRUMENT_FILTER_DETAILS,	fieldsDefinitionType);			
			}
		}
		if (isAllowedToDisplay)
		{
            cell.addElement(this.getParagraphCellContentTitleWithValue(
                    "Filter details: ", instrument.getFilterDetails(), EMPTY));
		}
	}
    
    protected String addFilterDetails(OpticonInstrument instrument){
    	String details="";
		//invariant: instrument != null
		List configurations = OptionsDelegate.getOptionsConfigurationTypes(
				OpticonConstants.INSTRUMENT_FILTER, new HashMap(), contextConfiguration);
		boolean isAllowedToDisplay = false;
		Iterator filtersIt = instrument.getFilters().iterator();
		while (filtersIt.hasNext() && !isAllowedToDisplay)
		{
			ConfigureOptionType configureOptionType = 
				(ConfigureOptionType) OptionsDelegate.getOption((String) filtersIt.next(), 
									configurations);	
			if (configureOptionType != null	&& configureOptionType.getSubfields() != null) 
			{
				FieldsDefinitionType fieldsDefinitionType = 
					(FieldsDefinitionType) configureOptionType.getSubfields();
				isAllowedToDisplay = OptionsDelegate.allowedToDisplay(
						OpticonConstants.INSTRUMENT_FILTER_DETAILS,	fieldsDefinitionType);			
			}
		}
		if (isAllowedToDisplay)
		{
			details="Filter details: "+ instrument.getFilterDetails();
		}
		return details;
	}
    
    protected void addInstrument(OpticonObservation opticonObservation,
            PdfPTable observationTable, int columns) throws DocumentException {
       // PdfPCell observingModeCell = getBorderlessCell();
       // observingModeCell.setColspan(columns);
       // PdfPTable observingModeTable = getTableNoSplit(1);

        /*
         * if backend exist
         */
        if (opticonObservation.getInstrument() != null) {
            OpticonInstrument instrument = opticonObservation.getInstrument();
            PdfPCell cell = getTableInnerHeaderCell(" Instrument details");
            cell.setColspan(columns);
            observationTable.addCell(cell);
            cell = getTableCell();
            cell.setColspan(columns);
            
            Map enteredValues = new HashMap();
            enteredValues.put(OpticonConstants.TELESCOPE_CONFIGURATIONS,
                    OptionsUtils.getList(opticonObservation.getTelescopeConfiguration()));

            List configurations = OptionsDelegate.getOptionsConfigurationTypes(
                    OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredValues,
                    contextConfiguration);
            ConfigureOptionType configureOptionType = (ConfigureOptionType) OptionsDelegate
                    .getOption(instrument.getName(), configurations);

            if (configureOptionType != null
                    && configureOptionType.getSubfields() != null) {
                FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) configureOptionType
                        .getSubfields();
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_FRAME_EXPOSURE_TIME_DETAILS,
                        fieldsDefinitionType)) {
                    if (!instrument.isNonDefaultFrameExposureTime()) {
                        cell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Non Default Frame Exposure Time: ", "no",
                                        EMPTY));
                    } else {
                        cell
                                .addElement(this
                                        .getParagraphCellContentTitleWithValue(
                                                "Frame Exposure Time: ",
                                                instrument.getNonDefaultFrameExposureTimeDetails(),
                                                EMPTY));
                    }
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_FILTER, fieldsDefinitionType)) 
                {
                	String filterStr = null;
                	if (instrument.getFilters()!= null)
                	{
                		Iterator filterIt = instrument.getFilters().iterator();
                		while (filterIt.hasNext())
                		{
                			if (filterStr == null)
                			{
                				filterStr = (String) filterIt.next();
                			}
                			else
                			{
                				filterStr += ", " + (String) filterIt.next();
                			}
                		}
                	}
                	
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Filter: ", filterStr, EMPTY));
                }
                addFilterDetails(instrument,cell);
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_MODE, fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Observing mode: ", instrument.getMode(), EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_POLARIMETRY,
                        fieldsDefinitionType)) {
                    if (instrument.isPolarimetry()) {
                        cell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Polarimetry: ", "yes", EMPTY));
                    } else {
                        cell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Polarimetry: ", "no", EMPTY));
                    }
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_GUIDELINE,
                        fieldsDefinitionType)) {
                    if (instrument.isGuideline()) {
                        cell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Guideline: ", "yes", EMPTY)); // accepted
                    } else {
                        cell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Guideline: ", "no", EMPTY)); // not accepted
                    }
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_WAVELENGTH,
                        fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Wavelength: ", instrument.getWavelength(), EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_ORDER, fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Order: ", AstronConverter.toString(instrument
                                    .getOrder()), NOT_SPECIFIED));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_SLIT, fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Slit: ", instrument.getSlit(), EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_SLIT_POSITION_ANGLE,
                        fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Slit position angle: ",
                            AstronConverter.toString(instrument
                                    .getSlitPositionAngle()), NOT_SPECIFIED));
                }
                if (OptionsDelegate
                        .allowedToDisplay(OpticonConstants.INSTRUMENT_GRATING,
                                fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Grating: ", instrument.getGrating(), EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_ORDER_FILTER, fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Order separation filter: ", instrument.getOrderFilter(), EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_READOUT, fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Read-out mode : ", instrument.getReadOut(), EMPTY));
                }                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_GRISM, fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Grism: ", instrument.getGrism(), EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_MICROSTEPPING,
                        fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Grism: ", instrument.getMicrostepping(), EMPTY));
                }

                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_CORONAGRAPHIC_MASK,
                        fieldsDefinitionType)) {
                    if (instrument.isCoronagraphicMask()) {
                        cell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Coronagraphic Mask: ", "yes", EMPTY));
                    } else {
                        cell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Coronagraphic Mask: ", "no", EMPTY));
                    }
                }
               // for lofar
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_STATION , fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Stations: ", instrument.getStation() , EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_CLOCK, fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Clock speed: ", instrument.getClock() , EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_ONE_FILTER , fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Filter: ", instrument.getOneFilter() , EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_ANTENNA, fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Antenna: ", instrument.getAntenna(), EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_INTEGRATION_TIME , fieldsDefinitionType)) {
                    cell.addElement(this.getParagraphCellContentTitleWithValue(
                            "Integration time: ", instrument.getIntegrationTime().toString(), EMPTY));
                }
                if (OptionsDelegate.allowedToDisplay(
                        OpticonConstants.INSTRUMENT_COMMENTS,
                        fieldsDefinitionType)) {
                    if (!AstronValidator
                            .isBlankOrNull(instrument.getComments())) {
                        cell
                                .addElement(this
                                        .getParagraphCellContentTitle("Configuration comments"));
                        cell.addElement(this.getParagraphCellContentNormal(
                                instrument.getComments(), EMPTY_NOT_REQUIRED));

                    } else {
                        cell
                                .addElement(this
                                        .getParagraphCellContentTitle("No configuration comments "));
                    }
                }
                
                
                FieldsDefinitionType targetsDefinitionType = 
         			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
                if (OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_POSTPROCESSING,
                        targetsDefinitionType)) {
                	PdfPTable postProcTable = getTableNoSplit(1);
                	postProcTable
    				.addCell(getTableInnerHeaderCell("Post Processing: "));
                	
        			PdfPCell postCell = getTableCell();
                	
                	if(instrument.isAveraging()){
                		postCell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Averaging time : ", instrument.getAveragingTime().toString(), EMPTY));

                		postCell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Averaging frequency : ", instrument.getAveragingFrequency().toString(), EMPTY));
                		
                	}
                	if(instrument.isCalibration()){
                		postCell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Calibration : ", "yes", EMPTY));
                    } else {
                    	postCell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Calibration : ", "no", EMPTY));
                	}
                	if(instrument.isFlagging()){
                		postCell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Flagging : ", "yes", EMPTY));
                    } else {
                    	postCell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Flagging : ", "no", EMPTY));
                	}
                	if(instrument.isImaging()){
                		postCell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Field size : ", instrument.getFieldSizeX()+" x "+instrument.getFieldSizeY() , EMPTY));
                		postCell.addElement(this
                                .getParagraphCellContentTitleWithValue(
                                        "Pixel size : ", instrument.getPixelSizeX()+" x "+instrument.getPixelSizeY() , EMPTY));

                	}
                	
                	postProcTable.addCell(postCell);
                	cell.addElement(postProcTable);
                }
             }
            observationTable.addCell(cell);
        }
        /*
         * if not
         */
        else {
        	PdfPCell cell =getTableInnerHeaderCell("Observing mode details");
        	cell.setColspan(columns);
        	observationTable
                    .addCell(cell);
        	observationTable.addCell(getParagraphCellContentNormal(null,
                    EMPTY));
        }
        //observingModeCell.addElement(observingModeTable);
        //observationTable.addCell(observingModeCell);

    }

	protected PdfPCell getTableHeaderCellSmall(String text) {
		PdfPCell cell = new PdfPCell();
		cell.setPhrase(new Phrase(text, TABLE_HEADER_FONT_SMALL));
		cell.setBackgroundColor(Color.DARK_GRAY);
		return cell;
	}
	
	/**
	 * The getTableNumbersCell method generates tablecell with numbers
	 * 
	 * @param numbers
	 * @return
	 */
	protected PdfPCell getTableNumbersCellSmall(String numbers) {
		PdfPCell cell = new PdfPCell();
		if (!AstronValidator.isBlankOrNull(numbers)) {
			cell.setPhrase(new Phrase(numbers, TABLE_CONTENT_FONT_SMALL));
		} else {
			cell.setPhrase(new Phrase(EMPTY_NOT_REQUIRED, TABLE_CONTENT_FONT_SMALL));
		}
		return cell;
	}
	
	private List createSummaryColumns(FieldsDefinitionType fieldsDefinitionType){
		// here the used columns are defined !!
		
		List summaryColumns = new ArrayList();
        summaryColumns.add("Field");
        summaryColumns.add("Ra");
        summaryColumns.add("Dec");
        summaryColumns.add("Epoch");
        summaryColumns.add("Exposure");
        summaryColumns.add("Runs");
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_MOON, fieldsDefinitionType))
		{ 
        	summaryColumns.add("Moon");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING, fieldsDefinitionType)){
        	summaryColumns.add("Seeing");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING_RANGE, fieldsDefinitionType)){
        	summaryColumns.add("SeeingLow");
        	summaryColumns.add("SeeingUp");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_WATER, fieldsDefinitionType)){
        	summaryColumns.add("Water");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SN, fieldsDefinitionType)){
        	summaryColumns.add("SN");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_MAGNITUDE, fieldsDefinitionType)){
        	summaryColumns.add("Magnitude");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX, fieldsDefinitionType)){
        	summaryColumns.add("Flux");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX_FREQUENCY, fieldsDefinitionType)){
        	summaryColumns.add("FluxFrequency");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECTRAL_INDEX, fieldsDefinitionType)){
        	summaryColumns.add("SpectralIndex");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SKY_QUALITIES, fieldsDefinitionType)){
        	summaryColumns.add("Sky");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_DIAMETER, fieldsDefinitionType)){
        	summaryColumns.add("Diameter");
        }
        if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_OPPORTUNITY, fieldsDefinitionType)){
        	summaryColumns.add("Opportunity");
        }
        summaryColumns.add("Comments");
        return summaryColumns;
	}
	
	
}