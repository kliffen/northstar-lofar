<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processAddRegisteredMember" method="GET">

<TABLE>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD colspan="2">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				
				<logic:equal property="invalidUrl" value="true" name="addRegisteredMemberForm">
				<TD valign='top'><PRE>
Your acceptance of the invitation cannot be registered.

The key is incomplete, make sure the specified Url is being used.</PRE>
				</TD>
				</logic:equal>
				
				<logic:equal property="noInvitation" value="true" name="addRegisteredMemberForm">
				<TD valign='top'><PRE>
Your acceptance of the invitation cannot be registered.

Please make sure that the email address in your NorthStar personal account
information (My Account button) is the same as the email address to which
your invitation was sent. If they are not equal you could ask the inviting
person to re-issue the invitation to the email address of your choice.

If the email addresses are equal the following may have happened:
- Your name may have been removed from the proposal by another proposer,
- The proposal may have been deleted by another proposer.
				</PRE>
				</TD>
				</logic:equal>
				<logic:equal property="invalidUser" value="true" name="addRegisteredMemberForm">
				<TD valign='top'><PRE>
Your acceptance of the invitation cannot be registered.

You are already an active participant for this proposal.

The invitation is no longer valid.</PRE>
				</TD>
				</logic:equal>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	</TABLE>
	
<table width="100%">
	<TR>
		<td align="center"><html:submit styleClass="list_accept_yes" title="Ok">Ok</html:submit></TD>
	</TR>
</TABLE>
</html:form>