// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.status.history;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.User;
import nl.astron.util.AstronConverter;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalStatus;
import eu.radionet.northstar.data.entities.admin.StatusEmail;

public class SetUpViewStatusHistoryAction extends Action {
	private NorthStarDelegate northStarDelegate = null;
	
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ViewStatusHistoryForm viewStatusHistoryForm = (ViewStatusHistoryForm) form;

		northStarDelegate = NorthStarDelegate.getInstance();
		Proposal proposal = northStarDelegate.getProposal(AstronConverter.toInteger(viewStatusHistoryForm.getProposalId()));
		
		viewStatusHistoryForm.setStatusBeans(getStatusBeans(proposal.getStatusHistory()));
		viewStatusHistoryForm.setProposalCode(proposal.getCode());
		if (proposal.getJustification()!=null){
			viewStatusHistoryForm.setProposalTitle(proposal.getJustification().getTitle());
		}	

		return mapping.findForward(Constants.SUCCESS);
	}
    public List getStatusBeans(List statusHistory) throws DatabaseException{
        List beans = new ArrayList();
        for (int i = statusHistory.size() -1; i >= 0; i--){
        	ProposalStatus proposalStatus = (ProposalStatus)statusHistory.get(i);
        	StatusBean statusBean = new StatusBean();
        	statusBean.setStatusCode(proposalStatus.getStatus().getCode());
        	statusBean.setStatusTime(AstronConverter.toDateString(proposalStatus.getStatusTime()));
        	User user = northStarDelegate.getUser(proposalStatus.getUserId());
        	statusBean.setAuthor(NorthStarDelegate.getName(user));
	        StatusEmail statusEmail = northStarDelegate.getStatusEmailByProposalStatusId(proposalStatus.getId());
	        if (statusEmail != null){
	        	statusBean.setSender(statusEmail.getSender());
	        	statusBean.setToRecipients(statusEmail.getToRecipients());
	        	statusBean.setCcRecipients(statusEmail.getCcRecipients());
	        	statusBean.setBccRecipients(statusEmail.getBccRecipients());
	        	statusBean.setSubject(statusEmail.getSubject());
	        	statusBean.setMessage(statusEmail.getMessage());
	        	statusBean.setGeneratedMail(statusEmail.isGeneratedMail());
	        	statusBean.setEmail(true);
	        }
	        beans.add(statusBean);
        }
        return beans;
    }
}
