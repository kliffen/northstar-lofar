<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processSubmitProposal" method="POST">


<TABLE>

	<logic:equal name="submitForm" property="proposalErrors.empty" value="false">
	<tr>
		<td>
			<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
			<table>
				<tr>
					
					<td>
						<img src="<%=request.getContextPath()%>/images/list/alert.gif">
					</td>
					<td>Proposal with title '<B><bean:write name="submitForm" property="proposalTitle"/></B>' for telescope <b><bean:write name="submitForm" property="telescope"/></b> cannot be submitted</td>
				</tr>
			</table>
			<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</td>
	</tr>
	<logic:iterate id="proposalError" name="submitForm" property="proposalErrors.errors">
	<tr>
		<td>&nbsp;</td>
	</tr>
	<TR>
		<TD width="100%" class="header">Empty fields in <bean:write name="proposalError" property="description"/>:</TD>
	</TR>
	<TR>
		<TD width="100%">
			<jsp:include page="/pages/layout/box_header_no_top.jsp" />
			<TABLE width="100%">
			<logic:iterate id="proposalError1" name="proposalError" property="errors">
			<logic:equal name="proposalError1" property="errors.empty" value="false">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<TR>
				<TD class="header">Empty fields in <bean:write name="proposalError1" property="description"/>:</TD>
			</TR>
			<TR>
				<TD>
					<jsp:include page="/pages/layout/box_header_no_top.jsp" />
					<TABLE>
						<logic:iterate id="proposalError2" name="proposalError1" property="errors">
						<logic:equal name="proposalError2" property="errors.empty" value="false">
						<tr>
							<td>&nbsp;</td>
						</tr>
						<TR>
							<TD class="header">Empty fields in <bean:write name="proposalError2" property="description"/>:</TD>
						</TR>
						<TR>
							<TD>
								<jsp:include page="/pages/layout/box_header_no_top.jsp" />
								<TABLE width="100%">
								<logic:iterate id="proposalError3" name="proposalError2" property="errors">
									<TR>
										<TD>
											<bean:write name="proposalError3" property="description"/>
										</TD>
									</TR>
								</logic:iterate>
								</TABLE>
								<jsp:include page="/pages/layout/box_footer.jsp" />
							</TD>
						</TR>
						</logic:equal>
						<logic:equal name="proposalError2" property="errors.empty" value="true">
						<TR>
							<TD>
								<bean:write name="proposalError2" property="description"/>
							</TD>
						</TR>
						</logic:equal>
						</logic:iterate>
					</TABLE>
					<jsp:include page="/pages/layout/box_footer.jsp" />
				</TD>
			</TR>
			</logic:equal>
			<logic:equal name="proposalError1" property="errors.empty" value="true">
			<TR>
				<TD>
					<bean:write name="proposalError1" property="description"/>
				</TD>
			</TR>
			</logic:equal>
			</logic:iterate>
			</TABLE>
			<jsp:include page="/pages/layout/box_footer.jsp" />
   		</TD>
	 </TR>

	</logic:iterate>

	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD align="center"><html:cancel styleClass="list_accept_yes">Ok</html:cancel>
		</TD>
	</TR>
	</logic:equal>
	<logic:equal name="submitForm" property="proposalErrors.empty" value="true">
	<TR>
		<TD>
			<jsp:include page="/pages/layout/box_header.jsp" />
			<TABLE width="100%">
				<TR>
					<TD width="50" valign="top">
						<IMG src="<%=request.getContextPath()%>/images/list/submit-big.gif"/>
					</TD>
					<TD width="*" valign="top">
						<logic:empty name="submitForm" property="proposalCode">
							Are you really sure you want to submit proposal for telescope <b><bean:write name="submitForm" property="telescope"/></b><br> with title '<B><bean:write name="submitForm" property="proposalTitle"/></B>'  ?
						</logic:empty>
						<logic:notEmpty name="submitForm" property="proposalCode">
							Are you really sure you want to submit proposal for telescope <b><bean:write name="submitForm" property="telescope"/></b> with title '<bean:write name="submitForm" property="proposalTitle"/>' and with project id '<bean:write name="submitForm" property="proposalCode"/>'?
						</logic:notEmpty>
							<BR>The proposal to submit has category '<b><bean:write name="submitForm" property="proposalCategory"/></B>'
					</TD>
				</TR>
			</TABLE>
			<jsp:include page="/pages/layout/box_footer.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
</TABLE>
<TABLE width="100%">
	<TR>
		<TD align="left"><html:submit property="submitButton" styleClass="list_accept_yes" title="Ok, submit this proposal">Ok</html:submit></TD>
		
		<TD class="right"><html:cancel styleClass="list_decline_t" title="Cancel submit">Cancel</html:cancel>
		</TD>
	</TR>
	</logic:equal>
</TABLE>
</html:form>
