
#
# convert to innoDB
#
ALTER TABLE `additionalissues` TYPE=InnoDB;
ALTER TABLE `category` TYPE=InnoDB;
ALTER TABLE `complexbackend` TYPE=InnoDB;
ALTER TABLE `darkgreybrighttime` TYPE=InnoDB;
ALTER TABLE `invitation` TYPE=InnoDB;
ALTER TABLE `jcmtobservation` TYPE=InnoDB;
ALTER TABLE `jcmtobservingrequest` TYPE=InnoDB;
ALTER TABLE `justification` TYPE=InnoDB;
ALTER TABLE `justificationfile` TYPE=InnoDB;
ALTER TABLE `member` TYPE=InnoDB;
ALTER TABLE `memberrole` TYPE=InnoDB;
ALTER TABLE `nonregisteredmember` TYPE=InnoDB;
ALTER TABLE `observation` TYPE=InnoDB;
ALTER TABLE `observingrequest` TYPE=InnoDB;
ALTER TABLE `proposal` TYPE=InnoDB;
ALTER TABLE `proposallock` TYPE=InnoDB;
ALTER TABLE `proposalstatus` TYPE=InnoDB;
ALTER TABLE `pumabackend` TYPE=InnoDB;
ALTER TABLE `registeredmember` TYPE=InnoDB;
ALTER TABLE `role` TYPE=InnoDB;
ALTER TABLE `semester` TYPE=InnoDB;
ALTER TABLE `simplecontinuumbackend` TYPE=InnoDB;
ALTER TABLE `simplelinebackend` TYPE=InnoDB;
ALTER TABLE `status` TYPE=InnoDB;
ALTER TABLE `velocity` TYPE=InnoDB;
ALTER TABLE `vlbibackend` TYPE=InnoDB;
ALTER TABLE `whtintobservation` TYPE=InnoDB;
ALTER TABLE `whtintobservingrequest` TYPE=InnoDB;
ALTER TABLE `wsrtbackend` TYPE=InnoDB;
ALTER TABLE `wsrtobservation` TYPE=InnoDB;
ALTER TABLE `wsrtobservingrequest` TYPE=InnoDB;

#
# drop indexes
#
ALTER TABLE `complexbackend` DROP INDEX `wsrtbackend_IND`;
ALTER TABLE `jcmtobservation` DROP INDEX `observation_IND`;
ALTER TABLE `jcmtobservingrequest` DROP INDEX `observingrequest_IND`;
ALTER TABLE `justification` DROP INDEX `member_IND`;
ALTER TABLE `justificationfile` DROP INDEX `justification_IND`;
ALTER TABLE `member` DROP INDEX `proposal_IND`;
ALTER TABLE `memberrole` DROP INDEX `member_IND`;
ALTER TABLE `memberrole` DROP INDEX `role_IND`;
ALTER TABLE `nonregisteredmember` DROP INDEX `member`;
ALTER TABLE `observation` DROP INDEX `observingrequest_IND`;
ALTER TABLE `observingrequest` DROP INDEX `proposal_IND`;
ALTER TABLE `observingrequest` DROP INDEX `category_IND`;
ALTER TABLE `proposal` DROP INDEX `justification_IND`;
ALTER TABLE `proposal` DROP INDEX `additionalissues_IND`;
ALTER TABLE `proposal` DROP INDEX `semester_IND`;
ALTER TABLE `proposalstatus` DROP INDEX `status_IND`;
ALTER TABLE `proposalstatus` DROP INDEX `proposal_IND`;
ALTER TABLE `pumabackend` DROP INDEX `wsrtbackend_IND`;
ALTER TABLE `registeredmember` DROP INDEX `member`;
ALTER TABLE `semester` DROP INDEX `category_IND`;
ALTER TABLE `simplecontinuumbackend` DROP INDEX `wsrtbackend_IND`;
ALTER TABLE `simplelinebackend` DROP INDEX `wsrtbackend_IND`;
ALTER TABLE `vlbibackend` DROP INDEX `wsrtbackend_IND`;
ALTER TABLE `whtintobservation` DROP INDEX `observation_IND`;
ALTER TABLE `whtintobservingrequest` DROP INDEX `observingrequest_IND`;
ALTER TABLE `whtintobservingrequest` DROP INDEX `timeawarededprev_IND`;
ALTER TABLE `whtintobservingrequest` DROP INDEX `timerequestedfuture_IND`;
ALTER TABLE `whtintobservingrequest` DROP INDEX `requestedtime_IND`;
ALTER TABLE `whtintobservingrequest` DROP INDEX `minimumtime_IND`;
ALTER TABLE `wsrtobservation` DROP INDEX `wsrtbackend_IND`;
ALTER TABLE `wsrtobservation` DROP INDEX `observation_IND`;
ALTER TABLE `wsrtobservingrequest` DROP INDEX `observingrequest_IND`;

#
# Auto increment identifies
#
ALTER TABLE `additionalissues` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `category` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `complexbackend` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `darkgreybrighttime` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `invitation` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `jcmtobservation` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `jcmtobservingrequest` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `justification` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `justificationfile` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `member` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `memberrole` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `nonregisteredmember` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `observation` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `observingrequest` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `proposal` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `proposallock` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `proposalstatus` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `pumabackend` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `registeredmember` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `role` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `semester` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `simplecontinuumbackend` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `simplelinebackend` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `status` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `velocity` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `vlbibackend` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `whtintobservation` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `whtintobservingrequest` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `wsrtbackend` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `wsrtobservation` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;
ALTER TABLE `wsrtobservingrequest` MODIFY COLUMN `id` int(11) NOT NULL auto_increment;

#
# Other modifications
#
ALTER TABLE `justification` DROP COLUMN `memberid`;
ALTER TABLE `observingrequest` DROP COLUMN `categoryid`;
ALTER TABLE `invitation` MODIFY COLUMN `proposalid` int(11) NOT NULL;
ALTER TABLE `invitation` MODIFY COLUMN `memberid` int(11) NOT NULL;
ALTER TABLE `invitation` MODIFY COLUMN `acceptcode` char(20) NOT NULL;
ALTER TABLE `justificationfile` MODIFY COLUMN `justificationid` int(11) NOT NULL;
ALTER TABLE `memberrole` MODIFY COLUMN `roleid` int(11) NOT NULL;
ALTER TABLE `proposal` MODIFY COLUMN `currentstatusid` int(11) default NULL;
ALTER TABLE `proposallock` MODIFY COLUMN `proposalid` int(11) NOT NULL;
ALTER TABLE `proposallock` MODIFY COLUMN `sessionid` varchar(255) NOT NULL;
ALTER TABLE `proposallock` MODIFY COLUMN `userid` int(11) NOT NULL;
ALTER TABLE `proposallock` MODIFY COLUMN `since` datetime NOT NULL;
ALTER TABLE `proposallock` MODIFY COLUMN `lastaction` datetime NOT NULL;
ALTER TABLE `proposalstatus` MODIFY COLUMN `statusid` int(11) NOT NULL;
ALTER TABLE `proposalstatus` MODIFY COLUMN `userid` int(11) NOT NULL;
ALTER TABLE `proposalstatus` MODIFY COLUMN `statustime` datetime NOT NULL;
ALTER TABLE `registeredmember` MODIFY COLUMN `userid` int(11) NOT NULL;
ALTER TABLE `role` MODIFY COLUMN `name` varchar(255) NOT NULL;
ALTER TABLE `semester` MODIFY COLUMN `prefix` varchar(15) NOT NULL;
ALTER TABLE `semester` MODIFY COLUMN `startdatetime` datetime NOT NULL;
ALTER TABLE `semester` MODIFY COLUMN `enddatetime` datetime NOT NULL;
ALTER TABLE `semester` MODIFY COLUMN `deadlinedatetime` datetime NOT NULL;
ALTER TABLE `semester` MODIFY COLUMN `telescope` varchar(15) NOT NULL;
ALTER TABLE `semester` MODIFY COLUMN `categoryid` int(11) NOT NULL;
ALTER TABLE `status` MODIFY COLUMN `code` varchar(15) NOT NULL;
ALTER TABLE `status` MODIFY COLUMN `type` varchar(15) NOT NULL;


ALTER TABLE `complexbackend` ADD UNIQUE  `complexbackend_UNIQ` (`wsrtbackendid`);
ALTER TABLE `complexbackend` ADD CONSTRAINT `wsrtbackend_complexbackend_FK` FOREIGN KEY (`wsrtbackendid`) REFERENCES `wsrtbackend` (`id`) ON DELETE CASCADE;

ALTER TABLE `invitation` ADD INDEX `proposal_invitation_IND` (`proposalid`);
ALTER TABLE `invitation` ADD INDEX `member_invitation_IND` (`memberid`);

ALTER TABLE `jcmtobservation` ADD UNIQUE  `jcmtobservation_UNIQ` (`observationid`);
ALTER TABLE `jcmtobservation` ADD CONSTRAINT `observation_jcmtobservation_FK` FOREIGN KEY (`observationid`) REFERENCES `observation` (`id`) ON DELETE CASCADE;

ALTER TABLE `jcmtobservingrequest` ADD UNIQUE  `jcmtobservingrequest_UNIQ` (`observingrequestid`);
ALTER TABLE `jcmtobservingrequest` ADD CONSTRAINT `observingrequest_jcmtobservingrequest_FK` FOREIGN KEY (`observingrequestid`) REFERENCES `observingrequest` (`id`) ON DELETE CASCADE;

ALTER TABLE `justificationfile` ADD INDEX `justification_justificationfile_IND` (`justificationid`);
ALTER TABLE `justificationfile` ADD CONSTRAINT `justification_justificationfile_FK` FOREIGN KEY (`justificationid`) REFERENCES `justification` (`id`);

ALTER TABLE `member` ADD INDEX `proposal_member_IND` (`proposalid`);
ALTER TABLE `member` ADD INDEX `proposal_index_member_IND` (`indexid`);
ALTER TABLE `member` ADD CONSTRAINT `proposal_member_FK` FOREIGN KEY (`proposalid`) REFERENCES `proposal` (`id`);

ALTER TABLE `memberrole` ADD INDEX `member_memberrole_IND` (`memberid`);
ALTER TABLE `memberrole` ADD INDEX `role_memberrole_IND` (`roleid`);
ALTER TABLE `memberrole` ADD INDEX `member_index_memberrole_IND` (`indexid`);
ALTER TABLE `memberrole` ADD CONSTRAINT `member_memberrole_FK` FOREIGN KEY (`memberid`) REFERENCES `member` (`id`);
ALTER TABLE `memberrole` ADD CONSTRAINT `role_memberrole_FK` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`);

ALTER TABLE `nonregisteredmember` ADD UNIQUE  `nonregisteredmember_UNIQ` (`memberid`);
ALTER TABLE `nonregisteredmember` ADD CONSTRAINT `member_nonregisteredmember_FK` FOREIGN KEY (`memberid`) REFERENCES `member` (`id`);

ALTER TABLE `observation` ADD INDEX `observingrequest_observation_IND` (`observingrequestid`);
ALTER TABLE `observation` ADD INDEX `observingrequest_index_observation_IND` (`indexid`);
ALTER TABLE `observation` ADD CONSTRAINT `observingrequest_observation_FK` FOREIGN KEY (`observingrequestid`) REFERENCES `observingrequest` (`id`) ON DELETE CASCADE;

ALTER TABLE `observingrequest` ADD INDEX `proposal_observingrequest_IND` (`proposalid`);
ALTER TABLE `observingrequest` ADD INDEX `proposal_index_observingrequest_IND` (`indexid`);
ALTER TABLE `observingrequest` ADD CONSTRAINT `proposal_observingrequest_FK` FOREIGN KEY (`proposalid`) REFERENCES `proposal` (`id`) ON DELETE CASCADE;

ALTER TABLE `proposal` ADD INDEX `justification_proposal_IND` (`justificationid`);
ALTER TABLE `proposal` ADD INDEX `additionalissues_proposal_IND` (`additionalissuesid`);
ALTER TABLE `proposal` ADD INDEX `proposalstatus_proposal_IND` (`currentstatusid`);
ALTER TABLE `proposal` ADD INDEX `semester_proposal_IND` (`semesterid`);
ALTER TABLE `proposal` ADD CONSTRAINT `justification_proposal_FK` FOREIGN KEY (`justificationid`) REFERENCES `justification` (`id`) ;
ALTER TABLE `proposal` ADD CONSTRAINT `additionalissues_proposal_FK` FOREIGN KEY (`additionalissuesid`) REFERENCES `additionalissues` (`id`) ;
ALTER TABLE `proposal` ADD CONSTRAINT `proposalstatus_proposal_FK` FOREIGN KEY (`currentstatusid`) REFERENCES `proposalstatus` (`id`) ;
ALTER TABLE `proposal` ADD CONSTRAINT `semester_proposal_FK` FOREIGN KEY (`semesterid`) REFERENCES `semester` (`id`);

ALTER TABLE `proposallock` ADD INDEX `proposal_proposallock_IND` (`proposalid`);

ALTER TABLE `proposalstatus` ADD INDEX `proposal_proposalstatus_IND` (`proposalid`);
ALTER TABLE `proposalstatus` ADD INDEX `proposal_index_proposalstatus_IND` (`indexid`);
ALTER TABLE `proposalstatus` ADD INDEX `status_proposalstatus_IND` (`statusid`);
ALTER TABLE `proposalstatus` ADD CONSTRAINT `proposal_proposalstatus_FK` FOREIGN KEY (`proposalid`) REFERENCES `proposal` (`id`) ON DELETE CASCADE;
ALTER TABLE `proposalstatus` ADD CONSTRAINT `status_proposalstatus_FK` FOREIGN KEY (`statusid`) REFERENCES `status` (`id`);

ALTER TABLE `pumabackend` ADD UNIQUE  `pumabackend_UNIQ` (`wsrtbackendid`);
ALTER TABLE `pumabackend` ADD CONSTRAINT `wsrtbackend_pumabackend_FK` FOREIGN KEY (`wsrtbackendid`) REFERENCES `wsrtbackend` (`id`) ON DELETE CASCADE;

ALTER TABLE `registeredmember` ADD UNIQUE  `registeredmember_UNIQ` (`memberid`);
ALTER TABLE `registeredmember` ADD CONSTRAINT `member_registeredmember_FK` FOREIGN KEY (`memberid`) REFERENCES `member` (`id`) ON DELETE CASCADE;

ALTER TABLE `semester` ADD INDEX `category_semester_IND` (`categoryid`);
ALTER TABLE `semester` ADD CONSTRAINT `category_semester_FK` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`);

ALTER TABLE `simplecontinuumbackend` ADD UNIQUE  `simplecontinuumbackend_UNIQ` (`wsrtbackendid`);
ALTER TABLE `simplecontinuumbackend` ADD CONSTRAINT `wsrtbackend_simplecontinuumbackend_FK` FOREIGN KEY (`wsrtbackendid`) REFERENCES `wsrtbackend` (`id`) ON DELETE CASCADE;

ALTER TABLE `simplelinebackend` ADD UNIQUE  `simplelinebackend_UNIQ` (`wsrtbackendid`);
ALTER TABLE `simplelinebackend` ADD CONSTRAINT `wsrtbackend_simplelinebackend_FK` FOREIGN KEY (`wsrtbackendid`) REFERENCES `wsrtbackend` (`id`) ON DELETE CASCADE;

ALTER TABLE `velocity` ADD INDEX `simplelinebackend_velocity_IND` (`simplelinebackendid`);
ALTER TABLE `velocity` ADD INDEX `simplelinebackend_index_velocity_IND` (`indexid`);
ALTER TABLE `velocity` ADD CONSTRAINT `simplelinebackend_velocity_FK` FOREIGN KEY (`simplelinebackendid`) REFERENCES `simplelinebackend` (`wsrtbackendid`) ON DELETE CASCADE;

ALTER TABLE `vlbibackend` ADD UNIQUE  `vlbibackend_UNIQ` (`wsrtbackendid`);
ALTER TABLE `vlbibackend` ADD CONSTRAINT `wsrtbackend_vlbibackend_FK` FOREIGN KEY (`wsrtbackendid`) REFERENCES `wsrtbackend` (`id`) ON DELETE CASCADE;

ALTER TABLE `whtintobservation` ADD UNIQUE  `whtintobservation_UNIQ` (`observationid`);
ALTER TABLE `whtintobservation` ADD CONSTRAINT `observation_whtintobservation_FK` FOREIGN KEY (`observationid`) REFERENCES `observation` (`id`) ON DELETE CASCADE;

ALTER TABLE `whtintobservingrequest` ADD UNIQUE  `whtintobservingrequest_UNIQ` (`observingrequestid`);
ALTER TABLE `whtintobservingrequest` ADD INDEX `timeawardedprev_whtintobservingrequest_IND` (`timeawardedprevid`);
ALTER TABLE `whtintobservingrequest` ADD INDEX `timerequestedfuture_whtintobservingrequest_IND` (`timerequestedfutureid`);
ALTER TABLE `whtintobservingrequest` ADD INDEX `requestedtime_whtintobservingrequest_IND` (`requestedtimeid`);
ALTER TABLE `whtintobservingrequest` ADD INDEX `minimumtime_whtintobservingrequest_IND` (`minimumtimeid`);
ALTER TABLE `whtintobservingrequest` ADD CONSTRAINT `observingrequest_whtintobservingrequest_FK` FOREIGN KEY (`observingrequestid`) REFERENCES `observingrequest` (`id`) ON DELETE CASCADE;
ALTER TABLE `whtintobservingrequest` ADD CONSTRAINT `timeawardedprev_whtintobservingrequest_FK` FOREIGN KEY (`timeawardedprevid`) REFERENCES `darkgreybrighttime` (`id`);
ALTER TABLE `whtintobservingrequest` ADD CONSTRAINT `timerequestedfuture_whtintobservingrequest_FK` FOREIGN KEY (`timerequestedfutureid`) REFERENCES `darkgreybrighttime` (`id`);
ALTER TABLE `whtintobservingrequest` ADD CONSTRAINT `requestedtime_whtintobservingrequest_FK` FOREIGN KEY (`requestedtimeid`) REFERENCES `darkgreybrighttime` (`id`);
ALTER TABLE `whtintobservingrequest` ADD CONSTRAINT `minimumtime_whtintobservingrequest_FK` FOREIGN KEY (`minimumtimeid`) REFERENCES `darkgreybrighttime` (`id`);

ALTER TABLE `wsrtobservation` ADD UNIQUE  `wsrtobservation_UNIQ` (`observationid`);
ALTER TABLE `wsrtobservation` ADD INDEX `wsrtbackend_wsrtobservation_IND` (`wsrtbackendid`);
ALTER TABLE `wsrtobservation` ADD CONSTRAINT `wsrtbackend_wsrtobservation_FK` FOREIGN KEY (`wsrtbackendid`) REFERENCES `wsrtbackend` (`id`);
ALTER TABLE `wsrtobservation` ADD CONSTRAINT `observation_wsrtobservation_FK` FOREIGN KEY (`observationid`) REFERENCES `observation` (`id`) ON DELETE CASCADE;

ALTER TABLE `wsrtobservingrequest` ADD UNIQUE  `wsrtobservingrequest_UNIQ` (`observingrequestid`);
ALTER TABLE `wsrtobservingrequest` ADD CONSTRAINT `observingrequest_wsrtobservingrequest_FK` FOREIGN KEY (`observingrequestid`) REFERENCES `observingrequest` (`id`) ON DELETE CASCADE;

