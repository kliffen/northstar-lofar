// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Jan 13, 2004
 *
 */
package eu.radionet.northstar.data.entities;

import java.io.Serializable;
import java.util.Date;

/**
 * The Justification provides
 * 
 * @author Bastiaan Verhoef
 *  
 */
public class Justification implements Serializable {
    protected Integer id = null;

    protected String title = null;

    protected String abstractText = null;

    protected String scientificFileName = null;

    protected String scientificFilePath = null;

    protected Integer scientificFileSize = null;

    protected Date scientificFileDate = null;
    
    protected Integer scientificFilePages = null;
    
    protected String technicalDetailsFileName = null;

    protected String technicalDetailsFilePath = null;

    protected Integer technicalDetailsFileSize = null;

    protected Date technicalDetailsFileDate = null;
    
    protected Integer technicalDetailsFilePages = null;
    
    protected String figureFileName = null;

    protected String figureFilePath = null;

    protected Integer figureFileSize = null;

    protected Date figureFileDate = null;

    protected Integer figureFilePages = null;
 
    protected String envelopeSheet = null;
   
    protected String observationStrategyText = null;
    
 // fields for technical justification
 	private boolean nighttime;
 	private String nightTimeReason;
 	
 	
 	private boolean parallelObservation;
 	private String parallelObservationReason;
 	
 	
 	private boolean internationalStation;
 	private boolean internationalStationEssential;
 	private String internationalStationEssentialReason;
 	
 	private boolean otherSchedulingConstraints;
 	private String otherSchedulingConstraintsReason;
 	
 	private boolean combinedDataProductRequest;
 	private String combinedDataProductRequestReason;
 	
 	private Double sensitivity;

 	private Double maxDataRate;
 
 	private boolean roProcessing;
 	// testing autoboxing here
 	private boolean defaultROProcessing;
 	private String roProcessingReason;
 	
 	private boolean ltaRawStorage;
 	private String ltaRawStorageReason;

 	private boolean ltaStorage;
 	private String ltaStorageReason;
 	private String ltaStorageLocation;
 
 	private boolean offlineROProcessing;
 	private String offlineROProcessingReason;

 	private boolean externalProcessing;
 	private String externalProcessingReason;
 	
 	private boolean cepRequesting;
 	private String cepRequestingReason;
 	
 	private boolean fillerTime;
 	private String fillerTimeReason;
 	
 	private boolean coObservationTeam;
 	//---End fields for technical justification
    
    
    
    /**
     * @return Returns the abstractText.
     */
    public String getAbstractText() {
        return abstractText;
    }
    /**
     * @param abstractText The abstractText to set.
     */
    public void setAbstractText(String abstractText) {
        this.abstractText = abstractText;
    }
    /**
     * @return Returns the figureFileDate.
     */
    public Date getFigureFileDate() {
        return figureFileDate;
    }
    /**
     * @param figureFileDate The figureFileDate to set.
     */
    public void setFigureFileDate(Date figureFileDate) {
        this.figureFileDate = figureFileDate;
    }
    /**
     * @return Returns the figureFileName.
     */
    public String getFigureFileName() {
        return figureFileName;
    }
    /**
     * @param figureFileName The figureFileName to set.
     */
    public void setFigureFileName(String figureFileName) {
        this.figureFileName = figureFileName;
    }
    /**
     * @return Returns the figureFilePath.
     */
    public String getFigureFilePath() {
        return figureFilePath;
    }
    /**
     * @param figureFilePath The figureFilePath to set.
     */
    public void setFigureFilePath(String figureFilePath) {
        this.figureFilePath = figureFilePath;
    }
    /**
     * @return Returns the figureFileSize.
     */
    public Integer getFigureFileSize() {
        return figureFileSize;
    }
    /**
     * @param figureFileSize The figureFileSize to set.
     */
    public void setFigureFileSize(Integer figureFileSize) {
        this.figureFileSize = figureFileSize;
    }
    /**
     * @return Returns the id.
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id The id to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * @return Returns the scientificFileDate.
     */
    public Date getScientificFileDate() {
        return scientificFileDate;
    }
    /**
     * @param scientificFileDate The scientificFileDate to set.
     */
    public void setScientificFileDate(Date scientificFileDate) {
        this.scientificFileDate = scientificFileDate;
    }
    /**
     * @return Returns the scientificFileName.
     */
    public String getScientificFileName() {
        return scientificFileName;
    }
    /**
     * @param scientificFileName The scientificFileName to set.
     */
    public void setScientificFileName(String scientificFileName) {
        this.scientificFileName = scientificFileName;
    }
    /**
     * @return Returns the scientificFilePath.
     */
    public String getScientificFilePath() {
        return scientificFilePath;
    }
    /**
     * @param scientificFilePath The scientificFilePath to set.
     */
    public void setScientificFilePath(String scientificFilePath) {
        this.scientificFilePath = scientificFilePath;
    }
    /**
     * @return Returns the scientificFileSize.
     */
    public Integer getScientificFileSize() {
        return scientificFileSize;
    }
    /**
     * @param scientificFileSize The scientificFileSize to set.
     */
    public void setScientificFileSize(Integer scientificFileSize) {
        this.scientificFileSize = scientificFileSize;
    }
    /**
     * @return Returns the technicalDetailsFileDate.
     */
    public Date getTechnicalDetailsFileDate() {
        return technicalDetailsFileDate;
    }
    /**
     * @param technicalDetailsFileDate The technicalDetailsFileDate to set.
     */
    public void setTechnicalDetailsFileDate(Date technicalDetailsFileDate) {
        this.technicalDetailsFileDate = technicalDetailsFileDate;
    }
    /**
     * @return Returns the technicalDetailsFileName.
     */
    public String getTechnicalDetailsFileName() {
        return technicalDetailsFileName;
    }
    /**
     * @param technicalDetailsFileName The technicalDetailsFileName to set.
     */
    public void setTechnicalDetailsFileName(String technicalDetailsFileName) {
        this.technicalDetailsFileName = technicalDetailsFileName;
    }
    /**
     * @return Returns the technicalDetailsFilePath.
     */
    public String getTechnicalDetailsFilePath() {
        return technicalDetailsFilePath;
    }
    /**
     * @param technicalDetailsFilePath The technicalDetailsFilePath to set.
     */
    public void setTechnicalDetailsFilePath(String technicalDetailsFilePath) {
        this.technicalDetailsFilePath = technicalDetailsFilePath;
    }
    /**
     * @return Returns the technicalDetailsFileSize.
     */
    public Integer getTechnicalDetailsFileSize() {
        return technicalDetailsFileSize;
    }
    /**
     * @param technicalDetailsFileSize The technicalDetailsFileSize to set.
     */
    public void setTechnicalDetailsFileSize(Integer technicalDetailsFileSize) {
        this.technicalDetailsFileSize = technicalDetailsFileSize;
    }
    /**
     * @return Returns the title.
     */
    public String getTitle() {
        return title;
    }
    /**
     * @param title The title to set.
     */
    public void setTitle(String title) {
        this.title = title;
    }
	public Integer getFigureFilePages() {
		return figureFilePages;
	}
	public void setFigureFilePages(Integer figureFilePages) {
		this.figureFilePages = figureFilePages;
	}
	public Integer getScientificFilePages() {
		return scientificFilePages;
	}
	public void setScientificFilePages(Integer scientificFilePages) {
		this.scientificFilePages = scientificFilePages;
	}
	public Integer getTechnicalDetailsFilePages() {
		return technicalDetailsFilePages;
	}
	public void setTechnicalDetailsFilePages(Integer technicalDetailsFilePages) {
		this.technicalDetailsFilePages = technicalDetailsFilePages;
	}
	public String getEnvelopeSheet() {
		return envelopeSheet;
	}
	public void setEnvelopeSheet(String envelopeSheet) {
		this.envelopeSheet = envelopeSheet;
	}
	public String getObservationStrategyText() {
		return observationStrategyText;
	}
	public void setObservationStrategyText(String observationStrategyText) {
		this.observationStrategyText = observationStrategyText;
	}
	public boolean isNighttime() {
		return nighttime;
	}
	public String getNightTimeReason() {
		return nightTimeReason;
	}
	public boolean isFillerTime() {
		return fillerTime;
	}
	public void setFillerTime(boolean fillerTime) {
		this.fillerTime = fillerTime;
	}
	public String getFillerTimeReason() {
		return fillerTimeReason;
	}
	public void setFillerTimeReason(String fillerTimeReason) {
		this.fillerTimeReason = fillerTimeReason;
	}
	public boolean isCoObservationTeam() {
		return coObservationTeam;
	}
	public void setCoObservationTeam(boolean coObservationTeam) {
		this.coObservationTeam = coObservationTeam;
	}
	public boolean isParallelObservation() {
		return parallelObservation;
	}
	public String getParallelObservationReason() {
		return parallelObservationReason;
	}
	public boolean isInternationalStation() {
		return internationalStation;
	}
	public boolean isInternationalStationEssential() {
		return internationalStationEssential;
	}
	public String getInternationalStationEssentialReason() {
		return internationalStationEssentialReason;
	}
	public boolean isOtherSchedulingConstraints() {
		return otherSchedulingConstraints;
	}
	public String getOtherSchedulingConstraintsReason() {
		return otherSchedulingConstraintsReason;
	}
	public boolean isCombinedDataProductRequest() {
		return combinedDataProductRequest;
	}
	public String getCombinedDataProductRequestReason() {
		return combinedDataProductRequestReason;
	}
	public Double getSensitivity() {
		return sensitivity;
	}
	
	public Double getMaxDataRate() {
		return maxDataRate;
	}

	public boolean isRoProcessing() {
		return roProcessing;
	}
	public boolean isDefaultROProcessing() {
		return defaultROProcessing;
	}
	public String getRoProcessingReason() {
		return roProcessingReason;
	}
	public boolean isLtaStorage() {
		return ltaStorage;
	}
	public String getLtaStorageReason() {
		return ltaStorageReason;
	}
	public boolean isLtaRawStorage() {
		return ltaRawStorage;
	}
	public String getLtaRawStorageReason() {
		return ltaRawStorageReason;
	}
	public boolean isOfflineROProcessing() {
		return offlineROProcessing;
	}
	public String getOfflineROProcessingReason() {
		return offlineROProcessingReason;
	}
	public boolean isExternalProcessing() {
		return externalProcessing;
	}
	public String getExternalProcessingReason() {
		return externalProcessingReason;
	}
	public boolean isCepRequesting() {
		return cepRequesting;
	}
	public String getCepRequestingReason() {
		return cepRequestingReason;
	}		
	public void setNighttime(boolean nighttime) {
		this.nighttime = nighttime;
	}
	public void setNightTimeReason(String nightTimeReason) {
		this.nightTimeReason = nightTimeReason;
	}
	public void setParallelObservation(boolean parallelObservation) {
		this.parallelObservation = parallelObservation;
	}
	public void setParallelObservationReason(String parallelObservationReason) {
		this.parallelObservationReason = parallelObservationReason;
	}
	public void setInternationalStation(boolean internationalStation) {
		this.internationalStation = internationalStation;
	}
	public void setInternationalStationEssential(
			boolean internationalStationEssential) {
		this.internationalStationEssential = internationalStationEssential;
	}
	public void setInternationalStationEssentialReason(
			String internationalStationEssentialReason) {
		this.internationalStationEssentialReason = internationalStationEssentialReason;
	}
	public void setOtherSchedulingConstraints(boolean otherSchedulingConstraints) {
		this.otherSchedulingConstraints = otherSchedulingConstraints;
	}
	public void setOtherSchedulingConstraintsReason(
			String otherSchedulingConstraintsReason) {
		this.otherSchedulingConstraintsReason = otherSchedulingConstraintsReason;
	}
	public void setCombinedDataProductRequest(boolean combinedDataProductRequest) {
		this.combinedDataProductRequest = combinedDataProductRequest; 
	}
	public void setCombinedDataProductRequestReason(
			String combinedDataProductRequestReason) {
		this.combinedDataProductRequestReason = combinedDataProductRequestReason;
	}
	public void setSensitivity(Double sensitivity) {
		this.sensitivity = sensitivity;
	}
	
	public void setMaxDataRate(Double maxDataRate) {
		this.maxDataRate = maxDataRate;
	}
	public void setRoProcessing(boolean roProcessing) {
		this.roProcessing = roProcessing;
	}
	public void setDefaultROProcessing(boolean defaultROProcessing) {
		this.defaultROProcessing = defaultROProcessing;
	}
	public void setRoProcessingReason(String roProcessingReason) {
		this.roProcessingReason = roProcessingReason;
	}
	public void setLtaStorage(boolean ltaStorage) {
		this.ltaStorage = ltaStorage;
	}
	public void setLtaStorageReason(String ltaStorageReason) {
		this.ltaStorageReason = ltaStorageReason;
	}
	public void setLtaRawStorage(boolean ltaRawStorage) {
		this.ltaRawStorage = ltaRawStorage;
	}
	public void setLtaRawStorageReason(String ltaRawStorageReason) {
		this.ltaRawStorageReason = ltaRawStorageReason;
	}
	public void setOfflineROProcessing(boolean offlineROProcessing) {
		this.offlineROProcessing = offlineROProcessing;
	}
	public void setOfflineROProcessingReason(String offlineROProcessingReason) {
		this.offlineROProcessingReason = offlineROProcessingReason;
	}
	public void setExternalProcessing(boolean externalProcessing) {
		this.externalProcessing = externalProcessing;
	}
	public void setExternalProcessingReason(String externalProcessingReason) {
		this.externalProcessingReason = externalProcessingReason;
	}
	public void setCepRequesting(boolean cepRequesting) {
		this.cepRequesting = cepRequesting;
	}
	public void setCepRequestingReason(String cepRequestingReason) {
		this.cepRequestingReason = cepRequestingReason;
	}
	public String getLtaStorageLocation() {
		return ltaStorageLocation;
	}
	public void setLtaStorageLocation(String ltaStorageLocation) {
		this.ltaStorageLocation = ltaStorageLocation;
	}	
}