package eu.radionet.northstar.control.admin.statistics;

import java.util.ArrayList;
import java.util.List;
import org.apache.struts.action.ActionForm;

public class StatisticsForm extends ActionForm {

	private String message = null;
	private String nextButton = null;
	private String exportButton = null;
	// totals
	private String totalProposals = null;
	private String totalTargets = null;
	private String totalTime = null;
	private String totalStorage = null;
	private List statisticsBeans = new ArrayList();
	private List statisticsTotalsBeans = new ArrayList();
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getNextButton() {
		return nextButton;
	}
	public void setNextButton(String nextButton) {
		this.nextButton = nextButton;
	}
	public String getExportButton() {
		return exportButton;
	}
	public void setExportButton(String exportButton) {
		this.exportButton = exportButton;
	}
	public String getTotalTargets() {
		return totalTargets;
	}
	public void setTotalTargets(String totalTargets) {
		this.totalTargets = totalTargets;
	}
	public String getTotalTime() {
		return totalTime;
	}
	public void setTotalTime(String totalTime) {
		this.totalTime = totalTime;
	}
	public String getTotalStorage() {
		return totalStorage;
	}
	public void setTotalStorage(String totalStorage) {
		this.totalStorage = totalStorage;
	}
	public List getStatisticsBeans() {
		return statisticsBeans;
	}
	public void setStatisticsBeans(List statisticsBeans) {
		this.statisticsBeans = statisticsBeans;
	}
	public String getTotalProposals() {
		return totalProposals;
	}
	public void setTotalProposals(String totalproposals) {
		this.totalProposals = totalproposals;
	}
	public List getStatisticsTotalsBeans() {
		return statisticsTotalsBeans;
	}
	public void setStatisticsTotalsBeans(List statisticsTotalsBeans) {
		this.statisticsTotalsBeans = statisticsTotalsBeans;
	}
}
