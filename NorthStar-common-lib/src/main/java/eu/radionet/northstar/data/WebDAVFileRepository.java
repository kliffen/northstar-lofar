// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.data;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import nl.astron.util.AstronConverter;

import org.apache.commons.httpclient.Credentials;
import org.apache.commons.httpclient.HostConfiguration;
import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpConnectionManager;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.UsernamePasswordCredentials;
import org.apache.commons.httpclient.auth.AuthScope;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.InputStreamRequestEntity;
import org.apache.commons.httpclient.methods.RequestEntity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.jackrabbit.webdav.client.methods.MkColMethod;
import org.apache.jackrabbit.webdav.client.methods.PropFindMethod;
import org.apache.jackrabbit.webdav.client.methods.PropPatchMethod;
import org.apache.jackrabbit.webdav.client.methods.PutMethod;
import org.apache.jackrabbit.webdav.property.DavProperty;
import org.apache.jackrabbit.webdav.property.DavPropertyName;
import org.apache.jackrabbit.webdav.property.DavPropertyNameSet;
import org.apache.jackrabbit.webdav.property.DavPropertySet;
import org.apache.jackrabbit.webdav.property.DefaultDavProperty;

import eu.radionet.northstar.data.entities.Proposal;

public class WebDAVFileRepository {
	protected Log log = LogFactory.getLog(this.getClass());

	public void store(Proposal proposal, byte[] fileData) throws IOException, URISyntaxException {
		// get the location of the webdav server from northstar-config.xml

		URI resourceUrl = new URI(WebDAVConfiguration.getUrl());

		// connect to the webdav server
		HttpClient client = getWebdavClient(resourceUrl);

		// get the location of the pdf
		String location = WebDAVConfiguration.getPdfLocation(proposal);
		// split the location and check for each directory if it exists, if not create
		// the directory
		String[] dirElements = location.split("/");
		String currentFolder = resourceUrl.getPath();
		if (!currentFolder.endsWith("/")) {
			currentFolder += "/";
		}
		int i = 0;
		while (i < dirElements.length - 1) {
			currentFolder = currentFolder + dirElements[i];
			if (!currentFolder.endsWith("/")) {
				currentFolder += "/";
			}
			createOrUpdateWebdavFolder(client, currentFolder, proposal.getId());
			i++;
		}

		String filePath = resourceUrl.getPath() + location;
		String proposalId = AstronConverter.toString(proposal.getId());
		String semesterId = AstronConverter.toString(proposal.getSemester().getId());

		// source, dest, overwrite
		PutMethod upload = new PutMethod(filePath);

		// InputStream inp = new ByteArrayInputStream(fileData);
		// upload.setRequestBody(inp); // deprecated

		RequestEntity requestEntity = new InputStreamRequestEntity(new ByteArrayInputStream(fileData));

		upload.setRequestEntity(requestEntity);

		/*
		 * DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		 * DocumentBuilder builder; try { builder = factory.newDocumentBuilder();
		 * Document doc = builder.parse(new ByteArrayInputStream(fileData));
		 * 
		 * //upload.setRequestBody( );
		 * upload.setRequestBody(doc);//setRequestEntity(requestEntity); } catch
		 * (ParserConfigurationException e) { // TODO Auto-generated catch block
		 * e.printStackTrace(); } catch (SAXException e) { // TODO Auto-generated catch
		 * block e.printStackTrace(); }
		 */
		int responseCode = executeMethod(client, upload);

		if (responseCode != HttpStatus.SC_CREATED) {
			log.warn("Webdav upload error, response code: " + responseCode);
		} else {
			log.info("uploaded file to " + filePath);
		}

		String readAccess = WebDAVConfiguration.NORTHSTAR_PROPOSAL_ID + proposalId;
		readAccess = readAccess + "," + WebDAVConfiguration.NORTHSTAR_REVIEW_PROPOSAL_ID + proposalId;
		readAccess = readAccess + "," + WebDAVConfiguration.NORTHSTAR_REVIEW_SEMESTER_ID + semesterId;
		DavPropertySet propSet = new DavPropertySet();
		DavProperty prop = new DefaultDavProperty(DavPropertyName.create(WebDAVConfiguration.READ_ACCESS), readAccess);
		propSet.add(prop);
		PropPatchMethod propPatch = new PropPatchMethod(filePath, propSet, new DavPropertyNameSet());

		log.info("set access " + readAccess);
		responseCode = executeMethod(client, propPatch);
		if (responseCode != HttpStatus.SC_CREATED) {
			log.warn("Webdav propset error, response code: " + responseCode);
		}
	}

	private int executeMethod(HttpClient client, HttpMethod davmethod) {
		int responseCode = 0;
		try {
			responseCode = client.executeMethod(davmethod);
		} catch (IOException e) {
			log.error("io error ", e);
		} finally {
			// inp.close();
			// Release the connection.
			davmethod.releaseConnection();
		}
		return responseCode;
	}

	protected HttpClient getWebdavClient(URI resourceUrl) throws IOException {
		HttpClient client = null;
		/*
		 * This creates the connection to, and session on, the webdav server
		 * 
		 * WeddavResource(url) throws HttpException if response is other than "OK" (200
		 * or 207)
		 */
		HostConfiguration hostConfig = new HostConfiguration();

		hostConfig.setHost(resourceUrl.getHost(), resourceUrl.getPort());

		HttpConnectionManager connectionManager = new MultiThreadedHttpConnectionManager();
		// HttpConnectionManagerParams params = new HttpConnectionManagerParams();
		// int maxHostConnections = 20;
		// params.setMaxConnectionsPerHost(hostConfig, maxHostConnections);
		// connectionManager.setParams(params);
		client = new HttpClient(connectionManager);
		Credentials creds = new UsernamePasswordCredentials(WebDAVConfiguration.getUser(),
				WebDAVConfiguration.getPassword());
		client.getState().setCredentials(new AuthScope(resourceUrl.getHost(), resourceUrl.getPort()), creds);
		client.setHostConfiguration(hostConfig);

		// test connection
		// Create a method instance.
		GetMethod method = new GetMethod(resourceUrl.toString());

		int statusCode = executeMethod(client, method);
		if (statusCode != HttpStatus.SC_OK) {
			log.warn("Could not connect to " + resourceUrl.toString() + " response code: " + statusCode);
		}
		return client;
	}

	/**
	 * Create or update webdav folder, with properties
	 * 
	 * @param client
	 * @param location
	 * @param properties
	 * @throws IOException
	 */
	protected void createOrUpdateWebdavFolder(HttpClient client, String location, Integer proposalId)
			throws IOException {
		PropFindMethod propfind = new PropFindMethod(location);
		int statusCode = 0;

		statusCode = executeMethod(client, propfind);
		if (statusCode == HttpStatus.SC_NOT_FOUND) {
			// log.info("mkCol: "+location);
			MkColMethod mkCol = new MkColMethod(location);
			statusCode = executeMethod(client, mkCol);

			if (statusCode == HttpStatus.SC_CREATED) {
				DavPropertySet propSet = new DavPropertySet();
				DavProperty prop = new DefaultDavProperty(DavPropertyName.create(WebDAVConfiguration.READ_ACCESS),
						WebDAVConfiguration.NORTHSTAR);
				propSet.add(prop);
				PropPatchMethod propPatch = new PropPatchMethod(location, propSet, new DavPropertyNameSet());

				statusCode = executeMethod(client, propPatch);
				if (statusCode != HttpStatus.SC_OK) {
					log.warn("could not set properties for: " + location + " response code: " + statusCode);
				}

			} else {
				log.warn("could not create dir: " + location);
			}
		}

	}

	public byte[] getProposalFile(Proposal proposal) throws IOException, URISyntaxException {


		URI resourceUrl = new URI(WebDAVConfiguration.getUrl());
		
		// connect to the webdav server
		HttpClient client = getWebdavClient(resourceUrl);
		
		//// Added by Roel
		//HttpClient client = new HttpClient(); 
		//client.getState().setCredentials(AuthScope.ANY, new UsernamePasswordCredentials("northstar", "northstar"));
		
		// get the location of the pdf
		String location = WebDAVConfiguration.getPdfLocation(proposal);

		// GetMethod downloadAction=new GetMethod(location);
		GetMethod downloadAction = new GetMethod(resourceUrl.toString() + location.substring(1));

		// Lines added by Roel
		byte[] responseBody = null;
		int responseCode = 0;
		try {
			responseCode = client.executeMethod(downloadAction);
		} catch (IOException e) {
			log.error("io error ", e);
		} finally {
			responseBody = downloadAction.getResponseBody();
			downloadAction.releaseConnection();
			
		}
		if (responseCode != HttpStatus.SC_OK) {
			log.warn("Webdav error at " + resourceUrl.toString() + " for file: " + location + " response code: "
					+ responseCode);
		}
		return responseBody;
	}

}
