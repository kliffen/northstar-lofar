<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<html:form action="/processPreviewEmail" method="POST">

<html:hidden property="proposalId"/>
<TABLE>
	<TR>
		<TD>
			<TABLE>
				<tr>
					<td colspan="2">
						<bean:write name="previewEmailForm" property="proposalTitle"/>
						(<bean:write name="previewEmailForm" property="proposalCode"/>)
					</td>				
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>				
				</tr>
				<tr>
					<td colspan="2"><b>Change status:</b></td>				
				</tr>
				<tr>
					<td class="right">Current status:&nbsp;</td>
					<td class="bold"><bean:write name="previewEmailForm" property="currentStatus"/>
			
					</td>
				</tr>		
				<tr>
					<td class="right">Choose new status:&nbsp;</td>
					<td class="bold"><bean:write name="previewEmailForm" property="newStatus"/>
			
					</td>
				</tr>
			</TABLE>
		</TD>
	</TR>
	<tr>
		<td>&nbsp;</td>				
	</tr>	
	<tr>
		<td>
			<TABLE>
				<tr>
					<td><b>Email for Contact Author:</b></td>				
				</tr>	
				<tr>
					<td>
						<table align="center" class="emailtable">
							<TR>
			     				<td class="emailheaderitem">&nbsp;From:&nbsp;</td>
			     				<td class="emailheadervalue">&nbsp;<bean:write name="previewEmailForm" property="sender"/>&nbsp;</td>
			     			</TR>
							<TR>
			     				<td class="emailheaderitem">&nbsp;To:&nbsp;</td>
			     				<td class="emailheadervalue">&nbsp;<bean:write name="previewEmailForm" property="receivers"/>&nbsp;</td>
			     			</TR>  
							<TR>
			     				<td class="emailheaderitem">&nbsp;Subject:&nbsp;</td>
			     				<td class="emailsubject">&nbsp;<bean:write name="previewEmailForm" property="subject"/>&nbsp;</td>
			     			</TR>
							<TR>
			     				<td class="emailcontent" colspan="2"><pre><bean:write name="previewEmailForm" property="message"/></pre></td>
			    			</TR>  
			    		</table>     			        			   			
					</td>
				</tr>
			</TABLE>
		</td>
	</tr>

</TABLE><br>
<table width="100%">
	<tr>
					<td align="left"><html:submit property="nextButton"  tabindex="5" styleClass="list_next">Change status and send email</html:submit></TD>
			<td align="left"><html:submit property="changeButton"  tabindex="6" styleClass="list_next">Change status without email</html:submit></TD>
		<td align="right"><html:cancel >Back</html:cancel>
 		</td>
	</tr>
</table>
</html:form>
