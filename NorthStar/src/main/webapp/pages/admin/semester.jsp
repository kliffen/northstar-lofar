<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />
<html:form action="/processSemester" focus="telescope" method="POST">
<html:hidden property="semesterId"/>

<table>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.admin.semester.telescope" /></td>
		<td  align="left" class="bold">
			<logic:empty name="semesterForm" property="semesterId" >
				<bean:define id="telescopes" name="semesterForm" property="telescopes"/> 
				<html:select property="telescope" onchange="document.forms[0].submit()">
					<html:options name="telescopes"/>		
				</html:select><span class="error"><astron:errors property="telescope" ifnotexist="*"/></span>
			</logic:empty>
			<logic:notEmpty name="semesterForm" property="semesterId">
				<bean:write name="semesterForm" property="telescope"/>

			</logic:notEmpty>
		</td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.admin.semester.community" /></td>
		<td  align="left" class="bold">
			<logic:empty name="semesterForm" property="semesterId" >
				<bean:define id="communities" name="semesterForm" property="communities"/> 
				<html:select property="community">
					<html:options collection="communities" property="value" labelProperty="label"/>		
				</html:select><span class="error"><astron:errors property="community" ifnotexist="*"/></span>
			</logic:empty>
			<logic:notEmpty name="semesterForm" property="semesterId">
				<bean:write name="semesterForm" property="communityLabel"/>
				<bean:write name="semesterForm" property="community"/>
			</logic:notEmpty>
		</td>
	</tr>	
	<tr>
		<td class="right"><astron:label key="label.admin.semester.category" /></td>
		<td  align="left" class="bold">
			<logic:empty name="semesterForm" property="semesterId" >
				<bean:define id="categories" name="semesterForm" property="categories"/> 
				<html:select property="category">
					<html:options collection="categories" property="id" labelProperty="code"/>		
				</html:select><span class="error"><astron:errors property="category" ifnotexist="*"/></span>
			</logic:empty>
			<logic:notEmpty name="semesterForm" property="semesterId">
				<bean:write name="semesterForm" property="category"/>
			</logic:notEmpty>
		</td>
	</tr>	
	
	<tr>
		<td class="right"><astron:label key="label.admin.semester.semester" /></td>
		<td align="left" class="bold">
			<logic:empty name="semesterForm" property="semesterId" >
				<html:text property="semester"  tabindex="1" maxlength="255"/><span class="error"><astron:errors property="semester" ifnotexist="*"/></span>
			</logic:empty>
			<logic:notEmpty name="semesterForm" property="semesterId">
				<bean:write name="semesterForm" property="semester"/>
				<html:hidden property="semester"/>
			</logic:notEmpty>
		</td>
	</tr>		
	<tr>
		<td class="right"><astron:label key="label.admin.semester.prefix" /></td>
		<td  align="left" class="bold">
			<logic:empty name="semesterForm" property="semesterId" >
				<html:text property="prefix"  tabindex="1" maxlength="255"/><span class="error"><astron:errors property="prefix" ifnotexist="*"/></span>
			</logic:empty>
			<logic:notEmpty name="semesterForm" property="semesterId">
				<bean:write name="semesterForm" property="prefix"/>
				<html:hidden property="prefix"/>
			</logic:notEmpty>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" align="center"><astron:label key="label.admin.semester.dateformat" /></td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.admin.semester.available" /></td>
		<td class="left"><html:text property="availableTime" tabindex="3"  maxlength="255"/><html:img page="/images/list/cal.gif" module="" onclick="scwShow(document.forms[0].availableTime, this, 'YYYY/MM/DD 00:00:00','YMD');"/><span class="error"><astron:errors property="availableTime" ifnotexist="*"/></span></td>
	</tr>		
	<tr>
		<td class="right"><astron:label key="label.admin.semester.start" /></td>
		<td class="left"><html:text property="startTime" tabindex="4"  maxlength="255"/><html:img page="/images/list/cal.gif" module="" onclick="scwShow(document.forms[0].startTime, this, 'YYYY/MM/DD 00:00:00','YMD');"/><span class="error"><astron:errors property="startTime" ifnotexist="*"/></span></td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.admin.semester.end" /></td>
		<td class="left"><html:text property="endTime" tabindex="5"  maxlength="255"/><html:img page="/images/list/cal.gif" module="" onclick="scwShow(document.forms[0].endTime, this, 'YYYY/MM/DD 00:00:00','YMD');"/><span class="error"><astron:errors property="endTime" ifnotexist="*"/></span></td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.admin.semester.deadline" /></td>
		<td class="left"><html:text property="deadLineTime"   tabindex="6"  maxlength="255"/><html:img page="/images/list/cal.gif" module="" onclick="scwShow(document.forms[0].deadLineTime, this, 'YYYY/MM/DD 00:00:00','YMD');"/><span class="error"><astron:errors property="deadLineTime" ifnotexist="*"/></span></td>
	</tr>
	
	<tr>
		<td class="right"><astron:label key="label.admin.semester.deadlinedelay" /></td>
		<td class="left"><html:text property="deadLineDelay"   tabindex="7"  maxlength="255"/><span class="error"><astron:errors property="deadLineDelay" ifnotexist="*"/></span></td>
	</tr>
	
	<tr>
		<td class="right"><astron:label key="label.admin.semester.selectparent" /></td>
		<td class="left">
		<bean:define id="leParents" name="semesterForm" property="parents"/>
	      <html:select property="selectedParentId">
					<html:options collection="leParents" property="value" labelProperty="label"/>
				</html:select>
		</td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.admin.semester.immediate" /></td>
		<td class="left"><html:multibox property="immediate">true</html:multibox><span class="error">*</span></td>
	</tr>	
	<tr>
		<td class="right"><astron:label key="label.admin.semester.lastnumber" /></td>
		<td class="left">
			<logic:empty name="semesterForm" property="semesterId" >0<html:hidden property="lastAssignedNumber"/></logic:empty>
			<logic:notEmpty name="semesterForm" property="semesterId">
			<html:text property="lastAssignedNumber"   tabindex="7"  maxlength="255"/><span class="error"><astron:errors property="lastAssignedNumber" ifnotexist="*"/></span></td>

			</logic:notEmpty>
	</tr>	
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>

	<tr>
		<td colspan="2"><span class="error"><astron:errors property="semesters" ifnotexist=""/></span>&nbsp;</td>
	</tr>		
</table>
<table width="100%">
	<tr>
		<td align="left"><html:submit property="storeButton"  tabindex="8" styleClass="list_accept_yes">Ok</html:submit>
 		</td>
		<td align="right"><html:cancel styleClass="list_decline_t">Cancel</html:cancel></TD>
	</tr>
</table>


</html:form>