// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.deadline;

import java.util.Date;
import java.util.Timer;

import javax.servlet.ServletConfig;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Semester;

public class TaskDaemon {
	private Log log = LogFactory.getLog(this.getClass());
	private static TaskDaemon taskDaemon = null;
	private static Timer timer = null;
	private ServletConfig servletConfig = null;
	

	private TaskDaemon(ServletConfig servletConfig) {
		timer = new Timer(true);
		this.servletConfig = servletConfig;
	}

	public static TaskDaemon getInstance(ServletConfig servletConfig) {
		if (taskDaemon == null) {
			taskDaemon = new TaskDaemon(servletConfig);

		}
		return taskDaemon;
	}
	public static TaskDaemon getTaskDeamon(){
		return taskDaemon;
	}
	public void addDeadlineTask(Semester semester){
		DeadlineTask task = new DeadlineTask(semester,servletConfig);
		Date deadLine = semester.getDeadLine();
		deadLine.setTime(deadLine.getTime()+(semester.getDeadLineDelay()*1000));
		timer.schedule(task,deadLine);
		
	}
	public void addTask(Semester semester, Date executeDate){
		DeadlineTask task = new DeadlineTask(semester,servletConfig);
		timer.schedule(task,executeDate);
		
	}
	public void addTask(Proposal proposal){
		ProposalTask task = new ProposalTask(proposal,servletConfig);
		try{
			timer.schedule(task,new Date());
		}catch(IllegalStateException e){
			log.warn("Timer has already been cancelled, creating new one ...");
			timer = new Timer(true);
			timer.schedule(task,new Date());
		}
	}
	public void cancelAllTasks(){
		log.info("Cancel all tasks");
		timer.cancel();
		timer = new Timer(true);
	}
	public void destroy(){
		log.info("Destroy taskDeamon");
		timer.cancel();
	}
	

}
