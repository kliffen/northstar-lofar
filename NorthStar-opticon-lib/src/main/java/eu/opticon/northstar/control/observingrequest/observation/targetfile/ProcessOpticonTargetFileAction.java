// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest.observation.targetfile;

import java.util.Map;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import eu.opticon.northstar.business.OpticonConstants;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.control.proposal.observingrequest.observation.targetfile.ProcessTargetFileAction;
import eu.radionet.northstar.control.proposal.observingrequest.observation.targetfile.TargetFileForm;
import eu.radionet.northstar.data.entities.ResourceType;
import eu.radionet.northstar.data.entities.Target;

public class ProcessOpticonTargetFileAction extends ProcessTargetFileAction 
{
	protected Target fillTarget(TargetFileForm targetFileForm) throws DatabaseException 
	{
		OpticonTarget opticonTarget = new OpticonTarget();
		OpticonTargetFileForm opticonTargetFileForm = (OpticonTargetFileForm) targetFileForm;

		ResourceType resourceType = northStarDelegate.getResourceType(OpticonConstants.TIME, OpticonConstants.SECONDS);
		Map allocations = opticonTarget.getAllocations();
		Double totalDuration = AstronConverter.getDoubleSecondsFromHours(opticonTargetFileForm.getTargetExposureTime());
		NorthStarDelegate.setResourceValue(allocations, OpticonConstants.TOTAL_DURATION, totalDuration,	resourceType);
		opticonTarget.setMoon(opticonTargetFileForm.getSelectedTargetMoon());
		opticonTarget.setSeeing(opticonTargetFileForm.getSelectedTargetSeeing());
		opticonTarget.setSeeingLower(AstronConverter.toDouble(opticonTargetFileForm.getTargetSeeingLower()));
		opticonTarget.setSeeingUpper(AstronConverter.toDouble(opticonTargetFileForm.getTargetSeeingUpper()));
		opticonTarget.setWater(opticonTargetFileForm.getSelectedTargetWater());
		opticonTarget.setSn(AstronConverter.toDouble(opticonTargetFileForm.getSelectedTargetSn()));
		opticonTarget.setMagnitude(AstronConverter.toDouble(opticonTargetFileForm.getSelectedTargetMagnitude()));
		opticonTarget.setFlux(AstronConverter.toDouble(opticonTargetFileForm.getTargetFlux()));
		opticonTarget.setFluxFrequency(AstronConverter.toDouble(opticonTargetFileForm.getTargetFluxFrequency()));
		opticonTarget.setSpectralIndex(AstronConverter.toDouble(opticonTargetFileForm.getTargetSpectralIndex()));
		opticonTarget.setSkyQuality(opticonTargetFileForm.getSelectedTargetSkyQuality());
		opticonTarget.setComments(opticonTargetFileForm.getSelectedTargetComments());
		return opticonTarget;
		
	}

	
}
