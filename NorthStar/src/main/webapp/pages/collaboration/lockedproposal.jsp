<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processLockedProposal" method="POST">

<TABLE>
	<TR>
		<TD>&nbsp;<html:hidden name="lockProposalForm" property="forwardUri"/></TD>
	</TR>
	<TR>
		<TD class="right">Title:</TD>
		<TD class="bold" align="left"><bean:write name="lockProposalForm" property="title"/></TD>
	</TR>
	<TR>
		<TD class="right">Locked by:</TD>
		<TD class="bold" align="left"><bean:write name="lockProposalForm" property="lockOwner"/></TD>
	</TR>
	<TR>
		<TD class="right">Locked since:</TD>
		<TD class="bold" align="left"><bean:write name="lockProposalForm" property="since"/></TD>
	</TR>
	<logic:equal name="lockProposalForm" property="since" value="true">
	<TR>
		<TD class="right">Last action:</TD>
		<TD class="bold" align="left"><bean:write name="lockProposalForm" property="lastAction"/></TD>
	</TR>
	</logic:equal>
	<TR>
		<TD>&nbsp;</TD>
	</TR>		
	<TR>
		<TD colspan="2">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif" module=''/></TD>
				<TD valign='top'><PRE>
 Your Edit lock was overridden by another session/user, as listed above.

 Any changes to the proposal since your previous Save have been lost !

 Your Edit session on this proposal will now be terminated.

 It would be possible to regain control by pressing the Edit button for this proposal again,
 even though it may still be locked, but it may be more advisable to contact the other user first.</PRE>
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	</TABLE>
	
<table width="100%">
	<TR>
		<td align="center"><html:cancel styleClass="list_accept_yes" title="Ok">Ok</html:cancel></TD>
	</TR>
</TABLE>
</html:form>