// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.upload;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Arrays;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.OutputStream;
import java.io.FilenameFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.ServletContext;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import eu.radionet.northstar.business.NorthStarDelegate;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXParseException;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.CollaborationConfiguration;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.Telescope;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.business.util.XMLConverter;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.UserTelescope;

public class SetUpUploadConfigAction extends Action {
    private Log log = LogFactory.getLog(SetUpUploadConfigAction.class);
	private UploadConfigForm uploadConfigForm;
	private static final String UPLOADCONFIG = "uploadConfig";
	private static final String NORTHSTAR = "northstar";
	private static final String NORTHSTAR_CONFIG = "northstar-config";
	private static final String TELESCOPE_CONFIG_FILE = "telescope-configuration-file";
	private static final String NORTHSTAR_TELESCOPE_CONFIGS = "northstar-telescope-configurations";
	private static final String BACKUP_DIR = "backup";
	protected static final String OPTION_VALIDATION_FILE = "option-validation-file";
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ActionMessages errors = new ActionMessages();
		if (form != null)
		{
			uploadConfigForm = (UploadConfigForm) form;
			HttpSession session = request.getSession();
			// get access rights
			UserAccount ownUserAccount = (UserAccount) session.getAttribute(Constants.OWN_USERACCOUNT);
			List currentAccessRights = new ArrayList();
			if(!NorthStarConfiguration.getRestrictUserTelescope()){
				currentAccessRights = getAllTelescopes();
			} else {
				currentAccessRights = getCurrentAccessRights(ownUserAccount);
			}
			if (currentAccessRights.isEmpty()){
				errors.add(UPLOADCONFIG, new ActionMessage("error.user.access.rights"));
			}
			// set telescope list
			if (setTelescopes(currentAccessRights)){
				// check if a file is uploaded
				errors = processUploads(session.getServletContext());
				
				// reload the configuration if a file has been uploaded
				if (errors.isEmpty()){
					if (uploadConfigForm.configFile != null || 
							uploadConfigForm.optionsFile != null){
						try{
							reloadConfig(request);
						}
						catch(Exception e){
							errors.add(UPLOADCONFIG, new ActionMessage(
							"error.file.save.config"));
						}
					}
				}
				// find the telescope config file and date
				String telescopeConfigUri = setConfigFile(session.getServletContext());
				if (telescopeConfigUri != null){
					// find the options file and date
					setOptionsFile(session.getServletContext(),telescopeConfigUri);
					// set attributes for download function
				}
				session.setAttribute("OptionFileUri", uploadConfigForm.getCurrentOptionsFileUrl());
				session.setAttribute("ConfigFileUri", uploadConfigForm.getCurrentConfigFileUrl());
				session.setAttribute("previousOptionsFile", uploadConfigForm.getPreviousOptionsFileLocation());
				session.setAttribute("previousConfigFile", uploadConfigForm.getPreviousConfigFileLocation());
			}
		}
		if (errors.size() > 0) {
			saveErrors(request, errors);
			return mapping.getInputForward();
		}
		return mapping.findForward(Constants.SUCCESS);
	}
	
	private List getCurrentAccessRights(UserAccount userAccount) throws Exception{
		List accessRights = new ArrayList();
		NorthStarDelegate northstarDelegate = null;
		try {
            northstarDelegate = NorthStarDelegate.getInstance();
        } catch (ConnectionException ce) {
            log.fatal("Cannot instantiate AdminDelegate: " + ce.getMessage());
        }
		
		try{
			accessRights = northstarDelegate.getTelescopeAccessRights(userAccount.getId());
		} catch (DatabaseException de) {
            log.error("Cannot retrieve user accounts: " + de.getMessage());
        }
        return accessRights;
	}
	
	private boolean setTelescopes(List currentAccessRights){
		if (currentAccessRights != null && !currentAccessRights.isEmpty())
		{
			// check if the user has access to all telescopes 
			Iterator myIterator = currentAccessRights.iterator();
			while (myIterator.hasNext()){
				UserTelescope myTelescope = (UserTelescope) myIterator.next();
				if (myTelescope.getTelescope().equalsIgnoreCase("all")){
					currentAccessRights = getAllTelescopes();
				}
			}
			if (currentAccessRights.size() > 1){
				uploadConfigForm.setUserTelescopes(currentAccessRights);
				uploadConfigForm.setHasMultipleAccess(true);
			}
			// check if a telescope is selected, else pre select one
			if(uploadConfigForm.getSelectedTelescope() == null || 
					uploadConfigForm.getSelectedTelescope().equalsIgnoreCase("")){
				UserTelescope myTelescope = (UserTelescope) currentAccessRights.get(0);
				uploadConfigForm.setSelectedTelescope(myTelescope.getTelescope());
			}
			return true;
		}
		return false;
	}
	
	private List getAllTelescopes(){
		List tempList = NorthStarConfiguration.getTelescopes();
		List accessList = new ArrayList();
		Iterator myIterator = tempList.iterator();
		while (myIterator.hasNext()){
			Telescope telescope = (Telescope) myIterator.next();
			String telescopeName = telescope.getName();
			if (!telescopeName.equalsIgnoreCase("all")){
				UserTelescope newTelescope = new UserTelescope();
				newTelescope.setTelescope(telescopeName);
				accessList.add(newTelescope);
			}
		}
		return accessList;
	}
	
	private ActionMessages processUploads(ServletContext currentContext) throws Exception{
		ActionMessages errors = new ActionMessages();
		String fs = System.getProperty("file.separator");
		// config file is uploaded
		if (uploadConfigForm.configFile != null && uploadConfigForm.configFile.getFileSize() > 8) {
			String newConfigFile = new String();
			try {
				newConfigFile = storeFile(uploadConfigForm.getConfigFile().getInputStream());
			}
			catch (IOException ie){
				errors.add(UPLOADCONFIG, new ActionMessage(
						"error.file.save.config"));
				return errors;
			}
			// validate
			if(newConfigFile == null){
				errors.add(UPLOADCONFIG, new ActionMessage(
				"error.file.save.config"));
				return errors;
			}
			try{
				String newOptionsUri = findOptionsUri(newConfigFile);
				if(newOptionsUri == null){
					errors.add(UPLOADCONFIG, new ActionMessage(
					"error.file.xml.validation"));
					return errors;
				}
				// get real paths
				newOptionsUri = currentContext.getRealPath(newOptionsUri);
				String northstarConfigUri = currentContext.getInitParameter(NORTHSTAR_CONFIG);
				
				// get backup dir (outside of context so it is not overwritten by a deploy)
				File contextDir = new File (currentContext.getInitParameter(NORTHSTAR_CONFIG));
				String contextPath = contextDir.getParent();
				contextDir = new File (contextPath+fs+BACKUP_DIR);
				if (!contextDir.exists()){
					contextDir.mkdir();
				}
				
				String telescopeConfigFileUri = findTelescopeConfigUri(northstarConfigUri,uploadConfigForm.getSelectedTelescope());
				telescopeConfigFileUri = currentContext.getRealPath(telescopeConfigFileUri);
				
				// check if options file declared in new config file exists
				File optionsFile = new File(newOptionsUri);
				if (optionsFile.exists()){
					// replace
					errors = replaceFile(telescopeConfigFileUri,newConfigFile,contextDir);
					
					uploadConfigForm.setConfigOk(true);
					uploadConfigForm.setNewConfigFileName(newConfigFile.substring(newConfigFile.lastIndexOf(fs)));
				}else{
					errors.add(UPLOADCONFIG, new ActionMessage(
					"error.file.options.missing"));
				}

			}catch (SAXParseException se){
				errors.add(UPLOADCONFIG, new ActionMessage(
				"error.file.xml.validation"));
			}
		}
		// options file is uploaded
		if (uploadConfigForm.optionsFile != null && uploadConfigForm.optionsFile.getFileSize() > 8) {
			String newOptionsFile = storeFile(uploadConfigForm.getOptionsFile().getInputStream());
			// validate
			if(newOptionsFile == null){
				errors.add(UPLOADCONFIG, new ActionMessage(
				"error.file.save.config"));
				return errors;
			}
			try{
				if (checkOptionsFile(newOptionsFile)){
					String currentOptionsFileUri = currentContext.getRealPath(uploadConfigForm.getCurrentOptionsFileUrl());
					// replace
					if(currentOptionsFileUri != null){
						// get backup dir (outside of context so it is not overwritten by a deploy)
						File contextDir = new File (currentContext.getInitParameter(NORTHSTAR_CONFIG));
						String contextPath = contextDir.getParent();
						contextDir = new File (contextPath+fs+BACKUP_DIR);
						if (!contextDir.exists()){
							contextDir.mkdir();
						}
						
						errors = replaceFile(currentOptionsFileUri,newOptionsFile,contextDir);
						uploadConfigForm.setOptionsOk(true);
						uploadConfigForm.setNewOptionsFileName(newOptionsFile.substring(newOptionsFile.lastIndexOf(fs)));
					}
					else{
						errors.add(UPLOADCONFIG, new ActionMessage(
						"error.file.options.missing"));
					}
				}
			}catch (SAXParseException se){
				errors.add(UPLOADCONFIG, new ActionMessage(
				"error.file.xml.validation"));
			}
		}
		return errors;
	}
	
	private ActionMessages replaceFile(String currentFileName, String newFileName, File backupDir){
		ActionMessages errors = new ActionMessages();
		
		File temFile = new File(newFileName);
		File curFile = new File(currentFileName);
		if (curFile.exists() && temFile.exists()){
			File backFile = new File(backupDir,curFile.getName()+"."+curFile.lastModified());
			
			try{
				InputStream in = new FileInputStream(temFile);
		        OutputStream out = new FileOutputStream(backFile);
		    
		        // copy new file to backup dir (old file is already there)
		        byte[] buf = new byte[1024];
		        int len;
		        while ((len = in.read(buf)) > 0) {
		            out.write(buf, 0, len);
		        }
		        in.close();
		        out.close();
			}catch(IOException e){
				errors.add(UPLOADCONFIG, new ActionMessage(
				"error.file.options.missing"));
				return errors; 
			}

			curFile.delete();
			temFile.renameTo(curFile);
		}else{
			errors.add(UPLOADCONFIG, new ActionMessage(
			"error.file.options.missing"));
		}

		return errors;
	}
	
	private String setConfigFile(ServletContext currentContext) throws Exception{
		String northstarConfigUri = currentContext.getInitParameter(NORTHSTAR_CONFIG);
		
		String telescopeConfigFileUri = findTelescopeConfigUri(northstarConfigUri,uploadConfigForm.getSelectedTelescope());
		String fs = System.getProperty("file.separator");
		
		if(telescopeConfigFileUri != null){
			uploadConfigForm.setCurrentConfigFileUrl(telescopeConfigFileUri);
			
			telescopeConfigFileUri = currentContext.getRealPath(telescopeConfigFileUri);
			
			String telescopeConfigFileDate = findFileDate(telescopeConfigFileUri);
			uploadConfigForm.setCurrentConfigFileDate(telescopeConfigFileDate);
			
			File contextDir = new File (currentContext.getInitParameter(NORTHSTAR_CONFIG));
			String backupdir = contextDir.getParent()+fs+BACKUP_DIR;
			
			int filestart = telescopeConfigFileUri.lastIndexOf(fs)+1;
			int filend = telescopeConfigFileUri.length();
			File previousConfigFile = findPreviousFile(backupdir,
					telescopeConfigFileUri.substring(filestart, filend),telescopeConfigFileUri );
			
			if (previousConfigFile != null){
				Date lDate = new Date(previousConfigFile.lastModified());
				SimpleDateFormat lFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
		
				uploadConfigForm.setPreviousConfigFileDate(lFormat.format(lDate));
				uploadConfigForm.setPreviousConfigFileLocation(previousConfigFile.getPath());
			}
			return telescopeConfigFileUri;
		}else{
			return null;
		}
		
	}
	
	private void setOptionsFile(ServletContext currentContext, String telescopeConfigFileUri) throws Exception{
		String fs = System.getProperty("file.separator");
		String telescopeOptionsUri = findOptionsUri(telescopeConfigFileUri);
		if (telescopeOptionsUri != null){ 
			uploadConfigForm.setCurrentOptionsFileUrl(telescopeOptionsUri);
			
			telescopeOptionsUri = currentContext.getRealPath(telescopeOptionsUri);
			
			String optionsFileDate = findFileDate(telescopeOptionsUri);
			
			uploadConfigForm.setCurrentOptionsFileDate(optionsFileDate);
			
			File contextDir = new File (currentContext.getInitParameter(NORTHSTAR_CONFIG));
			String backupdir = contextDir.getParent()+fs+BACKUP_DIR;
			
			int filestart = telescopeOptionsUri.lastIndexOf(fs)+1;
			int filend = telescopeOptionsUri.length();
			File previousOptionsFile = findPreviousFile(backupdir,
					telescopeOptionsUri.substring(filestart, filend),telescopeOptionsUri);
			
			if (previousOptionsFile != null){
				Date lDate = new Date(previousOptionsFile.lastModified());
				SimpleDateFormat lFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
		
				uploadConfigForm.setPreviousOptionsFileDate(lFormat.format(lDate));
				uploadConfigForm.setPreviousOptionsFileLocation(previousOptionsFile.getPath());
				
			}
		}
	}
	
	private String findTelescopeConfigUri(String northstarConfigUri, String selectedTelescope) throws Exception{
		// read configfile into string
		String northstarConfigFile = NorthStarUtils.read(northstarConfigUri);
		XMLConverter xmlConverter = XMLConverter.getInstance();
		Document northstarConfigDocument = xmlConverter.convertStringToDocument(northstarConfigFile);
		NodeList elements = northstarConfigDocument.getElementsByTagName(NORTHSTAR);

		String telescopeName = null;
		String configFileUri = null;
		
		if (!northstarConfigDocument.getDocumentElement().getLocalName()
				.equalsIgnoreCase(NORTHSTAR)){
			log.warn("cannot find northstar element in "+northstarConfigUri);
			return null;
		}
		
		// scan trough xml file for location of telescope dependant config file
		for (int i = 0; i < elements.getLength(); i++) {
			Node node = (Node) elements.item(i);
			for (int j = 0; j < node.getChildNodes().getLength(); j++) {
				Node nodeChild = node.getChildNodes().item(j);

				// telescopes section found, iterate trough children for correct telescope
				if (nodeChild.getNodeName().equals("telescopes")) {
					
					for (int k = 0; k < nodeChild.getChildNodes().getLength();k++){
						Node telescopeNode = (Node) nodeChild.getChildNodes().item(k);
						for (int l = 0; l < telescopeNode.getChildNodes().getLength();l++){
							Node telescopeChild = (Node) telescopeNode.getChildNodes().item(l);
							String ccnode = telescopeChild.getNodeName();
							
							if (ccnode.equalsIgnoreCase("name")){
								telescopeName = telescopeChild.getFirstChild().getNodeValue();
							}
							if(ccnode.equalsIgnoreCase(TELESCOPE_CONFIG_FILE) ){
								configFileUri = telescopeChild.getFirstChild().getNodeValue();
							}
						}
						//
						if (telescopeName != null && telescopeName.equalsIgnoreCase(selectedTelescope)){
							//uploadConfigForm.setCurrentConfigFileUrl(configFileUri);
							return configFileUri;
						}
						
					}
				}
			}
		}
		log.warn("cannot find telescope-configuration-file in "+northstarConfigUri);
		return null;
	}
		
	private String findOptionsUri(String telescopeConfigFileUri)throws Exception{
		String telescopeFile = NorthStarUtils.read(telescopeConfigFileUri);
		String optionUri = null;
		XMLConverter xmlConverter = XMLConverter.getInstance();

			Document optionsDocument = xmlConverter.convertStringToDocument(telescopeFile);
			
			//String localName = optionsDocument.getDocumentElement().getLocalName();
			if (!optionsDocument.getDocumentElement().getLocalName()
					.equalsIgnoreCase(NORTHSTAR_TELESCOPE_CONFIGS)){
				log.warn("cannot find "+NORTHSTAR_TELESCOPE_CONFIGS+" in "+telescopeConfigFileUri);
				return null;
			}
			NodeList elements = optionsDocument.getElementsByTagName(NORTHSTAR_TELESCOPE_CONFIGS);
			
			for (int i = 0; i < elements.getLength(); i++) {
				Node node = (Node) elements.item(i);
				
				for (int j = 0; j < node.getChildNodes().getLength(); j++) {
					Node nodeChild = node.getChildNodes().item(j);
	
					// telescopes section found, iterate trough children for correct telescope
					if (nodeChild.getNodeName().equals("northstar-telescope-configuration")) {
						for (int k = 0; k < nodeChild.getChildNodes().getLength();k++){
							Node telescopeNode = (Node) nodeChild.getChildNodes().item(k);
							
							String ccnode = telescopeNode.getNodeName();
							if (ccnode.equalsIgnoreCase(OPTION_VALIDATION_FILE)){
								optionUri = telescopeNode.getFirstChild().getNodeValue();
							}
							
						}
					}
				}
			}
			if (optionUri==null){
				log.warn("cannot find option-validation-file");
			}
		return optionUri;
	}
	
	private String findFileDate(String FileUri){
		File lFile = new File(FileUri);
		Date lDate = new Date(lFile.lastModified());
		SimpleDateFormat lFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
		
		return lFormat.format(lDate);
	}
	
	private String storeFile(InputStream inputStream) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int byteChar = inputStream.read();
		
		if (inputStream!=null)
		{
			while (byteChar != -1) {
				out.write(byteChar);
				byteChar = inputStream.read();
			}
		}
		
		if (out.size() > 8){
			File OutFile = File.createTempFile(System.currentTimeMillis()+"_", ".xml");
			FileOutputStream lFileStream = new FileOutputStream(
					OutFile);

			lFileStream.write(out.toByteArray());
			lFileStream.flush();
			lFileStream.close();
			
			return OutFile.getAbsolutePath();
		}
		else{
			return null;
		}
	}
	
	private boolean checkOptionsFile(String lFilePath) throws Exception{
		String telescopeFile = NorthStarUtils.read(lFilePath);
		XMLConverter xmlConverter = XMLConverter.getInstance();
		Document optionsDocument = xmlConverter.convertStringToDocument(telescopeFile);
		// String localName = optionsDocument.getDocumentElement().getLocalName();
		if (!optionsDocument.getDocumentElement().getLocalName()
				.equalsIgnoreCase("contexts")){
			log.warn("cannot find contexts in "+lFilePath);
			return false;
		}
		
		Node node = (Node) optionsDocument.getDocumentElement();
		for (int j = 0; j < node.getChildNodes().getLength(); j++) {
			Node nodeChild = node.getChildNodes().item(j);

			for (int k = 0; k < nodeChild.getChildNodes().getLength();k++){
				Node telescopeNode = (Node) nodeChild.getChildNodes().item(k);
				for (int l = 0; l < telescopeNode.getChildNodes().getLength();l++){
					Node telescopeChild = (Node) telescopeNode.getChildNodes().item(l);
					String ccnode = telescopeChild.getNodeName();
					if (ccnode.equalsIgnoreCase("options")){
						//<options name="telescopeConfigurations">
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private void reloadConfig(HttpServletRequest request)throws Exception{
		HttpSession session = request.getSession();
		NorthStarUtils.loadNorthStarConfiguration(session.getServletContext());
		CollaborationConfiguration.setAcceptInvitation(NorthStarUtils
				.getLinkPath(request));
		log.info("NorthStar configuration loaded");
	}
	
	private File findPreviousFile(String path, final String filterName, String currentFile){
		// create a filter that only the files of the selected telescope are used.
		FilenameFilter filter=new FilenameFilter(){
			public boolean accept(File dir, String fileName) {
			return fileName.startsWith(filterName);
			}
		};
		File f = new File(path);
		     
		File[] files = f.listFiles(filter);
		if (files == null || files.length == 0 ){
			return null;
		}
		
		// sort on date 
	 	Arrays.sort( files, new Comparator(){
	    	public int compare(Object o1, Object o2) {
	    	
	   		if (((File)o1).lastModified() > ((File)o2).lastModified()) {
	   			return -1;
	   		} else if (((File)o1).lastModified() < ((File)o2).lastModified()) {
	   			return +1;
	   		} 
	   		return 0;
	   		
	   	}
	    });
	 
	 // check if the latest file is of the same time and date as the current file	
	 // if true, return the semi last file
	 if (findFileDate(files[0].getPath()).equalsIgnoreCase(findFileDate(currentFile ))){
		 if(files.length>1 ){
			 return files[1];
		 }
	 }
	 // return the latest file.
	 return files[0];
	 
	}
	
}

