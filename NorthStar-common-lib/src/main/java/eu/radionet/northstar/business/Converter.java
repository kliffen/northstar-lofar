// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Converter.java 
 *
 * Created on Feb 23, 2005
 *
 * Version $Id: Converter.java,v 1.1 2006-05-02 10:01:13 verhoef Exp $
 *
 */
package eu.radionet.northstar.business;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

import nl.astron.util.AstronConverter;

/**
 * The Converter provides
 * 
 * @author Bastiaan Verhoef
 * 
 */
public class Converter extends AstronConverter implements Serializable {
    public static final String DATETIME_FORMAT = "yyyy/MM/dd HH:mm z";   
    public static final String TIME_FORMAT = "HH:mm";  
    public static final String SHORT_TIME_FORMAT = "HH";
    public static final String DATE_FORMAT = "yyyy/MM";   
    protected static Converter converter = null;


    public static String toUTCDateString(Date date) {
        if (date == null) {
            return null;
        } else {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                    DATETIME_FORMAT);
            Calendar UTCCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
            UTCCal.setTime(date);
            simpleDateFormat.setCalendar(UTCCal);
            return simpleDateFormat.format(UTCCal.getTime());
        }
    }



    public static String toTimeString(Date date) {
        return toDateString(date, TIME_FORMAT);

    }

    public static Date toTime(String string) {
        Date result = toDate(string, TIME_FORMAT);
        if (result == null) {
            result = toDate(string, SHORT_TIME_FORMAT);
        }
        return result;
    }

    public static String removeReturnsAndTabs(String string) {
        if (string != null) {
            return string.replaceAll("[\\n\\t\\r]", "");
        }
        return string;
    }
    public static String getLastName(String name){
        if (name == null){
            return null;
        }
        String[] nameParts = name.split(" ");
        return nameParts[nameParts.length - 1];
    }
    public static String toString(String prefix, Integer integer, String suffix) {
        if (integer == null) {
            return null;
        } else {
            String result = "";
            if (prefix != null){
                result += prefix;
            }
            result += integer.toString();
            if (suffix != null){
                result += suffix;
            }
            return result;
        }
    }

    public static String toString(String prefix,Double doubleValue, String suffix) {
        if (doubleValue == null) {
            return null;
        } else {
            String result = "";
            if (prefix != null){
                result += prefix;
            }
            result += doubleValue.toString();
            if (suffix != null){
                result += suffix;
            }
            return result;
        }
    }

    public static String toString(String prefix, Float floatValue, String suffix) {
        if (floatValue == null) {
            return null;
        } else {
            String result = "";
            if (prefix != null){
                result += prefix;
            }
            result += floatValue.toString();
            if (suffix != null){
                result += suffix;
            }
            return result;
        }
    }
}