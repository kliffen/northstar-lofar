// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessUploadAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: ProcessSubmitAction.java,v 1.2 2008-04-08 08:02:15 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.submit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.exception.DeadLinePassedException;
import eu.radionet.northstar.business.exception.DeprecatedException;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.Semester;

/**
 * The ProcessUploadAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessSubmitAction extends LockedAction {
	private Log log = LogFactory.getLog(this.getClass());

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		ActionMessages errors = new ActionMessages();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		/*
		 * if not pressed on cancel, it must be an submit
		 */
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		if (!isCancelled(request)) {

			try {
				proposalDelegate.submitAndStore(proposalData.getProposal(),
						ownUserAccount);
			} catch (DatabaseException de) {
				log.error("Cannot submit and store proposal: "
						+ de.getMessage());
				errors.add(Constants.ERROR, new ActionMessage(
						"error.database.submit.proposal"));
				saveErrors(request, errors);
				return mapping.getInputForward();
			} catch (DeadLinePassedException de) {
				Semester semester = proposalData.getProposal().getSemester();
				log.error("Cannot submit proposal, because deadline of ("
						+ semester.getTelescope() + ","
						+ semester.getSemester() + ", "
						+ semester.getCategory().getCode() + ","
						+ semester.getCommunity() + ")");
				/*
				 * set ignore deadline warning for submitting.
				 */
				proposalData.setIgnoreDeadLineWarning(false);
				return mapping.findForward(Constants.REFRESH);
			} catch (DeprecatedException de) {
				Semester semester = proposalData.getProposal().getSemester();
				log.error("Cannot submit proposal, because proposal version is deprecated of ("
						+ semester.getTelescope() + ","
						+ semester.getSemester() + ", "
						+ semester.getCategory().getCode() + ","
						+ semester.getCommunity() + ")");
				return mapping.findForward("deprecated");
			}
			return mapping.findForward(Constants.SUCCESS);
		} else {

			ActionForward forward = new ActionForward();
			if (!AstronValidator.isBlankOrNull(proposalData
					.getForwardUriAfterSubmit())) {
				try {

					proposalDelegate.store(proposalData.getProposal(),
							ownUserAccount, request.getSession().getId());
				} catch (DatabaseException de) {
					log.error(LogMessage.getMessage(ownUserAccount,
							"Cannot store proposal: " + de.getMessage(), de));
					errors.add(Constants.ERROR, new ActionMessage(
							"error.database.store.proposal"));
					saveErrors(request, errors);
					return mapping.findForward(Constants.REFRESH);
				}
				forward.setModule("");
				forward.setRedirect(true);
				forward.setPath(proposalData.getForwardUriAfterSubmit());
			} else {
				removeLock(request, ownUserAccount);
				forward = mapping.findForward(Constants.CANCEL);
			}

			return forward;
		}

	}
}