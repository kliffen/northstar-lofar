// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business;

import nl.astron.useradministration.data.entities.UserAccount;

public class LogMessage {
    public static String getMessage(UserAccount userAccount, String message,
            Throwable throwable) {
        if (userAccount != null) {

            return userAccount.getUsername() + " - " + message
                    + "\n" + stackTraceToString(throwable);
        } else {
            return "?" + " - " + message + "\n" + stackTraceToString(throwable);
        }
    }

    public static String getMessage(UserAccount userAccount, String message) {
        if (userAccount != null) {
            return userAccount.getUsername() + " - " + message
                    + " ";
        } else {
            return "?" + " - " + message + " ";
        }
    }
    /**
     * Convert a stack trace to a string
     * 
     * @param e
     * @return String
     */
    public static String stackTraceToString(Throwable e) {
        String result = "StackTrace:\n";
        result += e.getClass().toString() + "\n";
        for (int i = 0; i < e.getStackTrace().length; i++) {
            result += "\t" + (e.getStackTrace())[i].toString() + "\n";
        }
        return result;
    }
    public static String getMessage(String message,
            Throwable throwable) {
            return message + "\n" + stackTraceToString(throwable);

    }
}
