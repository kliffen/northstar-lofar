// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SetUpApplicantAction.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: SetUpApplicantAction.java,v 1.3 2008-04-08 08:02:59 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.applicants.addedit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The SetUpApplicantAction provides
 * 
 * @author Bastiaan Verhoef
 *  
 */
public class SetUpApplicantAction extends LockedAction {
	/*  the comparator has moved to the applicantform, also the filling of the county list.
	 // inner class for comparing countries
    private class LocaleComparator implements Comparator{
    	public int compare(Object emp1, Object emp2){
	    	//parameter are of type Object, so we have to downcast it 
	    	String emp1Name = ( (Locale) emp1 ).getDisplayCountry();
	    	String emp2Name = ( (Locale) emp2 ).getDisplayCountry();
	    	//uses compareTo method of String class to compare names 
	    	return emp1Name.compareTo(emp2Name);
    	}
    }
	*/
    public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        ApplicantForm applicantForm = (ApplicantForm) form;  		
        HttpSession session = request.getSession();
        ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
        TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
		
        String storedCountry = null;
        if (!AstronValidator.isBlankOrNull(applicantForm.getMemberId())) {
         	Proposal proposal = proposalData.getProposal();
            Member member = (Member) proposal.getMembers().get(AstronConverter.toInteger(applicantForm.getMemberId()).intValue());
            applicantForm.setName(member.getName());
            applicantForm.setEmail(member.getEmail());
            storedCountry = member.getCountry();
            
            applicantForm.setAffiliation(member.getAffiliation());
        }
		applicantForm.setInvite("yes");
		
		String[] preferredCountries = null;
		if(telescopeConfiguration.getPreferredCountries() != null){
			preferredCountries = telescopeConfiguration.getPreferredCountries().split(",");
		}
		
		Locale[] localit = Locale.getAvailableLocales();
	    //List countries = applicantForm.getCountries();
		Set prefCountries = new HashSet();
		for(int i=0; i < localit.length; i++){
        	Locale locale = localit[i]; 
       		final String contry = locale.getDisplayCountry();
            if (contry.length() > 0) {
        	   //	countries.add(locale);
	           	if(storedCountry != null && contry.equalsIgnoreCase(storedCountry)){
	           		applicantForm.setSelectedCountry(locale.getISO3Country());
	           	}
	           	try{
		           	final String abrev = locale.getISO3Country();
		           	
		           	if(preferredCountries != null){
			           	for(int j=0;j<preferredCountries.length;j++){
			           		if(abrev.equalsIgnoreCase(preferredCountries[j].trim()) ){
				           		prefCountries.add(locale);
				           	}
			           	}
		           	}
	           	}catch(MissingResourceException e){
	           		// do not add country...
	           	}
           }
        }
		// remove duplicates for preferred countries
		Set set = new HashSet();
   	 	for (Iterator iter = prefCountries.iterator(); iter.hasNext(); ) {
   	 		Locale locas = (Locale) iter.next();
   	 		if (set.add(locas.getDisplayCountry())){
   	 			try{
		           	final String abrev = locas.getISO3Country();
	   	 			applicantForm.getPreferredCountries().add(locas);
	   	 		}catch(MissingResourceException e){
	           		// do not add country...
	           	}
   	 		}
   	 	}
				
		
		// try to get current country from browser.
        // if(applicantForm.getSelectedCountry() == null){
        //	 applicantForm.setSelectedCountry(request.getLocale().getISO3Country());
        // }     
        
        return mapping.findForward(Constants.SUCCESS);

    }
}