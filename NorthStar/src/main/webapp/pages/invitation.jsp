<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processAcceptInvitation" method="POST">
  <TABLE>
	
	<TR>
		<TD>
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				<TD valign='top'><pre><bean:write name="invitationForm" property="acceptMessage"/>
					 </pre>
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>

	</TABLE>
  <TABLE width="100%">
	<TR>
		<logic:equal name="invitationForm" property="keyIsAccepted" value="true">
			<TD align="left"><html:submit property="okButton" styleClass="list_accept_yes" title="accept invitation">Ok</html:submit></TD>
		</logic:equal>
		<logic:equal name="invitationForm" property="keyIsAccepted" value="false">
			<TD class="left"><html:cancel styleClass="list_decline_t" title="Cancel submit">Cancel</html:cancel></TD>
		</logic:equal>
		<td> &nbsp; </td>
	</TR>
  </TABLE>
</html:form>