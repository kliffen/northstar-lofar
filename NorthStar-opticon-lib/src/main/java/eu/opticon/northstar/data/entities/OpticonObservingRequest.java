// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SkeletonObservingRequest.java 
 *
 * Created on Feb 14, 2005
 *
 * Version $Id: OpticonObservingRequest.java,v 1.12 2008-08-15 10:06:19 boelen Exp $
 *
 */
package eu.opticon.northstar.data.entities;

import eu.radionet.northstar.data.entities.ObservingRequest;

public class OpticonObservingRequest extends ObservingRequest 
{

    private String category = null;
    private String observingMode = null;
    private boolean nightTime = false;
    private Double noiseLevel = null;
    private boolean longTermProposal = false;
    private boolean largeProposal = false;
    protected String longTermProposalSpecifics = null;
    protected String allocationJustification = null;
    protected String grantNumber = null;
    protected String travel = null;
    protected String otherExpenditure = null;
    private boolean sponsoring = false;
    private String sponsorDetails = null;
    private boolean opticonFunding = false;
    private boolean ProprietaryException = false;
    private String ProprietaryExceptionSpecifics = null;
    protected boolean usefulTime = false;
    private String backupStrategy = null;
    private boolean dataReduction = false;
    private String dataReductionSpecifics = null;
    
    
    public final boolean isDataReduction() {
		return dataReduction;
	}

    
	public final boolean isNightTime() {
		return nightTime;
	}

	public final void setNightTime(boolean nightTime) {
		this.nightTime = nightTime;
	}

	public final Double getNoiseLevel() {
		return noiseLevel;
	}

	public final void setNoiseLevel(Double noiseLevel) {
		this.noiseLevel = noiseLevel;
	}

	public String getGrantNumber()
	{
		return grantNumber;
	}

	public void setGrantNumber(String grantNumber)
	{
		this.grantNumber = grantNumber;
	}

	public String getOtherExpenditure()
	{
		return otherExpenditure;
	}

	public void setOtherExpenditure(String otherExpenditure)
	{
		this.otherExpenditure = otherExpenditure;
	}

	public String getTravel()
	{
		return travel;
	}

	public void setTravel(String travel)
	{
		this.travel = travel;
	}
	
	/**
     * @return Returns the category.
     */
    public String getCategory() {
        return category;
    }

    /**
     * @param category The category to set.
     */
    public void setCategory(String category) {
        this.category = category;
    }

    /**
     * @return Returns the longTermProposal.
     */
    public boolean isLongTermProposal() {
        return longTermProposal;
    }

    /**
     * @param longTermProposal The longTermProposal to set.
     */
    public void setLongTermProposal(boolean longTermProposal) {
        this.longTermProposal = longTermProposal;
    }


	public String getLongTermProposalSpecifics()
	{
		return longTermProposalSpecifics;
	}

	public void setLongTermProposalSpecifics(String longTermProposalSpecifics)
	{
		this.longTermProposalSpecifics = longTermProposalSpecifics;
	}

	public boolean isLargeProposal()
	{
		return largeProposal;
	}

	public void setLargeProposal(boolean largeProposal)
	{
		this.largeProposal = largeProposal;
	}

	public String getAllocationJustification()
	{
		return allocationJustification;
	}

	public void setAllocationJustification(String allocationJustification)
	{
		this.allocationJustification = allocationJustification;
	}

	public String getObservingMode()
	{
		return observingMode;
	}

	public void setObservingMode(String observingMode)
	{
		this.observingMode = observingMode;
	}

	public boolean isOpticonFunding()
	{
		return opticonFunding;
	}

	public void setOpticonFunding(boolean opticonFunding)
	{
		this.opticonFunding = opticonFunding;
	}

	public boolean isSponsoring() {
		return sponsoring;
	}

	public void setSponsoring(boolean sponsoring) {
		this.sponsoring = sponsoring;
	}

	public String getSponsorDetails() {
		return sponsorDetails;
	}

	public void setSponsorDetails(String sponsorDetails) {
		this.sponsorDetails = sponsorDetails;
	}

	public boolean isProprietaryException() {
		return ProprietaryException;
	}

	public void setProprietaryException(boolean proprietaryException) {
		ProprietaryException = proprietaryException;
	}

	public String getProprietaryExceptionSpecifics() {
		return ProprietaryExceptionSpecifics;
	}

	public void setProprietaryExceptionSpecifics(
			String proprietaryExceptionSpecifics) {
		ProprietaryExceptionSpecifics = proprietaryExceptionSpecifics;
	}


	public String getBackupStrategy() {
		return backupStrategy;
	}


	public void setBackupStrategy(String backupStrategy) {
		this.backupStrategy = backupStrategy;
	}


	public String getDataReductionSpecifics() {
		return dataReductionSpecifics;
	}


	public void setDataReductionSpecifics(String dataReductionSpecifics) {
		this.dataReductionSpecifics = dataReductionSpecifics;
	}


	public void setDataReduction(boolean dataReduction) {
		this.dataReduction = dataReduction;
	}


	public boolean isUsefulTime() {
		return usefulTime;
	}


	public void setUsefulTime(boolean usefulTime) {
		this.usefulTime = usefulTime;
	}

}
