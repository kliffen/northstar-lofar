<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui-1.10.4.min.js"></script>
<link rel="stylesheet"  href="<%=request.getContextPath()%>/css/jquery-ui-1.10.4.min.css">
<tiles:get name="errors" />

<script type="text/javascript" language="JavaScript">
	setInterval ("CountChars(document.forms[0].title,document.forms[0].titleCount,document.forms[0].maxTitleCount.value)",1000 );
	setInterval ("CountWords(document.forms[0].abstractText,document.forms[0].abstractCount)" ,1001 );
</script>

 <script>
  $(function() {
    $( document ).tooltip();
  });
  </script>
	  


<html:form action="/processJustification" enctype="multipart/form-data" focus="title" method="POST">

<table width="100%">
	<TR>
		<td width="*">&nbsp;</TD>
		<TD class="help" width="60">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="60" height="25">
							<TR>
					  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#justification\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
				  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#justification\" target=\"help\">Help</a>"%></TD>
			  				</TR>
			  			</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />		
			</TD>
	</TR>
	</table><table align="center" width="100%">
	<html:hidden property="maxTitleCount"/>
	<tr>
		<td class="right"><astron:label key="label.justification.title" /><!-- Title&nbsp;(Max characters: --><bean:write name="justificationForm" property="maxTitleCount"/>)&nbsp;:&nbsp;</td>
		<td class="left"><html:text property="title" size="62" maxlength="255" onkeypress="" onchange="" onkeyup="CountChars(document.forms[0].title,document.forms[0].titleCount,document.forms[0].maxTitleCount.value)" onmouseup=""/>
		(Characters entered: <html:text property="titleCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="title" ifnotexist="*"/></span></td>
	</tr>
	
	<tr>
		<td class="right" valign="top"><astron:label key="label.justification.abstract" /><!--Abstract&nbsp;(Max words: --><bean:write name="justificationForm" property="maxAbstractCount"/>)&nbsp;:&nbsp;</td>
		<td class="left"><html:textarea property="abstractText" cols="92" rows="8" onkeypress="" onchange="" onkeydown="" onkeyup="CountWordsRed(document.forms[0].abstractText,document.forms[0].abstractCount,180)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
		(Words entered: <html:text property="abstractCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="abstractText" ifnotexist="*"/></span></td>
	</tr>

	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<TR>
		<td colspan="2"> 
			<jsp:include page="layout/box_header_noFullWidth.jsp" />
						<TABLE width="220" height="25">
							<TR>
					  			<TD width='20' height="25"><%= "<a title=\"Opens the instructions for preparation of justification files\" href=\"http://old.astron.nl/radio-observatory/observing-lofar/observing-proposals/justification-file-instructions/instructions-j\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
				  				<TD width="200" height="25"><%= "<a title=\"Opens the instructions for preparation of justification files\" href=\"http://old.astron.nl/radio-observatory/observing-lofar/observing-proposals/justification-file-instructions/instructions-j\" target=\"help\">Justification File(s):<br>Instructions for preparation</a>"%></TD>
			  				</TR>
			  			</TABLE>
					<jsp:include page="layout/box_footer_noFullWidth.jsp" />		
			</TD>
	</TR>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<logic:equal name="justificationForm" property="showLatex" value="true">
		<tr>
				<td class="right">LaTex class File :&nbsp;</td>
				<td>
					<html:link action="/processDownload.do?justificationFileType=latexClass" >
				 	<bean:write name="justificationForm" property="latexClassUrl" /> </html:link>
				</td>
		</tr>
			<tr>
				<td class="right">LaTex template File :&nbsp;</td>
				<td>
				
					<html:link action="/processDownload.do?justificationFileType=latexTemplate">
				 	<bean:write name="justificationForm" property="latexTemplateUrl" /> </html:link>
				</td>
			</tr>
		<tr>
				<td colspan="2">&nbsp;</td>
		</tr>
	
	</logic:equal>
	
	<logic:equal name="justificationForm" property="allowEnvelopeSheet" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.envelopesheet" /></td>
			<td class="left">
			<bean:define id="envelopeList" name="justificationForm" property="envelopeSheets"/> 
				
				<html:select name="justificationForm" property="selectedEnvelopeSheet" size="1" style="width:250px" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
					<html:options collection="envelopeList" property="id" labelProperty="title"/>		
				</html:select>
				
			</td>	
		</tr>	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</logic:equal>
	
	<logic:equal name="justificationForm" property="allowFirstJustification" value="true">
		<tr>
			<td class="right"><astron:label key="label.justification.firstfile" /><!--First Justification File &nbsp;:&nbsp;-->
			</td>
			<td class="left">
				<logic:notEmpty name="justificationForm" property="scientificFileName">
					<html:link action="/processDownload.do?justificationFileType=scientific">
						<bean:write name="justificationForm" property="scientificFileName"/> 
					</html:link> (<bean:write name="justificationForm" property="scientificFileSize"/>)
				</logic:notEmpty>
				<logic:empty name="justificationForm" property="scientificFileName">
					<astron:label key="label.justification.noneuploaded" /><!--None uploaded --> 
				</logic:empty>
				<span class="error">*</span>&nbsp;<html:submit property="uploadScientificFileButton" styleClass="list_upload" title="Upload Scientific Justification File">Upload</html:submit>
				</td>
		</tr>
		<logic:notEmpty name="justificationForm" property="scientificFileName">
		<tr>
			<td class="right"><astron:label key="label.justification.uploaded" /><!--Uploaded&nbsp;:&nbsp;--></td>
			<td class="left"><bean:write name="justificationForm" property="scientificFileDate"/></td>
		</tr>
		</logic:notEmpty>
	</logic:equal>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<logic:equal name="justificationForm" property="allowSecondJustification" value="true">
	<tr>
		<td class="right"><astron:label key="label.justification.secondfile" /><!--Second Justification File (optional)&nbsp;:&nbsp;--></td>
		<td class="left">
			<logic:notEmpty name="justificationForm" property="technicalDetailsFileName">
				<html:link action="/processDownload.do?justificationFileType=technicalDetails">
					<bean:write name="justificationForm" property="technicalDetailsFileName"/> 
				</html:link> (<bean:write name="justificationForm" property="technicalDetailsFileSize"/>)
			</logic:notEmpty>
			<logic:empty name="justificationForm" property="technicalDetailsFileName">
				<astron:label key="label.justification.noneuploaded" /><!--None uploaded -->
			</logic:empty>&nbsp;
			<logic:equal name="justificationForm" property="showSingleFileUpload" value="true">
				<html:submit property="uploadTechnicalDetailsFileButton" styleClass="list_upload" title="Upload Technical Justification File">Upload</html:submit>
				<logic:notEmpty name="justificationForm" property="technicalDetailsFileName">
					<html:submit property="deleteTechnicalDetailsFileButton" styleClass="list_delete_file" title="Delete technical details file">Delete</html:submit>
				</logic:notEmpty>
			</logic:equal>
			</td>
	</tr>
		<logic:notEmpty name="justificationForm" property="technicalDetailsFileName">
		<tr>
			<td class="right"><astron:label key="label.justification.uploaded" /><!--Uploaded&nbsp;:&nbsp;--></td>
			<td class="left"><bean:write name="justificationForm" property="technicalDetailsFileDate"/></td>
		</tr>
		</logic:notEmpty>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	</logic:equal>
	<logic:equal name="justificationForm" property="allowFiguresFile" value="true">
		<tr>
			<td class="right"><astron:label key="label.justification.figuresfile" /><!--Figure(s) File (optional)&nbsp;:&nbsp;--></td>
			<td class="left">
				<logic:notEmpty name="justificationForm" property="figureFileName">
				
					<html:link action="/processDownload.do?justificationFileType=figure">
						<bean:write name="justificationForm" property="figureFileName"/> 
					</html:link> (<bean:write name="justificationForm" property="figureFileSize"/>)
				</logic:notEmpty>
				<logic:empty name="justificationForm" property="figureFileName">
					<astron:label key="label.justification.noneuploaded" /><!--None uploaded -->
				</logic:empty>&nbsp; 
				<logic:equal name="justificationForm" property="showSingleFileUpload" value="true">
					<html:submit property="uploadFigureFileButton" styleClass="list_upload" title="Upload the specified figure file">Upload</html:submit>
					<logic:notEmpty name="justificationForm" property="figureFileName">
						<html:submit property="deleteFigureFileButton" styleClass="list_delete_file" title="Delete figure file">Delete</html:submit>
					</logic:notEmpty>
				</logic:equal>
			</td>
		</tr>
		<logic:notEmpty name="justificationForm" property="figureFileName">
		<tr>
			<td class="right"><astron:label key="label.justification.uploaded" /><!--Uploaded&nbsp;:&nbsp;--></td>
			<td class="left"><bean:write name="justificationForm" property="figureFileDate"/></td>
		</tr>
		</logic:notEmpty>
	</logic:equal>
	
	<!-- start of observation strategy -->
	<logic:equal name="justificationForm" property="displayObservationStrategy" value="true">
	<tr>
		<td colspan="2" class="tableheader">Observation strategy</td>
	</tr>
	<tr>
	<!-- This should be done with labels and the max word count should be set from the config file -->
	<td align="left" valign="top">
	<table><tr><td width="50%">&nbsp;</td><td width="500">
Observation strategy (Max words: 300) : &nbsp; <br>
Describe the observation strategy needed to complete the project. &nbsp; <br>
<b>--- Example 1 --- &nbsp; </b> <br>
10 min cal1 &nbsp; <br>
10 min cal2 &nbsp; <br>
4 hrs target: multi beam HBA-dual observation with 122 subband on each source &nbsp; <br>
15 min cal3 &nbsp; <br>
<b>---  Example 2 --- &nbsp; </b> <br>
Interleaved calibrator/target observations: &nbsp; <br>
20min on target, 3min on calibrator, repeat 12 times. &nbsp; <br>
<b>---  Example 3 --- &nbsp; </b> <br>
Two beamformed observations of target 1 in HBA-low mode &nbsp; <br>
duration of each observation: 30 min &nbsp; <br>
only 13 core stations, coherent stokes I, 3 simultaneous pointings with 4 TAB rings each &nbsp; <br>
spacing between consecutive observation: exactly 2.5 days &nbsp; <br>
</td></tr></table>
</td>
		<td class="left" width="200"><html:textarea property="observationStrategyText" cols="92" rows="15" title=
			"" onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].observationStrategyText,document.forms[0].observationStrategyTextCount)" 
			onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
			<div>Describe here the necessary observation steps to complete the project. (Words entered: <html:text property="observationStrategyTextCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="observationStrategyText" ifnotexist="*"/></span></div></td>
		</tr>
	</logic:equal>
		
	<!-- start of technical justification	 -->
	<logic:equal name="justificationForm" property="allowTechnicalJustification" value="true">
	<tr>
		<td colspan="2" class="tableheader"><astron:label key="label.justification.technical"/></td>
	</tr>
	
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<logic:equal name="justificationForm" property="displayNightTime" value="true">
	<tr>
			<td class="right"><astron:label key="label.justification.technical.nightime" /><!--Uploaded&nbsp;:&nbsp;--></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="nighttime" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="nighttime" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
	</tr>

			<logic:equal name="justificationForm" property="nighttime" value="true">
				<tr>
		<td class="right" valign="top">Elaborate. Explain why daytime observations are not suitable. (Max words:<bean:write name="justificationForm" property="maxTechCount"/>) : &nbsp;</td>
		<td class="left"><html:textarea property="nightTimeReason" cols="92" rows="6" title="Give a clear explanation why nighttime is essential for your observations." onkeypress="" onchange="" onkeydown="" onkeyup="CountWordsRed(document.forms[0].nightTimeReason,document.forms[0].nightTimeReasonCount,60)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
		(Words entered: <html:text property="nightTimeReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="nightTimeReason" ifnotexist="*"/></span></td>
			</tr>
			</logic:equal>
	</logic:equal>
	
	
	<logic:equal name="justificationForm" property="displayParallelObservatione" value="true">
					<!-- parallelObservation  -->		
	<tr>
			<td class="right"><astron:label key="label.justification.technical.parrel.observation" /><!--Uploaded&nbsp;:&nbsp;--></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="parallelObservation" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="parallelObservation" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
	</tr>

			<logic:equal name="justificationForm" property="parallelObservation" value="true">
				<tr>
					<td align="right" valign="top"><astron:label key="label.justification.technical.reason" /></td>
				   <td class="left"><html:textarea property="parallelObservationReason" cols="92" rows="6" title="Indicate which other telescopes/instruments are planned to run simultaneously, how this will be
coordinated, and how essential simultaneous observations are to the scientific scope of your project."  onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].parallelObservationReason,document.forms[0].parallelObservationReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="parallelObservationReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="parallelObservationReason" ifnotexist="*"/></span></td>
			   </tr>
			</logic:equal>	
	</logic:equal>	
	
	<logic:equal name="justificationForm" property="displayInternationalStation" value="true">
										<!-- International stations  -->		
	<tr>
			<td class="right"><astron:label key="label.justification.technical.international.station" /><!--Uploaded&nbsp;:&nbsp;--></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="internationalStation" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="internationalStation" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
	</tr>

			<logic:equal name="justificationForm" property="internationalStation" value="true">
			<tr>
			<td class="right"><astron:label key="label.justification.technical.international.station.essential" /></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="internationalStationEssential" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="internationalStationEssential" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>
			</tr>
			<logic:equal name="justificationForm" property="internationalStationEssential" value="true">
				<tr>
					<td align="right" valign="top"><astron:label key="label.justification.technical.reason" /></td>
				   <td class="left"><html:textarea property="internationalStationEssentialReason" cols="92" rows="6" title="Explain why the International stations are essential for the science proposed." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].internationalStationEssentialReason,document.forms[0].internationalStationEssentialReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="internationalStationEssentialReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="internationalStationEssentialReason" ifnotexist="*"/></span></td>
			   </tr>
			</logic:equal>		
		  </logic:equal>
		  
	</logic:equal>
	
	<logic:equal name="justificationForm" property="displayOtherSchedulingConstraints" value="true">
	
		  									<!-- scheduling constraints  -->		
	<tr>
			<td class="right"><astron:label key="label.justification.technical.other.scheduling" /><!--Uploaded&nbsp;:&nbsp;--></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="otherSchedulingConstraints" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="otherSchedulingConstraints" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
	</tr>

			<logic:equal name="justificationForm" property="otherSchedulingConstraints" value="true">
				<tr>
					<td align="right" valign="top"><astron:label key="label.justification.technical.reason" /></td>
				   <td class="left"><html:textarea property="otherSchedulingConstraintsReason" cols="92" rows="6" title="Indicate whether any stations are essential for the science goals to be achieved. For example, if the longest NL baselines are essential, list the stations here: RS508,RS509,RS210. Also give details here whether observations must be performed at a specific time." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].otherSchedulingConstraintsReason,document.forms[0].otherSchedulingConstraintsReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="otherSchedulingConstraintsReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="otherSchedulingConstraintsReason" ifnotexist="*"/></span></td>
			   </tr>
			</logic:equal>	
	</logic:equal>
	
	<logic:equal name="justificationForm" property="displayCombinedDataProductRequest" value="true">
	
												<!-- combined data product request  -->	
	<tr>
			<td class="right"><astron:label key="label.justification.technical.combined.data.product.requesting" /></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="combinedDataProductRequest" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="combinedDataProductRequest" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
	</tr>	

			<logic:equal name="justificationForm" property="combinedDataProductRequest" value="true">
				<tr>
					<td align="right" valign="top"><astron:label key="label.justification.technical.reason" /></td>
				   <td class="left"><html:textarea property="combinedDataProductRequestReason" cols="92" rows="6" title="" onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].combinedDataProductRequestReason,document.forms[0].combinedDataProductRequestReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="combinedDataProductRequestReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="combinedDataProductRequestReason" ifnotexist="*"/></span></td>
			   </tr>
			</logic:equal>	
	</logic:equal>
	
	<logic:equal name="justificationForm" property="displaySensitivity" value="true">
		  
				  									<!--sensitivity requirement  -->		

				<tr>
					<td align="right" valign="top"><astron:label key="label.justification.technical.sensitivity.req" /></td>
				   <td class="left"><html:text property="sensitivity" title="Give the (maximum) continuum sensitivity you wish to achieve for your project. If you wish to specify the sensitivity per channel, please do so in the scientific justification file." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].sensitivityReason,document.forms[0].sensitivityReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(mJy/Beam)<span class="error"><astron:errors property="sensitivity" ifnotexist="*"/></span></td>
			   </tr>
			
	</logic:equal>	  
	
	<logic:equal name="justificationForm" property="displayMaxDataRate" value="true">
		  												<!--expected maximum data rate -->		
		<tr>
					<td align="right" valign="top"><astron:label key="label.justification.technical.max.datarate" /></td>
				   <td class="left"><html:text property="maxDataRate"  title="Give the maximum data rate your observations will demand from the system (GB/s). An approximation
						can be made by dividing the storage request of the most demanding observing-run by its duration (in seconds)." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].maxDataRateReason,document.forms[0].maxDataRateReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(GB/s)<span class="error"><astron:errors property="maxDataRate" ifnotexist="*"/></span></td>
			   </tr>
			
	</logic:equal>
	
	<logic:equal name="justificationForm" property="displayROProcessing" value="true">
										<!--  processing offered by the RO  -->		
	<tr>
			<td class="right"><astron:label key="label.justification.technical.ro.processing" /><!--Uploaded&nbsp;:&nbsp;--></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="roProcessing" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="roProcessing" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
	</tr>
		
			<logic:equal name="justificationForm" property="roProcessing" value="true">
			<tr>
			<td class="right"><astron:label key="label.justification.technical.ro.processing.default" /></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="defaultROProcessing" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="defaultROProcessing" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>
 			</tr>
			<logic:equal name="justificationForm" property="defaultROProcessing" value="true">
				<tr>
					<td align="right" valign="top"><astron:label key="label.justification.technical.reason" /></td>
				   <td class="left"><html:textarea property="roProcessingReason" cols="92" rows="6" title="Clearly explain why you want the default imaging pipeline to be run on your data and whether the self-calibration pipeline should be run." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].roProcessingReason,document.forms[0].roProcessingReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="roProcessingReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="roProcessingReason" ifnotexist="*"/></span></td>
			   </tr>
			</logic:equal>		
		  </logic:equal>
	</logic:equal>
	
	<logic:equal name="justificationForm" property="displayROProcessingWithoutSelfCal" value="true">		
	<tr>
			<td class="right"><astron:label key="label.justification.technical.ro.processing" /></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="roProcessing" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="roProcessing" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
		</tr>
		<!--<logic:equal name="justificationForm" property="roProcessing" value="true">
			<tr>
				<td align="right" valign="top"><astron:label key="label.justification.technical.reason" /></td>
			   <td class="left"><html:textarea property="roProcessingReason" cols="92" rows="6" title="Give a clear explanation of the requested processing." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].roProcessingReason,document.forms[0].roProcessingReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="roProcessingReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="roProcessingReason" ifnotexist="*"/></span></td>
		   </tr>
		</logic:equal>-->		
	</logic:equal> 

	
	
	<logic:equal name="justificationForm" property="displayLtaStorage" value="true">		  
		  												<!--lta storage -->		
	<tr>
		<td class="right"><astron:label key="label.justification.technical.raw.storage" /><!--Uploaded&nbsp;:&nbsp;--></td>
		<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="ltaRawStorage" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
						 <html:radio styleClass="radio" name="justificationForm" property="ltaRawStorage" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
		</td>		
	</tr>

	<logic:equal name="justificationForm" property="ltaRawStorage" value="true">
	<tr>
		<td align="right" valign="top"><astron:label key="label.justification.technical.reason" /></td>
		<td class="left"><html:textarea property="ltaRawStorageReason" cols="92" rows="6" title="Give a clear reason why the raw data is essential to be stored in the LTA." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].ltaStorageReason,document.forms[0].ltaStorageReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
			(Words entered: <html:text property="ltaRawStorageReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="ltaStorageReason" ifnotexist="*"/></span></td>
	</tr>
	</logic:equal>	

	</logic:equal>	
	
	<logic:equal name="justificationForm" property="displayLtaStorageAdvanced" value="true">
				  
		  												<!--advanced lta storage -->		
	<tr>
			<td class="right">Do you request your data to be stored at a specific site? &nbsp;</td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="ltaStorage" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="ltaStorage" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
	</tr>

			<logic:equal name="justificationForm" property="ltaStorage" value="true">
				<tr>
					<td class="right">Which LTA site? &nbsp;</td>
					<td>
						<!--  
							A drop down box is implemented here. This is a way how to do it. But we strongly recommend to
							use the XML option system. Discussed in the opticonobservationgeneral.jsp	
				 		-->
					<html:select name="justificationForm" property="ltaStorageLocation" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
  						<html:option value="SARA"/>
  						<html:option value="Juelich"/>
  						<html:option value="Poznan"/>
  						<html:option value="SARA or Juelich"/>
  						<html:option value="SARA or Poznan"/>
  						<html:option value="Juelich or Poznan"/>
					</html:select>
					</td>
				</tr>
				<tr>
					<td align="right" valign="top">Motivate why this LTA site is needed : &nbsp;</td>
				   <td class="left"><html:textarea property="ltaStorageReason" cols="92" rows="6" title="Give a clear reason why the data needs to be stored at this specific LTA site." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].ltaStorageReason,document.forms[0].ltaStorageReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="ltaStorageReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="ltaStorageReason" ifnotexist="*"/></span></td>
			   </tr>
			</logic:equal>	
			<logic:equal name="justificationForm" property="ltaStorage" value="false">
				<jsp:setProperty name="justificationForm" property="ltaStorageLocation" value = ""/>
			</logic:equal>	
		</logic:equal>	
		
		<!--off-line data processing on RO facilities (CEP3) -->
		<logic:equal name="justificationForm" property="displayOfflineROProcessing" value="true">
				
	<tr>
			<td class="right"><astron:label key="label.justification.technical.ro.processing.offline" /><!--Uploaded&nbsp;:&nbsp;--></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="offlineROProcessing" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="offlineROProcessing" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
	</tr>

			<logic:equal name="justificationForm" property="offlineROProcessing" value="true">
				<tr>
					<td align="right" valign="top"><astron:label key="label.justification.technical.reason" /></td>
				   <td class="left"><html:textarea property="offlineROProcessingReason" cols="92" rows="6" title="Please, if you plan to execute your processing on both LOFAR and external facilities available to you, explain how you plan to split the time between CEP3 and these other facilities." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].offlineROProcessingReason,document.forms[0].offlineROProcessingReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="offlineROProcessingReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="offlineROProcessingReason" ifnotexist="*"/></span></td>
			   </tr>
			</logic:equal>		
	</logic:equal>
	
				  
		  												<!-- external processing facilities -->		
	
	<logic:equal name="justificationForm" property="displayExternalProcessing" value="true">
	<tr>
			<td class="right"><astron:label key="label.justification.technical.external.processing" /><!--Uploaded&nbsp;:&nbsp;--></td>
			<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="externalProcessing" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="externalProcessing" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
	</tr>

			<logic:equal name="justificationForm" property="externalProcessing" value="true">
				<tr>
					<td align="right" valign="top"><astron:label key="label.justification.technical.reason" /></td>
				   <td class="left"><html:textarea property="externalProcessingReason" cols="92" rows="6" title="Give a brief overview of the processing facilities you have access to, and how you plan to process the
					data there." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].externalProcessingReason,document.forms[0].externalProcessingReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="externalProcessingReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="externalProcessingReason" ifnotexist="*"/></span></td>
			   </tr>
			</logic:equal>	
		
		</logic:equal>
			
	<logic:equal name="justificationForm" property="displayCepRequesting" value="true">
		<tr>
				<td class="right"><astron:label key="label.justification.technical.cep.requesting" /></td>
				<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="cepRequesting" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
								 <html:radio styleClass="radio" name="justificationForm" property="cepRequesting" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
				</td>		
		</tr>

			<logic:equal name="justificationForm" property="cepRequesting" value="true">
				<tr>
					<td align="right" valign="top"><astron:label key="label.justification.technical.reason" /></td>
				   <td class="left"><html:textarea property="cepRequestingReason" cols="92" rows="6" title="Please elaborate why you request a processing time different from the Northstar calculations. E.g., you may find that Northstar made a miscalculation, 
				   which can be indicated here." onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].cepRequestingReason,document.forms[0].cepRequestingReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="cepRequestingReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="cepRequestingReason" ifnotexist="*"/></span></td>
			   </tr>
			</logic:equal>	
		</logic:equal>		
		
	<logic:equal name="justificationForm" property="displayFillerTime" value="true">
		<tr>
				<td class="right">Would you like to apply for user-shared support?  &nbsp; </td>
				<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="fillerTime" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="fillerTime" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>		
		</tr>

			<logic:equal name="justificationForm" property="fillerTime" value="true">
				<tr>
					<td align="right" valign="top">Please justify why your project is eligible for that. &nbsp; </td>
				   <td class="left"> <html:textarea property="fillerTimeReason" cols="92" rows="6" onkeypress="" onchange="" onkeydown="" onkeyup="CountWords(document.forms[0].fillerTimeReason,document.forms[0].fillerTimeReasonCount)" onblur="" onmousedown="" onmousemove="" onmouseout="" onmouseover="" onfocus="" ondblclick="" onmouseup="" />
					(Words entered: <html:text property="fillerTimeReasonCount" size="2" disabled="true" styleClass="count"/>)<span class="error"><astron:errors property="fillerTimeReason" ifnotexist="*"/></span></td>
			   </tr>
			</logic:equal>	
		</logic:equal>

	<logic:equal name="justificationForm" property="displayCoObservationTeam" value="true">
				<tr>
					<td class="right">Do you request <a href="http://old.astron.nl/radio-observatory/observing-proposals/co-observing-surveys-ksp-team/co-observing-surveys-ksp-team" target="_blank"> 
						co-observing with the LOFAR Two-metre Sky Survey</a>? &nbsp; </td>
					<td class="left"> <html:radio styleClass="radio" name="justificationForm" property="coObservationTeam" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							 <html:radio styleClass="radio" name="justificationForm" property="coObservationTeam" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
					</td>
				</tr>
			</logic:equal>





		</logic:equal>
	<!-- end of technical justification	 -->
		
		<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>
	<bean:define id="userIsContactAuthor" name="justificationForm" property="userIsContactAuthor"/>
<%@ include file="include/storebuttons.jsp" %>		

</html:form>
