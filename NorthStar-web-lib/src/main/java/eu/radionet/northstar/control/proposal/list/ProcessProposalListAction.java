// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessProposalAction.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: ProcessProposalListAction.java,v 1.4 2006-06-30 10:01:00 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.list;

import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The ProcessProposalAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessProposalListAction extends Action {
	private Log log = LogFactory.getLog(ProcessProposalListAction.class);

	private NorthStarDelegate proposalListDelegate = null;

	private CollaborationDelegate collaborationDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ProposalListForm proposalListForm = (ProposalListForm) form;
		HttpSession session = request.getSession();
		ActionMessages errors = new ActionMessages();
		Proposal proposal = null;
		ProposalData proposalData = null;
		SelectionBean selectionBean = new SelectionBean();
		selectionBean.setSelectedTelescopes(proposalListForm
				.getSelectedTelescopes());
		selectionBean.setShowReviewedProposals(proposalListForm
				.isShowReviewedProposals());
		session.setAttribute(Constants.OWN_PROPOSAL_SELECTION, selectionBean);
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);

		proposalListDelegate = NorthStarDelegate.getInstance();
		collaborationDelegate = CollaborationDelegate.getInstance();
		if (proposalListForm.getNewButton() != null) {
			/*
			 * create new proposal
			 */
			// proposalForm.setProposal(proposal);
			// session.setAttribute(Constants.PROPOSAL_FORM, proposalForm);
			session.setMaxInactiveInterval(28800);
			if (proposalListForm.getSelectedTelescopes() != null
					&& proposalListForm.getSelectedTelescopes().length == 1) {
				return ParameterAppender.append(mapping
						.findForward("selectTelescope"),
						"selectedTelescope", proposalListForm
								.getSelectedTelescopes()[0]);
			} else {
				return mapping.findForward("selectTelescope");
			}
		}
		Iterator telescopeBeanIterator = proposalListForm.getTelescopeBeans()
				.iterator();
		while (telescopeBeanIterator.hasNext()) {
			TelescopeBean telescopeBean = (TelescopeBean) telescopeBeanIterator
					.next();

			/*
			 * if delete, edit, copy, delete , submit button pressed, retrieve
			 * selected proposal
			 */
			if (telescopeBean.getDeleteButton(0) != null
					|| telescopeBean.getEditButton(0) != null
					|| telescopeBean.getCopyButton(0) != null
					|| telescopeBean.getRetractButton(0) != null
					|| telescopeBean.getSubmitButton(0) != null
					|| telescopeBean.getViewProposalButton(0) != null) {
				proposalData = new ProposalData();
				Integer proposalId = getProposalId(telescopeBean
						.getProposalBeans(), telescopeBean.getIndex());
				try {
					proposal = proposalListDelegate.getOwnProposal(proposalId,
							ownUserAccount);
					if (proposal == null) {
						proposal = collaborationDelegate.getProposal(
								proposalId, ownUserAccount);
					}
					proposalData = NorthStarUtils.getNewProposalData(proposal);

					if (telescopeBean.getCopyButton(0) != null) {

						return ParameterAppender.append(mapping
								.findForward("selectTelescope"), new String[] {
								"proposalId", "selectedTelescope" },
								new String[] { proposalId.toString(),
										proposalData.getSelectedTelescope() });

					}
					ActionForward forward = null;
					/*
					 * if delete button pressed
					 */
					if (telescopeBean.getDeleteButton(0) != null) {
						forward = mapping.findForward("delete");

					} else if (telescopeBean.getSubmitButton(0) != null) {
						// submit button
						forward = mapping.findForward("submit");
						proposalData.setIgnoreDeadLineWarning(false);
						proposalData
								.setIgnoreInvalidConfigurationWarning(false);
						proposalData.setCancelWarningForward(mapping
								.findForward(Constants.REFRESH));
						proposalData.setSuccessWarningForward(forward);

					} else if (telescopeBean.getRetractButton(0) != null) {
						// retract button
						forward = mapping.findForward("retract");

					} else if (telescopeBean.getEditButton(0) != null) {
						session.setMaxInactiveInterval(28800);

						forward = mapping.findForward("success");
						proposalData.setIgnoreDeadLineWarning(false);
						proposalData
								.setIgnoreInvalidConfigurationWarning(false);
						proposalData.setCancelWarningForward(mapping
								.findForward(Constants.REFRESH));
						proposalData.setSuccessWarningForward(forward);

					} else if (telescopeBean.getViewProposalButton(0) != null) {
						forward = mapping.findForward("viewProposal");
						forward = ParameterAppender.append(forward,
								"proposalId", proposalId.toString());
						// session.setAttribute(Constants.PROPOSAL_DATA,
						// proposalData);
						return forward;
					}
					ProposalData oldProposalData = (ProposalData) session
							.getAttribute(Constants.PROPOSAL_DATA);
					if (oldProposalData != null) {
						session.setAttribute(Constants.NEW_PROPOSAL_DATA,
								proposalData);
						String forwardPath = forward.getPath();

						return ParameterAppender.append(mapping
								.findForward("overWriteProposalData"),
								Constants.FORWARD, forwardPath);
					} else {
						session.setAttribute(Constants.PROPOSAL_DATA,
								proposalData);
					}
					/*
					 * checks if another session locks the proposal, if not it
					 * locks automaticly the proposal
					 */
					if (collaborationDelegate.proposalIsLockedByOthers(
							proposalId, ownUserAccount, session.getId())) {
						String forwardPath = forward.getPath();

						request.setAttribute(Constants.FORWARD, forwardPath);
						return ParameterAppender.append(mapping
								.findForward("unlockProposal"), "proposalId",
								AstronConverter.toString(proposalId));
					} else {
						collaborationDelegate.lockProposal(proposalId,
								ownUserAccount, session.getId());

					}

					return forward;
				} catch (DatabaseException de) {
					log.error("Cannot retrieve proposal: " + de.getMessage());
					errors.add(Constants.ERROR, new ActionMessage(
							"error.database.retrieve.proposal"));
					saveErrors(request, errors);
					return mapping.getInputForward();
				}

			} 
		}
		return mapping.findForward(Constants.REFRESH);

	}

	/**
	 * The getProposalId method retrieves proposalId with index
	 * 
	 * @param proposalBeans
	 * @param index
	 * @return
	 */
	private Integer getProposalId(List proposalBeans, int index) {
		ProposalBean proposalBean = (ProposalBean) proposalBeans.get(index);

		return new Integer(proposalBean.getId());
	}

}