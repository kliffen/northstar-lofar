// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalStatus;
import eu.radionet.northstar.data.entities.collaboration.ProposalLock;

public class SetUpAdminOverviewAction extends Action {

	private Log log = LogFactory.getLog(this.getClass());

	private NorthStarDelegate northStarDelegate = null;

	private CollaborationDelegate collaborationDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AdminOverviewForm adminOverviewForm = (AdminOverviewForm) form;

		HttpSession session = request.getSession();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		northStarDelegate = NorthStarDelegate.getInstance();
		collaborationDelegate = CollaborationDelegate.getInstance();
		List proposalLockBeans = new ArrayList();
		List proposalLocks = collaborationDelegate.getValidProposalLocks();
		//ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
		Iterator proposalLocksIterator = proposalLocks.iterator();
		while (proposalLocksIterator.hasNext()) {
			ProposalLockBean proposalLockBean = new ProposalLockBean();
			ProposalLock proposalLock = (ProposalLock) proposalLocksIterator
					.next();
			try {
				Proposal proposal = northStarDelegate.getProposal(proposalLock
						.getProposalId());
				proposalLockBean.setId(AstronConverter.toString(proposalLock
						.getProposalId()));
				proposalLockBean.setCode(proposal.getCode());
				if (proposal.getJustification() != null) {
					proposalLockBean.setTitle(proposal.getJustification()
							.getTitle());
				}
				proposalLockBean.setContactAuthor(NorthStarDelegate
						.getName(northStarDelegate
								.getContactAuthorAsUser(proposal)));
				proposalLockBean.setOwner(NorthStarDelegate
						.getName(northStarDelegate.getUser(proposalLock
								.getUserId())));
				proposalLockBean.setSince(AstronConverter
						.toDateString(proposalLock.getSince()));
				proposalLockBean.setLastAction(AstronConverter
						.toDateString(proposalLock.getLastAction()));

				proposalLockBean.setTelescope(proposal.getSemester().getTelescope());
				ProposalStatus proposalStatus = proposal
						.getCurrentStatus();
				proposalLockBean
						.setStatus(proposalStatus.getStatus().getCode());
				proposalLockBeans.add(proposalLockBean);
			} catch (Exception e) {
				log.warn(LogMessage.getMessage(ownUserAccount, "Error with proposal with id: " + proposalLock
						.getProposalId()));
			}
		}
		adminOverviewForm.setProposalLockBeans(proposalLockBeans);
		return mapping.findForward(Constants.SUCCESS);
	}
}
