// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ApplicantForm.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: ThesisForm.java,v 1.3 2006-08-14 14:41:43 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.additionalissues.thesis;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The ThesisForm provides
 * 
 * @author Hanno Holties
 *  
 */
public class ThesisForm extends ActionForm {

    private String thesisId = null;
    private String forward = null;

    private List memberBeans = new ArrayList();

    private String studentRef = null;
    private String studentName = null;
    private String studentLevel = null;
    private String supervisorRef = null;
    private String supervisorName = null;
    private String expectedCompletionDateString = null;
	private boolean dataRequired = true;
    private boolean studentApplicant = true;
    private boolean supervisorApplicant = true;

    private String commitThesisButton = null;
    
    private List studentLevels = new ArrayList();

    
    /*
	 * TODO: This should only be temporarely! this needs to be replaces with a decent 
	 * XML configuration
	 */
	public void initStudentLevels()
	{
		List stringList= new ArrayList();
		stringList.add("Pre-university");
		stringList.add("Bachelor");
		stringList.add("Master");
		stringList.add("Doctor");
		this.studentLevels = OptionsUtils.getLableValueBeansFromStringList(stringList);
	}
    
    
    
    /**
     * @return Returns the memberBeans.
     */
    public List getMemberBeans() {
        return memberBeans;
    }

    /**
     * @param memberBeans The memberBeans to set.
     */
    public void setMemberBeans(List memberBeans) {
        this.memberBeans = memberBeans;
    }

    /**
     * @return Returns the dataRequired.
     */
    public boolean isDataRequired() {
        return dataRequired;
    }

    /**
     * @param dataRequired The dataRequired to set.
     */
    public void setDataRequired(boolean dataRequired) {
        this.dataRequired = dataRequired;
    }

    /**
     * @return Returns the expectedCompletionDate.
     */
    public String getExpectedCompletionDate() {
        return expectedCompletionDateString;
    }

    /**
     * @param expectedCompletionDate The expectedCompletionDate to set.
     */
    public void setExpectedCompletionDate(String expectedCompletionDate) {
        this.expectedCompletionDateString = expectedCompletionDate;
    }

    /**
     * @return Returns the forward.
     */
    public String getForward() {
        return forward;
    }

    /**
     * @param forward The forward to set.
     */
    public void setForward(String forward) {
        this.forward = forward;
    }

    /**
     * @return Returns the studentName.
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @param studentName The studentName to set.
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getStudentLevel() {
		return studentLevel;
	}

	public void setStudentLevel(String studentLevel) {
		this.studentLevel = studentLevel;
	}

	/**
     * @return Returns the studentRef.
     */
    public String getStudentRef() {
        return studentRef;
    }

    /**
     * @param studentRef The studentRef to set.
     */
    public void setStudentRef(String studentRef) {
        this.studentRef = studentRef;
    }

    /**
     * @return Returns the supervisorName.
     */
    public String getSupervisorName() {
        return supervisorName;
    }

    /**
     * @param supervisorName The supervisorName to set.
     */
    public void setSupervisorName(String supervisorName) {
        this.supervisorName = supervisorName;
    }

    /**
     * @return Returns the supervisorRef.
     */
    public String getSupervisorRef() {
        return supervisorRef;
    }

    /**
     * @param supervisorRef The supervisorRef to set.
     */
    public void setSupervisorRef(String supervisorRef) {
        this.supervisorRef = supervisorRef;
    }

    /**
     * @return Returns the thesisId.
     */
    public String getThesisId() {
        return thesisId;
    }

    /**
     * @param thesisId The thesisId to set.
     */
    public void setThesisId(String thesisId) {
        this.thesisId = thesisId;
    }

    /**
     * @return Returns the studentApplicant.
     */
    public boolean isStudentApplicant() {
        return studentApplicant;
    }

    /**
     * @param studentApplicant The studentApplicant to set.
     */
    public void setStudentApplicant(boolean studentApplicant) {
        this.studentApplicant = studentApplicant;
    }

    /**
     * @return Returns the supervisorApplicant.
     */
    public boolean isSupervisorApplicant() {
        return supervisorApplicant;
    }

    /**
     * @param supervisorApplicant The supervisorApplicant to set.
     */
    public void setSupervisorApplicant(boolean supervisorApplicant) {
        this.supervisorApplicant = supervisorApplicant;
    }

    /**
     * @return Returns the commitThesisButton.
     */
    public String getCommitThesisButton() {
        return commitThesisButton;
    }

    /**
     * @param commitThesisButton The commitThesisButton to set.
     */
    public void setCommitThesisButton(String commitThesisButton) {
        this.commitThesisButton = commitThesisButton;
    }

    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        
        /*
         * Only validate if data is being committed
         */
        if (!AstronValidator.isBlankOrNull(commitThesisButton)) {
            if (AstronValidator.isBlankOrNull(studentRef)
                    && AstronValidator.isBlankOrNull(studentName)) {
                errors.add("student", new ActionMessage(
                        "error.required.thesis.student"));
            } 
            if (AstronValidator.isBlankOrNull(studentLevel)) {
                errors.add("studentLevel", new ActionMessage(
                        "error.required.thesis.studentlevel"));
            } 
            if (AstronValidator.isBlankOrNull(supervisorRef)
                    && AstronValidator.isBlankOrNull(supervisorName)) {
                errors.add("supervisor", new ActionMessage(
                        "error.required.thesis.supervisor"));
            } 
            if (!AstronValidator.isBlankOrNull(studentRef) &&
                    !AstronValidator.isBlankOrNull(supervisorRef)) {
                if (studentRef.equalsIgnoreCase(supervisorRef)) {
                    errors.add("supervisor", new ActionMessage(
                    "error.invalid.thesis.samemember"));
                }
            }
            
            HttpSession session = request.getSession();

            /*
             * retrieve session object with information
             */
            ProposalData proposalData = (ProposalData) session
                    .getAttribute(Constants.PROPOSAL_DATA);

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                    Constants.DATE_FORMAT);
            if (!AstronValidator.isBlankOrNull(expectedCompletionDateString)) {
                Date expectedCompletionDate = null;
                Date deadlineDate = null;
                /*
                 * check if it the date has a right format
                 */
                try {
                    expectedCompletionDate = simpleDateFormat
                            .parse(expectedCompletionDateString);

                    deadlineDate = proposalData.getProposal()
                            .getSemester().getDeadLine();

                    if (!isValidCompletionDate(expectedCompletionDate, deadlineDate)) {
                        errors.add("expectedCompletionDate", new ActionMessage(
                                "error.invalid.date.expectedcompletion"));
                    }
                } catch (ParseException pe) {
                    errors.add("expectedCompletionDate", new ActionMessage(
                            "error.dateformat"));
                }
            } else {
                errors.add("expectedCompletionDate", new ActionMessage(
                        "error.required.thesis.expectedCompletionDate"));
            }

            if (!errors.isEmpty()) {
                /*
                 * Refresh: Refill the list of members if required
                 */
                if (studentApplicant || supervisorApplicant) {
                    Proposal proposal = proposalData.getProposal();
                    /*
                     * iterator through members
                     */
                    Iterator membersIterator = proposal.getMembers().iterator();
                    List memberBeans = new ArrayList();
                    
                    int i = 0;
                    while (membersIterator.hasNext()) {
                        MemberBean memberBean = new MemberBean();
                        memberBean.setId(new Integer(i));

                        Member member = (Member) membersIterator.next();
                        memberBean.setMember(member);

                        i++;

                        memberBeans.add(memberBean);

                    }
                    this.memberBeans = memberBeans;
                }
                /*
                 * Refresh : reload the studentLevels
                 */
                initStudentLevels();                
            }
        }
        return errors;
    }

    public boolean isValidCompletionDate(Date expectedCompletionDate,
            Date deadLine) {
        if (expectedCompletionDate == null || deadLine == null) {
            return false;
        }
        if (expectedCompletionDate.after(deadLine)) {
            return true;
        } else {
            return false;
        }
    }

	public List getStudentLevels() {
		return studentLevels;
	}

	public void setStudentLevels(List studentLevels) {
		this.studentLevels = studentLevels;
	}

}