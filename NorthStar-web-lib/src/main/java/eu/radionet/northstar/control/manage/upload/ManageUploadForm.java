// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.manage.upload;

import java.util.List;

import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

public class ManageUploadForm extends ActionForm {
	private String currentConfigFileUrl = null;
	private boolean hasMultipleAccess = false;
	private List userTelescopes = null;
	private String selectedTelescope = null;
	private String uploadFileButton = null;
	private boolean configOk = false;
	private String readConfigFile = null;
	private FormFile configFile = null;
	private String currentConfigFileDate = null;
	private String previousConfigFileDate = null;
	
	public String getCurrentConfigFileUrl() {
		return currentConfigFileUrl;
	}
	public void setCurrentConfigFileUrl(String currentConfigFileUrl) {
		this.currentConfigFileUrl = currentConfigFileUrl;
	}
	public boolean isHasMultipleAccess() {
		return hasMultipleAccess;
	}
	public void setHasMultipleAccess(boolean hasMultipleAccess) {
		this.hasMultipleAccess = hasMultipleAccess;
	}
	public List getUserTelescopes() {
		return userTelescopes;
	}
	public void setUserTelescopes(List userTelescopes) {
		this.userTelescopes = userTelescopes;
	}
	public String getSelectedTelescope() {
		return selectedTelescope;
	}
	public void setSelectedTelescope(String selectedTelescope) {
		this.selectedTelescope = selectedTelescope;
	}
	public String getUploadFileButton() {
		return uploadFileButton;
	}
	public void setUploadFileButton(String uploadFileButton) {
		this.uploadFileButton = uploadFileButton;
	}
	public boolean isConfigOk() {
		return configOk;
	}
	public void setConfigOk(boolean configOk) {
		this.configOk = configOk;
	}
	public FormFile getConfigFile() {
		return configFile;
	}
	public void setConfigFile(FormFile configFile) {
		this.configFile = configFile;
	}
	public String getCurrentConfigFileDate() {
		return currentConfigFileDate;
	}
	public void setCurrentConfigFileDate(String currentConfigFileDate) {
		this.currentConfigFileDate = currentConfigFileDate;
	}
	public String getPreviousConfigFileDate() {
		return previousConfigFileDate;
	}
	public void setPreviousConfigFileDate(String previousConfigFileDate) {
		this.previousConfigFileDate = previousConfigFileDate;
	}
	public String getReadConfigFile() {
		return readConfigFile;
	}
	public void setReadConfigFile(String readConfigFile) {
		this.readConfigFile = readConfigFile;
	}
		

}
