// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessUploadAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: ProcessOverwriteProposalDataAction.java,v 1.1 2006-05-11 12:16:44 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.overwrite;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.ParameterAppender;

/**
 * The ProcessUploadAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessOverwriteProposalDataAction extends Action {
	private Log log = LogFactory.getLog(this.getClass());

	private CollaborationDelegate collaborationDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		OverwriteProposalDataForm lockProposalForm = (OverwriteProposalDataForm) form;

		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
		collaborationDelegate = CollaborationDelegate.getInstance();
		if (isCancelled(request)) {
			if (log.isTraceEnabled()) {
				log.trace(LogMessage.getMessage(ownUserAccount,
						"Cancelled, forward to '" + Constants.CANCEL + "'"));
			}
			session.removeAttribute(Constants.NEW_PROPOSAL_DATA);
			return mapping.findForward(Constants.CANCEL);
		}	
		removeLock(request,ownUserAccount);
		ProposalData newProposalData = (ProposalData) session.getAttribute(Constants.NEW_PROPOSAL_DATA);
		session.setAttribute(Constants.PROPOSAL_DATA,newProposalData);
		session.removeAttribute(Constants.NEW_PROPOSAL_DATA);
		/*
		 * checks if another session locks the proposal, if not it locks
		 * automaticly the proposal
		 */
		if (collaborationDelegate.proposalIsLockedByOthers(newProposalData.getProposal().getId(),
				ownUserAccount, session.getId())) {

			request.setAttribute(Constants.FORWARD, lockProposalForm.getForward());
			return ParameterAppender.append(mapping
					.findForward("unlockProposal"), "proposalId",
					AstronConverter.toString(newProposalData.getProposal().getId()));
		} else {
			collaborationDelegate.lockProposal(newProposalData.getProposal().getId(),
					ownUserAccount, session.getId());

		}
        ActionForward forward = new ActionForward();
        forward.setRedirect(true);
        forward.setPath(lockProposalForm.getForward());
        return forward;

	}
	protected void removeLock(HttpServletRequest request,
			UserAccount ownUserAccount) throws DatabaseException {
		collaborationDelegate.unlockProposals(ownUserAccount, request
				.getSession().getId());
		request.getSession().removeAttribute(Constants.PROPOSAL_DATA);
	}
}