// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * JustificationFile.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: JustificationFile.java,v 1.1 2006-05-02 10:01:10 verhoef Exp $
 *
 */
package eu.radionet.northstar.data.entities;

import java.io.Serializable;

/**
 * The JustificationFile provides
 * 
 * @author Bastiaan Verhoef
 *  
 */
public class JustificationFile implements Serializable {
    private Integer id = null;


    private byte[] uploadFile = null;
	protected Integer justificationId = null;

    /**
     * @return Returns the id.
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            The id to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }



    /**
     * @return Returns the uploadFile.
     */
    public byte[] getUploadFile() {
        return uploadFile;
    }

    /**
     * @param uploadFile
     *            The uploadFile to set.
     */
    public void setUploadFile(byte[] uploadFile) {
        this.uploadFile = uploadFile;
    }

	public Integer getJustificationId() {
		return justificationId;
	}

	public void setJustificationId(Integer justificationId) {
		this.justificationId = justificationId;
	}

}