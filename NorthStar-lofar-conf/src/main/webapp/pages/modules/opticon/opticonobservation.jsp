<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<link rel="stylesheet"  href="<%=request.getContextPath()%>/css/jquery-ui-1.10.4.min.css">
<tiles:get name="errors" />
<!-- This jsp is used when you want to define a target or observation  -->
<html:form action="/processObservation" method="POST">
	<!-- The top part of the observation page it sets a help button in the top right corner -->	
	<table width="100%">
		<TR>
			<TD width="*">&nbsp;</TD>
			<TD class="help" width="60">
				<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
				<TABLE width="60" height="25">
					<TR>
			  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#opticonobservation\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
		  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#opticonobservation\" target=\"help\">Help</a>"%></TD>
	  				</TR>
	  			</TABLE>
			<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
			</TD>
		</TR>
	</table>

	<table align="center">
<tr>
	<td colspan="2">&nbsp;</td>
</tr>


<tr>
	<td class="header" colspan="2">Targets :</td>
</tr>		
 <tr>
	<td class="header" colspan="2">Add here your target list per priority. Higher-priority targets on top.</td>
</tr>
<tr>
	<td colspan="2"><tiles:get name="box_header_no_top" />
	<table width="100%">
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
<pre>In case of partial allocations and no preference indicated by the ILT-PC, ASTRON will select the targets highest on your input list.
<b>Please check the coordinates of your intended target(s) in <a href="https://lta.lofar.eu/">the LOFAR archive</a> to confirm that your target has not been observed by LOFAR already.
In case of duplication, please specify in the science case why the new observation is needed. If you are interested in interferometric data and the
LOFAR Two-Metre Sky Survey (LoTTS) observing and processing setups are suitable to achieve your science goals, please also check if your targets have been covered in any
of the <a href="http://old.astron.nl/radio-observatory/observing-proposals/co-observing-surveys-ksp-team/co-observing-surveys-ksp-team" target="_blank">performed / planned pointings of the LoTTS</a> </b> </pre>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	<!-- If there are target/observations defined show some target properties in a tabel  -->
	<logic:notEmpty name="observationForm" property="targetSummaryValues">
	
	<TR>
		<TD colspan="2">
			<table class="projecttable"  align="center">
			<!-- write the column headers j should contain the number of target properties that are displayed. The total columns of this
				table should be j+3 (the proporties plus three columns containing buttons -->
			<% int j = 0; %>
				<TR>
					<logic:iterate id="targetSummaryLabel" name="observationForm" property="targetSummaryLabels">
						<TD  class="tableheader">&nbsp;<bean:write name="targetSummaryLabel" />&nbsp;</TD>
	 		<% j++; %>
					</logic:iterate>
					<!--  the last three burtton columns do not have a header -->
					<td class="tableheader" colspan="3">&nbsp;</td>
				</TR>
			<!-- 
				the following two variables are used to change the color of the table rows. Even rows have a different color than
				odd rows
			 -->
			<%
			 int i=0;
			 String styleClass = "";
			%>				
				<!--  Iterate though the defined targets. One target a row. -->
	 			<logic:iterate id="targetBean" name="observationForm" property="targetSummaryValues">
	 			<%
	 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
	 			%>
 	 			<TR>
 	 			<!-- iterate all the target properties and display them. (1st element has a different style) -->
 	 			<logic:iterate id="valueBean" indexId="valueBeanId" name="targetBean">
 	 				<logic:equal name="valueBean" property="display" value="true">
	 	 			<logic:equal name="valueBeanId" value="0">
						<TD class="tablefield_pri<%=styleClass%>">
						<!-- highlight target that is being edited -->
						<logic:equal name="observationForm" property="selectedTargetId" value="<%=\"\"+(i-1)%>">
							<b><i>
								</logic:equal>
									<bean:write name="valueBean" property = "value"/>
								<logic:equal name="observationForm" property="selectedTargetId" value="<%=\"\"+(i-1)%>">
							</i></b>
						</logic:equal>
						</TD>
	 	 			</logic:equal>
	 	 			<logic:notEqual name="valueBeanId" value="0">
	 	 				<TD class="tablefield<%=styleClass%>">
						<logic:equal name="observationForm" property="selectedTargetId" value="<%=\"\"+(i-1)%>">
							<b><i>
						</logic:equal>
	 	 					<bean:write name="valueBean" property = "value"/>
						<logic:equal name="observationForm" property="selectedTargetId" value="<%=\"\"+(i-1)%>">
							</i></b>
						</logic:equal>
	 	 				</TD>
	 	 			</logic:notEqual>
					</logic:equal>
 	 			</logic:iterate>
 	 			<!--  Display the edit buton -->
 	 			
				<TD class="tablefield<%=styleClass%>">
				
				<html:submit disabled="false"  indexed="true" property="editTargetButton" styleClass="list_edit" title="Edit this target/observation" onclick="setNorthStarCookie(getScrollCoordinates());">Edit</html:submit>  
				</TD>					
				<!--  when the maximum limit of target has not been reached show the copy button -->
				<logic:equal name="observationForm" property="maxNumberOfTargetsReached" value="true">
					<TD class="tablefield<%=styleClass%>"><html:submit disabled="true" indexed="true" property="copyTargetButton" styleClass="list_copy" title="Copy this target/observation" onclick="setNorthStarCookie(getScrollCoordinates());">Copy</html:submit></TD>
				</logic:equal>
				<logic:equal name="observationForm" property="maxNumberOfTargetsReached" value="false">
					<TD class="tablefield<%=styleClass%>"><html:submit disabled="false" indexed="true" property="copyTargetButton" styleClass="list_copy" title="Copy this target/observation" onclick="setNorthStarCookie(getScrollCoordinates());">Copy</html:submit></TD>
				</logic:equal>
				<!--  Display the delete button -->
				<TD class="tablefield<%=styleClass%>"><html:submit disabled="false" indexed="true" property="deleteTargetButton" styleClass="list_delete" title="Delete target/observation, no prompt!" onclick="setNorthStarCookie(getScrollCoordinates());">Delete</html:submit></TD>

	 			</TR>
	 			</logic:iterate>
	 			<tr><td colspan="<%=j+3 %>" class="spacer">&nbsp;</td></tr>	 				 			
			</table>
		</TD>
	</TR>
	<!-- If the maximum limit of target/observation has been reached display a message -->
	<tr>
		<td colspan="2" class="italic" align="center" >&nbsp;
		<logic:equal name="observationForm" property="maxNumberOfTargetsReached" value="true">
			The maximum number of targets is reached
		</logic:equal>
	</td>
	</tr>
	<!-- If the maximum limit of target/observation has been reached for sam run number while copying display a message -->
	<tr>
		<td colspan="2" class="italic" align="center" >&nbsp;
		<span class="error"><astron:errors property="maxNumberOfTargetsonSameRunReached" ifnotexist=""/></span>
	</td>
	</tr>
	</logic:notEmpty>	
	<logic:equal name="observationForm" property="autoCommit" value="false">
		<logic:empty name="observationForm" property="targetSummaryValues">
		<tr>
			<td colspan="2" class="bold" align="center" >&nbsp;
				List of targets is empty
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;
			</td>
		</tr>
	
		</logic:empty>
	</logic:equal>
<tr>
	<html:hidden name="observationForm" property="targetFirstInRun" />
	<html:hidden name="observationForm" property="enableTimeAndObservation" />
	<logic:equal name="observationForm" property="selectedTargetId" value="-1">
		<td class="header" colspan="2">New Target :</td>
	</logic:equal>
	<logic:notEqual name="observationForm" property="selectedTargetId" value="-1">
	 	<html:hidden name="observationForm" property="targetId" />
		<td class="header" colspan="2">Edit Target :</td>
	</logic:notEqual>
</tr>		
 
<tr>
	<td colspan="2"><tiles:get name="box_header_no_top" />
	<table width="100%">
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<!--  
			Here you the fields for the target can be found: The name including a button to SIMBAD, the RA, dec, and Epoch	
		 -->
		 <logic:equal name="observationForm" property="displayTargetCalibrationBeam" value="true">
		<tr>
			<td class="right"><astron:label key="label.opticon.observation.calibrationbeam" /></td>
			<td class="left">
		  			<html:radio name="observationForm" property="calibrationBeam" value="true" >Yes</html:radio>
					<html:radio name="observationForm" property="calibrationBeam" value="false" >No</html:radio>
			</td>
		</tr>
		</logic:equal>
	 
		<tr>
			<td class="right"><astron:label key="label.opticon.observation.fieldname" /></td> 
			<td class="left">
				<html:text property="fieldName" size="16" maxlength="255"/><span class="error">*</span>
<!--
Inline replacement of SOAP based simbad server, no longer supported from 2019, by javascript based name resolving

				<html:submit property="resolveButton"><astron:label key="label.opticon.observation.getsimbad" /></html:submit>
-->

				<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
				<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui-1.10.4.min.js"></script>

				<script type="text/javascript" language="JavaScript">
					function resolve_name() {
						try {
							result=$.ajax({
								// See http://cdsweb.u-strasbg.fr/doc/sesame.htx
								// Limiting results to Simbad database
								url: "https://cdsweb.u-strasbg.fr/cgi-bin/nph-sesame/-ox/S?"+document.getElementsByName("fieldName")[0].value,
								error: function(result, statusMessage, errorMessage) {
									throw("Problem querying the service\nStatus: " + statusMessage +
											"\nError: "+ errorMessage)
								},
								async: false
							});
							$xml = $( result.responseXML );
							// Handle case where no resolved sources are returned
							try {
								resolver = $xml.find("Resolver")[0].attributes["name"];
							} catch(error) {
								throw("Simbad could not resolve "+document.getElementsByName("fieldName")[0].value)
							}
							// Using first returned result only
						 	document.getElementsByName("ra")[0].value = $xml.find("jpos")[0].textContent.split(" ")[0];
						 	document.getElementsByName("dec")[0].value = $xml.find("jpos")[0].textContent.split(" ")[1];
							//Assume this is always true 
						 	document.getElementsByName("epoch")[0].value = "J2000";
						} catch(error) {
							// Catch all errors and show alert so user is aware of unexpected behaviour
							alert("Could not resolve name\n\n"+ error)
						}
					}
				</script>
				
				<button type="button"
					style="background-color: #DAE5EB;
					color: #1D1E91;
					padding-left: 10px;
					border: 2px solid #333333;
					border-top: 1px solid #666666;
					border-left: 1px solid #666666;
					cursor: pointer;
					font-family:  sans-serif;
					font-size: 1em;	
					height: 2.1em;
					font-weight		: normal;
					font-style		: normal;
					font-weight		: bold;"
					onclick="resolve_name()">Resolve using Simbad</button>
					&nbsp;
				<logic:messagesNotPresent property="fieldName">
					<astron:label key="label.opticon.observation.proposercheck" />
				</logic:messagesNotPresent>
				<span class="error">
					<astron:errors property="fieldName"/>
				</span>
			</td>
		</tr>
		<tr>
			<td class="right"><astron:label key="label.opticon.observation.rightascension" /></td>
			<td class="left">
				<html:text property="ra" size="11"/>
				<span class="courier">&nbsp;hh:mm[:ss.ss]&nbsp;</span>
				<span class="error">
					<!--  when the epoch is "Other System" the RA field is not mandatory. So no red star needs to be displayed -->
					<logic:equal name="observationForm" property="requiredTargetRa" value="false">
					   <astron:errors property="ra" ifnotexist=""/>
					</logic:equal>
					<logic:equal name="observationForm" property="epoch" value="Other system">
						<astron:errors property="ra" ifnotexist=""/>
					</logic:equal>
					<logic:equal name="observationForm" property="requiredTargetRa" value="true">
						<logic:notEqual name="observationForm" property="epoch" value="Other system">
							<astron:errors property="ra" ifnotexist="*"/>
						</logic:notEqual>
					</logic:equal>
				</span>
			</td>
		</tr>
		<tr>
			<td class="right"><astron:label key="label.opticon.observation.declination" /></td>
			<td class="left">
				<html:text property="dec" size="11" />
				<span class="courier">&nbsp;[+|-]dd:mm[:ss.s]&nbsp;</span>
				<span class="error">
					<!--  when the epoch is "Other System" the dec field is not mandatory. So no red star needs to be displayed -->
					<logic:equal name="observationForm" property="requiredTargetDec" value="false">
					    <astron:errors property="dec" ifnotexist=""/>
					</logic:equal>
					<logic:equal name="observationForm" property="epoch" value="Other system">
						<astron:errors property="dec" ifnotexist=""/>
					</logic:equal>
					<logic:equal name="observationForm" property="requiredTargetDec" value="true">
						<logic:notEqual name="observationForm" property="epoch" value="Other system">
							<astron:errors property="dec" ifnotexist="*"/>
						</logic:notEqual>
					</logic:equal>
				</span>
			</td>
		</tr>
		<tr>
			<td class="right"><astron:label key="label.opticon.observation.epoch" /></td>
			<td>
				<!--  
					A drop down box is implemented here. This is a way how to do it. But we strongly recommend to
					use the XML option system. Discussed in the opticonobservationgeneral.jsp	
				 -->
				<html:select property="epoch" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
  					<html:option value="J2000"/>
  					<html:option value="B1950"/>
  					<html:option value="Other system"/>
				</html:select>
			</td>
		</tr>

		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>

<tr>
	<td colspan="2">&nbsp;</td>
</tr>
<logic:equal name="observationForm" property="displayTargetMoon" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.moon" /></td>
	<td class="left">
 		<logic:iterate id="moonOption" name="observationForm" property="moonOptions">
        	<html:radio property="selectedTargetMoon" idName="moonOption" value="value">
                 <bean:write name="moonOption" property="label"/>
         	</html:radio>
		 </logic:iterate>
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displaySeeingOptions" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.seeing" /></td>
	<td class="left">
 		<logic:iterate id="seeingOption" name="observationForm" property="seeingOptions">
        	<html:radio property="selectedTargetSeeing" idName="seeingOption" value="value">
                 <bean:write name="seeingOption" property="label"/>
         	</html:radio>
		 </logic:iterate>
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displaySeeingRange" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.seeing.specifics" /></td>
	<td class="left"><astron:label key="label.opticon.observation.lower" />
		<html:text property="targetSeeingLower" size="7"/>
		<astron:label key="label.opticon.observation.upper" />
		<html:text property="targetSeeingUpper" size="7"/>
		<logic:equal name="observationForm" property="requiredSeeingRange" value="true">
			<span class="error"><astron:errors property="targetSeeingUpper" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredSeeingRange" value="false">
			<span class="error"><astron:errors property="targetSeeingUpper" ifnotexist="" /></span>
		</logic:equal>				
	</td>	
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetWater" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.water" /></td>
	<td class="left">
 		<logic:iterate id="waterOption" name="observationForm" property="waterOptions">
        	<html:radio property="selectedTargetWater" idName="waterOption" value="value">
                 <bean:write name="waterOption" property="label"/>
         	</html:radio>
		 </logic:iterate>
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetSn" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.sn" /></td> 
	<td class="left">
		<html:text property="selectedTargetSn" size="16"/>
		<logic:equal name="observationForm" property="requiredSn" value="true">
			<span class="error"><astron:errors property="selectedTargetSn" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredSn" value="false">
			<span class="error"><astron:errors property="selectedTargetSn" ifnotexist="" /></span>
		</logic:equal>			
		<astron:label key="label.opticon.observation.sn.comment" />
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetMagnitude" value="true">
<tr>
	<td class="right">
		<astron:label key="label.opticon.observation.magnitude" />
	</td> 
	<td class="left">
		<html:text property="selectedTargetMagnitude" size="16"/><span class="error"><astron:errors property="selectedTargetMagnitude" ifnotexist="*"/></span>
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetFlux" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.flux" /></td> 
	<td class="left">
		<html:text property="targetFlux" size="16"/>
		<logic:equal name="observationForm" property="requiredTargetFlux" value="true">
			<span class="error"><astron:errors property="targetFlux" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetFlux" value="false">
			<span class="error"><astron:errors property="targetFlux" ifnotexist="" /></span>
		</logic:equal>			

		<logic:equal name="observationForm" property="displayTargetFluxFrequency" value="true">
<!-- 		<tr>
			<td class="right"><astron:label key="label.opticon.observation.fluxfrequency" /></td> 
			<td class="left">  -->
				&nbsp;&nbsp;&nbsp;<astron:label key="label.opticon.observation.fluxfrequency" />&nbsp;<html:text property="targetFluxFrequency" size="16"/>
				<logic:equal name="observationForm" property="requiredTargetFluxFrequency" value="true">
					<span class="error"><astron:errors property="targetFluxFrequency" ifnotexist="*" /></span>
				</logic:equal>
				<logic:equal name="observationForm" property="requiredTargetFluxFrequency" value="false">
					<span class="error"><astron:errors property="targetFluxFrequency" ifnotexist="" /></span>
				</logic:equal>			
<!-- 			</td>
		</tr> -->
		</logic:equal>
	</td>
</tr>
</logic:equal>

<logic:equal name="observationForm" property="displayTargetSpectralIndex" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.spectralindex" /></td> 
	<td class="left">
		<html:text property="targetSpectralIndex" size="16"/>
		<logic:equal name="observationForm" property="requiredTargetSpectralIndex" value="true">
			<span class="error"><astron:errors property="targetSpectralIndex" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetSpectralIndex" value="false">
			<span class="error"><astron:errors property="targetSpectralIndex" ifnotexist="" /></span>
		</logic:equal>			
	</td>
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetSkyQuality" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.sky" /></td> 
	<td class="left">
		<bean:define id="skyQualities" name="observationForm" property="skyQualities" /> 
		<html:select property="selectedTargetSkyQuality">
			<html:options collection="skyQualities" property="value" labelProperty="label" />
		</html:select>
		<logic:equal name="observationForm" property="requiredTargetSkyQuality" value="true">
			<span class="error"><astron:errors property="targetSkyQuality" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetSkyQuality" value="false">
			<span class="error"><astron:errors property="targetSkyQuality" ifnotexist="" /></span>
		</logic:equal>			
	</td>
	
</tr>
</logic:equal>
<logic:equal name="observationForm" property="displayTargetDiameter" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.diameter" /></td> 
	<td class="left">
		<html:text property="targetDiameter" size="16"/>
		<logic:equal name="observationForm" property="requiredTargetDiameter" value="true">
			<span class="error"><astron:errors property="targetDiameter" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetDiameter" value="false">
			<span class="error"><astron:errors property="targetDiameter" ifnotexist="" /></span>
		</logic:equal>	
		<astron:label key="label.opticon.observation.diameter.comment" />			
	</td>
	
</tr>
</logic:equal>

<logic:equal name="observationForm" property="displayTargetStorage" value="true">
<tr>
	<td class="right"><astron:label key="label.opticon.observation.datastorage" /></td> 
	<td class="left">
		<html:text property="targetStorage" size="16"/>
		<logic:equal name="observationForm" property="requiredTargetStorage" value="true">
			<span class="error"><astron:errors property="targetStorage" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetStorage" value="false">
			<span class="error"><astron:errors property="targetStorage" ifnotexist="" /></span>
		</logic:equal>				
	</td>
	
</tr>
</logic:equal>




<logic:equal name="observationForm" property="displaySpecifyTargetSubbandList" value="true">
<!--  target subband list  -->
<tr>
	<td class="right"><astron:label key="label.opticon.observation.specifysubbandlist" /></td> 
	<td class="left">
			<html:radio name="observationForm" property="targetSpecifySubbandList" value="true" onclick="enableSpecifySubbandList(this.form)">Yes</html:radio>
			<html:radio name="observationForm" property="targetSpecifySubbandList" value="false" onclick="disableSpecifySubbandList(this.form)">No</html:radio>		
	</td>
</tr>
</logic:equal>

<logic:equal name="observationForm" property="displayTargetSubbandList" value="true">
<!-- adding the helper functionality -->
<!-- <tr> -->
<%-- 	<td class="right"><astron:label key="label.opticon.observation.centralfrequency" /></td>  --%>
<!-- 	<td class="left"> -->
<!-- 	<div id="displayTargetCentralFrequency"> -->
	
<%-- 		<html:text property="targetCentralFrequency" onkeyup="calculateSubbandList(this.form)" size="16"/> --%>
<%-- 		<logic:equal name="observationForm" property="requiredTargetCentralFrequency" value="true"> --%>
<%-- 			<span class="error"><astron:errors property="targetCentralFrequency" ifnotexist="*" /></span> --%>
<%-- 		</logic:equal> --%>
<%-- 		<logic:equal name="observationForm" property="requiredTargetCentralFrequency" value="false"> --%>
<%-- 			<span class="error"><astron:errors property="targetCentralFrequency" ifnotexist="" /></span> --%>
<%-- 		</logic:equal>	 --%>
<!-- 	</div>	 -->
<!-- 	</td> -->
<!-- </tr> -->


<%-- <tr>
	<td class="right"><astron:label key="label.opticon.observation.bandwidth" /></td> 
	<td class="left">
	<div id="displayTargetBandwidth">
	
		<html:text property="targetBandwidth" onkeyup="calculateSubbandList(this.form)" size="16"/>
		<logic:equal name="observationForm" property="requiredTargetBandwidth" value="true">
			<span class="error"><astron:errors property="targetBandwidth" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetBandwidth" value="false">
			<span class="error"><astron:errors property="targetBandwidth" ifnotexist="" /></span>
		</logic:equal>	
	</div>	
	</td>
</tr> --%>
<!-- </div> -->

<tr>
<td class="left"></td>
<td calss="right" colspan="2">

<logic:messagesNotPresent property="targetsubbandlist"><astron:label key="label.opticon.target.help.calculation.subband" /></logic:messagesNotPresent>

</td>


</tr>
<tr>
<td class="right"></td>
<td calss="left" >
<astron:label key="label.opticon.observation.centralfrequency" />
		
		<html:text property="targetCentralFrequency" onkeyup="calculateSubbandList(this.form)" onchange="calculateSubbandList(this.form)" size="16"/>
		<logic:equal name="observationForm" property="requiredTargetCentralFrequency" value="true">
			<span class="error"><astron:errors property="targetCentralFrequency" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetCentralFrequency" value="false">
			<span class="error"><astron:errors property="targetCentralFrequency" ifnotexist="" /></span>
		</logic:equal>	
		<astron:label key="label.opticon.observation.bandwidth" />
		<html:text property="targetBandwidth" onkeyup="calculateSubbandList(this.form)" onchange="calculateSubbandList(this.form)" size="16"/>
		<logic:equal name="observationForm" property="requiredTargetBandwidth" value="true">
			<span class="error"><astron:errors property="targetBandwidth" ifnotexist="*" /></span>
		</logic:equal>
		<logic:equal name="observationForm" property="requiredTargetBandwidth" value="false">
			<span class="error"><astron:errors property="targetBandwidth" ifnotexist="" /></span>
		</logic:equal>	
		
</td>
</tr>

<tr>
	<td class="right" id="rt"><astron:label key="label.opticon.observation.targetsubbandlist" /></td>
	
	<td class="left">
	<div id="displaydResult">
	<html:text property="targetSubbandList" maxlength="2000" size="16"/>
			<logic:equal name="observationForm" property="requiredTargetSubbandList" value="true">
				<span class="error"><astron:errors property="targetSubbandList" ifnotexist="*"/></span>
			</logic:equal>
			<logic:equal name="observationForm" property="requiredTargetSubbandList" value="false">
				<span class="error"><astron:errors property="targetSubbandList" ifnotexist="" /></span>
			</logic:equal>
			<%-- <div id="subbandResult"><bean:write name="observationForm" property="targetSubbandList" /></div> --%>
		
	</div>
	</td>
	
</tr>



<!--  end target subband list  -->
</logic:equal>


<logic:equal name="observationForm" property="displayTargetOpportunity" value="true">
<tr>
	<td class="right" valign="top"><astron:label key="label.opticon.observation.opportunity" /></td>
	<td class="left"><html:radio styleClass="radio" name="observationForm" property="targetOpportunity" value="true" >Yes</html:radio>
					<html:radio styleClass="radio" name="observationForm" property="targetOpportunity" value="false" >No</html:radio></td>
</tr>							
</logic:equal>	
<tr>
	<td colspan="2">&nbsp;</td>
</tr>

<logic:equal name="observationForm" property="displayTargetSelectRunnumber" value="true">
	<tr>
		<td class="right"><astron:label key="label.opticon.observation.selectrunnumber" /></td> 
		<td class="left">
				<bean:define id="runnumbers" name="observationForm" property="runnumbers" /> 
					<html:select name="observationForm" property="selectedRunnumber" onchange="validateObservingRuns(this.form)">
						<html:options collection="runnumbers" property="value"	labelProperty="label" />
					</html:select>
		</td>
	</tr>
</logic:equal>

<logic:equal name="observationForm" property="displayTargetExposureTime" value="true">
	<tr>
		<td class="right"><astron:label key="label.opticon.observation.exposuretime" /></td>
	   <td class="left"><html:text property="targetExposureTime" size="15" />
			<span class="error"><astron:errors property="targetExposureTime" ifnotexist="*" /></span>
			<astron:label key="label.opticon.observation.exposuretime.comment" />
		</td>
	</tr>
</logic:equal>

<logic:equal name="targetsOnTabbedPage" value="true">
   <tr>
		<td class="right"><astron:label key="label.opticon.observation.runs" /></td> 
		<td class="left">
				<logic:notEmpty name="observationForm" property="observingRuns">
					<logic:equal name="observationForm" property="singleTargetPerRun" value="true">
						<bean:define id="observingRuns" name="observationForm" property="observingRuns" /> 
						<html:select name="observationForm" property="selectedObservingRun">
							<html:options collection="observingRuns" property="value" labelProperty="label" />
						</html:select>
					</logic:equal>	
					<logic:equal name="observationForm" property="singleTargetPerRun" value="false">
						
					</logic:equal>	
				</logic:notEmpty>
		</td>
	</tr>
	
</logic:equal>	

<logic:equal name="observationForm" property="displayTargetSelectPipeline" value="true">
	<tr>
		<td class="right"><astron:label key="label.opticon.observation.selectpipeline" /></td> 
		<td class="left">
				<bean:define id="pipelines" name="observationForm" property="pipelines" /> 
				<html:select name="observationForm" property="selectedPipeline" multiple="true">
					<html:options collection="pipelines" property="value" labelProperty="label" />
				</html:select> (Press ctrl (or cmd on Mac) to link multiple pipelines to this target at once)
		</td>
	</tr>
</logic:equal>	

<tr>
	<td class="right"><astron:label key="label.opticon.observation.comments" /></td> 
	<td class="left">
		<html:text property="selectedTargetComments" size="16"  maxlength="255"/>
	</td>
</tr>
 <tr>
  <td colspan="2">&nbsp;</td>
 </tr>


<logic:equal name="observationForm" property="autoCommit" value="false">
	<TR>
		<TD colspan="2" align="center">
		<logic:equal name="observationForm" property="maxNumberOfTargetsReached" value="true">
			<html:submit disabled="true"  property="newTargetButton" styleClass="list_newobs" title="New Target" >Commit to list of targets</html:submit>&nbsp;
			<html:submit disabled="false"  property="clearTargetButton" styleClass="list_clearobs" title="Clear Target" >Clear target form</html:submit>&nbsp;
             <html:submit disabled="true"  property="newTargetListButton" styleClass="list_uploadlist" title="Upload target list" >Upload a target list</html:submit>    
		</logic:equal>
		<logic:equal name="observationForm" property="maxNumberOfTargetsReached" value="false">
			<html:submit disabled="false"  property="newTargetButton" styleClass="list_newobs" title="New Target"  onclick="setNorthStarCookie(getScrollCoordinates())">Commit to list of targets</html:submit>&nbsp;
			<html:submit disabled="false"  property="clearTargetButton" styleClass="list_clearobs" title="Clear Target"  onclick="setNorthStarCookie(getScrollCoordinates())">Clear target form</html:submit>&nbsp;
         <html:submit disabled="false"  property="newTargetListButton" styleClass="list_uploadlist" title="Upload target list"  onclick="setNorthStarCookie(getScrollCoordinates())">Upload a target list</html:submit>   
		</logic:equal>	
		<!--	<html:submit disabled="false" styleId="deleteAllButton" property="deleteAllTargetButton" styleClass="list_clearobs" title="Delete all targets">Delete all targets</html:submit> -->
			<!-- <html:button property="deleteAllTargetButton" disabled="false" styleId="deleteAllButton" styleClass="list_clearobs" title="Delete all targets" onclick="deleteAllBtn()">Delete all targets</html:button>
			<html:hidden property="deleteAllTargetButton" disabled="false" styleId="deleteAllButton" styleClass="list_clearobs" title="Delete all targets" onclick="deleteAllBtn()"></html:hidden> -->
			<!--<html:button property="deleteAllTargetBtn" styleId="deleteAllButton" styleClass="list_clearobs" title="Delete all targets" onclick="showPopupForDeleteAllConfirm(this.form)">Delete all targets</html:button>
			<!--<html:hidden property="deleteAllTargetButton" disabled="false"/>-->
			<!--<html:submit property="deleteAllTargetButton" disabled="false" styleId="deleteAllButton" styleClass="list_clearobs" title="Delete all targets" onclick="javacsript:return showPopupForDeleteAllConfirm();">Delete all targets</html:submit>-->
			<html:submit property="deleteAllTargetButton" disabled="false" styleId="deleteAllButton" styleClass="list_clearobs" title="Delete all targets" onclick="javascript:return confirm('Are you sure you want to delete all targets?');">Delete all targets</html:submit>
			
			<!--<input type="hidden" name="deleteAllTargetButton" id="deleteAllTargetButton" value="-1">--> 
		</TD>
	</TR>	
</logic:equal>
					</table>
					<tiles:get name="box_footer" />			
				</td>
			</tr>		
					</table>
					<tiles:get name="box_footer" />			
				</td>
			</tr>	
			
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>			
<!-- HERE THE INSTRUMENT PAGE IS INCLUDED -->
	
	<logic:equal name="observationForm" property="showInstrumentBean" value="true">
	    <html:hidden name="observationForm" property="index" />
		<jsp:include page="instrument.jsp" />
					
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		
	</logic:equal> 
				
<!-- 

	BEGIN COMMON CODE
	
 -->
</table>

	<logic:equal name="targetsOnTabbedPage" value="true">
		<jsp:include page="/pages/include/storebuttons.jsp"/>
	</logic:equal>

<!-- Cancel and Ok button -->
<logic:notEqual name="targetsOnTabbedPage" value="true">
	<table width="100%">
		<tr>			
			<td class="left"><html:submit property="saveButton" title="Ok" styleClass="list_accept_yes">Ok</html:submit></td>
				<td class="right"><html:cancel styleClass="list_decline_t" title="Cancel target specification">Cancel</html:cancel></td>
		</tr>		
	</table>
</logic:notEqual>

</html:form>



<script type="text/javascript" src="<%=request.getContextPath()%>/js/ajax.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.10.2.js"></script>
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-ui-1.10.4.min.js"></script>

<script type="text/javascript" language="JavaScript">

	$(document).ready(function () {
    	$("#dialog-confirm-delete-all-targets").dialog({ autoOpen: false });
  	});

		
	function showPopupForDeleteAllConfirm(){
		$("#dialog-confirm-delete-all-targets").dialog({
		      modal: true,
		 	      buttons: {
		        "Yes": function() {
		          	$( this ).dialog( "close" );
		          	console.log('true')
		          	return true;
		        },
		        "No": function() {
		          $( this ).dialog( "close" );
		          console.log('false')
		          return false;
		        }
		      }
		  });
		$("#dialog-confirm-delete-all-targets").dialog('open');
		return false
		

	}
	function deleteAllBtn(){
		console.log(document.getElementsByName('observationForm')[0])
		if(confirm("Do you want to clear all targets?")){
			document.getElementsByName('observationForm')[0].submit();;
		}
	}

	function enableSpecifySubbandList(myform){
		document.getElementById('editSubbandResult').style.display = 'block'; 
    	document.getElementById('subbandResult').style.display = 'none';
    	document.getElementById('displayTargetCentralFrequency').style.display = 'none';
    	document.getElementById('displayTargetBandwidth').style.display = 'none';
	}

	function disableSpecifySubbandList(myform){
		document.getElementById('editSubbandResult').style.display = 'none'; 
    	document.getElementById('subbandResult').style.display = 'block';
    	document.getElementById('displayTargetCentralFrequency').style.display = 'block';
    	document.getElementById('displayTargetBandwidth').style.display = 'block';
	}


	function disableTimeAndRun(){
		//this.form.enableTimeAndObservation
		var etimeobs = document.getElementsByName("enableTimeAndObservation")[0];
		var etimebox = document.getElementsByName("targetExposureTime")[0];
	    var obselect = document.getElementsByName("selectedObservingRun")[0];
	    var isPrimary = document.getElementsByName("targetFirstInRun")[0];

	    if(isPrimary.value == 'true'){
            etimebox.disabled = false;
            obselect.disabled = false;
            isPrimary.value = true;
    	}else{
            etimebox.value="";
            etimebox.disabled = true;
            obselect.options[0].selected = true;
            obselect.disabled = true;
            isPrimary.value = false;
    	}
    	
    	var specifysubbandlist = document.getElementsByName("targetSpecifySubbandList")[0];
    	if(specifysubbandlist.value == 'true' && specifysubbandlist.checked == true){
    		document.getElementById('editSubbandResult').style.display = 'block'; 
    		document.getElementById('subbandResult').style.display = 'none';
    		document.getElementById('displayTargetCentralFrequency').style.display = 'none';
    		document.getElementById('displayTargetBandwidth').style.display = 'none';
    	}else{
    		document.getElementById('editSubbandResult').style.display = 'none'; 
    		document.getElementById('subbandResult').style.display = 'block';
    		document.getElementById('displayTargetCentralFrequency').style.display = 'block';
    		document.getElementById('displayTargetBandwidth').style.display = 'block';
    	}
    	
	}
	
	function setCommitFocus(){
		var button = document.getElementsByName("newTargetButton")[0];
		button.focus();
	}
	addLoadEvent(setCommitFocus());
	addLoadEvent(disableTimeAndRun());
</script>

<div id="dialog-confirm-delete-all-targets" title="Warning">
  <p>Do you want to clear all targets?  </p>
</div>	
<!-- 

	END COMMON CODE
	
 -->
