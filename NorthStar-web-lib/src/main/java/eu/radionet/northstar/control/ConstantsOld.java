// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Jan 4, 2005
 *
 *
 */
package eu.radionet.northstar.control;

/**
 * @author Anton Smit
 */
public abstract class ConstantsOld
{
    public static final String JUSTIFICATION_FILES_PATH = "/PT/files/";



    public static final String FORWARD_PARAM = "?forward=";  

    public static final String NONE = " -- None Specified --";


 
 
	public static final String FORWARD_URI = "forwardUri";


    public static final String SELECTED_PROPOSAL = "proposal";
    public static final String PROPOSAL_ID = "proposalId";

    public static final String JUSTIFICATION_FILE = "justification_file";



	public static final String INVITATIONS = "invitations";
    //errors



    // control

    public static final String CONTINUE = "continue";


    public static final String DATETIME_FORMAT = "yyyy/MM/dd HH:mm z";   
    public static final String TIME_FORMAT = "HH:mm";  
    public static final String SHORT_TIME_FORMAT = "HH";
    public static final String DATE_FORMAT = "yyyy/MM";   


    public static final String SAVE = "save";

    public static final String APPLICANTS_PATH = "/processApplicants.do";
    public static final String OBSERVING_REQUEST_PATH = "observingrequest.do";



    public static final String EPOCH_J1950 = "J1950";
 

    
    
    
    // WHT/INT telescopes
    public static final String NIGHT = "Night";
    public static final String WEEK = "Week";
    
    

//    public static final String STATUS_SUBMITTED = "submitted";
//    public static final String STATUS_IN_PREPARATION = "in preparation";
//    public static final String STATUS_UNDER_REVIEW = "under review";
//    public static final String CATEGORY_URGENT = "urgent";
//    public static final String CATEGORY_REGULAR = "regular";
//    public static final String DEADLINE_PASSED = "deadlinepassed";
//    public static final String DEADLINE_NOT_PASSED = "deadlinenotpassed";
    public static final String REDSHIFT = "Redshift";

    
    //public static final String ANY = eu.radionet.northstar.data.Constants.ANY;
    public static final String DARK = "Dark";
    public static final String BRIGHT = "Bright";
    public static final String GREY = "Grey";
    

    public static final String[] STATUSES = {"in preparation","submitted","under review"};
    
    
}
