// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Jan 17, 2005
 *
 *
 */
package eu.radionet.northstar.control.proposal.observingrequest.observation;

/**
 * @author Anton Smit
 */
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.ConfigurationUtil;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.control.proposal.observingrequest.ValueBean;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Target;

public class SetUpObservationAction extends LockedAction {

	protected Log log = LogFactory.getLog(this.getClass());
	protected ContextType contextConfiguration = null;
	protected HttpServletRequest request = null;
	
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		this.request = request;
		HttpSession session = request.getSession();
		ObservationForm observationForm = (ObservationForm) form;

		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
		contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());
		/* This method can be overwritten in the telescopedependant code */
		performFormInitialisation(request, observationForm);
		
		/*
		 * The loading of observations is only needed when targets are on the same page
		 */
		Observation selectedObservation =null;
		ObservingRequest observingRequest = proposalData.getProposal().getObservingRequest();
		if (!telescopeConfiguration.isTargetsOnTabbedPage()){
			String indexParam = request.getParameter("index");
			selectedObservation=loadObservation(observationForm,proposalData.getProposal(),indexParam);
			if(session.getAttribute(Constants.OBSERVATION)==null){
				// this attribute will only be cleaned after ok or cancel button has been pressed.
				Observation newObservation = (Observation) deepCopy(selectedObservation);
				session.setAttribute(Constants.OBSERVATION, newObservation);
			}
		}

		if (observationForm.isUploadTargetList()){
			return mapping.findForward(Constants.UPLOAD_TARGET_LIST);
		}
		// upload was successful
		if (session.getAttribute(Constants.NEW_TARGETS)!= null)
		{
			//TODO insert targets to observingrequest..
			List nTargets = (List) session.getAttribute(Constants.NEW_TARGETS);
			observingRequest.getTargets().addAll(nTargets);
			processTargets(nTargets,observingRequest);
			//observationForm.getTempTargets().addAll(nTargets);
			//observationForm.getTargetSummaryValues().addAll((List) session.getAttribute(Constants.NEW_TARGETS));
			session.removeAttribute(Constants.NEW_TARGETS);
		}
		
		/*
		 * fill dropdown lists
		 */
		fillTargetOptions(request, observationForm);
		
		fillTargets(observingRequest, observationForm,selectedObservation);
		fillTargetSection(observingRequest, observationForm);
		observationForm.resetButtons();
		
		// set the current action attribute so the layout can see that this page has been selected
		request.setAttribute(Constants.CURRENT_ACTION, request.getServletPath());

		// the following option will load the proposal tabbed layout instead of the normal one
		// it looks simple, but was a lot of work to accomplish
		if (telescopeConfiguration.isTargetsOnTabbedPage()){
			return mapping.findForward("targets");
		}else{
			return mapping.findForward(Constants.SUCCESS);
		}
	}
	
	protected void processTargets(List nTargets, ObservingRequest observingRequest) {
		// TODO Auto-generated method stub
		
	}

	private Observation loadObservation(ObservationForm observationForm,
			Proposal proposal, String indexParam) throws DatabaseException {
		if (AstronValidator.isPositiveInt(indexParam)) 
		{
			//observationForm.reset();
			ObservingRequest observingRequest = proposal.getObservingRequest();
			int index = AstronConverter.toInteger(observationForm.getIndex()).intValue();
			
			if (observingRequest.getObservations().size() > index) 
			{
				Observation observation = (Observation) observingRequest.getObservations().get(index);
				if (observation != null)
				{
					fillObservationOptions(observation, observationForm);
					fillObservationForm( observation, observationForm);
					return observation;
				}
			}
		}else if (!AstronValidator.isBlankOrNull(indexParam)){
			// negative index, means new observation?
			if (indexParam.equals("new") || indexParam.equals(Constants.UPLOAD_TARGET_LIST)){
				observationForm.reset();
				if (indexParam.equals(Constants.UPLOAD_TARGET_LIST)){
					observationForm.setUploadTargetList(true);
				}
			}
			ObservingRequest observingRequest = proposal.getObservingRequest();
			ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
			TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
			
			 
			try {
				Observation newObservation = configurationUtil.getObservation(telescopeConfiguration);
				
				// fillObservationOptions
				fillObservationOptions(newObservation, observationForm);
				List obs = observingRequest.getObservations();
				obs.add(newObservation);
				String index = new Integer(obs.size()-1).toString();
				observationForm.setIndex(index);
				return newObservation;
				
			} catch (InvalidConfigurationException e) {
				log.warn("could not generate new observation with telescope configuration: "+telescopeConfiguration);
			}
		
		} 
		return null;
	}

	public void fillObservationForm(Observation observation, ObservationForm observationForm) 
		throws DatabaseException
	{
		observationForm.setRequiredSchedConstraints(observation.getRequiredSchedConstraints());
		observationForm.setPreferredSchedConstraints(observation.getPreferredSchedConstraints());
	}
	
	public void fillTargetFields(int targetId, ObservingRequest observingRequest, ObservationForm observationForm)
		throws DatabaseException
	{
		/* Overwritten in telescope specific module */
	}
	
	public void fillTargets(ObservingRequest observingRequest, ObservationForm observationForm, Observation selectedObservation) 
	throws DatabaseException
	{
	
	}

	public void fillTargetOptions(HttpServletRequest request, ObservationForm observationForm) throws DatabaseException
	{
		fillOptions(observationForm);
	}

	public void fillOptions(ObservationForm observationForm) throws DatabaseException
	{
		
	}
	
	
	public void fillObservationOptions(Observation observation, ObservationForm observationForm) throws DatabaseException
	{
		
	}

	public void fillTargetSection(ObservingRequest observingRequest, ObservationForm observationForm) throws DatabaseException
	{
        if ((observationForm.getSelectedTargetId() >= 0) &&
                (observationForm.getEditTargetButton(observationForm.getSelectedTargetId()) != null))
        {
            // It is assumed that an observation exists
            fillTargetFields(observationForm.getSelectedTargetId(), observingRequest, observationForm);
        }
        else
        {
        	fillTargetFields(-1, observingRequest, observationForm);
        }
	}
	
	/**
	 * TODO: This function is also defined in process!
	 * @param targetValues
	 * @param index
	 * @return
	 */
	protected String targetValue(List targetValues, int index)
	{
		if (targetValues.get(index)== null)
		{
			return null;
		}
		else
		{
			return ((ValueBean) targetValues.get(index)).getValue();
		}
	}

	/**
	 * This method is called everytime SetUpObservation is requested, it can be overwritten in 
	 * the telescope dependant code
	 * @param request
	 * @param observationForm
	 */
	protected void performFormInitialisation(HttpServletRequest request, 
			ObservationForm observationForm)
	{
		
	}

	/**
	 * This method sets default values in the form
	 * @param request
	 * @param observationForm
	 */
	protected void setDefaults(ObservationForm observationForm)
		throws DatabaseException
	{
		
	}
	/**
	 *  ugly solution to store the object for a cancel action.
	 *  is it better to regenerate the new object via the form?
	 *  this is only used for separate observation page.
	 * @param oldObj
	 * @return newObj
	 * @throws Exception
	 */
	static public Object deepCopy(Object oldObj) throws Exception
	   {
	      ObjectOutputStream oos = null;
	      ObjectInputStream ois = null;
	      try
	      {
	         ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
	         oos = new ObjectOutputStream(bos); 
	         // serialize and pass the object
	         oos.writeObject(oldObj);   
	         oos.flush();               
	         ByteArrayInputStream bin = 
	               new ByteArrayInputStream(bos.toByteArray()); 
	         ois = new ObjectInputStream(bin);                  
	         // return the new object
	         return ois.readObject(); 
	      }
	      catch(Exception e)
	      {
	         throw(e);
	      }
	      finally
	      {
	         oos.close();
	         ois.close();
	      }
	   }
		
}