// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessDownloadAction.java 
 *
 * Created on Feb 9, 2005
 *
 * Version $Id: ProcessAcceptedInvitationsAction.java,v 1.2 2006-05-16 12:21:22 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.admin.invitation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.control.Constants;

/**
 * The ProcessDownloadAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessAcceptedInvitationsAction extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		NorthStarDelegate northStarDelegate = NorthStarDelegate.getInstance();
		UserAccount maintenanceAccount = northStarDelegate
		.getUserAccountByName(NorthStarConfiguration
				.getMaintenanceAccount());
		CollaborationDelegate collaborationDelegate = CollaborationDelegate.getInstance();
		collaborationDelegate.processAllAcceptedProposals(session.getId(),maintenanceAccount);
		return mapping.findForward(Constants.SUCCESS);
	}
}
