// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Dec 20, 2005
 *
 *
 */
package eu.radionet.northstar.control.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.business.configuration.options.definition.DoubleType;
import eu.radionet.northstar.business.configuration.options.definition.FieldType;
import eu.radionet.northstar.business.configuration.options.definition.IntegerType;
import eu.radionet.northstar.business.configuration.options.definition.OptionListType;
import eu.radionet.northstar.business.configuration.options.definition.StringType;
import eu.radionet.northstar.business.configuration.options.definition.TextType;
import eu.radionet.northstar.business.configuration.options.definition.ValidationType;
import eu.radionet.northstar.control.Constants;

public class OptionsUtils {
	
	/**
	 * The method is used to by-pass the options system. It just makes a bean/value pair 
	 * from a list of Strings
	 * @param stringValues
	 * @return
	 */
	public static List getLableValueBeansFromStringList(List stringValues) {
		List beans = new ArrayList();
		beans.add(new LabelValueBean(Constants.NONE_SPECIFIED_LABEL, 
					Constants.NONE_SPECIFIED_VALUE));
		Iterator stringsIt = stringValues.iterator();
		while (stringsIt.hasNext()) {
			String value = (String) stringsIt.next();
			beans.add(new LabelValueBean(value, value));
		}
		return beans;
	}
	
	
	public static List getLabelValueBeans(String name, Map requiredValues,
			ContextType contextConfiguration) {
		List beans = new ArrayList();
		beans.add(new LabelValueBean(Constants.NONE_SPECIFIED_LABEL,
				Constants.NONE_SPECIFIED_VALUE));
		List optionFieldConfigurations = OptionsDelegate
				.getOptionsConfigurationTypes(name, requiredValues,
						contextConfiguration);
		Iterator optionIterator = optionFieldConfigurations.iterator();

		while (optionIterator.hasNext()) {
			ConfigureOptionType optionFieldConfiguration = (ConfigureOptionType) optionIterator
					.next();
			String label = optionFieldConfiguration.getLabel();
			String value = optionFieldConfiguration.getValue();
			if (AstronValidator.isBlankOrNull(label)) {
				label = value;
			}
			beans.add(new LabelValueBean(label, value));
		}
		return beans;
	}
	
	public static String getValue(String name, ContextType contextConfiguration) {
		return getValue(name,new HashMap(),contextConfiguration);
	}
	
	
	public static String getValue(String name, Map requiredValue,ContextType contextConfiguration) {
		String result = null;
		List optionFieldConfigurations = OptionsDelegate
				.getOptionsConfigurationTypes(name, requiredValue,
						contextConfiguration);
		Iterator optionIterator = optionFieldConfigurations.iterator();

		if (optionIterator.hasNext()) {
			ConfigureOptionType optionFieldConfiguration = (ConfigureOptionType) optionIterator
					.next();
			String label = optionFieldConfiguration.getLabel();
			String value = optionFieldConfiguration.getValue();
			result = value;
			
		}
		return result;
	}
	

	public static List getLabelValueBeansWithoutNoneSpecified(String name,
			Map requiredValues, ContextType contextConfiguration) {
		List beans = new ArrayList();
		List optionFieldConfigurations = OptionsDelegate
				.getOptionsConfigurationTypes(name, requiredValues,
						contextConfiguration);
		Iterator optionIterator = optionFieldConfigurations.iterator();

		while (optionIterator.hasNext()) {
			ConfigureOptionType optionFieldConfiguration = (ConfigureOptionType) optionIterator
					.next();
			String label = optionFieldConfiguration.getLabel();
			String value = optionFieldConfiguration.getValue();
			
			if (AstronValidator.isBlankOrNull(label)) {
				label = value;
			}
			beans.add(new LabelValueBean(label, value));
		}
		return beans;
	}

	public static List getLabelValueBeansFromStringList(List strings) {
		List beans = new ArrayList();
		beans.add(new LabelValueBean(Constants.NONE_SPECIFIED_LABEL,
				Constants.NONE_SPECIFIED_VALUE));
		Iterator stringsIt = strings.iterator();
		while (stringsIt.hasNext()) {
			String string = (String) stringsIt.next();
			beans.add(new LabelValueBean(string, string));
		}
		return beans;
	}

	public static List getLabelValueBeansFromStringListWithoutNoneSpecified(
			List strings) {
		List beans = new ArrayList();
		Iterator stringsIt = strings.iterator();
		while (stringsIt.hasNext()) {
			String string = (String) stringsIt.next();
			beans.add(new LabelValueBean(string, string));
		}
		return beans;
	}

	/**
	 * Get label of a option
	 * 
	 * @param name
	 *            name of the optionlist
	 * @param value
	 *            value of a option
	 * @param contextConfiguration
	 * @return Label
	 */
	public static String getLabel(String name, String value,
			ContextType contextConfiguration) {
		return OptionsDelegate.getLabel(name, value, contextConfiguration);
	}

	public static String getLabel(List labelValueBeans, String value) {
		if (AstronValidator.isBlankOrNull(value)) {
			if (labelValueBeans.size() > 0) {
				LabelValueBean labelValueBean = (LabelValueBean) labelValueBeans
						.get(0);
				return labelValueBean.getLabel();
			}
		}
		Iterator labelValueBeansIterator = labelValueBeans.iterator();
		while (labelValueBeansIterator.hasNext()) {
			LabelValueBean labelValueBean = (LabelValueBean) labelValueBeansIterator
					.next();
			if (labelValueBean.getValue().equals(value)) {
				return labelValueBean.getLabel();
			}
		}
		if (labelValueBeans.size() > 0) {
			LabelValueBean labelValueBean = (LabelValueBean) labelValueBeans
					.get(0);
			return labelValueBean.getLabel();
		}
		return null;
	}

	public static String getDefaultSelectedOptionOrValue(String optionName,
			String value, ContextType contextConfiguration)
			throws DatabaseException {

		if (value == null) {
			return OptionsDelegate.getDefaultOption(optionName,
					contextConfiguration);
		}
		return value;
	}

	public static LabelValueBean getFirstOption(List labelValueBeans) {
		Iterator labelValueBeansIterator = labelValueBeans.iterator();
		if (labelValueBeansIterator.hasNext()) {
			return (LabelValueBean) labelValueBeansIterator.next();
		}
		return new LabelValueBean();

	}

	public static List getList(String value) {
		List result = new ArrayList();
		result.add(value);
		return result;
	}

	public static List getList(String[] values) {
		List result = new ArrayList();
		if (values != null && values.length > 0) {
			for (int i = 0; i < values.length; i++) {
				result.add(values[i]);
			}
		}
		return result;
	}

	public static String[] getArray(List values) {
		String[] result = new String[values.size()];
		for (int i = 0; i < values.size(); i++) {
			result[i] = (String) values.get(i);
		}
		return result;
	}

	public static void validate(String name, String value,
			FieldsDefinitionType fieldsDefinitionType, ActionMessages errors,
			String errorName) {
		FieldType fieldType = (FieldType) fieldsDefinitionType.getFields().get(
				name);
		if (fieldType != null) {
			ValidationType validationType = fieldType.getValidation();
			if (fieldType.getClass().equals(DoubleType.class)) {
				if (!OptionsDelegate.isValid(name, value,
						(DoubleType) fieldType)) {
					if (validationType != null) {
						if (validationType.getLowerBoundary() != null
								&& validationType.getUpperBoundary() != null) {
							errors.add(errorName, new ActionMessage(
									"error.invalid.double.range",
									validationType.getLowerBoundary(),
									validationType.getUpperBoundary()));
						} else if (validationType.getLowerBoundary() != null
								) {
							errors.add(errorName, new ActionMessage(
									"error.invalid.double.range.upper",
									validationType.getLowerBoundary()));
						} else if (validationType.getUpperBoundary() != null
								) {
							errors.add(errorName, new ActionMessage(
									"error.invalid.double.range.lower",
									validationType.getUpperBoundary()));
						}
					} else {
						errors.add(errorName, new ActionMessage(
								"error.invalid.double"));
					}
				}
			} else if (fieldType.getClass().equals(IntegerType.class)) {
				if (!OptionsDelegate.isValid(name, value,
						(IntegerType) fieldType)) {
					if (validationType != null) {
						if (validationType.getLowerBoundary() != null
								&& validationType.getUpperBoundary() != null) {
							errors.add(errorName, new ActionMessage(
									"error.invalid.integer.range",
									validationType.getLowerBoundary(),
									validationType.getUpperBoundary()));
						} else if (validationType.getLowerBoundary() != null
								) {
							errors.add(errorName, new ActionMessage(
									"error.invalid.integer.range.upper",
									validationType.getLowerBoundary()));
						} else if (validationType.getUpperBoundary() != null
								) {
							errors.add(errorName, new ActionMessage(
									"error.invalid.integer.range.lower",
									validationType.getUpperBoundary()));
						}
					} else {
						errors.add(errorName, new ActionMessage(
								"error.invalid.integer"));
					}
				}
			} else if (fieldType.getClass().equals(StringType.class)) {

			} else if (fieldType.getClass().equals(TextType.class)) {

			} else if (fieldType.getClass().equals(OptionListType.class)) {

			}
		}

	}

}
