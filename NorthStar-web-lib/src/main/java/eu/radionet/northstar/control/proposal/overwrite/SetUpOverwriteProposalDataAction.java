// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SetUpUploadAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: SetUpOverwriteProposalDataAction.java,v 1.1 2006-05-11 12:16:44 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.overwrite;

import java.util.Iterator;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.RegisteredMember;

/**
 * The SetUpUploadAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class SetUpOverwriteProposalDataAction extends Action {
	private Log log = LogFactory.getLog(this.getClass());



	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		OverwriteProposalDataForm overwriteProposalDataForm = (OverwriteProposalDataForm) form;
		HttpSession session = request.getSession();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}

		
		ProposalData oldProposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		ProposalData newProposalData = (ProposalData) session.getAttribute(Constants.NEW_PROPOSAL_DATA);
		overwriteProposalDataForm.setOldProposal(getProposalBean(oldProposalData.getProposal()));
		overwriteProposalDataForm.setNewProposal(getProposalBean(newProposalData.getProposal()));

		return mapping.findForward(Constants.SUCCESS);	

	}

	protected ProposalBean getProposalBean(Proposal proposal){
		ProposalBean proposalBean = new ProposalBean();
		if (proposal.getJustification()!=null){
			proposalBean.setTitle(proposal.getJustification().getTitle());
		}
		proposalBean.setCode(proposal.getCode());
		proposalBean.setSemester(proposal.getSemester().getSemester());
		proposalBean.setCategory(proposal.getSemester().getCategory().getCode());
		proposalBean.setTelescope(proposal.getSemester().getTelescope());
		proposalBean.setCommunity(NorthStarConfiguration.getCommunityDescription(proposal.getSemester().getCommunity()));
		Iterator members = proposal.getMembers().iterator();
		/*
		 * if proposal has members
		 */
		while (members.hasNext()) {
			Member member = (Member) members.next();
			/*
			 * if member is an registered member
			 */
			if (member.getClass().equals(RegisteredMember.class) && member.isContactAuthor()) {
				RegisteredMember registeredMember = (RegisteredMember) member;
				proposalBean.setContactAuthor(registeredMember
						.getUser().getLastName());

			} 
		}
		return proposalBean;
	}


}
