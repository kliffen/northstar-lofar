ALTER TABLE 'lofar_proposal_astron_nl_northstar'.'opticoninstrument'
ADD COLUMN 'store_uv_data' TINYINT(1)  NOT NULL DEFAULT 0 ,
ADD COLUMN 'tbb_exposure_time' double  DEFAULT null ,
ADD COLUMN 'tbb_trigger_length' double  DEFAULT null,
ADD COLUMN 'tbb_trigger_rate' int(11)   DEFAULT 0 ,
ADD COLUMN 'tbb_sa_observation' VARCHAR(255)  DEFAULT NULL ,
ADD COLUMN 'tbb_trigger_source' VARCHAR(255)  DEFAULT NULL ;