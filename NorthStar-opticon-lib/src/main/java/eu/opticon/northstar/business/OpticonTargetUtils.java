package eu.opticon.northstar.business;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.exception.ParseTargetListException;

public class OpticonTargetUtils {

	public static BigDecimal convertToTB(BigDecimal bd) {
		// TODO Auto-generated method stub
		if(bd ==null)
			return new BigDecimal(0);
		bd = bd.divide(new BigDecimal("1000"));
		bd = bd.setScale(2,BigDecimal.ROUND_UP);
		return bd;
	}

	public static List sortTragetbyRunnumber(List targets) {
		List targets2 = targets;
		Collections.sort(targets2, new Comparator<OpticonTarget>() {
			public int compare(OpticonTarget o1, OpticonTarget o2) {
				if (o1 == null || o2 == null) {
					return 0;
				}
				if (o1.getRunnumber() == null) {
					o1.setRunnumber("1");
				}
				if (o2.getRunnumber() == null) {
					o2.setRunnumber("1");
				}
				if ( new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber())) == 0) {
					//return -1;
					return 1;
				}
				if (o2.isFirstInRun()
						&& new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber())) == 0) {
					return 1;
				}
				return new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber()));
			}
		});

		return targets2;
	}

	public static List getTargetsonSameRunNumber(List targets, int runnumber) {

		targets = sortTragetbyRunnumber(targets);
		
		ArrayList targetsonSameRunnumber = new ArrayList();
		Iterator targetIterator = targets.iterator();
		while (targetIterator.hasNext()) {
			
			OpticonTarget opticonTargert = (OpticonTarget) targetIterator.next();
			if(Integer.parseInt(opticonTargert.getRunnumber()) == runnumber)
				targetsonSameRunnumber.add(opticonTargert);
		}
		
		return targetsonSameRunnumber;
	}

	public static boolean validateTotalSubBandListForSameRunnumberPdf(
			int selectedRunnumber, List targets, int maxTargetOnSameRun)
			throws ParseTargetListException {
		
		int totalSubBand = 0;
		Iterator targetIterator = targets.iterator();
		while (targetIterator.hasNext()) {

			OpticonTarget target = (OpticonTarget) targetIterator.next();
			String subbandlist = target.getSubbandList();

			totalSubBand += getNumberofSubBands(subbandlist); // setupinstrument
																// convertsubbanddescription
		}
	
		if (totalSubBand <= maxTargetOnSameRun) {
			return true;
		} else
			return false;
	}

	public static int getNumberofSubBands(String subbandList)
			throws ParseTargetListException {
		int result = 0;
		List<String> parts = new ArrayList();
		if (subbandList != null && subbandList.length() > 0) {
			String[] pars = subbandList.split(",");
			for (int i = 0; i < pars.length; i++) {
				parts.add(pars[i]);
				if((!pars[i].contains("..")) && (!pars[i].contains("-")))
					result++;
			}
		}

		for (String listPart : parts) {
			if (listPart.contains("-") || listPart.contains("..")) {
				String[] splitparts;
				if (listPart.contains("-")) {
					splitparts = listPart.split("-");
				} else {
					splitparts = listPart.split("\\.\\.");
				}
				String begin = splitparts[0]; // 004
				String end = splitparts[1]; // 034556

				try {
					if (Integer.valueOf(begin).intValue() < Integer.valueOf(end).intValue()) { 
						result += (Integer.valueOf(end).intValue() - Integer.valueOf(begin).intValue()) + 1;
					}
				} catch (NumberFormatException nex) {
					throw new ParseTargetListException("invalid Subband List");
				}
			}
		}
		
		return result;
	}

	public static boolean validateTotalSubBandListForSameRunnumber(
			String selectedRunnumber, List targets, String maxtargetSameRun)
			throws ParseTargetListException {
		// TODO Auto-generated method stub
		if (targets == null || targets.size() == 1)
			return true;

		int numberofselectedSubBands = 0;

		boolean ifInt = checkInteger(selectedRunnumber, "selectedRunnumber");
		if (!ifInt)
			return false;
		int runnumber = Integer.parseInt(selectedRunnumber);

		numberofselectedSubBands += getNumberofSubbandforSelectedRunNumber(
				runnumber, targets);

		
		int maxVal=0;
		if(!AstronValidator.isBlankOrNull(maxtargetSameRun))
		{
			maxVal =Integer.parseInt(maxtargetSameRun.trim());
		}
		
		if (numberofselectedSubBands <= maxVal)
		return true;
		else
			return false;
	}

	private static int getNumberofSubbandforSelectedRunNumber(int runnumber,
			List targets) throws ParseTargetListException {
		int subbands = 0;
		if (targets == null && targets.size() <= 0) {
			
			return subbands;
		}
		if (targets.size() <= 0)
			return 0;
		
		targets = getTargetsonSameRunNumber(targets, runnumber);
		
		Iterator trIterator = targets.iterator();
		while (trIterator.hasNext()) {
			try {
				OpticonTarget opticonTarget = (OpticonTarget) trIterator.next();
				subbands += getNumberofSubBands(opticonTarget.getSubbandList());
				
			} catch (ParseTargetListException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				throw e;
			}

		}
		return subbands;
	}

	public static boolean validateExposureTime(List targetsOnSameRunnumber) {
		// TODO Auto-generated method stub

		Iterator trIterator = targetsOnSameRunnumber.iterator();
		OpticonTarget opticonTarget = (OpticonTarget) trIterator.next();
		OpticonTarget firstopticonTarget = opticonTarget;
		Double totalExposureTime = NorthStarDelegate.getResourceValue(
				firstopticonTarget.getAllocations(),
				OpticonConstants.TOTAL_DURATION);
		if(totalExposureTime==null && firstopticonTarget.isFirstInRun())
			return false;
		while (trIterator.hasNext()) {
			opticonTarget = (OpticonTarget) trIterator.next();
			Double totalET = NorthStarDelegate.getResourceValue(
					opticonTarget.getAllocations(),
					OpticonConstants.TOTAL_DURATION);
			
			
			if (totalExposureTime != null && totalET!=null
					&& totalExposureTime.doubleValue() != totalET.doubleValue())
				return false;
		}
		return true;
	}

	public static boolean checkInteger(String value, String name) {

		if (!AstronValidator.isBlankOrNull(value)) {
			try {
				if (Integer.valueOf(value).intValue() <= 0) {
					return false;
				}
			} catch (NumberFormatException e) {

				return false;
			}
		}
		return true;

	}
	
	
	public static String getHoursFromSeconds(Double seconds) {
		if (seconds != null) {
			double hours = seconds.doubleValue() / 3600.0;
			NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(2);
			return nf.format(hours) ;
		} else {
			return null;
		}
	}

}
