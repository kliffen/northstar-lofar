// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.util;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.w3c.dom.Document;

import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.data.entities.Proposal;

public class NorthStarUtils implements Serializable {
	protected static Log log = LogFactory.getLog(NorthStarUtils.class);

	private static final String NORTHSTAR_CONFIG = "northstar-config";
	private static final String NORTHSTAR_CONFIG_DEFAULT_URI = "conf/host/northstar-config.xml";

	private static final String HTTPS = "https";

	private static final String HTTP = "http";

	protected static String CATALINA_HOME = "${catalina.home}";
	private static Map translations=new HashMap();

	public static String read(String fileName, ServletContext servletcontext)
			throws IOException {
		if (startsWith(fileName, CATALINA_HOME)) {
			return read(fileName);
		} else if (startsWith(fileName, File.separator)) {
			return readFromContext(fileName, servletcontext);
		} else {
			return readFromContext(File.separator + fileName, servletcontext);
		}
	}

	protected static boolean startsWith(String fileName, String startsWith) {
		if (!AstronValidator.isBlankOrNull(fileName)) {
			return fileName.startsWith(startsWith);
		} else {
			return false;
		}
	}

	protected static String readFromContext(String fileName,
			ServletContext servletcontext) throws IOException {
		InputStream headerInputStream = servletcontext
				.getResourceAsStream(fileName);
		StringBuffer sb = new StringBuffer();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				headerInputStream));
		String s;
		while ((s = in.readLine()) != null) {
			sb.append(s);
			sb.append("\n");
		}
		in.close();
		return sb.toString();
	}

	public static String read(String fileName) throws IOException {
		if (startsWith(fileName,CATALINA_HOME)){
			fileName = fileName.substring(CATALINA_HOME.length());
	        String catalinaHome = System.getProperty("catalina.home");
			fileName = catalinaHome+ fileName;
		}
		File file = new File(fileName);

		InputStream headerInputStream = new FileInputStream(file);
		StringBuffer sb = new StringBuffer();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				headerInputStream));
		String s;
		while ((s = in.readLine()) != null) {
			sb.append(s);
			sb.append("\n");
		}
		in.close();
		return sb.toString();
	}

	public static String getLinkPath(HttpServletRequest request) {
		String serverPath = request.getScheme() + "://"
				+ request.getServerName();
		if (request.getScheme().equals(HTTP) && request.getServerPort() != 80) {
			serverPath += ":" + request.getServerPort();
		} else if (request.getScheme().equals(HTTPS)
				|| request.getServerPort() != 443) {
			serverPath += ":" + request.getServerPort();
		}
		String forwardPath = serverPath
				+ "/useradministration/user/forward.do?forward=" + serverPath;
		forwardPath += request.getContextPath()
				+ "/setUpAddRegisteredMember.do?key=";
		return forwardPath;
	}

	public static void loadNorthStarConfiguration(ServletContext servletcontext) throws Exception{
		
		String northstarConfigUri = servletcontext.getInitParameter(NORTHSTAR_CONFIG);
		
		if (northstarConfigUri == null) {
			northstarConfigUri = servletcontext.getRealPath("/") + NORTHSTAR_CONFIG_DEFAULT_URI;
			log.warn("No configuration file defined through " + NORTHSTAR_CONFIG + " parameter in context.xml");
			log.warn("Using default configuration in: " + northstarConfigUri);
			log.warn("This should not be used in a production environment");
		}
		
		String northstarConfig = NorthStarUtils.read(northstarConfigUri);

		XMLConverter xmlConverter = XMLConverter.getInstance();
		Document northstarConfigDocument = xmlConverter
				.convertStringToDocument(northstarConfig);
		Configurator configurator = new Configurator();
		log.info("parsing file: "+northstarConfigUri);
		configurator.configure(northstarConfigDocument, servletcontext);

	}
	public static String getUriPath(HttpServletRequest request) {
		String serverPath = request.getScheme() + "://"
				+ request.getServerName();
		if (request.getScheme().equals(HTTP) && request.getServerPort() != 80) {
			serverPath += ":" + request.getServerPort();
		} else if (request.getScheme().equals(HTTPS)
				|| request.getServerPort() != 443) {
			serverPath += ":" + request.getServerPort();
		}
		return serverPath + request.getContextPath();
	}

	public static boolean exists(String fileName) throws IOException {
		File file = new File(fileName);
		return file.exists();
	}

	public ActionMessages storeJustificationFiles(
			ProposalDelegate proposalDelegate, HttpServletRequest request,
			ProposalData proposalData) throws DatabaseException {
		ActionMessages errors = new ActionMessages();
		Proposal proposal = proposalData.getProposal();
		if (proposalData.getScientificFile() != null) {
			proposalData.getScientificFile().setJustificationId(
					proposal.getJustification().getId());
			/*
			 * store proposal to the database
			 */
			try {
				proposalDelegate.store(proposalData.getScientificFile());
			} catch (DatabaseException de) {
				log.error("Cannot store scientificFile: " + de.getMessage());
				errors.add(Constants.ERROR, new ActionMessage(
						"error.database.store.justificationfile"));
				return errors;
			}

			proposalData.setScientificFile(null);
		}

		if (proposalData.isDeleteTechnicalDetailsFile()) {
			proposalDelegate.deleteTechnicalDetailsFile(proposalData
					.getProposal().getJustification());
			proposalData.setDeleteTechnicalDetailsFile(false);
			proposalData.setTechnicalDetailsFile(null);
		}
		if (proposalData.getTechnicalDetailsFile() != null) {
			proposalData.getTechnicalDetailsFile().setJustificationId(
					proposal.getJustification().getId());
			/*
			 * store proposal to the database
			 */
			try {
				proposalDelegate.store(proposalData.getTechnicalDetailsFile());
			} catch (DatabaseException de) {
				log.error("Cannot store technicalDetailsFile: "
						+ de.getMessage());
				errors.add(Constants.ERROR, new ActionMessage(
						"error.database.store.justificationfile"));
				return errors;
			}

			proposalData.setTechnicalDetailsFile(null);
		}
		if (proposalData.isDeleteFigureFile()) {
			proposalDelegate.deleteFigureFile(proposalData.getProposal()
					.getJustification());
			proposalData.setDeleteFigureFile(false);
			proposalData.setFigureFile(null);
		}
		if (proposalData.getFigureFile() != null) {
			proposalData.getFigureFile().setJustificationId(
					proposal.getJustification().getId());
			/*
			 * store proposal to the database
			 */
			try {
				proposalDelegate.store(proposalData.getFigureFile());
			} catch (DatabaseException de) {
				log.error("Cannot store figureFile: " + de.getMessage());
				errors.add(Constants.ERROR, new ActionMessage(
						"error.database.store.justificationfile"));
				return errors;
			}

			proposalData.setFigureFile(null);
		}
		return errors;
	}

	/**
	 * The ps2pdf converts postscript to pdf with GNU Ghostscript
	 * 
	 * @param postscriptByteArray
	 * @param sessionId
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	public byte[] ps2pdf(byte[] postscriptByteArray, String sessionId) {
		try {
			/*
			 * get runtime
			 */
			Runtime rt = Runtime.getRuntime();
			/*
			 * create temp file
			 */
			File postscriptFile = File.createTempFile(sessionId, null);
			/*
			 * create outputstream
			 */
			FileOutputStream postscriptFileStream = new FileOutputStream(
					postscriptFile);
			/*
			 * write byte array to file
			 */
			postscriptFileStream.write(postscriptByteArray);
			postscriptFileStream.flush();
			postscriptFileStream.close();
			/*
			 * retrieve path
			 */
			String postscriptPath = postscriptFile.getAbsolutePath();
			String pdfPath = postscriptPath + ".result";
			String osName = System.getProperty("os.name");
			String cmd = "";
			/*
			 * make command
			 */
			if (osName.toLowerCase().startsWith("windows")) {
				cmd += "ps2pdf.bat ";
			} else {
				cmd += "ps2pdf ";
			}
			/*
			 * make pdf file name
			 */
			cmd += postscriptPath + " " + pdfPath;

			// log.debug("Executing : " + cmd);
			/*
			 * execute command
			 */
			Process process = rt.exec(cmd);
			/*
			 * wait for the end of the process
			 */
			process.waitFor();
			/*
			 * make new file and file inputstream
			 */
			File pdfFile = new File(pdfPath);

			FileInputStream fileInputStream = new FileInputStream(pdfFile);
			/*
			 * convert it to and byte array
			 */
			byte[] result = getByteArray(fileInputStream);
			postscriptFile.delete();
			pdfFile.delete();
			return result;
		} catch (Exception err) {
			log.error(AstronConverter.stackTraceToString(err));
			return null;
		}
	}

	private byte[] getByteArray(InputStream inputStream) throws IOException {
		int byteChar = inputStream.read();

		ByteArrayOutputStream out = new ByteArrayOutputStream();
		while (byteChar != -1) {
			out.write(byteChar);
			byteChar = inputStream.read();
		}
		inputStream.close();
		return out.toByteArray();
	}

	public static ProposalData getNewProposalData(Proposal proposal) {
		ProposalData proposalData = new ProposalData();
		proposalData.setProposal(proposal);
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		proposalData
				.setSelectedTelescope(proposal.getSemester().getTelescope());
		proposalData.setStrutsModule(telescopeConfiguration.getStrutsModule());
		proposalData.setHeaderImageUri(telescopeConfiguration
				.getHeaderImageUri());

		return proposalData;
	}
	
	public static String getLabel(ServletContext servletContext, Proposal proposal,String name) throws FileNotFoundException {
		// load the telescope specific if needed
		if(translations.size() ==0){
			//log.info("labeltag: got telescope "+ltelescope);
			if(servletContext == null){
				throw new FileNotFoundException("no servlet context defined");
			}
			//ProposalData proposalData = (ProposalData) request.getSession().getAttribute(Constants.PROPOSAL_DATA);
			TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
			String fileLoc = telescopeConfiguration.getLabelsFile();
			if(fileLoc == null){
				throw new FileNotFoundException("no labels file defined in config file for "+proposal.getSemester().getTelescope());
				//log.error("no labels file defined in config file for "+proposalData.getSelectedTelescope());
				
			}
			
			fileLoc = servletContext.getRealPath(fileLoc);
			try {
			      //use buffering, reading one line at a time
			      //FileReader always assumes default encoding is OK!
			      BufferedReader input =  new BufferedReader(new FileReader(fileLoc));
			      try {
			        String line = null; 
			        // readline returns an empty String if two newlines appear in a row.
			        while (( line = input.readLine()) != null){
			          String[] splitted = line.split("=");
			          if(splitted != null && splitted.length > 1){
			        	  String lkey=splitted[0];
			        	  String lvalue=splitted[1];
			        	  // if there are multiple '=' characters in the line, include them in the result 
			        	  for(int i=2;i<splitted.length;i++){
			        		  lvalue += "="+splitted[i];
			        	  }
			        	  translations.put(lkey, lvalue);
			          }
			        }
			      }
			      finally {
			        input.close();
			      }
			    }
			    catch (IOException ex){
			    	throw new FileNotFoundException("Could not read file "+fileLoc);
			    }
			}
		
		String result = (String) translations.get(name);
		if(result == null){
			return name;
		}
		return result;
	}
}
