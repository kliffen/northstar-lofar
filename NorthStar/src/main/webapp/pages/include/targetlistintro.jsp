<p>It is possible to upload a batch file containing the to be observed targets. 
This should be a plain text file. Each line should contain information about the to be observed target.
Each item on this line should be seperated by one or more spaces.</p>

<p>The exposure time should be a number with an <i>h</i>, <i>m</i> or <i>s</i> behind it. One hour can be represented in the 
following formats: 1h, 60m or 3600s<p>

<p>The format of the Right Ascension  and the Declination are the same as the one needed if you enter it
manually:  (hh:mm:ss.ss) for RA and (+dd:mm:ss.s) for Dec. Epochs you can use are: <i>J2000</i>, <i>B1950</i> and <i>Other</i> 
</p>

<p>A line for this proposal should be in the following format (optional items are presented within brackets): </p>
