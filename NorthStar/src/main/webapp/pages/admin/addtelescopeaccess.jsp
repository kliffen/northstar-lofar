<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>

<html:form action="/setUpAddTelescopeAccess" method="POST">
<TABLE>
	<tr>
	<td>
		Add users to telescope: 		
		<bean:write name="addTelescopeAccessForm" property="selectedTelescope"/>
	</td>
	</tr>
	<tr>
		<td><br>
		Available users: <br>
		<bean:define id="mynamelist" name="addTelescopeAccessForm" property="userNameList"/> 
		
		<html:select name="addTelescopeAccessForm" property="selectedUser" size="8">
			<html:options collection="mynamelist" property="id" labelProperty="lastName"/>		
		</html:select>
		</td>
	</tr>
	<tr>
		<td>&nbsp;
		<html:hidden property="selectedTelescope" styleId="selectedTelescope" />
		<html:submit value="Add" />
		<html:cancel value="Done" />
		</td>
	</tr>	

</TABLE>	

</html:form>




