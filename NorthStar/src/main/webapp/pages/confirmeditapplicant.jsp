<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />
<html:form action="/processEditApplicant" focus="name" method="POST">

<html:hidden property="memberId"/>
<table width="100%">
	<TR>
		<TD width="*">&nbsp;</TD>
		<TD class="help" width="60">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="60" height="25">
							<TR>
					  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#applicant\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
				  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#applicant\" target=\"help\">Help</a>"%></TD>
			  				</TR>
			  			</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
					</TD>
	</TR>
	</table>
<table>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="right">Name:&nbsp;</td>
		<td class="bold" align="left"><bean:write name="applicantForm" property="name"/></td>
	</tr>
	<tr>
		<td class="right">Affiliation:&nbsp;</td>
		<td class="bold" align="left"><bean:write name="applicantForm" property="affiliation"/></td>
	</tr>
	<tr>
		<td class="right">Email:&nbsp;</td>
		<td  class="bold" align="left"><bean:write name="applicantForm" property="email"/></td>
	</tr>
	<tr>
		<td class="right">Country:&nbsp;</td>
		<td  class="bold" align="left"><bean:write name="applicantForm" property="country"/></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>	
	<tr>	
		<TD colspan="2">
		<tiles:get name="box_header" />
			<TABLE width="100%">
				<TR>
					<TD><PRE>
Note: an invitation to become an Active Participant has already been sent
to the e-mail address listed,
but the invitation has not yet been accepted.

If you proceed to edit the details for this person now,
the outstanding invitation will become invalid
(you may of course send a new invitation).

Do you wish to continue and edit the particulars of this applicant ?
					</PRE></TD>
				</TR>
 			</TABLE>
		<tiles:get name="box_footer" />
		</TD>
	</TR>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>		
</table>
<table width="100%">
	<tr>
		<td align="left"><html:submit property="OkButton"  tabindex="5" styleClass="list_accept_yes">Ok</html:submit>
 		</td>
		<td align="right"><html:cancel styleClass="list_decline_t">Cancel</html:cancel></TD>
	</tr>
</table>

</html:form>