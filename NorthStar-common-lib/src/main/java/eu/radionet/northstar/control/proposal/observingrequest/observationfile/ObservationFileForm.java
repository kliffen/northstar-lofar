// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Mar 1, 2005
 *
 *
 */
package eu.radionet.northstar.control.proposal.observingrequest.observationfile;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;

public class ObservationFileForm extends ActionForm implements ObservationFileFormInterface
{
    private Log log = LogFactory.getLog(this.getClass());

    protected FormFile uploadFile = null;

    protected String saveButton = null;

    /**
     * @return Returns the uploadFile.
     */
    public FormFile getUploadFile()
    {
        return uploadFile;
    }
    /**
     * @param uploadFile The uploadFile to set.
     */
    public void setUploadFile(FormFile uploadFile)
    {
        this.uploadFile = uploadFile;
    }
    /**
     * @return Returns the uploadFileButton.
     */
    public String getSaveButton()
    {
        return saveButton;
    }
    /**
     * @param uploadFileButton The uploadFileButton to set.
     */
    public void setSaveButton(String uploadFileButton)
    {
        this.saveButton = uploadFileButton;
    }
    
    public void reset(ActionMapping mapping, HttpServletRequest request)  {
        this.saveButton = null;
        this.uploadFile = null;
    }
    
    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) 
    {
        ActionErrors errors = new ActionErrors();
        // Only set derived attributes if (non empty) file has been provided
        /*
         * if file is not empty
         */
        if (this.uploadFile.getFileSize() > 0) 
        {

        } 
        else 
        {
            log.warn("File not found or empty: ");
            errors.add("uploadFile", new ActionMessage("error.file.empty"));
        }
        return errors;
        
    }
}

