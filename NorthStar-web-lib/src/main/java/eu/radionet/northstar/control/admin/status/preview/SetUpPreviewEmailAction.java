// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.status.preview;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.util.AstronConverter;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.admin.collaboration.AdminLockedAction;
import eu.radionet.northstar.control.admin.status.ChangeStatusBean;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Status;

public class SetUpPreviewEmailAction extends AdminLockedAction {
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		PreviewEmailForm previewEmailForm = (PreviewEmailForm) form;
		HttpSession session = request.getSession();

		ChangeStatusBean changeStatusBean = (ChangeStatusBean) session
				.getAttribute(Constants.CHANGE_STATUS_BEAN);
		previewEmailForm.setSubject(changeStatusBean.getStatusEmail().getSubject());
		previewEmailForm.setMessage(AstronConverter.wrap(changeStatusBean.getStatusEmail().getMessage(), 75));
		Member contactAuthor = lockedProposal.getContactAuthor();
		previewEmailForm.setReceivers(contactAuthor.getEmail());
		previewEmailForm.setSender(NorthStarConfiguration.getEmailSender());
		previewEmailForm.setProposalCode(lockedProposal.getCode());
		previewEmailForm.setCurrentStatus(lockedProposal.getCurrentStatus().getStatus().getCode());

		previewEmailForm.setNewStatus(northStarDelegate.getStatus(changeStatusBean.getNewStatusId()).getCode());
		if (lockedProposal.getJustification() != null) {
			previewEmailForm.setProposalTitle(lockedProposal.getJustification()
					.getTitle());
		}
		return mapping.findForward(Constants.SUCCESS);
	}

	protected static List getLabelValueBeans(List statusses) {
		List beans = new ArrayList();
		beans.add(new LabelValueBean(Constants.NONE_SPECIFIED_LABEL,
				Constants.NONE_SPECIFIED_VALUE));
		Iterator statusIterator = statusses.iterator();

		while (statusIterator.hasNext()) {
			Status status = (Status) statusIterator.next();
			String label = status.getCode();
			String value = status.getId().toString();
			beans.add(new LabelValueBean(label, value));
		}
		return beans;
	}


}
