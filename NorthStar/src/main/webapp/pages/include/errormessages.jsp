<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>

<span class="fatal"><astron:errors property="fatal" ifnotexist=""/></span>
<span class="error"><astron:errors property="error" ifnotexist=""/></span>
<span class="warning"><astron:errors property="warning" ifnotexist=""/></span>

