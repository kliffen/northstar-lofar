ALTER TABLE justification ADD COLUMN `scientificfilepages` int(11) default NULL;
ALTER TABLE justification ADD COLUMN `technicaldetailsfilepages` int(11) default NULL;
ALTER TABLE justification ADD COLUMN `figurefilepages` int(11) default NULL;