// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.observingrequest.observation.targetfile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.LabelValueBean;


import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.ConfigurationUtil;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.exception.ParseTargetListException;
import eu.radionet.northstar.business.targetlist.TargetListParser;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.Target;

/**
 * @author Anton Smit
 */
public class ProcessTargetFileAction extends LockedAction {
	private Log log = LogFactory.getLog(ProcessTargetFileAction.class);

	/**
	 * Execute Details Actions
	 * 
	 * @param mapping -
	 *            The ActionMapping used to select this instance
	 * @param form -
	 *            The ActionForm bean for this request (if any)
	 * @param request -
	 *            The HTTP request we are processing
	 * @param response -
	 *            The HTTP response we are creating
	 */
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/*
		 * if pressed on cancel
		 */
		TargetFileForm targetFileForm = (TargetFileForm) form;
		HttpSession session = request.getSession();
		ActionMessages errors = new ActionMessages();
		// ActionForward forward = null;
		boolean exceedMaxAllowedTargets = false;
		boolean invalidTemplateTarget = false;
		/*
		 * retrieve proposal data from session with information
		 */
		ProposalData proposalData = (ProposalData) session
				.getAttribute(Constants.PROPOSAL_DATA);
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
		if (isCancelled(request)) {
			return ParameterAppender.append(mapping.findForward(Constants.SUCCESS),
					"index", targetFileForm.getIndex()+"");

		}

		if (targetFileForm.getSaveButton() != null) 
		{
			Target templateTarget = fillTarget(targetFileForm);
			if (templateTarget != null) 
			{
				invalidTemplateTarget = !isValidTarget(templateTarget);
				// session.setAttribute(Constants.OBSERVATION,
				// templateObservation);
				// forward = mapping.findForward(Constants.WARNING);
			}
			ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
			TargetListParser parser = configurationUtil	.getTargetListParser(proposalData.getProposal());
			TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
			ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());
			
			List maxtargetList = new ArrayList();
			String Urlmax = null;
			maxtargetList = OptionsUtils.getLabelValueBeans(Constants.MAX_TARGETS_SAME_RUN ,
						new HashMap(), contextConfiguration);
			
			int maxTargetOnSameRun=0;
			if (maxtargetList != null && maxtargetList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) maxtargetList.get(1);
				Urlmax =  urlValue.getValue();
				maxTargetOnSameRun=	AstronConverter.toInteger(Urlmax).intValue();
			}
			
			List targetValues = new ArrayList();
			try 
			{
				List list = proposalData.getProposal().getObservingRequests();
				
				if (templateTarget != null) 
				{
					targetValues = parser.parseTargetListFile(
							targetFileForm.getUploadFile().getInputStream(), templateTarget, list, maxTargetOnSameRun);
					
					
				} 
				else 
				{
					targetValues = parser.parseTargetListFile(targetFileForm.getUploadFile().getInputStream(),list,maxTargetOnSameRun);
				}
			} 
			catch (IOException ce) 
			{
				log.warn(LogMessage.getMessage(ownUserAccount,
						"Unable to parse TargetList file (wrongformat)"));
				errors.add("uploadFile", new ActionMessage("error.parser.wrongformat"));
				saveErrors(request, errors);
				return mapping.getInputForward();
			} 
			catch (ParseTargetListException pex) 
			{
				String logMessage = null;
				ActionMessage message = null;
				if (pex.isWrongHeaders()) 
				{
					String wrongHeaders = "'";
					Iterator wrongHeadersIterator = pex.getWrongHeaders().iterator();
					while (wrongHeadersIterator.hasNext()) 
					{
						wrongHeaders += (String) wrongHeadersIterator.next();
						if (wrongHeadersIterator.hasNext()) 
						{
							wrongHeaders += ", ";
						}
					}
					wrongHeaders += "'";
					message = new ActionMessage(pex.getMessage(), wrongHeaders);
					logMessage = "TargetList file: to many headers '" + wrongHeaders + "'";
				} 
			
				
				//Verification of maximum number of subbands per run 5210
				else if (pex.getRunNumber()!=0){
					message = new ActionMessage(pex.getMessage(),new Object[]{pex.getRunNumber(),pex.getLineNumber()});
					logMessage = pex.getMessage()+" "+pex.getRunNumber();
				}
				
				else if (!AstronValidator.isBlankOrNull(pex.getNotavailableobservation())){
					message = new ActionMessage(pex.getMessage(),pex.getNotavailableobservation());
				}
				
				else if (pex.isRequiredHeader()) 
				{
					message = new ActionMessage(pex.getMessage(), pex.getRequiredHeader());
					logMessage = "TargetList file: missing required header '"
							+ pex.getRequiredHeader() + "'";
				} 
				
				else 
				{
					message = new ActionMessage(pex.getMessage(),pex.getLineNumber());
					logMessage = "TargetList file: Unable to parse "+pex.getMessage()+ " "+pex.getLineNumber();
					
				}
				log.warn(LogMessage.getMessage(ownUserAccount, logMessage));
				errors.add("uploadFile",message);
				saveErrors(request, errors);
				return mapping.getInputForward();
			}
			finally 
			{
				if (targetFileForm.getUploadFile() != null) 
				{
					try 
					{
						targetFileForm.getUploadFile().getInputStream().close();
					} 
					catch (IOException f) 
					{
						/*skip*/
					}
					targetFileForm.setUploadFile(null);
				}
			}
			
			
			
			exceedMaxAllowedTargets = false;
			/* TODO: fix this.
				
				proposalDelegate
					.exceedMaxAllowedObservations(proposalData.getProposal(),
							observations);
			*/
			
			/*
			 * if max allowed observations exceed, give a warning
			 */
			if (exceedMaxAllowedTargets) {
				//TODO
				//session.setAttribute(Constants.OBSERVATIONS, targets);
			} 
			else 
			{
				/* 
				 * Upload was successfull. The targets are added to the session
				 * to be adding in the ObservationForm
				 * ??
				 */
				session.setAttribute(Constants.NEW_TARGETS, targetValues);
			}
			if (exceedMaxAllowedTargets || invalidTemplateTarget) 
			{
				return ParameterAppender.append(mapping
						.findForward(Constants.WARNING), new String[] {
						"exceedMaxAllowedObservations",
						"invalidTemplateObservation" }, new String[] {
						getString(exceedMaxAllowedTargets),
						getString(invalidTemplateTarget) });
			}
			return ParameterAppender.append(mapping.findForward(Constants.SUCCESS),
					"index", targetFileForm.getIndex()+""); 
		}

		return mapping.findForward(Constants.REFRESH);
	}

	protected Target fillTarget(TargetFileForm targetFileForm)
			throws DatabaseException {
		return null;
	}

	protected boolean isValidTarget(Target target) {
		return true;
	}

	protected String getString(boolean bool) {
		if (bool) {
			return "true";
		} else {
			return "false";
		}
	}

}