// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.view;

/**
 *  This class will work like internationalization, label files are specified for each telescope and 
 *  maybe later, for each community
 *  the class will for each telescope load the appropiate labels
 *  it will return the translated values for the given keys.
 * 
 */
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.data.entities.Proposal;

public class LabelTag extends TagSupport{
	private static final String DEFAULT = "default";  // default telescope, do not name a telescope default!!
	private Log log = LogFactory.getLog(this.getClass());
	private String key=null;
	private String telescope=null;
	private Map translations = new HashMap();
	
	public void setKey(String key) {
		this.key = key;
	}
	
	public void setTelescope(String telescope) {
		this.telescope = telescope;
	}

	public int doStartTag() throws JspException {
 	    // get the telescope and load the translations for it if needed
		telescope=getProposalTelescope();
		
		String result = null;
		if(translations==null){
			result = "no labels loaded";
		}else if(key==null){
			result = "missing key value";
		}else{
			// get the actual translation
			Map translation = (HashMap) translations.get(telescope);
			if(translation != null){
				result = (String) translation.get(key);
			}
			// key not found, try the default
			if(result == null){
				translation = (HashMap) translations.get(DEFAULT);
				if(translation != null){
					result = (String) translation.get(key);
				}else{
					result = " no translation file ";
				}
				if (result == null){
					log.warn("could not find translation for key: "+key);
					// return an empty field if it is not found
					result=" ";
				}
					
			}
		}
		
		
		
		try {
			// actually return the result
            pageContext.getOut().write(result);
        } catch (IOException ioe) {
            throw new JspException(ioe.getMessage());
        }
        return EVAL_BODY_INCLUDE;
	}

	private String getProposalTelescope() {
		// get the telescope from the selected proposal.
		// if the proposal is not selected yet, return 'default' as name
		// also find the translations if needed.
		String ltelescope = DEFAULT;
		ProposalData proposalData = (ProposalData) pageContext.getSession().getAttribute(Constants.PROPOSAL_DATA);
		if (proposalData != null){
			Proposal proposal = proposalData.getProposal();
			if (proposal != null){
				ltelescope = proposal.getSemester().getTelescope();
			}
		}
		// check if translations are already loaded.
		if(translations.get(DEFAULT) == null){
			//if not, load the default label file, as defined in northstar-config.xml 
			String fileLoc = NorthStarConfiguration.getLabelsFile();
			fileLoc = pageContext.getServletContext().getRealPath(fileLoc);
			if(fileLoc == null){
				log.error("no default labels file defined in config file");
				return DEFAULT;
			}
			Map translationMap = loadTranslation(fileLoc);
			log.info("labeltag: loaded "+translationMap.size()+" default labels");
			if(translationMap.size() > 0){
				translations.put(DEFAULT, translationMap);
			}
		}
		
		// load the telescope specific if needed
		if(proposalData != null && translations.get(ltelescope) == null){
			//log.info("labeltag: got telescope "+ltelescope);
			TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
			if(telescopeConfiguration==null){
				log.warn("no telescope configuration found for "+ltelescope);
				return DEFAULT;
			}
			String fileLoc = telescopeConfiguration.getLabelsFile();
			if(fileLoc == null){
				log.error("no labels file defined in config file for "+ltelescope);
				return DEFAULT;
			}
			fileLoc = pageContext.getServletContext().getRealPath(fileLoc);
			Map translationMap = loadTranslation(fileLoc);
			log.info("labeltag: loaded "+translationMap.size()+" labels for "+ltelescope);
			if(translationMap.size() > 0){
				translations.put(ltelescope, translationMap);
			}
		}
		
		return ltelescope;
	}

	private Map loadTranslation(String fileLoc) {
			
		Map translation = new HashMap();
		 try {
		      //use buffering, reading one line at a time
		      //FileReader always assumes default encoding is OK!
		      BufferedReader input =  new BufferedReader(new FileReader(fileLoc));
		      try {
		        String line = null; 
		        // readline returns an empty String if two newlines appear in a row.
		        while (( line = input.readLine()) != null){
		          String[] splitted = line.split("=");
		          if(splitted != null && splitted.length > 1){
		        	  String lkey=splitted[0];
		        	  String lvalue=splitted[1];
		        	  // if there are multiple '=' characters in the line, include them in the result 
		        	  for(int i=2;i<splitted.length;i++){
		        		  lvalue += "="+splitted[i];
		        	  }
		        	  translation.put(lkey, lvalue);
		          }
		        }
		      }
		      finally {
		        input.close();
		      }
		    }
		    catch (IOException ex){
		      log.error("Could not read file "+fileLoc);
		    }
		return translation;
	}

	
}
