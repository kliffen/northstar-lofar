<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />

<html:form action="/processSemesterList" method="POST">

<TABLE>

	<tr>
		<td align="center" colspan="2"><astron:label key="label.admin.semesterlist.servertime" /><bean:write name="semesterListForm" property="serverUTCTime"/></td>
	</tr>
	<TR>
		<td colspan="2"><b><astron:label key="label.admin.semesterlist.category" /></b>
 
	 <logic:iterate id="category" property="categories" name="semesterListForm">
  	    <html:multibox property="selectedCategories">
            <bean:write name="category" property="code"/>
		</html:multibox>
			<bean:write name="category" property="code"/>
	 </logic:iterate>   
		</td>
	</TR>
	<TR>
		<td colspan="2"><b><astron:label key="label.admin.semesterlist.telescopes" /></b>
 
	 <logic:iterate id="telescope" property="telescopes" name="semesterListForm">
  	    <html:multibox property="selectedTelescopes">
            <bean:write name="telescope" />
		</html:multibox>
			<bean:write name="telescope"/>
	 </logic:iterate>   
		</td>
	</TR>
	<TR>
		<td><b><astron:label key="label.admin.semesterlist.communities" /></b>
 
	 <logic:iterate id="community" property="communities" name="semesterListForm">
  	    <html:multibox property="selectedCommunities">
            <bean:write name="community" property="name" />
		</html:multibox>
			<bean:write name="community" property="description"/>
	 </logic:iterate>   
		</td>
	</TR>	
	<tr>
		<td colspan="2" ><astron:label key="label.admin.semesterlist.afterdate" /><html:radio styleClass="radio" property="afterCurrentDate" value="true" >Yes</html:radio>
						<html:radio styleClass="radio" property="afterCurrentDate" value="false">No</html:radio></td>
	</tr>	
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<TR>
		<TD colspan="2">
			
		<html:submit property="filterButton" styleClass="filter2" onclick="" tabindex="1">Filter</html:submit> 
		</TD>
	</TR>
	<TR>
		<TD colspan="2">
			<table align="center" class="projecttable">
				<TR>
					<td class="tableheader"><astron:label key="label.admin.semesterlist.prefix" /></td>
					<td class="tableheader"><astron:label key="label.admin.semesterlist.telescope" /></td>					
					<td class="tableheader"><astron:label key="label.admin.semesterlist.cat" /></td>
     				<td class="tableheader"><astron:label key="label.admin.semesterlist.available" /></td>
     				<td class="tableheader"><astron:label key="label.admin.semesterlist.immediate" /></td>					
     				<td class="tableheader"><astron:label key="label.admin.semesterlist.starttime" /></td>
     				<td class="tableheader"><astron:label key="label.admin.semesterlist.endtime" /></td>
     				<td class="tableheader"><astron:label key="label.admin.semesterlist.deadline" /></td>
					<td class="tableheader"><astron:label key="label.admin.semesterlist.lastnumber" /></td>     	
					<td class="tableheader">&nbsp;</td> 	
					<td class="tableheader">&nbsp;</td> 		
				</TR>
			<%
			int i=0;
			String styleClass = "";
			%>
	 			<logic:iterate id="semesterBean" name="semesterListForm" property="semesterBeans">
	 			<%
	 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
	 			%>
 	 			<TR>
					<TD class="tablefield_pri<%=styleClass%>" valign="top" align="center"><bean:write name="semesterBean" property="prefix"/><html:hidden name="semesterBean" property="id" indexed="true"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top" align="center"><bean:write name="semesterBean" property="telescope"/></TD>					
					<TD class="tablefield<%=styleClass%>" valign="top" align="center"><bean:write name="semesterBean" property="category"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top" align="center">
						<logic:equal name="semesterBean" property="available" value="true">	
							<html:img module="" page="/images/list/ok.gif" width="16" height="16"/>
						</logic:equal>
						<logic:notEqual name="semesterBean" property="available" value="true">	
							&nbsp;
						</logic:notEqual>						
					</TD>
					<TD class="tablefield<%=styleClass%>" valign="top" align="center">
						<logic:equal name="semesterBean" property="immediate" value="true">	
							<html:img module="" page="/images/list/ok.gif" width="16" height="16"/>
						</logic:equal>
						<logic:notEqual name="semesterBean" property="immediate" value="true">	
							&nbsp;
						</logic:notEqual>						
					</TD>
					<TD class="tablefield<%=styleClass%>" valign="top" align="right"><bean:write name="semesterBean" property="startTime"/></TD>					
					<TD class="tablefield<%=styleClass%>" valign="top" align="right"><bean:write name="semesterBean" property="endTime"/></TD>
					<TD class="tablefield<%=styleClass%>"  valign="top" align="right"><bean:write name="semesterBean" property="deadLineTime"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top" align="center"><bean:write name="semesterBean" property="lastAssignedNumber"/></TD>
					<TD class="tablefield<%=styleClass%>"  valign="top"><html:submit indexed="true" property="editButton" styleClass="list_edit" title="Edit this proposal">Edit</html:submit></TD>
					
					<TD class="tablefield<%=styleClass%>"  valign="top">
						<logic:equal name="semesterBean" property="allowedToDelete" value="true">
							<html:submit indexed="true" property="deleteButton" styleClass="list_delete"  title="Delete this semester">Delete</html:submit>
						</logic:equal>
					</TD>
	 			</TR>

	 			</logic:iterate>
				<tr><td colspan="8" class="spacer">&nbsp;</td></tr>
			</table>
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD align="center"  colspan="2"><html:submit tabindex="1" property="addButton" styleClass="list_create" title="Add a new semester."><astron:label key="label.admin.semesterlist.addsemester" /></html:submit></TD>
	</TR>
	

	
</TABLE>

</html:form>

