DROP TABLE IF EXISTS `usertelescope`;
CREATE TABLE `usertelescope` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) default NULL,
  `telescope` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;