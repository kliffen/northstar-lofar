ALTER TABLE lofar_proposal_astron_nl_northstar.justification
ADD COLUMN ceprequesting TINYINT(1)  NOT NULL DEFAULT 0 ,
ADD COLUMN ceprequestingreason TEXT  DEFAULT NULL;