// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.justification;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronValidator;
import nl.astron.util.Utils;
import nl.astron.util.AstronConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.SetUpProposalAction;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.Justification;
import eu.radionet.northstar.data.entities.admin.AdminProposalSummary;

public class SetUpJustificationAction extends SetUpProposalAction {
	private Log log = LogFactory.getLog(this.getClass());

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/*
		 * needed for tabs
		 */
		request
				.setAttribute(Constants.CURRENT_ACTION, request
						.getServletPath());
		JustificationForm justificationForm = (JustificationForm) form;
		
		HttpSession session = request.getSession();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
    	ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
    	// get configuration value for second justification from options file
    	TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
    	ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());
    	FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
    	
    	if (OptionsDelegate.allowedToDisplay(Constants.ENABLE_ENVELOPE_SHEET , fieldsDefinitionType)){
			justificationForm.setAllowEnvelopeSheet(true); 
		}else{
			justificationForm.setAllowEnvelopeSheet(false); 
		}
    	
    	if (OptionsDelegate.allowedToDisplay(Constants.ENABLE_FIRST_JUSTIFICATION , fieldsDefinitionType)){
			justificationForm.setAllowFirstJustification(true); 
		}else{
			justificationForm.setAllowFirstJustification(false); 
		}
    	
    	if (OptionsDelegate.allowedToDisplay(Constants.ENABLE_SECOND_JUSTIFICATION , fieldsDefinitionType)){
    			justificationForm.setAllowSecondJustification(true); 
		}else{
			justificationForm.setAllowSecondJustification(false); 
		}
    	if (OptionsDelegate.allowedToDisplay(Constants.ENABLE_FIGURES_FILE , fieldsDefinitionType)){
    			justificationForm.setAllowFiguresFile(true); 
		}else{
			justificationForm.setAllowFiguresFile(false); 
		}
    	
		if (OptionsDelegate.allowedToDisplay(Constants.ENABLE_TECHNICAL_JUSTIFICATION,
				fieldsDefinitionType)) {
			justificationForm.setAllowTechnicalJustification(true);
		} else {
			justificationForm.setAllowTechnicalJustification(false);
		}
 
    	if (OptionsDelegate.allowedToDisplay(Constants.SINGLE_FILE_UPLOAD , fieldsDefinitionType)){
    		justificationForm.setShowSingleFileUpload(true);
    	}
    	
		Justification justification = proposalData.getProposal()
				.getJustification();
		if (justification != null && getErrors(request).size() == 0) {
			justificationForm.setTitle(justification.getTitle());
			justificationForm.setAbstractText(justification.getAbstractText());
			justificationForm.setObservationStrategyTextCount("0");
			if(justificationForm.isAllowTechnicalJustification()) 
			{
				justificationForm.setObservationStrategyText(justification.getObservationStrategyText()!=null?justification.getObservationStrategyText():"");
				justificationForm.setDefaultROProcessing(justification.isDefaultROProcessing());
				justificationForm.setExternalProcessing(justification.isExternalProcessing());
				justificationForm.setExternalProcessingReason(justification.getExternalProcessingReason()!=null?justification.getExternalProcessingReason():"");
				justificationForm.setCepRequesting(justification.isCepRequesting());
				justificationForm.setCepRequestingReason(justification.getCepRequestingReason()!=null?justification.getCepRequestingReason():"");		
				justificationForm.setInternationalStation(justification.isInternationalStation());
				justificationForm.setInternationalStationEssential(justification.isInternationalStationEssential());
				justificationForm.setInternationalStationEssentialReason(justification.getInternationalStationEssentialReason()!=null?justification.getInternationalStationEssentialReason():"");
				justificationForm.setMaxDataRate(justification.getMaxDataRate()!=null?justification.getMaxDataRate():0.0);
				justificationForm.setNighttime(justification.isNighttime());
				justificationForm.setNightTimeReason(justification.getNightTimeReason()!=null?justification.getNightTimeReason():"");
				justificationForm.setOfflineROProcessing(justification.isOfflineROProcessing());
				justificationForm.setOfflineROProcessingReason(justification.getOfflineROProcessingReason()!=null ? justification.getOfflineROProcessingReason() : "");
				justificationForm.setOtherSchedulingConstraints(justification.isOtherSchedulingConstraints());
				justificationForm.setOtherSchedulingConstraintsReason(justification.getOtherSchedulingConstraintsReason()!=null? justification.getOtherSchedulingConstraintsReason():"");				
				justificationForm.setCombinedDataProductRequest(justification.isCombinedDataProductRequest());
				justificationForm.setCombinedDataProductRequestReason(justification.getCombinedDataProductRequestReason() !=null? justification.getCombinedDataProductRequestReason():"");						
				justificationForm.setParallelObservation(justification.isParallelObservation());
				justificationForm.setParallelObservationReason(justification.getParallelObservationReason()!=null?justification.getParallelObservationReason():"");
				
				justificationForm.setRoProcessing(justification.isRoProcessing());
				justificationForm.setRoProcessingReason(justification.getRoProcessingReason()!=null?justification.getRoProcessingReason(): "");
				
				justificationForm.setSensitivity(justification.getSensitivity()!=null?justification.getSensitivity():0.0);
				justificationForm.setCoObservationTeam(justification.isCoObservationTeam());
				justificationForm.setFillerTime(justification.isFillerTime());
				
				justificationForm.setFillerTimeReason(justification.getFillerTimeReason()!=null?justification.getFillerTimeReason(): "");
				
				justificationForm.setLtaRawStorage(justification.isLtaRawStorage());
				justificationForm.setLtaRawStorageReason(justification.getLtaRawStorageReason()!=null?justification.getLtaRawStorageReason():"");
				
				justificationForm.setLtaStorage(justification.isLtaStorage());
				justificationForm.setLtaStorageReason(justification.getLtaStorageReason()!=null?justification.getLtaStorageReason():"");
				justificationForm.setLtaStorageLocation(justification.getLtaStorageLocation()!=null?justification.getLtaStorageLocation():"");
				 
			}
			
					
		}
		
		justificationForm.setEnvelopeSheets(getEnvelopeSheets(proposalData, ownUserAccount) );
		
		if (justification != null){
			justificationForm.setFigureFileDate(AstronConverter
					.toDateString(justification.getFigureFileDate()));
			justificationForm.setFigureFileName(justification
					.getFigureFileName());
			justificationForm.setFigureFileSize(AstronConverter
					.toFileSize(justification.getFigureFileSize()));
			justificationForm.setScientificFileDate(AstronConverter
					.toDateString(justification.getScientificFileDate()));
			justificationForm.setScientificFileName(justification
					.getScientificFileName());
			justificationForm.setScientificFileSize(AstronConverter
					.toFileSize(justification.getScientificFileSize()));
			justificationForm.setTechnicalDetailsFileDate(AstronConverter
					.toDateString(justification.getTechnicalDetailsFileDate()));
			justificationForm.setTechnicalDetailsFileName(justification
					.getTechnicalDetailsFileName());
			justificationForm.setTechnicalDetailsFileSize(AstronConverter
					.toFileSize(justification.getTechnicalDetailsFileSize()));
			justificationForm.setSelectedEnvelopeSheet(justification.getEnvelopeSheet() );
		}
		
		justificationForm.setAbstractCount(Utils.countWords(justificationForm.getAbstractText()).toString());
	
		justificationForm.setTitleCount(Utils.countCharacters(justificationForm.getTitle()).toString());
		justificationForm.setMaxAbstractCount(telescopeConfiguration.getMaxWordsForAbstract()+"");
		justificationForm.setMaxTitleCount(telescopeConfiguration.getMaxCharsForTitle()+"");
		justificationForm.setMaxTechCount(telescopeConfiguration.getMaxWordsForTech()+"");
		if(justification!=null)
		{	
		
		justificationForm.setObservationStrategyTextCount(Utils.countWords(justification.getObservationStrategyText()!=null?justification.getObservationStrategyText():"").toString());
		justificationForm.setNightTimeReasonCount(Utils.countWords(justification.getNightTimeReason()!=null?justification.getNightTimeReason():"").toString());
		justificationForm.setParallelObservationReasonCount(Utils.countWords(justification.getParallelObservationReason()!=null?justification.getParallelObservationReason():"" ).toString());
		justificationForm.setExternalProcessingReasonCount(Utils.countWords(justification.getExternalProcessingReason()!=null?justification.getExternalProcessingReason():"").toString());
		justificationForm.setCepRequestingReasonCount(Utils.countWords(justification.getCepRequestingReason()!=null?justification.getCepRequestingReason():"").toString());
		
		justificationForm.setInternationalStationEssentialReasonCount(Utils.countWords(justification.getInternationalStationEssentialReason()!=null?justification.getInternationalStationEssentialReason():"").toString());
		justificationForm.setLtaRawStorageReasonCount(Utils.countWords(justification.getLtaRawStorageReason()!=null?justification.getLtaRawStorageReason():"").toString());
		justificationForm.setLtaStorageReasonCount(Utils.countWords(justification.getLtaStorageReason()!=null?justification.getLtaStorageReason():"").toString());
		
		justificationForm.setOfflineROProcessingReasonCount(Utils.countWords(justification.getOfflineROProcessingReason()!=null?justification.getOfflineROProcessingReason():"").toString());
		justificationForm.setOtherSchedulingConstraintsReasonCount(Utils.countWords(justification.getOtherSchedulingConstraintsReason()!=null?justification.getOtherSchedulingConstraintsReason():"").toString());
		justificationForm.setCombinedDataProductRequestReasonCount(Utils.countWords(justification.getCombinedDataProductRequestReason()!=null?justification.getCombinedDataProductRequestReason():"").toString());
		justificationForm.setRoProcessingReasonCount(Utils.countWords(justification.getRoProcessingReason()!=null?justification.getRoProcessingReason():"").toString());
		justificationForm.setFillerTimeReasonCount(Utils.countWords(justification.getFillerTimeReason()!=null?justification.getFillerTimeReason():"").toString());
		}
		
		else
		{
			justificationForm.setNightTimeReasonCount("0");
			justificationForm.setParallelObservationReasonCount("0");
			justificationForm.setExternalProcessingReasonCount("0");
			justificationForm.setCepRequestingReasonCount("0");
			justificationForm.setInternationalStationEssentialReasonCount("0");
			justificationForm.setLtaRawStorageReasonCount("0");
			justificationForm.setLtaStorageReasonCount("0");
			justificationForm.setOfflineROProcessingReasonCount("0");
			justificationForm.setOtherSchedulingConstraintsReasonCount("0");
			justificationForm.setCombinedDataProductRequestReasonCount("0");
			justificationForm.setRoProcessingReasonCount("0");
			justificationForm.setFillerTimeReasonCount("0");
		}
		//String url =  telescopeConfiguration.getLatexClassFile();
		List urlList = new ArrayList();
		String url = new String();
		urlList = OptionsUtils.getLabelValueBeans(Constants.LATEX_CLASS_FILE, new HashMap(), contextConfiguration);
		
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			url =  urlValue.getValue(); 
		} 

		
		if(url != null){
			// test if file exists
			String realUrl = request.getSession().getServletContext().getRealPath(url);
			if(url != null && realUrl != null){
				File latexFile = new File(realUrl);
				if (latexFile.exists()){
					url = getFileName(url);
					justificationForm.setLatexClassUrl(url);
				}
			}
			
			
			urlList = OptionsUtils.getLabelValueBeans(Constants.LATEX_TEMPLATE_FILE, new HashMap(), contextConfiguration);
			
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
				url =  urlValue.getValue(); 
			} 
			//url= telescopeConfiguration.getLatexTemplateFile();
			realUrl = request.getSession().getServletContext().getRealPath(url);
			if(url != null && realUrl != null){
				File latexFile = new File(realUrl);
				if (latexFile.exists()){
					url = getFileName(url);
					justificationForm.setLatexTemplateUrl(url);
				}
			}
		}
		if(!AstronValidator.isBlankOrNull(justificationForm.getLatexClassUrl()) || !AstronValidator.isBlankOrNull(justificationForm.getLatexTemplateUrl())){
    		justificationForm.setShowLatex(true);
    	}
    	
		List technicalQuestions = OptionsDelegate.getOptionsConfigurationTypes(
        		Constants.DISPLAY_TECH_JUSTIFICATIONS, new HashMap(), contextConfiguration);

		
		
		if (technicalQuestions != null && technicalQuestions.size() > 0) {
			ConfigureOptionType optionFieldConfiguration =(ConfigureOptionType) technicalQuestions.get(0);
			ConfigureOptionType techOptions = OptionsDelegate.getOption(optionFieldConfiguration.getValue(), technicalQuestions);
			FieldsDefinitionType questions = techOptions.getSubfields();
			// The display of the observation strategy should not be fixed under the technical questions
			justificationForm.setDisplayObservationStrategy(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_OBSERVATION_STRATEGY, questions));
			
			justificationForm.setDisplayExternalProcessing(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_EXTERNAL_PROCESSING,questions));

			justificationForm.setDisplayCepRequesting(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_CEP_REQUEST,questions));
			
			justificationForm.setDisplayInternationalStation(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_INTERNATIONAL_STATION,questions)); 
			
			justificationForm.setDisplayLtaStorage(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_LTA_STORAGE,questions));
			
			justificationForm.setDisplayLtaStorageAdvanced(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_LTA_STORAGE_ADVANCED,questions));
				
			justificationForm.setDisplayMaxDataRate(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_MAX_DATA_RATE,questions));
			
			justificationForm.setDisplayNightTime(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_NIGHT_TIME,questions));
			
			justificationForm.setDisplayOfflineROProcessing(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_OFFLINE_PROCESSING,questions));
			
			justificationForm.setDisplayOtherSchedulingConstraints(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_OTHER_SCHEDULING,questions));
			
			justificationForm.setDisplayCombinedDataProductRequest(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_COMBINED_DATA_PRODUCT_REQUEST,questions));
			
			justificationForm.setDisplayParallelObservatione(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_PARALLEL_OBSERVATION,questions));
			
			justificationForm.setDisplaySensitivity(OptionsDelegate.allowedToDisplay(
							Constants.DISPLAY_TECH_SENSITIVITY,questions));
			
			justificationForm.setDisplayROProcessing(OptionsDelegate.allowedToDisplay(
					Constants.DISPLAY_TECH_RO_PROCESSING,questions));
			
			justificationForm.setDisplayROProcessingWithoutSelfCal(OptionsDelegate.allowedToDisplay(
					Constants.DISPLAY_TECH_RO_PROCESSING_WITHOUT_SELFCAL,questions));
			
			justificationForm.setDisplayFillerTime(OptionsDelegate.allowedToDisplay(
					Constants.DISPLAY_TECH_FILLER_TIME,questions));
			
			justificationForm.setDisplayCoObservationTeam(OptionsDelegate.allowedToDisplay(
					Constants.DISPLAY_TECH_COOBSERVATION_TEAM,questions));
			
		}
        return super.lockedExecute(mapping,form,request,response);
	}

	 private List getEnvelopeSheets(ProposalData proposalData,
			UserAccount ownUserAccount) {
		//List<LabelValueBean> envelopeSheetLvbs = new ArrayList();
		Map<String,AdminProposalSummary> envelopeSheetProposalSummaries = new HashMap();
		for (AdminProposalSummary sheet : northStarDelegate.getEnvelopeSheetProposalSummaries(ownUserAccount)){
			/* only add submitted proposals
			if(!envelopeSheetProposalSummaries.containsKey(sheet.getTitle()) && sheet.getCode() != ""){
				envelopeSheetProposalSummaries.put(sheet.getTitle(), sheet);
			}*/
			envelopeSheetProposalSummaries.put(AstronValidator.isBlankOrNull(sheet.getTitle())?sheet.getId()+"":sheet.getTitle(), sheet);
		}
		//envelopeSheetProposalSummaries.addAll();
		
		/*for (ProposalSummary proposalSummary : envelopeSheetProposalSummaries){
			LabelValueBean lvb = new LabelValueBean();
			lvb.setLabel(proposalSummary.getTitle());
			lvb.setValue(proposalSummary.getTitle());
			envelopeSheetLvbs.add(lvb);
		}
		*/
		List result = new ArrayList();
		//  add one empty
		AdminProposalSummary empty = new AdminProposalSummary();
		result.add(empty);	
		result.addAll(envelopeSheetProposalSummaries.values() );
		
		return result;
	}

	public String getFileName(String url){
	    	int separatorpos = url.lastIndexOf(File.separatorChar);
			if (separatorpos>0 ){				
				url = url.substring(separatorpos+1,url.length());
			}
			/*else {
				separatorpos = url.lastIndexOf("\\");
				if (separatorpos>0 ){				
					url = url.substring(separatorpos+1,url.length());
				}
			}
			*/
			return url;
	    }
}
