// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.manage.invitation;

import org.apache.struts.action.ActionForm;

public class InvitationForm extends ActionForm {
	
	private String key = null;
	private String okButton = null;
	private String acceptMessage = null;
	private boolean keyIsAccepted = false;

	public String getOkButton() {
		return okButton;
	}

	public void setOkButton(String okButton) {
		this.okButton = okButton;
	}

	public String getAcceptMessage() {
		return acceptMessage;
	}

	public void setAcceptMessage(String acceptMessage) {
		this.acceptMessage = acceptMessage;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isKeyIsAccepted() {
		return keyIsAccepted;
	}

	public void setKeyIsAccepted(boolean keyIsAccepted) {
		this.keyIsAccepted = keyIsAccepted;
	}

}
