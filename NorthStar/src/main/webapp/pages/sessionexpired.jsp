<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processSessionExpired" method="GET">
<TABLE>
	
	<TR>
		<TD>
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				<TD valign='top'><PRE>
 A problem has occurred: The proposal is no longer available for editing.
 
 This problem can occur if more than a single window is used to work with
 NorthStar or if a proposal edit page is requested from e.g. a bookmark.
 If this is not the case, please <html:link
								title="Report the problem"
								action="/setUpSendQuestionsProblemsAction"
								target="sendQuestionsProblems">contact us</html:link>.
 
 Press OK to return to the proposal list.

</PRE>
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>	
	</TABLE>
<TABLE width="100%">
	<TR>
		<TD align="center"><html:submit property="okButton" styleClass="list_accept_yes" title="Ok, return to the proposal list">Ok</html:submit></TD>
		</TD>
	</TR>
</TABLE>
</html:form>
