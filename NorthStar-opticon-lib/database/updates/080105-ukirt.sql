alter table `opticonobservingrequest` 
	change column `service_mode` `observing_mode` varchar(100);

alter table `opticonobservingrequest` 
	add column `calibration_requirements` text default NULL, 
	add column `new_observer_experience` text default NULL,
	add column `grant_number` varchar(255) default NULL, 
	add column `travel` text default NULL,
	add column `other_expenditure` text default NULL;

alter table `opticonobservingrequest` 
	add column `opticon_funding` tinyint(1) NOT NULL default '0';

alter table `opticoninstrument` 
	change column `default_frame_exposuretime` `non_default_frame_exposuretime` tinyint(1) NOT NULL default '0',
	add column `non_default_frame_exposuretime_details` text default NULL after `non_default_frame_exposuretime`;
	

alter table `opticontarget` 
	add column  `flux` double default NULL,  
    add column `seeing_upper` double default NULL,  
    add column `seeing_lower` double default NULL;
    
alter table `opticontarget` 
	add column `sky_quality` varchar(255) default NULL;
	