<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html:hidden property="forward"/>	
<table width="100%">
<tr>
<td align="left"><html:submit property="saveButton" styleClass="list_saveandcontinue" title="Save changes and continue editing" altKey="" onclick="setNorthStarCookie(getScrollCoordinates());">Save and Continue</html:submit>&nbsp;
<html:submit property="saveAndViewButton" styleClass="list_saveandview" title="Save changes and view your proposal. (pdf)" onclick="setNorthStarCookie(getScrollCoordinates());">Save and Preview</html:submit>&nbsp;
<html:submit property="saveAndExitButton" styleClass="list_saveandexit" title="Save changes and exit">Save and Exit</html:submit>&nbsp;
<logic:equal name="observingRequestForm" property="userIsContactAuthor" value="true">
<html:submit property="saveAndSubmitButton" styleClass="list_saveandsubmit" title="Save changes and submit your proposal">Save and Submit</html:submit>
</logic:equal>
&nbsp;
</td>

<td align="right"><html:cancel styleClass="list_quitdontsave" title="Quit without saving changes">Quit without save</html:cancel></TD>
</tr>
</table>
