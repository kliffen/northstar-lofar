// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SetUpUploadAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: SetUpUploadAction.java,v 1.4 2008-07-18 13:59:28 boelen Exp $
 *
 */
package eu.radionet.northstar.control.proposal.upload;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.tar.TarEntry;
import nl.astron.util.tar.TarInputStream;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.FigureFile;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The SetUpUploadAction provides
 * 
 * @author Bastiaan Verhoef
 *  
 */
public class SetUpUploadAction extends LockedAction {
	private static String UPLOAD_MESSAGE = "/text/host/upload.txt";
	
    public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
    	
    	UploadForm uploadForm = (UploadForm) form;
    	HttpSession session = request.getSession();
    	ProposalData proposalData = (ProposalData) request.getSession().getAttribute(Constants.PROPOSAL_DATA);
    	
    	TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
    	ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());
    	FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();

    	try{
			uploadForm.setUploadMessage(
				NorthStarUtils.read(UPLOAD_MESSAGE,request.getSession().getServletContext()));
		}catch(IOException io){
			uploadForm.setUploadMessage("The upload file may not exceed 10MB ");
		}
    	if (OptionsDelegate.allowedToDisplay( Constants.ENABLE_PAGE_NUMBER_CONF, fieldsDefinitionType)){
    		uploadForm.setVariableMaxPageNumber(true);
    	}else {
    		uploadForm.setVariableMaxPageNumber(false);
    	}
    	if(uploadForm.getKindUploadFile() != null && uploadForm.getKindUploadFile().equalsIgnoreCase(Constants.ENVELOPE) ){
    		// get list of envelope files from all proposals from this pi
    		List envelopeFileNames = getEnvelopeNames(proposalData,session);
    		uploadForm.getFileList().addAll(envelopeFileNames);
    		uploadForm.setUploadMessage("Add envelope sheet: the file may not exceed 10MB ");
		}else{
			if (OptionsDelegate.allowedToDisplay(Constants.SINGLE_FILE_UPLOAD , fieldsDefinitionType)){
	    		uploadForm.setShowSingleFileUpload(true);
			}else{
			
				//  add scientific file name to the list
				if (proposalData.getProposal().getJustification().getScientificFileName() != null){
					LabelValueBean lvb = new LabelValueBean();
					lvb.setLabel(proposalData.getProposal().getJustification().getScientificFileName());
					lvb.setValue(proposalData.getProposal().getJustification().getScientificFileName());
					uploadForm.getFileList().add(lvb);
					}
				
				// extract tar file for figure names
				if(proposalData.getProposal().getJustification().getFigureFileName() != null){
					uploadForm.getFileList().addAll(addFigureNames(proposalData));
		
				}
			}
		}
		
		return mapping.findForward("success");
    }

    
	private List addFigureNames(ProposalData proposalData) {
		Proposal proposal = proposalData.getProposal();
		FigureFile figureFile=proposalData.getFigureFile();
		List figureNameList = new ArrayList();
		if (figureFile==null){
			try {
				proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposal);
				figureFile = proposalDelegate.getFigureFile(proposal.getJustification());
			} catch (DatabaseException e) {
				e.printStackTrace();
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		}
		InputStream inp = new ByteArrayInputStream(figureFile.getUploadFile());
		TarInputStream tis = new TarInputStream(inp);
		TarEntry ter;
		
		String fileName="";
		try {
			while( (ter=tis.getNextEntry()) != null){
				if(ter.isDirectory()){
					// pfff directories..
					fileName=ter.getName()+File.separator;
				}
				else{
					// get filename from entry
				    fileName += ter.getName();
				    LabelValueBean lvb = new LabelValueBean();
				    lvb.setLabel(fileName);
				    lvb.setValue(fileName);
				    figureNameList.add(lvb);
				    fileName="";
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return figureNameList;

	}
    
	private List getEnvelopeNames(ProposalData proposalData, HttpSession session) {
		// get list of all proposals
		List<Proposal> proposals = new ArrayList();
		List<String> envelopeNames = new ArrayList();
		List<String> names = NorthStarConfiguration.getTelescopeNames();
		UserAccount ownUserAccount = (UserAccount) session.getAttribute(Constants.OWN_USERACCOUNT);
		
		for(String telescopeName : names){
			proposals.addAll( northStarDelegate.getOwnProposals(
					true, telescopeName, ownUserAccount));
		}
		
		for(Proposal proposal : proposals){
			//proposal.getJustification().getEnvelopes();
		}
		
		return envelopeNames;
		
	}
}

