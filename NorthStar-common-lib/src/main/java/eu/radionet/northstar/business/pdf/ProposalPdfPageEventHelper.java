// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.pdf;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

public class ProposalPdfPageEventHelper extends PdfPageEventHelper {
    private Log log = LogFactory.getLog(this.getClass());

    protected PdfPTable firstHeaderTable = null;

    protected ProposalWriter documentWriter = null;

    protected PdfPTable headerTable = null;
	
	protected float firstHeaderSize = 0;
	
	protected float otherHeaderSize = 0;
    protected float footerPosition = 0;

    public ProposalPdfPageEventHelper(PdfPTable firstHeaderTable,
            PdfPTable headerTable, float footerPosition,float firstHeaderSize, float otherHeaderSize,
            ProposalWriter documentWriter) {
        super();
		this.firstHeaderSize = firstHeaderSize;
		this.otherHeaderSize = otherHeaderSize;
        this.headerTable = headerTable;
        this.firstHeaderTable = firstHeaderTable;
        this.documentWriter = documentWriter;
        this.footerPosition = footerPosition;

    }

    public ProposalPdfPageEventHelper(
            PdfPTable headerTable, float otherHeaderSize ) {
        super();

		this.otherHeaderSize = otherHeaderSize;		
        this.headerTable = headerTable;


    }

    public void onOpenDocument(PdfWriter writer, Document document) {

    }

    public void onEndPage(PdfWriter writer, Document document) {
        PdfContentByte cb = writer.getDirectContent();
        cb.saveState();
        if (document.getPageNumber() == 1 && firstHeaderTable != null) {
			float headTopMargin = document
            .getPageSize().getHeight() - (document.topMargin()-firstHeaderSize-otherHeaderSize);
            firstHeaderTable.setTotalWidth(document.right() - document.left());
            firstHeaderTable.writeSelectedRows(0, -1, document.left(), headTopMargin, cb);
        } else {
			float headTopMargin = document
            .getPageSize().getHeight() - (document.topMargin()-otherHeaderSize);
            headerTable.setTotalWidth(document.right() - document.left());
            headerTable.writeSelectedRows(0, -1, document.left(),headTopMargin, cb);
        }
        if (documentWriter != null) {
            try {
                PdfPTable footerTable = documentWriter.getFooter();
                footerTable.setTotalWidth(document.right() - document.left());
                footerTable.writeSelectedRows(0, -1, document.left(),
                        this.footerPosition, cb);

            } catch (DocumentException de) {
                log.error("DocumentException: " + de.getMessage());
            }
        }
    }

    public void onStartPage(PdfWriter writer, Document document) {

    }

    public void onCloseDocument(PdfWriter writer, Document document) {

    }

}
