// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Mar 27, 2006
 *
 *
 */
package eu.opticon.northstar.business;

public class OpticonConstants
{
    /* 
     * Define your telescope dependand constants here
     * The ones defined here, are used for time allocation using the
     * Resource objects
    */
	public static final String TOTAL_DURATION = "totalDuration";
    
	public static final String DARK = "Dark";    
    public static final String FIRST_Q = "FirstQ";    
    public static final String LAST_Q = "LastQ";    
    public static final String BRIGHT = "Bright";    
    public static final String GREY = "Grey";    
    public static final String GLOBAL = "Global";  
    public static final String AWARDED = "Awarded";
    public static final String LONGTERM = "LongTerm";

    public static final String AWARDEDDARK = "awardedDark";
    public static final String LONGTERMDARK = "longTermDark";
    public static final String USEFULDARK = "usefulDark";
    public static final String AWARDEDBRIGHT = "awardedBright";
    public static final String LONGTERMBRIGHT = "longTermBright";
    public static final String USEFULBRIGHT = "usefulBright";
    public static final String AWARDEDGREY = "awardedGrey";
    public static final String LONGTERMGREY = "longTermGrey";
    public static final String USEFULGREY = "usefulGrey";
    public static final String LONGTERMSTORAGE = "longTermStorage";
    public static final String USEFULGLOBAL = "usefulGlobal";
    public static final String REQUESTEDTIME = "requestedTime";
    public static final String MINIMUMTIME = "minimumTime";
    public static final String EXTRA_LONGTERM_PAGECOUNT = "extra-longterm-pages";
    
    // Targets use a different string and needs a different constant
    public static final String TARGET_FIRST_Q = "first quarter";    
    public static final String TARGET_LAST_Q = "last quarter";    
    
    public static final String TIME = "time";
	public static final String SECONDS = "seconds";
    public static final String MINUTES = "minutes";
    public static final String HOURS = "hours";
    public static final String NIGHTS = "nights";
	

	public static final String REQUIRED_SCHEDULING_CONSTRAINTS = "requiredSchedulingConstraints";
	public static final String PREFERRED_SCHEDULING_CONSTRAINTS = "preferredSchedulingConstraints";

	public static final String OBSERVATION_REQUIRED_SCHEDULING_CONSTRAINTS = "observationRequiredSchedulingConstraints";
	public static final String OBSERVATION_PREFERRED_SCHEDULING_CONSTRAINTS = "observationPreferredSchedulingConstraints";
	
	public static final String MAX_TARGETS = "maxTargets";
	
	public static final String STORAGECALCULATORURL = "storageCalculatorUrl";
	
	public static final String ALLOCATION_JUSTIFICATION = "allocationJustification";
	public static final String SCIENCE_CATEGORY = "scienceCategory";
	public static final String SCIENCE_CATEGORIES = "scienceCategories";
	public static final String NOISE_LEVEL = "noiseLevel";
	public static final String NIGHT_TIME = "nightTime";
	public static final String OBSERVING_MODE = "observingMode";
	public static final String CALIBRATION_REQUIREMENTS = "calibrationRequirements";
	//public static final String NEW_OBSERVER_EXPERIENCE = "newObserverExperience";
	public static final String GRANT_NUMBER = "grantNumber";
	public static final String TRAVEL = "travel";
	public static final String BACKUPSTRATEGY = "backupStrategy";
	public static final String OTHER_EXPENDITURE = "otherExpenditure";
	public static final String OPTICON_FUNDING = "opticonFunding";
	public static final String PROPRIETARY = "proprietaryException";
	public static final String DISPLAY_PIGGYBACK = "piggyBack";
	public static final String DISPLAY_PIGGYBACK_MODE="instrumentPiggyBack";
	public static final String DISPLAY_REQUESTED_TIME = "enableRequestedTime";
	public static final String DISPLAY_LONGTERM_PROPOSAL = "longTermProposal";
	public static final String DISPLAY_LONGTERM_TIME = "enableLongTermTime";
	public static final String DISPLAY_LONGTERM_STORAGE = "enableLongTermStorage";
	public static final String DISPLAY_TOTAL_TIME = "enableTotalTime";
	public static final String USEFUL_TIME = "enableUsefulTime";
	public static final String AWARDED_TIME = "enableAwardedTime";
	public static final String OBSERVATION_DURATION = "observationDuration";
	public static final String OBSERVATION_TIME = "observationTime";
	public static final String OBSERVATION_PHASE = "observationPhase";// moonPhase
	public static final String OBSERVATION_MODE = "observationMode";
	public static final String OBSERVATION_SEEING = "observationSeeing";
	public static final String OBSERVATION_WEATHER = "observationWeather";
	public static final String OBSERVATION_DATES = "observationDates";
	public static final String OBSERVATION_NOISE_LEVEL = "observationNoiseLevel";
	public static final String OBSERVATION_SINGLETARGET = "observationSingleTarget";
	
	public static final String TARGET_RA = "targetRa";
	public static final String TARGET_DEC = "targetDec";
	public static final String TARGET_MOON = "moon";
	public static final String TARGET_SEEING = "seeing";
	public static final String TARGET_WATER = "water";
	public static final String TARGET_SEEING_RANGE = "targetSeeingRange";
	public static final String TARGET_FLUX = "targetFlux";
	public static final String TARGET_FLUX_FREQUENCY = "targetFluxFrequency";
	
	public static final String TARGET_SPECIFY_SUBBAND = "targetSpecifySubband";
	public static final String TARGET_CENTRAL_FREQUENCY = "targetCentralFrequency";
	public static final String TARGET_BANDWIDTH = "targetBandWidth";
	public static final String TARGET_SUBBAND_LIST = "targetSubbandList";
	
	public static final String TARGET_TOTAL_SUBBAND_LIST = "targetTotalSubbandList";
	
	public static final String TARGET_SPECTRAL_INDEX = "targetSpectralIndex";
	public static final String TARGET_SKY_QUALITIES = "targetSkyQualities";
	public static final String TARGET_SN = "targetSn";
	public static final String TARGET_DIAMETER = "targetDiameter";
	public static final String TARGET_MAGNITUDE = "targetMagnitude";
	public static final String TARGET_EXPOSURE_TIME = "targetExposureTime";
	public static final String TARGET_RUNS = "targetRuns";
	public static final String TARGET_SINGLE_PER_RUN = "singleTargetPerRun";
	public static final String TARGET_OPPORTUNITY = "targetOpportunity";
	public static final String TARGET_SELECT_PIPELINE = "targetSelectPipeline";
	public static final String TARGET_SELECT_RUNNUMBER = "targetSelectRunnumber";
	
	public static final String TELESCOPE_CONFIGURATIONS = "telescopeConfigurations";
	public static final String INSTRUMENT_CONFIGURATIONS = "instrumentConfigurations";
	public static final String INSTRUMENT_FILTER = "instrumentFilter";
	public static final String INSTRUMENT_CUSTOM_FILTER = "instrumentCustomFilter";
	public static final String INSTRUMENT_FILTERW2 = "instrumentFilterw2";
	public static final String INSTRUMENT_SLITW = "instrumentSlitw";
	public static final String INSTRUMENT_GRISMW = "instrumentGrismw";
	public static final String INSTRUMENT_GRATINGW = "instrumentGratingw";	
	public static final String INSTRUMENT_FILTER_DETAILS = "instrumentFilterDetails";
	public static final String INSTRUMENT_MODE = "instrumentMode";
	public static final String INSTRUMENT_READOUT = "instrumentReadOut";
	public static final String INSTRUMENT_ORDER_FILTER = "instrumentOrderFilter";
	public static final String INSTRUMENT_POLARIMETRY = "instrumentPolarimetry";
	public static final String INSTRUMENT_GUIDELINE = "instrumentGuideline";
	public static final String INSTRUMENT_GUIDELINE_URL = "instrumentGuidelineUrl";
	public static final String INSTRUMENT_WAVELENGTH = "instrumentWavelength";
	public static final String INSTRUMENT_ORDER = "instrumentOrder";
	public static final String INSTRUMENT_SLIT = "instrumentSlit";
	public static final String INSTRUMENT_SLIT_POSITION_ANGLE = "instrumentSlitPositionAngle";
	public static final String INSTRUMENT_GRATING = "instrumentGrating";
	public static final String INSTRUMENT_GRISM = "instrumentGrism";
	public static final String INSTRUMENT_COMMENTS = "instrumentComments";
	public static final String INSTRUMENT_FRAME_EXPOSURE_TIME_DETAILS = "instrumentFrameExposureTimeDetails";
	public static final String INSTRUMENT_MICROSTEPPING = "instrumentMicrostepping";
	public static final String INSTRUMENT_CORONAGRAPHIC_MASK = "instrumentCoronagraphicMask";
	public static final String INSTRUMENT_CCD = "instrumentCCD";
	
	// for lofar
	public static final String DISPLAY_POSTPROCESSING = "displayPostProcessing";
	public static final String DISPLAY_SPONSORING = "displaySponsoring";
	public static final String DISPLAY_CALCULATED_SIZE = "displayCalculatedSize";
	public static final String DISPLAY_DIRECT_DATASTORAGE = null;
	
	public static final String INSTRUMENT_STATION = "instrumentStation";
	public static final String INSTRUMENT_CLOCK = "instrumentClock";
	public static final String INSTRUMENT_ONE_FILTER = "instrumentOneFilter";
	public static final String INSTRUMENT_ANTENNA = "instrumentAntenna";
	public static final String INSTRUMENT_SUBARRAY_POINTINGS = "instrumentSubarrayPointings";
	public static final String INSTRUMENT_STORE_RAW_DATA = "instrumentStoreRawData";
	public static final String INSTRUMENT_CORRELATED_VISIBILITIES = "instrumentCorrelatedVisibilities";
	public static final String INSTRUMENT_STORE_CORRELATED_VISIBILITIES = "instrumentStoreUVData";
	public static final String INSTRUMENT_INTEGRATION_TIME = "instrumentIntegrationTime";
	public static final String INSTRUMENT_SINGLE_STATION = "singleStationSpecifics";
	public static final String TARGET_STORAGE = "targetStorage";
	public static final String POLARIZATIONS = "polarizations";
	public static final String INCPOLARIZATIONS = "incPolarizations";
	public static final String CHANNELS = "channels";

	public static final String INSTRUMENT_DIRECT_DATASTORAGE = "instrumentDirectDataStorage";
	
	public static final String POST_IMAGING = "postImaging";
	public static final String POST_INTERFERO = "postInterfero";
	public static final String POST_TIMESERIES = "postTimeseries";
	public static final String POST_BEAMFORMED= "postBeamformed";

	public static final String DISPLAY_OBSERVING_DETAILS = "observingDetails";
	public static final String SUMMARY_TOTALTIME = "summaryTotalTime";

	public static final String TELESCOPE_MODE = "telescopeMode";
	public static final String TELESCOPE_MOON = "telescopeMoon";
	public static final String TELESCOPE_SCHEDULING = "telescopeScheduling";
	public static final String TELESCOPE_HOURS = "telescopeHours";
	public static final String INSTRUMENT_URL = "instrumentUrl";
	public static final String DATA = "Data";
	public static final String GB = "GB";
	
	public static final String DISPLAY_PIPELINE = "enablePipeline";
	public static final String POST_AVERAGING = "postAveraging";
	public static final String DISPLAY_PROCESSING_MODE = "processingMode";
	public static final String DISPLAY_SUBBANDS_PER_IMAGE = "subbandsPerImage";
	public static final String DISPLAY_FIELD_OF_VIEW = "fieldOfView";
	public static final String DISPLAY_FLAGGING_STRATEGY = "flaggingStrategy";
	public static final String DISPLAY_POST_AVERAGING = "postAveraging";
	public static final String DISPLAY_POST_DEMIXING = "postDemixing";
	
	public static final String PROCESSING_MODES = "processingModes";
	public static final String FLAGGING_STRATEGIES = "flaggingStrategies";
	
	public static final String DATASTORAGE = "Storage";
	
	public static final String SHOW_CHILDREN = "showChildren";
	public static final String SELECT_PARENT = "selectParent";
	
	public static final String DEMIXING_SOURCE = "demixingSource";
	public static final String TARGET_CALIBRATION_BEAM = "targetCalibrationBeam";
	

	
	public static final String DATA_STORE_ALLOW = "dataStoreAllow";
	public static final String DATA_STORE_MODE = "dataStoreMode";
	public static final String BEAM_DATA_SIZE = "beamDataSize";
	public static final String CORRELATED_VISIBILITY_DATA_SIZE = "uvDataSize";
	public static final String INTERFEROMETER_DATA_SIZE = "infDataSize";
	public static final String TOTAL_DATA_SIZE = "totalDataSize";
	
	//for processing time calculation
	
	public static final String PROCESSING_MODE_CALIBRATION ="Calibration";
	public static final String PROCESSING_MODE_CALIBRATION_AND_IMAGING="Calibration + imaging";
	public static final String PROCESSING_MODE_PREPROCESSING_ONLY ="Pre processing only";
	public static final String PROCESSING_MODE_PULSAR="Pulsar pipeline";
	public static final String PROCESSING_MODE_USER_SPECIFIED="User specified pipeline";
	public static final String PROCESSING_MODE_BASELINE="Long baseline calibration";
	public static final String PROCESSING_TIME_BASELINE_FACTOR ="long-basline-calibration--processing-time-facto";
	public static final String PROCESSING_TIME_CALIBRATION ="Calibration Time";
	public static final String PROCESSING_TIME_IMAGING ="Imating Time";
	public static final String PROCESSING_TIME_RATIO ="P/O Ratio";
	public static final String PROCESSING_TIME_TOTAL ="Total Processing Time (Hours)";

	public static final String PROCESSING_TIME_RATIO_IMAGING = "P/O Ratio imaging";

	public static final String UV_DATA_STORE_ALLOWE = "uvDataStoreAllow";

	public static final String COHTABEAMS = "cohTaBeams";

	public static final String INCOHTABEAMS = "incohTaBeams";

	public static final String INSTEPS = "intsteps";
	
	public static final String MAX_COHTABEAMS_VALUE = "max-cohtabeams-value";
	public static final String MAX_INCOHTABEAMS_VALUE = "max-incohtabeams-value";
	public static final String MIN_COHTABEAMS_VALUE = "min-cohtabeams-value";
	public static final String MIN_INCOHTABEAMS_VALUE = "min-incohtabeams-value";
	
	/***
	 * fields for TBB
	 */
	public static final String MAX_TRIGGER_LENGTH = "max-trigger-length";
	public static final String MIN_TRIGGER_LENGTH = "min-trigger-length";
	
	public static final String TBB_PIGGYBACK="TBBPIGGYBACK";
	public static final String TBB_TRIGGER_SOURCE= "tbbTriggerSource";
	public static final String TBB_OBSERVATION_LIST= "ObservationsListforStandAloneMode";
	public static final String TBB_EXPECTED_TRIGGER_RATE ="instrumentExpectedTriggerRate";
	public static final String TBB_TRIGGER_LENGTH="instrumentTriggerLength";
	public static final String TBB_EXPOUSERE_TIME="tbbExposureTime";
	public static final String TBB_REQUESTED_STAND_ALONE_TIME="requestedStandAloneTime";

	public static final String TBB_DATA_SIZE = "tbbDataSize";

	public static final String DISPLAY_PROCESSING_TIME = "enableDisplayProcessingTime";

	public static final String TOTAL_PROCESSING_TIME = "totalProcessingTime";
	
	public static final String CONFIG_FILE_LOCATION = "/conf/modules/opticon/lofar-options.xml";
}