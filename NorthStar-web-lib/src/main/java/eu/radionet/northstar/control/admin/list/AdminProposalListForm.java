// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.struts.action.ActionForm;

import eu.radionet.northstar.business.configuration.CommunityConfiguration;

public class AdminProposalListForm extends ActionForm{
	private String[] selectedStatusses = new String[] { "submitted",
			"under review" };
	private List adminTelescopeBeans = new ArrayList();
	private List statusses = new ArrayList();

	private String[] selectedTelescopes = null;

	private List telescopeList = new ArrayList();

	private String[] selectedCategories = new String[] { "regular" };

	private List categories = new ArrayList();
	
	private String[] selectedSemesters = null;
	
	private Set semesters = new TreeSet();

	private String[] selectedCommunities = null;
	
	private List communities = new ArrayList();
	
	private List yearList = new ArrayList();
	
	private String selectedYear = null;
	
	private String allStatusses = null;

	private String allCategories = null;

	private String filterButton = null;

	private String xmlButton = null;
	private String listButton = null;
	private String allButton = null;
	private String abstractButton = null;
	private String viewStatisticsButton = null;
	private boolean forceProjectId = false;
	
	// private List semesters = new ArrayList();
	public List getCategories() {
		return categories;
	}

	public void setCategories(List categories) {
		this.categories = categories;
	}

	public String[] getSelectedStatusses() {
		return selectedStatusses;
	}

	public void setSelectedStatusses(String[] selectedStatusses) {
		this.selectedStatusses = selectedStatusses;
	}

	public String[] getSelectedTelescopes() {
		return selectedTelescopes;
	}

	public void setSelectedTelescopes(String[] selectedTelescopes) {
		this.selectedTelescopes = selectedTelescopes;
	}

	public List getStatusses() {
		return statusses;
	}

	public void setStatusses(List statusses) {
		this.statusses = statusses;
	}
	
	public List getYearList() {
		return this.yearList;
	}
	
	public String getSelectedYear() {
		return this.selectedYear;
	}

	public void setSelectedYear(String selectedYear) {
		this.selectedYear = selectedYear;
	}

	public void setYearList(List yearlist) {
		this.yearList = yearlist;
	}

	public List getTelescopes() {
		return telescopeList;
	}
	
	public List getTelescopeList() {
		return telescopeList;
	}

	public void setTelescopeList(List telescopelist) {
		if (selectedTelescopes == null) {
			String[] selectedTelescopes = new String[telescopelist.size()];
			for (int i = 0; i < telescopelist.size(); i++) {
				selectedTelescopes[i] = (String) telescopelist.get(i);
			}
			this.selectedTelescopes = selectedTelescopes;
		}
		this.telescopeList = telescopelist;
	}

	public String[] getSelectedCategories() {
		return selectedCategories;
	}

	public void setSelectedCategories(String[] selectedCategories) {
		this.selectedCategories = selectedCategories;
	}

	public String getAllCategories() {
		return allCategories;
	}

	public void setAllCategories(String allCategories) {
		this.allCategories = allCategories;
	}

	public String getAllStatusses() {
		return allStatusses;
	}

	public void setAllStatusses(String allStatusses) {
		this.allStatusses = allStatusses;
	}

	public String getFilterButton() {
		return filterButton;
	}

	public void setFilterButton(String filterButton) {
		this.filterButton = filterButton;
	}

	public String getAllButton() {
		return allButton;
	}

	public void setAllButton(String allButton) {
		this.allButton = allButton;
	}
	
	public String[] getSelectedSemesters() {
		return selectedSemesters;
	}

	public void setSelectedSemesters(String[] selectedSemesters) {
		this.selectedSemesters = selectedSemesters;
	}

	public Set getSemesters() {
		return semesters;
	}

	public void setSemesters(Set semesters) {
		if (selectedSemesters == null) {
			String[] selectedSemesters = new String[semesters.size()];
			Iterator iterator = semesters.iterator();
			int i = 0;
			while (iterator.hasNext()){
				
				selectedSemesters[i] = (String) iterator.next();
				i++;
			}
			this.selectedSemesters = selectedSemesters;
		}
		this.semesters = semesters;
	}

	public String getListButton() {
		return listButton;
	}

	public void setListButton(String listButton) {
		this.listButton = listButton;
	}

	public String getXmlButton() {
		return xmlButton;
	}

	public void setXmlButton(String xmlButton) {
		this.xmlButton = xmlButton;
	}


	public List getCommunities() {
		return communities;
	}

	public void setCommunities(List communities) {
		if (selectedCommunities == null) {
			String[] selectedCommunities = new String[communities.size()];
			Iterator communitiesIterator = communities.iterator();
			int i = 0;
			while (communitiesIterator.hasNext()){
				CommunityConfiguration communityConfiguration = (CommunityConfiguration) communitiesIterator.next();
				selectedCommunities[i] = communityConfiguration.getName();
				i++;
			}
			this.selectedCommunities = selectedCommunities;
		}	
		this.communities = communities;
	}

	public String[] getSelectedCommunities() {
		return selectedCommunities;
	}

	public void setSelectedCommunities(String[] selectedCommunities) {
		this.selectedCommunities = selectedCommunities;
	}

	public List getAdminTelescopeBeans() {
		return adminTelescopeBeans;
	}

	public void setAdminTelescopeBeans(List proposalBeans) {
		this.adminTelescopeBeans = proposalBeans;
	}    
    public AdminTelescopeBean getAdminTelescopeBean(int index) {
    	resizeAdminTelescopeBeans(index);
        return (AdminTelescopeBean) adminTelescopeBeans.get(index);

    }

    private void resizeAdminTelescopeBeans(int index) {
        int newSize = index + 1;
        if (adminTelescopeBeans.size() < newSize) {
            for (int i = adminTelescopeBeans.size(); adminTelescopeBeans.size() < newSize; i++){
            	adminTelescopeBeans.add(new AdminTelescopeBean());
            }
        }
    }

	public boolean isForceProjectId() {
		return forceProjectId;
	}

	public void setForceProjectId(boolean forceProjectId) {
		this.forceProjectId = forceProjectId;
	}

	public String getAbstractButton() {
		return abstractButton;
	}

	public void setAbstractButton(String abstractButton) {
		this.abstractButton = abstractButton;
	}
	
	public String getViewStatisticsButton() {
		return viewStatisticsButton;
	}

	public void setViewStatisticsButton(String viewStatisticsButton) {
		this.viewStatisticsButton = viewStatisticsButton;
	}


}
