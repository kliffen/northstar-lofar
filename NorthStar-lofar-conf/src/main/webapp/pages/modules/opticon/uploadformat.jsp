Format reminder: <br/>
<b>(note that the order of columns has changed as of January 2018! Run number is now mandatory and has been moved up to column 5)</b>
<pre class="courier">
Fieldname RA Dec Epoch Run# [Calibrator [Exposure_time [Flux [Ref_freq [Spectral_index [Subband list [Observation [Pipeline 1; Pipeline 2; ... [Comments]]]]]]]]]
</pre>
Only Fieldname RA Dec Epoch and Run# are required, when entering other fields, all fields before it have to be filled in. Multiple pipelines should be seperared by semicolon<br>
<p> An example:
<pre class="courier">
m31,00:48:31.20,+12:09:00.0,j2000,1,N,11m 
"hd 39801",05:55:10.31,+07:24:25.4,j2000,2,Y,7200s,1,100,,12-120,A,A;B,on different days
m32,00:42:41.87,+40:51:57.2,j2000,2
</pre>
</p>

