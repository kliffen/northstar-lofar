// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessApplicantAction.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: ProcessThesisAction.java,v 1.5 2008-04-08 08:03:00 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.additionalissues.thesis;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.AdditionalIssues;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Thesis;

/**
 * The ProcessApplicantAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessThesisAction extends LockedAction {


	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/*
		 * if pressed on Discard changes
		 */
		if (isCancelled(request)) {
			return mapping.findForward("additionalIssues");

		}
		HttpSession session = request.getSession();
		ThesisForm thesisForm = (ThesisForm) form;

        /*
         * if pressed on commit changes
         */
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
        if (thesisForm.getCommitThesisButton()!=null) {
            
            Proposal proposal = proposalData.getProposal();
            AdditionalIssues additionalIssues = proposal.getAdditionalIssues();

            Thesis thesis = null;
            
            /*
             * if thesisId is not blank, it must be edit.
             */
            if (!AstronValidator.isBlankOrNull(thesisForm.getThesisId())) {
                thesis = (Thesis) additionalIssues.getTheses().get(
                        AstronConverter.toInteger(thesisForm.getThesisId())
                                .intValue());
            }
            /*
             * else it must be add.
             */
            else {
                thesis = new Thesis();
            }

            if (!AstronValidator.isBlankOrNull(thesisForm.getStudentRef())) {
                Member member = (Member) proposal.getMembers().get(
                        AstronConverter.toInteger(thesisForm.getStudentRef())
                                .intValue());
                thesis.setStudent(member);
            } else {
                thesis.setStudent(null);
                thesis.setStudentName(thesisForm.getStudentName());
            }
            thesis.setStudentLevel(thesisForm.getStudentLevel());           
            if (!AstronValidator.isBlankOrNull(thesisForm.getSupervisorRef())) {
                Member member = (Member) proposal.getMembers().get(
                        AstronConverter.toInteger(thesisForm.getSupervisorRef())
                                .intValue());
                thesis.setSupervisor(member);
            } else {
                thesis.setSupervisor(null);
                thesis.setSupervisorName(thesisForm.getSupervisorName());
            }

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
                    Constants.DATE_FORMAT);

            thesis.setExpectedCompletionDate(simpleDateFormat
                    .parse(thesisForm.getExpectedCompletionDate()));
            
            thesis.setDataRequired(thesisForm.isDataRequired());

            additionalIssues.addChangeThesis(thesis);

            session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
            return mapping.findForward("additionalIssues");
        }
        

        /*
         * Refresh: Refill the list of members if required
         */
        if (thesisForm.isStudentApplicant() || thesisForm.isSupervisorApplicant()) {
            Proposal proposal = proposalData.getProposal();
            /*
             * iterator through members
             */
            Iterator membersIterator = proposal.getMembers().iterator();
            List memberBeans = new ArrayList();
            
            int i = 0;
            while (membersIterator.hasNext()) {
                MemberBean memberBean = new MemberBean();
                memberBean.setId(new Integer(i));

                Member member = (Member) membersIterator.next();
                memberBean.setMember(member);

                i++;

                memberBeans.add(memberBean);

            }
            thesisForm.setMemberBeans(memberBeans);
        }
        /*
         * Refresh: Refill the student level list.
         * TODO: change this to a xml option configuration.
         */
        thesisForm.initStudentLevels();        
        
        return mapping.getInputForward();
	}

}
