ALTER TABLE `additionalissues` ADD COLUMN `relatedpublications` text;
ALTER TABLE `opticonobservingrequest` ADD COLUMN `backup_strategy` text;
ALTER TABLE `opticonobservation`
 ADD COLUMN `time` VARCHAR(255) DEFAULT null,
 ADD COLUMN `weather` VARCHAR(255) DEFAULT null,
 ADD COLUMN `mode` VARCHAR(255) DEFAULT null,
 ADD COLUMN `seeing` VARCHAR(255) DEFAULT null,
 ADD COLUMN `preferredDates` VARCHAR(255) DEFAULT null;