// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.email;

import javax.naming.NamingException;

import nl.astron.useradministration.data.entities.User;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronMailUtility;
import nl.astron.util.AstronValidator;
import nl.astron.util.exception.AstronMailException;
import eu.radionet.northstar.business.configuration.EmailConfiguration;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.admin.StatusEmail;

public class EmailGenerator {
	protected static final String SUBJECT_START = "<SUBJECT>";

	protected static final String SUBJECT_END = "</SUBJECT>";

	/**
	 * Sends
	 * 
	 * @param statusEmail
	 * @param emailConfiguration
	 * @param message
	 * @param userAccount
	 * @throws AstronMailException
	 * @throws NamingException
	 * @throws DatabaseException
	 */
	protected StatusEmail sendGeneratedStatusChangeEmail(Proposal proposal,
			EmailConfiguration emailConfiguration, String message)
			throws AstronMailException, NamingException {
		AstronMailUtility mailUtility = AstronMailUtility.getInstance();
		String subject = "";
		if (message.startsWith(SUBJECT_START)) {
			message = message.substring(SUBJECT_START.length());
			int first = message.indexOf(SUBJECT_END);
			subject = message.substring(0, first);
			message = message.substring(first + SUBJECT_END.length());
			if (message.startsWith("\n")) {
				message = message.substring("\n".length());
			}
		}
		String emailSender = NorthStarConfiguration.getEmailSender();
		String[] bcc = getMailAdresses(emailConfiguration.getBcc());
		String toRecipient = proposal.getContactAuthor().getEmail();
		mailUtility.sendMail(emailSender, new String[] { toRecipient }, null,
				bcc, subject, message.toString());
		StatusEmail statusEmail = new StatusEmail();
		statusEmail.setToRecipients(toRecipient);
		statusEmail.setBccRecipients(emailConfiguration.getBcc());
		statusEmail.setSender(emailSender);
		statusEmail.setSubject(subject);
		statusEmail.setMessage(message);
		statusEmail.setProposalId(proposal.getId());
		statusEmail.setProposalStatusId(proposal.getCurrentStatus()
				.getId());
		statusEmail.setGeneratedMail(true);
		return statusEmail;
	}

	protected StatusEmail sendStatusChangeEmail(Proposal proposal,
			EmailConfiguration emailConfiguration, String subject,
			String message) throws AstronMailException, NamingException {
		AstronMailUtility mailUtility = AstronMailUtility.getInstance();
		StatusEmail statusEmail = new StatusEmail();
		String emailSender = NorthStarConfiguration.getEmailSender();
		String toRecipient = proposal.getContactAuthor().getEmail();
		String[] bcc = null;
		if (emailConfiguration != null) {
			String bccRecipient = emailConfiguration.getBcc();
			bcc = getMailAdresses(bccRecipient);
			statusEmail.setBccRecipients(emailConfiguration.getBcc());			
		}
		String[] cc = new String[proposal.getMembers().size() - 1];
		int ccIndex = 0;
		for (int i = 0; i < proposal.getMembers().size(); i++) {
			Member member = (Member) proposal.getMembers().get(i);
			if (!member.isContactAuthor()) {
				cc[ccIndex] = member.getEmail();
				ccIndex++;
			}
		}

		mailUtility.sendMail(emailSender, new String[] { toRecipient }, cc,
				bcc, subject, message);

		statusEmail.setToRecipients(toRecipient);
		statusEmail.setCcRecipients(getMailAdresses(cc));
		statusEmail.setSender(emailSender);
		statusEmail.setSubject(subject);
		statusEmail.setMessage(message);
		statusEmail.setProposalId(proposal.getId());
		statusEmail.setProposalStatusId(proposal.getCurrentStatus()
				.getId());
		statusEmail.setGeneratedMail(false);
		return statusEmail;
	}

	protected void sendEmail(EmailConfiguration emailConfiguration,
			String message, UserAccount userAccount) throws AstronMailException,
			NamingException {
		AstronMailUtility mailUtility = AstronMailUtility.getInstance();
		String subject = "";
		if (message.startsWith(SUBJECT_START)) {
			message = message.substring(SUBJECT_START.length());
			int first = message.indexOf(SUBJECT_END);
			subject = message.substring(0, first);
			message = message.substring(first + SUBJECT_END.length());
			if (message.startsWith("\n")) {
				message = message.substring("\n".length());
			}
		}

		String[] bcc = getMailAdresses(emailConfiguration.getBcc());
		mailUtility.sendMail(NorthStarConfiguration.getEmailSender(),
				new String[] { userAccount.getUser().getEmail() }, null, bcc,
				subject, message.toString());
	}

	protected void sendEmail(EmailConfiguration emailConfiguration,
			String message, String email) throws AstronMailException,
			NamingException {
		AstronMailUtility mailUtility = AstronMailUtility.getInstance();
		String subject = "";
		if (message.startsWith(SUBJECT_START)) {
			message = message.substring(SUBJECT_START.length());
			int first = message.indexOf(SUBJECT_END);
			subject = message.substring(0, first);
			message = message.substring(first + SUBJECT_END.length());
			if (message.startsWith("\n")) {
				message = message.substring("\n".length());
			}
		}

		String[] bcc = getMailAdresses(emailConfiguration.getBcc());
		mailUtility.sendMail(NorthStarConfiguration.getEmailSender(),
				new String[] { email }, null, bcc, subject, message.toString());
	}

	public static String getEmailName(User user) {
		String name = null;
		if (AstronValidator.isBlankOrNull(user.getTitle())) {
			name = user.getFirstName();
		} else {
			name = user.getTitle();
		}
		name += " " + user.getLastName();
		return name;
	}

	public String[] getMailAdresses(String adresses) {
		if (!AstronValidator.isBlankOrNull(adresses)) {
			return adresses.split(",");
		} else {
			return null;
		}
	}

	public String getMailAdresses(String[] addresses) {
		String addressesString = null;
		for (int i = 0; i < addresses.length; i++) {
			if (i > 0) {
				addressesString = addressesString + "," + addresses[i];
			} else {
				addressesString = addresses[i];
			}
		}
		return addressesString;

	}
}
