// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class ProposalForm extends ActionForm {
	protected boolean userIsContactAuthor = false;
    protected String forward = null;
    protected String saveButton = null;
    protected String saveAndExitButton = null;
    protected String saveAndSubmitButton = null;
    protected String saveAndViewButton = null;
	public String getSaveAndExitButton() {
		return saveAndExitButton;
	}
	public void setSaveAndExitButton(String saveAndExitButton) {
		this.saveAndExitButton = saveAndExitButton;
	}
	public String getSaveAndSubmitButton() {
		return saveAndSubmitButton;
	}
	public void setSaveAndSubmitButton(String saveAndSubmitButton) {
		this.saveAndSubmitButton = saveAndSubmitButton;
	}
	public String getSaveAndViewButton() {
		return saveAndViewButton;
	}
	public void setSaveAndViewButton(String saveAndViewButton) {
		this.saveAndViewButton = saveAndViewButton;
	}
	public String getSaveButton() {
		return saveButton;
	}
	public void setSaveButton(String saveButton) {
		this.saveButton = saveButton;
	}
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
    public void reset(ActionMapping mapping,
            HttpServletRequest request) {
        this.saveAndExitButton = null;
        this.saveAndSubmitButton = null;
        this.saveAndViewButton = null;
        this.saveButton = null;
        this.forward = null;
    }
	public boolean isUserIsContactAuthor() {
		return userIsContactAuthor;
	}
	public void setUserIsContactAuthor(boolean userIsContactAuthor) {
		this.userIsContactAuthor = userIsContactAuthor;
	}
}
