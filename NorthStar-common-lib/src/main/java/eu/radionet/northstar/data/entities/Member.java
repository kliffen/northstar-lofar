// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Member.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: Member.java,v 1.1 2006-05-02 10:01:10 verhoef Exp $
 *
 */
package eu.radionet.northstar.data.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import eu.radionet.northstar.data.Constants;

/**
 * The Member provides
 *
 * @author Bastiaan Verhoef
 *
 */
public abstract class Member implements Serializable{
    protected Integer id = null;
    protected Integer fakeId = null;
    private List roles = new ArrayList();
    public abstract String getName();
    public abstract  String getAffiliation();
    public abstract  String getEmail();
    public abstract  String getCountry();
    private boolean pi = false;
    private boolean contactAuthor = false;
    private boolean potentialObserver = false;


    /**
     * @return Returns the id.
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id The id to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return Returns the roles.
     */
    public List getRoles() {
        return roles;
    }
    /**
     * @param roles The roles to set.
     */
    public void setRoles(List roles) {
        
        this.roles = roles;
        Iterator rolesIterator = roles.iterator();
        while (rolesIterator.hasNext()){
            setRoleSettings ((Role) rolesIterator.next());
            
        }
    }
    public void addRole(Role role){
        this.roles.add(role);
        setRoleSettings(role);
    }
    public void removeRole(Role role){
        if (role.id.intValue() == Constants.PI){
            pi = false;
        }
        if  (role.id.intValue() == Constants.CONTACT_AUTHOR){
            this.contactAuthor = false;
        }
        if (role.id.intValue() == Constants.POTENTIAL_OBSERVER){
            this.potentialObserver = false;
        }
		boolean found = false;
		for (int i = 0; i < roles.size() && !found; i ++){
			Role currentRole = (Role) roles.get(i);
			if (currentRole.getId().intValue() == role.id.intValue()){
				roles.remove(i);
				found = true;
			}
		}

    }
    public void removeRole(int roleId){
        boolean found = false;
        Iterator rolesIterator = roles.iterator();
        while (rolesIterator.hasNext() && !found){
            Role role =  (Role) rolesIterator.next();
            if (role.getId().intValue() == roleId){
                found = true;
                removeRole(role);
            }
        }

    }
    protected void setRoleSettings(Role role){
        if (role.id.intValue() == Constants.PI){
            pi = true;
        }
        if  (role.id.intValue() == Constants.CONTACT_AUTHOR){
            this.contactAuthor = true;
        }
        if (role.id.intValue() == Constants.POTENTIAL_OBSERVER){
            this.potentialObserver = true;
        }
    }
    public boolean isPi(){
        return pi;
    }
    public boolean isContactAuthor(){
        return contactAuthor;
    }
    public boolean isPotentialObserver(){
        return potentialObserver;
    }
    /**
     * @return Returns the fakeId.
     */
    public Integer getFakeId() {
        return fakeId;
    }
    /**
     * @param fakeId The fakeId to set.
     */
    public void setFakeId(Integer fakeId) {
        this.fakeId = fakeId;
    }

}
