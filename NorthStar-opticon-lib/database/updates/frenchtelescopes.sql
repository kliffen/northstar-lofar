ALTER TABLE `opticonobservingrequest` 
	ADD `service_mode` varchar(100) default NULL AFTER `long_term_proposal`;
ALTER TABLE `opticonobservingrequest` 
	ADD `long_term_proposal_specifics` text default NULL AFTER `long_term_proposal`;
	
	
ALTER TABLE `opticontarget`
	ADD `sn` double default NULL,  
   	ADD `magnitude` double default NULL,  
   	ADD `comments` varchar(255) default NULL;

ALTER TABLE `opticoninstrument`
  ADD `second_filter` varchar(255) default NULL,  
  ADD `obs_mode` varchar(255) default NULL;
  
ALTER TABLE `opticonobservingrequest` 
	ADD `large_proposal` tinyint(1)  NOT NULL default '0' AFTER `long_term_proposal_specifics`;

	
ALTER TABLE `opticoninstrument`
  DROP `second_filter`,  
  ADD `read_out` varchar(255) default NULL;
	
ALTER TABLE `opticoninstrument`
  ADD `order_filter` varchar(255) default NULL;	
  
ALTER TABLE `opticonobservingrequest` 
	ADD `allocation_justification` text default NULL;
	
ALTER TABLE `opticoninstrument`
  ADD `filter_details` text default NULL AFTER `filter`; 	
  
DROP TABLE IF EXISTS `opticoninstrumentfilters`;
CREATE TABLE `opticoninstrumentfilters` (
  `id` int(11) NOT NULL auto_increment,
  `instrumentid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `value` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  INDEX  `instrument_opticonfilters_IND` (`instrumentid`),
  INDEX  `instrument_index_opticonfilters_IND` (`indexid`),
  CONSTRAINT `instrument_opticonfilters_FK` FOREIGN KEY (`instrumentid`) REFERENCES `opticoninstrument` (`id`) ON DELETE CASCADE
) TYPE=InnoDB;
  