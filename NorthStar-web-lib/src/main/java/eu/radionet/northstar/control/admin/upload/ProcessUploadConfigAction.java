// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.upload;

import java.util.Iterator;
import java.util.List;
import java.io.File;
import java.io.InputStream;
import java.io.FileInputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.business.configuration.CollaborationConfiguration;
import eu.radionet.northstar.control.Constants;
/*
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.util.PdfUtils;
import eu.radionet.northstar.control.ActionServlet;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.UserTelescope;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.useradministration.data.entities.User;

import javax.servlet.http.HttpSession;
import javax.servlet.ServletContext;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;

*/

public class ProcessUploadConfigAction extends Action {
	protected Log log = LogFactory.getLog(this.getClass());

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (form != null){

			String FileParam = request.getParameter("fileType");
			if (FileParam != null){
				sendConfigFile(request,response);
				return null;
			}
			
			String restoreParam = request.getParameter("fileRestore");
			if (restoreParam != null){
				restoreFile(request);
			}
		}
		return mapping.findForward(Constants.FORWARD);
	}
	
	private void restoreFile(HttpServletRequest request){
		String restoreParam = request.getParameter("fileRestore");
		if (restoreParam != null){
			String fileName= null;
			String destFileName= null;
			
			if (restoreParam.equalsIgnoreCase("config"))
			{
				fileName =(String) request.getSession().getAttribute("previousConfigFile");
				destFileName =(String) request.getSession().getAttribute("ConfigFileUri");
			}
			
			if (restoreParam.equalsIgnoreCase("options"))
			{
				fileName =(String) request.getSession().getAttribute("previousOptionsFile");
				destFileName =(String) request.getSession().getAttribute("OptionFileUri");
			}
			
			if (fileName != null && destFileName != null){
				destFileName = request.getSession().getServletContext().getRealPath(destFileName);
				
				File curFile=new File(destFileName);
				File temFile=new File(fileName);
				
				if(curFile.exists() && temFile.exists()){
					// copy previous over current... place a backup of current at the location of tem with its own time.
					File newFile = new File(temFile.getPath()+"."+curFile.lastModified() );
					curFile.renameTo(newFile);
					temFile.renameTo(curFile);
					
					try{
					  NorthStarUtils.loadNorthStarConfiguration(request.getSession().getServletContext());
					  CollaborationConfiguration.setAcceptInvitation(NorthStarUtils
								.getLinkPath(request));
						log.info("NorthStar configuration loaded for ");
					}catch (Exception e){
						log.info("could not load NorthStar configuration");
						
					}
				}
			}
		}
	}
	
	private void sendConfigFile(HttpServletRequest request, HttpServletResponse response) throws Exception{
		String FileParam = request.getParameter("fileType");
		String fileName= null;
		
		if (FileParam.equalsIgnoreCase("config"))
		{
			fileName =(String) request.getSession().getAttribute("ConfigFileUri");
		}
		
		if (FileParam.equalsIgnoreCase("options"))
		{
			fileName= (String) request.getSession().getAttribute("OptionFileUri");
		}
		// get absolute path
		fileName=request.getSession().getServletContext().getRealPath(fileName);
		
		if (fileName != null)
		{
			File downloadFile = new File(fileName);
			if (downloadFile.exists()){
				Long filesize =  new Long(downloadFile.length());
				
				response.setContentLength(filesize.intValue());
				response.setContentType("text/plain");

				response.setHeader("Content-disposition", "attachment; filename="
						+ FileParam+".xml");
				response.setHeader("Cache-Control", "max-age=600");

				ServletOutputStream outStream = response.getOutputStream();
				
				InputStream instream = new FileInputStream(downloadFile);
				byte[] idata = new byte[instream.available()];
				instream.read(idata);
				// byte[] output = optionsFile.
				// write byte[]
				outStream.write(idata);
				outStream.flush();
				outStream.close();
				
				response.setStatus(200); // 200 OK
				log.info("send file: "+fileName);
				return;
			}
		}	
	}

}
