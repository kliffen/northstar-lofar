-- MySQL dump 10.10
--
-- Host: localhost    Database: northstar
-- ------------------------------------------------------
-- Server version	5.0.15-nt-max

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `additionalissues`
--

DROP TABLE IF EXISTS `additionalissues`;
CREATE TABLE `additionalissues` (
  `id` int(11) NOT NULL auto_increment,
  `linkedproposals` tinyint(1) NOT NULL default '0',
  `linkedproposalsspecifics` text,
  `linkedproposalselsewhere` tinyint(1) NOT NULL default '0',
  `linkedproposalselsewherespecifics` text,
  `previousallocations` tinyint(1) NOT NULL default '0',
  `previousallocationsspecifics` text,
  `additionalremarks` text,
  `relatedpublications` text,
  `grantnumber` text,
  `sponsoring` tinyint(1) NOT NULL default '0',
  `sponsordetails` text,
  `new_observer_experience` text,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `northstar`.`thesis`
--

DROP TABLE IF EXISTS `thesis`;
CREATE TABLE `thesis` (
  `id` int(11) NOT NULL auto_increment,
  `additionalissues_id` int(11) default NULL,
  `indexid` int(11) default NULL,
  `student_ref` int(11) default NULL,
  `student_name` varchar(255) default NULL,
  `student_level` varchar(255) default NULL,
  `supervisor_ref` int(11) default NULL,
  `supervisor_name` varchar(255) default NULL,
  `expectedcompletiondate` datetime default NULL,
  `datarequired` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `member_student_FK` (`student_ref`),
  KEY `member_supervisor_FK` (`supervisor_ref`),
  KEY `additionalissues_thesis_IND` (`additionalissues_id`),
  KEY `additionalissues_index_thesis_IND` (`indexid`),
  CONSTRAINT `additionalissues_thesis_IND` FOREIGN KEY (`additionalissues_id`) REFERENCES `additionalissues` (`id`),
  CONSTRAINT `member_student_FK` FOREIGN KEY (`student_ref`) REFERENCES `member` (`id`),
  CONSTRAINT `member_supervisor_FK` FOREIGN KEY (`supervisor_ref`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `invitation`
--

DROP TABLE IF EXISTS `invitation`;
CREATE TABLE `invitation` (
  `id` int(11) NOT NULL auto_increment,
  `proposalid` int(11) NOT NULL,
  `memberid` int(11) NOT NULL,
  `acceptcode` char(20) NOT NULL,
  `invitationtime` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `userid` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `proposal_invitation_IND` (`proposalid`),
  KEY `member_invitation_IND` (`memberid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `justification`
--

DROP TABLE IF EXISTS `justification`;
CREATE TABLE `justification` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(255) default NULL,
  `abstract` text,
  `scientificfilename` varchar(255) default NULL,
  `scientificfilepath` varchar(255) default NULL,
  `scientificfiledatetime` datetime default NULL,
  `scientificfilesize` int(11) default NULL,
  `scientificfilepages` int(11) default NULL,
  `technicaldetailsfilename` varchar(255) default NULL,
  `technicaldetailsfilepath` varchar(255) default NULL,
  `technicaldetailsfiledatetime` datetime default NULL,
  `technicaldetailsfilesize` int(11) default NULL,
  `technicaldetailsfilepages` int(11) default NULL,  
  `figurefilename` varchar(255) default NULL,
  `figurefilepath` varchar(255) default NULL,
  `figurefiledatetime` datetime default NULL,
  `figurefilesize` int(11) default NULL,
  `figurefilepages` int(11) default NULL,  
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `justificationfile`
--

DROP TABLE IF EXISTS `justificationfile`;
CREATE TABLE `justificationfile` (
  `id` int(11) NOT NULL auto_increment,
  `justificationid` int(11) NOT NULL,
  `uploadfile` longblob,
  `type` varchar(50) default NULL,
  PRIMARY KEY  (`id`),
  KEY `justification_justificationfile_IND` (`justificationid`),
  CONSTRAINT `justification_justificationfile_FK` FOREIGN KEY (`justificationid`) REFERENCES `justification` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` int(11) NOT NULL auto_increment,
  `proposalid` int(11) default NULL,
  `indexid` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `proposal_member_IND` (`proposalid`),
  KEY `proposal_index_member_IND` (`indexid`),
  CONSTRAINT `proposal_member_FK` FOREIGN KEY (`proposalid`) REFERENCES `proposal` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `memberrole`
--

DROP TABLE IF EXISTS `memberrole`;
CREATE TABLE `memberrole` (
  `id` int(11) NOT NULL auto_increment,
  `memberid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `roleid` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `member_memberrole_IND` (`memberid`),
  KEY `role_memberrole_IND` (`roleid`),
  KEY `member_index_memberrole_IND` (`indexid`),
  CONSTRAINT `role_memberrole_FK` FOREIGN KEY (`roleid`) REFERENCES `role` (`id`),
  CONSTRAINT `member_memberrole_FK` FOREIGN KEY (`memberid`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `nonregisteredmember`
--

DROP TABLE IF EXISTS `nonregisteredmember`;
CREATE TABLE `nonregisteredmember` (
  `id` int(11) NOT NULL auto_increment,
  `memberid` int(11) default NULL,
  `name` varchar(255) default NULL,
  `affiliation` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `country` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `nonregisteredmember_UNIQ` (`memberid`),
  CONSTRAINT `member_nonregisteredmember_FK` FOREIGN KEY (`memberid`) REFERENCES `member` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `target`;
CREATE TABLE `target` (
  `id` int(11) NOT NULL auto_increment,
  `observationid` int(11) default NULL,
  `observingrequestid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `fieldname` varchar(255) default NULL,
  `ra` double default NULL,
  `decl` double default NULL,
  `epoch` varchar(255) default NULL,
  `targettype` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observation_target_IND` (`observationid`),
  KEY `observation_index_target_IND` (`indexid`),
  CONSTRAINT `observation_target_FK` FOREIGN KEY (`observationid`) REFERENCES `observation` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `observation`
--

DROP TABLE IF EXISTS `observation`;
CREATE TABLE `observation` (
  `id` int(11) NOT NULL auto_increment,
  `observingrequestid` int(11) default NULL,
  `indexid` int(11) default NULL,
--  `fieldname` varchar(255) default NULL,
--  `ra` double default NULL,
--  `decl` double default NULL,
--  `epoch` varchar(255) default NULL,
  `requiredschedconstraints` text,
  `preferredschedconstraints` text,
  `observationtype` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observingrequest_observation_IND` (`observingrequestid`),
  KEY `observingrequest_index_observation_IND` (`indexid`),
  CONSTRAINT `observingrequest_observation_FK` FOREIGN KEY (`observingrequestid`) REFERENCES `observingrequest` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `observationallocation`;
CREATE TABLE `observationallocation` (
  `id` int(11) NOT NULL auto_increment,
  `resourceid` int(11) default NULL,
  `observationid` int(11) default NULL,
  `allocationkey` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observation_observationallocation_IND` (`observationid`),
  KEY `resource_observationallocation_IND` (`resourceid`),
  KEY `observation_key_observationallocation_IND` (`allocationkey`),
  CONSTRAINT `observation_observationallocation_FK` FOREIGN KEY (`observationid`) REFERENCES `observation` (`id`),
  CONSTRAINT `resource_observationallocation_FK` FOREIGN KEY (`resourceid`) REFERENCES `resource` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



--
-- Table structure for table `targetallocation`
--

DROP TABLE IF EXISTS `targetallocation`;
CREATE TABLE `targetallocation` (
  `id` int(11) NOT NULL auto_increment,
  `resourceid` int(11) default NULL,
  `targetid` int(11) default NULL,
  `allocationkey` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `target_targetallocation_IND` (`targetid`),
  KEY `resource_targetallocation_IND` (`resourceid`),
  KEY `target_key_targetallocation_IND` (`allocationkey`),
  CONSTRAINT `target_targetallocation_FK` FOREIGN KEY (`targetid`) REFERENCES `target` (`id`),
  CONSTRAINT `resource_targetallocation_FK` FOREIGN KEY (`resourceid`) REFERENCES `resource` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `observingrequest`
--

DROP TABLE IF EXISTS `observingrequest`;
CREATE TABLE `observingrequest` (
  `id` int(11) NOT NULL auto_increment,
  `proposalid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `overallrequiredschedconstraints` text,
  `overallpreferredschedconstraints` text,
  `observingrequesttype` varchar(20) default NULL,
  PRIMARY KEY  (`id`),
  KEY `proposal_observingrequest_IND` (`proposalid`),
  KEY `proposal_index_observingrequest_IND` (`indexid`),
  CONSTRAINT `proposal_observingrequest_FK` FOREIGN KEY (`proposalid`) REFERENCES `proposal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `observingrequestallocation`
--

DROP TABLE IF EXISTS `observingrequestallocation`;
CREATE TABLE `observingrequestallocation` (
  `id` int(11) NOT NULL auto_increment,
  `resourceid` int(11) default NULL,
  `observingrequestid` int(11) default NULL,
  `allocationkey` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observingrequest_observingrequestallocation_IND` (`observingrequestid`),
  KEY `resource_observingrequestallocation_IND` (`resourceid`),
  KEY `observingrequest_key_observingrequestallocation_IND` (`allocationkey`),
  CONSTRAINT `observingrequest_observingrequestallocation_FK` FOREIGN KEY (`observingrequestid`) REFERENCES `observingrequest` (`id`),
  CONSTRAINT `resource_observingrequestallocation_FK` FOREIGN KEY (`resourceid`) REFERENCES `resource` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `proposal`
--

DROP TABLE IF EXISTS `proposal`;
CREATE TABLE `proposal` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(255) default NULL,
  `justificationid` int(11) default NULL,
  `additionalissuesid` int(11) default NULL,
  `currentstatusid` int(11) default NULL,
  `parentid` int(11) default NULL,
  `semesterid` int(11) NOT NULL,
  `version` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `justification_proposal_IND` (`justificationid`),
  KEY `additionalissues_proposal_IND` (`additionalissuesid`),
  KEY `proposalstatus_proposal_IND` (`currentstatusid`),
  KEY `semester_proposal_IND` (`semesterid`),
  CONSTRAINT `semester_proposal_FK` FOREIGN KEY (`semesterid`) REFERENCES `semester` (`id`),
  CONSTRAINT `additionalissues_proposal_FK` FOREIGN KEY (`additionalissuesid`) REFERENCES `additionalissues` (`id`),
  CONSTRAINT `justification_proposal_FK` FOREIGN KEY (`justificationid`) REFERENCES `justification` (`id`),
  CONSTRAINT `proposalstatus_proposal_FK` FOREIGN KEY (`currentstatusid`) REFERENCES `proposalstatus` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `proposallock`
--

DROP TABLE IF EXISTS `proposallock`;
CREATE TABLE `proposallock` (
  `id` int(11) NOT NULL auto_increment,
  `proposalid` int(11) NOT NULL,
  `sessionid` varchar(255) NOT NULL,
  `userid` int(11) NOT NULL,
  `since` datetime NOT NULL,
  `lastaction` datetime NOT NULL,
  `valid` tinyint(1) NOT NULL default '0',
  `removerid` int(11) default NULL,
  `removedsince` datetime default NULL,
  PRIMARY KEY  (`id`),
  KEY `proposal_proposallock_IND` (`proposalid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `proposalstatus`
--

DROP TABLE IF EXISTS `proposalstatus`;
CREATE TABLE `proposalstatus` (
  `id` int(11) NOT NULL auto_increment,
  `statusid` int(11) NOT NULL,
  `proposalid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `statustime` datetime NOT NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `proposal_proposalstatus_IND` (`proposalid`),
  KEY `proposal_index_proposalstatus_IND` (`indexid`),
  KEY `status_proposalstatus_IND` (`statusid`),
  CONSTRAINT `status_proposalstatus_FK` FOREIGN KEY (`statusid`) REFERENCES `status` (`id`),
  CONSTRAINT `proposal_proposalstatus_FK` FOREIGN KEY (`proposalid`) REFERENCES `proposal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `registeredmember`
--

DROP TABLE IF EXISTS `registeredmember`;
CREATE TABLE `registeredmember` (
  `id` int(11) NOT NULL auto_increment,
  `memberid` int(11) default NULL,
  `userid` int(11) NOT NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `registeredmember_UNIQ` (`memberid`),
  CONSTRAINT `member_registeredmember_FK` FOREIGN KEY (`memberid`) REFERENCES `member` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `resource`
--

DROP TABLE IF EXISTS `resource`;
CREATE TABLE `resource` (
  `id` int(11) NOT NULL auto_increment,
  `resourcetypeid` int(11) NOT NULL,
  `value` double NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `resourcetype_resource_IND` (`resourcetypeid`),
  CONSTRAINT `resourcetype_resource_FK` FOREIGN KEY (`resourcetypeid`) REFERENCES `resourcetype` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `resourcetype`
--

DROP TABLE IF EXISTS `resourcetype`;
CREATE TABLE `resourcetype` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `unit` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `description` varchar(100) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `role_UNIQ` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `semester`
--

DROP TABLE IF EXISTS `semester`;
CREATE TABLE `semester` (
  `id` int(11) NOT NULL auto_increment,
  `prefix` varchar(15) NOT NULL,
  `availabledatetime` datetime NOT NULL,
  `startdatetime` datetime NOT NULL,
  `enddatetime` datetime NOT NULL,
  `deadlinedatetime` datetime NOT NULL,
  `deadlinedelay` int(11) NOT NULL default '0',
  `lastassigned` int(11) NOT NULL default '0',
  `telescope` varchar(20) NOT NULL,
  `categoryid` int(11) NOT NULL,
  `semester` varchar(255) NOT NULL,
  `community` varchar(255) NOT NULL,
  `closed` tinyint(1) NOT NULL default '0',
  `immediate` tinyint(1) NOT NULL default '0',
   `parentid` int(11),
  PRIMARY KEY  (`id`),
  KEY `category_semester_IND` (`categoryid`),
  CONSTRAINT `category_semester_FK` FOREIGN KEY (`categoryid`) REFERENCES `category` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(11) NOT NULL auto_increment,
  `code` varchar(15) NOT NULL,
  `type` varchar(15) NOT NULL,
  `description` varchar(100) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `status_UNIQ` (`code`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Table structure for table `statusemail`
--

DROP TABLE IF EXISTS `statusemail`;
CREATE TABLE `statusemail` (
  `id` int(11) NOT NULL auto_increment,
  `proposalstatusid` int(11) NOT NULL,
  `proposalid` int(11) NOT NULL,
  `subject` varchar(255) default NULL,
  `sender` varchar(255) default NULL,
  `torecipients` varchar(255) default NULL,
  `ccrecipients` varchar(255) default NULL,
  `bccrecipients` varchar(255) default NULL,
  `message` text,
  `generatedmail` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `proposal_statusemail_IND` (`proposalid`),
  CONSTRAINT `proposal_statusemail_FK` FOREIGN KEY (`proposalid`) REFERENCES `proposal` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `usertelescope`;
CREATE TABLE `usertelescope` (
  `id` int(11) NOT NULL auto_increment,
  `userid` int(11) default NULL,
  `memberid` int(11) default NULL,
  `telescope` varchar(20) default NULL,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

DROP TABLE IF EXISTS `privileges`;
DROP VIEW IF EXISTS `privileges`;
CREATE VIEW `privileges` AS
SELECT p.id as proposalid, r.userid  FROM proposal p, member m, registeredmember r
WHERE m.proposalid=p.id AND r.memberid=m.id;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

