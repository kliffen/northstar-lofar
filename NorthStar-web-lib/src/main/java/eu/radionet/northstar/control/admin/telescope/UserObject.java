// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.telescope;

import java.io.Serializable;

public class UserObject implements Serializable {
	protected String Name = null;
	protected String userId = null;
	protected String statusCode = null;
	protected String subject  = null;
	protected String message = null;

	UserObject(){
		Name ="empty";
		userId="null";
	}
	
	UserObject(String name,String id){
		userId = id;
		Name = name;
	}
	
	public String getUserId() {
		return userId;
	}
	public String getName() {
		return Name;
	}
	
	public String getUserName() {
		return Name;
	}
	
	public void setUserId(String id) {
		userId = id;
	}
	public void setUserName(String name) {
		Name = name;
	}

}
