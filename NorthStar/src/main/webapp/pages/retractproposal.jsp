<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processRetractProposal" method="POST">

<TABLE>
	<TR>
		<TD>
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%"><TR><TD><IMG src="<%=request.getContextPath()%>/images/list/retract-big.gif"/></TD>
			
			<TD valign='top'>
			Are you really sure you want to retract proposal with title '<bean:write name="retractDeleteForm" property="proposalTitle"/>' and with project id '<bean:write name="retractDeleteForm" property="proposalCode"/>'?
			</TD></TR></TABLE>
			<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	</TABLE>
	
<table width="100%">
	<TR>
		<TD class="left"><html:submit property="retractButton" styleClass="list_accept_yes" title="Ok">Ok</html:submit></td>
		<td  class="right"><html:cancel styleClass="list_decline_t" title="Cancel retraction">Cancel</html:cancel></TD>
	</TR>
</TABLE>
</html:form>
