<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />
<html:form action="/processInviteUser" method="POST">

<html:hidden property="memberId"/>
<table width="100%">
	<TR>
		<TD width="*">&nbsp;</TD>
		<TD class="help" width="60">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
				<TABLE width="60" height="25">
					<TR>
			  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#applicant\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
		  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#applicant\" target=\"help\">Help</a>"%></TD>
	  				</TR>
	  			</TABLE>
			<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
			</TD>
	</TR>
	</table>
	<table>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<logic:empty name="inviteUserForm" property="invitationOk">
		<tr>
			<td class="right">selected telescope: &nbsp; </td>
			<td class="left"><bean:write name="inviteUserForm" property="selectedTelescope" /> </td>
		</tr>
		<tr>
		<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">Note: people who are already a northstar user, <br/> or invited once, should be on the user list. And do not need to be invited.</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	
		<tr>
			<td class="right"><astron:label key="label.applicant.name" /></td>
			<td class="left"><html:text property="name"  tabindex="1" maxlength="255"/><span class="error"><astron:errors property="name" ifnotexist="*"/></span></td>
		</tr>
		<tr>
			<td class="right"><astron:label key="label.applicant.affiliation" /></td>
			<td class="left"><html:text property="affiliation" tabindex="2"  maxlength="255"/><span class="error"><astron:errors property="affiliation" ifnotexist="*"/></span></td>
		</tr>
		<tr>
			<td class="right"><astron:label key="label.applicant.email" /></td>
			<td class="left"><html:text property="email" tabindex="3"  maxlength="255"/><span class="error"><astron:errors property="email" ifnotexist="*"/></span></td>
		</tr>
		<tr>
			<td class="right"><astron:label key="label.applicant.country" /></td>
			<td class="left"><bean:define id="mcountry" name="inviteUserForm" property="countries"/> 
				<html:select property="selectedCountry"  onchange="document.forms[0].submit()">
					<html:options collection="mcountry" property="ISO3Country" labelProperty="displayCountry"/>
				</html:select>
				<span class="error"><astron:errors property="country" ifnotexist="*"/></span></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td class="right">Message:&nbsp;</td>
			<td class="left"><html:textarea property="personalMessage" cols="50" rows="4" tabindex="5" />
			</td>
		</tr>
		</logic:empty>
 		<logic:notEmpty name="inviteUserForm" property="invitationOk">
 			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>	
			<tr>
				<td colspan="2"> &nbsp; Invitation has been sent &nbsp; </td>
			</tr>
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>	
 		</logic:notEmpty>	
	<tr>
		<td colspan="2">&nbsp; <span class="error"><astron:errors property="sendemail" ifnotexist=""/></span></td>
	</tr>		
</table>
<table width="100%">
	<tr>
		<logic:empty name="inviteUserForm" property="invitationOk">
			<html:hidden property="selectedTelescope"/>
			<td align="left"><html:submit property="inviteButton"  styleClass="list_accept_yes">Invite</html:submit>
 			</td>
 			<td align="right"><html:cancel styleClass="list_decline_t">Cancel</html:cancel></TD>
 		</logic:empty>
 		<logic:notEmpty name="inviteUserForm" property="invitationOk">
			<td colspan="2"><html:submit property="okButton"  styleClass="list_accept_yes">Ok</html:submit>
 			</td>
 		
		</logic:notEmpty>
		
	</tr>
</table>

</html:form>