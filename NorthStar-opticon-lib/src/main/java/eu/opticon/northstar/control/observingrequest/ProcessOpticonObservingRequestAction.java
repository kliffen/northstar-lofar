// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.LabelValueBean;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import eu.opticon.northstar.business.OpticonConstants;
import eu.opticon.northstar.business.OpticonTargetUtils;
import eu.opticon.northstar.control.observingrequest.instrument.ProcessInstrumentBean;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.observingrequest.ObservingRequestForm;
import eu.radionet.northstar.control.proposal.observingrequest.ProcessObservingRequestAction;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Resource;
import eu.radionet.northstar.data.entities.ResourceType;
import eu.radionet.northstar.data.entities.Target;

/**
 * This class storeds telescope specific observation information in the database
 * from the information obtained out of the form.
 * @author Anton Smit
 */
public class ProcessOpticonObservingRequestAction extends
		ProcessObservingRequestAction {
    
    /**
     * fills the that data obect the  information out of the form. Already implemented in
     * this skeleton are the observation details and the requested time the proposer wants
     * to use.
     * @see eu.radionet.northstar.control.proposal.observingrequest.ProcessObservingRequestAction#fillObservingRequest(javax.servlet.http.HttpServletRequest, eu.radionet.northstar.data.entities.ObservingRequest, eu.radionet.northstar.control.proposal.observingrequest.ObservingRequestForm)
     */
	protected void fillObservingRequest(HttpServletRequest request,
		ObservingRequest observingRequest,
		ObservingRequestForm observingRequestForm) throws DatabaseException {
		
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
		OpticonObservingRequestForm opticonObservingRequestForm = (OpticonObservingRequestForm) observingRequestForm;
        
		// if the instrument form is visible, then store the instrument data
		String index= opticonObservingRequestForm.getSelectedObservation();
		String pipeindex= opticonObservingRequestForm.getSelectedPipeline();
		
		
		ActionMessages errors= getErrors(request);
		
		
		if(!AstronValidator.isBlankOrNull(index)){
			List observations = opticonObservingRequest.getObservations();
			int obsIndex = new Integer(index).intValue();
			// this should not be possible
			if(obsIndex >= observations.size()){
				opticonObservingRequestForm.setSelectedObservation(null);
			}else{
				OpticonObservation opticonObservation = (OpticonObservation) observations.get(obsIndex);
				ProcessInstrumentBean.processForm(request, opticonObservation);
			
				if(opticonObservation.getObservationKind()!=null && opticonObservation.getObservationKind().equals("pipeline")&&( opticonObservation.getInstrument().getProcessingMode()!=null && opticonObservation.getInstrument().getProcessingMode().equalsIgnoreCase(OpticonConstants.PROCESSING_MODE_PULSAR)
						|| opticonObservation.getInstrument().getProcessingMode().equalsIgnoreCase(OpticonConstants.PROCESSING_MODE_USER_SPECIFIED)))
				{
					opticonObservation.getInstrument().setAveragingFrequency(null);
					opticonObservation.getInstrument().setAveragingTime(null);
				}
				
				if(opticonObservingRequestForm.getCommitObservationButton()!=null){
				if(AstronValidator.isBlankOrNull(opticonObservation.getInstrument().getAntenna())){
					errors.add("selectedAntenna", new ActionMessage("errors.required","Selected Antenna"));
					//throw new DatabaseException();
				}
				
				if(AstronValidator.isBlankOrNull(opticonObservation.getInstrument().getStation())){
					errors.add("selectedStation", new ActionMessage("errors.required","Selected Station"));
					//throw new DatabaseException();
				}
				
				if(opticonObservation.getInstrument().getOneFilter()==null && ! opticonObservation.getInstrument().getName().startsWith("TBB")){
					errors.add("selectedOneFilter", new ActionMessage("errors.required","Filter"));
					//throw new DatabaseException();
				}
				
//				String value  = request.getParameter("integrationTime");
//				log.info(value);
//				double integrationTime = 0;
//				if (value != null){
//					try{
//						integrationTime = new Double (value);
//					}catch(NumberFormatException e){
//						value = null;
//					}
//				}
//				
//				if(integrationTime <= 0){
//					errors.add("integrationTime", new ActionMessage("error.observation.input.value.range","Integration time should be greater than 0."));
//				}
				
				/*else{
					if(opticonObservation.getInstrument().getFilters().size()==0){
						errors
						.add("selectedOneFilter", new ActionMessage("errors.required","Filter"));
						//throw new DatabaseException();
					}
				}*/
				
				if(opticonObservation.getInstrument().isCohstokes()){
					if(opticonObservation.getInstrument().getCohTaBeams()== null){
						errors.add(OpticonConstants.COHTABEAMS, new ActionMessage("errors.required","Coherent tied array beams"));
						//throw new DatabaseException();
					}
					else{
						List urlList = new ArrayList();
						String url = new String();
						urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.MAX_COHTABEAMS_VALUE, new HashMap(), contextConfiguration);
						int maxCohTaBeams= 1;
						if (urlList != null && urlList.size() > 1){
							LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
							url =  urlValue.getValue(); 
							maxCohTaBeams= Integer.parseInt(url);
						}
						urlList = new ArrayList();
						url = new String();
						urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.MIN_COHTABEAMS_VALUE, new HashMap(), contextConfiguration);
						int minCohTaBeams=1;
						if (urlList != null && urlList.size() > 1){
							LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
							url =  urlValue.getValue(); 
							minCohTaBeams =Integer.parseInt(url);
						}
						 
						if (opticonObservation.getInstrument().getCohTaBeams().intValue()>maxCohTaBeams 
								|| opticonObservation.getInstrument().getCohTaBeams().intValue()<minCohTaBeams){
							errors.add(OpticonConstants.COHTABEAMS, new ActionMessage("error.observation.input.value.range",new String[]{"Coherent tied array beams",minCohTaBeams+" - "+maxCohTaBeams}));
						}
					}
				}
				
				
				if(opticonObservation.getInstrument().isIncohstokes()){
					if(opticonObservation.getInstrument().getInCohTaBeams()==null){
						errors.add(OpticonConstants.INCOHTABEAMS, new ActionMessage("errors.required","Incoherent tied array beams"));
						//throw new DatabaseException();
					}
					else{
						
						List urlList = new ArrayList();
						String url = new String();
						urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.MAX_INCOHTABEAMS_VALUE, new HashMap(), contextConfiguration);
						int maxInCohTaBeams= 1;
						if (urlList != null && urlList.size() > 1){
							LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
							url =  urlValue.getValue(); 
							maxInCohTaBeams=Integer.parseInt(url);
						}
						urlList = new ArrayList();
						url = new String();								
						urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.MIN_INCOHTABEAMS_VALUE, new HashMap(), contextConfiguration);
						int minInCohTaBeams= 1;
						
						if (urlList != null && urlList.size() > 1){
							LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
							url =  urlValue.getValue(); 
							minInCohTaBeams= Integer.parseInt(url);
						}
						
						
						if (opticonObservation.getInstrument().getInCohTaBeams().intValue()>maxInCohTaBeams 
								|| opticonObservation.getInstrument().getInCohTaBeams().intValue()<minInCohTaBeams){
							errors.add(OpticonConstants.INCOHTABEAMS, new ActionMessage("error.observation.input.value.range",new String[]{"Incoherent tied array beams",minInCohTaBeams+" - "+maxInCohTaBeams}));
						}
					}
				}
				
				if(opticonObservation.getInstrument().isPiggyBack()){
					if(AstronValidator.isBlankOrNull(opticonObservation.getInstrument().getTbbExposureTime()+"")){
						errors.add(OpticonConstants.DISPLAY_PIGGYBACK, new ActionMessage("errors.required","Exposure Time"));
						//throw new DatabaseException();
					}
				
				}
				
				
				if(opticonObservation.getInstrument().getName().startsWith("TBB")){
					
					List urlList = new ArrayList();
					String url = new String();
					urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.MAX_TRIGGER_LENGTH, new HashMap(), contextConfiguration);
					double maxTriggerLength= 1;
					if (urlList != null && urlList.size() > 1){
						LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
						url =  urlValue.getValue(); 
						maxTriggerLength= Double.parseDouble(url);
					}
					urlList = new ArrayList();
					url = new String();
					urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.MIN_TRIGGER_LENGTH, new HashMap(), contextConfiguration);
					double minTriggerLength=0.001;
					if (urlList != null && urlList.size() > 1){
						LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
						url =  urlValue.getValue(); 
						minTriggerLength =Double.parseDouble(url);
					}
					 
					if(opticonObservation.getInstrument().getTbbTriggerRate()==null){
						errors.add(OpticonConstants.TBB_TRIGGER_LENGTH, new ActionMessage("error.required",new String[]{"Trigger Rate"}));
					}
					else if (opticonObservation.getInstrument().getTbbTriggerLength().doubleValue()>maxTriggerLength 
							|| opticonObservation.getInstrument().getTbbTriggerLength().doubleValue()<minTriggerLength){
						errors.add(OpticonConstants.TBB_TRIGGER_LENGTH, new ActionMessage("error.observation.input.value.range",new String[]{"Trigger Length",minTriggerLength+" - "+maxTriggerLength}));
					}
					
					if(!AstronValidator.isBlankOrNull(opticonObservation.getInstrument().getTbbSelectedObservationId())){
						String tbbObservationIndex =opticonObservation.getInstrument().getTbbSelectedObservationId();
						char c= tbbObservationIndex.charAt(0);
						OpticonObservation oo = (OpticonObservation) observations.get(new Integer(c).intValue()-65);
						double totalDuration=0d;
						for(Target target : opticonObservingRequest.getTargets()){
							OpticonTarget optarget = (OpticonTarget) target;
							if(optarget.getObservingrun() != null && optarget.getObservingrun().equalsIgnoreCase(opticonObservation.getInstrument().getTbbSelectedObservationId())){
								totalDuration+=NorthStarDelegate.getResourceValue(
										optarget.getAllocations(), OpticonConstants.TOTAL_DURATION).doubleValue();
							}
						}
						opticonObservation.getInstrument().setTbbExposureTime(Double.valueOf(OpticonTargetUtils.getHoursFromSeconds(totalDuration)));
					}
					

					
				}
				
				if(AstronValidator.isBlankOrNull(opticonObservation.getInstrument().getIntsteps()+"")){
					errors.add(OpticonConstants.INSTEPS, new ActionMessage("errors.required","Time integration factor"));
					//throw new DatabaseException();
				}
				if(errors.size()>0){
				saveErrors(request, errors);
				return;
				}
				}
				observations.set(obsIndex,opticonObservation );
				opticonObservingRequest.setObservations(observations);
			}
		}else if(!AstronValidator.isBlankOrNull(pipeindex)){
			List pipelines = opticonObservingRequest.getPipelines();
			int obsIndex = new Integer(pipeindex).intValue();
			// this should not be possible
			if(obsIndex >= pipelines.size()){
				opticonObservingRequestForm.setSelectedPipeline(null);
			}else{
				OpticonObservation opticonObservation = (OpticonObservation) pipelines.get(obsIndex);
				ProcessInstrumentBean.processForm(request, opticonObservation);
				
				
				
				pipelines.set(obsIndex,opticonObservation );
				opticonObservingRequest.setPipelines(pipelines);
			}
		}else{
		
			// if the instrument form is visible, the request data is hidden, do not update them, the fields will be cleared then.
	        if (!AstronValidator.isBlankOrNull(opticonObservingRequestForm.getNoiseLevel())) {
				try {
					opticonObservingRequest.setNoiseLevel(new Double(opticonObservingRequestForm.getNoiseLevel()));
		        } catch (NumberFormatException nfe) {
		        	// Ignore
		        }
	        } else {
	        	opticonObservingRequest.setNoiseLevel(null);
	        }
	        
	  
	        opticonObservingRequest.setNightTime(opticonObservingRequestForm.isNightTime());
	        opticonObservingRequest.setLongTermProposal(opticonObservingRequestForm.isLongTermProposal());
	        opticonObservingRequest.setLongTermProposalSpecifics(opticonObservingRequestForm.getLongTermProposalSpecifics());
	        opticonObservingRequest.setLargeProposal(opticonObservingRequestForm.isLargeProposal());
	        opticonObservingRequest.setCategory(opticonObservingRequestForm.getCategory());
	        opticonObservingRequest.setObservingMode(opticonObservingRequestForm.getSelectedObservingMode());
	        opticonObservingRequest.setAllocationJustification(opticonObservingRequestForm.getAllocationJustification());
	        opticonObservingRequest.setTravel(opticonObservingRequestForm.getTravel());
	        opticonObservingRequest.setProprietaryException(opticonObservingRequestForm.isEnableProprietaryException());
	        opticonObservingRequest.setProprietaryExceptionSpecifics(opticonObservingRequestForm.getProprietaryExceptionSpecifics());
	        opticonObservingRequest.setOtherExpenditure(opticonObservingRequestForm.getOtherExpenditure());
	        opticonObservingRequest.setOverallRequiredSchedConstraints(opticonObservingRequestForm.getOverallRequiredSchedConstraints());
			/* set the generic (telescope independent fields) like RA, dec, epoch etc 
			 * in the super class. */
			super.fillObservingRequest(request, observingRequest,
					observingRequestForm);
			
			fillAllocations(opticonObservingRequest,opticonObservingRequestForm);
		}
                
        
       

	}
	
	

	protected void fillAllocations(OpticonObservingRequest opticonObservingRequest, 
			OpticonObservingRequestForm opticonObservingRequestForm)
		throws DatabaseException
	{
	       ResourceType resourceType = 
	    	   northStarDelegate.getResourceType(OpticonConstants.TIME, OpticonConstants.NIGHTS );
	       ResourceType resourceTypeHours = 
	    	   northStarDelegate.getResourceType(OpticonConstants.TIME, OpticonConstants.HOURS );
	       ResourceType resourceTypeData = 
	    	   northStarDelegate.getResourceType(OpticonConstants.DATASTORAGE, OpticonConstants.GB );

	       Iterator allocationsIt = opticonObservingRequestForm.getAllocationBeans().iterator();
	       
	       //old allocations need to be removed. It will be filled again with the bean information
	       opticonObservingRequest.getAllocations().clear();
	       while (allocationsIt.hasNext())
	       {
	    	   /* 
	    	    * Allocations are stored with a label <telescope name>_<moon phase> 
	    	    * This will make the label dynamic as well as unique
	    	    */
	    	   AllocationBean allocationBean = (AllocationBean) allocationsIt.next();
	    	   
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.DARK,
		                AstronConverter.toDouble(allocationBean.getDarkTime()), resourceType);	
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.BRIGHT,
		                AstronConverter.toDouble(allocationBean.getBrightTime()), resourceType);	
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.GREY,
		                AstronConverter.toDouble(allocationBean.getGreyTime()), resourceType);	
	    	   /*
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.FIRST_Q,
		                AstronConverter.toDouble(allocationBean.getFirstQTime()), resourceType);	
	    	   NorthStarDelegate.setResourceValue(l
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.LAST_Q,
		                AstronConverter.toDouble(allocationBean.getLastQTime()), resourceType);*/
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.GLOBAL,
		                AstronConverter.toDouble(allocationBean.getGlobalTime()), resourceTypeHours);
	    	   
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.USEFULGREY,
		                AstronConverter.toDouble(allocationBean.getUsefulGreyTime()), resourceType);
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.USEFULDARK,
		                AstronConverter.toDouble(allocationBean.getUsefulDarkTime()), resourceType);
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.USEFULBRIGHT,
		                AstronConverter.toDouble(allocationBean.getUsefulBrightTime()), resourceType);
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.USEFULGLOBAL,
		                AstronConverter.toDouble(allocationBean.getUsefulGlobalTime()), resourceType);
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.LONGTERMSTORAGE,
		                AstronConverter.toDouble(allocationBean.getLongTermStorage()), resourceTypeData);
	    	   
	    	   NorthStarDelegate.setResourceValue(
	    			   opticonObservingRequest.getAllocations(), 
	    			   allocationBean.getTelescope()+ '_' + OpticonConstants.TOTAL_PROCESSING_TIME,
		                AstronConverter.toDouble(allocationBean.getTotalProcessingTime()), resourceTypeData);
	    	   if (opticonObservingRequestForm.isLongTermProposal()){
		    	   
		    	   NorthStarDelegate.setResourceValue(
		    			   opticonObservingRequest.getAllocations(), 
		    			   allocationBean.getTelescope()+ '_' + OpticonConstants.AWARDED,
			                AstronConverter.toDouble(allocationBean.getAwardedTime()), resourceType);
		    	   
		    	   NorthStarDelegate.setResourceValue(
		    			   opticonObservingRequest.getAllocations(), 
		    			   allocationBean.getTelescope()+ '_' + OpticonConstants.LONGTERM,
			                AstronConverter.toDouble(allocationBean.getLongTermTime()), resourceType);
		    	   
		    	   NorthStarDelegate.setResourceValue(
		    			   opticonObservingRequest.getAllocations(), 
		    			   allocationBean.getTelescope()+ '_' + OpticonConstants.AWARDEDDARK,
			                AstronConverter.toDouble(allocationBean.getAwardedDarkTime()), resourceType);
		    	   NorthStarDelegate.setResourceValue(
		    			   opticonObservingRequest.getAllocations(), 
		    			   allocationBean.getTelescope()+ '_' + OpticonConstants.LONGTERMDARK,
			                AstronConverter.toDouble(allocationBean.getLongTermDarkTime()), resourceType);
		    	   NorthStarDelegate.setResourceValue(
		    			   opticonObservingRequest.getAllocations(), 
		    			   allocationBean.getTelescope()+ '_' + OpticonConstants.AWARDEDBRIGHT,
			                AstronConverter.toDouble(allocationBean.getAwardedBrightTime()), resourceType);
		    	   NorthStarDelegate.setResourceValue(
		    			   opticonObservingRequest.getAllocations(), 
		    			   allocationBean.getTelescope()+ '_' + OpticonConstants.LONGTERMBRIGHT,
			                AstronConverter.toDouble(allocationBean.getLongTermBrightTime()), resourceType);
		    	   NorthStarDelegate.setResourceValue(
		    			   opticonObservingRequest.getAllocations(), 
		    			   allocationBean.getTelescope()+ '_' + OpticonConstants.AWARDEDGREY,
			                AstronConverter.toDouble(allocationBean.getAwardedGreyTime()), resourceType);
		    	   NorthStarDelegate.setResourceValue(
		    			   opticonObservingRequest.getAllocations(), 
		    			   allocationBean.getTelescope()+ '_' + OpticonConstants.LONGTERMGREY,
			                AstronConverter.toDouble(allocationBean.getLongTermGreyTime()), resourceType);
		    	  		    
	    	   }
	       }
	}
	
	
	protected List updateTargetCoupling(ObservingRequest observingRequest,
			int selectedObservationId, int selectedPipelineId) {
		List<Target> targets = observingRequest.getTargets();
		for(Target target : targets){
			OpticonTarget optarget = (OpticonTarget) target;
			//optarget.setComments("d");
			if(selectedObservationId >= 0 && optarget.getObservingrun() != null && !optarget.getObservingrun().equals("")){
				//optarget.setComments("o");
				String run = optarget.getObservingrun();
				int targetobservationnr = run.charAt(0) - 65;
				if(run.length() -1 >= 0){
					if(targetobservationnr ==  selectedObservationId){
						//TODO clear allocations
						//optarget.setAllocations(null);
						optarget.setFirstInRun(false);
						optarget.setObservingrun("");
					}else if(targetobservationnr >  selectedObservationId){
						char chr = (char) (targetobservationnr + 64);
						optarget.setObservingrun(String.valueOf(chr));
					}
				}
			}
			
			if(selectedPipelineId >= 0 && optarget.getPipeline() != null && !optarget.getPipeline().equals("")){
				//optarget.setComments("p");
				String line = optarget.getPipeline();
				StringBuilder updatedLine = new StringBuilder("");
				char selectedPipelineChar = (char)(selectedPipelineId + 65);
				for(int charIter = 0 ; charIter < line.length(); charIter++) {
					if(line.charAt(charIter) != ';' && line.charAt(charIter) == selectedPipelineChar) {
					} else if(line.charAt(charIter) != ';' && line.charAt(charIter) > selectedPipelineChar) {
						updatedLine.append((char) (line.charAt(charIter) - 1));
					} else if(line.charAt(charIter) == ';') {
						if(line.charAt(charIter - 1) != ';') {
							updatedLine.append(';');
						}
					} else {
						updatedLine.append((char) (line.charAt(charIter)));
					}
				}
				if (updatedLine.length() > 0) {
					if (updatedLine.charAt(0) == ';') {
						updatedLine.deleteCharAt(0);
					}
					if (updatedLine.charAt(updatedLine.length() - 1) == ';') {
						updatedLine.deleteCharAt(updatedLine.length() - 1);
					}
				}
				optarget.setPipeline(updatedLine.toString());
				
				
					
				//if(line.length() -1 >= 0){
					//char nr = line.charAt(0);
					//int targetpipelinenr = nr - 65;
					/*
					if(targetpipelinenr == selectedPipelineId){
						//optarget.setAllocations(null);
						optarget.setFirstInRun(false);
						optarget.setPipeline("");
					}else if(targetpipelinenr >  selectedPipelineId){
						char chr = (char) (targetpipelinenr + 64);
						optarget.setPipeline(String.valueOf(chr));
					}
				}*/
			}
			//if(optarget.getPipeline() != null && optarget.getPipeline().equalsIgnoreCase(String.valueOf(chr) )){
			//	targetCount++;
			//}
		}
		return targets;
	}


	/**
	 * Converts a string array to a list
	 * @param result the result list	
	 * @param values the array
	 */
	protected void fillList(List result, String[] values) {
		result.clear();
		if (values != null) {
			for (int i = 0; i < values.length; i++) {
				result.add(values[i]);
			}
		}
	}
}



