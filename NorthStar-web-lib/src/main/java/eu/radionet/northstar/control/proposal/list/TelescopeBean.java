// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.list;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TelescopeBean implements Serializable {
	private String telescope = null;
	private String description = null;
	private List proposalBeans = new ArrayList();
    protected int selectedValue = 0;

    public String copyButton = null;
    public String submitButton = null;
    private String editButton =  null;
    private String deleteButton =  null;

    private String retractButton =  null;
    private String viewProposalButton =  null;
    
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List getProposalBeans() {
		return proposalBeans;
	}
	public void setProposalBeans(List proposalBeans) {
		this.proposalBeans = proposalBeans;
	}
	public String getTelescope() {
		return telescope;
	}
	public void setTelescope(String telescope) {
		this.telescope = telescope;
	}
    public void setEditButton(int index, String value) {
        selectedValue = index;
        editButton = value;
    }

    public String getEditButton(int index) {
        return editButton;
    }

    public void setDeleteButton(int index, String value) {
        selectedValue = index;
        deleteButton = value;
    }

    public String getDeleteButton(int index) {
        return deleteButton;
    }

    public int getIndex() {
        return selectedValue;
    }
    /**
     * @return Returns the copyButton.
     */
    public String getCopyButton(int index) {
        return copyButton;
    }

    /**
     * @param copyButton
     *            The copyButton to set.
     */
    public void setCopyButton(int index, String value) {
        selectedValue = index;
        copyButton = value;
    }

    /**
     * @return Returns the submitButton.
     */
    public String getSubmitButton(int index) {
        return submitButton;
    }

    /**
     * @param submitButton
     *            The submitButton to set.
     */
    public void setSubmitButton(int index, String value) {
        selectedValue = index;
        submitButton = value;
    }

    public ProposalBean getProposalBean(int index) {
        resizeProposalBeans(index);
        return (ProposalBean) proposalBeans.get(index);

    }

    private void resizeProposalBeans(int index) {
        int newSize = index + 1;
        if (proposalBeans.size() < newSize) {
            for (int i = proposalBeans.size(); proposalBeans.size() < newSize; i++){
                proposalBeans.add(new ProposalBean());
            }
        }
    }

    /**
     * @return Returns the retractButton.
     */
    public String getRetractButton(int index) {
        return retractButton;
    }

    /**
	 * @param retractButton The retractButton to set.
	 */

    public void setRetractButton(int index, String value) {
        selectedValue = index;
        retractButton = value;
    }

    /**
     * @return Returns the ViewProposalButton.
     */
    public String getViewProposalButton(int index) {
        return viewProposalButton;
    }

    /**
     * @param submitButton
     *            The submitButton to set.
     */
    public void setViewProposalButton(int index, String value) {
        selectedValue = index;
        viewProposalButton = value;
    }

}
