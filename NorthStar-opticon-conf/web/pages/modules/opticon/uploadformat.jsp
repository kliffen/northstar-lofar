Format reminder:<br/>
<pre class="courier">
Fieldname RA Dec Epoch [Exposure_time [Runs [S/N [Red Magn. [Infrared Magn. [Diameter [Comments]]]]]]]]] 
</pre>
<p> An example:
<pre class="courier">
m31 00:48:31.20 +12:09:00.0 j2000 11m    B 6
"hd 39801" 05:55:10.31 +07:24:25.4 j2000 7200s  A,B 7
m32 00:42:41.87 +40:51:57.2 j2000

</pre>
</p>

