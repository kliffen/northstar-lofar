// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ThesisBean.java 
 *
 * Created on Feb 9, 2006
 *
 * Version $Id: ThesisBean.java,v 1.2 2006-08-14 11:45:28 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.additionalissues;

import java.io.Serializable;

import nl.astron.util.AstronConverter;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.Thesis;

/**
 * The ThesisBean provides
 *
 * @author Hanno Holties
 *
 */
public class ThesisBean implements Serializable{

    private Thesis thesis = null;
    private Integer id = null;

    /**
     * @return Returns the id.
     */
    public String getId() {
        return id.toString();
    }
    public void setId(Integer id){
        this.id=id;
    }

    /**
     * @return Returns the thesis.
     */
    public Thesis getThesis() {
        return thesis;
    }
    /**
     * @param thesis The thesis to set.
     */
    public void setThesis(Thesis thesis) {
        this.thesis = thesis;
    }
    
    /**
     * @return Returns the studentName.
     */
    public String getStudentName() {
        return thesis.getStudentName();
    }

    /**
     * @return Returns the studentLevel.
     */
    public String getStudentLevel() {
        return thesis.getStudentLevel();
    }

    /**
     * @return Returns isStudentApplicant.
     */
    public boolean isStudentApplicant() {
        return (thesis.getStudent() != null);
    }

    /**
     * @return Returns the supervisorName.
     */
    public String getSupervisorName() {
        return thesis.getSupervisorName();
    }

    /**
     * @return Returns isSupervisorApplicant.
     */
    public boolean isSupervisorApplicant() {
        return (thesis.getSupervisor() != null);
    }

    /**
     * @return Returns the expectedCompletionDate.
     */
    public String getExpectedCompletionDate() {
        return AstronConverter.toDateString(thesis.getExpectedCompletionDate(),Constants.DATE_FORMAT);
    }

    /**
     * @return Returns the isDataRequired.
     */
    public boolean isDataRequired() {
        return thesis.isDataRequired();
    }

}
