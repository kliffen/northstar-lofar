// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>


package eu.radionet.northstar.data.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

//import java.util.*;

/**
 * The AdditionalIssues provides
 *
 * @author Bastiaan Verhoef
 *
 */
public class AdditionalIssues implements Serializable
{
    protected Integer id = null;
    
    protected boolean linkedProposals = false;
    protected String linkedProposalsSpecifics = null;
    
    protected boolean linkedProposalsElsewhere = false;
    protected String linkedProposalsElsewhereSpecifics = null;
    
    protected boolean previousAllocations = false;
    protected String previousAllocationsSpecifics = null;
    
    protected List theses = new ArrayList();
    protected String additionalRemarks = null;
    protected boolean enableRelatedPublications  = false;
    protected boolean enablePreviousInvolvedProposal = false;
    protected String relatedPublications = null;
    protected String relatedPreviousInvolvedProposal = null;
    protected String grantNumber = null;
    protected boolean sponsoring = false;
    protected String sponsorDetails= null;
    protected String newObserverExperience=null;
	
    public String getNewObserverExperience() {
		return newObserverExperience;
	}
	public void setNewObserverExperience(String newObserverExperience) {
		this.newObserverExperience = newObserverExperience;
	}
	/**
     * @return Returns the additionalRemarks.
     */
    public String getAdditionalRemarks() {
        return additionalRemarks;
    }
    /**
     * @param additionalRemarks The additionalRemarks to set.
     */
    public void setAdditionalRemarks(String additionalRemarks) {
        this.additionalRemarks = additionalRemarks;
    }
    
    public void setRelatedPublications(String relatedPublications){
    	this.relatedPublications = relatedPublications;
    }
    
    public String getRelatedPublications(){
    	return relatedPublications;
    }
    
    public void setRelatedPreviousInvolvedProposal(String relatedPreviousInvolvedProposal) {
    	this.relatedPreviousInvolvedProposal = relatedPreviousInvolvedProposal;
    }
    
    public String getRelatedPreviousInvolvedProposal() {
    	return this.relatedPreviousInvolvedProposal;
    }
    /**
     * @return Returns the id.
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id The id to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }
  
    /**
     * @return Returns the theses.
     */
    public List getTheses() {
        return theses;
    }
    /**
     * @param theses The theses to set.
     */
    public void setTheses(List theses) {
        this.theses = theses;
    }

    public Thesis getThesis(Integer thesisId) {
        Iterator thesisIterator = theses.iterator();
        while (thesisIterator.hasNext()) {
            Thesis thesis = (Thesis) thesisIterator.next();
            if (thesis.getId() != null
                    && thesis.getId().intValue() == thesisId.intValue()) {
                return thesis;
            }
        }
        return null;
    }

    public void addChangeThesis(Thesis thesis) {

        /*
         * if id is null, we need a fake id
         */
        if (thesis.getId() == null && thesis.getFakeId() == null) {
            int fakeId = 1;
            /*
             * iterator through theses
             */
            for (int i = 0; i < theses.size(); i++) {
                Thesis thesisTemp = (Thesis) theses.get(i);
                /*
                 * if saved in database, thesis has id
                 */
                if (thesisTemp.getId() != null) {
                    /*
                     * if thesisId >= fakeId then fakeId = thesisId + 1
                     */
                    if (thesisTemp.getId().intValue() >= fakeId) {
                        fakeId = thesisTemp.getId().intValue() + 1;
                    }
                }
                /*
                 * if thesisFakeId >= fakeId then fakeId = thesisId + 1
                 */
                else if (thesisTemp.getFakeId().intValue() >= fakeId) {
                    fakeId = thesisTemp.getFakeId().intValue() + 1;
                }

            }
            thesis.setFakeId(new Integer(fakeId));
            this.theses.add(thesis);
        } else {
            boolean found = false;
            for (int i = 0; i < theses.size() && !found; i++) {
                Thesis thesisTemp = (Thesis) theses.get(i);
                if (thesis.getId() != null && thesisTemp.getId() != null) {
                    if (thesis.getId().intValue() == thesisTemp.getId()
                            .intValue()) {
                        theses.set(i, thesis);
                        found = true;
                    }
                } else if (thesis.getFakeId() != null
                        && thesisTemp.getFakeId() != null) {
                    if (thesis.getFakeId().intValue() == thesisTemp.getFakeId()
                            .intValue()) {
                        theses.set(i, thesis);
                        found = true;
                    }
                }
            }
        }
    }

    public void removeThesis(Thesis thesisToBeRemoved) {
        boolean found = false;
        for (int i = theses.size() - 1; i >= 0 && !found; i--) {
            Thesis thesis = (Thesis) theses.get(i);
            /*
             * if thesis is saved in the database
             */
            if (thesis.getId() != null && thesisToBeRemoved.getId() != null) {
                /*
                 * check if ids are equal
                 */
                if (thesis.getId().intValue() == thesisToBeRemoved.getId()
                        .intValue()) {
                    this.theses.remove(thesis);
                    found = true;
                }
            }
            /*
             * if thesis isn't saved check if fake id is the same
             */
            else if (thesis.getFakeId() != null
                    && thesisToBeRemoved.getFakeId() != null) {
                if (thesis.getFakeId().intValue() == thesisToBeRemoved
                        .getFakeId().intValue()) {
                    this.theses.remove(thesis);
                    found = true;
                }
            }
        }
    }

    public void removeMemberFromTheses(Member memberToBeRemoved) {
       /* 
        * iterator through theses
        */
       for (int i = 0; i < theses.size(); i++) {
           Thesis thesisTemp = (Thesis) theses.get(i);
           /*
            * check if student is member to be removed
            */
           if (thesisTemp.getStudent()!=null &&
                   thesisTemp.getStudent().equals(memberToBeRemoved)) {
               /*
                * remove reference and update with member's name
                */
               thesisTemp.setStudent(null);
               thesisTemp.setStudentName(memberToBeRemoved.getName());
           }
           /*
            * check if supervisor is member to be removed
            */
           if (thesisTemp.getSupervisor()!=null &&
                   thesisTemp.getSupervisor().equals(memberToBeRemoved)) {
               /*
                * remove reference and update with member's name
                */
               thesisTemp.setSupervisor(null);
               thesisTemp.setSupervisorName(memberToBeRemoved.getName());
           }

       }
    }
 
	/**
	 * @return Returns the linkedProposals.
	 */
	public boolean isLinkedProposals() {
		return linkedProposals;
	}
	/**
	 * @param linkedProposals The linkedProposals to set.
	 */
	public void setLinkedProposals(boolean linkedProposals) {
		this.linkedProposals = linkedProposals;
	}
	/**
	 * @return Returns the linkedProposalsSpecifics.
	 */
	public String getLinkedProposalsSpecifics() {
		return linkedProposalsSpecifics;
	}
	/**
	 * @param linkedProposalsSpecifics The linkedProposalsSpecifics to set.
	 */
	public void setLinkedProposalsSpecifics(String linkedProposalsSpecifics) {
		this.linkedProposalsSpecifics = linkedProposalsSpecifics;
	}
	public boolean isLinkedProposalsElsewhere() {
		return linkedProposalsElsewhere;
	}
	public void setLinkedProposalsElsewhere(boolean linkedProposalsElsewhere) {
		this.linkedProposalsElsewhere = linkedProposalsElsewhere;
	}
	public String getLinkedProposalsElsewhereSpecifics() {
		return linkedProposalsElsewhereSpecifics;
	}
	public void setLinkedProposalsElsewhereSpecifics(
			String linkedProposalsElsewhereSpecifics) {
		this.linkedProposalsElsewhereSpecifics = linkedProposalsElsewhereSpecifics;
	}
	public boolean isPreviousAllocations() {
		return previousAllocations;
	}
	public void setPreviousAllocations(boolean previousAllocations) {
		this.previousAllocations = previousAllocations;
	}
	public String getPreviousAllocationsSpecifics() {
		return previousAllocationsSpecifics;
	}
	public void setPreviousAllocationsSpecifics(String previousAllocationsSpecifics) {
		this.previousAllocationsSpecifics = previousAllocationsSpecifics;
	}
	public String getGrantNumber() {
		return grantNumber;
	}
	public void setGrantNumber(String grantNumber) {
		this.grantNumber = grantNumber;
	}
	public boolean isSponsoring() {
		return sponsoring;
	}
	public void setSponsoring(boolean sponsoring) {
		this.sponsoring = sponsoring;
	}
	public String getSponsorDetails() {
		return sponsorDetails;
	}
	public void setSponsorDetails(String sponsorDetails) {
		this.sponsorDetails = sponsorDetails;
	}
	public boolean isEnableRelatedPublications() {
		return enableRelatedPublications;
	}
	public void setEnableRelatedPublications(boolean enableRelatedPublications) {
		this.enableRelatedPublications = enableRelatedPublications;
	}
	public boolean isEnablePreviousInvolvedProposal(){
		return enablePreviousInvolvedProposal;
	}
	public void setEnablePreviousInvolvedProposal(boolean enablePreviousInvolvedProposal){
		this.enablePreviousInvolvedProposal = enablePreviousInvolvedProposal;
	}
  }