// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.manage.upload;

import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.astron.database.exception.ConnectionException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.business.util.OptionsConfigurator;
import eu.radionet.northstar.business.util.XMLConverter;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.admin.telescope.AddTelescopeAccessForm;
import eu.radionet.northstar.data.entities.UserTelescope;

public class SetUpManageUploadAction extends Action {

	private Log log = LogFactory.getLog(this.getClass());
	private NorthStarDelegate northStarDelegate = null;
	private CollaborationDelegate collaborationDelegate = null;

	private static final String NORTHSTAR = "northstar";
	private static final String NORTHSTAR_CONFIG = "northstar-config";
	private static final String TELESCOPE_CONFIG_FILE = "telescope-configuration-file";
	private static final String NORTHSTAR_TELESCOPE_CONFIGS = "northstar-telescope-configurations";
	private static final String BACKUP_DIR = "backup";
	protected static final String OPTION_VALIDATION_FILE = "option-validation-file";

	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		ManageUploadForm manageUploadForm = (ManageUploadForm) form;
        try {
            northStarDelegate = NorthStarDelegate.getInstance();
        } catch (ConnectionException ce) {
            log.fatal("Cannot instantiate AdminDelegate: " + ce.getMessage());
            return null;
        }
        UserAccount ownUserAccount = (UserAccount) request.getSession().getAttribute(Constants.OWN_USERACCOUNT);
        
        // get list of telescopes where this user has access to.
        if(ownUserAccount != null && ownUserAccount.getUser() != null){
	        List telescopes = northStarDelegate.getTelescopeAccessRights(ownUserAccount.getUser().getId());
	        if(telescopes != null && telescopes.size() > 1){
	        	manageUploadForm.setHasMultipleAccess(true);
	        	manageUploadForm.setUserTelescopes(telescopes);
	        }
			if(telescopes == null || telescopes.size() == 0){
				// No Telescope Access
				manageUploadForm.setCurrentConfigFileUrl("");
				return mapping.findForward(Constants.SUCCESS);
			}
					
			if(manageUploadForm.getSelectedTelescope() == null){
				UserTelescope userTelescope = (UserTelescope) telescopes.get(0);
				manageUploadForm.setSelectedTelescope(userTelescope.getTelescope() );
				setConfigFile(request.getSession().getServletContext(),manageUploadForm);
				
			}
			
			// get current telescope config and restore option
			if(manageUploadForm.getSelectedTelescope() != null){
				setConfigFile(request.getSession().getServletContext(),manageUploadForm);
				
			}
        }
		
		
		return mapping.findForward(Constants.SUCCESS);
	}
	
	
	private String setConfigFile(ServletContext currentContext, ManageUploadForm manageUploadForm) throws Exception{
		String northstarConfigUri = currentContext.getInitParameter(NORTHSTAR_CONFIG);
		
		String telescopeConfigFileUri = findTelescopeConfigUri(northstarConfigUri);
		String fs = System.getProperty("file.separator");
				
		if(telescopeConfigFileUri != null){
			telescopeConfigFileUri = currentContext.getRealPath(telescopeConfigFileUri);
			String telescopeOptionsFileUri= findOptionsUri(telescopeConfigFileUri);
			
			String telescopeOptionsFile = NorthStarUtils.read(telescopeOptionsFileUri,currentContext);	
			XMLConverter xmlConverter = XMLConverter.getInstance();
			Document optionConfigDocument = 
				xmlConverter.convertStringToDocument(telescopeOptionsFile);
			telescopeOptionsFile=null;
			
			Map telescopeFiles = getTelescopeFiles(optionConfigDocument);
			
			String telescopeFile = (String) telescopeFiles.get(manageUploadForm.getSelectedTelescope());
			if(telescopeFile == null || telescopeFile.length() == 0 ){
				manageUploadForm.setCurrentConfigFileUrl("");
				return null;
			}
			manageUploadForm.setCurrentConfigFileUrl(telescopeFile);
			
			//telescopeConfigFileUri = currentContext.getRealPath(telescopeConfigFileUri);
			telescopeFile =currentContext.getRealPath(telescopeFile);
			String telescopeConfigFileDate = findFileDate(telescopeFile);
			manageUploadForm.setCurrentConfigFileDate(telescopeConfigFileDate);
			
			// ugly solution to see if upload has succeeded.
			SimpleDateFormat lFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
			String currentDate = lFormat.format(new Date());
			if(currentDate.equalsIgnoreCase(manageUploadForm.getCurrentConfigFileDate() )){
				manageUploadForm.setConfigOk(true);
			}
			
			
			File contextDir = new File (currentContext.getInitParameter(NORTHSTAR_CONFIG));
			String backupdir = contextDir.getParent()+fs+BACKUP_DIR;
			
			int filestart = telescopeFile.lastIndexOf(fs)+1;
			int filend = telescopeFile.length();
			String fileName = telescopeFile.substring(filestart, filend);
 			File previousConfigFile = findPreviousFile(backupdir,fileName,telescopeFile );
			
			if (previousConfigFile != null){
				Date lDate = new Date(previousConfigFile.lastModified());
				
				manageUploadForm.setPreviousConfigFileDate(lFormat.format(lDate));
				//manageUploadForm.setPreviousConfigFileLocation(previousConfigFile.getPath());
				
			}

			return telescopeConfigFileUri;
		}else{
			return null;
		}
		
		
	}

	private String findFileDate(String telescopeConfigFileUri) {
		if(telescopeConfigFileUri == null){
			return "";
		}
		File lFile = new File(telescopeConfigFileUri);
		if(lFile ==null){
			return "";
		}
		Date lDate = new Date(lFile.lastModified());
		SimpleDateFormat lFormat = new SimpleDateFormat("yyyy-MMM-dd HH:mm");
		
		return lFormat.format(lDate);
	}


	/**
	 *  find location of telescope config file
	 */
	private String findTelescopeConfigUri(String northstarConfigUri) throws Exception{
		// read configfile into string
		String northstarConfigFile = NorthStarUtils.read(northstarConfigUri);
		XMLConverter xmlConverter = XMLConverter.getInstance();
		Document northstarConfigDocument = xmlConverter.convertStringToDocument(northstarConfigFile);
		NodeList elements = northstarConfigDocument.getElementsByTagName(NORTHSTAR);

		String telescopeName = null;
		String configFileUri = null;
		
		if (!northstarConfigDocument.getDocumentElement().getLocalName()
				.equalsIgnoreCase(NORTHSTAR)){
			log.warn("cannot find northstar element in "+northstarConfigUri);
			return null;
		}
		
		// scan trough xml file for location of telescope dependant config file
		for (int i = 0; i < elements.getLength(); i++) {
			Node node = (Node) elements.item(i);
			for (int j = 0; j < node.getChildNodes().getLength(); j++) {
				Node nodeChild = node.getChildNodes().item(j);

				// telescopes section found, iterate trough children for correct telescope
				if (nodeChild.getNodeName().equals("telescopes")) {
					
					for (int k = 0; k < nodeChild.getChildNodes().getLength();k++){
						Node telescopeNode = (Node) nodeChild.getChildNodes().item(k);
						for (int l = 0; l < telescopeNode.getChildNodes().getLength();l++){
							Node telescopeChild = (Node) telescopeNode.getChildNodes().item(l);
							String ccnode = telescopeChild.getNodeName();
							
							if (ccnode.equalsIgnoreCase("name")){
								telescopeName = telescopeChild.getFirstChild().getNodeValue();
							}
							if(ccnode.equalsIgnoreCase(TELESCOPE_CONFIG_FILE) ){
								configFileUri = telescopeChild.getFirstChild().getNodeValue();
							}
						}
						//
						if (telescopeName != null){
							//&& telescopeName.equalsIgnoreCase(selectedTelescope)
							//uploadConfigForm.setCurrentConfigFileUrl(configFileUri);
							
							
							
							if(configFileUri != null){
								return configFileUri;
							}else{
								return null;
							}
						}
					}
				}
			}
		}
		log.warn("cannot find telescope-configuration-file in "+northstarConfigUri);
		return null;
	}

	/**
	 * find location of options file
	 */
	private String findOptionsUri(String telescopeConfigFileUri)throws Exception{
		
		String telescopeFile = NorthStarUtils.read(telescopeConfigFileUri);
		
		String optionUri = null;
		XMLConverter xmlConverter = XMLConverter.getInstance();

			Document optionsDocument = xmlConverter.convertStringToDocument(telescopeFile);
			
			//String localName = optionsDocument.getDocumentElement().getLocalName();
			if (!optionsDocument.getDocumentElement().getLocalName()
					.equalsIgnoreCase(NORTHSTAR_TELESCOPE_CONFIGS)){
				log.warn("cannot find "+NORTHSTAR_TELESCOPE_CONFIGS+" in "+telescopeConfigFileUri);
				return null;
			}
			NodeList elements = optionsDocument.getElementsByTagName(NORTHSTAR_TELESCOPE_CONFIGS);
			
			for (int i = 0; i < elements.getLength(); i++) {
				Node node = (Node) elements.item(i);
				
				for (int j = 0; j < node.getChildNodes().getLength(); j++) {
					Node nodeChild = node.getChildNodes().item(j);
	
					// telescopes section found, iterate trough children for correct telescope
					if (nodeChild.getNodeName().equals("northstar-telescope-configuration")) {
						for (int k = 0; k < nodeChild.getChildNodes().getLength();k++){
							Node telescopeNode = (Node) nodeChild.getChildNodes().item(k);
							
							String ccnode = telescopeNode.getNodeName();
							if (ccnode.equalsIgnoreCase(OPTION_VALIDATION_FILE)){
								optionUri = telescopeNode.getFirstChild().getNodeValue();
							}
							
						}
					}
				}
			}
			if (optionUri==null){
				log.warn("cannot find option-validation-file");
			}
		return optionUri;
	}

	/**
	 * find location of instrument file
	 */
	private Map getTelescopeFiles(Document optionConfigDocument) {
		Node rootNode = optionConfigDocument.getDocumentElement();
		
		Map telescopeFiles = new HashMap();
		if (rootNode.getNodeName().endsWith("contexts")) {
			for (int i = 0; i < rootNode.getChildNodes().getLength(); i++) {
				Node nodeChild = rootNode.getChildNodes().item(i);
				if (nodeChild.getNodeName().equals("context")) {
					NamedNodeMap attributes = nodeChild.getAttributes();
					String category = attributes.getNamedItem("category").getNodeValue();
					
					for (int j = 0; j < nodeChild.getChildNodes().getLength(); j++) {
						Node nodeChildChild = nodeChild.getChildNodes().item(j);
						/*
						 * if child is an element
						 */
						if (AstronValidator.implementsInterface(Element.class, nodeChildChild
								.getClass())) {
							 if (nodeChildChild.getNodeName().equals("telescopes")) {
								 for (int k = 0; k < nodeChildChild.getChildNodes().getLength(); k++) {
									Node telescopeNode = nodeChildChild.getChildNodes().item(k);
									String file = null;
									String name = null;
									for (int m = 0; m < telescopeNode.getChildNodes().getLength(); m++) {
								 		Node subNode = telescopeNode.getChildNodes().item(m);
								 		
								 		if(subNode.getNodeName().equalsIgnoreCase("configurationfile")){
								 			file = subNode.getFirstChild().getNodeValue();		
								 		}
								 		if(subNode.getNodeName().equalsIgnoreCase("name")){
								 			name = subNode.getFirstChild().getNodeValue();		
								 		}
								 	}
									telescopeFiles.put(name, file);
									
								 }
							}
						}
					}
				}

			}
		}
		if(telescopeFiles.size()==0){
			log.warn("could not find telescope files");
		}
		return telescopeFiles;
	}
	
	private File findPreviousFile(String path, final String filterName, String currentFile){
		// create a filter that only the files of the selected telescope are used.
		FilenameFilter filter=new FilenameFilter(){
			public boolean accept(File dir, String fileName) {
			return fileName.startsWith(filterName);
			}
		};
		File f = new File(path);
		     
		File[] files = f.listFiles(filter);
		if (files == null || files.length == 0 ){
			return null;
		}
		
		// sort on date 
	 	Arrays.sort( files, new Comparator(){
	    	public int compare(Object o1, Object o2) {
	    	
	   		if (((File)o1).lastModified() > ((File)o2).lastModified()) {
	   			return -1;
	   		} else if (((File)o1).lastModified() < ((File)o2).lastModified()) {
	   			return +1;
	   		} 
	   		return 0;
	   		
	   	}
	    });
	 
	 // check if the latest file is of the same time and date as the current file	
	 // if true, return the semi last file
	 if (findFileDate(files[0].getPath()).equalsIgnoreCase(findFileDate(currentFile ))){
		 if(files.length>1 ){
			 return files[1];
		 }
	 }
	 // return the latest file.
	 return files[0];
	 
	}

}
