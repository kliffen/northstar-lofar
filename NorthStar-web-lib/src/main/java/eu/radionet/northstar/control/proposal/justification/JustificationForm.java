// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.justification;

import java.util.ArrayList;
import java.util.List;
import eu.radionet.northstar.control.proposal.ProposalForm;

public class JustificationForm extends ProposalForm {
	protected String title = null;
	protected String abstractText = null;
	protected String uploadScientificFileButton = null;
	protected String uploadFigureFileButton = null;	
	protected String uploadTechnicalDetailsFileButton = null;
	protected String deleteFigureFileButton = null;
	protected String deleteTechnicalDetailsFileButton = null;
	protected String scientificFileName = null;
	protected String scientificFileSize = null;	
	protected String scientificFileDate = null;
	protected String figureFileName = null;
	protected String figureFileSize = null;	
	protected String figureFileDate = null;	
	protected String technicalDetailsFileName = null;
	protected String technicalDetailsFileSize = null;	
	protected String technicalDetailsFileDate = null;
	protected String titleCount= null;
	protected String abstractCount = null;
	protected String techCount = null;
	protected String maxTechCount = null;
	protected String maxTitleCount = null;
	protected String maxAbstractCount = null;
	protected boolean allowFirstJustification = true;
	protected boolean allowSecondJustification = true;
	protected boolean allowFiguresFile = true;
	protected boolean allowTechnicalJustification =false;
	protected String LatexClassUrl = null;
	protected String LatexTemplateUrl = null;
	protected boolean ShowLatex = false;
	protected boolean showSingleFileUpload = false;
	protected boolean allowEnvelopeSheet = false;
	protected String selectedEnvelopeSheet = null;
	protected List envelopeSheets = new ArrayList();
	//protected String uploadEnvelopeSheetButton = null;
	
	private boolean displayObservationStrategy;
	protected String observationStrategyText = null;
	protected String observationStrategyTextCount= null;
	
	private boolean displayFillerTime;
	private boolean fillerTime;
	private String fillerTimeReason;
	protected String fillerTimeReasonCount = null;
	
	private boolean displayCoObservationTeam;
	protected boolean coObservationTeam;	
	
	// fields for technical justification
	private boolean displayNightTime;
	private boolean nighttime;
	private String nightTimeReason;
	protected String nightTimeReasonCount = null;
	
	private boolean displayParallelObservatione;
	private boolean parallelObservation;
	private String parallelObservationReason;
	protected String parallelObservationReasonCount = null;
	
	private boolean displayInternationalStation;
	private boolean internationalStation;
	private boolean internationalStationEssential;
	private String internationalStationEssentialReason;
	protected String internationalStationEssentialReasonCount = null;
	
	private boolean displayOtherSchedulingConstraints;
	private boolean otherSchedulingConstraints;
	private String otherSchedulingConstraintsReason;
	protected String otherSchedulingConstraintsReasonCount = null;
	
	private boolean displayCombinedDataProductRequest;
	private boolean combinedDataProductRequest;
	private String combinedDataProductRequestReason;
	protected String combinedDataProductRequestReasonCount = null;
	
	private boolean displaySensitivity;
	private double sensitivity;

	private boolean displayMaxDataRate;
	private double maxDataRate;
	
// testing autoboxing
	private boolean displayROProcessing;
	private boolean displayROProcessingWithoutSelfCal;
	private boolean roProcessing;
	private boolean defaultROProcessing;
	private String roProcessingReason;
	protected String roProcessingReasonCount = null;

	private boolean displayLtaStorage;
	private boolean ltaRawStorage;
	private String ltaRawStorageReason;
	protected String ltaRawStorageReasonCount = null;
	
	private boolean displayLtaStorageAdvanced;
	private String ltaStorageLocation = "";	
	private boolean ltaStorage;
	private String ltaStorageReason;
	protected String ltaStorageReasonCount = null;
	
	private boolean displayOfflineROProcessing;
	private boolean offlineROProcessing;
	private String offlineROProcessingReason;
	protected String offlineROProcessingReasonCount = null;
	
	private boolean displayExternalProcessing;
	private boolean externalProcessing;
	private String externalProcessingReason;
	protected String externalProcessingReasonCount = null;
	
	private boolean displayCepRequesting;
	private boolean cepRequesting = false;
	private String cepRequestingReason;
	protected String cepRequestingReasonCount = null;
	//---End fields for technical justification
	
	public String getLatexClassUrl() {
		return LatexClassUrl;
	}
	public void setLatexClassUrl(String latexClassUrl) {
		LatexClassUrl = latexClassUrl;
	}
	public String getLatexTemplateUrl() {
		return LatexTemplateUrl;
	}
	public void setLatexTemplateUrl(String latexTemplateUrl) {
		LatexTemplateUrl = latexTemplateUrl;
	}
	public boolean isShowLatex() {
		return ShowLatex;
	}
	public void setShowLatex(boolean showLatex) {
		ShowLatex = showLatex;
	}
	public String getAbstractText() {
		return abstractText;
	}
	public void setAbstractText(String abstractText) {
		this.abstractText = abstractText;
	}

	public String getObservationStrategyText() {
		return observationStrategyText;
	}
	public void setObservationStrategyText(String observationStrategyText) {
		this.observationStrategyText = observationStrategyText; 
	}
	public String getObservationStrategyTextCount() {
		return observationStrategyTextCount;
	}
	public void setObservationStrategyTextCount(String observationStrategyCount) {
		this.observationStrategyTextCount = observationStrategyTextCount; 
	}

	public String getFigureFileDate() {
		return figureFileDate;
	}
	public void setFigureFileDate(String figureFileDate) {
		this.figureFileDate = figureFileDate;
	}
	public String getFigureFileName() {
		return figureFileName;
	}
	public void setFigureFileName(String figureFileName) {
		this.figureFileName = figureFileName;
	}
	public String getFigureFileSize() {
		return figureFileSize;
	}
	public void setFigureFileSize(String figureFileSize) {
		this.figureFileSize = figureFileSize;
	}

	public String getScientificFileDate() {
		return scientificFileDate;
	}
	public void setScientificFileDate(String scientificFileDate) {
		this.scientificFileDate = scientificFileDate;
	}
	public String getScientificFileName() {
		return scientificFileName;
	}
	public void setScientificFileName(String scientificFileName) {
		this.scientificFileName = scientificFileName;
	}
	public String getScientificFileSize() {
		return scientificFileSize;
	}
	public void setScientificFileSize(String scientificFileSize) {
		this.scientificFileSize = scientificFileSize;
	}

	public String getTechnicalDetailsFileDate() {
		return technicalDetailsFileDate;
	}
	public void setTechnicalDetailsFileDate(String technicalDetailsFileDate) {
		this.technicalDetailsFileDate = technicalDetailsFileDate;
	}
	public String getTechnicalDetailsFileName() {
		return technicalDetailsFileName;
	}
	public void setTechnicalDetailsFileName(String technicalDetailsFileName) {
		this.technicalDetailsFileName = technicalDetailsFileName;
	}
	public String getTechnicalDetailsFileSize() {
		return technicalDetailsFileSize;
	}
	public void setTechnicalDetailsFileSize(String technicalDetailsFileSize) {
		this.technicalDetailsFileSize = technicalDetailsFileSize;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUploadFigureFileButton() {
		return uploadFigureFileButton;
	}
	public void setUploadFigureFileButton(String uploadFigureFileButton) {
		this.uploadFigureFileButton = uploadFigureFileButton;
	}
	public String getUploadScientificFileButton() {
		return uploadScientificFileButton;
	}
	public void setUploadScientificFileButton(String uploadScientificFileButton) {
		this.uploadScientificFileButton = uploadScientificFileButton;
	}
	public String getUploadTechnicalDetailsFileButton() {
		return uploadTechnicalDetailsFileButton;
	}
	public void setUploadTechnicalDetailsFileButton(
			String uploadTechnicalDetailsFileButton) {
		this.uploadTechnicalDetailsFileButton = uploadTechnicalDetailsFileButton;
	}
	public String getDeleteFigureFileButton() {
		return deleteFigureFileButton;
	}
	public void setDeleteFigureFileButton(String deleteFigureFileButton) {
		this.deleteFigureFileButton = deleteFigureFileButton;
	}
	public String getDeleteTechnicalDetailsFileButton() {
		return deleteTechnicalDetailsFileButton;
	}
	public void setDeleteTechnicalDetailsFileButton(
			String deleteTechnicalDetailsFileButton) {
		this.deleteTechnicalDetailsFileButton = deleteTechnicalDetailsFileButton;
	}
	public String getAbstractCount() {
		return abstractCount;
	}
	public void setAbstractCount(String abstractCount) {
		this.abstractCount = abstractCount;
	}
	public String getTitleCount() {
		return titleCount;
	}
	public void setTitleCount(String titleCount) {
		this.titleCount = titleCount;
	}
	public String getMaxAbstractCount() {
		return maxAbstractCount;
	}
	public void setMaxAbstractCount(String maxAbstractCount) {
		this.maxAbstractCount = maxAbstractCount;
	}
	public String getMaxTitleCount() {
		return maxTitleCount;
	}
	public void setMaxTitleCount(String maxTitleCount) {
		this.maxTitleCount = maxTitleCount;
	}
	public boolean isAllowSecondJustification()
	{
		return allowSecondJustification;
	}
	public void setAllowSecondJustification(boolean allowSecondJustification)
	{
		this.allowSecondJustification = allowSecondJustification;
	}
	public boolean isAllowFirstJustification() {
		return allowFirstJustification;
	}
	public void setAllowFirstJustification(boolean allowFirstJustification) {
		this.allowFirstJustification = allowFirstJustification;
	}
	public boolean isAllowFiguresFile() {
		return allowFiguresFile;
	}
	public void setAllowFiguresFile(boolean allowFiguresFile) {
		this.allowFiguresFile = allowFiguresFile;
	}
	public boolean isShowSingleFileUpload() {
		return showSingleFileUpload;
	}
	public void setShowSingleFileUpload(boolean showSingleFileUpload) {
		this.showSingleFileUpload = showSingleFileUpload;
	}
	public boolean isAllowEnvelopeSheet()
	{
		return allowEnvelopeSheet;
	}
	public void setAllowEnvelopeSheet(boolean allowEnvelopeSheet)
	{
		this.allowEnvelopeSheet = allowEnvelopeSheet;
	}
	public String getSelectedEnvelopeSheet() {
		return selectedEnvelopeSheet;
	}
	public void setSelectedEnvelopeSheet(String selectedEnvelopeSheet) {
		this.selectedEnvelopeSheet = selectedEnvelopeSheet;
	}
	public List getEnvelopeSheets() {
		return envelopeSheets;
	}
	public void setEnvelopeSheets(List envelopeSheets) {
		this.envelopeSheets = envelopeSheets;
	}
	public boolean isAllowTechnicalJustification() {
		return allowTechnicalJustification;
	}
	public void setAllowTechnicalJustification(boolean allowTechnicalJustification) {
		this.allowTechnicalJustification = allowTechnicalJustification;
	}
	public String getTechCount() {
		return techCount;
	}
	public String getMaxTechCount() {
		return maxTechCount;
	}
	public boolean isNighttime() {
		return nighttime;
	}
	public String getNightTimeReason() {
		return nightTimeReason;
	}
	public String getNightTimeReasonCount() {
		return nightTimeReasonCount;
	}
	public boolean isParallelObservation() {
		return parallelObservation;
	}
	public String getParallelObservationReason() {
		return parallelObservationReason;
	}
	public String getParallelObservationReasonCount() {
		return parallelObservationReasonCount;
	}
	public boolean isInternationalStation() {
		return internationalStation;
	}
	public boolean isInternationalStationEssential() {
		return internationalStationEssential;
	}
	public String getInternationalStationEssentialReason() {
		return internationalStationEssentialReason;
	}
	public String getInternationalStationEssentialReasonCount() {
		return internationalStationEssentialReasonCount;
	}
	public boolean isOtherSchedulingConstraints() {
		return otherSchedulingConstraints;
	}
	public String getOtherSchedulingConstraintsReason() {
		return otherSchedulingConstraintsReason;
	}
	public String getOtherSchedulingConstraintsReasonCount() {
		return otherSchedulingConstraintsReasonCount;
	}
	public boolean isCombinedDataProductRequest() {
		return combinedDataProductRequest;
	}
	public String getCombinedDataProductRequestReason() {
		return combinedDataProductRequestReason;
	}
	public String getCombinedDataProductRequestReasonCount() {
		return combinedDataProductRequestReasonCount;
	}
	public double getSensitivity() {
		return sensitivity;
	}
	public double getMaxDataRate() {
		return maxDataRate;
	}
	
	public boolean isRoProcessing() {
		return roProcessing;
	}
	public boolean isDefaultROProcessing() {
		return defaultROProcessing;
	}
	public String getRoProcessingReason() {
		return roProcessingReason;
	}
	public String getRoProcessingReasonCount() {
		return roProcessingReasonCount;
	}
	public boolean isLtaRawStorage() {
		return ltaRawStorage;
	}
	public String getLtaRawStorageReason() {
		return ltaRawStorageReason;
	}
	public String getLtaRawStorageReasonCount() {
		return ltaRawStorageReasonCount;
	}
	public boolean isLtaStorage() {
		return ltaStorage;
	}
	public String getLtaStorageReason() {
		return ltaStorageReason;
	}
	public String getLtaStorageReasonCount() {
		return ltaStorageReasonCount;
	}
	public boolean isOfflineROProcessing() {
		return offlineROProcessing;
	}
	public String getOfflineROProcessingReason() {
		return offlineROProcessingReason;
	}
	public String getOfflineROProcessingReasonCount() {
		return offlineROProcessingReasonCount;
	}
	public boolean isExternalProcessing() {
		return externalProcessing;
	}
	public String getExternalProcessingReason() {
		return externalProcessingReason;
	}
	public String getExternalProcessingReasonCount() {
		return externalProcessingReasonCount;
	}
	public boolean isCepRequesting() {
		return cepRequesting;
	}
	public String getCepRequestingReason(){
		return cepRequestingReason;
	}
	public String getCepRequestingReasonCount() {
		return cepRequestingReasonCount;
	}
	public void setTechCount(String techCount) {
		this.techCount = techCount;
	}
	public void setMaxTechCount(String maxTechCount) {
		this.maxTechCount = maxTechCount;
	}
	public void setNighttime(boolean nighttime) {
		this.nighttime = nighttime;
	}
	public void setNightTimeReason(String nightTimeReason) {
		this.nightTimeReason = nightTimeReason;
	}
	public void setNightTimeReasonCount(String nightTimeReasonCount) {
		this.nightTimeReasonCount = nightTimeReasonCount;
	}
	public void setParallelObservation(boolean parallelObservation) {
		this.parallelObservation = parallelObservation;
	}
	public void setParallelObservationReason(String parallelObservationReason) {
		this.parallelObservationReason = parallelObservationReason;
	}
	public void setParallelObservationReasonCount(
			String parallelObservationReasonCount) {
		this.parallelObservationReasonCount = parallelObservationReasonCount;
	}
	public void setInternationalStation(boolean internationalStation) {
		this.internationalStation = internationalStation;
	}
	public void setInternationalStationEssential(
			boolean internationalStationEssential) {
		this.internationalStationEssential = internationalStationEssential;
	}
	public void setInternationalStationEssentialReason(
			String internationalStationEssentialReason) {
		this.internationalStationEssentialReason = internationalStationEssentialReason;
	}
	public void setInternationalStationEssentialReasonCount(
			String internationalStationEssentialReasonCount) {
		this.internationalStationEssentialReasonCount = internationalStationEssentialReasonCount;
	}
	public void setOtherSchedulingConstraints(boolean otherSchedulingConstraints) {
		this.otherSchedulingConstraints = otherSchedulingConstraints;
	}
	public void setOtherSchedulingConstraintsReason(
			String otherSchedulingConstraintsReason) {
		this.otherSchedulingConstraintsReason = otherSchedulingConstraintsReason;
	}
	public void setOtherSchedulingConstraintsReasonCount(
			String otherSchedulingConstraintsReasonCount) {
		this.otherSchedulingConstraintsReasonCount = otherSchedulingConstraintsReasonCount;
	}
	public void setCombinedDataProductRequest(boolean combinedDataProductRequest) {
		this.combinedDataProductRequest = combinedDataProductRequest;
	}
	public void setCombinedDataProductRequestReason(
			String combinedDataProductRequestReason) {
		this.combinedDataProductRequestReason = combinedDataProductRequestReason;
	}
	public void setCombinedDataProductRequestReasonCount(
			String combinedDataProductRequestReasonCount) {
		this.combinedDataProductRequestReasonCount = combinedDataProductRequestReasonCount;
	}
	public void setSensitivity(double sensitivity) {
		this.sensitivity = sensitivity;
	}

	public void setMaxDataRate(double maxDataRate) {
		this.maxDataRate = maxDataRate;
	}
	
	public void setRoProcessing(boolean roProcessing) {
		this.roProcessing = roProcessing;
	}
	public void setDefaultROProcessing(boolean defaultROProcessing) {
		this.defaultROProcessing = defaultROProcessing;
	}
	public void setRoProcessingReason(String roProcessingReason) {
		this.roProcessingReason = roProcessingReason;
	}
	public void setRoProcessingReasonCount(String roProcessingReasonCount) {
		this.roProcessingReasonCount = roProcessingReasonCount;
	}
	public void setLtaRawStorage(boolean ltaRawStorage) {
		this.ltaRawStorage = ltaRawStorage;
	}
	public void setLtaRawStorageReason(String ltaRawStorageReason) {
		this.ltaRawStorageReason = ltaRawStorageReason;
	}
	public void setLtaRawStorageReasonCount(String ltaRawStorageReasonCount) {
		this.ltaRawStorageReasonCount = ltaRawStorageReasonCount;
	}
	public void setLtaStorage(boolean ltaStorage) {
		this.ltaStorage = ltaStorage;
	}
	public void setLtaStorageReason(String ltaStorageReason) {
		this.ltaStorageReason = ltaStorageReason;
	}
	public void setLtaStorageReasonCount(String ltaStorageReasonCount) {
		this.ltaStorageReasonCount = ltaStorageReasonCount;
	}
	public void setOfflineROProcessing(boolean offlineROProcessing) {
		this.offlineROProcessing = offlineROProcessing;
	}
	public void setOfflineROProcessingReason(String offlineROProcessingReason) {
		this.offlineROProcessingReason = offlineROProcessingReason;
	}
	public void setOfflineROProcessingReasonCount(
			String offlineROProcessingReasonCount) {
		this.offlineROProcessingReasonCount = offlineROProcessingReasonCount;
	}
	public void setExternalProcessing(boolean externalProcessing) {
		this.externalProcessing = externalProcessing;
	}
	public void setExternalProcessingReason(String externalProcessingReason) {
		this.externalProcessingReason = externalProcessingReason;
	}
	public void setExternalProcessingReasonCount(
			String externalProcessingReasonCount) {
		this.externalProcessingReasonCount = externalProcessingReasonCount;
	}
	public void setCepRequesting(boolean cepRequesting) {
		this.cepRequesting = cepRequesting;
	}
	public void setCepRequestingReason(String cepRequestingReason) {
		this.cepRequestingReason = cepRequestingReason;
	}
	public void setCepRequestingReasonCount(String cepRequestingReasonCount) {
		this.cepRequestingReasonCount = cepRequestingReasonCount;
	}
	public boolean isDisplayParallelObservatione() {
		return displayParallelObservatione;
	}
	public boolean isDisplayInternationalStation() {
		return displayInternationalStation;
	}
	public boolean isDisplayOtherSchedulingConstraints() {
		return displayOtherSchedulingConstraints;
	}
	public boolean isDisplayCombinedDataProductRequest() {
		return displayCombinedDataProductRequest;
	}
	public boolean isDisplaySensitivity() {
		return displaySensitivity;
	}
	public boolean isDisplayMaxDataRate() {
		return displayMaxDataRate;
	}
	public boolean isDisplayLtaStorage() {
		return displayLtaStorage;
	}
	public boolean isDisplayOfflineROProcessing() {
		return displayOfflineROProcessing;
	}
	public boolean isDisplayExternalProcessing() {
		return displayExternalProcessing;
	}
	public boolean isDisplayCepRequesting() {
		return displayCepRequesting;
	}
	public void setDisplayParallelObservatione(boolean displayParallelObservatione) {
		this.displayParallelObservatione = displayParallelObservatione;
	}
	public void setDisplayInternationalStation(boolean displayInternationalStation) {
		this.displayInternationalStation = displayInternationalStation;
	}
	public void setDisplayOtherSchedulingConstraints(
			boolean displayOtherSchedulingConstraints) {
		this.displayOtherSchedulingConstraints = displayOtherSchedulingConstraints;
	}
	public void setDisplayCombinedDataProductRequest(
			boolean displayCombinedDataProductRequest) {
		this.displayCombinedDataProductRequest = displayCombinedDataProductRequest;
	}
	public void setDisplaySensitivity(boolean displaySensitivity) {
		this.displaySensitivity = displaySensitivity;
	}
	public void setDisplayMaxDataRate(boolean displayMaxDataRate) {
		this.displayMaxDataRate = displayMaxDataRate;
	}
	public void setDisplayLtaStorage(boolean displayLtaStorage) {
		this.displayLtaStorage = displayLtaStorage;
	}
	public void setDisplayOfflineROProcessing(boolean displayOfflineROProcessing) {
		this.displayOfflineROProcessing = displayOfflineROProcessing;
	}
	public void setDisplayExternalProcessing(boolean displayExternalProcessing) {
		this.displayExternalProcessing = displayExternalProcessing;
	}
	public void setDisplayCepRequesting(boolean displayCepRequesting) {
		this.displayCepRequesting = displayCepRequesting;
	}
	public boolean isDisplayROProcessing() {
		return displayROProcessing;
	}
	public void setDisplayROProcessing(boolean displayROProcessing) {
		this.displayROProcessing = displayROProcessing;
	}
	public boolean isDisplayNightTime() {
		return displayNightTime;
	}
	public void setDisplayNightTime(boolean displayNightTime) {
		this.displayNightTime = displayNightTime;
	}
	public boolean isDisplayObservationStrategy() {
		return displayObservationStrategy;
	}
	public void setDisplayObservationStrategy(boolean displayObservationStrategy) {
		this.displayObservationStrategy = displayObservationStrategy;
	}
	public boolean isDisplayFillerTime() {
		return displayFillerTime;
	}
	public void setDisplayFillerTime(boolean displayFillerTime) {
		this.displayFillerTime = displayFillerTime;
	}
	public boolean isFillerTime() {
		return fillerTime;
	}
	public void setFillerTime(boolean fillerTime) {
		this.fillerTime = fillerTime;
	}
	public String getFillerTimeReason() {
		return fillerTimeReason;
	}
	public void setFillerTimeReason(String fillerTimeReason) {
		this.fillerTimeReason = fillerTimeReason;
	}
	public boolean isDisplayCoObservationTeam() {
		return displayCoObservationTeam;
	}
	public void setDisplayCoObservationTeam(boolean displayCoObservingteam) {
		this.displayCoObservationTeam = displayCoObservingteam;
	}
	public boolean isCoObservationTeam() {
		return coObservationTeam;
	}
	public void setCoObservationTeam(boolean coObservationTeam) {
		this.coObservationTeam = coObservationTeam;
	}
	public String getFillerTimeReasonCount() {
		return fillerTimeReasonCount;
	}
	public void setFillerTimeReasonCount(String fillerTimeReasonCount) {
		this.fillerTimeReasonCount = fillerTimeReasonCount;
	}
	public String getLtaStorageLocation() {
		return ltaStorageLocation;
	}
	public void setLtaStorageLocation(String ltaStorageLocation) {
		this.ltaStorageLocation = ltaStorageLocation;
	}
	public boolean isDisplayLtaStorageAdvanced() {
		return displayLtaStorageAdvanced;
	}
	public void setDisplayLtaStorageAdvanced(boolean displayLtaStorageAdvanced) {
		this.displayLtaStorageAdvanced = displayLtaStorageAdvanced;
	}
	public boolean isDisplayROProcessingWithoutSelfCal() {
		return displayROProcessingWithoutSelfCal;
	}
	public void setDisplayROProcessingWithoutSelfCal(boolean displayROProcessingWithoutSelfCal) {
		this.displayROProcessingWithoutSelfCal = displayROProcessingWithoutSelfCal;
	}
		
}
