// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.configuration;

import java.util.Hashtable;

public class CollaborationConfiguration {
	private static Hashtable emailConfigurations = new Hashtable();
	private static String acceptInvitation = null;
	
	public void addEmailConfiguration(String type, EmailConfiguration emailConfiguration){
		emailConfigurations.put(type,emailConfiguration);
	}
	public static EmailConfiguration getEmailConfiguration(String type){
		return (EmailConfiguration) emailConfigurations.get(type);
	}
	public Hashtable getEmailConfigurations(){
		return emailConfigurations;
	}
	public static String getAcceptInvitation() {
		return acceptInvitation;
	}
	public static void setAcceptInvitation(String acceptInvitation) {
		CollaborationConfiguration.acceptInvitation = acceptInvitation;
	}


}
