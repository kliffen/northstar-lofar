-- MySQL dump 10.10
--
-- Host: localhost    Database: northstar
-- ------------------------------------------------------
-- Server version	5.0.15-nt-max

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping data for table `category`
--


/*!40000 ALTER TABLE `category` DISABLE KEYS */;
LOCK TABLES `category` WRITE;
INSERT INTO `category` VALUES (1,'regular'),(2,'service'),(3,'urgent');
UNLOCK TABLES;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

--
-- Dumping data for table `resourcetype`
--


/*!40000 ALTER TABLE `resourcetype` DISABLE KEYS */;
LOCK TABLES `resourcetype` WRITE;
INSERT INTO `resourcetype` VALUES (1,'time','hours'),(2,'time','minutes'),(3,'time','seconds'),(4,'time','nights'),(5,'Storage','GB');
UNLOCK TABLES;
/*!40000 ALTER TABLE `resourcetype` ENABLE KEYS */;

--
-- Dumping data for table `role`
--


/*!40000 ALTER TABLE `role` DISABLE KEYS */;
LOCK TABLES `role` WRITE;
INSERT INTO `role` VALUES (1,'PI',NULL),(2,'Correponding author',NULL),(3,'Potential observer',NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;



--
-- Dumping data for table `status`
--


/*!40000 ALTER TABLE `status` DISABLE KEYS */;
LOCK TABLES `status` WRITE;
INSERT INTO `status` VALUES (1,'in preparation','',NULL),(2,'submitted','',NULL),(3,'under review','',NULL),(4,'accepted','',NULL),(5,'rejected','',NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `status` ENABLE KEYS */;

/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

