// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * NonRegisteredMember.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: NonRegisteredMember.java,v 1.1 2006-05-02 10:01:10 verhoef Exp $
 *
 */
package eu.radionet.northstar.data.entities;



/**
 * The NonRegisteredMember provides
 *
 * @author Bastiaan Verhoef
 *
 */
public class NonRegisteredMember extends Member {
    protected String name = null;
    protected String affiliation = null;
    protected String email = null;
    protected String country = null;
	protected boolean invite = false;
	protected boolean removeInvitation = false;
    /**
     * @return Returns the affiliation.
     */
    public String getAffiliation() {
        return affiliation;
    }
    /**
     * @param affiliation The affiliation to set.
     */
    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }
    /**
     * @return Returns the country.
     */
    public String getCountry() {
        return country;
    }
    /**
     * @param country The country to set.
     */
    public void setCountry(String country) {
        this.country = country;
    }
    /**
     * @return Returns the email.
     */
    public String getEmail() {
        return email.trim();
    }
    /**
     * @param email The email to set.
     */
    public void setEmail(String email) {
        this.email = email.trim();
    }
    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }
    /**
     * @param name The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

	public boolean isInvite() {
		return invite;
	}
	public void setInvite(boolean invite) {
		this.invite = invite;
	}
	public boolean isRemoveInvitation() {
		return removeInvitation;
	}
	public void setRemoveInvitation(boolean removeInvitation) {
		this.removeInvitation = removeInvitation;
	}
}
