// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Jan 17, 2006
 *
 *
 */
package eu.radionet.northstar.business.util;

import org.w3c.dom.Document;

import eu.radionet.northstar.business.configuration.OtherFilesConfiguration;

public abstract class OtherFilesConfigurator
{
	public abstract OtherFilesConfiguration configure(Document otherFilesDocument) throws Exception;
    /* this is an empty place holder. It is implemeted by other classes */
}
