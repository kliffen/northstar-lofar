// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Repository.java 
 *
 * Created on Feb 1, 2005
 *
 * Version $Id: Repository.java,v 1.9 2008-05-27 12:29:28 boelen Exp $
 *
 */
package eu.radionet.northstar.data;

import java.io.Serializable;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import nl.astron.database.hibernate3.connection.OldHibernateConnectionFactory;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.cfg.Configuration;

import eu.radionet.northstar.data.entities.Category;
import eu.radionet.northstar.data.entities.FigureFile;
import eu.radionet.northstar.data.entities.Member;
import nl.astron.useradministration.data.entities.User;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.UserTelescope;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ResourceType;
import eu.radionet.northstar.data.entities.Role;
import eu.radionet.northstar.data.entities.ScientificFile;
import eu.radionet.northstar.data.entities.Semester;
import eu.radionet.northstar.data.entities.Status;
import eu.radionet.northstar.data.entities.TechnicalDetailsFile;
import eu.radionet.northstar.data.entities.admin.StatusEmail;
import eu.radionet.northstar.data.entities.collaboration.Invitation;
import eu.radionet.northstar.data.entities.collaboration.ProposalLock;

/**
 * The Repository provides
 * 
 * @author Bastiaan Verhoef
 */
public class Repository {
	protected Log log = LogFactory.getLog(this.getClass());

	protected static OldHibernateConnectionFactory connectionFactory = null;

	private SimpleDateFormat dateConverter = null;

	/**
	 * Default constructor
	 */
	public Repository() throws ConnectionException {
		dateConverter = new SimpleDateFormat();
		dateConverter.applyPattern("yyyy-MM-dd HH:mm:ss");
		dateConverter.setTimeZone(TimeZone.getTimeZone("UTC"));

		if (connectionFactory == null) {
			throw new ConnectionException();
		}

	}

	public static void init(Set resources) throws ConnectionException {
		Configuration configuration = new DataConfiguration();
		ClassLoader classLoader = (ClassLoader) Thread.currentThread()
				.getContextClassLoader();
		URL url = classLoader.getResource("northstar.cfg.xml");
		configuration.configure(url);
		Iterator resourcesIterator = resources.iterator();
		while (resourcesIterator.hasNext()) {
			String resource = (String) resourcesIterator.next();
			configuration.addResource(resource);
		}
		connectionFactory = new OldHibernateConnectionFactory(
				"northstar.cfg.xml", configuration);
	}

	/**
	 * The store method stores an persistent object in the database
	 * 
	 * @param object
	 * @throws DatabaseException
	 */
	public void store(Object object) throws DatabaseException {
		connectionFactory.store(object);
	}

	public void store(List objects) throws DatabaseException {
		connectionFactory.store(objects);
	}

	/**
	 * The delete method delete an persistent object
	 * 
	 * @param object
	 * @throws DatabaseException
	 */
	public void delete(Object object) throws DatabaseException {
		connectionFactory.delete(object);
	}

	public void delete(List objects) throws DatabaseException {
		connectionFactory.delete(objects);
	}

	/**
	 * The get method retrieves object by class and id
	 * 
	 * @param clazz
	 * @param id
	 * @return
	 * @throws DatabaseException
	 */
	public Object get(Class clazz, Serializable id) throws DatabaseException {
		return connectionFactory.get(clazz, id);

	}

	/**
	 * The getDefaultRoles method retrieve default roles
	 * 
	 * @return List of roles
	 * @throws DatabaseException
	 */
	public List getDefaultRoles() throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(new Integer(1));
		parameters.add(new Integer(2));
		List roleList = connectionFactory.find(
				"FROM Role WHERE id = ? OR id= ?", parameters);
		return roleList;

	}

	public ResourceType getResourceType(String name, String unit)
			throws DatabaseException {
		List parameters = new ArrayList();
		ResourceType resourceType = null;
		parameters.add(name);
		parameters.add(unit);
		List results = connectionFactory.find(
				"FROM ResourceType WHERE name=? AND unit = ?", parameters);
		if (!results.isEmpty()) {
			resourceType = (ResourceType) results.get(0);
		}
		return resourceType;
	}

	public List getProposals(String[] statusses, String[] categories,
			String[] telescopes, String[] semesters, String[] communities)
			throws DatabaseException {
		List parameters = new ArrayList();
		List proposals = new ArrayList();
		if (statusses == null || categories == null || telescopes == null
				|| semesters == null || communities == null
				|| statusses.length == 0 || categories.length == 0
				|| telescopes.length == 0 || semesters.length == 0
				|| communities.length == 0) {
			return proposals;
		}
		String select = "SELECT proposal ";
		String from = "FROM Proposal AS proposal INNER JOIN "
				+ "proposal.currentStatus as proposalStatus INNER JOIN "
				+ "proposalStatus.status as status INNER JOIN "
				+ "proposal.semester as semester INNER JOIN "
				+ "semester.category AS category ";
		String where = "WHERE (";
		for (int i = 0; i < statusses.length; i++) {
			where += "status.code = ? ";
			parameters.add(statusses[i]);
			if (i < statusses.length - 1) {
				where += "OR ";
			}
		}

		where += " ) AND (";
		for (int i = 0; i < categories.length; i++) {
			where += "category.code = ? ";
			parameters.add(categories[i]);
			if (i < categories.length - 1) {
				where += "OR ";
			}
		}
		where += " ) AND (";
		for (int i = 0; i < telescopes.length; i++) {
			where += "semester.telescope = ? ";
			parameters.add(telescopes[i]);
			if (i < telescopes.length - 1) {
				where += "OR ";
			}
		}
		where += " ) AND (";
		for (int i = 0; i < semesters.length; i++) {
			if (semesters[i] == null) {
			} else {
				where += "semester.semester = ? ";
				parameters.add(semesters[i]);
			}
			if (i < semesters.length - 1) {
				where += "OR ";
			}
		}
		where += " ) AND (";
		for (int i = 0; i < communities.length; i++) {
			if (communities[i] == null) {
			} else {
				where += "semester.community = ? ";
				parameters.add(communities[i]);
			}
			if (i < communities.length - 1) {
				where += "OR ";
			}
		}
		where += " )";
		String orderBy = "ORDER BY proposal.code asc";
		proposals = connectionFactory.find(select + from + where + orderBy,
				parameters);
		return proposals;
	}

	public List getProposalSummaries(boolean forceProjId, String[] statusses, String[] categories,
			String[] telescopes, String[] semesters, String[] communities)
			throws DatabaseException {
		List parameters = new ArrayList();
		List proposalSummaries = new ArrayList();
		if (statusses == null || categories == null || telescopes == null
				|| semesters == null || communities == null
				|| statusses.length == 0 || categories.length == 0
				|| telescopes.length == 0 || semesters.length == 0
				|| communities.length == 0) {
			return proposalSummaries;
		}
		String select = "SELECT proposal.id, proposal.code, proposal.currentStatus, proposal.semester, proposal.justification ";
		String from = "FROM Proposal AS proposal INNER JOIN "
				+ "proposal.currentStatus as proposalStatus INNER JOIN "
				+ "proposalStatus.status as status INNER JOIN "
				+ "proposal.semester as semester INNER JOIN "
				+ "semester.category AS category ";
				
		String where = "WHERE (";
		for (int i = 0; i < statusses.length; i++) {
			where += "status.code = ? ";
			parameters.add(statusses[i]);
			if (i < statusses.length - 1) {
				where += "OR ";
			}
		}

		where += " ) AND (";
		for (int i = 0; i < categories.length; i++) {
			where += "category.code = ? ";
			parameters.add(categories[i]);
			if (i < categories.length - 1) {
				where += "OR ";
			}
		}
		where += " ) AND (";
		for (int i = 0; i < telescopes.length; i++) {
			where += "semester.telescope = ? ";
			parameters.add(telescopes[i]);
			if (i < telescopes.length - 1) {
				where += "OR ";
			}
		}
		where += " ) AND (";
		for (int i = 0; i < semesters.length; i++) {
			if (semesters[i] == null) {
			} else {
				where += "semester.semester = ? ";
				parameters.add(semesters[i]);
			}
			if (i < semesters.length - 1) {
				where += "OR ";
			}
		}
		where += " ) AND (";
		for (int i = 0; i < communities.length; i++) {
			if (communities[i] == null) {
			} else {
				where += "semester.community = ? ";
				parameters.add(communities[i]);
			}
			if (i < communities.length - 1) {
				where += "OR ";
			}
		}
		if(forceProjId){
			where += " ) AND ( proposal.code IS NOT NULL ";
		}
		where += " )";
		String orderBy = "ORDER BY proposal.code asc";
		proposalSummaries = connectionFactory.find(select + from + where + orderBy,
				parameters);
		return proposalSummaries;
	}
	public List getPiAndContactAuthor(Integer proposalId) throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(new Integer(Constants.PI));
		parameters.add(new Integer(Constants.CONTACT_AUTHOR));
		parameters.add(proposalId);
		String query = "SELECT member FROM Proposal as proposal "
				+ "INNER JOIN proposal.members as member "
				+ "INNER JOIN member.roles as role "
				+ "WHERE (role.id = ? OR role.id = ? )AND proposal.id = ?";
		return connectionFactory.find(query, parameters);
	}

	/*
	 * remove it
	 */
	public List getProposalsSummaries(String[] statusses, Integer semesterId)
			throws DatabaseException {
		List parameters = new ArrayList();
		List proposalSummaries = new ArrayList();
		if (statusses == null || statusses.length == 0) {
			return proposalSummaries;
		}
		String select = "SELECT proposal.id, proposal.code, justification.title, semester.id ";
		String from = "FROM Proposal AS proposal "
				+ "INNER JOIN proposal.currentStatus as proposalStatus "
				+ "INNER JOIN proposalStatus.status as status "
				+ "INNER JOIN proposal.semester as semester "
				+ "INNER JOIN proposal.justification as justification ";
		String where = "WHERE (";
		for (int i = 0; i < statusses.length; i++) {
			where += "status.code = ? ";
			parameters.add(statusses[i]);
			if (i < statusses.length - 1) {
				where += "OR ";
			}
		}

		where += " ) AND (";
		where += " semester.id = ? ";
		parameters.add(semesterId);
		where += " ) ";
		String orderBy = "ORDER BY proposal.code asc";
		proposalSummaries = connectionFactory.find(select + from + where
				+ orderBy, parameters);
		return proposalSummaries;
	}
	
	public List getProposalsSummaries(Integer semesterId)
			throws DatabaseException {
		List parameters = new ArrayList();
		List proposalSummaries = new ArrayList();
		String select = "SELECT proposal.id, proposal.code, justification.title, semester.id ";
		String from = "FROM Proposal AS proposal "
				+ "INNER JOIN proposal.semester as semester "
				+ "INNER JOIN proposal.justification as justification ";
		String where = "WHERE (";
		where += " semester.id = ? ";
		parameters.add(semesterId);
		where += " ) ";
		String orderBy = "ORDER BY proposal.code asc";
		proposalSummaries = connectionFactory.find(select + from + where
				+ orderBy, parameters);
		return proposalSummaries;
	}

	public List getProposalsByIds(String[] ids) throws DatabaseException {
		List parameters = new ArrayList();
		List proposals = new ArrayList();
		if (ids == null || ids.length == 0) {
			return proposals;
		}
		String select = "SELECT proposal ";
		String from = "FROM Proposal AS proposal ";
		String where = "WHERE ";
		for (int i = 0; i < ids.length; i++) {
			where += "proposal.id = ? ";
			parameters.add(ids[i]);
			if (i < ids.length - 1) {
				where += "OR ";
			}
		}
		String orderBy = "ORDER BY proposal.code asc";
		proposals = connectionFactory.find(select + from + where + orderBy,
				parameters);
		return proposals;
	}

	public List getSemesters() throws DatabaseException {
		return connectionFactory.find("FROM Semester", new ArrayList());
	}

	public List getNotClosedSemesters() throws DatabaseException {
		return connectionFactory.find("SELECT semester "
				+ "FROM Semester as semester "
				+ "WHERE semester.closed = false ", new ArrayList());
	}

	public List getNextSemesters(Semester semester, Calendar currentDate)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(semester.getTelescope());
		parameters.add(semester.getCommunity());
		parameters.add(semester.getCategory().getCode());
		parameters.add(currentDate.getTime());
		String select = "SELECT semester ";
		String from = "FROM Semester AS semester INNER JOIN semester.category as category ";
		String where = "WHERE semester.telescope = ? AND semester.community = ?";
		where += " AND category.code = ? AND semester.deadLine > ? ";
		String orderBy = " ORDER BY semester.deadLine";
		return connectionFactory.find(select + from + where + orderBy,
				parameters);
	}

	public Semester getSemester(Integer id) throws DatabaseException {
		return (Semester) connectionFactory.get(Semester.class, id);
	}

	public List getSemesters(String[] categories, String[] telescopes,
			String[] communities, Calendar date) throws DatabaseException {
		List parameters = new ArrayList();
		List semesters = new ArrayList();
		if (categories == null || telescopes == null || communities == null
				|| categories.length == 0 || telescopes.length == 0
				|| communities.length == 0) {
			return semesters;
		}
		String select = "SELECT semester ";
		String from = "FROM Semester AS semester INNER JOIN semester.category as category ";
		String where = "WHERE (";
		for (int i = 0; i < telescopes.length; i++) {
			where += "semester.telescope = ? ";
			parameters.add(telescopes[i]);
			if (i < telescopes.length - 1) {
				where += "OR ";
			}
		}

		where += " ) AND (";
		for (int i = 0; i < categories.length; i++) {
			where += "category.code = ? ";
			parameters.add(categories[i]);
			if (i < categories.length - 1) {
				where += "OR ";
			}
		}
		where += " ) AND (";
		for (int i = 0; i < communities.length; i++) {
			where += "semester.community = ? ";
			parameters.add(communities[i]);
			if (i < communities.length - 1) {
				where += "OR ";
			}
		}
		where += " )";
		if (date != null) {
			where += " AND semester.deadLine > ? ";
			parameters.add(date);
		}
		String orderBy = " ORDER BY semester.deadLine asc";
		semesters = connectionFactory.find(select + from + where + orderBy,
				parameters);
		return semesters;
	}

	public List getStatusses() throws DatabaseException {
		return connectionFactory.find("FROM Status", new ArrayList());
	}

	public Role getPiRole() throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(new Integer(1));
		Role role = null;
		List roleList = connectionFactory.find("FROM Role WHERE id = ?",
				parameters);
		if (!roleList.isEmpty()) {
			role = (Role) roleList.get(0);
		}
		return role;
	}

	public Role getContactAuthorRole() throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(new Integer(2));
		Role role = null;
		List roleList = connectionFactory.find("FROM Role WHERE id = ?",
				parameters);
		if (!roleList.isEmpty()) {
			role = (Role) roleList.get(0);
		}
		return role;
	}

	public Role getPotentialObserverRole() throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(new Integer(3));
		Role role = null;
		List roleList = connectionFactory.find("FROM Role WHERE id = ?",
				parameters);
		if (!roleList.isEmpty()) {
			role = (Role) roleList.get(0);
		}
		return role;
	}

	public Proposal getOwnProposal(Integer proposalId, Integer userId)
			throws DatabaseException {
		List members = this.getRegisteredMembers(userId);
		if (members.size() > 0) {
			return getOwnProposal(proposalId, members);
		} else {
			return null;
		}
	}

	/**
	 * The getProposal method retrieves a proposal by proposalId and memberId
	 * 
	 * @param proposalId
	 * @param members
	 * @return Proposal
	 * @throws DatabaseException
	 */
	public Proposal getOwnProposal(Integer proposalId, List members)
			throws DatabaseException {
		List parameters = new ArrayList();
		Proposal proposal = null;
		String select = "SELECT proposal ";
		String from = "FROM Proposal AS proposal "
				+ "INNER JOIN proposal.members AS member ";
		String where = "WHERE proposal.id = ? AND ( ";
		parameters.add(proposalId);
		for (int i = 0; i < members.size() - 1; i++) {
			Member member = (Member) members.get(i);
			where += " member.id = ? OR ";
			parameters.add(member.getId());
		}
		where += " member.id = ?)";
		parameters.add(((Member) members.get(members.size() - 1)).getId());

		List proposalList = connectionFactory.find(select + from + where,
				parameters);
		if (!proposalList.isEmpty())
			proposal = (Proposal) proposalList.get(0);
		return proposal;
	}

	public Proposal getProposal(Integer proposalId) throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(proposalId);
		Proposal proposal = null;
		String select = "SELECT proposal ";
		String from = "FROM Proposal AS proposal ";
		String where = "WHERE proposal.id= ?";
		List proposalList = connectionFactory.find(select + from + where,
				parameters);
		if (!proposalList.isEmpty())
			proposal = (Proposal) proposalList.get(0);
		return proposal;
	}

	public List getReviewProposals() throws DatabaseException {
		String selectfrom = "SELECT proposal FROM Proposal AS proposal INNER JOIN proposal.currentStatus as proposalStatus INNER JOIN proposalStatus.status as status WHERE status.code = 'submitted' OR status.code = 'under review' ORDER BY proposal.code asc";
		return connectionFactory.find(selectfrom, new ArrayList());

	}

	public List getAcceptedProposals() throws DatabaseException {
		String selectfrom = "SELECT proposal FROM Proposal AS proposal INNER JOIN proposal.currentStatus as proposalStatus INNER JOIN proposalStatus.status as status WHERE status.code = 'accepted' ORDER BY proposal.code asc";
		return connectionFactory.find(selectfrom, new ArrayList());

	}

	public List getSubmittedProposals() throws DatabaseException {
		String selectfrom = "SELECT proposal FROM Proposal AS proposal INNER JOIN proposal.currentStatus as proposalStatus INNER JOIN proposalStatus.status as status WHERE status.code = 'submitted' ORDER BY proposal.code asc";
		return connectionFactory.find(selectfrom, new ArrayList());

	}

	public List getSubmittedProposalsBySemesterId(Integer semesterId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(semesterId);
		return connectionFactory
				.find(
						"SELECT proposal FROM Proposal as proposal "
								+ "INNER JOIN proposal.semester as semester  "
								+ "INNER JOIN proposal.currentStatus as proposalStatus "
								+ "INNER JOIN proposalStatus.status as status "
								+ "WHERE semester.id = ? AND status.code = 'submitted'",
						parameters);
	}

	/**
	 * The getOwnProposals method retrieves all own proposals
	 * 
	 * @param members
	 * @return
	 * @throws DatabaseException
	 */
	public List getOwnProposals(List members) throws DatabaseException {
		List parameters = new ArrayList();
		String select = "SELECT proposal ";
		String from = "FROM Proposal AS proposal "
				+ "INNER JOIN proposal.members as member ";
		String where = "WHERE ";

		for (int i = 0; i < members.size() - 1; i++) {
			Member member = (Member) members.get(i);
			where += " member.id = ? OR ";
			parameters.add(member.getId());
		}
		where += " member.id = ?";
		parameters.add(((Member) members.get(members.size() - 1)).getId());

		return connectionFactory.find(select + from + where, parameters);
	}

	public List getOwnProposals(String[] telescopes, Integer userId)
			throws DatabaseException {
		List parameters = new ArrayList();
		if (telescopes == null || telescopes.length == 0) {
			return new ArrayList();
		}
		String select = "SELECT proposal ";
		String from = "FROM Proposal AS proposal "
				+ "INNER JOIN proposal.members as member "
				+ "INNER JOIN proposal.semester as semester ";
		String where = "WHERE (";
		for (int i = 0; i < telescopes.length; i++) {
			where += "semester.telescope = ? ";
			parameters.add(telescopes[i]);
			if (i < telescopes.length - 1) {
				where += "OR ";
			}
		}
		where += " ) AND ( member.id IN ";
		where += "( SELECT regmember.id "
				+ "FROM RegisteredMember as regmember "
				+ "WHERE regmember.userId = ? ))" + "ORDER BY proposal.id asc";
		parameters.add(userId);
		return connectionFactory.find(select + from + where, parameters);
	}

		
	
	public List getOwnProposals(String telescope, List statusCodes,
			Integer userId) throws DatabaseException {
		List parameters = new ArrayList();
		if (statusCodes.size() == 0) {
			return new ArrayList();
		}
		String select = "SELECT proposal ";
		String from = "FROM Proposal AS proposal "
				+ "INNER JOIN proposal.members as member "
				+ "INNER JOIN proposal.semester as semester "
				+ "LEFT JOIN proposal.currentStatus AS currentStatus "
				+ "INNER JOIN currentStatus.status AS status ";
		String where = "WHERE (";
		for (int i = 0; i < statusCodes.size(); i++) {
			where += "status.code = ? ";
			parameters.add(statusCodes.get(i));
			if (i < statusCodes.size() - 1) {
				where += "OR ";
			}
		}
		parameters.add(telescope);
		where += " ) AND semester.telescope = ? AND ( ";
		String subqueryRegisteredMember = "SELECT regmember.id "
					+ "FROM RegisteredMember as regmember "
					+ "WHERE regmember.userId = ?";
		String subqueryInvitation = "SELECT invitation.proposalId, invitation.memberId " +
						"FROM Invitation AS invitation " +
						"WHERE invitation.userId = ?";
		where += " member.id IN ( " + subqueryRegisteredMember +  " ) " +
				"OR (proposal.id, member.id) IN (" +subqueryInvitation + " ) " 						
				+ " ) ORDER BY currentStatus.statusTime desc";
		parameters.add(userId);
		parameters.add(userId);
		return connectionFactory.find(select + from + where, parameters);
	}

	/**
	 * The getRegisteredMembers method retrieves registered members by userId
	 * 
	 * @param userId
	 * @return List of registered members
	 * @throws DatabaseException
	 */
	public List getRegisteredMembers(Integer userId) throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(userId);
		return connectionFactory.find("FROM RegisteredMember WHERE userId = ?",
				parameters);

	}

	/**
	 * The getStatus method retrieves the status by id
	 * 
	 * @return Status
	 * @throws DatabaseException
	 */
	// nv: 11 feb 2005
	public Status getStatus(int id) throws DatabaseException {
		return (Status) connectionFactory.get(Status.class, new Integer(id));
	}

	/**
	 * The getDefaultStatus method retrieves default status
	 * 
	 * @return Status
	 * @throws DatabaseException
	 */
	public Status getDefaultStatus() throws DatabaseException {
		return (Status) connectionFactory.get(Status.class, new Integer(1));
	}

	/**
	 * The getInPreparationStatus method retrieves the 'in preparation' status
	 * 
	 * @return Status
	 * @throws DatabaseException
	 */
	public Status getInPreparationStatus() throws DatabaseException {
		return (Status) connectionFactory.get(Status.class, new Integer(1));
	}

	/**
	 * The getSubmittedStatus method retrieves submitted status
	 * 
	 * @return Status
	 * @throws DatabaseException
	 */
	public Status getSubmittedStatus() throws DatabaseException {
		return (Status) connectionFactory.get(Status.class, new Integer(2));
	}

	/**
	 * The getUnderReviewStatus method retrieves 'under review' status
	 * 
	 * @return Status
	 * @throws DatabaseException
	 */
	public Status getUnderReviewStatus() throws DatabaseException {
		return (Status) connectionFactory.get(Status.class, new Integer(3));
	}

	public Status getAcceptedStatus() throws DatabaseException {
		return (Status) connectionFactory.get(Status.class, new Integer(4));
	}

	public Status getRejectedStatus() throws DatabaseException {
		return (Status) connectionFactory.get(Status.class, new Integer(5));
	}

	/**
	 * The getDefaultCategory method retrieves default category
	 * 
	 * @return
	 * @throws DatabaseException
	 */
	public Category getDefaultCategory() throws DatabaseException {
		return (Category) connectionFactory.get(Category.class, new Integer(1));
	}

	public Category getCategory(Integer id) throws DatabaseException {
		return (Category) connectionFactory.get(Category.class, id);
	}

	/**
	 * The method retrieves available categories
	 * 
	 * @return List of categories
	 * @throws DatabaseException
	 */
	public List getCategories() throws DatabaseException {
		return connectionFactory.find("FROM Category", new ArrayList());
	}

	public StatusEmail getStatusEmailByProposalStatusId(Integer proposalStatusId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(proposalStatusId);
		StatusEmail statusEmail = null;
		List statusEmailList = connectionFactory.find(
				"FROM StatusEmail WHERE proposalStatusId = ?", parameters);
		if (!statusEmailList.isEmpty()) {
			statusEmail = (StatusEmail) statusEmailList.get(0);
		}
		return statusEmail;
	}

	public ScientificFile getScientificFile(Integer justificationId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(justificationId);
		ScientificFile justificationFile = null;
		List justificationFileList = connectionFactory.find(
				"FROM ScientificFile WHERE justificationId = ?", parameters);
		if (!justificationFileList.isEmpty()) {
			justificationFile = (ScientificFile) justificationFileList.get(0);
		}
		return justificationFile;
	}

	public TechnicalDetailsFile getTechnicalDetailsFile(Integer justificationId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(justificationId);
		TechnicalDetailsFile justificationFile = null;
		List justificationFileList = connectionFactory.find(
				"FROM TechnicalDetailsFile WHERE justificationId = ?",
				parameters);
		if (!justificationFileList.isEmpty()) {
			justificationFile = (TechnicalDetailsFile) justificationFileList
					.get(0);
		}
		return justificationFile;
	}

	public FigureFile getFigureFile(Integer justificationId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(justificationId);
		FigureFile justificationFile = null;
		List justificationFileList = connectionFactory.find(
				"FROM FigureFile WHERE justificationId = ?", parameters);
		if (!justificationFileList.isEmpty()) {
			if(justificationFileList.size() > 1){
				// if there are multiple files, get the latest, and remove the rest...
				for(int i=0;i<justificationFileList.size()-1;i++){
					FigureFile deleteFile = (FigureFile)  justificationFileList.get(i);
					connectionFactory.delete(deleteFile);
				}
			}
			justificationFile = (FigureFile) justificationFileList.get(justificationFileList.size()-1);
		}
		return justificationFile;
	}

	public List getJustificationFiles(Integer justificationId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(justificationId);
		return connectionFactory.find(
				"FROM JustificationFile WHERE justificationId = ?", parameters);
	}

	public List getSemesters(String telescope, Calendar currentDate)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(telescope);
		parameters.add(currentDate);
		return connectionFactory
				.find(
						"FROM Semester as sem WHERE sem.telescope = ? AND sem.deadLine > ?",
						parameters);
	}

	public List getAvailableSemesters(String telescope, Calendar currentDate)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(telescope);
		parameters.add(currentDate);
		parameters.add(currentDate);
		return connectionFactory
				.find(
						"FROM Semester as sem WHERE sem.telescope = ? AND sem.deadLine > ? AND sem.availableDate < ?",
						parameters);
	}

	public List getAvailableSemesters(String telescope, String community,
			Calendar currentDate) throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(telescope);
		parameters.add(community);
		parameters.add(currentDate);
		parameters.add(currentDate);
		return connectionFactory
				.find(
						"FROM Semester  as sem WHERE sem.telescope = ? AND sem.community = ? AND sem.deadLine > ? AND sem.availableDate < ?",
						parameters);
	}

	/**
	 * The getSemesters method get semesters from telescope without all given
	 * semesters id
	 * 
	 * @param telescope
	 * @param semesterIds
	 * @return
	 * @throws DatabaseException
	 */
	public List getAvailableSemesters(String telescope, String community,
			String category, Calendar currentDate) throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(telescope);
		parameters.add(community);
		parameters.add(category);
		parameters.add(currentDate);
		parameters.add(currentDate);
		String query = "SELECT sem FROM Semester as sem INNER JOIN sem.category as category ";
		query += "WHERE sem.telescope = ? AND sem.community = ? ";
		query += " AND category.code = ? AND sem.deadLine > ? AND sem.availableDate < ? ";
		query += "ORDER BY semester asc";
		return connectionFactory.find(query, parameters);
	}

	public Semester getAvailableSemester(String telescope, String community,
			String semester, String category, Calendar currentDate)
			throws DatabaseException {
		Semester result = null;
		List parameters = new ArrayList();
		parameters.add(telescope);
		parameters.add(community);
		parameters.add(semester);
		parameters.add(category);
		parameters.add(currentDate);
		parameters.add(currentDate);
		String query = "SELECT sem ";
		query += "FROM Semester as sem INNER JOIN sem.category as category ";
		query += "WHERE sem.telescope = ? AND ";
		query += "sem.community = ? AND ";
		query += "sem.semester = ? AND ";
		query += "category.code = ? AND sem.deadLine > ? AND  sem.availableDate < ? ";
		List results = connectionFactory.find(query, parameters);
		if (!results.isEmpty()) {
			result = (Semester) results.get(0);
		}
		return result;
	}

	public Semester getSemester(String telescope, String community,
			String semester, String category, Calendar currentDate)
			throws DatabaseException {
		Semester result = null;
		List parameters = new ArrayList();
		parameters.add(telescope);
		parameters.add(community);
		parameters.add(semester);
		parameters.add(category);
		parameters.add(currentDate);
		String query = "SELECT sem ";
		query += "FROM Semester as sem INNER JOIN sem.category as category ";
		query += "WHERE sem.telescope = ? AND ";
		query += "sem.community = ? AND ";
		query += "sem.semester = ? AND ";
		query += "category.code = ? AND sem.deadLine > ? ";
		List results = connectionFactory.find(query, parameters);
		if (!results.isEmpty()) {
			result = (Semester) results.get(0);
		}
		return result;
	}

	public Semester getSemester(String telescope, String community,
			String semester, String category) throws DatabaseException {
		Semester result = null;
		List parameters = new ArrayList();
		parameters.add(telescope);
		parameters.add(community);
		parameters.add(semester);
		parameters.add(category);
		String query = "SELECT sem ";
		query += "FROM Semester as sem INNER JOIN sem.category as category ";
		query += "WHERE sem.telescope = ? AND ";
		query += "sem.community = ? AND ";
		query += "sem.semester = ? AND ";
		query += "category.code = ?";
		List results = connectionFactory.find(query, parameters);
		if (!results.isEmpty()) {
			result = (Semester) results.get(0);
		}
		return result;
	}

	public Object copy(Object source, Object destination)
			throws DatabaseException {
		return connectionFactory.copy(source, destination);
	}

	public Object copy(Object object) throws DatabaseException {
		return connectionFactory.copy(object);
	}

	public List retrieveMembersWithRole(String roleName)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(roleName);
		return connectionFactory.find("FROM Role WHERE name = ?", parameters);
	}

	public List retrieveStatuses() throws DatabaseException {
		return connectionFactory.find("FROM Status", new ArrayList());
	}

	public ProposalLock getValidProposalLock(Integer proposalId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(proposalId);
		ProposalLock proposalLock = null;
		List results = connectionFactory.find(
				"FROM ProposalLock WHERE proposalId = ? " + " AND valid=true",
				parameters);
		if (!results.isEmpty()) {
			proposalLock = (ProposalLock) results.get(0);
		}
		return proposalLock;
	}

	public List getValidProposalLocks() throws DatabaseException {

		return connectionFactory.find(
				"FROM ProposalLock WHERE valid=true ORDER BY lastAction desc",
				new ArrayList());
	}

	public ProposalLock getProposalLock(Integer proposalId, Integer userId,
			String sessionId) throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(proposalId);
		parameters.add(userId);
		parameters.add(sessionId);
		ProposalLock proposalLock = null;
		List results = connectionFactory
				.find(
						"SELECT proposalLock FROM ProposalLock as proposalLock WHERE proposalLock.proposalId = ?"
								+ " AND proposalLock.userId = ?"
								+ " AND proposalLock.sessionId = ?", parameters);
		if (!results.isEmpty()) {
			proposalLock = (ProposalLock) results.get(0);
		}
		return proposalLock;
	}

	public List getProposalLocks(Integer userId, String sessionId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(userId);
		parameters.add(sessionId);
		List results = connectionFactory
				.find(
						"SELECT proposalLock FROM ProposalLock as proposalLock WHERE proposalLock.userId = ? "
								+ " AND proposalLock.sessionId = ?", parameters);
		return results;
	}

	public boolean isUniqueInvitationKey(String invitationKey)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(invitationKey);
		List list = connectionFactory
				.find(
						"SELECT invitation.acceptCode FROM Invitation AS invitation WHERE invitation.acceptCode = ?",
						parameters);

		return list.isEmpty();
	}

	public Invitation getInvitation(String invitationKey)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(invitationKey);
		Invitation invitation = null;
		List results = connectionFactory
				.find(
						"SELECT invitation FROM Invitation as invitation WHERE invitation.acceptCode = ?",
						parameters);
		if (!results.isEmpty()) {
			invitation = (Invitation) results.get(0);
		}
		return invitation;

	}

	public List getAcceptedInvitations(Integer proposalId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(proposalId);
		List results = connectionFactory.find(
				"FROM Invitation  WHERE proposalId = ? "
						+ " AND userId is not null", parameters);
		return results;

	}

	public List getAllAcceptedInvitations() throws DatabaseException {

		List results = connectionFactory.find(
				"FROM Invitation  WHERE userId is not null", new ArrayList());
		return results;

	}

	public List getInvitations(Integer proposalId) throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(proposalId);
		List results = connectionFactory.find(
				"FROM Invitation  WHERE proposalId = ? "
						+ " ORDER BY memberId asc", parameters);
		return results;

	}

	public List getAcceptedInvitationsByUserId(Integer userId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(userId);
		List results = connectionFactory.find(
				"FROM Invitation WHERE userId = ? ", parameters);
		return results;

	}

	public Invitation getAcceptedInvitation(Integer proposalId, Integer userId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(proposalId);
		parameters.add(userId);
		Invitation invitation = null;
		List results = connectionFactory.find(
				"FROM Invitation  WHERE proposalId = ? " + " AND userId = ? ",
				parameters);
		if (!results.isEmpty()) {
			invitation = (Invitation) results.get(0);
		}
		return invitation;

	}

	public Invitation getInvitation(Integer proposalId, Integer memberId)
			throws DatabaseException {
		List parameters = new ArrayList();
		parameters.add(proposalId);
		parameters.add(memberId);
		Invitation invitation = null;
		List results = connectionFactory.find(
				"FROM Invitation  WHERE proposalId = ? " + " AND memberId = ?",
				parameters);
		if (!results.isEmpty()) {
			invitation = (Invitation) results.get(0);
		}
		return invitation;

	}

	public void reassociate(Object object) throws DatabaseException {
		if (object != null) {
			connectionFactory.reassociate(object);
		}
	}

	public void deleteObject(Class clazz, Integer id) throws DatabaseException {
		Object object = connectionFactory.get(clazz, id);
		/*
		 * if exists delete unrefenced object
		 */
		if (object != null) {
			connectionFactory.delete(object);
		}
	}
	
	public List getUsersTelescope(String Telescope)throws DatabaseException {
		List parameters = new ArrayList();
		String query = new String();

			parameters.add(Telescope);
			query = "FROM UserTelescope WHERE telescope = ? ";
		return connectionFactory.find(query,parameters);
	}
	
	public List getUsersNotTelescope(String Telescope, List userList)throws DatabaseException {
		List parameters = new ArrayList();
		String query = new String();
		parameters.add(Telescope);
		
		query = "FROM UserTelescope WHERE telescope = ?";
		List results = connectionFactory.find(query,parameters);
		List userIds = new ArrayList();
		
		// create a list of usrIds 
		Iterator idIterator = userList.iterator();
		while (idIterator.hasNext()){
			User admin = (User) idIterator.next();
			// if userid is not in resultlist
			if(!isInList(admin.getId(),results)){
				userIds.add(admin.getId());
			}
		}
			
		return userIds;
	}
	
	private boolean isInList(Integer Id,List searchList){
		Iterator idIterator = searchList.iterator();
		while (idIterator.hasNext()){
			UserTelescope userTelescope = (UserTelescope) idIterator.next();
			if(userTelescope.getUserId().equals(Id)){
				return true;
			}
		}
		return false;
	}
	
	public List getTelescopeAccessRights(Integer userId)throws DatabaseException {
		List parameters = new ArrayList();
		String query = new String();
		
		query= "FROM UserTelescope WHERE userid = ?";
		parameters.add(userId);
		return connectionFactory.find(query,parameters);
	}

	public NonRegisteredMember getMember(Integer memberId) throws DatabaseException {
		List parameters = new ArrayList();
		NonRegisteredMember member = null;
		
		parameters.add(memberId);
		List results = connectionFactory.find(
				"FROM NonRegisteredMember WHERE memberid = ? ",
				parameters);
		if (!results.isEmpty()) {
			member = (NonRegisteredMember) results.get(0);
		}
		return member;
	}

	public void convertUsertelescope(Integer memberId, Integer userId) throws DatabaseException {
		// set the userid of all usertelescope items where the member id matches.
		List parameters = new ArrayList();
		parameters.add(memberId);
		memberId=new Integer(memberId.intValue() * -1 );
		parameters.add(memberId);
		List results = connectionFactory.find(
				"FROM UserTelescope WHERE memberid = ? OR memberid = ? ",
				parameters);
		Iterator resit = results.iterator();
		while(resit.hasNext()){
			UserTelescope utel = (UserTelescope) resit.next();
			utel.setUserId(userId);
			store(utel);
		}
		
	}

	public List getMembersNotProposal() throws DatabaseException {
		List results = connectionFactory.find(
				"FROM Member WHERE proposalId is Null ");
		return results;

	}

	public List getProposalChildSummaries(Integer id) {
		List results = connectionFactory.find("SELECT proposal.id, proposal.code, justification.title, semester.id "+
				"FROM Proposal AS proposal "+
				"INNER JOIN proposal.semester as semester " +
				"INNER JOIN proposal.justification as justification " +
				"WHERE proposal.parentId ='"+id.toString()+"'");
		return results;
	}

	public List getEnvelopeSheetProposalSummaries(Integer id) {
		List results = connectionFactory.find("SELECT proposal.id, proposal.code, justification.title, semester.id "+
				"FROM Proposal AS proposal "+
				"INNER JOIN proposal.semester as semester " +
				"INNER JOIN proposal.members as member " +
				"INNER JOIN proposal.justification as justification " +
				"INNER JOIN proposal.currentStatus as proposalstatus " + 
				"INNER JOIN proposalstatus.status as status " + 
				"WHERE  semester.category = 9 AND member.id in (select regmember.id from RegisteredMember as regmember where regmember.userId='"+id.toString()+"' ) ");
		return results; // remove status from the WHERE clause status.id = 2 AND
	}

	public Integer getJustificationId(Integer proposalid) {
		Integer result = null;
		List results = connectionFactory.find("SELECT justification.id "+
				"FROM Proposal AS proposal "+
				"INNER JOIN proposal.justification as justification " +
				"WHERE proposal.id ='"+proposalid.toString()+"'");
		
		if (!results.isEmpty()) {
			result = (Integer) results.get(0);
		}
		return result;
	}
	
	
	
}