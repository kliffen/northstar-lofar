// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest.instrument;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

public class InstrumentBean {
	
	
		protected String id = null;
		protected String telescope = null;
		
		protected String selectedTelescopeConfiguration = null;
		protected List telescopeConfigurations = new ArrayList();
		protected boolean displayTelescopeDropbox = false;
		
		protected String selectedInstrumentConfiguration  = null;
		protected List instrumentConfigurations = new ArrayList();
		protected List selectedInstrumentConfigurations = new ArrayList();
		protected boolean displayInstrumentConfiguration = false;
		protected List deSelectedInstConf = new ArrayList();
		protected List moonOptions = new ArrayList();
		protected List skyQualities = new ArrayList();
		protected List waterOptions = new ArrayList();
		protected List seeingOptions = new ArrayList();
		protected List telescopeSchedulingOptions = new ArrayList();

		protected boolean displayFilters = false;
		protected boolean displayFiltersw2 = false;
		protected boolean displaySlitw = false;
		protected boolean displayGratingw = false;
		protected boolean displayGrismw = false;
		protected boolean displayFilterDetails = false;
		protected boolean displayReadOut = false;
		protected boolean displayOrderFilter = false;
		protected boolean displayTelescopeScheduling=false;
		protected boolean displayCustomFilter=false;
		
		protected boolean displayMode = false;
		
		protected boolean displayPolarimetry = false;
		protected boolean displayGuideline = false;
		protected String guidelineUrl = null;
		
		protected boolean displayWavelength = false;
		protected boolean displayOrder = false;
		protected boolean displaySlits = false;
		protected boolean displayGrating = false;
		protected boolean displayGrism = false;
		protected boolean displayComments = false;
		protected boolean displayCCD = false;
		
		protected boolean displayFrameExposureTime = false;
		protected boolean displayMicrostepping = false;
		protected boolean displayCoronagraphicMask = false;
		protected boolean displaySlitPositionAngle = false;

		protected boolean requiredInstrumentConfiguration = false;
		
		protected boolean requiredFilters = false;
		protected boolean requiredFilterDetails = false;
		protected boolean requiredReadOut = false;
		protected boolean requiredOrderFilter = false;
		
		protected boolean requiredMode = false;

		protected boolean requiredPolarimetry = false;
		protected boolean requiredGuideline = false;

		protected boolean requiredWavelength = false;
		protected boolean requiredOrder = false;
		protected boolean requiredSlits = false;
		protected boolean requiredGrating = false;
		protected boolean requiredGrism = false;
		protected boolean requiredComments = false;
		protected boolean requiredCCD = false;

		protected boolean requiredNonDefaultFrameExposureTimeDetails = false;
		protected boolean requiredMicrostepping = false;
		protected boolean requiredCoronagraphicMask = false;
		protected boolean requiredSlitPositionAngle = false;

		
		protected String comments = null;
		protected boolean nonDefaultFrameExposureTime = false;
		protected String nonDefaultFrameExposureTimeDetails = null;
		protected String frameExposureTime = null;
		protected String selectedMicrostepping = null;
		protected boolean coronagraphicMask = false;
		protected List microstepping = new ArrayList();

		protected String[] selectedFilters = null;
		protected String[] selectedFiltersw2 = null;
		protected String[] selectedSlitw = null;
		protected String[] selectedGrismw = null;
		protected String[] selectedGratingw = null;
		
		protected String selectedReadOut = null;
		protected String selectedMode = null;
		protected String selectedCCD = null;
		protected String selectedOrderFilter = null;
		protected String selectedTelescopeScheduling = null;
		protected String instrumentUrl = null;

		protected List filters = new ArrayList();
		protected List filtersw2 = new ArrayList();
		protected List slitw = new ArrayList();
		protected List grismw = new ArrayList();
		protected List gratingw = new ArrayList();
		protected List readOuts = new ArrayList();
		protected List orderFilters = new ArrayList();
		protected List modes = new ArrayList();
		protected List CCDs = new ArrayList();
		

		protected boolean polarimetry = false;
		protected boolean guideline = false;
		
		protected String wavelength = null;

		protected String filterDetails = null;

		protected String order = null;

		protected String selectedSlit = null;
		protected List slits = new ArrayList();
		protected String slitPositionAngle = "0";

		protected String selectedGrating = null;
		
		protected List gratings = new ArrayList();
		
		protected String selectedGrism = null;
		protected List grisms = new ArrayList();
		
		protected boolean displayObservationRequestedTime = false;
		protected boolean requiredObservationRequestedTime = false;
		protected String observationRequestedTime=null;
		
		protected String observationMinimumTime=null;
		
		protected boolean displayObservationPhase = false;
		protected List observationPhaseOptions = new ArrayList();
		protected String selectedObservationPhase = null;
		
		protected boolean displayObservationWeather = false;
		protected List observationWeatherOptions = new ArrayList();
		protected List selectedObservationWeather = new ArrayList();
		
		protected boolean displayObservationNoiseLevel = false;
		protected boolean requiredObservationNoiseLevel = false;
		protected String observationNoiseLevel = null;
		
		protected boolean displayObservationMode = false;
		protected List observationModeOptions = new ArrayList();
		protected String selectedObservationMode = null;
		
		protected boolean displayObservationSeeing = false;
		protected List observationSeeingOptions = new ArrayList();
		protected String selectedObservationSeeing = null;
		
		protected boolean displayObservationDates = false;
		protected String observationDates = null;

		protected boolean displayRequiredSchedConstraints = false;
		protected boolean displayPreferredSchedConstraints = false;
						 
		protected String requiredSchedConstraints = null;
		protected String preferredSchedConstraints = null;
		protected String timeSuffix = null;
		
		// for lofar
		protected boolean displayPostProcessing = false;
		protected boolean displayCalculatedSize = false;
		
		protected boolean averaging = false;
		protected boolean flagging = false;
		protected boolean calibration = false;
		protected boolean imaging= false;
		protected boolean cohstokes= false;
		protected boolean incohstokes= false;
		protected boolean complexVoltage = false;
		protected boolean flysEye = false;
		protected boolean rawVoltage = false;
		protected boolean summaryPlots = false;
		protected boolean displayStoreRawData = false;
		protected boolean storeRawData = false;
		protected boolean displayStoreUVData = false;
		protected boolean storeUVData = false;
		
		protected String filterFrequency = null;
		protected String filterBandwidth = null;
		protected boolean filterContiguousCoverage = true;
		
		protected String customStationSpecifics = null;
		//protected String averagingFrequency = null;
		//protected String averagingTime = null;
		protected String fieldSizeX = null;
		protected String pixelSizeX = null;
		protected String fieldSizeY = null;
		protected String pixelSizeY = null;
		
		protected boolean displayStation = false;
		protected List stations = new ArrayList();
		protected String selectedStation = null;
		protected boolean requiredStation = false;

		protected boolean displayClock = false;
		protected List clocks = new ArrayList();
		protected String selectedClock = null;
		protected boolean requiredClock = false;
		
		protected boolean displayOneFilter = false;
		protected List oneFilters = new ArrayList();
		protected String selectedOneFilter = null;
		protected boolean requiredOneFilter = false;

		protected boolean displayAntenna = false;
		protected List antennas = new ArrayList();
		protected String selectedAntenna = null;
		protected boolean requiredAntenna = false;

		protected boolean displaySubarrayPointings = false;
		protected String subarrayPointings = null;
		protected boolean requiredSubarrayPointings = false;

		protected boolean displayIntegrationTime = false;
		protected String integrationTime = null;
		protected boolean requiredIntegrationTime = false;
		
		protected boolean displayInstrumentDetails = false;
		protected String instrumentDetails = null;
		
		protected boolean customFilter = false;
		//protected boolean contiguousCoverage = true;
		
		protected boolean displaySingleStationSpecifics = false;
		protected String singleStationSpecifics = null;
		
		protected List polarizations = new ArrayList();
		protected String selectedPolarizations = null;
		protected List incPolarizations = new ArrayList();
		protected String selectedIncPolarizations = null;
		
		protected boolean displaySubFilter=false;
		
		protected String estimatedDataStorage = null;
		protected String dataStorageSpecifics = null;
		protected String dataCalculationType = null;
		
		protected boolean displayPiggyBack=false;
		protected boolean displayPiggyBackFeatures =false;
		protected boolean enablePiggyBack;
		protected String piggyBackSpecifics=null;
		
		protected boolean displayDirectDataStorage=false;
		protected boolean directDataStorage=false;
		protected boolean displaySchedulingInfo=false;
		
		//protected boolean displayPostAveraging=false;
		protected boolean displayPostImaging=false;
		protected boolean displayPostBeamformed=false;
		protected boolean displayPostTimeseries=false;
		protected boolean displayPostInterfero =false;
		
		protected String frequencyChannels = null;
		protected String beams = null;
		protected String cohTaBeams = null;
		protected String incohTaBeams = null;
		protected String rings = null;
		
		protected String channels = null;
		protected String incchannels = null;
		protected List channellist = new ArrayList();
		//protected String intsteps = "1";
		protected String intsteps = null;
		//protected String  incohIntsteps = null;
		protected String  incohIntsteps = "1";
		protected String samplerate = null;
		protected String events = null;
		protected boolean displayBackupStrategy=false;
		protected String backupStrategy = null;
		protected boolean displayCalibrationRequirements = false;
		protected String calibrationRequirements = null;
		protected String estimatedCepLoad = null;
		
		protected boolean displayObservationMinimumTime = false;
		
		// new pipeline
		protected boolean displayPipelineOptions = false;
		protected boolean displayProcessingMode = false;
		protected boolean displaySubbandsPerImage = false;
		protected boolean displayFieldOfView = false;
		protected boolean displayFlaggingStrategy = false;
		protected boolean displayPostAveraging = false;
		protected boolean displayPostDemixing = false;
		
		protected String selectedProcessingMode = null;
		protected String subbandsPerImage = null;
		protected String fieldOfView = null;
		protected String selectedFlaggingStrategy = null;
		protected String averagingTime = null;
		protected String averagingFrequency = null;
		protected boolean demixing = false;
		protected String demixingTime = null;
		protected String demixingFrequency = null;
		
		protected boolean displayDemixingSource = false;
		protected List demixingSources = new ArrayList();
		protected String[] selectedDemixingSources = null;
		
		protected List flaggingStrategies = new ArrayList();
		protected List processingModes = new ArrayList();

		protected boolean displayCorrelatedVisibilities = false; 
	    protected boolean correlatedVisibilities = false;
	    protected boolean displayImagingParameters = false;

	    protected boolean enableCombinedOutputProductSelection = false;
		protected boolean beamConf;
		protected boolean interferoConf;
		protected boolean tbbConf;
	
		protected boolean otherConf;
		/**
		 * fields for pipeline parameters.
		 */
		protected boolean skipRFI = false;
		protected boolean skipFolding = false;
		protected boolean skipPdmp = false;
		protected boolean skipDspsr = false;
		protected boolean skipPrepfold = false;
		protected boolean singlePulse = false;
		protected boolean rratsAnalysis = false;
		protected boolean skipDynamicAverage = false;
		protected String subintegrationLength = null;
		protected boolean convertRawData = false;
		protected String threshold = null;
		protected String sigmaLimit = null;
		protected String numberOfBlocks = null;
		protected String prepfoldOptions = null;
		protected String presubbandOptions = null;
		protected String rfifind = null;
		protected String dspsrOptions = null;
		protected String digifilOptions = null;
		protected String predataOptions = null;
		protected String bf2fitsOptions = null;
		protected String pulsarDesc = null;
		
		/***
		 * fields for TBB
		 */
	    protected String tbbRequestedMinimumTime = null;
		protected String tbbRequestedSATime = null;
		protected String tbbTriggerLength = null;
		protected String tbbTriggerRate = null;
		protected List tbbTriggerSource = new ArrayList();
		protected String tbbSelectedObservationId = null;
		protected List tbbObservations = new ArrayList(); 
		protected String tbbTriggerSourceId=null;
		protected String tbbExposureTime =null;
		
		protected String totalSum = null;
		public String getTbbExposureTime() {
			return tbbExposureTime;
		}

		public void setTbbExposureTime(String tbbExposureTime) {
			this.tbbExposureTime = tbbExposureTime;
		}

		public static final String MULTITEM_SEPARATOR = "," ;
	    
		public void reset()
	    {
	        
	    }

		public List getMoonOptions() {
			return moonOptions;
		}

		public void setMoonOptions(List moonOptions) {
			this.moonOptions = moonOptions;
		}

		public List getSkyQualities() {
			return skyQualities;
		}

		public void setSkyQualities(List skyQualities) {
			this.skyQualities = skyQualities;
		}

		public List getWaterOptions() {
			return waterOptions;
		}

		public void setWaterOptions(List waterOptions) {
			this.waterOptions = waterOptions;
		}

		public String getSelectedTelescopeConfiguration() {
			return selectedTelescopeConfiguration;
		}

		public void setSelectedTelescopeConfiguration(
				String selectedTelescopeConfiguration) {
			this.selectedTelescopeConfiguration = selectedTelescopeConfiguration;
		}

		public List getTelescopeConfigurations() {
			return telescopeConfigurations;
		}

		public void setTelescopeConfigurations(List telescopeConfigurations) {
			this.telescopeConfigurations = telescopeConfigurations;
		}

		public boolean isDisplayTelescopeDropbox() {
			return displayTelescopeDropbox;
		}

		public void setDisplayTelescopeDropbox(boolean displayTelescopeDropbox) {
			this.displayTelescopeDropbox = displayTelescopeDropbox;
		}

		public String getSelectedInstrumentConfiguration() {
			return selectedInstrumentConfiguration;
		}

		public void setSelectedInstrumentConfiguration(
				String selectedInstrumentConfiguration) {
			this.selectedInstrumentConfiguration = selectedInstrumentConfiguration;
		}

		public List getInstrumentConfigurations() {
			return instrumentConfigurations;
		}

		public void setInstrumentConfigurations(List instrumentConfigurations) {
			this.instrumentConfigurations = instrumentConfigurations;
		}

		public boolean isDisplayInstrumentConfiguration() {
			return displayInstrumentConfiguration;
		}

		public void setDisplayInstrumentConfiguration(
				boolean displayInstrumentConfiguration) {
			this.displayInstrumentConfiguration = displayInstrumentConfiguration;
		}

		public boolean isDisplayFilters() {
			return displayFilters;
		}

		public void setDisplayFilters(boolean displayFilters) {
			this.displayFilters = displayFilters;
		}

		public boolean isDisplayFilterDetails() {
			return displayFilterDetails;
		}

		public void setDisplayFilterDetails(boolean displayFilterDetails) {
			this.displayFilterDetails = displayFilterDetails;
		}

		public boolean isDisplayReadOut() {
			return displayReadOut;
		}

		public void setDisplayReadOut(boolean displayReadOut) {
			this.displayReadOut = displayReadOut;
		}

		public boolean isDisplayOrderFilter() {
			return displayOrderFilter;
		}

		public void setDisplayOrderFilter(boolean displayOrderFilter) {
			this.displayOrderFilter = displayOrderFilter;
		}

		public boolean isDisplayMode() {
			return displayMode;
		}

		public void setDisplayMode(boolean displayMode) {
			this.displayMode = displayMode;
		}

		public boolean isDisplayPolarimetry() {
			return displayPolarimetry;
		}

		public void setDisplayPolarimetry(boolean displayPolarimetry) {
			this.displayPolarimetry = displayPolarimetry;
		}

		public boolean isDisplayGuideline() {
			return displayGuideline;
		}

		public void setDisplayGuideline(boolean displayGuideline) {
			this.displayGuideline = displayGuideline;
		}

		public String getGuidelineUrl() {
			return guidelineUrl;
		}

		public void setGuidelineUrl(String guidelineUrl) {
			this.guidelineUrl = guidelineUrl;
		}

		public boolean isDisplayWavelength() {
			return displayWavelength;
		}

		public void setDisplayWavelength(boolean displayWavelength) {
			this.displayWavelength = displayWavelength;
		}

		public boolean isDisplayOrder() {
			return displayOrder;
		}

		public void setDisplayOrder(boolean displayOrder) {
			this.displayOrder = displayOrder;
		}

		public boolean isDisplaySlits() {
			return displaySlits;
		}

		public void setDisplaySlits(boolean displaySlits) {
			this.displaySlits = displaySlits;
		}

		public boolean isDisplayGrating() {
			return displayGrating;
		}

		public void setDisplayGrating(boolean displayGrating) {
			this.displayGrating = displayGrating;
		}

		public boolean isDisplayGrism() {
			return displayGrism;
		}

		public void setDisplayGrism(boolean displayGrism) {
			this.displayGrism = displayGrism;
		}

		public boolean isDisplayComments() {
			return displayComments;
		}

		public void setDisplayComments(boolean displayComments) {
			this.displayComments = displayComments;
		}

		public boolean isDisplayFrameExposureTime() {
			return displayFrameExposureTime;
		}

		public void setDisplayFrameExposureTime(boolean displayFrameExposureTime) {
			this.displayFrameExposureTime = displayFrameExposureTime;
		}

		public boolean isDisplayMicrostepping() {
			return displayMicrostepping;
		}

		public void setDisplayMicrostepping(boolean displayMicrostepping) {
			this.displayMicrostepping = displayMicrostepping;
		}

		public boolean isDisplayCoronagraphicMask() {
			return displayCoronagraphicMask;
		}

		public void setDisplayCoronagraphicMask(boolean displayCoronagraphicMask) {
			this.displayCoronagraphicMask = displayCoronagraphicMask;
		}

		public boolean isDisplaySlitPositionAngle() {
			return displaySlitPositionAngle;
		}

		public void setDisplaySlitPositionAngle(boolean displaySlitPositionAngle) {
			this.displaySlitPositionAngle = displaySlitPositionAngle;
		}

		public boolean isRequiredInstrumentConfiguration() {
			return requiredInstrumentConfiguration;
		}

		public void setRequiredInstrumentConfiguration(
				boolean requiredInstrumentConfiguration) {
			this.requiredInstrumentConfiguration = requiredInstrumentConfiguration;
		}

		public boolean isRequiredFilters() {
			return requiredFilters;
		}

		public void setRequiredFilters(boolean requiredFilters) {
			this.requiredFilters = requiredFilters;
		}

		public boolean isRequiredFilterDetails() {
			return requiredFilterDetails;
		}

		public void setRequiredFilterDetails(boolean requiredFilterDetails) {
			this.requiredFilterDetails = requiredFilterDetails;
		}

		public boolean isRequiredReadOut() {
			return requiredReadOut;
		}

		public void setRequiredReadOut(boolean requiredReadOut) {
			this.requiredReadOut = requiredReadOut;
		}

		public boolean isRequiredOrderFilter() {
			return requiredOrderFilter;
		}

		public void setRequiredOrderFilter(boolean requiredOrderFilter) {
			this.requiredOrderFilter = requiredOrderFilter;
		}

		public boolean isRequiredMode() {
			return requiredMode;
		}

		public void setRequiredMode(boolean requiredMode) {
			this.requiredMode = requiredMode;
		}

		public boolean isRequiredPolarimetry() {
			return requiredPolarimetry;
		}

		public void setRequiredPolarimetry(boolean requiredPolarimetry) {
			this.requiredPolarimetry = requiredPolarimetry;
		}

		public boolean isRequiredGuideline() {
			return requiredGuideline;
		}

		public void setRequiredGuideline(boolean requiredGuideline) {
			this.requiredGuideline = requiredGuideline;
		}

		public boolean isRequiredWavelength() {
			return requiredWavelength;
		}

		public void setRequiredWavelength(boolean requiredWavelength) {
			this.requiredWavelength = requiredWavelength;
		}

		public boolean isRequiredOrder() {
			return requiredOrder;
		}

		public void setRequiredOrder(boolean requiredOrder) {
			this.requiredOrder = requiredOrder;
		}

		public boolean isRequiredSlits() {
			return requiredSlits;
		}

		public void setRequiredSlits(boolean requiredSlits) {
			this.requiredSlits = requiredSlits;
		}

		public boolean isRequiredGrating() {
			return requiredGrating;
		}

		public void setRequiredGrating(boolean requiredGrating) {
			this.requiredGrating = requiredGrating;
		}

		public boolean isRequiredGrism() {
			return requiredGrism;
		}

		public void setRequiredGrism(boolean requiredGrism) {
			this.requiredGrism = requiredGrism;
		}

		public boolean isRequiredComments() {
			return requiredComments;
		}

		public void setRequiredComments(boolean requiredComments) {
			this.requiredComments = requiredComments;
		}

		public boolean isRequiredNonDefaultFrameExposureTimeDetails() {
			return requiredNonDefaultFrameExposureTimeDetails;
		}

		public void setRequiredNonDefaultFrameExposureTimeDetails(
				boolean requiredNonDefaultFrameExposureTimeDetails) {
			this.requiredNonDefaultFrameExposureTimeDetails = requiredNonDefaultFrameExposureTimeDetails;
		}

		public boolean isRequiredMicrostepping() {
			return requiredMicrostepping;
		}

		public void setRequiredMicrostepping(boolean requiredMicrostepping) {
			this.requiredMicrostepping = requiredMicrostepping;
		}

		public boolean isRequiredCoronagraphicMask() {
			return requiredCoronagraphicMask;
		}

		public void setRequiredCoronagraphicMask(boolean requiredCoronagraphicMask) {
			this.requiredCoronagraphicMask = requiredCoronagraphicMask;
		}

		public boolean isRequiredSlitPositionAngle() {
			return requiredSlitPositionAngle;
		}

		public void setRequiredSlitPositionAngle(boolean requiredSlitPositionAngle) {
			this.requiredSlitPositionAngle = requiredSlitPositionAngle;
		}

		public String getComments() {
			return comments;
		}

		public void setComments(String comments) {
			this.comments = comments;
		}

		public boolean isNonDefaultFrameExposureTime() {
			return nonDefaultFrameExposureTime;
		}

		public void setNonDefaultFrameExposureTime(boolean nonDefaultFrameExposureTime) {
			this.nonDefaultFrameExposureTime = nonDefaultFrameExposureTime;
		}

		public String getNonDefaultFrameExposureTimeDetails() {
			return nonDefaultFrameExposureTimeDetails;
		}

		public void setNonDefaultFrameExposureTimeDetails(
				String nonDefaultFrameExposureTimeDetails) {
			this.nonDefaultFrameExposureTimeDetails = nonDefaultFrameExposureTimeDetails;
		}

		public String getFrameExposureTime() {
			return frameExposureTime;
		}

		public void setFrameExposureTime(String frameExposureTime) {
			this.frameExposureTime = frameExposureTime;
		}

		public String getSelectedMicrostepping() {
			return selectedMicrostepping;
		}

		public void setSelectedMicrostepping(String selectedMicrostepping) {
			this.selectedMicrostepping = selectedMicrostepping;
		}

		public boolean isCoronagraphicMask() {
			return coronagraphicMask;
		}

		public void setCoronagraphicMask(boolean coronagraphicMask) {
			this.coronagraphicMask = coronagraphicMask;
		}

		public List getMicrostepping() {
			return microstepping;
		}

		public void setMicrostepping(List microstepping) {
			this.microstepping = microstepping;
		}

		public String[] getSelectedFilters() {
			return selectedFilters;
		}

		public void setSelectedFilters(String[] selectedFilters) {
			this.selectedFilters = selectedFilters;
		}

		public String getSelectedReadOut() {
			return selectedReadOut;
		}

		public void setSelectedReadOut(String selectedReadOut) {
			this.selectedReadOut = selectedReadOut;
		}

		public String getSelectedMode() {
			return selectedMode;
		}

		public void setSelectedMode(String selectedMode) {
			this.selectedMode = selectedMode;
		}

		public String getSelectedOrderFilter() {
			return selectedOrderFilter;
		}

		public void setSelectedOrderFilter(String selectedOrderFilter) {
			this.selectedOrderFilter = selectedOrderFilter;
		}

		public List getFilters() {
			return filters;
		}

		public void setFilters(List filters) {
			this.filters = filters;
		}

		public List getReadOuts() {
			return readOuts;
		}

		public void setReadOuts(List readOuts) {
			this.readOuts = readOuts;
		}

		public List getOrderFilters() {
			return orderFilters;
		}

		public void setOrderFilters(List orderFilters) {
			this.orderFilters = orderFilters;
		}

		public List getModes() {
			return modes;
		}

		public void setModes(List modes) {
			this.modes = modes;
		}

		public boolean isPolarimetry() {
			return polarimetry;
		}

		public void setPolarimetry(boolean polarimetry) {
			this.polarimetry = polarimetry;
		}

		public boolean isGuideline() {
			return guideline;
		}

		public void setGuideline(boolean guideline) {
			this.guideline = guideline;
		}

		public String getWavelength() {
			return wavelength;
		}

		public void setWavelength(String wavelength) {
			this.wavelength = wavelength;
		}

		public String getFilterDetails() {
			return filterDetails;
		}

		public void setFilterDetails(String filterDetails) {
			this.filterDetails = filterDetails;
		}

		public String getOrder() {
			return order;
		}

		public void setOrder(String order) {
			this.order = order;
		}

		public String getSelectedSlit() {
			return selectedSlit;
		}

		public void setSelectedSlit(String selectedSlit) {
			this.selectedSlit = selectedSlit;
		}

		public List getSlits() {
			return slits;
		}

		public void setSlits(List slits) {
			this.slits = slits;
		}

		public String getSlitPositionAngle() {
			return slitPositionAngle;
		}

		public void setSlitPositionAngle(String slitPositionAngle) {
			this.slitPositionAngle = slitPositionAngle;
		}

		public String getSelectedGrating() {
			return selectedGrating;
		}

		public void setSelectedGrating(String selectedGrating) {
			this.selectedGrating = selectedGrating;
		}

		public List getGratings() {
			return gratings;
		}

		public void setGratings(List gratings) {
			this.gratings = gratings;
		}

		public String getSelectedGrism() {
			return selectedGrism;
		}

		public void setSelectedGrism(String selectedGrism) {
			this.selectedGrism = selectedGrism;
		}

		public List getGrisms() {
			return grisms;
		}

		public void setGrisms(List grisms) {
			this.grisms = grisms;
		}

		public boolean isDisplayObservationWeather() {
			return displayObservationWeather;
		}

		public void setDisplayObservationWeather(boolean displayObservationWeather) {
			this.displayObservationWeather = displayObservationWeather;
		}

		public List getObservationWeatherOptions() {
			return observationWeatherOptions;
		}

		public void setObservationWeatherOptions(List observationWeatherOptions) {
			this.observationWeatherOptions = observationWeatherOptions;
		}

		public List getSelectedObservationWeather() {
			return selectedObservationWeather;
		}

		public void setSelectedObservationWeather(String selectedObservationWeather) {
			if(selectedObservationWeather != null){
				String[] split = selectedObservationWeather.split(MULTITEM_SEPARATOR) ;
				for(String item : split)
					this.selectedObservationWeather.add(item) ;
			}
		}

		public final boolean isDisplayObservationNoiseLevel() {
			return displayObservationNoiseLevel;
		}

		public final void setDisplayObservationNoiseLevel(
				boolean displayObservationNoiseLevel) {
			this.displayObservationNoiseLevel = displayObservationNoiseLevel;
		}

		public final boolean isRequiredObservationNoiseLevel() {
			return requiredObservationNoiseLevel;
		}

		public final void setRequiredObservationNoiseLevel(
				boolean requiredObservationNoiseLevel) {
			this.requiredObservationNoiseLevel = requiredObservationNoiseLevel;
		}

		public final String getObservationNoiseLevel() {
			return observationNoiseLevel;
		}

		public final void setObservationNoiseLevel(String observationNoiseLevel) {
			this.observationNoiseLevel = observationNoiseLevel;
		}

		public boolean isDisplayObservationMode() {
			return displayObservationMode;
		}

		public void setDisplayObservationMode(boolean displayObservationMode) {
			this.displayObservationMode = displayObservationMode;
		}

		public List getObservationModeOptions() {
			return observationModeOptions;
		}

		public void setObservationModeOptions(List observationModeOptions) {
			this.observationModeOptions = observationModeOptions;
		}

		public String getSelectedObservationMode() {
			return selectedObservationMode;
		}

		public void setSelectedObservationMode(String selectedObservationMode) {
			this.selectedObservationMode = selectedObservationMode;
		}

		public boolean isDisplayObservationSeeing() {
			return displayObservationSeeing;
		}

		public void setDisplayObservationSeeing(boolean displayObservationSeeing) {
			this.displayObservationSeeing = displayObservationSeeing;
		}

		public List getObservationSeeingOptions() {
			return observationSeeingOptions;
		}

		public void setObservationSeeingOptions(List observationSeeingOptions) {
			this.observationSeeingOptions = observationSeeingOptions;
		}

		public String getSelectedObservationSeeing() {
			return selectedObservationSeeing;
		}

		public void setSelectedObservationSeeing(String selectedObservationSeeing) {
			this.selectedObservationSeeing = selectedObservationSeeing;
		}

		public boolean isDisplayObservationDates() {
			return displayObservationDates;
		}

		public void setDisplayObservationDates(boolean displayObservationDates) {
			this.displayObservationDates = displayObservationDates;
		}

		public String getObservationDates() {
			return observationDates;
		}

		public void setObservationDates(String observationDates) {
			this.observationDates = observationDates;
		}

		public boolean isDisplayRequiredSchedConstraints() {
			return displayRequiredSchedConstraints;
		}

		public void setDisplayRequiredSchedConstraints(
				boolean displayRequiredSchedConstraints) {
			this.displayRequiredSchedConstraints = displayRequiredSchedConstraints;
		}

		public boolean isDisplayPreferredSchedConstraints() {
			return displayPreferredSchedConstraints;
		}

		public void setDisplayPreferredSchedConstraints(
				boolean displayPreferredSchedConstraints) {
			this.displayPreferredSchedConstraints = displayPreferredSchedConstraints;
		}

		public boolean isDisplayPostProcessing() {
			return displayPostProcessing;
		}

		public void setDisplayPostProcessing(boolean displayPostProcessing) {
			this.displayPostProcessing = displayPostProcessing;
		}

		public boolean isAveraging() {
			return averaging;
		}

		public void setAveraging(boolean averaging) {
			this.averaging = averaging;
		}

		public boolean isFlagging() {
			return flagging;
		}

		public void setFlagging(boolean flagging) {
			this.flagging = flagging;
		}

		public boolean isCalibration() {
			return calibration;
		}

		public void setCalibration(boolean calibration) {
			this.calibration = calibration;
		}

		public boolean isImaging() {
			return imaging;
		}

		public void setImaging(boolean imaging) {
			this.imaging = imaging;
		}

		public String getFilterFrequency() {
			return filterFrequency;
		}

		public void setFilterFrequency(String filterFrequency) {
			this.filterFrequency = filterFrequency;
		}

		public String getFilterBandwidth() {
			return filterBandwidth;
		}

		public void setFilterBandwidth(String filterBandwidth) {
			this.filterBandwidth = filterBandwidth;
		}

		public boolean isFilterContiguousCoverage() {
			return filterContiguousCoverage;
		}

		public void setFilterContiguousCoverage(boolean filterContiguousCoverage) {
			this.filterContiguousCoverage = filterContiguousCoverage;
		}

		public String getCustomStationSpecifics() {
			return customStationSpecifics;
		}

		public void setCustomStationSpecifics(String customStationSpecifics) {
			this.customStationSpecifics = customStationSpecifics;
		}

		public String getAveragingFrequency() {
			return averagingFrequency;
		}

		public void setAveragingFrequency(String averagingFrequency) {
			this.averagingFrequency = averagingFrequency;
		}

		public String getAveragingTime() {
			return averagingTime;
		}

		public void setAveragingTime(String averagingTime) {
			this.averagingTime = averagingTime;
		}

		public String getFieldSizeX() {
			return fieldSizeX;
		}

		public void setFieldSizeX(String fieldSizeX) {
			this.fieldSizeX = fieldSizeX;
		}

		public String getPixelSizeX() {
			return pixelSizeX;
		}

		public void setPixelSizeX(String pixelSizeX) {
			this.pixelSizeX = pixelSizeX;
		}

		public String getFieldSizeY() {
			return fieldSizeY;
		}

		public void setFieldSizeY(String fieldSizeY) {
			this.fieldSizeY = fieldSizeY;
		}

		public String getPixelSizeY() {
			return pixelSizeY;
		}

		public void setPixelSizeY(String pixelSizeY) {
			this.pixelSizeY = pixelSizeY;
		}

		public boolean isDisplayStation() {
			return displayStation;
		}

		public void setDisplayStation(boolean displayStation) {
			this.displayStation = displayStation;
		}

		public List getStations() {
			return stations;
		}

		public void setStations(List stations) {
			this.stations = stations;
		}

		public String getSelectedStation() {
			return selectedStation;
		}

		public void setSelectedStation(String selectedStation) {
			this.selectedStation = selectedStation;
		}

		public boolean isRequiredStation() {
			return requiredStation;
		}

		public void setRequiredStation(boolean requiredStation) {
			this.requiredStation = requiredStation;
		}

		public boolean isDisplayClock() {
			return displayClock;
		}

		public void setDisplayClock(boolean displayClock) {
			this.displayClock = displayClock;
		}

		public List getClocks() {
			return clocks;
		}

		public void setClocks(List clocks) {
			this.clocks = clocks;
		}

		public String getSelectedClock() {
			return selectedClock;
		}

		public void setSelectedClock(String selectedClock) {
			this.selectedClock = selectedClock;
		}

		public boolean isRequiredClock() {
			return requiredClock;
		}

		public void setRequiredClock(boolean requiredClock) {
			this.requiredClock = requiredClock;
		}

		public boolean isDisplayOneFilter() {
			return displayOneFilter;
		}

		public void setDisplayOneFilter(boolean displayOneFilter) {
			this.displayOneFilter = displayOneFilter;
		}

		public List getOneFilters() {
			return oneFilters;
		}

		public void setOneFilters(List oneFilters) {
			this.oneFilters = oneFilters;
		}

		public String getSelectedOneFilter() {
			return selectedOneFilter;
		}

		public void setSelectedOneFilter(String selectedOneFilter) {
			this.selectedOneFilter = selectedOneFilter;
		}

		public boolean isRequiredOneFilter() {
			return requiredOneFilter;
		}

		public void setRequiredOneFilter(boolean requiredOneFilter) {
			this.requiredOneFilter = requiredOneFilter;
		}

		public boolean isDisplayAntenna() {
			return displayAntenna;
		}

		public void setDisplayAntenna(boolean displayAntenna) {
			this.displayAntenna = displayAntenna;
		}

		public List getAntennas() {
			return antennas;
		}

		public void setAntennas(List antennas) {
			this.antennas = antennas;
		}

		public String getSelectedAntenna() {
			return selectedAntenna;
		}

		public void setSelectedAntenna(String selectedAntenna) {
			this.selectedAntenna = selectedAntenna;
		}

		public boolean isRequiredAntenna() {
			return requiredAntenna;
		}

		public void setRequiredAntenna(boolean requiredAntenna) {
			this.requiredAntenna = requiredAntenna;
		}

		public boolean isDisplaySubarrayPointings() {
			return displaySubarrayPointings;
		}

		public void setDisplaySubarrayPointings(boolean displaySubarrayPointings) {
			this.displaySubarrayPointings = displaySubarrayPointings;
		}

		public boolean isDefaultSubarrayPointings() {
			return (subarrayPointings == null);
		}

		public void setDefaultSubarrayPointings(boolean defaultSubarrayPointings) {
			if (defaultSubarrayPointings) {
				this.subarrayPointings = null;
			} else if (!defaultSubarrayPointings && (this.subarrayPointings == null)) {
				this.subarrayPointings = "1";
			}
		}

		public String getSubarrayPointings() {
			return subarrayPointings;
		}

		public void setSubarrayPointings(String subarrayPointings) {
			this.subarrayPointings = subarrayPointings;
		}

		public boolean isRequiredSubarrayPointings() {
			return requiredSubarrayPointings;
		}

		public void setRequiredSubarrayPointings(boolean requiredSubarrayPointings) {
			this.requiredSubarrayPointings = requiredSubarrayPointings;
		}

		public boolean isDisplayIntegrationTime() {
			return displayIntegrationTime;
		}

		public void setDisplayIntegrationTime(boolean displayIntegrationTime) {
			this.displayIntegrationTime = displayIntegrationTime;
		}

		public String getIntegrationTime() {
			return integrationTime;
		}

		public void setIntegrationTime(String integrationTime) {
			this.integrationTime = integrationTime;
		}

		public boolean isRequiredIntegrationTime() {
			return requiredIntegrationTime;
		}

		public void setRequiredIntegrationTime(boolean requiredIntegrationTime) {
			this.requiredIntegrationTime = requiredIntegrationTime;
		}

		public boolean isDisplayInstrumentDetails() {
			return displayInstrumentDetails;
		}

		public void setDisplayInstrumentDetails(boolean displayInstrumentDetails) {
			this.displayInstrumentDetails = displayInstrumentDetails;
		}

		public String getInstrumentDetails() {
			return instrumentDetails;
		}

		public void setInstrumentDetails(String instrumentDetails) {
			this.instrumentDetails = instrumentDetails;
		}

		public boolean isCustomFilter() {
			return customFilter;
		}

		public void setCustomFilter(boolean customFilter) {
			this.customFilter = customFilter;
		}

		public boolean isDisplaySingleStationSpecifics() {
			return displaySingleStationSpecifics;
		}

		public void setDisplaySingleStationSpecifics(
				boolean displaySingleStationSpecifics) {
			this.displaySingleStationSpecifics = displaySingleStationSpecifics;
		}

		public String getSingleStationSpecifics() {
			return singleStationSpecifics;
		}

		public void setSingleStationSpecifics(String singleStationSpecifics) {
			this.singleStationSpecifics = singleStationSpecifics;
		}

		public List getPolarizations() {
			return polarizations;
		}

		public void setPolarizations(List polarizations) {
			this.polarizations = polarizations;
		}

		public boolean isDisplaySubFilter() {
			return displaySubFilter;
		}

		public void setDisplaySubFilter(boolean displaySubFilter) {
			this.displaySubFilter = displaySubFilter;
		}

		public String getEstimatedDataStorage() {
			return estimatedDataStorage;
		}

		public void setEstimatedDataStorage(String estimatedDataStorage) {
			this.estimatedDataStorage = estimatedDataStorage;
		}

		public String getDataStorageSpecifics() {
			return dataStorageSpecifics;
		}

		public void setDataStorageSpecifics(String dataStorageSpecifics) {
			this.dataStorageSpecifics = dataStorageSpecifics;
		}

		public String getDataCalculationType() {
			return dataCalculationType;
		}

		public void setDataCalculationType(String dataCalculationType) {
			this.dataCalculationType = dataCalculationType;
		}

		public String getRequiredSchedConstraints() {
			return requiredSchedConstraints;
		}

		public void setRequiredSchedConstraints(String requiredSchedConstraints) {
			this.requiredSchedConstraints = requiredSchedConstraints;
		}

		public String getPreferredSchedConstraints() {
			return preferredSchedConstraints;
		}

		public void setPreferredSchedConstraints(String preferredSchedConstraints) {
			this.preferredSchedConstraints = preferredSchedConstraints;
		}

		public List getSeeingOptions() {
			return seeingOptions;
		}
					
		public void setSeeingOptions(List seeingOptions) {
			this.seeingOptions = seeingOptions;
		}

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public String getTelescope() {
			return telescope;
		}

		public void setTelescope(String telescope) {
			this.telescope = telescope;
		}

		public boolean isDisplayPiggyBack() {
			return displayPiggyBack;
		}

		public void setDisplayPiggyBack(boolean displayPiggyBack) {
			this.displayPiggyBack = displayPiggyBack;
		}

		public boolean isEnablePiggyBack() {
			return enablePiggyBack;
		}

		public void setEnablePiggyBack(boolean enablePiggyBack) {
			this.enablePiggyBack = enablePiggyBack;
		}

		public String getPiggyBackSpecifics() {
			return piggyBackSpecifics;
		}

		public void setPiggyBackSpecifics(String piggyBackSpecifics) {
			this.piggyBackSpecifics = piggyBackSpecifics;
		}

		public boolean isDisplayDirectDataStorage() {
			return displayDirectDataStorage;
		}

		public void setDisplayDirectDataStorage(boolean displayDirectDataStorage) {
			this.displayDirectDataStorage = displayDirectDataStorage;
		}

		public boolean isDirectDataStorage() {
			return directDataStorage;
		}

		public void setDirectDataStorage(boolean directDataStorage) {
			this.directDataStorage = directDataStorage;
		}

		public boolean isDisplayPostAveraging() {
			return displayPostAveraging;
		}

		public void setDisplayPostAveraging(boolean displayPostAveraging) {
			this.displayPostAveraging = displayPostAveraging;
		}

		public boolean isDisplayPostImaging() {
			return displayPostImaging;
		}

		public void setDisplayPostImaging(boolean displayPostImaging) {
			this.displayPostImaging = displayPostImaging;
		}

		public boolean isDisplayPostBeamformed() {
			return displayPostBeamformed;
		}

		public void setDisplayPostBeamformed(boolean displayPostBeamformed) {
			this.displayPostBeamformed = displayPostBeamformed;
		}

		public boolean isDisplayPostTimeseries() {
			return displayPostTimeseries;
		}

		public void setDisplayPostTimeseries(boolean displayPostTimeseries) {
			this.displayPostTimeseries = displayPostTimeseries;
		}

		public String getFrequencyChannels() {
			return frequencyChannels;
		}

		public void setFrequencyChannels(String frequencyChannels) {
			this.frequencyChannels = frequencyChannels;
		}

		public String getBeams() {
			return beams;
		}

		public void setBeams(String beams) {
			this.beams = beams;
		}

		public String getSamplerate() {
			return samplerate;
		}

		public void setSamplerate(String samplerate) {
			this.samplerate = samplerate;
		}

		public String getEvents() {
			return events;
		}

		public void setEvents(String events) {
			this.events = events;
		}

		public boolean isDisplayBackupStrategy() {
			return displayBackupStrategy;
		}

		public void setDisplayBackupStrategy(boolean displayBackupStrategy) {
			this.displayBackupStrategy = displayBackupStrategy;
		}

		public String getBackupStrategy() {
			return backupStrategy;
		}

		public void setBackupStrategy(String backupStrategy) {
			this.backupStrategy = backupStrategy;
		}

		public boolean isDisplayObservationPhase() {
			return displayObservationPhase;
		}

		public void setDisplayObservationPhase(boolean displayObservationPhase) {
			this.displayObservationPhase = displayObservationPhase;
		}

		public List getObservationPhaseOptions() {
			return observationPhaseOptions;
		}

		public void setObservationPhaseOptions(List observationPhaseOptions) {
			this.observationPhaseOptions = observationPhaseOptions;
		}

		public String getSelectedObservationPhase() {
			return selectedObservationPhase;
		}

		public void setSelectedObservationPhase(String selectedObservationPhase) {
			this.selectedObservationPhase = selectedObservationPhase;
		}

		public boolean isDisplayObservationRequestedTime() {
			return displayObservationRequestedTime;
		}

		public void setDisplayObservationRequestedTime(
				boolean displayObservationRequestedTime) {
			this.displayObservationRequestedTime = displayObservationRequestedTime;
		}

		public String getObservationRequestedTime() {
			return observationRequestedTime;
		}

		public void setObservationRequestedTime(String observationRequestedTime) {
			this.observationRequestedTime = observationRequestedTime;
		}

		public String getObservationMinimumTime() {
			return observationMinimumTime;
		}

		public void setObservationMinimumTime(String observationMinimumTime) {
			this.observationMinimumTime = observationMinimumTime;
		}

		public boolean isDisplayCalibrationRequirements() {
			return displayCalibrationRequirements;
		}

		public void setDisplayCalibrationRequirements(
				boolean displayCalibrationRequirements) {
			this.displayCalibrationRequirements = displayCalibrationRequirements;
		}

		public String getCalibrationRequirements() {
			return calibrationRequirements;
		}

		public void setCalibrationRequirements(String calibrationRequirements) {
			this.calibrationRequirements = calibrationRequirements;
		}

		public boolean isDisplaySchedulingInfo() {
			return displaySchedulingInfo;
		}

		public void setDisplaySchedulingInfo(boolean displaySchedulingInfo) {
			this.displaySchedulingInfo = displaySchedulingInfo;
		}

		public List getTelescopeSchedulingOptions() {
			return telescopeSchedulingOptions;
		}

		public void setTelescopeSchedulingOptions(List telescopeSchedulingOptions) {
			this.telescopeSchedulingOptions = telescopeSchedulingOptions;
		}

		public boolean isDisplayTelescopeScheduling() {
			return displayTelescopeScheduling;
		}

		public void setDisplayTelescopeScheduling(boolean displayTelescopeScheduling) {
			this.displayTelescopeScheduling = displayTelescopeScheduling;
		}

		public String getSelectedTelescopeScheduling() {
			return selectedTelescopeScheduling;
		}

		public void setSelectedTelescopeScheduling(String selectedTelescopeScheduling) {
			this.selectedTelescopeScheduling = selectedTelescopeScheduling;
		}

		public String getTimeSuffix() {
			return timeSuffix;
		}

		public void setTimeSuffix(String timeSuffix) {
			this.timeSuffix = timeSuffix;
		}

		public List getFiltersw2() {
			return filtersw2;
		}

		public void setFiltersw2(List filtersw2) {
			this.filtersw2 = filtersw2;
		}

		public boolean isDisplayFiltersw2() {
			return displayFiltersw2;
		}

		public void setDisplayFiltersw2(boolean displayFiltersw2) {
			this.displayFiltersw2 = displayFiltersw2;
		}

		public String[] getSelectedFiltersw2() {
			return selectedFiltersw2;
		}

		public void setSelectedFiltersw2(String[] selectedFiltersw2) {
			this.selectedFiltersw2 = selectedFiltersw2;
		}

		public String getInstrumentUrl() {
			return instrumentUrl;
		}

		public void setInstrumentUrl(String instrumentUrl) {
			this.instrumentUrl = instrumentUrl;
		}

		public boolean isRequiredObservationRequestedTime() {
			return requiredObservationRequestedTime;
		}

		public void setRequiredObservationRequestedTime(
				boolean requiredObservationRequestedTime) {
			this.requiredObservationRequestedTime = requiredObservationRequestedTime;
		}

		public boolean isDisplaySlitw() {
			return displaySlitw;
		}

		public void setDisplaySlitw(boolean displaySlitw) {
			this.displaySlitw = displaySlitw;
		}

		public boolean isDisplayGrismw() {
			return displayGrismw;
		}

		public void setDisplayGrismw(boolean displayGrismw) {
			this.displayGrismw = displayGrismw;
		}

		public String[] getSelectedSlitw() {
			return selectedSlitw;
		}

		public void setSelectedSlitw(String[] selectedSlitw) {
			this.selectedSlitw = selectedSlitw;
		}

		public String[] getSelectedGrismw() {
			return selectedGrismw;
		}

		public void setSelectedGrismw(String[] selectedGrismw) {
			this.selectedGrismw = selectedGrismw;
		}

		public List getSlitw() {
			return slitw;
		}

		public void setSlitw(List slitw) {
			this.slitw = slitw;
		}

		public List getGrismw() {
			return grismw;
		}

		public void setGrismw(List grismw) {
			this.grismw = grismw;
		}

		public boolean isDisplayCustomFilter() {
			return displayCustomFilter;
		}

		public void setDisplayCustomFilter(boolean displayCustomFilter) {
			this.displayCustomFilter = displayCustomFilter;
		}

		public boolean isDisplayGratingw() {
			return displayGratingw;
		}

		public void setDisplayGratingw(boolean displayGratingw) {
			this.displayGratingw = displayGratingw;
		}

		public List getGratingw() {
			return gratingw;
		}

		public void setGratingw(List gratingw) {
			this.gratingw = gratingw;
		}

		public String[] getSelectedGratingw() {
			return selectedGratingw;
		}

		public void setSelectedGratingw(String[] selectedGratingw) {
			this.selectedGratingw = selectedGratingw;
		}

		public boolean isDisplayCCD() {
			return displayCCD;
		}

		public void setDisplayCCD(boolean displayCCD) {
			this.displayCCD = displayCCD;
		}

		public String getSelectedCCD() {
			return selectedCCD;
		}

		public void setSelectedCCD(String selectedCCD) {
			this.selectedCCD = selectedCCD;
		}

		public List getCCDs() {
			return CCDs;
		}

		public void setCCDs(List cCDs) {
			CCDs = cCDs;
		}

		public boolean isRequiredCCD() {
			return requiredCCD;
		}

		public void setRequiredCCD(boolean requiredCCD) {
			this.requiredCCD = requiredCCD;
		}

		public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
			ActionErrors errors = new ActionErrors();
			if(this.cohTaBeams != null && AstronValidator.isPositiveInt(this.cohTaBeams)) {
				errors.add("cohTaBeams",new ActionMessage("errors.required","cohTaBeams"));
			}

			return errors;
		}
		//public static ActionMessages validate(ActionMapping mapping,
		public static ActionMessages staticValidate(ActionMapping mapping,
				HttpServletRequest request) {
			ActionMessages errors = new ActionMessages();  
			Map params = request.getParameterMap();
			String value = null;
			value  = request.getParameter("averagingFrequency");
			if (!AstronValidator.isBlankOrNull(value) && !AstronValidator.isPositiveFloat(value)){
				errors.add("averagingFrequency", new ActionMessage("error.noint.positive") );
			}
			value  = request.getParameter("averagingTime");
			if (!AstronValidator.isBlankOrNull(value) && !AstronValidator.isPositiveFloat(value)){
				errors.add("averagingTime", new ActionMessage("error.noint.positive") );
			}
			return errors;
		}

		public boolean isCohstokes() {
			return cohstokes;
		}

		public void setCohstokes(boolean cohstokes) {
			this.cohstokes = cohstokes;
		}

		public boolean isIncohstokes() {
			return incohstokes;
		}

		public void setIncohstokes(boolean incohstokes) {
			this.incohstokes = incohstokes;
		}

		public boolean isComplexVoltage() {
			return complexVoltage;
		}

		public void setComplexVoltage(boolean complexVoltage) {
			this.complexVoltage = complexVoltage;
		}

		public boolean isFlysEye() {
			return flysEye;
		}

		public void setFlysEye(boolean flysEye) {
			this.flysEye = flysEye;
		}

		public boolean isRawVoltage() {
			return rawVoltage;
		}

		public void setRawVoltage(boolean rawVoltage) {
			this.rawVoltage = rawVoltage;
		}

		public String getChannels() {
			return channels;
		}

		public void setChannels(String channels) {
			this.channels = channels;
		}
		
		public String getIncchannels() {
			return incchannels;
		}

		public void setIncchannels(String incchannels) {
			this.incchannels = incchannels;
		}

		public String getIntsteps() {
			return intsteps;
		}

		public void setIntsteps(String intsteps) {
			this.intsteps = intsteps;
		}

		
		public String getIncohIntsteps() {
			return incohIntsteps;
		}

		public void setIncohIntsteps(String incohIntsteps) {
			this.incohIntsteps = incohIntsteps;
		}

		public List getIncPolarizations() {
			return incPolarizations;
		}

		public void setIncPolarizations(List incPolarizations) {
			this.incPolarizations = incPolarizations;
		}

		public String getSelectedPolarizations() {
			return selectedPolarizations;
		}

		public void setSelectedPolarizations(String selectedPolarizations) {
			this.selectedPolarizations = selectedPolarizations;
		}

		public String getSelectedIncPolarizations() {
			return selectedIncPolarizations;
		}

		public void setSelectedIncPolarizations(String selectedIncPolarizations) {
			this.selectedIncPolarizations = selectedIncPolarizations;
		}

		public List getChannellist() {
			return channellist;
		}

		public void setChannellist(List channellist) {
			this.channellist = channellist;
		}

		public boolean isDisplayCalculatedSize() {
			return displayCalculatedSize;
		}

		public void setDisplayCalculatedSize(boolean displayCalculatedSize) {
			this.displayCalculatedSize = displayCalculatedSize;
		}

		public String getEstimatedCepLoad() {
			return estimatedCepLoad;
		}

		public void setEstimatedCepLoad(String estimatedCepLoad) {
			this.estimatedCepLoad = estimatedCepLoad;
		}

		public boolean isSummaryPlots() {
			return summaryPlots;
		}

		public void setSummaryPlots(boolean summaryPlots) {
			this.summaryPlots = summaryPlots;
		}

		public final boolean isDisplayStoreRawData() {
			return displayStoreRawData;
		}

		public final void setDisplayStoreRawData(boolean displayStoreRawData) {
			this.displayStoreRawData = displayStoreRawData;
		}

		public final boolean isStoreRawData() {
			return storeRawData;
		}

		public final void setStoreRawData(boolean storeRawData) {
			this.storeRawData = storeRawData;
		}

		public boolean isDisplayPostInterfero() {
			return displayPostInterfero;
		}

		public void setDisplayPostInterfero(boolean displayPostInterfero) {
			this.displayPostInterfero = displayPostInterfero;
		}

		public boolean isDisplayObservationMinimumTime() {
			return displayObservationMinimumTime;
		}

		public void setDisplayObservationMinimumTime(
				boolean displayObservationMinimumTime) {
			this.displayObservationMinimumTime = displayObservationMinimumTime;
		}

		public boolean isDisplayPipelineOptions() {
			return displayPipelineOptions;
		}

		public void setDisplayPipelineOptions(boolean displayPipelineOptions) {
			this.displayPipelineOptions = displayPipelineOptions;
		}

		public boolean isDisplayProcessingMode() {
			return displayProcessingMode;
		}

		public void setDisplayProcessingMode(boolean displayProcessingMode) {
			this.displayProcessingMode = displayProcessingMode;
		}

		public boolean isDisplaySubbandsPerImage() {
			return displaySubbandsPerImage;
		}

		public void setDisplaySubbandsPerImage(boolean displaySubbandsPerImage) {
			this.displaySubbandsPerImage = displaySubbandsPerImage;
		}

		public boolean isDisplayFieldOfView() {
			return displayFieldOfView;
		}

		public void setDisplayFieldOfView(boolean displayFieldOfView) {
			this.displayFieldOfView = displayFieldOfView;
		}

		public boolean isDisplayFlaggingStrategy() {
			return displayFlaggingStrategy;
		}

		public void setDisplayFlaggingStrategy(boolean displayFlaggingStrategy) {
			this.displayFlaggingStrategy = displayFlaggingStrategy;
		}

		public boolean isDisplayPostDemixing() {
			return displayPostDemixing;
		}

		public void setDisplayPostDemixing(boolean displayPostDemixing) {
			this.displayPostDemixing = displayPostDemixing;
		}

		public String getSelectedProcessingMode() {
			return selectedProcessingMode;
		}

		public void setSelectedProcessingMode(String selectedProcessingMode) {
			this.selectedProcessingMode = selectedProcessingMode;
		}

		public String getSubbandsPerImage() {
			return subbandsPerImage;
		}

		public void setSubbandsPerImage(String subbandsPerImage) {
			this.subbandsPerImage = subbandsPerImage;
		}

		public String getFieldOfView() {
			return fieldOfView;
		}

		public void setFieldOfView(String fieldOfView) {
			this.fieldOfView = fieldOfView;
		}

		public String getSelectedFlaggingStrategy() {
			return selectedFlaggingStrategy;
		}

		public void setSelectedFlaggingStrategy(String selectedFlaggingStartegy) {
			this.selectedFlaggingStrategy = selectedFlaggingStartegy;
		}

		public boolean isDemixing() {
			return demixing;
		}

		public void setDemixing(boolean demixing) {
			this.demixing = demixing;
		}

		public String getDemixingTime() {
			return demixingTime;
		}

		public void setDemixingTime(String demixingTime) {
			this.demixingTime = demixingTime;
		}

		public String getDemixingFrequency() {
			return demixingFrequency;
		}

		public void setDemixingFrequency(String demixingFrequency) {
			this.demixingFrequency = demixingFrequency;
		}

		public List getFlaggingStrategies() {
			return flaggingStrategies;
		}

		public void setFlaggingStrategies(List flaggingStrategies) {
			this.flaggingStrategies = flaggingStrategies;
		}

		public List getProcessingModes() {
			return processingModes;
		}

		public void setProcessingModes(List processingModes) {
			this.processingModes = processingModes;
		}

		public boolean isDisplayDemixingSource() {
			return displayDemixingSource;
		}

		public void setDisplayDemixingSource(boolean displayDemixingSource) {
			this.displayDemixingSource = displayDemixingSource;
		}

		public List getDemixingSources() {
			return demixingSources;
		}

		public void setDemixingSources(List demixingSources) {
			this.demixingSources = demixingSources;
		}

		public String[] getSelectedDemixingSources() {
			return selectedDemixingSources;
		}

		public void setSelectedDemixingSources(String[] selectedDemixingSources) {
			this.selectedDemixingSources = selectedDemixingSources;
		}

		public boolean isDisplayCorrelatedVisibilities() {
			return displayCorrelatedVisibilities;
		}

		public void setDisplayCorrelatedVisibilities(
				boolean displayCorrelatedVisibilities) {
			this.displayCorrelatedVisibilities = displayCorrelatedVisibilities;
		}

		public boolean isCorrelatedVisibilities() {
			return correlatedVisibilities;
		}

		public void setCorrelatedVisibilities(boolean correlatedVisibilities) {
			this.correlatedVisibilities = correlatedVisibilities;
		}

		public boolean isDisplayImagingParameters() {
			return displayImagingParameters;
		}

		public void setDisplayImagingParameters(boolean displayImagingParameters) {
			this.displayImagingParameters = displayImagingParameters;
		}

		public String getCohTaBeams() {
			return cohTaBeams;
		}

		public void setCohTaBeams(String cohTaBeams) {
			this.cohTaBeams = cohTaBeams;
		}

		public String getIncohTaBeams() {
			new Exception().printStackTrace();
			return incohTaBeams;
		}

		public void setIncohTaBeams(String incohTaBeams) {
			this.incohTaBeams = incohTaBeams;
		}

		public String getRings() {
			return rings;
		}

		public void setRings(String rings) {
			this.rings = rings;
		}

		public boolean isDisplayStoreUVData() {
			return displayStoreUVData;
		}

		public void setDisplayStoreUVData(boolean displayStoreUVData) {
			this.displayStoreUVData = displayStoreUVData;
		}

		public boolean isStoreUVData() {
			return storeUVData;
		}

		public void setStoreUVData(boolean storeUVData) {
			this.storeUVData = storeUVData;
		}

		public String getTbbRequestedMinimumTime() {
			return tbbRequestedMinimumTime;
		}

		public void setTbbRequestedMinimumTime(String tbbRequestedMinimumTime) {
			this.tbbRequestedMinimumTime = tbbRequestedMinimumTime;
		}

		public String getTbbRequestedSATime() {
			return tbbRequestedSATime;
		}

		public void setTbbRequestedSATime(String tbbRequestedSATime) {
			this.tbbRequestedSATime = tbbRequestedSATime;
		}

		public String getTbbTriggerLength() {
			return tbbTriggerLength;
		}

		public void setTbbTriggerLength(String tbbTriggerLength) {
			this.tbbTriggerLength = tbbTriggerLength;
		}

		public String getTbbTriggerRate() {
			return tbbTriggerRate;
		}

		public void setTbbTriggerRate(String tbbTriggerRate) {
			this.tbbTriggerRate = tbbTriggerRate;
		}

		public List getTbbTriggerSource() {
			return tbbTriggerSource;
		}

		public void setTbbTriggerSource(List tbbTriggerSource) {
			this.tbbTriggerSource = tbbTriggerSource;
		}

		public String getTbbSelectedObservationId() {
			return tbbSelectedObservationId;
		}

		public void setTbbSelectedObservationId(String tbbSelectedObservationId) {
			this.tbbSelectedObservationId = tbbSelectedObservationId;
		}

		public List getTbbObservations() {
			return tbbObservations;
		}

		public void setTbbObservations(List tbbObservations) {
			this.tbbObservations = tbbObservations;
		}

		public String getTbbTriggerSourceId() {
			return tbbTriggerSourceId;
		}

		public void setTbbTriggerSourceId(String tbbTriggerSourceId) {
			this.tbbTriggerSourceId = tbbTriggerSourceId;
		}

		public boolean isDisplayPiggyBackFeatures() {
			return displayPiggyBackFeatures;
		}

		public void setDisplayPiggyBackFeatures(boolean displayPiggyBackFeatures) {
			this.displayPiggyBackFeatures = displayPiggyBackFeatures;
		}

		public List getDeSelectedInstConf() {
			return deSelectedInstConf;
		}

		public void setDeSelectedInstConf(List deSelectedInstConf) {
			this.deSelectedInstConf = deSelectedInstConf;
		}

		public boolean isBeamConf() {
			return beamConf;
		}


		public boolean isInterferoConf() {
			return interferoConf;
		}


		public boolean isTbbConf() {
			return tbbConf;
		}


		public boolean isOtherConf() {
			return otherConf;
		}


		public void setBeamConf(boolean beamConf) {
			this.beamConf = beamConf;
		}


		public void setInterferoConf(boolean interferoConf) {
			this.interferoConf = interferoConf;
		}


		public void setTbbConf(boolean tbbConf) {
			this.tbbConf = tbbConf;
		}


		public void setOtherConf(boolean otherConf) {
			this.otherConf = otherConf;
		}

		public List getSelectedInstrumentConfigurations() {
			return selectedInstrumentConfigurations;
		}

		public void setSelectedInstrumentConfigurations(
				List selectedInstrumentConfigurations) {
			this.selectedInstrumentConfigurations = selectedInstrumentConfigurations;
		}

		public boolean isEnableCombinedOutputProductSelection() {
			return enableCombinedOutputProductSelection;
		}

		public void setEnableCombinedOutputProductSelection(
				boolean enableCombinedOutputProductSelection) {
			this.enableCombinedOutputProductSelection = enableCombinedOutputProductSelection;
		}

		public boolean isSkipRFI() {
			return skipRFI;
		}

		public void setSkipRFI(boolean skipRFI) {
			this.skipRFI = skipRFI;
		}

		public boolean isSkipFolding() {
			return skipFolding;
		}

		public void setSkipFolding(boolean skipFolding) {
			this.skipFolding = skipFolding;
		}

		public boolean isSkipPdmp() {
			return skipPdmp;
		}

		public void setSkipPdmp(boolean skipPdmp) {
			this.skipPdmp = skipPdmp;
		}

		public boolean isSkipDspsr() {
			return skipDspsr;
		}

		public void setSkipDspsr(boolean skipDspsr) {
			this.skipDspsr = skipDspsr;
		}

		public boolean isSkipPrepfold() {
			return skipPrepfold;
		}

		public void setSkipPrepfold(boolean skipPrepfold) {
			this.skipPrepfold = skipPrepfold;
		}

		public boolean isSinglePulse() {
			return singlePulse;
		}

		public void setSinglePulse(boolean singlePulse) {
			this.singlePulse = singlePulse;
		}

		public boolean isRratsAnalysis() {
			return rratsAnalysis;
		}

		public void setRratsAnalysis(boolean rratsAnalysis) {
			this.rratsAnalysis = rratsAnalysis;
		}

		public boolean isSkipDynamicAverage() {
			return skipDynamicAverage;
		}

		public void setSkipDynamicAverage(boolean skipDynamicAverage) {
			this.skipDynamicAverage = skipDynamicAverage;
		}

		public String getSubintegrationLength() {
			return subintegrationLength;
		}

		public void setSubintegrationLength(String subintegrationLength) {
			this.subintegrationLength = subintegrationLength;
		}

		public boolean isConvertRawData() {
			return convertRawData;
		}

		public void setConvertRawData(boolean convertRawData) {
			this.convertRawData = convertRawData;
		}

		public String getThreshold() {
			return threshold;
		}

		public void setThreshold(String threshold) {
			this.threshold = threshold;
		}

		public String getSigmaLimit() {
			return sigmaLimit;
		}

		public void setSigmaLimit(String sigmaLimit) {
			this.sigmaLimit = sigmaLimit;
		}

		public String getNumberOfBlocks() {
			return numberOfBlocks;
		}

		public void setNumberOfBlocks(String numberOfBlocks) {
			this.numberOfBlocks = numberOfBlocks;
		}

		public String getPrepfoldOptions() {
			return prepfoldOptions;
		}

		public void setPrepfoldOptions(String prepfoldOptions) {
			this.prepfoldOptions = prepfoldOptions;
		}

		public String getPresubbandOptions() {
			return presubbandOptions;
		}

		public void setPresubbandOptions(String presubbandOptions) {
			this.presubbandOptions = presubbandOptions;
		}

		public String getRfifind() {
			return rfifind;
		}

		public void setRfifind(String rfifind) {
			this.rfifind = rfifind;
		}

		public String getDspsrOptions() {
			return dspsrOptions;
		}

		public void setDspsrOptions(String dspsrOptions) {
			this.dspsrOptions = dspsrOptions;
		}

		public String getDigifilOptions() {
			return digifilOptions;
		}

		public void setDigifilOptions(String digifilOptions) {
			this.digifilOptions = digifilOptions;
		}

		public String getPredataOptions() {
			return predataOptions;
		}

		public void setPredataOptions(String predataOptions) {
			this.predataOptions = predataOptions;
		}

		public String getBf2fitsOptions() {
			return bf2fitsOptions;
		}

		public void setBf2fitsOptions(String bf2fitsOptions) {
			this.bf2fitsOptions = bf2fitsOptions;
		}

		public String getPulsarDesc() {
			return pulsarDesc;
		}

		public void setPulsarDesc(String pulsarDesc) {
			this.pulsarDesc = pulsarDesc;
		}

		public String getTotalSum() {
			return totalSum;
		}

		public void setTotalSum(String totalSum) {
			this.totalSum = totalSum;
		}
		
	}

