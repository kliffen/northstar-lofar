// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessProposalAction.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: ProcessProposalListAction.java,v 1.7 2008-06-17 07:43:01 boelen Exp $
 *
 */
package eu.radionet.northstar.control.admin.list;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.zip.ZipOutputStream;
import java.util.zip.ZipEntry;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Cookie;
import javax.xml.parsers.ParserConfigurationException;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import com.lowagie.text.BadElementException;
import com.lowagie.text.DocumentException;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.Converter;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.util.PdfUtils;
import eu.radionet.northstar.business.util.XMLConverter;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.WebDAVFileRepository;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.UserTelescope;

/**
 * The ProcessProposalAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessProposalListAction extends Action {
	protected Log log = LogFactory.getLog(this.getClass());
	static private final int MAX_AGE_IN_SECONDS = 600;
	static private final int YEAR_IN_SECONDS = 31556926;
	static private final String BUNDLE_FILE_NAME = "proposals.zip";
	
	private NorthStarDelegate northStarDelegate = null;

	private CollaborationDelegate collaborationDelegate = null;
	
	private AdminProposalListForm proposalListForm;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		proposalListForm = (AdminProposalListForm) form;
		HttpSession session = request.getSession();

		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);

		/*
		 * get instance of ProposalDelegate
		 */
		northStarDelegate = NorthStarDelegate.getInstance();

		// store cookie
		if(proposalListForm.getSelectedSemesters() != null){
			response = storeCookies(response);
		}
		/*
		 * if delete, edit, copy, delete , submit button pressed, retrieve
		 * selected proposal
		 */
		if (!AstronValidator.isBlankOrNull(proposalListForm.getFilterButton())) {
			SelectionBean selectionBean = new SelectionBean();
			selectionBean.setSelectedTelescopes(proposalListForm.getSelectedTelescopes());
			selectionBean.setSelectedCategories(proposalListForm.getSelectedCategories());
			selectionBean.setSelectedStatusses(proposalListForm.getSelectedStatusses());
			selectionBean.setSelectedSemesters(proposalListForm.getSelectedSemesters());	
			selectionBean.setSelectedCommunities(proposalListForm.getSelectedCommunities());	
			selectionBean.setSelectedYear(proposalListForm.getSelectedYear());	
			selectionBean.setForceProjectId(proposalListForm.isForceProjectId());
			session.setAttribute(Constants.ADMIN_PROPOSAL_SELECTION,
					selectionBean);
			return mapping.findForward("refresh");
		}
		
		if (proposalListForm.getListButton() != null
				|| proposalListForm.getXmlButton() != null || proposalListForm.getAllButton() != null
				|| proposalListForm.getAbstractButton() != null || proposalListForm.getViewStatisticsButton() != null) {
			List idsList = new ArrayList();
			Iterator telescopeBeanIterator = proposalListForm.getAdminTelescopeBeans().iterator();
			while (telescopeBeanIterator.hasNext()){
				AdminTelescopeBean telescopeBean = (AdminTelescopeBean) telescopeBeanIterator.next();
				Iterator proposalIterator = telescopeBean.getAdminProposalBeans().iterator();
				while (proposalIterator.hasNext()){
					AdminProposalBean proposalBean = (AdminProposalBean) proposalIterator.next();
					idsList.add(proposalBean.getId());
				}
			}
			String[] ids = OptionsUtils.getArray(idsList);

			List proposals = northStarDelegate.getProposalsByIds(ids);
			if (proposalListForm.getListButton() != null) {
				downloadList(proposals, response);
				return null;
			} else if (proposalListForm.getXmlButton() != null) {
				downloadXML(proposals, response);
				return null;
			}
			if (proposalListForm.getAllButton() != null){
				downloadAll(proposals, request, response);
				return null;
			}
			if (proposalListForm.getAbstractButton() != null){
				downloadAbstracts(proposals, request, response);
				return null;
			}
			if (proposalListForm.getViewStatisticsButton() != null){
				ActionForward forward = mapping.findForward("viewStatistics");
				//String proposalIds ="";
				List<Integer> proposalIds = new ArrayList();
				
				Iterator proposalit = proposals.iterator();
				
				
				while (proposalit.hasNext()){
					Proposal proposal = (Proposal) proposalit.next();
					//proposalIds += proposal.getId().toString()+" ";
					proposalIds.add(proposal.getId());
					
				}
				//forward = ParameterAppender.append(forward, "proposalIds",proposalIds);
				//request.
				//response.
				session.setAttribute("proposalIds", proposalIds);
				return forward;
			}
		}
		
		Iterator telescopeBeanIterator = proposalListForm.getAdminTelescopeBeans().iterator();
		while (telescopeBeanIterator.hasNext()){
			AdminTelescopeBean telescopeBean = (AdminTelescopeBean) telescopeBeanIterator.next();
		if (telescopeBean.getChangeStatusButton(0) != null
				|| telescopeBean.getViewProposalButton(0) != null
				|| telescopeBean.getViewStatusHistoryButton(0) != null) {

			Integer proposalId = getProposalId(telescopeBean
					.getAdminProposalBeans(), telescopeBean.getIndex());
			if (telescopeBean.getChangeStatusButton(0) != null) {
				collaborationDelegate = CollaborationDelegate.getInstance();
				ActionForward forward = mapping.findForward("changeStatus");
				/*
				 * checks if another session locks the proposal, if not it locks
				 * automaticly the proposal
				 */
				if (collaborationDelegate.proposalIsLockedByOthers(proposalId,
						ownUserAccount, session.getId())) {

					String forwardPath = forward.getPath();

					request.setAttribute(Constants.FORWARD, forwardPath);
					return ParameterAppender.append(mapping
							.findForward("unlockProposal"), "proposalId",
							AstronConverter.toString(proposalId));
				} else {
					collaborationDelegate.lockProposal(proposalId,
							ownUserAccount, session.getId());

				}
				return ParameterAppender.append(forward, "proposalId",
						proposalId.toString());

			} else if (telescopeBean.getViewProposalButton(0) != null) {
				return ParameterAppender.append(mapping
						.findForward("viewProposal"), "proposalId", proposalId
						.toString());
			} else if (telescopeBean.getViewStatusHistoryButton(0) != null) {
				return ParameterAppender.append(mapping
						.findForward("viewStatusHistory"), "proposalId",
						proposalId.toString());
			}

		} 
		}
		SelectionBean selectionBean = new SelectionBean();
		selectionBean.setSelectedYear(proposalListForm.getSelectedYear());	
		selectionBean.setSelectedCategories(proposalListForm.getSelectedCategories());
		selectionBean.setSelectedStatusses(proposalListForm.getSelectedStatusses());
		session.setAttribute(Constants.ADMIN_PROPOSAL_SELECTION,
				selectionBean);
		return mapping.findForward("refresh");
	}

	

	/**
	 * The getProposalId method retrieves proposalId with index
	 * 
	 * @param proposalBeans
	 * @param index
	 * @return
	 */
	private Integer getProposalId(List proposalBeans, int index) {
		AdminProposalBean proposalBean = (AdminProposalBean) proposalBeans.get(index);

		return new Integer(proposalBean.getId());
	}

	private String getString(String string, int width) {
		String result = null;
		if (string != null && string.length() > width) {
			result = string.substring(0, width - 3);
			result += "...";
			return result;
		} else if (string == null || string.length() < width) {
			if (string == null) {
				result = " ";
			} else {
				result = string + " ";
			}
			while (result.length() < width) {
				result += " ";
			}
			return result;
		} else {
			return string;
		}

	}

	protected void downloadList(List proposals, HttpServletResponse response)
			throws DatabaseException, IOException {

		Iterator iterator = proposals.iterator();
		String fileName = "proposals.txt";
		response.setHeader("Content-disposition", "attachment; filename="
				+ fileName);
		response.setHeader("Cache-Control", "max-age=" + MAX_AGE_IN_SECONDS);
		BufferedWriter br = new BufferedWriter(new OutputStreamWriter(response
				.getOutputStream()));

		final String DELIMITER = " ";
		final int PROJECT_CODE_WIDTH = 12;
		final int PROJECT_TITLE_WIDTH = 70;
		final int PI_WIDTH = 30;
		final int CO_I_WIDTH = 30;
		String code = getString("Projectcode", PROJECT_CODE_WIDTH);
		String title = getString("Project Title", PROJECT_TITLE_WIDTH);
		String pi = getString("PI", PI_WIDTH);
		String coI = getString("CoI's", CO_I_WIDTH);
		br.write(code + DELIMITER + title + DELIMITER + pi + DELIMITER + coI
				+ "\n");
		while (iterator.hasNext()) {
			Proposal proposal = (Proposal) iterator.next();
			code = getString(proposal.getCode(), PROJECT_CODE_WIDTH);
			title = getString(null, PROJECT_TITLE_WIDTH);
			if (proposal.getJustification() != null) {
				title = Converter.removeReturnsAndTabs(proposal
						.getJustification().getTitle());
				title = getString(title, PROJECT_TITLE_WIDTH);
			}
			pi = NorthStarDelegate.getName(northStarDelegate
					.getContactAuthorAsUser(proposal));
			pi = getString(pi, PI_WIDTH);
			List coIs = new ArrayList();
			Iterator membersIterator = proposal.getMembers().iterator();
			while (membersIterator.hasNext()) {
				Member member = (Member) membersIterator.next();
				if (!member.isPi()) {
					coIs.add(member.getName());
				}
			}
			String line = code + DELIMITER + title + DELIMITER + pi + DELIMITER;
			Iterator coIsIterator = coIs.iterator();
			while (coIsIterator.hasNext()) {
				line += getString((String) coIsIterator.next(), CO_I_WIDTH);
				if (coIsIterator.hasNext()) {
					line += DELIMITER;
				}
			}
			br.write(line + "\n");

		}
		br.flush();
	}

	protected void downloadXML(List proposals, HttpServletResponse response)
			throws DatabaseException, IOException,
			InvalidConfigurationException, ParserConfigurationException {
		String fileName = "proposals.xml";
		response.setContentType("application/xml");
		response.setHeader("Content-disposition", "attachment; filename="
				+ fileName);
		response.setHeader("Cache-Control", "max-age=" + MAX_AGE_IN_SECONDS);
		XMLConverter xmlConverter = XMLConverter.getInstance();
		xmlConverter
				.convertProposals2XML(proposals, response.getOutputStream());
		response.getOutputStream().flush();
	}
	
	private void downloadAbstracts(List proposals, HttpServletRequest request,
			HttpServletResponse response) 
			throws DatabaseException, IOException, BadElementException, InvalidConfigurationException, DocumentException{
		PdfUtils pdfUtils = new PdfUtils();
		
		String fileName = "abstracts.pdf";
		response.setContentType("application/pdf");
		response.setHeader("Content-disposition", "attachment; filename="
				+ fileName);
		response.setHeader("Cache-Control", "max-age=" + MAX_AGE_IN_SECONDS);
		
		response.getOutputStream().write(pdfUtils.getAbstractsPdf(proposals, request));
		
		response.getOutputStream().flush();
		
	}
	
	protected void downloadAll(List proposals,HttpServletRequest request, HttpServletResponse response)
	throws Exception {
		PdfUtils pdfUtils = new PdfUtils();
		// initialize the output
		response.setContentType("application/zip");
		response.setHeader("Content-disposition", "attachment; filename="+ BUNDLE_FILE_NAME);
		response.setHeader("Cache-Control", "max-age=" + MAX_AGE_IN_SECONDS);
		ZipOutputStream out = new ZipOutputStream(response.getOutputStream() );
		
		WebDAVFileRepository fileRepository = new WebDAVFileRepository();
		Iterator proposalIterator = proposals.iterator();
		
		
		while (proposalIterator.hasNext()){
			Proposal proposal = (Proposal) proposalIterator.next();
			// here the proposal pdf's are generated, 
			
			try{
				byte[] content = null;
				if(proposal!= null && proposal.getCode() == null){
				//if(proposal!= null){
					//log.info("Proposal.getCode(): "+proposal.getCode());
					content = pdfUtils.getProposalPdf(proposal, request);
					out.putNextEntry(new ZipEntry(proposal.getId()+".pdf" ));
				}else{
					content = fileRepository.getProposalFile(proposal);
					out.putNextEntry(new ZipEntry(proposal.getCode()+".pdf" ));
				}
				// the byte array stream is directly put in the zip file.
				out.write(content);
	            out.closeEntry();
	           
			}catch(Exception e){
				if(proposal != null){
					log.warn("Could not add proposal: "+proposal.getCode());
				}else{
					log.warn("Not a valid proposal");
				}
			}
 		}
		out.finish();
		// send the output
		response.flushBuffer();
		
	}
	
	
	private HttpServletResponse storeCookies(HttpServletResponse response){
		// selected telescopes
		String[] SelectedTelescopes = proposalListForm.getSelectedTelescopes();
		if (SelectedTelescopes != null && SelectedTelescopes.length > 0){
			String telescopes= new String();
			for(int i=0; i< proposalListForm.getSelectedTelescopes().length;i++){
				telescopes += proposalListForm.getSelectedTelescopes()[i]+"##";
			}
			Cookie telescopeCookie = new Cookie("telescopes",telescopes);
			telescopeCookie.setMaxAge(YEAR_IN_SECONDS); // one year
			response.addCookie(telescopeCookie);
		}
		// selected categories
		String categories = new String();
		for(int i=0; i< proposalListForm.getSelectedCategories().length;i++){
			categories += proposalListForm.getSelectedCategories()[i]+"##";
		}
		Cookie categoryCookie = new Cookie("categories",categories);
		categoryCookie.setMaxAge(YEAR_IN_SECONDS); // one year
		response.addCookie(categoryCookie);
		// selected statuses
		String statuses = new String();
		for(int i=0; i< proposalListForm.getSelectedStatusses().length;i++){
			statuses += proposalListForm.getSelectedStatusses()[i]+"##";
		}
		Cookie statusesCookie = new Cookie("statusses",statuses);
		statusesCookie.setMaxAge(YEAR_IN_SECONDS); // one year
		response.addCookie(statusesCookie);
		// selected semesters
		String semesters = new String();
		for(int i=0; i< proposalListForm.getSelectedSemesters().length;i++){
			semesters += proposalListForm.getSelectedSemesters()[i]+"##";
		}
		Cookie semestersCookie = new Cookie("semesters",semesters);
		semestersCookie.setMaxAge(YEAR_IN_SECONDS); // one year
		response.addCookie(semestersCookie);
		// selected communities
		String[] lselectedCommunities = proposalListForm.getSelectedCommunities();
		if (lselectedCommunities != null && lselectedCommunities.length > 0){
			String communities = new String();
			for(int i=0; i< lselectedCommunities.length ;i++){
				communities += lselectedCommunities[i]+"##";
			}
			Cookie communitieCookie = new Cookie("communities",communities);
			communitieCookie.setMaxAge(YEAR_IN_SECONDS); // one year
			response.addCookie(communitieCookie);
		}
		// selected year
		Cookie yearCookie = new Cookie("selectedYear",proposalListForm.getSelectedYear());
		yearCookie.setMaxAge(YEAR_IN_SECONDS); // one year
		response.addCookie(yearCookie);
		
		return response;

	}
	
	
}