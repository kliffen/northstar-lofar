<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<TABLE width="100%">

	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD><html:link action="/setUpAdminOverview.do"><astron:label key="label.admin.index.proposallocks" /></html:link></TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD><html:link action="/setUpProposalList.do"><astron:label key="label.admin.index.proposals" /></html:link></TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD><html:link action="/setUpSemesterList.do"><astron:label key="label.admin.index.semesters" /></html:link></TD>
	</TR>		
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD><html:link action="/setUpTelescopeAccess.do"><astron:label key="label.admin.index.telescopeaccess" /></html:link></TD>
	</TR>		
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD><html:link action="/setUpUploadConfig.do"><astron:label key="label.admin.index.upload" /></html:link></TD>
	</TR>		
	<TR>
		<TD>&nbsp;</TD>
	</TR>		
	<TR>
		<TD><html:link action="/processAcceptedInvitations.do"><astron:label key="label.admin.index.processinvitations" /></html:link></TD>
	</TR>		
	<TR>
		<TD>&nbsp;</TD>
	</TR>	
	<TR>
		<TD><html:link action="/processReloadNorthStarConfigAction.do"><astron:label key="label.admin.index.reload" /></html:link></TD>
	</TR>		
	<TR>
		<TD>&nbsp;</TD>
	</TR>		

</TABLE>
