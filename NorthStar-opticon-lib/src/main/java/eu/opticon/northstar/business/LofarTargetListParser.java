package eu.opticon.northstar.business;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Vector;

import au.com.bytecode.opencsv.CSVReader;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.AstroCoordinate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.exception.NorthStarException;
import eu.radionet.northstar.business.exception.ParseTargetListException;
import eu.radionet.northstar.business.targetlist.TargetFileBean;
import eu.radionet.northstar.business.targetlist.TargetListParser;
import eu.radionet.northstar.control.proposal.observingrequest.ValueBean;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ResourceType;
import eu.radionet.northstar.data.entities.Target;

public class LofarTargetListParser extends TargetListParser {

	public LofarTargetListParser(ProposalDelegate proposalDelegate) {
		super(proposalDelegate);
		// TODO Auto-generated constructor stub
	}


	protected static final String EXPOSURE_TIME = "exposure_time";
	protected static final String FLUX = "flux";
	protected static final String FLUX_FREQUENCY = "flux_frequency";
	protected static final String SPECTRAL_INDEX = "spectral_index";
	protected static final String COMMENTS = "comments";

	private String[] optionalHeaders = new String[] {EXPOSURE_TIME, FLUX, FLUX_FREQUENCY, SPECTRAL_INDEX, COMMENTS  };
	protected NorthStarDelegate northStarDelegate = null;

	
	public List parseTargetListFile(InputStream targetListFile, List list, int maxTargetonSameRun)
	throws IOException, ParseTargetListException, DatabaseException {
		lineNumber = 1;
		List targets = new ArrayList();
		BufferedReader br = null;
		try {
		br = new BufferedReader(new InputStreamReader(
				targetListFile));
		// It seems that the CSVReader gives an error when an additional " is found, which does not throw an error
		// This might fix the problem
		CSVReader reader = new CSVReader(br,',');
		
	    String [] pieces;
	    while ((pieces = reader.readNext()) != null) {
	        // nextLine[] is an array of values from the line
	        //System.out.println(nextLine[0] + nextLine[1] + "etc...");
	    	OpticonTarget target = createTarget(pieces);
			targets.add(target);
			lineNumber++;
	    }
	    /*boolean oldFormat = false;
		lineNumber = 1;
		
		String line = br.readLine();
		String splitword = "[ \\t\\x0B]+";
		if (line.matches(".*;.*")) {
			splitword = ";";
			oldFormat = true;
		}
		// spaces and tabs
		String[] header = null;
		
		 // parse al the lines
		 
		while (line != null && line.length() > 0) {
			String[] pieces = line.split(splitword);
			OpticonTarget target = null;
			
			 // if there is an valid header
			 
			if (lineNumber == 1 && oldFormat && isHeader(pieces)) {
				header = pieces;
			} else {
				target = createTarget(pieces);
				targets.add(target);
				//targets.add(createObservation(targetFileBean));
			}
			line = br.readLine();
			lineNumber++;
		}
	*/
	    /**
	     * Verification of maximum number of subbands per run
	     */
	  targets= validateTargets(targets,maxTargetonSameRun);
		} finally {
			br.close();
		}
	    
		return targets;
	}


	private List validateTargets(List targets, int maxTargetOnSameRun) throws ParseTargetListException {
			if (targets!=null && targets.size()>0) {
				// TO DO: check of dit nog werkt
				targets = OpticonTargetUtils.sortTragetbyRunnumber(targets);
				Iterator targetIterator = targets.iterator();
				OpticonTarget tr = (OpticonTarget) targetIterator.next();
				
				while (targetIterator.hasNext()) {

					//Dit kan weg
					int selectedRunNumber = Integer.parseInt(tr.getRunnumber());
					int runNumber = selectedRunNumber;
					OpticonTarget firsttr = tr;
					List targetsOnSameRunnumber = new ArrayList();
					while (selectedRunNumber == runNumber
							&& targetIterator.hasNext()) {
						targetsOnSameRunnumber.add(tr);
						tr = (OpticonTarget) targetIterator.next();
						runNumber = Integer.parseInt(tr.getRunnumber());
						
					}
					// Dit kan weg
					if(!targetIterator.hasNext()){//for the last item
						if(selectedRunNumber == runNumber)
							targetsOnSameRunnumber.add(tr);
						else {
							targetsOnSameRunnumber= new ArrayList();
							targetsOnSameRunnumber.add(tr);
							selectedRunNumber=Integer.parseInt(tr.getRunnumber());
						}
					   }
				
						// Dit moet behouden blijven
						if (!OpticonTargetUtils
								.validateTotalSubBandListForSameRunnumberPdf(
										selectedRunNumber,
										targetsOnSameRunnumber,maxTargetOnSameRun)) {
						
							throw new ParseTargetListException(
									"error.exceed.maximum.subband.list",
									selectedRunNumber, 0);
						}
						
						if (!OpticonTargetUtils
								.validateExposureTime(targetsOnSameRunnumber)) {

							Double totalExposureTime = NorthStarDelegate
									.getResourceValue(firsttr.getAllocations(),
											OpticonConstants.TOTAL_DURATION);
							if(totalExposureTime ==null)
								throw new ParseTargetListException(
										"errors.required.runnumber",
										selectedRunNumber, 0);
							else
							throw new ParseTargetListException(
									"error.exposure.time.unequal.same.runnumber",
									selectedRunNumber, 0);
						}
					
				}
				
				
			}
			
			else {
				throw new ParseTargetListException("Invalid target File");
			}
		return targets;
	}


	/*public List parseTargetListFile(InputStream targetListFile,
			Observation templateObservation) throws IOException,
			ParseTargetListException, DatabaseException, NorthStarException {
		boolean oldFormat = false;
		lineNumber = 1;
		List targets = new ArrayList();
		BufferedReader br = new BufferedReader(new InputStreamReader(
				targetListFile));
		String line = br.readLine();
		String splitword = "[ \\t\\x0B]+";
		if (line.matches(".*;.*")) {
			splitword = ";";
			oldFormat = true;
		}
		String[] header = null;
		
		 //parse al the lines
		 
		while (line != null && line.length() > 0) {
			String[] pieces = line.split(splitword);
			OpticonTarget target = null;
			
			 // if there is an valid header
			 
			if (lineNumber == 1 && oldFormat && isHeader(pieces)) {
				header = pieces;
			} else {
				target = createTarget(pieces);
				targets.add(target);
			}
			line = br.readLine();
			lineNumber++;
		}

		return targets;
	}
*/
	public List parseTargetListFile(InputStream targetListFile,
			Target templateTarget, List list, int maxTargetOnSameRun) throws IOException,
			ParseTargetListException, DatabaseException, NorthStarException {
		List<PastTargets> existingRunWithTime = new ArrayList();
	    int observationSize = 0;
	    int pipelineSize = 0;
	    
	    if (list!=null){
	    	int index=0;
	    	while(index<list.size()){
	    		OpticonObservingRequest ooRequest=(OpticonObservingRequest) list.get(index++);
	    		if(ooRequest.getObservations()!=null) {
	    			observationSize+=ooRequest.getObservations().size();
	    		}
	    		if(ooRequest.getPipelines()!=null) {
	    			pipelineSize+=ooRequest.getPipelines().size();
	    		}
	    		Iterator targetIt = ooRequest.getTargets().iterator();
	    		while (targetIt.hasNext()){
	    		  OpticonTarget opTarget =(OpticonTarget) targetIt.next();
	    		  existingRunWithTime.add(new PastTargets(
	    				  opTarget.getRunnumber(),
	    				  NorthStarDelegate.getResourceValue(opTarget.getAllocations(),OpticonConstants.TOTAL_DURATION),
	    				  0));
	    		}    			
	    	}
	    }

		lineNumber = 1;
		List<OpticonTarget> targets = new ArrayList();
		BufferedReader br = null;	
		try {
			br = new BufferedReader(new InputStreamReader(
				targetListFile));
			
			String line = "";
			
			while ((line = br.readLine()) != null) {
				StringBuilder tempLine = new StringBuilder("");
				boolean isQuote = false;
				for (int iterLine = 0; iterLine < line.length(); iterLine++) {
					if(line.charAt(iterLine)=='"') {
						isQuote = !isQuote;
					}
					if(line.charAt(iterLine) ==',' && !isQuote) {
						tempLine.append("CuThErE");
					} else {
						tempLine.append(line.charAt(iterLine));
					}
				}
				if(isQuote) {
					throw new ParseTargetListException("error.parser.noquotes",lineNumber);
				}
				String [] pieces = tempLine.toString().split("CuThErE"); 
	        
				if (pieces.length >= 1 && pieces[0].length() > 0	&& !pieces[0].startsWith("#")) {
				
					OpticonTarget target = createTarget(pieces);
					
					boolean isRunNrmKnown = false;
					Double targetsDuration = NorthStarDelegate.getResourceValue(target.getAllocations(),OpticonConstants.TOTAL_DURATION);
					search: for(PastTargets pastTarget : existingRunWithTime) {
						if(target.getRunnumber().trim().equals(pastTarget.getRunnumber().trim())) {
							isRunNrmKnown = true;
							if(targetsDuration != null) {
								if(pastTarget.getDurInSec() != null) {
									if(!targetsDuration.equals(pastTarget.getDurInSec())) {
										throw new ParseTargetListException("error.parser.contradicting.durations",lineNumber);
									}
								} else {
									for(int targetIter = existingRunWithTime.size() - 1 ; targetIter >= 0; targetIter--) {
										if(existingRunWithTime.get(targetIter).getRunnumber().equals(target.getRunnumber())) {
											existingRunWithTime.set(targetIter,new PastTargets(target.getRunnumber(), targetsDuration, 0));
										}
									}
									for(int targetIter = 0 ; targetIter < targets.size(); targetIter++) {
										if (targets.get(targetIter).getRunnumber().equals(target.getRunnumber())){
											OpticonTarget fixedTarget = targets.get(targetIter);
											Map allocations = target.getAllocations();
											fixedTarget.setAllocations(allocations);
											targets.set(targetIter, fixedTarget);
										}
									}
									break search;
								}
							} else {
								if(pastTarget.getDurInSec() != null) {
									ResourceType resourceType = null;
									try {
										northStarDelegate = NorthStarDelegate.getInstance();
										resourceType = northStarDelegate.getResourceType(OpticonConstants.TIME, OpticonConstants.SECONDS);
									} catch (DatabaseException e) {
										log.error("db error");
									}
									Map allocations = new HashMap();
									NorthStarDelegate.setResourceValue(allocations, OpticonConstants.TOTAL_DURATION, pastTarget.getDurInSec(),	resourceType);
									target.setAllocations(allocations);			
								} else {
									existingRunWithTime.add(new PastTargets(target.getRunnumber(), null, lineNumber));
									break search;
								}
							}
						}
					}
					if(!isRunNrmKnown) {
						target.setFirstInRun(true);
						if(targetsDuration != null)
							existingRunWithTime.add(new PastTargets(target.getRunnumber(), targetsDuration, 0));	
						else
							existingRunWithTime.add(new PastTargets(target.getRunnumber(), null, lineNumber));
					}
					
					if(	!AstronValidator.isBlankOrNull(target.getObservingrun()) && 
						'A' - target.getObservingrun().trim().toUpperCase().charAt(0) + observationSize <= 0){
						throw new ParseTargetListException("error.uavilable.observation.list",lineNumber);
					}
				
					if(	!AstronValidator.isBlankOrNull(target.getPipeline())) {
						String[] pipes = target.getPipeline().split(";");
						for(String pipe : pipes) {
							if('A' - pipe.trim().toUpperCase().charAt(0) + pipelineSize <= 0) {
								throw new ParseTargetListException("error.uavilable.pipeline.list",lineNumber);
							}
						}
					}
					
					targets.add(target);
					lineNumber++;
				}
			}
		} finally {
			br.close();
		}
		// Check here if any time is still empty
		for(PastTargets pastTarget:existingRunWithTime) {
			if(pastTarget.getDurInSec() == null) {
				throw new ParseTargetListException("error.parser.no.known.duration",pastTarget.getLineNumber());
			}
		}
		validateTargets(targets,maxTargetOnSameRun);
		return targets;
	}	



	private OpticonTarget createTarget(String[] pieces) throws ParseTargetListException {
		OpticonTarget target = new OpticonTarget();

		int piecesIter = 0;
		//if(pieces[0].startsWith("\"")){
		//	pieces = createTargetName(pieces);
		//}
		
		//if (pieces.length >= 4) 
		if (pieces.length >= 5) // Runnumber is now a required field
		{	
			target.setFieldName(pieces[piecesIter]); //0
			if (!AstronValidator.isRa(pieces[++piecesIter])){ //1 
				throw new ParseTargetListException("error.parser.notra", lineNumber);
			} 
			else if (!AstronValidator.isDec(pieces[++piecesIter])) //2 
			{
				throw new ParseTargetListException("error.parser.notdec",lineNumber);
			} else {	
				try {
					AstroCoordinate raDec = new AstroCoordinate(pieces[piecesIter - 1], pieces[piecesIter]);
					target.setRa(new Double(raDec.RA()));
					target.setDecl(new Double(raDec.Dec()));
				} catch (Exception e) {
					// should not happen
				}
			}
			
			if (isEpoch(pieces[++piecesIter])) {
				target.setEpoch(convertEpoch(pieces[piecesIter]));
			} else {
				throw new ParseTargetListException("error.parser.notepoch",	lineNumber);
			}
			
			if (AstronValidator.isPositiveInt(pieces[++piecesIter])){
				target.setRunnumber(pieces[piecesIter]);
			} else if(pieces[piecesIter] != ""  && pieces[piecesIter].trim().length() != 0){ 
				throw new ParseTargetListException("error.parser.notrunnumber", lineNumber);
			}
			
			if (pieces.length > ++piecesIter) {
				if (pieces[piecesIter].trim().equalsIgnoreCase("Y") || pieces[piecesIter].trim().equalsIgnoreCase("N") || pieces[piecesIter].equalsIgnoreCase("")) {//5
					if(pieces[piecesIter].trim().equalsIgnoreCase("Y")){
						target.setCalibrationBeam(true);
					} else {
						target.setCalibrationBeam(false);
					}
				} else if(pieces[piecesIter].trim().length() != 0){
					throw new ParseTargetListException("error.parser.nocalibrator",	lineNumber);
				}
				
				if (pieces.length > ++piecesIter){	
					if (pieces[piecesIter].trim().length() != 0 && isTime(pieces[piecesIter].trim()))
					{
						String timevalue=pieces[piecesIter].trim();
						timevalue=timevalue.replaceAll(",","");
						
						Integer targetTime = timeToSeconds(timevalue.trim());
						Double totalDuration = new Double(targetTime.doubleValue());
						if(totalDuration <= 0) {
							throw new ParseTargetListException("error.parser.nottime",lineNumber);
						}
						ResourceType resourceType =null;
						try {
							northStarDelegate = NorthStarDelegate.getInstance();
							resourceType = northStarDelegate.getResourceType(OpticonConstants.TIME, OpticonConstants.SECONDS);
						} catch (DatabaseException e) {
							log.error("db error");
						}
						Map allocations = new HashMap();
						NorthStarDelegate.setResourceValue(allocations, OpticonConstants.TOTAL_DURATION, totalDuration,	resourceType);
						target.setAllocations(allocations);
					}
					else if(pieces[piecesIter] != null && pieces[piecesIter].trim().length() == 0 ){
						// TO DO: check why this was commented
						throw new ParseTargetListException("error.parser.nottime",lineNumber);
					}
					
					if (pieces.length > ++piecesIter){
						if (isDouble(pieces[piecesIter])) {
							target.setFlux(new Double(pieces[piecesIter]));
						} else if(pieces[piecesIter].trim().length() != 0){ 
							throw new ParseTargetListException("error.parser.notflux", lineNumber);
						}
						
						if (pieces.length > ++piecesIter){
							if (isDouble(pieces[piecesIter])) {
								target.setFluxFrequency(new Double(pieces[piecesIter]));
							} else if(pieces[piecesIter] != null && pieces[piecesIter].trim().length() != 0){ 
								throw new ParseTargetListException("error.parser.notfluxfrequency", lineNumber);
							}
						
							if (pieces.length > ++piecesIter){
								if (isDouble(pieces[piecesIter])) {
									target.setSpectralIndex(new Double(pieces[piecesIter]));
								} else if(pieces[piecesIter].trim().length() != 0){ 
									throw new ParseTargetListException("error.parser.notspectralindex", lineNumber);
								}
								
								if (pieces.length > ++piecesIter){
									// TO DO: test this
									target.setSubbandList(pieces[piecesIter].replaceAll("\"",""));
									try {
										target.setTotalSubbandList(String.valueOf(OpticonTargetUtils.getNumberofSubBands(pieces[piecesIter].replaceAll("\"",""))));
									} catch (Exception e) {
										if (pieces[piecesIter].trim().length() != 0)
											throw new ParseTargetListException("error.parser.subband", lineNumber);
									}
									
										if (pieces.length > ++piecesIter){
											if (pieces[piecesIter].length() == 1 || pieces[piecesIter].length() == 2) {
												target.setObservingrun(pieces[piecesIter] );
											} else if(pieces[piecesIter].length() != 0){ 
												throw new ParseTargetListException("error.parser.notobservingrun", lineNumber);
											}
											
											if (pieces.length > ++piecesIter){
												if(pieces[piecesIter].trim().length() != 0) {
													target.setPipeline(pieces[piecesIter].replaceAll("\\s+",""));
												} 
												
												if (pieces.length > ++piecesIter) {
													String comment = concatListFromIndex(pieces, piecesIter);
													if(comment.length() > 24){ 
														throw new ParseTargetListException("error.parser.commentexceeded", lineNumber);
													}else{
														target.setComments(comment);
														
													}
												}
												
												if (pieces.length > ++piecesIter) {
													throw new ParseTargetListException("error.parser.toomanyfields", lineNumber);
												}
											}
										}
								}
							}
						}
					}
				}
			}
		}else 
		{
			throw new ParseTargetListException("error.parser.notenoughfields", lineNumber);
		}


		return target;
	}


	private String[] createTargetName(String[] pieces) throws ParseTargetListException {
		String name="";
		int i=0;
		while(!pieces[i].endsWith("\"") && i < pieces.length -1){
			name+=pieces[i]+" ";
			i++;
		}
		name+=pieces[i];
		// no closing quotes
		if(i== pieces.length -1){
			throw new ParseTargetListException("error.parser.noquotes",lineNumber);
		}
		name=name.substring(1);
		name=name.substring(0, name.length()-1);
		String[] result = new String[pieces.length-i] ;
		result[0]=name;
		for(int j=1;j<pieces.length-i;j++){
			result[j]=pieces[j+i];
		}
		
		return result;
		
		
	}


	private List convertRuns(String runs) {
		String[] list = runs.split(",");
		List result = new ArrayList();
		if (list != null && list.length > 0) {
			for (int i = 0; i < list.length; i++) {
				String value = list[i];
				// only add single characters
				if(value.length()==1){
					result.add(value);
				}
			}
		}
		return result;
	}


	private boolean isRuns(String runs) {
		if(runs.length() > 1){
			String[] list = runs.split(",");
			if (list.length==1){
				return false;
			}
		}
		return true;
	}

	/**
	 * parses the headers
	 * @see eu.radionet.northstar.business.targetlist.TargetListParser#parseHeader(java.util.List)
	 */
	protected List parseHeader(List possibleHeaders) throws ParseTargetListException
	{
		for (int i = 0; i < optionalHeaders.length; i++){
			boolean found = false;
			for (int j = possibleHeaders.size() - 1; j >= 0 && !found;j--){
				String possibleHeader = (String )possibleHeaders.get(j);
				if (possibleHeader.equalsIgnoreCase(optionalHeaders[i])){
					found = true;
					possibleHeaders.remove(j);
				}
			}
		}
		return super.parseHeader(possibleHeaders);
	}

	/**
	 * Converts a row of the target list into a Java bean
	 * @see eu.radionet.northstar.business.targetlist.TargetListParser#createTargetFileBean(java.lang.String[])
	 */
	protected TargetFileBean createTargetFileBean(String[] pieces)
	throws ParseTargetListException 
	{

		TargetFileBean targetFileBean = new TargetFileBean();

		if (pieces.length >= 4) 
		{
			targetFileBean.getFields().put(FIELD_NAME, pieces[0]);
			if (!AstronValidator.isRa(pieces[1])) 
			{
				throw new ParseTargetListException("error.parser.notra", lineNumber);
			} 
			else if (!AstronValidator.isDec(pieces[2])) 
			{
				throw new ParseTargetListException("error.parser.notdec",lineNumber);
			} 
			else 
			{
				targetFileBean.getFields().put(RA, pieces[1]);
				targetFileBean.getFields().put(DEC, pieces[2]);
			}
			if (isEpoch(pieces[3])) 
			{
				targetFileBean.getFields().put(EPOCH, pieces[3]);
			} 
			else 
			{
				throw new ParseTargetListException("error.parser.notepoch",	lineNumber);
			}
			if (pieces.length > 4)
			{	
				if (isTime(pieces[4])){
					targetFileBean.getFields().put(EXPOSURE_TIME, pieces[4]);
				} else {
					throw new ParseTargetListException("error.parser.nottime",	lineNumber);
				}
				if (pieces.length > 5)
				{
					if (isDouble(pieces[5])) {
						targetFileBean.getFields().put(FLUX, pieces[5]);
					} else {
						throw new ParseTargetListException("error.parser.notflux", lineNumber);
					}
					if (pieces.length > 6)
					{
						if (isDouble(pieces[6])) {
							targetFileBean.getFields().put(FLUX_FREQUENCY, pieces[6]);
						} else {
							throw new ParseTargetListException("error.parser.notfluxfrequency", lineNumber);
						}
						if (pieces.length > 7)
						{
							if (isDouble(pieces[7])) {
								targetFileBean.getFields().put(SPECTRAL_INDEX, pieces[7]);
							} else {
								throw new ParseTargetListException("error.parser.notspectralindex", lineNumber);
							}
							if (pieces.length > 8) {
								targetFileBean.getFields().put(COMMENTS, concatListFromIndex(pieces, 8));
							}
						}
					}
				}
			}	
		} 
		else 
		{
			throw new ParseTargetListException("error.parser.notenoughfields", lineNumber);
		}
		return targetFileBean;
	}

	private boolean isDouble(String value) {
		
		if (AstronValidator.isPositiveDouble(value)) {
			return true;
		}
		
		return false;
	}


	/**
	 * Creates the observation object out of a bean representing a row
	 * in the target list.
	 * @see eu.radionet.northstar.business.targetlist.TargetListParser#createObservation(eu.radionet.northstar.business.targetlist.TargetFileBean)
	 */
	protected List createTargetValues(TargetFileBean targetFileBean)
	throws ParseTargetListException, DatabaseException 
	{
		List targetValues = new ArrayList();
		if (!targetFileBean.getFields().isEmpty()) 
		{
			Map fields = targetFileBean.getFields();
			/*
			 * retrieve fields
			 */
			String fieldName = (String) fields.get(FIELD_NAME);
			String ra = (String) fields.get(RA);
			String dec = (String) fields.get(DEC);
			String epoch = (String) fields.get(EPOCH);
			if (AstronValidator.isBlankOrNull(fieldName)) {
				throw new ParseTargetListException(
						"error.parser.fieldname.mandatory", lineNumber);
			}
			if (!AstronValidator.isRa(ra)) {
				throw new ParseTargetListException("error.parser.notra",
						lineNumber);
			}
			if (!AstronValidator.isDec(dec)) {
				throw new ParseTargetListException("error.parser.notdec",
						lineNumber);
			}
			if (AstronValidator.isBlankOrNull(epoch) && isEpoch(epoch)) {
				throw new ParseTargetListException("error.parser.notepoch",
						lineNumber);
			}

			targetValues.add(new ValueBean(fieldName));
			targetValues.add(new ValueBean(ra));
			targetValues.add(new ValueBean(dec));
			targetValues.add(new ValueBean(convertEpoch(epoch)));
			String exposureTime = (String) fields.get(EXPOSURE_TIME);
			if (!AstronValidator.isBlankOrNull(exposureTime)) 
			{
				targetValues.add(new ValueBean(timeToHours(exposureTime).toString()));
			}
			else
			{
				targetValues.add(new ValueBean(null));
			}
			targetValues.add(new ValueBean((String) fields.get(COMMENTS)));
		} 
		return targetValues;
	}



	/*
	 *  (non-Javadoc)
	 * @see eu.radionet.northstar.business.targetlist.TargetListParser#createObservation(eu.radionet.northstar.business.targetlist.TargetFileBean, eu.radionet.northstar.data.entities.Observation)
	 */
	protected List createTargetValues(TargetFileBean targetFileBean, Target templateTarget)  
	throws ParseTargetListException, DatabaseException
	{
		List targetValues =  createTargetValues(targetFileBean);
		OpticonTarget opticonTemplateTarget = (OpticonTarget) templateTarget;
		if (((ValueBean) targetValues.get(4)).getValue() == null && opticonTemplateTarget.getAllocations() != null)
		{
			Double totalExposureTime = NorthStarDelegate.getResourceValue(
					opticonTemplateTarget.getAllocations(),	OpticonConstants.TOTAL_DURATION);
			targetValues.set(4, new ValueBean(AstronConverter.getMinutesFromSeconds(totalExposureTime)));
		}
		if (((ValueBean) targetValues.get(5)).getValue()== null)
		{
			targetValues.set(5, new ValueBean(opticonTemplateTarget.getComments()));
		}
		return targetValues;
	}


	/**
	 * Sets the Resource  for all allocations in the observation
	 * @param observation
	 * @param exposureTime
	 * @throws DatabaseException
	 */
	protected void setTotalDuration(OpticonTarget target, Integer exposureTime) 
	throws DatabaseException
	{
		if (exposureTime != null)
		{
			NorthStarDelegate northStarDelegate = NorthStarDelegate.getInstance();
			Double value = new Double(exposureTime.doubleValue());
			ResourceType resourceType = northStarDelegate.getResourceType("time","seconds");
			Map allocations = target.getAllocations();
			NorthStarDelegate.setResourceValue(
					allocations, OpticonConstants.TOTAL_DURATION, value,resourceType);
		}
	}


	protected Observation createObservation(
			TargetFileBean targetFileBean, Observation observation) throws ParseTargetListException, DatabaseException
			{
		//not used
		return null;
			}

	protected Observation createObservation(TargetFileBean targetFileBean) throws ParseTargetListException, DatabaseException
	{
		//not used
		return null;
	}	

	protected boolean isTime(String time)
	{
		boolean result = false;
		if (time.endsWith("s") || time.endsWith("m") || time.endsWith("h"))
		{
			result = AstronValidator.isInt(time.substring(0, time.length()-1));
		} 
		return result;
	}


	protected String concatListFromIndex(String[] pieces, int index)
	{
		String result = pieces[index];
		if (pieces.length > index+1)
		{
			for (int i = index+1; i < pieces.length; i++)
			{
				result = result + ' ' + pieces[i];
			}
		}
		return result;
	}
}
	
class PastTargets{
	private String runnumber;
	private Double durInSec;
	private int lineNumber;
	
	PastTargets(String runnumber, Double durInSec, int lineNumber){
		this.runnumber = runnumber;
		this.durInSec = durInSec;
		this.lineNumber = lineNumber;
	}
	
	String getRunnumber() {
		return this.runnumber;
	}
	
	Double getDurInSec() {
		return this.durInSec;
	}
	
	void setDurInSec(Double durInSec) {
		this.durInSec = durInSec;
	}
	
	int getLineNumber() {
		return lineNumber;
	}
}
