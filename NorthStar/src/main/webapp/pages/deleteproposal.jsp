<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>

<tiles:get name="errors" />
<html:form action="/processDeleteProposal" method="POST">
<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
<TABLE>
	<TR>
		<TD valign="top"><img src="<%=request.getContextPath()%>/images/list/delete_big.gif"></TD>
		<TD valign="top">
			<logic:empty name="retractDeleteForm" property="proposalCode">
				Are you really sure you want to delete proposal for telescope <b><bean:write name="retractDeleteForm" property="telescope"/></b> with title '<B><bean:write name="retractDeleteForm" property="proposalTitle"/></B>' ?
			</logic:empty>
			<logic:notEmpty name="retractDeleteForm" property="proposalCode">
				Are you really sure you want to delete proposal for telescope <b><bean:write name="retractDeleteForm" property="telescope"/></b> <BR>with title '<B><bean:write name="retractDeleteForm" property="proposalTitle"/></B>' and with project id '<B><bean:write name="retractDeleteForm" property="proposalCode"/></B>'?
			</logic:notEmpty>
			<logic:equal name="retractDeleteForm" property="telescope" value="WSRT">
				<BR>The proposal to delete has category '<b><bean:write name="retractDeleteForm" property="proposalCategory"/></B>'
			</logic:equal>
		</TD>
	</TR>
	</TABLE>
	<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />	
<TABLE width="100%">
	<TR>
		<TD class="left">
			<html:submit property="deleteButton" tabindex="1" styleClass="list_accept_yes" title="Ok">Ok</html:submit></TD><td class="right">
			<html:cancel tabindex="2" styleClass="list_decline_no" title="Cancel">Cancel</html:cancel>
		</TD>
	</TR>
</TABLE>

</html:form>


