// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletContext;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class VersionTag extends TagSupport {
	protected static String version = null;

	public int doStartTag() throws JspException {
		if (version == null) {
			String tempVersion = null;
			try {
				tempVersion = read("/version.txt", this.pageContext
						.getServletContext());
				version = getVersion(tempVersion);

			} catch (Exception ioe) {
				version = "No version information available";
			}

		}
		JspWriter out = pageContext.getOut();
		try {
			out.print(version);
		} catch (IOException e) {
			throw new JspException("Cannot print");
		}
		return EVAL_BODY_INCLUDE;
	}

	protected String getVersion(String inputVersion) {
		String result = "NorthStar version ";
		String badResult = "No version information available";
		if (inputVersion.startsWith("VERSION=")) {
			inputVersion = inputVersion.substring("VERSION=".length());
			/*
			 * test build
			 */
			if (inputVersion.startsWith("${tagversion}")) {
				String[] versionElements = inputVersion.split("BUILDDATE=");
				result = "BUILD " + versionElements[1];
			} else {
				String temp = "";
				if (inputVersion.startsWith("Ntestrelease")
						 ) {
					temp += "beta";
				} else if (inputVersion.startsWith("demorelease")) {
					temp += "demo";
				}else if (inputVersion.startsWith("Tbranch")) {
					temp += "branch";
				}
				String[] versionElements = inputVersion.split("[_ ]");
				result += versionElements[4] + "." + versionElements[5] + "."
						+ versionElements[6] + temp;
				if (inputVersion.startsWith("Tbranch")) {
					versionElements = inputVersion.split("BUILDDATE=");
					result += " ( BUILD " + versionElements[1] + ")";
				} else {
					String dateString = inputVersion.split("BUILDDATE=")[1];
					String date = dateString.split(" ")[0];
					result += " (" + date + ")";
				}

			}
		} else {
			return badResult;
		}
		return result;
	}

	protected String read(String fileName, ServletContext servletcontext)
			throws IOException {
		InputStream headerInputStream = servletcontext
				.getResourceAsStream(fileName);
		StringBuffer sb = new StringBuffer();
		BufferedReader in = new BufferedReader(new InputStreamReader(
				headerInputStream));
		String s;
		while ((s = in.readLine()) != null) {
			sb.append(s);
			sb.append("\n");
		}
		in.close();
		return sb.toString();
	}
}
