package eu.opticon.northstar.control.observingrequest.instrument;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.xml.rpc.ServiceException;

import nl.astron.service.lofar.StorageCalculator;
import nl.astron.service.lofar.client.StorageCalculatorBinding;

import org.junit.Before;
import org.junit.Test;

import eu.opticon.northstar.business.OpticonConstants;
import eu.opticon.northstar.control.observingrequest.SetUpOpticonObservingRequestAction;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;


public class SetUpInstrumentBeanTest {

	SetUpInstrumentBean opticonBean;
	ProcessingTimeDAO calibrationDao;
	ProcessingTimeDAO baseLineCalibrationDao;
	ProcessingTimeDAO calibrationImagingDao;
	ProcessingTimeDAO preProcessingDao;
	StorageCalculatorBinding binding;
	StorageCalculator sCalc ;
	@Before
	public void setUp() throws Exception {
		opticonBean = new 	SetUpInstrumentBean();
		java.net.URL address = new java.net.URL("http://lofar.astron.nl:8080/service/services/storageCalculator");
		binding = new StorageCalculatorBinding();
		binding.setService(address.toString());
		
		 sCalc = (StorageCalculator) binding;
		String flaggingStrategy="LBA";
		String antenna ="HBA One (24)";
		boolean isDemixing = true;
		List<String> demixingSources = new ArrayList<String>();
		demixingSources.add("CygA");
		demixingSources.add("TauA");
		demixingSources.add("HerA");
		demixingSources.add("HydA");
		int totalStation = 24;
		int totalTargetSubband = 100;
		double totalObservingDuration = 3600;
		double fieldOfView=1.0;
		double uvRange = 2.0;
		double baselineFactor = 4.0;
		String processingMode = "Pre processing only";

		calibrationDao = new ProcessingTimeDAO(
				OpticonConstants.PROCESSING_MODE_CALIBRATION, flaggingStrategy,
				antenna, isDemixing, demixingSources, totalStation,
				totalTargetSubband, totalObservingDuration, fieldOfView,
				uvRange, baselineFactor, processingMode);
		
		baseLineCalibrationDao = new ProcessingTimeDAO(
				OpticonConstants.PROCESSING_MODE_BASELINE, flaggingStrategy,
				antenna, false, null, totalStation,
				totalTargetSubband, totalObservingDuration, fieldOfView,
				uvRange, baselineFactor, processingMode);
		
		calibrationImagingDao = new ProcessingTimeDAO(
				OpticonConstants.PROCESSING_MODE_CALIBRATION_AND_IMAGING, flaggingStrategy,
				antenna, isDemixing, demixingSources, totalStation,
				totalTargetSubband, totalObservingDuration, fieldOfView,
				uvRange, baselineFactor, processingMode);
		
		

	}

	@Test
	public void testCalculateStorageOpticonObservationURL() {
		//fail("Not yet implemented");
	}

	@Test
	public void testCalculateStorageOpticonObservationDoubleIntStorageCalculator() {
		//fail("Not yet implemented");
	}

	@Test
	public void testCalculateBFAndUVSize() {
		double totalDuration = 3600;
		OpticonObservation opticonObservation = new OpticonObservation();
		opticonObservation.setInstrument(prepareOpticonIOnstrument("Beam"));
		int targetSubbands=100;
		HashMap result = null;
		try {
			result =SetUpInstrumentBean.calculateBFAndUVSize(opticonObservation, totalDuration,
					targetSubbands, sCalc);
			System.out.println("Result for Beam Observation"+ result!=null?result: "ERROR");
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
		
		opticonObservation.setInstrument(prepareOpticonIOnstrument("Beam-Interfero"));
	
		try {
			result =SetUpInstrumentBean.calculateBFAndUVSize(opticonObservation, totalDuration,
					targetSubbands, sCalc);
			 System.out.println("Result for Beam-Interfero Observation"+ result!=null?result: "ERROR");
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		opticonObservation.setInstrument(prepareOpticonIOnstrument("TBB"));
		
		try {
			result =SetUpInstrumentBean.calculateBFAndUVSize(opticonObservation, totalDuration,
					targetSubbands, sCalc);
			 System.out.println("Result for TBB Observation"+ result!=null?result: "ERROR");
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		opticonObservation.setInstrument(prepareOpticonIOnstrument(OpticonConstants.TBB_PIGGYBACK));
		
		try {
			result =SetUpInstrumentBean.calculateBFAndUVSize(opticonObservation, totalDuration,
					targetSubbands, sCalc);
			 System.out.println("Result for TBBPIGGYBACK Observation"+ result!=null?result: "ERROR");
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		opticonObservation.setInstrument(prepareOpticonIOnstrument("Beam-TBB"));
		
		try {
			result =SetUpInstrumentBean.calculateBFAndUVSize(opticonObservation, totalDuration,
					targetSubbands, sCalc);
			 System.out.println("Result for Beam-TBB Observation"+ result!=null?result: "ERROR");
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
		opticonObservation.setInstrument(prepareOpticonIOnstrument("TBB-Interfero"));
		
		try {
			result =SetUpInstrumentBean.calculateBFAndUVSize(opticonObservation, totalDuration,
					targetSubbands, sCalc);
			System.out.println("Result for TBB-Interfero Observation"+ result!=null?result: "ERROR");
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		opticonObservation.setInstrument(prepareOpticonIOnstrument("Interfero"));
		
		try {
			result =SetUpInstrumentBean.calculateBFAndUVSize(opticonObservation, totalDuration,
					targetSubbands,sCalc);
			System.out.println("Result for Interfero Observation"+ result!=null?result: "ERROR");
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}

	private OpticonInstrument prepareOpticonIOnstrument(String instrumentName) {
		OpticonInstrument instrument = new OpticonInstrument();
		instrument.setAntenna("HBA One (24)");
		instrument.setAveragingFrequency(2.0);
		instrument.setName(instrumentName);
		instrument.setChannels(64);
		instrument.setCohstokes(true);
		instrument.setIncchannels(64);
		instrument.setIncohIntsteps(1);
		instrument.setIncohstokes(true);
		instrument.setIntegrationTime(1d);
		instrument.setIntsteps(2);
		instrument.setRawVoltage(true);
		instrument.setRings(23);
		instrument.setStoreRawData(true);
		instrument.setStoreUVData(true);
		instrument.setStation("Core (24, 0, 0)");
		instrument.setPolarization("I");;
		return instrument;
	}

	@Test
	public void testCalculateProcessingTime() {

		HashMap processingTime =new HashMap();
		try {
			processingTime=SetUpInstrumentBean.calculateProcessingTime(calibrationDao.getMode(), calibrationDao.getFlaggingStrategy(), calibrationDao.getAntenna(),
					false, null, calibrationDao.getTotalStation(), calibrationDao.getTotalTargetSubband(),
					calibrationDao.getTotalObservingDuration(), calibrationDao.getFieldOfView(), calibrationDao.getUvRange(), sCalc,calibrationDao.getBaselineFactor(), null, calibrationDao.getProcessingMode());

			
			System.out.println("Result for Calibration Observation"+ processingTime!=null?processingTime: "ERROR");
			processingTime=SetUpInstrumentBean.calculateProcessingTime(calibrationDao.getMode(), calibrationDao.getFlaggingStrategy(), calibrationDao.getAntenna(),
					true, calibrationDao.getDemixingSources(), calibrationDao.getTotalStation(), calibrationDao.getTotalTargetSubband(),
					calibrationDao.getTotalObservingDuration(), calibrationDao.getFieldOfView(), calibrationDao.getUvRange(), sCalc,calibrationDao.getBaselineFactor(), null, calibrationDao.getProcessingMode());
			
			
			System.out.println("Result for Calibration-demixing Observation"+ processingTime!=null?processingTime: "ERROR");
			processingTime=SetUpInstrumentBean.calculateProcessingTime(calibrationImagingDao.getMode(), calibrationImagingDao.getFlaggingStrategy(), calibrationDao.getAntenna(),
					false, null, calibrationImagingDao.getTotalStation(), calibrationDao.getTotalTargetSubband(),
					calibrationImagingDao.getTotalObservingDuration(), calibrationImagingDao.getFieldOfView(), calibrationDao.getUvRange(), sCalc,calibrationDao.getBaselineFactor(), null, calibrationDao.getProcessingMode());

			System.out.println("Result for Calibration-Imaging Observation"+ processingTime!=null?processingTime: "ERROR");
			processingTime=SetUpInstrumentBean.calculateProcessingTime(calibrationImagingDao.getMode(), calibrationImagingDao.getFlaggingStrategy(), calibrationDao.getAntenna(),
					true, calibrationDao.getDemixingSources(), calibrationDao.getTotalStation(), calibrationDao.getTotalTargetSubband(),
					calibrationDao.getTotalObservingDuration(), calibrationDao.getFieldOfView(), calibrationDao.getUvRange(), sCalc,calibrationDao.getBaselineFactor(), null, calibrationDao.getProcessingMode());
			System.out.println("Result for Calibration-Imaging- demixing Observation"+ processingTime!=null?processingTime: "ERROR");
		
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		
	}

}
