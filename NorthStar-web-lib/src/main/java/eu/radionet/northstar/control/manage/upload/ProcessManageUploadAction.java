// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.manage.upload;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.radionet.northstar.business.configuration.CollaborationConfiguration;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.business.util.XMLConverter;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.admin.upload.SetUpUploadConfigAction;

public class ProcessManageUploadAction extends Action {

	private Log log = LogFactory.getLog(SetUpUploadConfigAction.class);
	private static final String BACKUP_DIR = "backup";
	private static final String NORTHSTAR_CONFIG = "northstar-config";
	private static final String MANAGE_UPLOAD = "manageUpload";
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ManageUploadForm manageUploadForm = (ManageUploadForm) form;
		ActionMessages errors = new ActionMessages();
		
		if(manageUploadForm.getUploadFileButton() != null){
			String newConfigFile = new String();
			try {
				newConfigFile = storeFile(manageUploadForm.getConfigFile().getInputStream());
			}
			catch (IOException ie){
				//could not save backup file
				errors.add(MANAGE_UPLOAD, new ActionMessage("error.file.save.backup"));
				saveErrors(request, errors);
				return mapping.findForward(Constants.REFRESH);
			}
			String telescope = null;
			try{
				telescope = validateFile(newConfigFile);
			}catch(Exception e){
				// not a valid xml file
				errors.add(MANAGE_UPLOAD, new ActionMessage("error.file.save.xml"));
				saveErrors(request, errors);
				return mapping.findForward(Constants.REFRESH);
				
			}
			if(telescope == null){
				// could not find telescope name
				errors.add(MANAGE_UPLOAD, new ActionMessage("error.file.save.mismatch"));
				saveErrors(request, errors);
				return mapping.findForward(Constants.REFRESH);
			}
			// copy to the new location
			// get backup dir (outside of context so it is not overwritten by a deploy)
			if(telescope.equalsIgnoreCase(manageUploadForm.getSelectedTelescope() )){
				String currentConfigFileLocation = request.getSession().getServletContext().getRealPath(manageUploadForm.getCurrentConfigFileUrl());
				replaceFile(currentConfigFileLocation,newConfigFile,request);
				reloadConfig(request);
			}else{
				// telescope name does not match
				errors.add(MANAGE_UPLOAD, new ActionMessage("error.file.save.differ"));
				saveErrors(request, errors);
				log.warn("telescopes do not match, given: "+telescope+" required: "+manageUploadForm.getSelectedTelescope());
				return mapping.findForward(Constants.REFRESH);
			}
			
		}
		if(manageUploadForm.getReadConfigFile()!= null){
			String fileName=manageUploadForm.getReadConfigFile();
				fileName= request.getSession().getServletContext().getRealPath(fileName);
			sendFile(fileName,response);
			return null;
		}
		
 		return mapping.findForward(Constants.REFRESH);
	}
	
	private String validateFile(String newConfigFile) throws Exception {
		String telescopeFile = NorthStarUtils.read(newConfigFile);
		String optionUri = null;
		NodeList elements = null;
		try{
			XMLConverter xmlConverter = XMLConverter.getInstance();
			Document optionsDocument = xmlConverter.convertStringToDocument(telescopeFile);
			elements = optionsDocument.getElementsByTagName("configuration");
		}catch (Exception e){
			log.warn(" not a valid xml file");
			throw e;
		}
		for (int i = 0; i < elements.getLength(); i++) {
			Node node = (Node) elements.item(i);
			for (int j = 0; j < node.getChildNodes().getLength(); j++) {
				Node nodeChild = node.getChildNodes().item(j);
				// telescopes section found, iterate trough children for correct telescope
				if( nodeChild.getAttributes() != null){
					Node name = nodeChild.getAttributes().getNamedItem("name");
					if (name != null && name.getNodeValue() != null && name.getNodeValue().equalsIgnoreCase("telescopeConfigurations")){
						for (int k = 0; k < nodeChild.getChildNodes().getLength(); k++) {
							Node optionNode = nodeChild.getChildNodes().item(k);
							for (int m = 0; m < optionNode.getChildNodes().getLength(); m++) {
								Node valueNode = optionNode.getChildNodes().item(m);
								if(valueNode.getNodeName().equalsIgnoreCase("value")){
									return valueNode.getFirstChild().getNodeValue();
								}
							}
						}
					}
				}
			}
		}
		return null;
	}
	
	
	private void reloadConfig(HttpServletRequest request)throws Exception{
		HttpSession session = request.getSession();
		NorthStarUtils.loadNorthStarConfiguration(session.getServletContext());
		CollaborationConfiguration.setAcceptInvitation(NorthStarUtils
				.getLinkPath(request));
		log.info("NorthStar configuration loaded");
	}
	
	private void replaceFile(String currentFileName, String newFileName, HttpServletRequest request ) throws IOException{
		File contextDir = new File (request.getSession().getServletContext().getInitParameter(NORTHSTAR_CONFIG));
		String contextPath = contextDir.getParent();
		contextDir = new File (contextPath+System.getProperty("file.separator")+BACKUP_DIR);
		if (!contextDir.exists()){
			contextDir.mkdir();
		}
		ActionMessages errors = new ActionMessages();
		
		File temFile = new File(newFileName);
		File curFile = new File(currentFileName);
		if (curFile.exists() && temFile.exists()){
			File backFile = new File(contextDir,curFile.getName()+"."+curFile.lastModified());
			
			try{
				InputStream in = new FileInputStream(temFile);
		        OutputStream out = new FileOutputStream(backFile);
		    
		        // copy new file to backup dir (old file is already there)
		        byte[] buf = new byte[1024];
		        int len;
		        while ((len = in.read(buf)) > 0) {
		            out.write(buf, 0, len);
		        }
		        in.close();
		        out.close();
		        log.info("replaced file: "+currentFileName);
			}catch(IOException e){
				log.warn("could not copy file: "+curFile.getName());
				throw e;
			}

			curFile.delete();
			temFile.renameTo(curFile);
		}else{
			log.warn("file does not exist: "+currentFileName);
			
		}
	}

	private String storeFile(InputStream inputStream) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		int byteChar = inputStream.read();
		
		if (inputStream!=null)
		{
			while (byteChar != -1) {
				out.write(byteChar);
				byteChar = inputStream.read();
			}
		}
		
		if (out.size() > 8){
			File OutFile = File.createTempFile(System.currentTimeMillis()+"_", ".xml");
			FileOutputStream lFileStream = new FileOutputStream(OutFile);

			lFileStream.write(out.toByteArray());
			lFileStream.flush();
			lFileStream.close();
			
			return OutFile.getAbsolutePath();
		}
		else{
			return null;
		}
	}

	private void sendFile(String fileName, HttpServletResponse response) throws Exception{
		
		if (fileName != null)
		{
			File downloadFile = new File(fileName);
			if (downloadFile.exists()){
				String newFileName=fileName.substring(fileName.lastIndexOf("/")+1, fileName.length());
				Long filesize =  new Long(downloadFile.length());
				
				response.setContentLength(filesize.intValue());
				response.setContentType("text/plain");

				response.setHeader("Content-disposition", "attachment; filename="
						+ newFileName);
				response.setHeader("Cache-Control", "max-age=600");

				ServletOutputStream outStream = response.getOutputStream();
				
				InputStream instream = new FileInputStream(downloadFile);
				byte[] idata = new byte[instream.available()];
				instream.read(idata);
				// byte[] output = optionsFile.
				// write byte[]
				outStream.write(idata);
				outStream.flush();
				outStream.close();
				
				response.setStatus(200); // 200 OK
				log.info("send file: "+fileName);
				return;
			}
		}	
	}


}
