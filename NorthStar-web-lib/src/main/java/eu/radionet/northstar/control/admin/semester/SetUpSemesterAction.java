// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.semester;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.Telescope;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.Category;
import eu.radionet.northstar.data.entities.Semester;

public class SetUpSemesterAction extends Action {


	private NorthStarDelegate northStarDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SemesterForm semesterForm = (SemesterForm) form;

		/*
		 * retrieve own_useraccount info from session
		 */

		northStarDelegate = NorthStarDelegate.getInstance();
		Calendar currentDate = NorthStarDelegate.getCurrentDate();
		semesterForm.setCategories(northStarDelegate.getCategories());
		semesterForm.setCategory(northStarDelegate.getDefaultCategory().getId()
				.toString());
		semesterForm.setTelescopes(NorthStarConfiguration.getTelescopeNames());
		if (AstronValidator.isBlankOrNull(semesterForm.getTelescope())) {
			semesterForm.setTelescope((String) semesterForm.getTelescopes()
					.get(0));
		}
		Set communitySet = new TreeSet(SORT_LABEL_VALUE_BEAN);
		Telescope telescope = NorthStarConfiguration.getTelescope(semesterForm
				.getTelescope());
		Iterator iterator = telescope.getTelescopeConfigurations().iterator();
		while (iterator.hasNext()) {
			TelescopeConfiguration telescopeConf = (TelescopeConfiguration) iterator
					.next();
			if (telescopeConf.getEndTime() == null
					|| telescopeConf.getEndTime().after(currentDate.getTime())) {
				communitySet.add(new LabelValueBean(NorthStarConfiguration
						.getCommunityDescription(telescopeConf.getCommunity()),
						telescopeConf.getCommunity()));
			}
		}
		
		String singleComunity = null;
		if (communitySet.size() == 1){
			Object[] temp = communitySet.toArray();
			LabelValueBean community = (LabelValueBean) temp[0];
			semesterForm.setCommunityLabel(community.getLabel());
			singleComunity = community.getValue();
		}
		semesterForm.setCommunities(communitySet);
		SimpleDateFormat longDateFormat = new SimpleDateFormat();
		longDateFormat.applyPattern(Constants.SEMESTER_DATETIME_FORMAT);
		longDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		
		semesterForm.getParents().add(new LabelValueBean("none","" ) );
		
			List parents = northStarDelegate.getAvailableSemesters(semesterForm.getTelescope());
			Iterator parentit = parents.iterator();
			while (parentit.hasNext()){
				Semester sem = (Semester) parentit.next();
				semesterForm.getParents().add(new LabelValueBean(sem.getSemester(),sem.getId().toString() ) );
			}
		         
		
		if (AstronValidator.isPositiveInt(semesterForm.getSemesterId())) {
			Semester semester = northStarDelegate.getSemester(AstronConverter
					.toInteger(semesterForm.getSemesterId()));
			semesterForm.setCategory(semester.getCategory().getCode());
			semesterForm.setSemesterId(semester.getId().toString());
			semesterForm.setPrefix(semester.getPrefix());
			semesterForm.setTelescope(semester.getTelescope());
			semesterForm.setAvailableTime(longDateFormat.format(semester
					.getAvailableDate()));
			semesterForm.setStartTime(longDateFormat.format(semester
					.getStartDate()));
			semesterForm.setSemester(semester.getSemester());
			semesterForm.setEndTime(longDateFormat
					.format(semester.getEndDate()));
			semesterForm.setDeadLineTime(longDateFormat.format(semester
					.getDeadLine()));
			semesterForm.setLastAssignedNumber(AstronConverter.toString(semester
					.getLastAssigned()));
			semesterForm.setImmediate(semester.isImmediate());
            semesterForm.setCommunityLabel(NorthStarConfiguration.
                    getCommunityDescription(semester.getCommunity()));
            semesterForm.setDeadLineDelay(semester.getDeadLineDelay().toString() );
            
            semesterForm.setSelectedParentId(AstronConverter.toString(semester.getParentId()) );
            
		}
		return mapping.findForward(Constants.SUCCESS);
	}

	protected String getCategory(List categories, Integer id)
			throws DatabaseException {
		if (id == null) {
			return northStarDelegate.getDefaultCategory().getCode();
		}
		Iterator categoriesIterator = categories.iterator();
		while (categoriesIterator.hasNext()) {
			Category category = (Category) categoriesIterator.next();
			if (category.getId().intValue() == id.intValue()) {
				return category.getCode();
			}
		}
		return null;

	}

	public static final Comparator SORT_LABEL_VALUE_BEAN = new Comparator() {
		public int compare(Object a, Object b) {
			LabelValueBean beanA = (LabelValueBean) a;
			LabelValueBean beanB = (LabelValueBean) b;
			return beanA.getLabel().compareToIgnoreCase(beanB.getLabel());
		}
	};
}
