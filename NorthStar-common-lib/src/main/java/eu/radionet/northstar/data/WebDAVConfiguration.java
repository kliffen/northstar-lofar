// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.data;

import nl.astron.util.AstronConverter;
import eu.radionet.northstar.data.entities.Proposal;

public class WebDAVConfiguration {
	protected static String url = null;
	protected static String redirectUrl = null;
	protected static String user = null;
	protected static String password = null;
	protected static String baseDir = null;
	public static final String READ_ACCESS = "read_access";	
	public static final String WRITE_ACCESS = "write_access";
	
	public static final String NORTHSTAR = "/roles/northstar";
	public static final String NORTHSTAR_PROPOSAL_ID = NORTHSTAR+ "/proposalid_";
	public static final String NORTHSTAR_REVIEW = "/roles/northstar-review";
	public static final String NORTHSTAR_REVIEW_PROPOSAL_ID = NORTHSTAR_REVIEW+ "/proposalid_";
	public static final String NORTHSTAR_REVIEW_SEMESTER_ID = NORTHSTAR_REVIEW+ "/semesterid_";	
	public static String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		WebDAVConfiguration.password = password;
	}
	public static String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		WebDAVConfiguration.url = url;
	}
	public static String getUser() {
		return user;
	}
	public void setUser(String user) {
		WebDAVConfiguration.user = user;
	}
	public static String getBaseDir() {
		return baseDir;
	}
	public void setBaseDir(String baseDir) {
		WebDAVConfiguration.baseDir = baseDir;
	}
	public static String getPdfLocation(Proposal proposal) {
		String telescope = proposal.getSemester().getTelescope().toLowerCase();
		String semester = proposal.getSemester().getSemester().toLowerCase();
		String community = proposal.getSemester().getCommunity().toLowerCase();
		String category = proposal.getSemester().getCategory().getCode()
				.toLowerCase();
		// String dir = catalinaHome + "/proposals/";
		String file = null;
		if (WebDAVConfiguration.baseDir.startsWith("/")){
			file = WebDAVConfiguration.baseDir;
		}else {
			file = "/" + WebDAVConfiguration.baseDir;
		}
		file += "/proposals/";
		file += telescope + "/";
		file += community + "/";
		file += semester + "/";
		file += category + "/";
		file += AstronConverter.removeNonWordCharacters(proposal.getCode()) + ".pdf";
		return file;
	}
	public static String getRedirectUrl() {
		return redirectUrl;
	}
	public void setRedirectUrl(String redirectUrl) {
		WebDAVConfiguration.redirectUrl = redirectUrl;
	}
}
