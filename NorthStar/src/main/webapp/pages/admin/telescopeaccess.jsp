<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />

	<html:form action="/processTelescopeAccess" method="POST">
<TABLE>
	<tr>
	
		<logic:equal name="telescopeAccessForm" property="hasMultipleAccess" value="true">
			
			<td>&nbsp;<br>
			
			Telescope: <br>
			<bean:define id="mytelescopelist" name="telescopeAccessForm" property="telescopeNameList"/>
			<html:select name="telescopeAccessForm" property="selectedTelescope" onchange="document.forms[0].submit()">
				<html:options collection="mytelescopelist" property="telescope" labelProperty="telescope"/>
			</html:select>
			
			</td>

		</logic:equal>
		<logic:equal name="telescopeAccessForm" property="hasMultipleAccess" value="false">
			<td>
			Telescope: <br>
			<bean:write name="telescopeAccessForm" property="selectedTelescope"/><br>
			</td>
		</logic:equal>
	</tr>
		
			<tr>
				<td> 
		 		</td>
			<tr>
				<td><br>
				Allowed Users: <br>
				<bean:define id="mynamelist" name="telescopeAccessForm" property="userNameList"/> 
				
				<html:select name="telescopeAccessForm" property="selectedUser" size="8">
					<html:options collection="mynamelist" property="id" labelProperty="lastName"/>		
				</html:select>
				</td>
			</tr>
			<tr>
				<td>&nbsp;
				<html:hidden property="selectedTelescope" styleId="selectedTelescope" />
				<html:hidden property="operation" styleId="operation" value="" />
				<html:submit value="Add" onclick= "document.getElementById('operation').value='adduser';" />
				<html:submit value="Invite" onclick= "document.getElementById('operation').value='invite';" />
				<html:submit value="Remove" onclick= "document.getElementById('operation').value='remove';" />
				</td>
			</tr>	

</TABLE>	

		</html:form>




