DROP DATABASE IF EXISTS `northstar`;
CREATE DATABASE `northstar`;
USE `northstar`;
DROP TABLE IF EXISTS `additionalissues`;
CREATE TABLE `additionalissues` (
  `id` int(11) NOT NULL,
  `linkedproposals` tinyint(1) NOT NULL default '0',
  `linkedproposalsspecifics` text,
  `relatedprojects` text,
  `previousprojects` text,
  `thesis` tinyint(1) NOT NULL default '0',
  `studentname` varchar(255) default NULL,
  `supervisorname` varchar(255) default NULL,
  `expectedcompletiondate` datetime default NULL,
  `datarequired` tinyint(1) NOT NULL default '0',
  `additionalremarks` text,
  `relatedpublications` text,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
CREATE TABLE `category` (
  `id` int(11) NOT NULL default '0',
  `code` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Table structure for table `complexbackend`
--

DROP TABLE IF EXISTS `complexbackend`;
CREATE TABLE `complexbackend` (
  `id` int(11) NOT NULL,
  `wsrtbackendid` int(11) default NULL,
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `wsrtbackend_IND` (`wsrtbackendid`)
) TYPE=MyISAM;

--
-- Table structure for table `darkgreybrighttime`
--

DROP TABLE IF EXISTS `darkgreybrighttime`;
CREATE TABLE `darkgreybrighttime` (
  `id` int(11) NOT NULL,
  `darktime` int(11) default NULL,
  `greytime` int(11) default NULL,
  `brighttime` int(11) default NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Table structure for table `invitation`
--

DROP TABLE IF EXISTS `invitation`;
CREATE TABLE `invitation` (
  `id` int(11) NOT NULL,
  `proposalid` int(11) NOT NULL default '0',
  `memberid` int(11) NOT NULL default '0',
  `acceptcode` char(20) NOT NULL default '',
  `invitationtime` timestamp NOT NULL,
  `userid` int(11) default NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Table structure for table `jcmtobservation`
--

DROP TABLE IF EXISTS `jcmtobservation`;
CREATE TABLE `jcmtobservation` (
  `id` int(11) NOT NULL,
  `observationid` int(11) default NULL,
  `intensity` float default NULL,
  `weatherquality` varchar(255) default NULL,
  `priority` int(11) default NULL,
  `backendsetup` varchar(255) default NULL,
  `pol1` tinyint(1) NOT NULL default '0',
  `pol2` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `observation_IND` (`observationid`)
) TYPE=MyISAM;

--
-- Table structure for table `jcmtobservingrequest`
--

DROP TABLE IF EXISTS `jcmtobservingrequest`;
CREATE TABLE `jcmtobservingrequest` (
  `id` int(11) NOT NULL,
  `observingrequestid` int(11) default NULL,
  `initialsemester` varchar(255) default NULL,
  `finalsemester` varchar(255) default NULL,
  `timeawardedprev` int(11) default NULL,
  `timerequestedfuture` int(11) default NULL,
  `requestedtime` int(11) default NULL,
  `abandreceiver` int(11) default NULL,
  `bbandreceiver` int(11) default NULL,
  `cbandreceiver` int(11) default NULL,
  `dbandreceiver` int(11) default NULL,
  `scubareceiver` int(11) default NULL,
  `otherreceiver` int(11) default NULL,
  `verydryweatherquality` int(11) default NULL,
  `dryweatherquality` int(11) default NULL,
  `mediumweatherquality` int(11) default NULL,
  `wetweatherquality` int(11) default NULL,
  `verywetweatherquality` int(11) default NULL,
  `lststart` time default NULL,
  `lstend` time default NULL,
  `flexiblescheduling` tinyint(1) NOT NULL default '0',
  `pol1` tinyint(1) NOT NULL default '0',
  `pol2` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `observingrequest_IND` (`observingrequestid`)
) TYPE=MyISAM;

--
-- Table structure for table `justification`
--

DROP TABLE IF EXISTS `justification`;
CREATE TABLE `justification` (
  `id` int(11) NOT NULL,
  `title` varchar(255) default NULL,
  `abstract` text,
  `scientificfilename` varchar(255) default NULL,
  `scientificfilepath` varchar(255) default NULL,
  `scientificfiledatetime` datetime default NULL,
  `scientificfilesize` int(11) default NULL,
  `technicaldetailsfilename` varchar(255) default NULL,
  `technicaldetailsfilepath` varchar(255) default NULL,
  `technicaldetailsfiledatetime` datetime default NULL,
  `technicaldetailsfilesize` int(11) default NULL,
  `figurefilename` varchar(255) default NULL,
  `figurefilepath` varchar(255) default NULL,
  `figurefiledatetime` datetime default NULL,
  `figurefilesize` int(11) default NULL,
  `memberid` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `member_IND` (`memberid`)
) TYPE=MyISAM;

--
-- Table structure for table `justificationfile`
--

DROP TABLE IF EXISTS `justificationfile`;
CREATE TABLE `justificationfile` (
  `id` int(11) NOT NULL,
  `justificationid` int(11) NOT NULL default '0',
  `uploadfile` longblob,
  `type` varchar(50) default NULL,
  PRIMARY KEY  (`id`),
  KEY `justification_IND` (`justificationid`)
) TYPE=MyISAM;

--
-- Table structure for table `member`
--

DROP TABLE IF EXISTS `member`;
CREATE TABLE `member` (
  `id` int(11) NOT NULL,
  `proposalid` int(11) default NULL,
  `indexid` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `proposal_IND` (`proposalid`)
) TYPE=MyISAM;

--
-- Table structure for table `memberrole`
--

DROP TABLE IF EXISTS `memberrole`;
CREATE TABLE `memberrole` (
  `id` int(11) NOT NULL,
  `memberid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `roleid` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `member_IND` (`memberid`),
  KEY `role_IND` (`roleid`)
) TYPE=MyISAM;

--
-- Table structure for table `nonregisteredmember`
--

DROP TABLE IF EXISTS `nonregisteredmember`;
CREATE TABLE `nonregisteredmember` (
  `id` int(11) NOT NULL,
  `memberid` int(11) default NULL,
  `name` varchar(255) default NULL,
  `affiliation` varchar(255) default NULL,
  `email` varchar(255) default NULL,
  `country` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `member` (`memberid`)
) TYPE=MyISAM;

--
-- Table structure for table `observation`
--

DROP TABLE IF EXISTS `observation`;
CREATE TABLE `observation` (
  `id` int(11) NOT NULL,
  `observingrequestid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `fieldname` varchar(255) default NULL,
  `totalduration` int(11) default NULL,
  `ra` double default NULL,
  `decl` double default NULL,
  `epoch` varchar(255) default NULL,
  `requiredschedconstraints` text,
  `preferredschedconstraints` text,
  `observationtype` varchar(10) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observingrequest_IND` (`observingrequestid`)
) TYPE=MyISAM;

--
-- Table structure for table `observingrequest`
--

DROP TABLE IF EXISTS `observingrequest`;
CREATE TABLE `observingrequest` (
  `id` int(11) NOT NULL,
  `proposalid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `categoryid` int(11) default NULL,
  `largeproposal` tinyint(1) NOT NULL default '0',
  `longtermproject` tinyint(1) NOT NULL default '0',
  `overallrequiredschedconstraints` text,
  `overallpreferredschedconstraints` text,
  `observingrequesttype` varchar(10) default NULL,
  PRIMARY KEY  (`id`),
  KEY `proposal_IND` (`proposalid`),
  KEY `category_IND` (`categoryid`)
) TYPE=MyISAM;

--
-- Table structure for table `proposal`
--

DROP TABLE IF EXISTS `proposal`;
CREATE TABLE `proposal` (
  `id` int(11) NOT NULL,
  `code` varchar(255) default NULL,
  `justificationid` int(11) default NULL,
  `additionalissuesid` int(11) default NULL,
  `currentstatusid` int(11) default '0',
  `semesterid` int(11) NOT NULL,
  `version` varchar(20) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `justification_IND` (`justificationid`),
  KEY `additionalissues_IND` (`additionalissuesid`),
  KEY `semester_IND` (`semesterid`)
) TYPE=MyISAM;

--
-- Table structure for table `proposallock`
--

DROP TABLE IF EXISTS `proposallock`;
CREATE TABLE `proposallock` (
  `id` int(11) NOT NULL,
  `proposalid` int(11) NOT NULL default '0',
  `sessionid` varchar(255) NOT NULL default '',
  `userid` int(11) NOT NULL default '0',
  `since` datetime NOT NULL default '0000-00-00 00:00:00',
  `lastaction` datetime NOT NULL default '0000-00-00 00:00:00',
  `valid` tinyint(1) NOT NULL default '0',
  `removerid` int(11) default NULL,
  `removedsince` datetime default NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Table structure for table `proposalstatus`
--

DROP TABLE IF EXISTS `proposalstatus`;
CREATE TABLE `proposalstatus` (
  `id` int(11) NOT NULL,
  `statusid` int(11) default NULL,
  `proposalid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `statustime` datetime NOT NULL default '0000-00-00 00:00:00',
  `userid` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `status_IND` (`statusid`),
  KEY `proposal_IND` (`proposalid`)
) TYPE=MyISAM;

--
-- Table structure for table `pumabackend`
--

DROP TABLE IF EXISTS `pumabackend`;
CREATE TABLE `pumabackend` (
  `id` int(11) NOT NULL,
  `wsrtbackendid` int(11) default NULL,
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `wsrtbackend_IND` (`wsrtbackendid`)
) TYPE=MyISAM;

--
-- Table structure for table `registeredmember`
--

DROP TABLE IF EXISTS `registeredmember`;
CREATE TABLE `registeredmember` (
  `id` int(11) NOT NULL,
  `memberid` int(11) default NULL,
  `userid` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `member` (`memberid`)
) TYPE=MyISAM;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `name` varchar(25) NOT NULL default '',
  `description` varchar(100) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `role_UNIQ` (`name`)
) TYPE=MyISAM;

--
-- Table structure for table `semester`
--

DROP TABLE IF EXISTS `semester`;
CREATE TABLE `semester` (
  `id` int(11) NOT NULL,
  `prefix` varchar(15) NOT NULL default '',
  `startdatetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `enddatetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `deadlinedatetime` datetime NOT NULL default '0000-00-00 00:00:00',
  `lastassigned` int(11) NOT NULL default '0',
  `telescope` varchar(255) NOT NULL default '',
  `categoryid` int(11) default NULL,
  `semester` varchar(255) NOT NULL,
  `community` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`),
  KEY `category_IND` (`categoryid`)
) TYPE=MyISAM;

--
-- Table structure for table `simplecontinuumbackend`
--

DROP TABLE IF EXISTS `simplecontinuumbackend`;
CREATE TABLE `simplecontinuumbackend` (
  `id` int(11) NOT NULL,
  `wsrtbackendid` int(11) default NULL,
  `polarisationproducts` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `wsrtbackend_IND` (`wsrtbackendid`)
) TYPE=MyISAM;

--
-- Table structure for table `simplelinebackend`
--

DROP TABLE IF EXISTS `simplelinebackend`;
CREATE TABLE `simplelinebackend` (
  `id` int(11) NOT NULL,
  `wsrtbackendid` int(11) default NULL,
  `polarisationproducts` varchar(255) default NULL,
  `ivcbandsused` int(11) default NULL,
  `bandwidth` double default NULL,
  `spectralchannels` int(11) default NULL,
  `taper` varchar(255) default NULL,
  `freqspecificationmode` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `wsrtbackend_IND` (`wsrtbackendid`)
) TYPE=MyISAM;

--
-- Table structure for table `status`
--

DROP TABLE IF EXISTS `status`;
CREATE TABLE `status` (
  `id` int(11) NOT NULL,
  `code` varchar(15) NOT NULL default '',
  `type` varchar(15) NOT NULL default '',
  `description` varchar(100) default NULL,
  PRIMARY KEY  (`id`),
  UNIQUE KEY `status_UNIQ` (`code`,`type`)
) TYPE=MyISAM;

--
-- Table structure for table `velocity`
--

DROP TABLE IF EXISTS `velocity`;
CREATE TABLE `velocity` (
  `id` int(11) NOT NULL,
  `simplelinebackendid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `frequency` double default NULL,
  `velocity` double default NULL,
  `type` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Table structure for table `vlbibackend`
--

DROP TABLE IF EXISTS `vlbibackend`;
CREATE TABLE `vlbibackend` (
  `id` int(11) NOT NULL,
  `wsrtbackendid` int(11) default NULL,
  `addingConfiguration` text,
  `description` text,
  PRIMARY KEY  (`id`),
  KEY `wsrtbackend_IND` (`wsrtbackendid`)
) TYPE=MyISAM;

--
-- Table structure for table `whtintobservation`
--

DROP TABLE IF EXISTS `whtintobservation`;
CREATE TABLE `whtintobservation` (
  `id` int(11) NOT NULL,
  `observationid` int(11) default NULL,
  `mag` varchar(255) default NULL,
  `colour` varchar(255) default NULL,
  `darkgreybright` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observation_IND` (`observationid`)
) TYPE=MyISAM;

--
-- Table structure for table `whtintobservingrequest`
--

DROP TABLE IF EXISTS `whtintobservingrequest`;
CREATE TABLE `whtintobservingrequest` (
  `id` int(11) NOT NULL,
  `observingrequestid` int(11) default NULL,
  `initialsemester` varchar(255) default NULL,
  `finalsemester` varchar(255) default NULL,
  `timeawardedprevid` int(11) default NULL,
  `timerequestedfutureid` int(11) default NULL,
  `longnightweek` varchar(255) default NULL,
  `requestedtimeid` int(11) default NULL,
  `minimumtimeid` int(11) default NULL,
  `requestNightWeek` varchar(255) default NULL,
  `supportAstronomer` varchar(255) default NULL,
  `summaryBackupProgramme` varchar(255) default NULL,
  `experienceLevel` varchar(255) default NULL,
  `focalStation` varchar(255) default NULL,
  `instrument` varchar(255) default NULL,
  `detectors` varchar(255) default NULL,
  `gratingsFilters` varchar(255) default NULL,
  `servicemode` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`id`),
  KEY `observingrequest_IND` (`observingrequestid`),
  KEY `timeawarededprev_IND` (`timeawardedprevid`),
  KEY `timerequestedfuture_IND` (`timerequestedfutureid`),
  KEY `requestedtime_IND` (`requestedtimeid`),
  KEY `minimumtime_IND` (`minimumtimeid`)
) TYPE=MyISAM;

--
-- Table structure for table `wsrtbackend`
--

DROP TABLE IF EXISTS `wsrtbackend`;
CREATE TABLE `wsrtbackend` (
  `id` int(11) NOT NULL default '0',
  `name` varchar(255) default NULL,
  PRIMARY KEY  (`id`)
) TYPE=MyISAM;

--
-- Table structure for table `wsrtobservation`
--

DROP TABLE IF EXISTS `wsrtobservation`;
CREATE TABLE `wsrtobservation` (
  `id` int(11) NOT NULL,
  `observationid` int(11) default NULL,
  `wsrtbackendid` int(11) default NULL,
  `positionmosaic` tinyint(1) NOT NULL default '0',
  `arrayconfiguration` varchar(255) default NULL,
  `observingbandorreceiver` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observation_IND` (`observationid`),
  KEY `wsrtbackend_IND` (`wsrtbackendid`)
) TYPE=MyISAM;

--
-- Table structure for table `wsrtobservingrequest`
--

DROP TABLE IF EXISTS `wsrtobservingrequest`;
CREATE TABLE `wsrtobservingrequest` (
  `id` int(11) NOT NULL,
  `observingrequestid` int(11) default NULL,
  `initialsemester` varchar(255) default NULL,
  `finalsemester` varchar(255) default NULL,
  `timeawardedprev` int(11) default NULL,
  `timerequestedfuture` int(11) default NULL,
  `requestedtime` int(11) default NULL,
  PRIMARY KEY  (`id`),
  KEY `observingrequest_IND` (`observingrequestid`)
) TYPE=MyISAM;

