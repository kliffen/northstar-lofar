// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.observingrequest;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.control.proposal.ProposalForm;

public class ObservingRequestForm extends ProposalForm {

    protected boolean maxNumberOfObservationsReached = false;
    protected String selectedObservation=null;
    
    protected String overallRequiredSchedConstraints = null;
    protected boolean enableCombinedOutputProductSelection = false;
    protected String overallPreferredSchedConstraints = null;

    protected String[] observationSummaryLabels = new String[0];
    protected List observationSummaryValues = new ArrayList();
	protected String newObservationButton = null;
    protected String editObservationButton = null;
    protected String commitObservationButton = null;
    protected String cancelObservationButton = null;
    protected String copyObservationButton = null;

    protected String deleteObservationButton = null;
    protected int selectedObservationId = 0;
    protected String newObservationFileButton = null;
	
    protected boolean displayChildren = false;
    protected boolean displaySelectParent = false;
    protected List children = new ArrayList();
    protected List parents = new ArrayList();
    protected String selectedParent = null;
    
    protected String selectedPipeline = null;
    protected int selectedPipelineId = 0;
    protected String newPipelineButton = null;
    protected String editPipelineButton = null;
    protected String deletePipelineButton = null;
    protected String copyPipelineButton = null;
    protected boolean displayPipeline = false;
    protected boolean displayCommitPipeline = false;
    protected String commitPipelineButton = null;
    protected String cancelPipelineButton = null;
	public String getOverallPreferredSchedConstraints() {
		return overallPreferredSchedConstraints;
	}

	public void setOverallPreferredSchedConstraints(
			String overallPreferredSchedConstraints) {
		this.overallPreferredSchedConstraints = overallPreferredSchedConstraints;
	}

	public String getOverallRequiredSchedConstraints() {
		return overallRequiredSchedConstraints;
	}

	public void setOverallRequiredSchedConstraints(
			String overallRequiredSchedConstraints) {
		this.overallRequiredSchedConstraints = overallRequiredSchedConstraints;
	}

    public void setDeleteObservationButton(int index, String value) {
        selectedObservationId = index;
        deleteObservationButton = value;
    }

    public String getDeleteObservationButton(int index) {
        return deleteObservationButton;
    }

    public void setCopyObservationButton(int index, String value) {
        selectedObservationId = index;
        copyObservationButton = value;
    }

    public String getCopyObservationButton(int index) {
        return copyObservationButton;
    }

    /**
     * @return Returns the selectedObservationId.
     */
    public int getSelectedObservationId() {
        return selectedObservationId;
    }

   /**
     * @return Returns the newObservationFileButton.
     */
    public String getNewObservationFileButton()
    {
        return newObservationFileButton;
    }
    /**
     * @param newObservationFileButton The newObservationFileButton to set.
     */
    public void setNewObservationFileButton(String newObservationFileButton)
    {
        this.newObservationFileButton = newObservationFileButton;
    }


	public String[] getObservationSummaryLabels() {
		return observationSummaryLabels;
	}

	public void setObservationSummaryLabels(String[] observationSummaryLabels) {
		this.observationSummaryLabels = observationSummaryLabels;
	}

	public List getObservationSummaryValues() {
		return observationSummaryValues;
	}

	public void setObservationSummaryValues(List observationSummaryValues) {
		this.observationSummaryValues = observationSummaryValues;
	}

	public void setSelectedObservationId(int selectedObservationId) {
		this.selectedObservationId = selectedObservationId;
	}

    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
        
    }

	public String getNewObservationButton() {
		return newObservationButton;
	}

	public void setNewObservationButton(String newObservationButton) {
		this.newObservationButton = newObservationButton;
	}

	public boolean isMaxNumberOfObservationsReached() {
		return maxNumberOfObservationsReached;
	}

	public void setMaxNumberOfObservationsReached(
			boolean maxNumberOfObservationsReached) {
		this.maxNumberOfObservationsReached = maxNumberOfObservationsReached;
	}

	public String getSelectedObservation() {
		return selectedObservation;
	}

	public void setSelectedObservation(String selectedObservation) {
		this.selectedObservation = selectedObservation;
	}

	public String getCommitObservationButton() {
		return commitObservationButton;
	}

	public void setCommitObservationButton(String commitObservationButton) {
		this.commitObservationButton = commitObservationButton;
	}

	public String getCancelObservationButton() {
		return cancelObservationButton;
	}

	public void setCancelObservationButton(String cancelObservationButton) {
		this.cancelObservationButton = cancelObservationButton;
	}

	public boolean isDisplayChildren() {
		return displayChildren;
	}

	public void setDisplayChildren(boolean displayChildren) {
		this.displayChildren = displayChildren;
	}

	public boolean isDisplaySelectParent() {
		return displaySelectParent;
	}

	public void setDisplaySelectParent(boolean displaySelectParent) {
		this.displaySelectParent = displaySelectParent;
	}

	public List getChildren() {
		return children;
	}

	public void setChildren(List children) {
		this.children = children;
	}

	public List getParents() {
		return parents;
	}

	public void setParents(List parents) {
		this.parents = parents;
	}

	public String getSelectedParent() {
		return selectedParent;
	}

	public void setSelectedParent(String selectedParent) {
		this.selectedParent = selectedParent;
	}

	public String getSelectedPipeline() {
		return selectedPipeline;
	}

	public void setSelectedPipeline(String selectedPipeline) {
		this.selectedPipeline = selectedPipeline;
	}

	public String getNewPipelineButton() {
		return newPipelineButton;
	}

	public void setNewPipelineButton(String newPipelineButton) {
		this.newPipelineButton = newPipelineButton;
	}

	public void setEditObservationButton(int index, String value) {
        selectedObservationId = index;
        editObservationButton = value;
    }

    public String getEditObservationButton(int index) {
        return editObservationButton;
    }
	
    public String getEditPipelineButton(int index) {
		return editPipelineButton;
	}

	public void setEditPipelineButton(int index, String value) {
		this.selectedPipelineId = index;
		//this.selectedObservationId = index;
		this.editPipelineButton = value;
	}

	public void setDeletePipelineButton(int index, String value) {
        selectedPipelineId = index;
        deletePipelineButton = value;
    }

    public String getDeletePipelineButton(int index) {
        return deletePipelineButton;
    }

	public void setCopyPipelineButton(int index, String value) {
        selectedPipelineId = index;
        copyPipelineButton = value;
    }

    public String getCopyPipelineButton(int index) {
        return copyPipelineButton;
    }
    
    public boolean isDisplayPipeline() {
		return displayPipeline;
	}

	public void setDisplayPipeline(boolean displayPipeline) {
		this.displayPipeline = displayPipeline;
	}

	public boolean isDisplayCommitPipeline() {
		return displayCommitPipeline;
	}

	public void setDisplayCommitPipeline(boolean displayCommitPipeline) {
		this.displayCommitPipeline = displayCommitPipeline;
	}

	public String getCommitPipelineButton() {
		return commitPipelineButton;
	}

	public void setCommitPipelineButton(String commitPipelineButton) {
		this.commitPipelineButton = commitPipelineButton;
	}
	
		
	public String getCancelPipelineButton() {
		return cancelPipelineButton;
	}

	public void setCancelPipelineButton(String cancelPipelineButton) {
		this.cancelPipelineButton = cancelPipelineButton;
	}

	public int getSelectedPipelineId() {
		return selectedPipelineId;
	}

	public void setSelectedPipelineId(int selectedPipelineId) {
		this.selectedPipelineId = selectedPipelineId;
	}
	
	public boolean isEnableCombinedOutputProductSelection() {
		return enableCombinedOutputProductSelection;
	}

	public void setEnableCombinedOutputProductSelection(
			boolean enableCombinedOutputProductSelection) {
		this.enableCombinedOutputProductSelection = enableCombinedOutputProductSelection;
	}
}
