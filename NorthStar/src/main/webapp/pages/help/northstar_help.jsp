<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@page import="eu.radionet.northstar.business.configuration.NorthStarConfiguration" %>

<html:html>
<head>
	<link rel=stylesheet href="<%=request.getContextPath()%>/css/proposalLayout.css" type="text/css">
    <title>NorthStar Help File</title>
</head>

<body>

<jsp:include page="../layout/box_header_noFullWidth.jsp" />

<jsp:include page="<%=NorthStarConfiguration.getNorthstarHelpUri() %>"/>

<jsp:include page="../layout/box_footer_noFullWidth.jsp" />

</body>
</html:html>