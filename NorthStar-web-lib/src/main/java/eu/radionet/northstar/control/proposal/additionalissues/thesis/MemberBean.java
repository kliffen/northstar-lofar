// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * MemberBean.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: MemberBean.java,v 1.1 2006-05-03 10:17:22 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.additionalissues.thesis;

import java.io.Serializable;

import eu.radionet.northstar.data.entities.Member;

/**
 * The MemberBean provides
 *
 * @author Bastiaan Verhoef
 *
 */
public class MemberBean implements Serializable{

    private Member member = null;
    private Integer id = null;

    /**
     * @return Returns the id.
     */
    public String getId() {
        return id.toString();
    }
    public void setId(Integer id){
        this.id=id;
    }
    /**
     * @return Returns the member.
     */
    public Member getMember() {
        return member;
    }
    /**
     * @param member The member to set.
     */
    public void setMember(Member member) {
        this.member = member;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return member.getName();
    }

}
