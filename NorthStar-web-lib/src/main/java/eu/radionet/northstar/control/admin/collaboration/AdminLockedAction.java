// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.collaboration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.Proposal;

public abstract class AdminLockedAction extends Action {
	protected CollaborationDelegate collaborationDelegate = null;

	protected ProposalDelegate proposalDelegate = null;

	protected Proposal lockedProposal = null;
	protected NorthStarDelegate northStarDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		Integer proposalId = AstronConverter.toInteger(request.getParameter("proposalId"));
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		collaborationDelegate = CollaborationDelegate.getInstance();
		northStarDelegate = NorthStarDelegate.getInstance();



		lockedProposal = northStarDelegate.getProposal(proposalId);
		proposalDelegate = ProposalDelegateFactory
				.getProposalDelegate(lockedProposal);


		/*
		 * checks if another session locks the proposal, if not it locks
		 * automaticly the proposal
		 */
		if (!collaborationDelegate.checkAndUpdateProposalLock(lockedProposal.getId(),
				ownUserAccount, session.getId())) {

			return ParameterAppender.append(mapping
					.findForward(Constants.LOCKED_PROPOSAL), "proposalId",
					AstronConverter.toString(lockedProposal.getId()));
		}
		return lockedExecute(mapping, form, request, response);

	}

	protected ActionForward cancel(Integer proposalId, ActionMapping mapping,
			HttpServletRequest request, UserAccount ownUserAccount)
			throws DatabaseException {
		removeLock(proposalId, request, ownUserAccount);
		return mapping.findForward(Constants.DISCARD);
	}

	protected void removeLock(Integer proposalId,HttpServletRequest request,
			UserAccount ownUserAccount) throws DatabaseException {
		collaborationDelegate.unlockProposal(proposalId, ownUserAccount, request
				.getSession().getId());
	}

	public abstract ActionForward lockedExecute(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception;
}
