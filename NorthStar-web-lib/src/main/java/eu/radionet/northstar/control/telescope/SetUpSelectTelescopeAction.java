// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SetUpUploadAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: SetUpSelectTelescopeAction.java,v 1.3 2006-08-04 09:45:01 holties Exp $
 *
 */
package eu.radionet.northstar.control.telescope;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.Telescope;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.Semester;

/**
 * SetUpSelectTelescopeAction
 * 
 * @author Nico Vermaas, Bastiaan Verhoef
 */
public class SetUpSelectTelescopeAction extends Action {
	private Log log = LogFactory.getLog(this.getClass());

	private NorthStarDelegate northStarDelegate = null;

	public static final String SEMESTER_DATETIME_FORMAT = "dd MMMMMMMMMMMM yyyy HH:mm:ss z";

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SelectTelescopeForm telescopeForm = (SelectTelescopeForm) form;

		northStarDelegate = NorthStarDelegate.getInstance();
		/*
		 * clean old semesterId
		 */
		if (AstronValidator.isBlankOrNull(telescopeForm.getSelectedTelescope())
				|| AstronValidator.isBlankOrNull(telescopeForm
						.getSelectedCommunity())
				|| AstronValidator.isBlankOrNull(telescopeForm
						.getSelectedCategory())) {
			telescopeForm.setSelectedSemesterId(null);
		}
		fillTelescopes(telescopeForm);
		/*
		 * retrieve list of all communities from semester tabel
		 */
		if (!AstronValidator.isBlankOrNull(telescopeForm.getSelectedTelescope())) 
        {
            fillCommunities(telescopeForm);
		}

		/*
		 * if telescope is selected, retrieve available semesters
		 */
		if (!AstronValidator.isBlankOrNull(telescopeForm.getSelectedCommunity())) {
			fillCategories(telescopeForm);

			/*
			 * if semester is selected, retrieve categories
			 */
			if (!AstronValidator.isBlankOrNull(telescopeForm.getSelectedCategory())) 
            {
			    fillSemesters(telescopeForm);
			}
		}
 		return mapping.findForward(Constants.SUCCESS);
	}

	protected LabelValueBean getDummy() {
		return new LabelValueBean(Constants.NONE_SPECIFIED_LABEL,
				Constants.NONE_SPECIFIED_VALUE);
	}

	protected void fillTelescopes(SelectTelescopeForm telescopeForm) {
		Iterator telescopes = NorthStarConfiguration.getTelescopes().iterator();
		Set telescopesSet = new TreeSet(SORT_LABEL_VALUE_BEAN);
		while (telescopes.hasNext()) {
			Telescope telescope = (Telescope) telescopes.next();
			telescopesSet.add(new LabelValueBean(telescope.getDescription(),
					telescope.getName()));
		}
        /*
         * if only one telescope, do not show select box
         */
        if (telescopesSet.size() == 1) {
            telescopeForm.setNoFacilityChoice(true);
            LabelValueBean first = getFirstItem(telescopesSet);
            telescopeForm.setSelectedTelescope(first.getValue());
            telescopeForm.setSelectedTelescopeLabel(first.getLabel());
        } else {
            telescopesSet.add(getDummy());
        }

		telescopeForm.setTelescopes(telescopesSet);
	}

	protected void fillCategories(SelectTelescopeForm telescopeForm) throws DatabaseException {
        List semesters = northStarDelegate.getAvailableSemesters(
                telescopeForm.getSelectedTelescope(), telescopeForm
                        .getSelectedCommunity());        
		Set categories = new TreeSet();
		Iterator iterator = semesters.iterator();
		while (iterator.hasNext()) {
			Semester semester = (Semester) iterator.next();
			categories.add(new LabelValueBean(semester.getCategory().getCode(),
					semester.getCategory().getCode()));
		}
        /*
         * when empty set special boolean
         */
        if (categories.isEmpty())
        {
            telescopeForm.setAcceptsProposals(false);
        }
        else
        {
            
            
    		/*
    		 * if no category is selected, choose default category 
    		 */
    		if (AstronValidator.isBlankOrNull(telescopeForm.getSelectedCategory()) 
                    || !validCategory(semesters, telescopeForm.getSelectedCategory()))
            {
                LabelValueBean first = getFirstItem(categories);
                telescopeForm.setSelectedCategory(first.getValue());        
                telescopeForm.setSelectedCategoryLabel(first.getLabel());        
            }
    		/*
    		 * if only one category, do not show select box
    		 */
    		if (categories.size() == 1) {
    			telescopeForm.setNoCategoryChoice(true);
    			LabelValueBean first = getFirstItem(categories);
    			telescopeForm.setSelectedCategory(first.getValue());
    			telescopeForm.setSelectedCategoryLabel(first.getLabel());
    		}
    		telescopeForm.setCategories(categories);
        }
	}

    protected void fillCommunities(SelectTelescopeForm telescopeForm) throws DatabaseException
    {
        Set communitiesSet = new TreeSet(SORT_LABEL_VALUE_BEAN);
        List semesters = northStarDelegate
                .getAvailableSemesters(telescopeForm.getSelectedTelescope());
        Iterator semesterIterator = semesters.iterator();
        while (semesterIterator.hasNext()) {
            Semester semester = (Semester) semesterIterator.next();
            communitiesSet.add(new LabelValueBean(NorthStarConfiguration
                    .getCommunityDescription(semester.getCommunity()),
                    semester.getCommunity()));
        }

        /*
         * when empty set special boolean
         */
        if (communitiesSet.isEmpty())
        {
            telescopeForm.setAcceptsProposals(false);
        }
        else
        {
            
            /*
             * No community set yet. Use the first one in the community list.
             */
            if (AstronValidator.isBlankOrNull(telescopeForm.getSelectedCommunity())
                    || !validCommunity(semesters, telescopeForm.getSelectedCommunity() ))
            {
                LabelValueBean first = getFirstItem(communitiesSet);
                telescopeForm.setSelectedCommunityLabel(first.getLabel());
                telescopeForm.setSelectedCommunity(first.getValue());                
            }
            
            /*
             * if only one community, do not show select box
             */
            if (communitiesSet.size() == 1) {
                telescopeForm.setNoCommunityChoice(true);
                LabelValueBean first = getFirstItem(communitiesSet);
                telescopeForm.setSelectedCommunityLabel(first.getLabel());
                telescopeForm.setSelectedCommunity(first.getValue());
            }
        }
        telescopeForm.setCommunities(communitiesSet);
    }
    
    protected void fillSemesters(SelectTelescopeForm telescopeForm) throws DatabaseException
    {
        List semesters = northStarDelegate.getAvailableSemesters(
                telescopeForm.getSelectedTelescope(), telescopeForm
                        .getSelectedCommunity(), telescopeForm
                        .getSelectedCategory());
        Set semestersSet = new TreeSet(SORT_LABEL_VALUE_BEAN);
       Iterator iterator = semesters.iterator();
        while (iterator.hasNext()) 
        {
            Semester semester = (Semester) iterator.next();
            semestersSet.add(new LabelValueBean(semester.getSemester(),
                    semester.getId().toString()));
        }
            
        /*
         * if only one semester, do not show select box
         */
        if (semestersSet.size() == 1) {
            telescopeForm.setNoSemesterChoice(true);
            LabelValueBean first = getFirstItem(semestersSet);
            telescopeForm.setSelectedSemesterLabel(first.getLabel());
            telescopeForm.setSelectedSemesterId(first.getValue());
        }
        /*
         * if more than one semester
         */
        else if (semestersSet.size() > 1){
            /*
             * if no semester selected, select first
             */
            if (!AstronValidator.isPositiveInt(telescopeForm
                    .getSelectedSemesterId())){
                LabelValueBean first = getFirstItem(semestersSet);
                telescopeForm.setSelectedSemesterId(first.getValue());
            }
            /*
             * if semester selected, but not exists in list of available semester, select first
             */
            else {
                Integer semesterId = AstronConverter.toInteger(telescopeForm.getSelectedSemesterId());
                if (!validSemester(semesters,semesterId)){
                    LabelValueBean first = getFirstItem(semestersSet);
                    telescopeForm.setSelectedSemesterId(first.getValue());
                }
            }
        }

        /*
         * if semester is selected
         */
        if (AstronValidator.isPositiveInt(telescopeForm
                .getSelectedSemesterId())) {
            Semester selectedSemester = northStarDelegate
                    .getSemester(AstronConverter.toInteger(telescopeForm
                            .getSelectedSemesterId()));
            SimpleDateFormat longDateFormat = new SimpleDateFormat();
            longDateFormat.applyPattern(SEMESTER_DATETIME_FORMAT);
            longDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
            telescopeForm.setDeadLineTime(longDateFormat
                    .format(selectedSemester.getDeadLine()));
            telescopeForm.setImmediate(selectedSemester.isImmediate());
        }
        telescopeForm.setSemesters(semestersSet);        
    }

    
	public static final Comparator SORT_LABEL_VALUE_BEAN = new Comparator() {
		public int compare(Object a, Object b) {
			LabelValueBean beanA = (LabelValueBean) a;
			LabelValueBean beanB = (LabelValueBean) b;
			return beanA.getLabel().compareToIgnoreCase(beanB.getLabel());
		}
	};

	protected LabelValueBean getFirstItem(Collection collection) 
    {
	    Iterator it = collection.iterator();
        if (it.hasNext())
        {
            return ((LabelValueBean) it.next());
        }
        else
        {
            log.warn("Collection is empty");
            return null;
        }
	}

	protected boolean validSemester(List semesters, Integer semesterId) {
		Iterator iterator = semesters.iterator();
		while (iterator.hasNext()) {
			Semester semester = (Semester) iterator.next();
			if (semester.getId().intValue() == semesterId.intValue()) {
				return true;
			}
		}
		return false;
	}
    
    protected boolean validCategory(List semesters, String selectedCategory)
    {
        Iterator iterator = semesters.iterator();
        while (iterator.hasNext()) {
            Semester semester = (Semester) iterator.next();
            if (semester.getCategory().getCode().equals(selectedCategory)) 
            {
                return true;
            }
        }
        return false;
    }

    protected boolean validCommunity(List semesters, String selectedCommunity)
    {
        Iterator iterator = semesters.iterator();
        while (iterator.hasNext()) {
            Semester semester = (Semester) iterator.next();
            if (semester.getCommunity().equals(selectedCommunity)) 
            {
                return true;
            }
        }
        return false;
    }
    
    
    
}
