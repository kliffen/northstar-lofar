// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.xml;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.util.XMLConverter;

public class ProcessXMLProposalAction extends Action {
	static private final int MAX_AGE_IN_SECONDS = 600;


	private NorthStarDelegate northStarDelegate = null;



	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		XMLProposalForm xmlProposalForm = (XMLProposalForm) form;
		String[] statusses = null;
		String[] categories = null;
		String[] telescopes = null;
		String[] semesters = null;
		String[] communities = null;
		if (!AstronValidator.isBlankOrNull(xmlProposalForm.getStatus())){
			statusses = new String[]{xmlProposalForm.getStatus()};
		}
		if (!AstronValidator.isBlankOrNull(xmlProposalForm.getCategory())){
			categories = new String[]{xmlProposalForm.getCategory()};
		}
		if (!AstronValidator.isBlankOrNull(xmlProposalForm.getTelescope())){
			telescopes = new String[]{xmlProposalForm.getTelescope()};
		}		
		if (!AstronValidator.isBlankOrNull(xmlProposalForm.getSemester())){
			semesters = new String[]{xmlProposalForm.getSemester()};
		}	
		if (!AstronValidator.isBlankOrNull(xmlProposalForm.getCommunity())){
			communities = new String[]{xmlProposalForm.getCommunity()};
		}			
		northStarDelegate = NorthStarDelegate.getInstance();
		List proposals = northStarDelegate.getProposals(statusses, categories,telescopes, semesters,communities);
		downloadXML(proposals, response);


		return null;
	}




	protected void downloadXML(List proposals, HttpServletResponse response)
			throws DatabaseException, IOException,
			InvalidConfigurationException, ParserConfigurationException {
		String fileName = "proposals.xml";
		response.setContentType("application/xml");
		response.setHeader("Content-disposition", "attachment; filename="
				+ fileName);
		response.setHeader("Cache-Control", "max-age=" + MAX_AGE_IN_SECONDS);
		XMLConverter xmlConverter = XMLConverter.getInstance();
		xmlConverter
				.convertProposals2XML(proposals, response.getOutputStream());
		response.getOutputStream().flush();
	}
}
