<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processSubmitDetailsProposal" method="GET">

<TABLE>
	<TR>
		<TD>
			<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
			Your proposal with title '<bean:write name="submitDetailsForm" property="proposalTitle"/>' is submitted!!<BR>
			Your proposal gets the project id <B><bean:write name="submitDetailsForm" property="proposalCode"/></B>.<BR>
			A confirmation is sent to the following email address : <B><bean:write name="submitDetailsForm" property="email"/></B>.
			<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD align="center"><html:submit property="okButton" styleClass="list_accept_yes" title="Ok">Ok</html:submit>
		</TD>
	</TR>

</TABLE>
</html:form>
