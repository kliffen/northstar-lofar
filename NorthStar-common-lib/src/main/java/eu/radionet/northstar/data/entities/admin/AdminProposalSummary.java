// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.data.entities.admin;

import java.io.Serializable;

import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.ProposalStatus;
import eu.radionet.northstar.data.entities.Semester;

public class AdminProposalSummary implements Serializable {
	protected Integer id = null;
	protected Member pi = null;
	protected Member contactAuthor = null;
	protected String code = null;
	protected String title = null;
	protected ProposalStatus currentStatus = null;
	protected Semester semester = null;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Member getContactAuthor() {
		return contactAuthor;
	}
	public void setContactAuthor(Member contactAuthor) {
		this.contactAuthor = contactAuthor;
	}
	public ProposalStatus getCurrentStatus() {
		return currentStatus;
	}
	public void setCurrentStatus(ProposalStatus currentStatus) {
		this.currentStatus = currentStatus;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Member getPi() {
		return pi;
	}
	public void setPi(Member pi) {
		this.pi = pi;
	}
	public Semester getSemester() {
		return semester;
	}
	public void setSemester(Semester semester) {
		this.semester = semester;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
