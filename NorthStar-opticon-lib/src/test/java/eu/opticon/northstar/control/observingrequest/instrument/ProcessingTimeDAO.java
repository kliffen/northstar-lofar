package eu.opticon.northstar.control.observingrequest.instrument;

import java.util.List;

public class ProcessingTimeDAO {
	String mode;
	String flaggingStrategy;
	String antenna;
	boolean isDemixing;
	List<String> demixingSources;
	int totalStation;
	int totalTargetSubband;
	double totalObservingDuration;
	double fieldOfView;
	double uvRange;
	double baselineFactor;
	String processingMode;
	
	
	public ProcessingTimeDAO(String mode, String flaggingStrategy,
			String antenna, boolean isDemixing, List<String> demixingSources,
			int totalStation, int totalTargetSubband,
			double totalObservingDuration, double fieldOfView, double uvRange,
			double baselineFactor, String processingMode) {
		super();
		this.mode = mode;
		this.flaggingStrategy = flaggingStrategy;
		this.antenna = antenna;
		this.isDemixing = isDemixing;
		this.demixingSources = demixingSources;
		this.totalStation = totalStation;
		this.totalTargetSubband = totalTargetSubband;
		this.totalObservingDuration = totalObservingDuration;
		this.fieldOfView = fieldOfView;
		this.uvRange = uvRange;
		this.baselineFactor = baselineFactor;
		this.processingMode = processingMode;
	}


	public String getMode() {
		return mode;
	}


	public String getFlaggingStrategy() {
		return flaggingStrategy;
	}


	public String getAntenna() {
		return antenna;
	}


	public boolean isDemixing() {
		return isDemixing;
	}


	public List<String> getDemixingSources() {
		return demixingSources;
	}


	public int getTotalStation() {
		return totalStation;
	}


	public int getTotalTargetSubband() {
		return totalTargetSubband;
	}


	public double getTotalObservingDuration() {
		return totalObservingDuration;
	}


	public double getFieldOfView() {
		return fieldOfView;
	}


	public double getUvRange() {
		return uvRange;
	}


	public double getBaselineFactor() {
		return baselineFactor;
	}


	public String getProcessingMode() {
		return processingMode;
	}
	
	
	
}
