// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.deadline;

import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import eu.radionet.northstar.business.util.XMLConverter;
import eu.radionet.northstar.data.WebDAVConfiguration;

public class WebDAVConfigurator {
	private Log log = LogFactory.getLog(this.getClass());

	protected static final String REPOSITORY = "repository";

	protected static final String URL = "url";

	protected static final String REDIRECT_URL = "redirect-url";
	
	protected static final String USER = "user";

	protected static final String PASSWORD = "password";
	
	protected static final String BASEDIR = "basedir";
	public WebDAVConfiguration getConfiguration(String string) throws Exception {

		XMLConverter converter =XMLConverter.getInstance();
		return getConfiguration(converter.convertStringToDocument(string));
	}
	public WebDAVConfiguration getConfiguration(Document document) throws Exception {

		/*
		 * search for project
		 */
		NodeList elements = document.getElementsByTagName(REPOSITORY);
		for (int i = 0; i < elements.getLength(); i++) {
			Node node = (Node) elements.item(i);
			// log.info("Node: " + node.getNodeName());
			return parseRepositoryNode(node);
		}
		return null;
	}

	public WebDAVConfiguration parseRepositoryNode(Node repositoryNode) throws Exception {
		WebDAVConfiguration configuration = new WebDAVConfiguration();
		for (int i = 0; i < repositoryNode.getChildNodes().getLength(); i++) {
			Node nodeChild = repositoryNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, URL)) {
					configuration.setUrl(getValue(nodeChild));
				}else if (equal(nodeChild, REDIRECT_URL)) {
					configuration.setRedirectUrl(getValue(nodeChild));
				}else if (equal(nodeChild, USER)) {
					configuration.setUser(getValue(nodeChild));
				}else if (equal(nodeChild, PASSWORD)) {
					configuration.setPassword(getValue(nodeChild));
				} else if (equal(nodeChild, BASEDIR)) {
					configuration.setBaseDir(getValue(nodeChild));
				}   
			}

		}
		return configuration;

	}

	/**
	 * The getValue method returns the value of an node
	 * 
	 * @param node
	 * @return
	 */
	protected String getValue(Node node) {
		String value = null;
		if (node.getFirstChild() != null) {
			value = node.getFirstChild().getNodeValue();
			if (log.isDebugEnabled()) {
				log.debug("Node: " + node.getNodeName() + " value: " + value);
			}
		}
		return value;
	}

	/**
	 * The equal method compares if an node has the given name
	 * 
	 * @param node
	 * @param nodeName
	 * @return
	 */
	protected boolean equal(Node node, String nodeName) {
		return node.getNodeName().equals(nodeName);
	}
}
