// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SetUpUploadAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: SetUpLockProposalAction.java,v 1.2 2006-05-11 12:14:38 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.collaboration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.User;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.collaboration.ProposalLock;

/**
 * The SetUpUploadAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class SetUpLockProposalAction extends Action {
	private Log log = LogFactory.getLog(this.getClass());

	private NorthStarDelegate northStarDelegate = null;

	private CollaborationDelegate collaborationDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		LockProposalForm lockedProposalForm = (LockProposalForm) form;
		HttpSession session = request.getSession();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
		northStarDelegate = NorthStarDelegate.getInstance();
		collaborationDelegate = CollaborationDelegate.getInstance();
		lockedProposalForm.setForwardUri((String) request
				.getAttribute(Constants.FORWARD));
		ProposalLock proposalLock = collaborationDelegate
				.getValidProposalLock(AstronConverter
						.toInteger(lockedProposalForm.getProposalId()));
		if (proposalLock == null) {
			proposalLock = collaborationDelegate.getProposalLock(AstronConverter
					.toInteger(lockedProposalForm.getProposalId()),
					ownUserAccount.getUser().getId(), session.getId());
		}
		/*
		 * if no proposal lock exists
		 */
		if (proposalLock == null){
			return mapping.findForward("nolock");
		}
		Proposal lockedProposal = northStarDelegate.getOwnProposal(proposalLock
				.getProposalId(), ownUserAccount);
		/*
		 * if no own locked proposal, perhaps it is an proposal you are invited
		 * into
		 */
		if (lockedProposal == null) {
			lockedProposal = collaborationDelegate.getProposal(proposalLock
					.getProposalId(), ownUserAccount);

		}
		if (proposalLock.isValid()){
			fillValidProposalLock(proposalLock,
					lockedProposal, lockedProposalForm);
			return mapping.findForward(Constants.SUCCESS);		
		}else {
			fillInValidProposalLock(proposalLock,
					lockedProposal, lockedProposalForm);
			return mapping.findForward("invalid");
		}
	


	}

	protected void fillValidProposalLock(ProposalLock proposalLock,
			Proposal lockedProposal, LockProposalForm lockedProposalForm)
			throws DatabaseException {
		User owner = northStarDelegate.getUser(proposalLock.getUserId());
		lockedProposalForm.setCode(lockedProposal.getCode());
		if (lockedProposal.getJustification() != null) {
			lockedProposalForm.setTitle(lockedProposal.getJustification()
					.getTitle());
		}
		lockedProposalForm.setLastAction(AstronConverter
				.toDateString(proposalLock.getLastAction()));
		lockedProposalForm.setSince(AstronConverter.toDateString(proposalLock
				.getSince()));
		lockedProposalForm.setLockOwner(NorthStarDelegate.getName(owner));
	}

	protected void fillInValidProposalLock(ProposalLock proposalLock,
			Proposal lockedProposal, LockProposalForm lockedProposalForm)
			throws DatabaseException {
		User owner = northStarDelegate.getUser(proposalLock.getRemoverId());
		lockedProposalForm.setCode(lockedProposal.getCode());
		if (lockedProposal.getJustification() != null) {
			lockedProposalForm.setTitle(lockedProposal.getJustification()
					.getTitle());
		}
		lockedProposalForm.setSince(AstronConverter.toDateString(proposalLock
				.getSince()));
		lockedProposalForm.setLockOwner(NorthStarDelegate.getName(owner));
	}
}
