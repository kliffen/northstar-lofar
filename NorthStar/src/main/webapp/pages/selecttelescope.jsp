<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />
<html:form action="/processSelectTelescope" method="POST">

<html:hidden property="proposalId"/>
<TABLE width="100%">


	<tr>
		<td>&nbsp;</td>
	</tr>
	<logic:equal name="selectTelescopeForm" property="noFacilityChoice" value="false">	
	<TR>
		<TD class="right"><astron:label key="label.telescope.selectfacility" /></TD>	
		<TD>
			<bean:define id="telescopes" name="selectTelescopeForm" property="telescopes"/> 
			<html:select property="selectedTelescope" styleClass="selecttelescope" onchange="document.forms[0].submit()">
				<html:options collection="telescopes" property="value" labelProperty="label"/>
			</html:select><span class="error"><astron:errors property="selectedTelescope" ifnotexist="*"/></span>	
		</TD>
	</TR>
	</logic:equal>
	<logic:equal name="selectTelescopeForm" property="noFacilityChoice" value="true">
	<TR>
		<TD class="right"><astron:label key="label.telescope.facility" /></TD>	
		<TD class="bold" align="left" >
			<bean:write  name="selectTelescopeForm" property="selectedTelescopeLabel" />
			<html:hidden property="selectedTelescope"/>
		</TD>
	</TR>	
	</logic:equal>
	<logic:notEmpty name="selectTelescopeForm" property="selectedTelescope">
	<logic:equal name="selectTelescopeForm" property="acceptsProposals" value="true">	
	<logic:equal name="selectTelescopeForm" property="noCommunityChoice" value="true">
	<TR>
		<TD class="right"><astron:label key="label.telescope.community" /></TD>	
		<TD class="bold" align="left" >
			<bean:write  name="selectTelescopeForm" property="selectedCommunityLabel" />
			<html:hidden property="selectedCommunity"/>
		</TD>
	</TR>	
	</logic:equal>
	<logic:equal name="selectTelescopeForm" property="noCommunityChoice" value="false">	
	<TR>
		<TD class="right"><astron:label key="label.telescope.selectcommunity" /></TD>	
		<TD>
			<bean:define id="communities" name="selectTelescopeForm" property="communities"/> 
			<html:select property="selectedCommunity" onchange="document.forms[0].submit()">
				<html:options collection="communities" property="value" labelProperty="label"/>
			</html:select><span class="error"><astron:errors property="selectedCommunity" ifnotexist="*"/></span>	
		</TD>
	</TR>
	</logic:equal>
	<logic:notEmpty name="selectTelescopeForm" property="selectedCommunity">
	<logic:equal name="selectTelescopeForm" property="noCategoryChoice" value="true">
	<TR>
		<TD class="right"><astron:label key="label.telescope.category" /></TD>	
		<TD class="bold" align="left" >
			<bean:write  name="selectTelescopeForm" property="selectedCategoryLabel" />
			<html:hidden property="selectedCategory"/>
		</TD>
	</TR>	
	</logic:equal>	
	<logic:equal name="selectTelescopeForm" property="noCategoryChoice" value="false">	
	<TR>
		<TD class="right"><astron:label key="label.telescope.selectcategory" /></TD>	
		<TD>
			<bean:define id="categories" name="selectTelescopeForm" property="categories"/> 
			<html:select property="selectedCategory" onchange="document.forms[0].submit()">
				<html:options collection="categories" property="value" labelProperty="label"/>
			</html:select><span class="error"><astron:errors property="selectedCategory" ifnotexist="*"/></span>	
		</TD>
	</TR>	
	</logic:equal>
	<logic:notEmpty name="selectTelescopeForm" property="selectedCategory">
	<logic:equal name="selectTelescopeForm" property="selectedCategory" value="single_cycle">
		<tr>
			<td colspan="2">
					<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<table>
							<tr>
								<td valign="top">&nbsp;</td>
								<td valign="top"><b>Apply for a project to be carried out during the upcoming semester</b></td>
							</tr>
						</table>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
			</td>
		</tr>
	</logic:equal>	
	<logic:equal name="selectTelescopeForm" property="noSemesterChoice" value="true">
	<TR>
		<TD class="right"><astron:label key="label.telescope.semester" /></TD>	
		<TD class="bold" align="left" >
			<bean:write  name="selectTelescopeForm" property="selectedSemesterLabel" />
			<html:hidden property="selectedSemesterId"/>
		</TD>
	</TR>	
	</logic:equal>	
	<logic:equal name="selectTelescopeForm" property="noSemesterChoice" value="false">		
	<TR>
		<TD class="right"><astron:label key="label.telescope.selectsemester" /></TD>	
		<TD>
			<bean:define id="semesters" name="selectTelescopeForm" property="semesters"/> 
			<html:select property="selectedSemesterId" onchange="document.forms[0].submit()">
				<html:options collection="semesters" property="value" labelProperty="label"/>
			</html:select><span class="error"><astron:errors property="selectedSemesterId" ifnotexist="*"/></span>	
		</TD>
	</TR>	
	</logic:equal>
	<TR>
			<TD>&nbsp;</TD>
	</TR>
	</logic:notEmpty>
	</logic:notEmpty>
	</logic:equal>
	<logic:notEqual name="selectTelescopeForm" property="acceptsProposals" value="true">	
		<tr>
			<td colspan="2">
					<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<table>
							<tr>
								<td valign="top"><html:img page="/images/list/info.gif"/></td>
								<td valign="top"><astron:label key="label.telescope.noproposals" /></td>
							</tr>
						</table>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
			</td>
		</tr>	
	</logic:notEqual>
	
	</logic:notEmpty>
	<logic:equal name="selectTelescopeForm" property="acceptsProposals" value="true">	
	<logic:notEmpty name="selectTelescopeForm" property="selectedSemesterId">
	<logic:notEqual name="selectTelescopeForm" property="immediate" value="true">
		<tr>
			<td colspan="2">
					<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<table>
							<tr>
								<td valign="top"><html:img page="/images/list/info.gif"/></td>
								<td valign="top"><b>Deadline: </b><bean:write  name="selectTelescopeForm" property="deadLineTime" />
								</td>
							</tr>
						</table>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
			</td>
		</tr>
	</logic:notEqual>	
	<TR>
			<TD>&nbsp;</TD>
	</TR>		
	<logic:equal name="selectTelescopeForm" property="immediate" value="true">
	<TR>
		<TD colspan="2">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				<TD><astron:label key="label.telescope.immediatenote" />
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>	
	</logic:equal>		
	</logic:notEmpty>		
	</logic:equal>
</TABLE><br>
<TABLE width="100%">
	<TR>
		<TD class="left">
			<logic:equal name="selectTelescopeForm" property="acceptsProposals" value="true">
		   		<html:submit disabled="false" property="selectButton" styleClass="list_accept_yes" title="Ok Select telescope">OK</html:submit>
		   	</logic:equal>
			<logic:notEqual name="selectTelescopeForm" property="acceptsProposals" value="true">
		   		<html:submit disabled="true" property="selectButton" styleClass="list_accept_yes_disabled" title="Ok Select telescope">OK</html:submit>
		   	</logic:notEqual>
		   		
		 </TD>
   		<td class="right"><html:cancel title="Cancel selection" styleClass="list_decline_t">Cancel</html:cancel>
		</TD>
	</TR>
</TABLE>
</html:form>
