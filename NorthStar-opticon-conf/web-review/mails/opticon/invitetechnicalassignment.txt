<SUBJECT>Your %committee% technical review assignment</SUBJECT>
Dear %reviewerFirstName%,

As I hope you already know, you have been nominated as a technical 
referee for OPTICON trans-national proposals at your facility. The 
Chairman or Panel Coordinator has probably already approached you about
this personally.

The proposal assessment process makes use of the web-based tool
%proposalreviewtool%. This invitation mail is being sent to you via the
tool to allow you to initiate your online activities.

To register for the review processes in %proposalreviewtool%, signaling
your agreement to participate, please follow this link in your browser:
%acceptlink%

This link will first prompt you to log in to %proposalreviewtool%. If
you have an existing %proposalreviewtool% user account, which relates
to the e-mail address where you receive the current mail, you can use
that.

New users of %proposalreviewtool% will be able to start setting up an
account from the login page presented when following the link; after
setup, a password (which can be changed later) will be sent to your
e-mail address. When you have your account available, please follow the
link again to register as a participant in the review process.

Could you please provide a brief technical report on the following 
proposals in the current cycle. You are invited to mark them as 
feasible or not, and to add any general comments or advice to 
the panel in the text box. 

Thanks for helping with the OPTICON common TAC review process.


YOUR ASSIGNED PROPOSALS:
------------------------
Technical reviewer (%reviewerCount% proposals):
%reviewerCodes%

Please enter your reviews online in %proposalreviewtool%:
%proposalreviewuri%

Check now, please, whether you have any conflict-of-interest. If so,
please contact me by e-mail directly. Affiliations at the
same institute are unavoidable.

You first have to be registered for the online review process for the
%committee%; you should have received an e-mail instructing you how
to register if that was still needed; if you have not done so already,
please follow the link in that mail to register now.


--> The schedule of the common TAC is very tight, please try to
submit your reviews in the next 3 weeks. <--

Further information can be found online.

Best regards,
%sender%

