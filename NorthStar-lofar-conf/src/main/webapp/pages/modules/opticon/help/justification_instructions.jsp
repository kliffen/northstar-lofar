<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<html:html>
<head>
	<link rel=stylesheet href="<%=request.getContextPath()%>/css/proposalLayout.css" type="text/css">
    <title>Justification File Instructions</title>
</head>

<body>
<jsp:include page="../../../layout/box_header_noFullWidth.jsp" /> 
<h2>Instructions for the justification file:</h2><PRE>
The proposers should make their case in a fully self-contained science
justification, uploaded as an A4 pdf file. The page-limit for the pdf-file
varies per call for proposals. The document should include the science 
justification, additional technical information that is not provided in
the "technical questions" section within the Northstar tool, and any desired
ancillary material such as figures and tables.

The current page limits are:

Cycle 4: 4-8 pages of science justification (including a possible technical
addendum) including ancillary material (depending on the amount of proposed
hours, see the call for details)

Northstar accepts uploads of a single pdf file with a minimum font size of
11 pt (12 pt recommended). The pdf can be generated from, e.g., Microsoft Word,
LaTeX and through several other routes at the proposer's choice. An example
latex template is provided below with the appropriate sections and descriptions.

Note: all PI's should check the pdf of the proposal BEFORE submission, making
sure that text is not overlapping with headers and, in general, that no layout
issues are present that make text unreadable. Proposals affected by layout
issues may be rejected.

The pdf file should contain at least the following sections:
1 Scientific Rationale

When additional Technical explanation is needed, include it in a separate Section:
2 Technical Justification


Scientific justification of the proposal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Example TEX file %%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%% Howto generate the pdf %%%%%%
%-> latex template.tex
%-> dvipdf template.dvi
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\documentclass[a4paper,12pt]{article}
\oddsidemargin=-0.54cm
\evensidemargin=-0.54cm
\topmargin=-1cm
\textwidth=17cm
\textheight=22cm
\pagestyle{empty}
\begin{document}
%%%% Title of proposal
\begin{center}
{\bf \Large Proposal Title} \\*[3mm]
\end{center}
\section{Scientific Rationale}
%%%% TEXT OF JUSTIFICATION HERE!
%%%% Non-mandatory sections
% \section{Technical Addendum}
%%%% Additional technical justification which is not covered in
%%%%   the "Technical questions" section of the Northstar tool
%%%% Example sections for additional technical information
% \subsection{Sensitivity and instrument setup}
% \subsection{data volumes/rates}
% \subsection{Processing requirements}
% \subsection{Data storage and LTA requirements}
\end{document}

</PRE>
<br/>
<jsp:include page="../../../layout/box_footer_noFullWidth.jsp" /> 
</body>
</html:html>