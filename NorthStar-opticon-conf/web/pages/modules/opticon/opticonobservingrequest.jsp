<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />

<html:form action="/processObservingRequest" method="POST">

<TABLE  width="100%">
<!--  Help link in the top right corner  -->
	<TR>
		<TD width="*">&nbsp;</TD>
		<TD class="help" width="60">
<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="60" height="25">
							<TR>
					  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#opticonobservingrequest\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
				  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#opticonobservingrequest\" target=\"help\">Help</a>"%></TD>
			  				</TR>
			  			</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />		
		</TD>
	</TR>
</TABLE>
<TABLE align="center" width="100%">	
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<logic:equal name="observingRequestForm" property="displayScienceCategory" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.sciencecategory" /><!-- Science Category :&nbsp;--></td>
			<td class="left">
			<bean:define id="mynamelist" name="observingRequestForm" property="scienceCategories"/> 
				
				<html:select name="observingRequestForm" property="category" size="1" style="width:250px">
					<html:options collection="mynamelist" property="value" labelProperty="value"/>		
				</html:select>
			</td>	
		</tr>	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</logic:equal>
	<logic:equal name="observingRequestForm" property="displayLongTermProposal" value="true">	
		<tr>
			<td class="right"><astron:label key="label.opticon.observingrequest.longterm" /></td>
			<td class="left">
				<html:radio property="longTermProposal" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
				<html:radio property="longTermProposal" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>
		</tr>

		<logic:equal name="observingRequestForm" property="longTermProposal" value="true">
			<tr>
				<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.details" /></td>
				<td class="left"><html:textarea property="longTermProposalSpecifics" cols="60" rows="4"/><span class="error">*</span></td>
			</tr>
		</logic:equal>	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	</logic:equal>	
	
	<logic:equal name="observingRequestForm" property="displayLargeProposal" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.largeproposal" /></td>
			<td class="left"><html:radio property="largeProposal" value="true">Yes</html:radio>
							<html:radio property="largeProposal" value="false">No</html:radio></td>
		</tr>	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</logic:equal>
	
	<logic:equal name="observingRequestForm" property="displayObservingMode" value="true">
		<tr>
			<td class="right">
				<astron:label key="label.opticon.observingrequest.observingmode" />
			</td>
			<td class="left">
				<logic:iterate id="observingModeItem" name="observingRequestForm" property="observingModes">
					<bean:define id="observingModeLabel">
						<bean:write name="observingModeItem" property="label"/>
					</bean:define>
					<html:radio property="selectedObservingMode" value="<%=observingModeLabel%>">
						<bean:write name="observingModeItem" property="value"/>
					</html:radio>
				</logic:iterate>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	</logic:equal>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	
	<!-- If there are target/observations defined show some target properties in a tabel  -->
	<logic:notEmpty name="observingRequestForm" property="observationSummaryValues">
	
	<TR>
		<TD colspan="2">
			<table class="projecttable"  align="center">
			<!-- write the column headers j should contain the number of target properties that are displayed. The total columns of this
				table should be j+3 (the proporties plus three columns containing buttons -->
			<% int j = 0; %>
				<TR>
					<logic:iterate id="observationSummaryLabel" name="observingRequestForm" property="observationSummaryLabels">
						<TD  class="tableheader">&nbsp;<bean:write name="observationSummaryLabel"/>&nbsp;</TD>
	 		<% j++; %>
					</logic:iterate>
					<!--  the last three burtton columns do not have a header -->
					<td class="tableheader" colspan="3">&nbsp;</td>
				</TR>
			<!-- 
				the following two variables are used to change the color of the table rows. Even rows have a different color than
				odd rows
			 -->
			<%
			 int i=0;
			 String styleClass = "";
			%>				
				<!--  Iterate though the defined targets. One target a row. -->
	 			<logic:iterate id="observationBean" name="observingRequestForm" property="observationSummaryValues">
	 			<%
	 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
	 			%>
 	 			<TR>
 	 			<!-- iterate all the target properties and display them. (1st element has a different style) -->
 	 			<logic:iterate id="valueBean" indexId="valueBeanId" name="observationBean">
	 	 			<logic:equal name="valueBeanId" value="0">
						<TD class="tablefield_pri<%=styleClass%>"><bean:write name="valueBean" property = "value"/></TD>
	 	 			</logic:equal>
	 	 			<logic:notEqual name="valueBeanId" value="0">
	 	 				<TD class="tablefield<%=styleClass%>"><bean:write name="valueBean" property = "value"/></TD>
	 	 			</logic:notEqual>
 	 			</logic:iterate>
 	 			<!--  Display the edit buton -->
				<TD class="tablefield<%=styleClass%>"><html:submit indexed="true" property="editObservationButton" styleClass="list_edit" title="Edit target/obersavation!">Edit</html:submit></TD>					
				<!--  when the maximum limit of observation has not been reached show the copy button -->
				<logic:equal name="observingRequestForm" property="maxNumberOfObservationsReached" value="true">
					<TD class="tablefield<%=styleClass%>"><html:submit disabled="true" indexed="true" property="copyObservationButton" styleClass="list_copy" title="Copy this target/obersavation" onclick="setNorthStarCookie(getScrollCoordinates());">Copy</html:submit></TD>
				</logic:equal>
				<logic:equal name="observingRequestForm" property="maxNumberOfObservationsReached" value="false">
					<TD class="tablefield<%=styleClass%>"><html:submit disabled="false" indexed="true" property="copyObservationButton" styleClass="list_copy" title="Copy this target/obersavation" onclick="setNorthStarCookie(getScrollCoordinates());">Copy</html:submit></TD>
				</logic:equal>					
				<!--  Display the delete button -->
				<TD class="tablefield<%=styleClass%>"><html:submit indexed="true" property="deleteObservationButton" styleClass="list_delete" title="Delete target/obersavation, no prompt!" onclick="setNorthStarCookie(getScrollCoordinates());">Delete</html:submit></TD>

	 			</TR>
	 			</logic:iterate>
	 			<tr><td colspan="<%=j+3 %>" class="spacer">&nbsp;</td></tr>	 				 			
			</table>
		</TD>
	</TR>
	<!-- If the maximum limit of target/observation has been reached display a message -->
	<tr>
		<td colspan="2" class="italic" align="center" >&nbsp;
		<logic:equal name="observingRequestForm" property="maxNumberOfObservationsReached" value="true">
			<astron:label key="label.opticon.observingrequest.maxreached" />
		</logic:equal>
		</td>
	</tr>
	</logic:notEmpty>

	<!-- If there are no targets/observations defined, display message 
	<logic:empty name="observingRequestForm" property="observationSummaryValues">
	<TR>
		<TD colspan="2" class="italic" align="center" ><astron:label key="label.opticon.observingrequest.specify.setup"/></TD>
	</TR>
	</logic:empty> --><!-- You must specify targets/observation setup here.-->
	 <html:hidden name="observingRequestForm" property="selectedObservation" />
			
			<logic:equal name="observingRequestForm" property="maxNumberOfObservationsReached" value="true">
				<TR>
				<TD colspan="2" align="center">
					<html:submit disabled="true"  property="newObservationButton" styleClass="list_newobs" title="New Target"> <astron:label key="label.opticon.observingrequest.new.observation" /></html:submit>&nbsp;
						</TD>
				</TR>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</logic:equal>
			<logic:equal name="observingRequestForm" property="maxNumberOfObservationsReached" value="false">
			  <logic:empty name="observingRequestForm" property="selectedObservation">
			  <TR>
				<TD colspan="2" align="center">
				<html:submit disabled="false" property="newObservationButton" styleClass="list_newobs" title="New Target"><astron:label key="label.opticon.observingrequest.new.observation" /> </html:submit>&nbsp;<!-- Specify a new observation -->
						</TD>
				</TR>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			  </logic:empty>
			</logic:equal>	
		

	<logic:equal name="observingRequestForm" property="isDisplayPipeline" value="true">
	
					<!-- If there are target/observations defined show some target properties in a tabel  -->
			<logic:notEmpty name="observingRequestForm" property="pipelineSummaryValues">
			
			<TR>
				<TD colspan="2">
					<table class="projecttable"  align="center">
					<!-- write the column headers j should contain the number of target properties that are displayed. The total columns of this
						table should be j+3 (the proporties plus three columns containing buttons -->
					<% int j = 0; %>
						<TR>
							<logic:iterate id="pipelineSummaryLabel" name="observingRequestForm" property="pipelineSummaryLabels">
								<TD  class="tableheader">&nbsp;<bean:write name="pipelineSummaryLabel"/>&nbsp;</TD>
			 		<% j++; %>
							</logic:iterate>
							<!--  the last three burtton columns do not have a header -->
							<td class="tableheader" colspan="3">&nbsp;</td>
						</TR>
					<!-- 
						the following two variables are used to change the color of the table rows. Even rows have a different color than
						odd rows
					 -->
					<%
					 int i=0;
					 String styleClass = "";
					%>				
						<!--  Iterate though the defined targets. One target a row. -->
			 			<logic:iterate id="pipelineBean" name="observingRequestForm" property="pipelineSummaryValues">
			 			<%
			 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
			 			%>
		 	 			<TR>
		 	 			<!-- iterate all the target properties and display them. (1st element has a different style) -->
		 	 			<logic:iterate id="valueBean" indexId="valueBeanId" name="pipelineBean">
			 	 			<logic:equal name="valueBeanId" value="0">
								<TD class="tablefield_pri<%=styleClass%>"><bean:write name="valueBean" property = "value"/></TD>
			 	 			</logic:equal>
			 	 			<logic:notEqual name="valueBeanId" value="0">
			 	 				<TD class="tablefield<%=styleClass%>"><bean:write name="valueBean" property = "value"/></TD>
			 	 			</logic:notEqual>
		 	 			</logic:iterate>
		 	 			<!--  Display the edit buton -->
						<TD class="tablefield<%=styleClass%>"><html:submit indexed="true" property="editPipelineButton" styleClass="list_edit" title="Edit pipeline!">Edit</html:submit></TD>					
						<!--  when the maximum limit of observation has not been reached show the copy button 
						<logic:equal name="observingRequestForm" property="maxNumberOfObservationsReached" value="true">
							<TD class="tablefield<%=styleClass%>"><html:submit disabled="true" indexed="true" property="copyObservationButton" styleClass="list_copy" title="Copy this target/obersavation" onclick="setNorthStarCookie(getScrollCoordinates());">Copy</html:submit></TD>
						</logic:equal>
						<logic:equal name="observingRequestForm" property="maxNumberOfObservationsReached" value="false">
							<TD class="tablefield<%=styleClass%>"><html:submit disabled="false" indexed="true" property="copyObservationButton" styleClass="list_copy" title="Copy this target/obersavation" onclick="setNorthStarCookie(getScrollCoordinates());">Copy</html:submit></TD>
						</logic:equal>		
						-->			
						<!--  Display the delete button 
						<TD class="tablefield<%=styleClass%>"><html:submit indexed="true" property="deleteObservationButton" styleClass="list_delete" title="Delete target/obersavation, no prompt!" onclick="setNorthStarCookie(getScrollCoordinates());">Delete</html:submit></TD>
						-->
			 			</TR>
			 			</logic:iterate>
			 			<tr><td colspan="<%=j+3 %>" class="spacer">&nbsp;</td></tr>	 				 			
					</table>
				</TD>
			</TR>
			<!-- If the maximum limit of target/observation has been reached display a message -->
			
			</logic:notEmpty>
	
			  <logic:empty name="observingRequestForm" property="selectedPipeline">
			  <TR>
				<TD colspan="2" align="center">
				<html:submit disabled="false" property="newPipelineButton" styleClass="list_newobs" title="New Pipeline"><astron:label key="label.opticon.observingrequest.new.pipeline" /> </html:submit>&nbsp;<!-- Specify a new observation -->
						</TD>
				</TR>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			  </logic:empty>
	</logic:equal>


	<!-- HERE THE INSTRUMENT PAGE IS INCLUDED -->
	<logic:equal name="observingRequestForm" property="showInstrumentBean" value="true">
	 
	  <jsp:include page="instrument.jsp" />
	  <logic:empty name="observingRequestForm" property="selectedObservation">
		<tr>
			<td class="header" colspan="2"><astron:label key="label.opticon.observation.telescope.configuration" /><!--Telescope configuration :--></td>
		</tr>	
	
		<tr>
			<td colspan="2"><tiles:get name="box_header_no_top" />
				<table width="100%">
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				</table>
			<tiles:get name="box_footer" /></td>
		</tr>
	  </logic:empty>		
	
	  <logic:notEmpty name="observingRequestForm" property="selectedObservation">
		<tr>
		<td colspan="2" align="center">
			<html:submit disabled="false" property="commitObservationButton" styleClass="list_newobs" title="commit observation"> Commit Observing run</html:submit>
		</td>
		</tr>
	  </logic:notEmpty>
	</logic:equal> 
	    
	
					
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		

	<logic:equal name="observingRequestForm" property="displayAllocations" value="true">
		<tr>
			<logic:equal name="observingRequestForm" property="multipleTelescopes" value="true">
				<td class="header" colspan="2"> <astron:label key="label.opticon.observingrequest.allocations"/><!-- Allocations per telescope :--></td>
			</logic:equal>
			<logic:equal name="observingRequestForm" property="multipleTelescopes" value="false">
				<td class="header" colspan="2"><astron:label key="label.opticon.observingrequest.specify.setup"/><!-- Allocations :--></td>
			</logic:equal>
			
		</tr>	
		<tr>
			<td colspan="2"><jsp:include page="/pages/layout/box_header_no_top.jsp" />
		<table align="center">
			<tr>
				<td>&nbsp;</td>
			</tr>
			<logic:equal name="observingRequestForm" property="displayUseResources" value="false">
			
				<logic:empty name="observingRequestForm" property="allocationBeans">
					<tr>
						<td class="italic" align="center" ><astron:label key="label.opticon.observingrequest.undefined" /><!-- No telescopes are defined yet within the observations--></td>
					</tr>	
				</logic:empty>
			</logic:equal>
			
			<logic:notEmpty name="observingRequestForm" property="allocationBeans">
	 			<logic:iterate id="allocationBean" name="observingRequestForm" property="allocationBeans">
	 				<html:hidden name="allocationBean" property="id" indexed="true"/>
	 				<html:hidden name="allocationBean" property="telescope" indexed="true"/>
					<tr>
						<td class="header"><bean:write name="allocationBean" property="telescope"/></td>
					</tr>	 	
					<tr>
						<td><jsp:include page="/pages/layout/box_header_no_top.jsp" />
						<table>	
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td colspan="4" align="center" class="subscript"><astron:label key="label.opticon.observingrequest.time.specified" /> <!--Time specified for targets (in minutes): <BR>-->
								
								<logic:notEmpty name="allocationBean" property="obsDarkTime">
								<astron:label key="label.opticon.observingrequest.dark" /><!-- dark :--> <bean:write name="allocationBean" property="obsDarkTime"/>&nbsp;&nbsp;&nbsp;
								</logic:notEmpty>
								<logic:notEmpty name="allocationBean" property="obsGreyTime">
								<astron:label key="label.opticon.observingrequest.grey" /><!-- grey :--><bean:write name="allocationBean" property="obsGreyTime"/>&nbsp;&nbsp;&nbsp;
								</logic:notEmpty>
								<logic:equal name="observingRequestForm" property="displayBright" value="true">
									<logic:notEmpty name="allocationBean" property="obsBrightTime">
										<astron:label key="label.opticon.observingrequest.bright" /><!-- bright :--> <bean:write name="allocationBean" property="obsBrightTime"/>&nbsp;&nbsp;&nbsp;
									</logic:notEmpty>
								</logic:equal>
								<!--
								<logic:notEmpty name="allocationBean" property="obsLastQTime">
									last quarter: <bean:write name="allocationBean" property="obsLastQTime"/>
								</logic:notEmpty>
								-->
								<logic:notEmpty name="allocationBean" property="obsGlobalTime">
									<astron:label key="label.opticon.observingrequest.total" /><!-- Total :-->  <bean:write name="allocationBean" property="obsGlobalTime"/>
								</logic:notEmpty>
								</td>						
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<logic:equal name="observingRequestForm" property="displayRequestedTime" value="true">
								<tr>
									<td colspan="4"><astron:label key="label.opticon.observingrequest.requested.this" /> <!-- Hours requested this semester (incl. cal. and o/heads) :--></td>
								</tr>
								
								<tr>
									<logic:equal name="observingRequestForm" property="displayDark" value="true">
										<td><astron:label key="label.opticon.observingrequest.dark" /><!-- dark :--> <html:text name="allocationBean" property="darkTime" size="4" indexed="true"/>&nbsp;&nbsp;&nbsp;<span class="error">*</span></td>
									</logic:equal>
									<logic:equal name="observingRequestForm" property="displayGrey" value="true">
										<td><astron:label key="label.opticon.observingrequest.grey" /><!-- grey :--> <html:text name="allocationBean" property="greyTime" size="4" indexed="true"/>&nbsp;&nbsp;&nbsp;<span class="error">*</span></td>
									</logic:equal>
									<logic:equal name="observingRequestForm" property="displayBright" value="true">
										<td><astron:label key="label.opticon.observingrequest.bright" /><!-- bright :--> <html:text name="allocationBean" property="brightTime" size="4" indexed="true"/>&nbsp;&nbsp;&nbsp;<span class="error">*</span></td>
									</logic:equal>
									<logic:equal name="observingRequestForm" property="displayGlobal" value="true">
										<td><astron:label key="label.opticon.observingrequest.total" /><!-- Total :-->  <html:text name="allocationBean" property="globalTime" size="4" indexed="true"/>&nbsp;&nbsp;&nbsp;<span class="error">*</span></td>
									</logic:equal>	
									
									<td> &nbsp; </td>
								</tr>
							</logic:equal>
							<!--	
							<tr>
								<td>Last Quarter : <html:text name="allocationBean" property="lastQTime" size="4" indexed="true"/></td>
								<td colspan="4">
									Hours requested this semester (incl. cal. and o/heads) :  
									<html:text name="allocationBean" property="globalTime" size="4" indexed="true"/>
									<span class="error"><astron:errors property="requestedTime" ifnotexist="*" /></span>
							</tr> 
							-->
							<logic:equal name="observingRequestForm" property="displayUsefulTime" value="true">
								<tr>
									<td colspan="4"><astron:label key="label.opticon.observingrequest.requested.usefull" /><!-- Minimum usefull time: --></td>
								</tr>
								<tr>
								<td><astron:label key="label.opticon.observingrequest.dark" /><!-- dark :--> <html:text name="allocationBean" property="usefulDarkTime" size="4" indexed="true"/>&nbsp;&nbsp;&nbsp;<span class="error">*</span></td>
								<td><astron:label key="label.opticon.observingrequest.grey" /><!-- grey :--> <html:text name="allocationBean" property="usefulGreyTime" size="4" indexed="true"/>&nbsp;&nbsp;&nbsp;<span class="error">*</span></td>
								<td><astron:label key="label.opticon.observingrequest.bright" /><!-- bright :--> <html:text name="allocationBean" property="usefulBrightTime" size="4" indexed="true"/>&nbsp;&nbsp;&nbsp;<span class="error">*</span></td>
								<td> &nbsp; </td>
								</tr>
							</logic:equal>
							<logic:equal name="observingRequestForm" property="displayLongTermTime" value="true">
								<logic:equal name="observingRequestForm" property="longTermProposal" value="true">
									<tr><td colspan="4" align="left">
									<astron:label key="label.opticon.observingrequest.requested.program" /><!-- Total hours requested for the program:-->
									</td></tr>
									  <tr>
									<logic:equal name="observingRequestForm" property="displayDark" value="true">
										<td><astron:label key="label.opticon.observingrequest.dark" /><!-- dark :--> <html:text name="allocationBean" property="longTermDarkTime" size="4" indexed="true"/><span class="error">*</span> </td>
									</logic:equal>								
									<logic:equal name="observingRequestForm" property="displayGrey" value="true">
										<td><astron:label key="label.opticon.observingrequest.grey" /><!-- grey :--> <html:text name="allocationBean" property="longTermGreyTime" size="4" indexed="true"/><span class="error">*</span> </td>
									</logic:equal>
									<logic:equal name="observingRequestForm" property="displayBright" value="true">
										<td><astron:label key="label.opticon.observingrequest.bright" /><!-- bright :--> <html:text name="allocationBean" property="longTermBrightTime" size="4" indexed="true"/><span class="error">*</span> </td>
									</logic:equal>
									<logic:equal name="observingRequestForm" property="displayGlobal" value="true">
										<td> <astron:label key="label.opticon.observingrequest.total" /><!-- Total :-->  <html:text name="allocationBean" property="longTermGlobalTime" size="4" indexed="true"/><span class="error">*</span> </td>
									</logic:equal>
									  </tr>
								</logic:equal>
							</logic:equal>
							
							<logic:equal name="observingRequestForm" property="displayTotalTime" value="true">
							  <tr><td colspan="4" align="left">
									<astron:label key="label.opticon.observingrequest.requested.program" /><!-- Total hours requested for the program:-->
									</td></tr>
							  <tr>
								<td><astron:label key="label.opticon.observingrequest.dark" /><!-- dark :--> <html:text name="allocationBean" property="longTermDarkTime" size="4" indexed="true"/> <span class="error">*</span></td>
								<td><astron:label key="label.opticon.observingrequest.grey" /><!-- grey :--> <html:text name="allocationBean" property="longTermGreyTime" size="4" indexed="true"/><span class="error">*</span> </td>
								<td> <astron:label key="label.opticon.observingrequest.bright" /><!-- bright :-->  <html:text name="allocationBean" property="longTermBrightTime" size="4" indexed="true"/> <span class="error">*</span></td>
								<td> <astron:label key="label.opticon.observingrequest.total" /><!-- Total :-->  <html:text name="allocationBean" property="longTermGlobalTime" size="4" indexed="true"/> <span class="error">*</span></td>
								
							  </tr>
							</logic:equal>
							
							<logic:equal name="observingRequestForm" property="displayLongTermStorage" value="true">
								<tr>
									<td colspan="4">&nbsp;</td>
								</tr>
								<tr><td colspan="4"  align="center" class="subscript">Storage specified: <bean:write name="observingRequestForm" property="specifiedStorage"/> GB&nbsp;&nbsp;
								</td></tr>
							  <tr><td colspan="4" align="left">
									 <astron:label key="label.opticon.observingrequest.requested.storage" /> <!-- Long term storage requested in gigabytes (10^9 bytes):-->
									</td></tr>
							  <tr>
								<td><astron:label key="label.opticon.observingrequest.total" /><!-- Total :--> <html:text name="allocationBean" property="longTermStorage" size="6" indexed="true"/><span class="error">*</span> </td>
								
							  </tr>
							</logic:equal>
							
						</table>
						<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />			
						</td>
					</tr>
					<tr>
						<td colspan="2">&nbsp;</td>
					</tr>					
	 			</logic:iterate>
			</logic:notEmpty>
		</table>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />	
			</td>
		</tr>
	</logic:equal>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<logic:equal name="observingRequestForm" property="displayAllocationJustification" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.justify" /><!-- justify allocations :&nbsp;--></td>
			<td class="left"><html:textarea property="allocationJustification" cols="60" rows="10"/><span class="error">*</span></td>
		</tr>	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</logic:equal>
	<logic:equal name="observingRequestForm" property="displayObservingDetails" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.observing.details" /><!-- observing details :&nbsp;<br/> -->
			<!-- (including preferred dates and periods to be avoided)&nbsp; --></td>
			<td class="left"><html:textarea property="overallRequiredSchedConstraints" cols="60" rows="4"/></td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	</logic:equal>
	
	
	<logic:equal name="observingRequestForm" property="displayTravel" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.travel" /><!-- Non-standard travel and subsistence :&nbsp;<br/>(UK Observers only)&nbsp;&nbsp;--></td>
			<td class="left"><html:textarea property="travel" cols="60" rows="4"/></td>
		</tr>	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</logic:equal>
	<logic:equal name="observingRequestForm" property="displayProprietaryException" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.proprietary" /></td>
			<td class="left">	
				<html:radio property="enableProprietaryException" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
				<html:radio property="enableProprietaryException" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
		</logic:equal>	
		<logic:equal name="observingRequestForm" property="enableProprietaryException" value="true">
		<tr>
			<td class="right"> <astron:label key="label.opticon.observingrequest.proprietary.specifics" /> <!-- list one or more organisations from which you are requesting sponsorship (e.g. GLOW) :&nbsp;--></td>
			<td class="left"><html:textarea property="proprietaryExceptionSpecifics" cols="60" rows="4"/></td>
				
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</logic:equal>
	
	<logic:equal name="observingRequestForm" property="displayOtherExpenditure" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.other.expences" /><!-- Any other expenditure :&nbsp;<br/>(only if approved)&nbsp;&nbsp;--></td>
			<td class="left"><html:textarea property="otherExpenditure" cols="60" rows="4"/></td>
		</tr>	
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</logic:equal>
	
</TABLE>
<jsp:include page="/pages/include/observingrequeststorebuttons.jsp"/>

</html:form>