// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.applicants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.SetUpProposalAction;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.Status;
import eu.radionet.northstar.data.entities.collaboration.Invitation;

public class SetUpApplicantsAction extends SetUpProposalAction {
	private Log log = LogFactory.getLog(this.getClass());

	private static String INVITED = "invited";

	private static String INVITATION_PENDING = "invited after<BR>next save";

	private static String NOT_INVITED = "no";

	private static String ACTIVE = "yes";

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ApplicantsForm applicantsForm = (ApplicantsForm) form;
		/*
		 * needed for tabs
		 */
		request
				.setAttribute(Constants.CURRENT_ACTION, request
						.getServletPath());
		HttpSession session = request.getSession();

		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		Proposal proposal = proposalData.getProposal();
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
		if (telescopeConfiguration.isTargetsOnTabbedPage()){
			session.setAttribute(Constants.TARGETS_ON_TABBED_PAGE ,"true" );
		}else if(session.getAttribute(Constants.TARGETS_ON_TABBED_PAGE) != null){
			session.setAttribute(Constants.TARGETS_ON_TABBED_PAGE ,"false" );
		}
		List invitations = collaborationDelegate.getInvitations(proposal);

		/*
		 * iterator through members
		 */
		Iterator membersIterator = proposal.getMembers().iterator();
		String[] potentialObservers = new String[proposal.getMembers().size()];
		List memberBeans = new ArrayList();
		int i = 0;
		while (membersIterator.hasNext()) {
			MemberBean memberBean = new MemberBean();
			memberBean.setId(new Integer(i));

			Member member = (Member) membersIterator.next();
			memberBean.setFirst(i == 0);
			memberBean.setLast(!membersIterator.hasNext());
			memberBean.setMember(member);
			if (memberBean.isPi()) {
				applicantsForm.setPi(memberBean.getId());
			}
			if (memberBean.isContactAuthor()) {
				applicantsForm.setContactAuthor(memberBean.getId());
			}
			if (memberBean.isPotentialObserver()) {
				potentialObservers[i] = memberBean.getId();

			}
			if (RegisteredMember.class.equals(member.getClass())) {
				RegisteredMember registeredMember = (RegisteredMember) member;
				if (registeredMember.getUserId().intValue() == ownUserAccount
						.getUser().getId().intValue()) {
					if (memberBean.isContactAuthor()) {
						applicantsForm.setUserIsContactAuthor(true);
					}
					memberBean.setCurrentUser(true);
				}
				memberBean.setActiveApplicant(ACTIVE);
			} else {
				NonRegisteredMember nonRegisteredMember = (NonRegisteredMember) member;
				if (isInvited(invitations, nonRegisteredMember)) {
					if (nonRegisteredMember.isRemoveInvitation()
							&& nonRegisteredMember.isInvite()) {
						memberBean.setActiveApplicant(INVITATION_PENDING);

					} else {
						memberBean.setActiveApplicant(INVITED);
					}

				} else {
					if (nonRegisteredMember.isInvite()) {
						memberBean.setActiveApplicant(INVITATION_PENDING);
					} else {
						memberBean.setActiveApplicant(NOT_INVITED);
					}
				}

			}
			i++;
			memberBeans.add(memberBean);

		}
		applicantsForm.setMemberBeans(memberBeans);
		applicantsForm.setPotentialObservers(potentialObservers);
		Map proposalsMap = new HashMap();
		ArrayList allProposals =new ArrayList();
		String telescopeList[]= new String [NorthStarConfiguration.getTelescopeNames().size()];
		Iterator telIt = NorthStarConfiguration.getTelescopeNames().iterator(); int index=0;
		while (telIt.hasNext()){
			telescopeList[index++] = (String) telIt.next();
		}		
			List proposals = northStarDelegate.getOwnProposals(telescopeList,
                    ownUserAccount);
			if(proposals != null && proposals.size() >0){
				allProposals.addAll(proposals);
			
			
		}
		applicantsForm.setPoposalListforApplicants(getLabelValueBeans(allProposals));
		return super.lockedExecute(mapping, form, request, response);
	}

	private List getLabelValueBeans(ArrayList allProposals) {
		List beans = new ArrayList();
        beans.add(new LabelValueBean(Constants.NONE_SPECIFIED_LABEL,Constants.NONE_SPECIFIED_VALUE));
        Iterator proIt = allProposals.iterator();
		while (proIt.hasNext()){
			Proposal proposal = (Proposal) proIt.next();
			 String value = proposal.getId().toString();
			 String temptitle=proposal.getSemester().getSemester()+"_"+proposal.getSemester().getTelescope();
		
			String title =proposal.getJustification()!=null?proposal.getJustification().getTitle():"";
			if(title!=null && title.length()>25)
				title =title.substring(0,25);
			 String label =proposal.getCode()!=null?proposal.getCode()+"_":"";
			 if(!AstronValidator.isBlankOrNull(title))
			 label+= title;
			 else
				 label+= temptitle;
			 label+= "("+proposal.getMembers().size()+")";
			 beans.add(new LabelValueBean(label, value));
        }
        return beans;
	}

	protected boolean isInvited(List invitations,
			NonRegisteredMember nonRegisteredMember) {
		if (nonRegisteredMember.getId() == null) {
			return false;
		}
		Iterator invitationsIterator = invitations.iterator();
		while (invitationsIterator.hasNext()) {
			Invitation invitation = (Invitation) invitationsIterator.next();
			if (invitation.getMemberId().intValue() == nonRegisteredMember
					.getId().intValue()) {
				return true;
			}
		}
		return false;
	}

}
