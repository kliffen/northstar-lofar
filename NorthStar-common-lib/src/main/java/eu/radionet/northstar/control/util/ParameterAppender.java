// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ParameterAppender.java 
 *
 * Created on Jan 21, 2005
 *
 * Version $Id: ParameterAppender.java,v 1.1 2006-05-02 10:01:14 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.util;

import org.apache.struts.action.ActionForward;


/**
 * The ParameterAppender provides
 *
 * @author Bastiaan Verhoef
 *
 */
public class ParameterAppender {

    private static ParameterAppender parameterAppender = null;
    private ParameterAppender()  {
    }

    public static ParameterAppender getInstance() {
        if (null == parameterAppender) {
            parameterAppender = new ParameterAppender();
        }
        return parameterAppender;
    }
    
    public static ActionForward append(ActionForward oldForward, String[] names, String[] values){
        ActionForward forward = new ActionForward();
        forward.setRedirect(oldForward.getRedirect());
		forward.setModule(oldForward.getModule());
        StringBuffer path = new StringBuffer(oldForward.getPath());
        for (int i=0; i < names.length; i++){
            if (path.indexOf("?")>=0){
                path.append("&"+names[i] + "=" + values[i]);
            }
            else {
                path.append("?"+names[i] + "=" + values[i]);
            }
        }  
        forward.setPath(path.toString());
        return forward;
    }
    public static ActionForward append(ActionForward oldForward, String name, String value){
		String[] names = new String[] {name};
		String[] values = new String[] {value};
        ActionForward forward = new ActionForward();
        forward.setRedirect(oldForward.getRedirect());
		forward.setModule(oldForward.getModule());
        StringBuffer path = new StringBuffer(oldForward.getPath());
        for (int i=0; i < names.length; i++){
            if (path.indexOf("?")>=0){
                path.append("&"+names[i] + "=" + values[i]);
            }
            else {
                path.append("?"+names[i] + "=" + values[i]);
            }
        }  
        forward.setPath(path.toString());
        return forward;
    }
}
