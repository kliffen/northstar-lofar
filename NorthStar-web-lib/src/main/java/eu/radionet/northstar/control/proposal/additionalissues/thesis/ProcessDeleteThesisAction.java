// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessDeleteThesisAction.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: ProcessDeleteThesisAction.java,v 1.3 2008-04-08 08:03:00 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.additionalissues.thesis;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.AdditionalIssues;

/**
 * The ProcessDeleteThesisAction provides
 * 
 * @author Hanno Holties
 */
public class ProcessDeleteThesisAction extends LockedAction {
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ThesisForm thesisForm = (ThesisForm) form;
		if (isCancelled(request)) {
			return mapping.findForward(Constants.SUCCESS);

		}
		
		if (!AstronValidator.isBlankOrNull(thesisForm.getThesisId())) {
			HttpSession session = request.getSession();
			ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);

			AdditionalIssues additionalIssues = proposalData
                .getProposal().getAdditionalIssues();
			additionalIssues.getTheses().remove(
					AstronConverter.toInteger(thesisForm.getThesisId())
							.intValue());
		}

		return mapping.findForward(Constants.SUCCESS);

	}
}