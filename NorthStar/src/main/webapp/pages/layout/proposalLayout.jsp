<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>

<html:html>
<head>
	<logic:equal name="browser" value="gecko">
	<link rel=stylesheet href="<%=request.getContextPath()%>/css/proposalLayout.css" type="text/css"/>
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/ffutils.js"></script>
	</logic:equal>
	<logic:equal name="browser" value="msie">
		<script type="text/javascript" src="<%=request.getContextPath()%>/js/ieutils.js"></script>
		<link rel=stylesheet href="<%=request.getContextPath()%>/css/proposalLayout.css" type="text/css"/>
	</logic:equal>
	<logic:equal name="browser" value="other">
	<link rel=stylesheet href="<%=request.getContextPath()%>/css/proposalLayout.css" type="text/css"/>
	</logic:equal>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/data-picker.js"></script>			
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/cookie.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/utils.js"></script>
	<script type="text/javascript" src="<%=request.getContextPath()%>/js/ajax.js"></script>
	<script type="text/javascript" language="JavaScript">
		function addLoadEvent(func) {
		  var oldonload = window.onload;
		  if (typeof window.onload != 'function') {
		    window.onload = func;
		  } else {
		    window.onload = function() {
		      if (oldonload) {
		        oldonload();
		      }
		      func();
		    }
		  }
		}
		function initiateWindow() { 
			var cookieString = getNorthStarCookie();
			if (cookieString != null){
				var array = cookieString.split(';');
				scrollToCoordinates(getCoordinates(array));
				removeNorthStarCookie();
			}
		}
		addLoadEvent(initiateWindow());
	</script>
	<tiles:insert name="metadata" flush="true"/>
    <title><tiles:getAsString name="title"/></title>
</head>

<body>

<jsp:include page="box_header_noFullWidth.jsp" />

<TABLE>
	<TR class="header" align="center">
		<TD><tiles:getAsString name="title"/></TD>
	</TR>
	<TR>
		<TD>
			<jsp:include page="box_header_no_top.jsp" />
				<tiles:insert name="body" flush="true">
					<tiles:put name="box_footer"        value="/pages/layout/box_footer.jsp" />
					<tiles:put name="box_header"        value="/pages/layout/box_header.jsp" />
					<tiles:put name="box_header_no_top" value="/pages/layout/box_header_no_top.jsp" />
					<tiles:put name="errors"            value="/pages/include/errormessages.jsp" />
				</tiles:insert>
			<jsp:include page="box_footer.jsp" />
		</TD>
	</TR>	
</TABLE>

<jsp:include page="box_footer_noFullWidth.jsp" />
<TABLE width="100%">
	
	<TR>
		<TD align="center">
			<div class="version_info"><astron:version/></div>
		</TD>
	</TR>
</TABLE>
</body>
</html:html>