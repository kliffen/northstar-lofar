<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />

<html:form action="/processApplicants" method="POST">

<TABLE width="100%">
	<TR>
		<TD width="90%">&nbsp;</TD>
		<TD class="help" align="right">
				<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="60" height="25">
							<TR>
					  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#applicants\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
				  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#applicants\" target=\"help\">Help</a>"%></TD>
			  				</TR>
			  			</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />		
	</TD>
	</TR>
	<logic:equal name="applicantsForm" property="validData" value="false">
	<TR>
		<TD>
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				<TD valign='top'><astron:label key="label.applicants.notapplied" /><!-- <PRE>
 The changes have not been applied!

 If there is another window open for editing this proposal,
 please close one of the windows before you continue.
</PRE>-->
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	</logic:equal>	
	<TR>
		<TD colspan="2">
			<table align="center" class="projecttable" width="100%">
				<TR>
				    <td colspan ="2" class="tableheader">&nbsp;</td>
				    <td class="tableheader" nowrap="true"><astron:label key="label.applicants.participant" /><!-- &nbsp;Active&nbsp;<BR>&nbsp;Participant&nbsp;--></td>
				    <td class="tableheader" nowrap="true"><astron:label key="label.applicants.contactauthor" /><!--nbsp;Contact&nbsp;<BR>&nbsp;Author&nbsp;--></td>
				    <td class="tableheader"><astron:label key="label.applicants.pi" /><!--&nbsp;PI&nbsp;--></td>
     				<td class="tableheader"><astron:label key="label.applicants.name" /><!--&nbsp;Name&nbsp;--></td>
     				<td class="tableheader"><astron:label key="label.applicants.affiliation" /><!--&nbsp;Affiliation&nbsp;--></td>
     				<td class="tableheader"><astron:label key="label.applicants.country" /><!--&nbsp;Country&nbsp;--></td>
     				<td class="tableheader"><astron:label key="label.applicants.email" /><!--&nbsp;Email&nbsp;--></td>
				    <td class="tableheader" nowrap="true"><astron:label key="label.applicants.ispi" /><!--&nbsp;Potential&nbsp;<BR>&nbsp;Observer&nbsp;--></td>
     				<td class="tableheader" colspan="3">&nbsp;</td>
				</TR>
				<%
			int i=0;
			String styleClass = "";
			
			%>
	 			<logic:iterate id="member" name="applicantsForm" property="memberBeans">
	 				<%
	 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
					
	 			%>
	 
 	 			<TR height="28">
 	 				<TD>
 	 				<logic:notEqual name="member" property="last" value="true">	
						<html:submit property="moveDownMemberButton" styleClass="movedown" indexed="true" title="move down">&nbsp;</html:submit>   
					</logic:notEqual> 
					</TD>
					<TD>
					<logic:notEqual name="member" property="first" value="true">	
						<html:submit property="moveUpMemberButton" styleClass="moveup" indexed="true" title="move up">&nbsp;</html:submit>   
					</logic:notEqual> 
					</TD>
					<TD class="tablefield_pri<%=styleClass%>"  valign="top" align="center">
						<bean:write name="member" property="activeApplicant" filter="false"/>
					</TD>
					<TD class="tablefield<%=styleClass%>"  valign="top" align="center">
					<logic:equal name="member" property="registeredMember" value="true">	
					
							<html:radio styleClass='<%= styleClass == \"\" ? \"radio\" : \"radio_odd\"%>' property="contactAuthor" idName="member" value="id" onclick="document.forms[0].submit()"/>
							
					</logic:equal>
					</TD>
					<TD class="tablefield<%=styleClass%>"  valign="top" align="center"> 
						<html:radio styleClass='<%= styleClass == "" ? "radio" : "radio_odd"%>' property="pi" idName="member" value="id" onclick="document.forms[0].submit()"/>
					</TD>
					
					<TD class="tablefield<%=styleClass%> "  valign="top"><bean:write name="member" property="name"/></TD>
					<TD class="tablefield<%=styleClass%>"  valign="top"><bean:write name="member" property="affiliation"/></TD>
					<TD class="tablefield<%=styleClass%>"  valign="top">&nbsp;<bean:write name="member" property="country"/></TD>
					<TD class="tablefield<%=styleClass%>"  valign="top"><bean:write name="member" property="email"/></TD>

					<TD class="tablefield<%=styleClass%>"  valign="top" align="center">
					
						<html:multibox styleClass='<%= styleClass == "" ? "box" : "box_odd"%>' property="potentialObservers"><bean:write name="member" property="id"/></html:multibox> 
					</TD>
					<TD class="tablefield<%=styleClass%>" valign="top">
					<logic:equal name="member" property="activeApplicant" value="no">
						<html:submit indexed="true" property="inviteMemberButton" styleClass="list_edit" title="Invite applicant" onclick="setNorthStarCookie(getScrollCoordinates());"><astron:label key="label.applicants.invite" /></html:submit><!--Invite-->
					</logic:equal>
					<logic:equal name="member" property="activeApplicant" value="invited">
						<html:submit indexed="true" property="reInviteMemberButton" styleClass="list_edit" title="Re-invite applicant" onclick="setNorthStarCookie(getScrollCoordinates());"><astron:label key="label.applicants.reinvite" /></html:submit><!--Re-invite-->
					</logic:equal>					
					</TD>
					<TD class="tablefield<%=styleClass%>" valign="top">
					<logic:equal name="member" property="nonRegisteredMember" value="true">
						<html:submit indexed="true" property="editMemberButton" styleClass="list_edit" title="Edit applicant" onclick="setNorthStarCookie(getScrollCoordinates());">Edit</html:submit>
					</logic:equal>
					</TD>
					<TD class="tablefield<%=styleClass%>"  valign="top">
					<logic:notEqual name="member" property="contactAuthor" value="true">	
						<logic:notEqual name="member" property="pi" value="true">
							<html:submit indexed="true" property="deleteMemberButton" title="Delete applicant" styleClass="list_delete" onclick="setNorthStarCookie(getScrollCoordinates());">Delete</html:submit>
					    </logic:notEqual>
					</logic:notEqual>
					</TD>

	 			</TR>
	 			</logic:iterate>
	 			<tr><td colspan="13" class="spacer">&nbsp;</td></tr>
			</table>
		</TD>
	</TR>
	<tr>
		<td>&nbsp;</td>
	</tr>
	
	
		
		<TR>
				<td class="left"><astron:label key="label.opticon.observation.choose.proposal" />
				<!--Choose a proposal&nbsp;:&nbsp; --><bean:define id="poposalListforApplicants" name="applicantsForm" property="poposalListforApplicants" /> 
				<html:select name="applicantsForm" property="selectedProposal" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
						<html:options collection="poposalListforApplicants" property="value"	labelProperty="label" />
					</html:select></td>
				<td class="left">
					
				<html:submit property="addApplicantsButton" styleClass="list_applicants" style="width: 20em;"><astron:label key="label.applicants.addapplicant.other.proposal" /></html:submit>
				</td>
			</TR>
			
		<tr>
		<td>&nbsp;</td>
	</tr>
	<TR>
		<TD class="right" colspan="2"><html:submit property="addMemberButton" styleClass="list_applicants" style="width: 15em;"><astron:label key="label.applicants.addapplicant" /></html:submit><!--Add applicant-->
		</TD>
	</TR>
	<tr>
		<td>&nbsp;</td>
	</tr>
	<bean:define id="userIsContactAuthor" name="applicantsForm" property="userIsContactAuthor"/>
</TABLE>
<%@ include file="include/storebuttons.jsp" %>	
</html:form>