// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts.util.LabelValueBean;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import eu.opticon.northstar.data.OpticonRepository;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.exception.BadJustificationFileException;
import eu.radionet.northstar.business.exception.DeadLinePassedException;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.Repository;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalError;
import eu.radionet.northstar.data.entities.Semester;
import eu.radionet.northstar.data.entities.Target;

public class OpticonProposalDelegate extends ProposalDelegate 
{

    /**
     * This class contains methods concerning the data layer
     * @throws ConnectionException
     */
    public OpticonProposalDelegate() throws ConnectionException {
        super();
    }


    /**
     * retrieves the repository class, which will be a SkeletonRepository in this
     * case
     * @see eu.radionet.northstar.business.ProposalDelegate#getRepository()
     */
    protected Repository getRepository() throws ConnectionException{
        return new OpticonRepository();
    }


    /**
     * removes old allocations from a proposal
     * @see eu.radionet.northstar.business.ProposalDelegate#removeInvalidAllocations(eu.radionet.northstar.data.entities.ObservingRequest)
     */
    protected void removeInvalidAllocations(ObservingRequest observingRequest) throws DatabaseException {
        observingRequest.getAllocations().clear();
        Iterator targetsIt = observingRequest.getTargets().iterator();
    	while (targetsIt.hasNext())
    	{
    		((Target) targetsIt.next()).getAllocations().clear();
    	}
       
    }
    
    
	public Proposal storeWithoutLocking(Proposal proposal,
			UserAccount userAccount) throws DatabaseException {

		//List newTargets = new ArrayList();
		Set newTargets = new HashSet();
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		ObservingRequest observingRequest = proposal.getObservingRequest();
		if(telescopeConfiguration.isTargetsOnTabbedPage()){
			newTargets.addAll(proposal.getObservingRequest().getTargets());
		}
		
		for (int i = 0; i < observingRequest.getObservations().size(); i++) {
			OpticonObservation opticonObservation = (OpticonObservation) observingRequest
					.getObservations().get(i);
			
				if (opticonObservation.getInstrument() != null) {
				OpticonInstrument opticonInstrument = opticonObservation.getInstrument();
				/*
				 * if opticonInstrument is new
				 */
				if (opticonInstrument.getId() == null) {
					opticonInstrument.setObservation(opticonObservation);
					/*
					 * if opticonObservation has id
					 */
					if (opticonObservation.getId() != null) {
						repository.deleteObject(OpticonInstrument.class,
								opticonObservation.getId());
					}
				}
			} else {
				/*
				 * if opticonObservation has id
				 */
				if (opticonObservation.getId() != null) {
					repository.deleteObject(OpticonInstrument.class,
							opticonObservation.getId());
				}
			}
			//FIXME !!! quick fix
			// issue with adding multiple targets when targets on tabbed page is off

			/*
			if(!telescopeConfiguration.isTargetsOnTabbedPage()){
				if(opticonObservation.getTargets() !=null && opticonObservation.getTargets().size()==1){
					Target target = (Target) opticonObservation.getTargets().get(0);
					if(target != null){
						// see if it is in the observing request list
						newTargets.add(target);					
					}
					
				}
			}else{
				newTargets.addAll(opticonObservation.getTargets());
			}
			// end quick fix
			 * 
			 */
			if (opticonObservation.getTargets() != null) {
				newTargets.addAll(opticonObservation.getTargets());
			}
		}
		
		for (int i = 0; i < observingRequest.getPipelines().size(); i++) {
			OpticonObservation opticonPipeline = (OpticonObservation) observingRequest
					.getPipelines().get(i);
			
			if (opticonPipeline.getInstrument() != null) {
				OpticonInstrument opticonInstrument = opticonPipeline.getInstrument();
				/*
				 * if opticonInstrument is new
				 */
				if (opticonInstrument.getId() == null) {
					opticonInstrument.setObservation(opticonPipeline);
					//opticonInstrument.setId(opticonPipeline.getId());
					/*
					 * if opticonObservation has id
					 */
					if (opticonPipeline.getId() != null) {
						repository.deleteObject(OpticonInstrument.class,
								opticonPipeline.getId());
					}
				}
			} else {
				/*
				 * if opticonObservation has id
				 */
				if (opticonPipeline.getId() != null) {
					repository.deleteObject(OpticonInstrument.class,
							opticonPipeline.getId());
				}
			}
			
			if (opticonPipeline.getTargets() != null) {
				newTargets.addAll(opticonPipeline.getTargets());
			}
		}

				
		//
		//List targets = observingRequest.getTargets();
		//if (targets != null){
			Iterator tarit = newTargets.iterator();
			while (tarit.hasNext()){
				Target target = (Target) tarit.next();
				if(target.getId() != null && target.getId().intValue() < 0){
					// store targets before proposal... ??
					target.setId(null);
				}
			}
		//}
		List targets = new ArrayList();
		targets.addAll(newTargets);
		observingRequest.setTargets(targets);
		repository.store(proposal);
		log.info(LogMessage.getMessage(userAccount, "Store proposal with id: "
				+ proposal.getId()));
		return proposal;
	}    

    /**
     * This method checks if the number of pages of the justifications file(s) meet the
     * requirements given by the time allocation committee
     * @see eu.radionet.northstar.business.ProposalDelegate#checkJustificationFiles(eu.radionet.northstar.data.entities.Proposal, java.lang.Integer, java.lang.Integer, java.lang.Integer)
     */
    public void checkJustificationFiles(Proposal proposal,
            Integer scientificFilePages, Integer technicalDetailsPages,
            Integer figureFilePages) throws BadJustificationFileException {
        boolean scientificFile = false;
        boolean technicalDetails = false;
        boolean figureFile = false;
        boolean overall = false;
        int totalfilepages = 0;
         if(scientificFilePages!=null)
        	 totalfilepages+=scientificFilePages.intValue();
         if(technicalDetailsPages!=null)
        	 totalfilepages+=technicalDetailsPages.intValue();
         if(figureFilePages!=null)
         totalfilepages+=figureFilePages;
        
        OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) proposal.getObservingRequest();
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		ProposalError errorChild = null;
		if(proposal.getSemester().getCategory().getCode().equals("envelope_sheet")||proposal.getSemester().getCategory().getCode().equals("ddt")||proposal.getSemester().getCategory().getCode().equals("progress_report"))
			errorChild =validatePageNumber(proposal,opticonObservingRequest,totalfilepages,true);
		else	
			errorChild = validatePageNumber(proposal,opticonObservingRequest,totalfilepages,false);
		
		/*
		
		int maxScientificFilePages=2; //telescopeConfiguration.getMaxPublicationCount();
		List urlList = new ArrayList();
		String Url = null;
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_SCIENTIFIC_PAGECOUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			maxScientificFilePages = AstronConverter.toInteger(Url).intValue();
		}

		int maxFigureFilePages=1; //telescopeConfiguration.getMaxPublicationCount();
		urlList = new ArrayList();
		Url = null;
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_FIGURE_PAGECOUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			maxFigureFilePages = AstronConverter.toInteger(Url).intValue();
		}
		
		int maxTechnicalFilePages=0; //telescopeConfiguration.getMaxPublicationCount();
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_TECHNICAL_PAGECOUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			maxTechnicalFilePages = AstronConverter.toInteger(Url).intValue();
		}
		
		int maxTotalFilePages=0; //telescopeConfiguration.getMaxPublicationCount();
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_TOTAL_PAGECOUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			maxTotalFilePages = AstronConverter.toInteger(Url).intValue();
		}

		
		if (opticonObservingRequest.isLongTermProposal()) 
		{
			
			int extraLongTermPages=0;
			urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.EXTRA_LONGTERM_PAGECOUNT  ,
					new HashMap(), contextConfiguration);
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
				Url =  urlValue.getValue();
				extraLongTermPages = AstronConverter.toInteger(Url).intValue();
			}
			
			if (scientificFilePages != null && scientificFilePages.intValue() > maxScientificFilePages+extraLongTermPages) 
			{
				scientificFile = true;
			}
			if (technicalDetailsPages != null && technicalDetailsPages.intValue() > maxTechnicalFilePages) 
			{
				technicalDetails = true;
			}
			if (figureFilePages != null && figureFilePages.intValue() > 3) // magic number 3
			{
				figureFile = true;
			}
			if (scientificFilePages != null && technicalDetailsPages != null && 
					scientificFilePages.intValue()+ technicalDetailsPages.intValue() > maxTotalFilePages + extraLongTermPages) 
			{
				overall = true;
			}
		} 
		else 
		{
	        if (scientificFilePages != null && scientificFilePages.intValue() > maxScientificFilePages) 
	        {
	            scientificFile = true;
	        }
	        if (technicalDetailsPages != null && technicalDetailsPages.intValue() > maxTechnicalFilePages) 
	        {
	            technicalDetails = true;
	        }
	        if (figureFilePages != null && figureFilePages.intValue() > maxFigureFilePages) 
	        {
	            figureFile = true;
	        }
	        if (scientificFilePages != null && technicalDetailsPages != null && 
	        		scientificFilePages.intValue() + technicalDetailsPages.intValue() > maxTotalFilePages) 
	        {
	            overall = true;
	        }
		}*/
		
		if (!errorChild.isEmpty()) 
        {
            throw new BadJustificationFileException("Wrong number of pages",
                    scientificFile, technicalDetails, figureFile, true);
       
        }
    }
    
    
    public ProposalError validatePageNumber(Proposal proposal,
			ObservingRequest observingRequest, int totalfilepages, boolean fixed) {
		ProposalError error = new ProposalError("justification","Justification");
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		
		
		String telescopeName  ="";
		if (observingRequest.getObservations().size()>0) {
            OpticonObservation observation = (OpticonObservation) observingRequest.getObservations().get(0);
            telescopeName = observation.getTelescopeConfiguration();
		} 

		if(telescopeName.length()==0 && proposal!=null && proposal.getSemester()!=null)
		{
			telescopeName = proposal.getSemester().getTelescope();
		}
		
		List urlList = new ArrayList();
		String Url = null;
		
		int maxTotalPages=0; //telescopeConfiguration.getMaxPublicationCount();
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_TOTAL_PAGECOUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			maxTotalPages = AstronConverter.toInteger(Url).intValue();
		
		}
	
		if(fixed)
		{
			if(totalfilepages>maxTotalPages)
				error.getErrors().add(new ProposalError("maximumFileSize","Maximum Number of page should not exceed "+ maxTotalPages));
			
		}
		
		double timeGap =0;
		
		urlList = OptionsUtils.getLabelValueBeans(Constants.TIME_GAP_PAGE_COUNT_INCREMENT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			if(AstronValidator.isPositiveInt(Url)){
				timeGap = new Integer(Url).intValue();
			}
		}
		
		 urlList = new ArrayList();
		 Url = null;
		int minimumPageNumber =0;
		
		urlList = OptionsUtils.getLabelValueBeans(Constants.MINIMUM_JUSTIFICATION_PAGE_NUMBER ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			if(AstronValidator.isPositiveInt(Url)){
				minimumPageNumber = new Integer(Url).intValue();
			}
		}
		
		double requestedTimeVal =timeGap; // null pointer check for existing proposal who does not have minimum hours
		String requestedTimeHours=AstronConverter.toString(NorthStarDelegate.getResourceValue(
				observingRequest.getAllocations(),
				telescopeName+'_'+OpticonConstants.GLOBAL));
		if(requestedTimeHours!=null)
			{Double value =AstronConverter.toDouble(requestedTimeHours);
			if(value !=null) 
			requestedTimeVal =value.doubleValue();
			}
		
		/***
		 * < 250 hours: up to 4 pages
		 * < 500 hours: up to 5 pages
		 * < 750 hours: up to 6 pages
		 * < 1000 hours: up to 7 pages
		 * > =1000 hours: up to 8 pages
		 */
		if(requestedTimeVal<=timeGap) {
			if(totalfilepages>minimumPageNumber)
				error.getErrors().add(new ProposalError("maximumFileSize",
						"Page limits are dependent on the amount of observing time requested. &nbs observing time requested is less than"+ timeGap +"hours, therefore, page limit must not exceed"+minimumPageNumber+"." ));	
		}
		else if(timeGap<requestedTimeVal && requestedTimeVal <=(2*timeGap)) {
			
			if(totalfilepages>(minimumPageNumber+1))
				error.getErrors().add(new ProposalError("maximumFileSize",
						"Page limits are dependent on the amount of observing time requested. &nbs observing time requested is less than "+2*timeGap +" hours, therefore, page limit must not exceed "+(minimumPageNumber+1)+"."));	
		}
		else if((2*timeGap)<requestedTimeVal && requestedTimeVal <=(3*timeGap)) {
			if(totalfilepages>(minimumPageNumber+2))
				error.getErrors().add(new ProposalError("maximumFileSize",
						"Page limits are dependent on the amount of observing time requested. &nbs observing time requested is less than "+3* timeGap +" hours, therefore, page limit must not exceed "+(minimumPageNumber+2)+"."));	
		} 
		else if((3*timeGap)<requestedTimeVal && requestedTimeVal <=(4*timeGap)) {
			if(totalfilepages>(minimumPageNumber+3))
				error.getErrors().add(new ProposalError("maximumFileSize",
						"Page limits are dependent on the amount of observing time requested. &nbs observing time requested is less than "+4* timeGap +" hours, therefore, page limit must not exceed "+(minimumPageNumber+3)+"."));	
		} 
		else if(requestedTimeVal>(4*timeGap)) {
			int maxTotalFilePages=0; //telescopeConfiguration.getMaxPublicationCount();
			urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_TOTAL_PAGECOUNT ,
						new HashMap(), contextConfiguration);
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
				Url =  urlValue.getValue();
				maxTotalFilePages = AstronConverter.toInteger(Url).intValue();
			}
			
			if(totalfilepages>maxTotalFilePages)
				error.getErrors().add(new ProposalError("maximumFileSize",
						"Page limits are dependent on the amount of observing time requested. &nbs observing time requested is more than 1000 hours, therefore, page limit must not exceed 8."));	
		}
		

		return error;
			
	}
    
    public Double getTotalTime(OpticonObservingRequest opticonObservingRequest)
    {
    	double result = 0;
    	Iterator obsIt = opticonObservingRequest.getObservations().iterator();
    	Set telescopeSet = new HashSet(); 
    	// Sets only have unique values (removing double telescopes)
    	while (obsIt.hasNext())
    	{
    		OpticonObservation observation = (OpticonObservation) obsIt.next();
    		if (observation.getTelescopeConfiguration()!= null)
    		{
    			telescopeSet.add(observation.getTelescopeConfiguration());
    		}
    	}
    	Iterator telescopeIt = new ArrayList(telescopeSet).iterator();
    	while (telescopeIt.hasNext())
		{
			String telescopeName = (String) telescopeIt.next();
			Double time = NorthStarDelegate.getResourceValue(
					opticonObservingRequest.getAllocations(), telescopeName
							+ '_' + OpticonConstants.BRIGHT);
			if (time != null)
			{
				result += time.doubleValue();
			}
			// nights =
			// NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
			// telescopeName + '_' + OpticonConstants.GREY);
			// if (nights != null)
			// {
			// result += nights.doubleValue();
			// }
			time = NorthStarDelegate.getResourceValue(opticonObservingRequest
					.getAllocations(), telescopeName + '_'
					+ OpticonConstants.DARK);
			if (time != null)
			{
				result += time.doubleValue();
			}
			time = NorthStarDelegate.getResourceValue(opticonObservingRequest
					.getAllocations(), telescopeName + '_'
					+ OpticonConstants.FIRST_Q);
			if (time != null)
			{
				result += time.doubleValue();
			}
			time = NorthStarDelegate.getResourceValue(opticonObservingRequest
					.getAllocations(), telescopeName + '_'
					+ OpticonConstants.LAST_Q);
			if (time != null)
			{
				result += time.doubleValue();
			}
			time = NorthStarDelegate.getResourceValue(opticonObservingRequest
					.getAllocations(), telescopeName + '_'
					+ OpticonConstants.GLOBAL);
			if (time != null)
			{
				result += time.doubleValue();
			}
		}

		if (result == 0)
		{
			return null;
		}
		else
		{
			return new Double(result);
		}

	}
    
	public void setProposalCode(Proposal proposal) throws DatabaseException,
			DeadLinePassedException
	{
		Semester semester = northStarDelegate.getSemester(proposal
				.getSemester().getId());
		/*
		 * no code is assigned or code starts with no valid prefix (e.g. prefix
		 * of last semester)
		 */
		if (proposal.getCode() == null
				|| !proposal.getCode().startsWith(semester.getPrefix()))
		{

			Calendar currentDate = NorthStarDelegate.getCurrentDate();
			if (!currentDate.getTime().before(semester.getDeadLine()))
			{
				throw new DeadLinePassedException();
			}

			semester.setLastAssigned(new Integer(semester.getLastAssigned()
					.intValue() + 1));
			repository.store(semester);
			
			proposal.setCode(getProposalCode(semester, getTelescopePrefix(proposal)));
		}

	}

	/*
	 * 
	 */
	protected String getTelescopePrefix(Proposal proposal)
	{
		Set telescopeSet = new HashSet();
		Iterator observationsIt = proposal.getObservingRequest().getObservations().iterator();
		while (observationsIt.hasNext())
		{
			OpticonObservation opticonObservation = (OpticonObservation) observationsIt.next();
			String telescope = opticonObservation.getTelescopeConfiguration();
			telescopeSet.add(telescope);
		}
		// if empty set "multi" this should not happen anyways.
		// if contains one element, the proposal only has one telescope.
		if (telescopeSet.size() == 1)
		{
			Iterator it = telescopeSet.iterator();
			return (String) it.next();
		}
		//if (telescopeSet.size() == 0)
		return proposal.getSemester().getTelescope();
		//return prefix;
	}
	
	/*
	 * Return TEL-SEMESTER-CAT-nnn (non-Javadoc)
	 * @see eu.radionet.northstar.business.ProposalDelegate#getProposalCode(eu.radionet.northstar.data.entities.Semester)
	 */
	protected String getProposalCode(Semester semester, String telescope) {
		return super.getProposalCode(semester);

	}
	
	public Map getAllocations(ObservingRequest observingRequest){
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
		if(opticonObservingRequest.getAllocations() != null){
			return opticonObservingRequest.getAllocations();
		}
		return new HashMap();
	}
	
	public  String getObservationInstrumentName(Observation observation){
		OpticonObservation opticonObservation = (OpticonObservation) observation;
		return opticonObservation.getInstrument().getName();
	}

}

