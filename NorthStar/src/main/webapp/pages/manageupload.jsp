<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />

<html:form action="/processManageUpload" enctype="multipart/form-data" method="POST">
<html:hidden property="currentConfigFileUrl" name="manageUploadForm"/>
<bean:define id="currentConfigFileUrl" name="manageUploadForm" property="currentConfigFileUrl" type="java.lang.String" />

	<TABLE>
		<tr><td align="right">
	<logic:empty name="manageUploadForm" property="currentConfigFileUrl">
			&nbsp;
			</tr></td>
			<tr><td>
			There is no telescope to manage with this useraccount<br/>
			Please contact the administrator if you did not expect this.<br/>
			</td></tr>
		</logic:empty>
		<logic:notEmpty name="manageUploadForm" property="currentConfigFileUrl">
		

				<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
							<TABLE width="100" height="25">
								<TR>
						  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/manager/manager_help.jsp\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
					  				<TD width="80" height="25"><%= "<a title=\"Opens help\" href=\"" + request.getContextPath() + "/manager/manager_help.jsp\" target=\"help\">Help</a>"%></TD>
				  				</TR>  				
				  			</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />	
			</td></tr>
	
			<tr><td>
			<TABLE>
		
			<logic:equal name="manageUploadForm" property="hasMultipleAccess" value="true">
				<tr><td><br><br></td><td>Telescope:</td><td> 
				<bean:define id="mytelescopelist" name="manageUploadForm" property="userTelescopes"/>
				<html:select name="manageUploadForm" property="selectedTelescope" onchange="document.forms[0].submit()">
					<html:options collection="mytelescopelist" property="telescope" labelProperty="telescope"/>
				</html:select>
				</td></tr>
			</logic:equal>
			<logic:equal name="manageUploadForm" property="hasMultipleAccess" value="false">
				<tr><td><br><br></td><td>Telescope:</td><td>
				 			<bean:write name="manageUploadForm" property="selectedTelescope"/>
				 			<html:hidden name="manageUploadForm" property="selectedTelescope"/>
				 </td></tr>
			</logic:equal>
			<tr><td><br><br></td><td>
				Current config: 
				</td><td>
					<html:link action='<%="/processManageUpload.do?readConfigFile="+currentConfigFileUrl%>'>
								<bean:write name="manageUploadForm" property="currentConfigFileUrl"/>
						</html:link>
						( <bean:write name="manageUploadForm" property="currentConfigFileDate"/> )
				</td></tr>
				<tr><td><br><br></td><td>
				Upload config file: 
				</td><td>
					<logic:equal name="manageUploadForm" property="configOk" value="false">
						<html:file  property="configFile" size="60" style="textarea"/>
						<html:submit property="uploadFileButton"  title="Upload" styleClass="list_upload">Upload</html:submit>
					</logic:equal>
					<logic:equal name="manageUploadForm" property="configOk" value="true">
						
						<font color="green"> OK</font>
					</logic:equal>
					<span class="error"><astron:errors property="manageUpload" ifnotexist=""/></span>
				</td></tr>
				<tr><td><br><br></td><td>
	
				previous config file: 
				</td><td>
					( <bean:write name="manageUploadForm" property="previousConfigFileDate"/> )
					<html:link action='<%="/processManageUpload.do?restoreConfigFile="+currentConfigFileUrl%>'> restore </html:link>
				</td></tr>
			<tr><td><br><br></td><td>
	
		
			</TABLE>
		</logic:notEmpty>
		</td></tr>		
		<tr><td>
			&nbsp; <span class="error"><astron:errors property="uploadConfig" ifnotexist=""/></span>
		</td></tr>
	</TABLE>
	<table width="100%">
		<tr>
			<td class="right">
				<br>
		</tr>
	</table>

</html:form>




