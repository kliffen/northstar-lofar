// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal;

import java.io.Serializable;

import nl.astron.util.AstronConverter;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.data.entities.Semester;

public class SemesterBean implements Serializable {
	protected String semester = null;
	protected String telescope = null;
	protected String community = null;
	protected String category = null;
	protected String deadLine = null;
	protected String startTime = null;
	protected String endTime = null;
	public SemesterBean(Semester semester){
		this.semester = semester.getSemester();
		this.telescope = semester.getTelescope();
		this.category = semester.getCategory().getCode();
		this.community = NorthStarConfiguration.getCommunityDescription(semester.getCommunity());
		this.deadLine = AstronConverter.toDateString(semester.getDeadLine());
		this.startTime = AstronConverter.toDateString(semester.getStartDate());
		this.endTime = AstronConverter.toDateString(semester.getEndDate());
	}
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public String getSemester() {
		return semester;
	}
	public void setSemester(String semester) {
		this.semester = semester;
	}
	public String getTelescope() {
		return telescope;
	}
	public void setTelescope(String telescope) {
		this.telescope = telescope;
	}

	public String getDeadLine() {
		return deadLine;
	}

	public void setDeadLine(String deadLine) {
		this.deadLine = deadLine;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
}
