<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />
<html:form action="/processOverwriteProposalData" method="GET">
<TABLE>
	
	<TR>
		<TD>
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				<TD valign='top'><PRE>
 There already is a proposal open for editing!
 
 All changes since the last save action in the proposal being edited
 will be lost if you continue!

 Are you really sure that you want to proceed?
</PRE>
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;<html:hidden name="overwriteProposalDataForm" property="forward"/></TD>
	</TR>
	<TR>
		<TD class="header"><astron:label key="label.overwriteproposal.session"/></TD>
	</TR>
	<TR>
		<TD>
		<tiles:get name="box_header_no_top" />
			<table class="center" width="100%">
				<TR>
					<TD width="70" class="right"><astron:label key="label.overwriteproposal.code"/></TD>
					<TD class="bold" ><bean:write name="overwriteProposalDataForm" property="oldProposal.code"/></TD>
				</TR>				
				<TR>
					<TD width="70"  class="right"><astron:label key="label.overwriteproposal.title"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="oldProposal.title"/></TD>
				</TR>		
				<TR>
					<TD width="70"  class="right"><astron:label key="label.overwriteproposal.author"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="oldProposal.contactAuthor"/></TD>
				</TR>						
				<TR>
					<TD  width="70" class="right"><astron:label key="label.overwriteproposal.semester"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="oldProposal.semester"/></TD>
				</TR>
				<TR>
					<TD width="70"  class="right"><astron:label key="label.overwriteproposal.community"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="oldProposal.community"/></TD>
				</TR>
				<TR>
					<TD width="70"  class="right"><astron:label key="label.overwriteproposal.telescope"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="oldProposal.telescope"/></TD>
				</TR>
				<TR>
					<TD width="70"  class="right"><astron:label key="label.overwriteproposal.category"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="oldProposal.category"/></TD>
				</TR>			
			</table>
		<tiles:get name="box_footer" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>	
	<TR>
		<TD class="header"><astron:label key="label.overwriteproposal.edit"/></TD>
	</TR>
	<TR>
		<TD>
		<tiles:get name="box_header_no_top" />
			<table class="center" width="100%">
				<TR>
					<TD width="70" class="right"><astron:label key="label.overwriteproposal.code"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="newProposal.code"/></TD>
				</TR>				
				<TR>
					<TD width="70" class="right"><astron:label key="label.overwriteproposal.title"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="newProposal.title"/></TD>
				</TR>		
				<TR>
					<TD width="70" class="right"><astron:label key="label.overwriteproposal.author"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="newProposal.contactAuthor"/></TD>
				</TR>				
				<TR>
					<TD width="70" class="right"><astron:label key="label.overwriteproposal.semester"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="newProposal.semester"/></TD>
				</TR>
				<TR>
					<TD width="70" class="right"><astron:label key="label.overwriteproposal.community"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="newProposal.community"/></TD>
				</TR>
				<TR>
					<TD width="70" class="right"><astron:label key="label.overwriteproposal.telescope"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="newProposal.telescope"/></TD>
				</TR>
				<TR>
					<TD width="70" class="right"><astron:label key="label.overwriteproposal.category"/></TD>
					<TD class="bold" align="left"><bean:write name="overwriteProposalDataForm" property="newProposal.category"/></TD>
				</TR>			
			</table>
		<tiles:get name="box_footer" />
		</TD>
	</TR>	
	<TR>
		<TD>&nbsp;</TD>
	</TR>	
	</TABLE>
<TABLE width="100%">
	<TR>
		<TD align="left"><html:submit property="submitButton" styleClass="list_accept_yes" title="Ok">Yes</html:submit></TD>
		
		<TD class="right"><html:cancel styleClass="list_decline_t" title="Cancel submit">No</html:cancel>
		</TD>
	</TR>
</TABLE>
</html:form>
