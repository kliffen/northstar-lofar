// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Jan 13, 2004
 */
package eu.radionet.northstar.data.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import nl.astron.util.AstronValidator;

/**
 * @author anton
 */
public class Proposal implements Serializable {
	protected Integer id = null;

	protected String code = null;

	protected Integer northStarId = null;

	protected Semester semester = null;

	protected Justification justification = null;

	protected List observingRequests = new ArrayList();

	protected AdditionalIssues additionalIssues = null;

	protected List members = new ArrayList();

	protected List statusHistory = new ArrayList();

	protected ProposalStatus currentStatus = null;

	protected String version = null;

	protected Integer parentId = null;
	
	/**
	 * @return Returns the additionalIssues.
	 */
	public AdditionalIssues getAdditionalIssues() {
		return additionalIssues;
	}

	/**
	 * @param additionalIssues
	 *            The additionalIssues to set.
	 */
	public void setAdditionalIssues(AdditionalIssues additionalIssues) {
		this.additionalIssues = additionalIssues;
	}

	/**
	 * @return Returns the code.
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code
	 *            The code to set.
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return Returns the id.
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            The id to set.
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return Returns the justification.
	 */
	public Justification getJustification() {
		return justification;
	}

	/**
	 * @param justification
	 *            The justification to set.
	 */
	public void setJustification(Justification justification) {

		this.justification = justification;

	}

	/**
	 * @return Returns the members.
	 */
	public List getMembers() {
		return members;
	}

	/**
	 * @param members
	 *            The members to set.
	 */
	public void setMembers(List proposalMembers) {
		this.members = proposalMembers;
	}

	/**
	 * @return Returns the statusHistory.
	 */
	public List getStatusHistory() {
		return statusHistory;
	}

	/**
	 * @param statusHistory
	 *            The statusHistory to set.
	 */
	public void setStatusHistory(List statusHistory) {
		this.statusHistory = statusHistory;
	}

	public void addChangeMember(Member member) {

		/*
		 * if id is null, we need an fake id
		 */
		if (member.getId() == null && member.getFakeId() == null) {
			int fakeId = 1;
			/*
			 * iterator through observations
			 */
			for (int i = 0; i < members.size(); i++) {
				Member memberTemp = (Member) members.get(i);
				/*
				 * if saved in database, observation has id
				 */
				if (memberTemp.getId() != null) {
					/*
					 * if observationId >= fakeId then fakeId = observationId +
					 * 1
					 */
					if (memberTemp.getId().intValue() >= fakeId) {
						fakeId = memberTemp.getId().intValue() + 1;
					}
				}
				/*
				 * if observationFakeId >= fakeId then fakeId = observationId +
				 * 1
				 */
				else if (memberTemp.getFakeId().intValue() >= fakeId) {
					fakeId = memberTemp.getFakeId().intValue() + 1;
				}

			}
			member.setFakeId(new Integer(fakeId));
			this.members.add(member);
		} else {
			boolean found = false;
			for (int i = 0; i < members.size() && !found; i++) {
				Member memberTemp = (Member) members.get(i);
				if (member.getId() != null && memberTemp.getId() != null) {
					if (member.getId().intValue() == memberTemp.getId()
							.intValue()) {
						members.set(i, member);
						found = true;
					}
				} else if (member.getFakeId() != null
						&& memberTemp.getFakeId() != null) {
					if (member.getFakeId().intValue() == memberTemp.getFakeId()
							.intValue()) {
						members.set(i, member);
						found = true;
					}
				}
			}
		}
	}

	public void removeMember(Member memberToBeRemoved) {
		boolean found = false;
		for (int i = members.size() - 1; i >= 0 && !found; i--) {
			Member member = (Member) members.get(i);
			/*
			 * if member is saved in the databased
			 */
			if (member.getId() != null && memberToBeRemoved.getId() != null) {
				/*
				 * check if ids are equal
				 */
				if (member.getId().intValue() == memberToBeRemoved.getId()
						.intValue()) {
					this.members.remove(member);
					found = true;
				}
			}
			/*
			 * if member isn't save check if fake id is the same
			 */
			else if (member.getFakeId() != null
					&& memberToBeRemoved.getFakeId() != null) {
				if (member.getFakeId().intValue() == memberToBeRemoved
						.getFakeId().intValue()) {
					this.members.remove(member);
					found = true;
				}
			}
		}
	}

	public void setCurrentProposalStatus(Status status, Integer userId) {
		Calendar cal = Calendar.getInstance();
		ProposalStatus proposalStatus = new ProposalStatus();
		proposalStatus.setUserId(userId);
		proposalStatus.setStatusTime(cal.getTime());
		proposalStatus.setStatus(status);
		setCurrentStatus(proposalStatus);
		statusHistory.add(proposalStatus);
	}


	public Member getMember(Integer memberId) {
		Iterator memberIterator = members.iterator();
		while (memberIterator.hasNext()) {
			Member member = (Member) memberIterator.next();
			if (member.getId() != null
					&& member.getId().intValue() == memberId.intValue()) {
				return member;
			}
		}
		return null;
	}

	/**
	 * @return Returns the observingRequests.
	 */
	public List getObservingRequests() {
		return observingRequests;
	}

	/**
	 * @param observingRequests
	 *            The observingRequests to set.
	 */
	public void setObservingRequests(List observingRequests) {
		this.observingRequests = observingRequests;
	}

	public void addObservingRequest(ObservingRequest observingRequest) {
		this.observingRequests.clear();
		this.observingRequests.add(observingRequest);
	}

	public ObservingRequest getObservingRequest() {
		Iterator observingRequestsIterator = observingRequests.iterator();
		if (observingRequestsIterator.hasNext()) {
			return (ObservingRequest) observingRequestsIterator.next();
		}
		return null;
	}

	/**
	 * The isSubmitted method return true is submitted
	 * 
	 * @return
	 */
	public boolean isSubmitted() {
		if (AstronValidator.isBlankOrNull(code)) {
			return false;
		} else {
			return true;
		}
	}

	public Member getPi() {
		Member pi = null;
		Iterator mit = members.iterator();
		boolean found = false;
		while (mit.hasNext() && !found) {
			Member m = (Member) mit.next();
			if (m.isPi()) {
				found = true;
				pi = m;
			}
		}
		return pi;
	}

	public RegisteredMember getContactAuthor() {
		Member correspondingAuthor = null;
		Iterator mit = members.iterator();
		boolean found = false;
		while (mit.hasNext() && !found) {
			Member m = (Member) mit.next();
			if (m.isContactAuthor()) {
				found = true;
				correspondingAuthor = m;
			}
		}
		return (RegisteredMember) correspondingAuthor;
	}

	public Integer getNorthStarId() {
		return northStarId;
	}

	public void setNorthStarId(Integer northStarId) {
		this.northStarId = northStarId;
	}

	public ProposalStatus getCurrentStatus() {
		return currentStatus;
	}

	public void setCurrentStatus(ProposalStatus currentStatus) {
		this.currentStatus = currentStatus;
	}

	public String getLastName(String name) {
		if (name == null) {
			return null;
		}
		String[] nameParts = name.split(" ");
		return nameParts[nameParts.length - 1];
	}

	public Semester getSemester() {
		return semester;
	}

	public void setSemester(Semester semester) {
		this.semester = semester;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public Integer getParentId() {
		return parentId;
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
}