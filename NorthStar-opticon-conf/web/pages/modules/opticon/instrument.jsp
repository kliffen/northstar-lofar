<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>

 <tr>
	<td class="header" colspan="2"><astron:label key="label.opticon.observation.telescope.configuration" /><!--Telescope configuration :--></td>
</tr>	

<tr>
	<td colspan="2"><tiles:get name="box_header_no_top" />


	<table width="100%">
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<!-- the instrument bean is defined as a request attribute in the observationSetUp page -->
		<logic:equal name="instrumentBean" property="displayTelescopeDropbox" value="true">		
			<tr>
				<td class="right"><astron:label key="label.opticon.observation.choose.telescope" /><!--Choose a telescope&nbsp;:&nbsp;--></td>
				<td class="left"><bean:define id="telescopeConfigurations" name="instrumentBean" property="telescopeConfigurations" /> 
					<html:select name="instrumentBean" property="selectedTelescopeConfiguration" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
						<html:options collection="telescopeConfigurations" property="value"	labelProperty="label" />
					</html:select><span class="error">*</span>
				</td>
			</tr>
		</logic:equal>
		<logic:equal name="instrumentBean" property="displayTelescopeDropbox" value="false">		
			<tr>
				<td class="right"><astron:label key="label.opticon.observation.telescope" /><!--Telescope&nbsp;:&nbsp;--></td>
				<td class="left"><bean:write name="instrumentBean" property="selectedTelescopeConfiguration"/>
					<html:hidden name="instrumentBean" property="selectedTelescopeConfiguration" />
				</td>
			</tr>
		</logic:equal>
		
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<logic:notEmpty name="instrumentBean" property="selectedTelescopeConfiguration">
						<tr>
							<td class="right"><astron:label key="label.opticon.observation.choose.instrument" /><!--Choose an instrument&nbsp;:&nbsp;--></td>
							<td class="left"><bean:define id="instrumentConfigurations" name="instrumentBean" property="instrumentConfigurations" /> 
								<html:select name="instrumentBean" property="selectedInstrumentConfiguration" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
									<html:options collection="instrumentConfigurations" property="value"	labelProperty="label" />
								</html:select><span class="error">*</span>
							</td>
						</tr>
				<tr>
					<td>&nbsp;</td>
					<td width="60%"></td> <!--  now the fields stay fixed -->
				</tr>
				<logic:notEmpty name="instrumentBean" property="selectedInstrumentConfiguration">
							<logic:notEmpty name="instrumentBean" property="instrumentUrl">
								 <tr>
			  							<td class="right" valign="top"><astron:label key="label.opticon.observation.documentation" /><!--  Instrument documentation : --></td>
			  							<td class="left"><a href="<bean:write name="instrumentBean" property="instrumentUrl" />" target="_blank" >
																 <bean:write name="instrumentBean" property="instrumentUrl" /> </a>
			  							</td>
			 					 </tr><tr>
										<td colspan="2">&nbsp;</td>
								 </tr>
		 					 </logic:notEmpty>
							<logic:equal name="instrumentBean" property="displayStation" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.station" /><!--Choose Station&nbsp;:&nbsp;--></td>
								<td class="left"><bean:define id="stations"
									name="instrumentBean" property="stations" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedStation" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
									<html:options collection="stations" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredStation" value="true">
										<span class="error"><astron:errors property="selectedStation" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredStation" value="false">
										<span class="error"><astron:errors property="selectedStation" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displaySingleStationSpecifics" value="true">
							 <tr>
		  							<td class="right" valign="top"><astron:label key="label.opticon.observation.specifics" /><!--  Any preferences? : --></td>
		  							<td class="left"><html:text name="instrumentBean" property="singleStationSpecifics" size="44" />
		  							</td>
		 					 </tr>
		 					 </logic:equal>
							<logic:equal name="instrumentBean" property="selectedStation" value="Custom">
							 <tr>
		  							<td class="right" valign="top"><astron:label key="label.opticon.observation.specify" /><!-- Please Specify : --></td>
		  							<td class="left"><html:text name="instrumentBean" property="customStationSpecifics" size="44" />
		  							</td>
		 					 </tr>
		 					 <tr><td clospan="2">&nbsp;</td></tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayClock" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.clock" /><!--Choose Clock speed&nbsp;:&nbsp;--></td>
								<td class="left"><bean:define id="clocks"
									name="instrumentBean" property="clocks" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedClock">
									<html:options collection="clocks" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredClock" value="true">
										<span class="error"><astron:errors property="selectedClock" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredClock" value="false">
										<span class="error"><astron:errors property="selectedClock" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							
							<logic:equal name="instrumentBean" property="displayOneFilter" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.filter" /><!--Choose filter&nbsp;:&nbsp;--></td>
								<td class="left"><bean:define id="oneFilters"
									name="instrumentBean" property="oneFilters" /> 
									<html:select styleClass="selectwide" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()" name="instrumentBean" property="selectedOneFilter">
									<html:options collection="oneFilters" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredOneFilter" value="true">
										<span class="error"><astron:errors property="selectedOneFilter" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredOneFilter" value="false">
										<span class="error"><astron:errors property="selectedOneFilter" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displaySubFilter" value="true">
								<tr><td class="right"><astron:label key="label.opticon.observation.central.frequency" /><!--Central frequency: (MHz)&nbsp;:&nbsp;-->
									</td><td class="left"> <html:text name="instrumentBean" property="filterFrequency" size="4" />
								</td></tr>
								<tr><td class="right"><astron:label key="label.opticon.observation.bandwidth" /><!--Bandwidth (MHz)&nbsp;:&nbsp; -->
									</td><td class="left"><html:text name="instrumentBean" property="filterBandwidth" size="4" />
								</td></tr>
								<tr><td class="right"><astron:label key="label.opticon.observation.contiguous" /><!--Contiguous Coverage :&nbsp; --></td><td class="left">
									<html:radio styleClass="radio" name="instrumentBean" property="filterContiguousCoverage" value="true" onclick="document.forms[0].submit()" >yes</html:radio>
										<html:radio styleClass="radio" name="instrumentBean" property="filterContiguousCoverage" value="false"  onclick="document.forms[0].submit()">no</html:radio></td>
								</tr>								
								<logic:equal name="instrumentBean" property="filterContiguousCoverage" value="false">
									<tr><td class="right"> <astron:label key="label.opticon.observation.specify" /><!--please specify -->
									</td><td class="left"><html:textarea name="instrumentBean" property="filterDetails" cols="30" rows="2"/></td></tr>
								</logic:equal>	
							</logic:equal>
							
							<logic:equal name="instrumentBean" property="displayAntenna" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.antenna" /><!-- Choose Antenna&nbsp;:&nbsp;--></td>
								<td class="left"><bean:define id="antennas"
									name="instrumentBean" property="antennas" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedAntenna">
									<html:options collection="antennas" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredAntenna" value="true">
										<span class="error"><astron:errors property="selectedAntenna" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredAntenna" value="false">
										<span class="error"><astron:errors property="selectedAntenna" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							
							<logic:equal name="instrumentBean" property="displayIntegrationTime" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.integration.time" /><!--Integration time (seconds)&nbsp;:&nbsp;--></td>
								<td class="left"><html:text name="instrumentBean" property="integrationTime" size="4" />
								</td>
							</tr>
							</logic:equal>
								
							<logic:equal name="instrumentBean" property="displayDirectDataStorage" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.direct.datastorage" /></td>
								<td class="left">									
									<html:radio styleClass="radio" name="instrumentBean" property="directDataStorage" value="true" onclick="document.forms[0].submit()" >yes</html:radio>
									<html:radio styleClass="radio" name="instrumentBean" property="directDataStorage" value="false"  onclick="document.forms[0].submit()">no</html:radio></td>

								</td>
							</tr>
							</logic:equal>
								
							<logic:equal name="instrumentBean" property="displayInstrumentDetails" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.specify" /><!--Please specify&nbsp;:&nbsp; --></td>
								<td class="left">
									<html:textarea property="instrumentDetails" cols="60" rows="2"/>
									<logic:equal name="instrumentBean" property="requiredFilterDetails" value="true">
										<span class="error">*</span>
									</logic:equal>
								</td>
							</tr>
							</logic:equal>		
							
							<logic:equal name="instrumentBean" property="displayFrameExposureTime" value="true">
							<tr>
								<td class="right" valign="top"><astron:label key="label.opticon.observation.special.exposuretime" /></td>
								<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="nonDefaultFrameExposureTime" value="true"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="nonDefaultFrameExposureTime" value="false"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
								</td>
							</tr>
							<logic:equal name="instrumentBean" property="nonDefaultFrameExposureTime" value="true">								
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.specify" /><!--Please specify&nbsp;:&nbsp; --></td>
								<td class="left">
									<html:textarea name="instrumentBean" property="nonDefaultFrameExposureTimeDetails" cols="60" rows="2"/>
									<logic:equal name="instrumentBean" property="requiredNonDefaultFrameExposureTimeDetails" value="true">
										<span class="error"><astron:errors property="nonDefaultFrameExposureTimeDetails" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredNonDefaultFrameExposureTimeDetails" value="false">
										<span class="error"><astron:errors property="nonDefaultFrameExposureTimeDetails" ifnotexist="" /></span>
									</logic:equal>									
								</td>
							</tr>							
							</logic:equal>	
							</logic:equal>			
							<% String styleClass = ""; %>			
							<logic:equal name="instrumentBean" property="displayFilters" value="true">
							<tr>
								<td> &nbsp; </td>
								<td>
									<table align="left" class="projecttable">
										<TR>
							 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.filters" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
										</TR>							
										<%	int i=0; %>									
										<logic:iterate id="filterItem"name="instrumentBean" property="filters">
											<% styleClass = (i++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedFilters">
 													<bean:write name="filterItem" property="value" filter="false"/>
												</html:multibox></td><td class="tablefield_pri<%=styleClass%>">
												<bean:write name="filterItem" property="label"/>
											</td></tr>
										</logic:iterate>
										<logic:equal name="instrumentBean" property="displayCustomFilter" value="true">
											<% styleClass = (i++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedFilters" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Other/Custom</html:multibox>
												</td><td class="tablefield_pri<%=styleClass%>">
												Other/Custom
											</td></tr>
										</logic:equal>
									</table>
								<logic:equal name="instrumentBean" property="displayFiltersw2" value="true">
									<table align="left" class="projecttable">
										<TR>
							 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.filtersw2" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
										</TR>							
										<%	int j=0; %>										
										<logic:iterate id="filterItemw2"name="instrumentBean" property="filtersw2">
											<% styleClass = (j++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedFiltersw2">
 													<bean:write name="filterItemw2" property="value" filter="false"/>
												</html:multibox></td><td class="tablefield_pri<%=styleClass%>">
												<bean:write name="filterItemw2" property="label"/>
											</td></tr>
										</logic:iterate>
																						
									</table>
								</logic:equal>
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayGrismw" value="true">
							<tr><<td> &nbsp; </td>
								<td>
									<table align="left" class="projecttable">
										<TR>
							 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.choose.grism" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
										</TR>							
										<%	int j=0; %>										
										<logic:iterate id="grismwItem" name="instrumentBean" property="grismw">
											<% styleClass = (j++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedGrismw" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
 													<bean:write name="grismwItem" property="value" filter="false"/>
												</html:multibox></td><td class="tablefield_pri<%=styleClass%>">
												<bean:write name="grismwItem" property="label"/>
											</td></tr>
										</logic:iterate>
																						
									</table>
								</td></tr>	
							</logic:equal>
							<logic:equal name="instrumentBean" property="displaySlitw" value="true">
							<tr><<td> &nbsp; </td>
								<td>
									<table align="left" class="projecttable">
									<TR>
							 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.choose.slits" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
										</TR>							
										<%	int j=0; %>										
										<logic:iterate id="slitwItem" name="instrumentBean" property="slitw">
											<% styleClass = (j++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedSlitw" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
 													<bean:write name="slitwItem" property="value" filter="false"/>
												</html:multibox></td><td class="tablefield_pri<%=styleClass%>">
												<bean:write name="slitwItem" property="label"/>
											</td></tr>
										</logic:iterate>
																						
									</table>
								</td></tr>	
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayGratingw" value="true">
							<tr><<td> &nbsp; </td>
								<td>
									<table align="left" class="projecttable">
									<TR>
							 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.choose.gratings" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
										</TR>							
										<%	int j=0; %>										
										<logic:iterate id="gratingwItem" name="instrumentBean" property="gratingw">
											<% styleClass = (j++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedGratingw" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
 													<bean:write name="gratingwItem" property="value" filter="false"/>
												</html:multibox></td><td class="tablefield_pri<%=styleClass%>">
												<bean:write name="gratingwItem" property="label"/>
											</td></tr>
										</logic:iterate>
																						
									</table>
								</td></tr>	
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayFilterDetails" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.specify" /><!--Please specify&nbsp;:&nbsp; --></td>
								<td class="left">
									<html:textarea name="instrumentBean" property="filterDetails" cols="60" rows="2"/>
									<logic:equal name="instrumentBean" property="requiredFilterDetails" value="true">
										<span class="error">*</span>
									</logic:equal>
								</td>
							</tr>
							</logic:equal>							
							<logic:equal name="instrumentBean" property="displayMode" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.mode" /><!--Choose mode&nbsp;:&nbsp; --></td>
								<td class="left">
									<bean:define id="modes" name="instrumentBean" property="modes" /> 
									<html:select name="instrumentBean" styleClass="selectwide" property="selectedMode">
										<html:options collection="modes" property="value" labelProperty="label" />
									</html:select>
									<logic:equal name="instrumentBean" property="requiredMode" value="true">
										<span class="error">*</span>
									</logic:equal>
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayPolarimetry" value="true">
							<tr>
								<td class="right" valign="top"><astron:label key="label.opticon.observation.polarimetry" /></td>
								<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="polarimetry" value="true" >Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="polarimetry" value="false" >No</html:radio></td>
							</tr>	
							</logic:equal>							
							<logic:equal name="instrumentBean" property="displayGuideline" value="true">
							<tr>
							<td class="right"><astron:label key="label.opticon.observation.guideline" /></td>
							<td class="left">
							<a href="<bean:write name="instrumentBean" property="guidelineUrl" />" >
							 <bean:write name="instrumentBean" property="guidelineUrl" /> </a>
							</td> </tr>
							<tr>
								<td class="right" valign="top">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Accept guideline? :&nbsp;</td>
								<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="guideline" value="true" >Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="guideline" value="false" >No</html:radio>
												<span class="error"><astron:errors property="guideline" ifnotexist="*" /></span>				
								</td>
							</tr>	
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayWavelength" value="true">
							<tr>
								<td class="right">
									<astron:label key="label.opticoninstrument.wavelength"/>&nbsp;
								</td>
								<td class="left"><html:text name="instrumentBean" property="wavelength" size="15" />
									<logic:equal name="instrumentBean" property="requiredWavelength" value="true">
										<span class="error"><astron:errors property="wavelength" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredWavelength" value="false">
										<span class="error"><astron:errors property="wavelength" ifnotexist="" /></span>
									</logic:equal>										
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayOrder" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.order" /></td>
								<td class="left"><html:text name="instrumentBean" property="order" size="15" />
									<logic:equal name="instrumentBean" property="requiredOrder" value="true">
										<span class="error"><astron:errors property="order" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredOrder" value="false">
										<span class="error"><astron:errors property="order" ifnotexist="" /></span>
									</logic:equal>		
								</td>									
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displaySlits" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.slits" /></td>
								<td class="left"><bean:define id="slits"
									name="instrumentBean" property="slits" /> <html:select
									 styleClass="selectwide" name="instrumentBean" property="selectedSlit">
									<html:options collection="slits" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredSlits" value="true">
										<span class="error"><astron:errors property="selectedSlit" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredSlits" value="false">
										<span class="error"><astron:errors property="selectedSlit" ifnotexist="" /></span>
									</logic:equal>									
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displaySlitPositionAngle" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.slit.angle" /></td>
								<td class="left"><html:text name="instrumentBean" property="slitPositionAngle" size="15" />
									<logic:equal name="instrumentBean" property="requiredSlitPositionAngle" value="true">
										<span class="error"><astron:errors property="slitPositionAngle" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredSlitPositionAngle" value="false">
										<span class="error"><astron:errors property="slitPositionAngle" ifnotexist="" /></span>
									</logic:equal>										
								</td>
							</tr>
							</logic:equal>							
							<logic:equal name="instrumentBean" property="displayGrating" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.grating" /></td>
								<td class="left"><bean:define id="gratings"
									name="instrumentBean" property="gratings" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedGrating">
									<html:options collection="gratings" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredGrating" value="true">
										<span class="error"><astron:errors property="selectedGrating" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredGrating" value="false">
										<span class="error"><astron:errors property="selectedGrating" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayCCD" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.ccd" /></td>
								<td class="left"><bean:define id="CCDs"
									name="instrumentBean" property="CCDs" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedCCD">
									<html:options collection="CCDs" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredCCD" value="true">
										<span class="error"><astron:errors property="selectedCCD" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredCCD" value="false">
										<span class="error"><astron:errors property="selectedCCD" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayOrderFilter" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.separation.filter" /></td>
								<td class="left">
									<bean:define id="orderFilters" name="instrumentBean" property="orderFilters" /> 
									<html:select name="instrumentBean" property="selectedOrderFilter">
										<html:options collection="orderFilters" property="value" labelProperty="label" />
									</html:select>
									<logic:equal name="instrumentBean" property="requiredOrderFilter" value="true">
										<span class="error">*</span>
									</logic:equal>
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayReadOut" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.read.mode" /></td>
								<td class="left">
									<bean:define id="readOuts" name="instrumentBean" property="readOuts" /> 
									<html:select name="instrumentBean" property="selectedReadOut">
										<html:options collection="readOuts" property="value" labelProperty="label" />
									</html:select>
									<logic:equal name="instrumentBean" property="requiredReadOut" value="true">
										<span class="error">*</span>
									</logic:equal>
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayGrism" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.grism" /></td>
								<td class="left"><bean:define id="grisms"
									name="instrumentBean" property="grisms" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedGrism">
									<html:options collection="grisms" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredGrism" value="true">
										<span class="error"><astron:errors property="selectedGrism" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredGrism" value="false">
										<span class="error"><astron:errors property="selectedGrism" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>							
							</logic:equal>

							<logic:equal name="instrumentBean" property="displayMicrostepping" value="true">
							<tr>
								<td class="right">Choose microstepping&nbsp;:&nbsp;</td>
								<td class="left"><bean:define id="microstepping"
									name="instrumentBean" property="microstepping" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedMicrostepping">
									<html:options collection="microstepping" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredMicrostepping" value="true">
										<span class="error"><astron:errors property="selectedMicrostepping" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredMicrostepping" value="false">
										<span class="error"><astron:errors property="selectedMicrostepping" ifnotexist="" /></span>
									</logic:equal>									
								</td>
							</tr>						
							</logic:equal>									
							<logic:equal name="instrumentBean" property="displayCoronagraphicMask" value="true">
							<tr>
								<td class="right" valign="top">Coronagraphic Mask? :&nbsp;</td>
								<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="coronagraphicMask" value="true" >Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="coronagraphicMask" value="false" >No</html:radio></td>
							</tr>							
							</logic:equal>	
							<logic:equal name="instrumentBean" property="displayComments" value="true">														
							<tr>
								<td class="right" valign="top">Configuration comments&nbsp;:&nbsp;</td>
								<td class="left"><html:textarea name="instrumentBean" property="comments" cols="50" rows="2"/>
									<logic:equal name="instrumentBean" property="requiredComments" value="true">
										<span class="error"><astron:errors property="comments" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredComments" value="false">
										<span class="error"><astron:errors property="comments" ifnotexist="" /></span>
									</logic:equal>										
								</td>
							</tr>	
							</logic:equal>	
										
				</logic:notEmpty>
		</logic:notEmpty>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
					
<!--  
			begin observation info
 -->
		 <logic:equal name="instrumentBean" property="displaySchedulingInfo" value="true">
			 <tr>
				<td class="header" colspan="2"><astron:label key="label.opticon.observation.scheduling" /></td>
			</tr>	
			<tr>
				<td colspan="2">
				<tiles:get name="box_header_no_top" />
	
	
				<table width="100%">	
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			<logic:equal name="instrumentBean" property="displayObservationRequestedTime" value="true">
				<tr>
					<td class="right"> 	Total time for this run : &nbsp; </td>
					<td class="left"> 	
							<html:text name="instrumentBean" property="observationRequestedTime" size="10" />
							<logic:equal name="instrumentBean" property="requiredObservationRequestedTime" value="true">
										<span class="error"><astron:errors property="comments" ifnotexist="*" /></span>
							</logic:equal>
							 <bean:write name="instrumentBean" property="timeSuffix" />		
						</td>
				</tr>
				<tr>
					<td class="right"> 	Minimum useful time for this run : &nbsp; </td>
					<td class="left"> 	
							<html:text name="instrumentBean" property="observationMinimumTime" size="10" />
							<logic:equal name="instrumentBean" property="requiredObservationRequestedTime" value="true">
										<span class="error"><astron:errors property="comments" ifnotexist="*" /></span>
							</logic:equal>	
							 <bean:write name="instrumentBean" property="timeSuffix" />
						</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</logic:equal>
							  
			<logic:equal name="instrumentBean" property="displayObservationPhase" value="true">
				<tr>
					<td class="right"> 	Time : &nbsp; </td>
					<td class="left"> 	
						<bean:define id="phaselist" name="instrumentBean" property="observationPhaseOptions"/> 
							<html:select name="instrumentBean" property="selectedObservationPhase" size="1">
								<html:options collection="phaselist" property="value" labelProperty="value"/>		
							</html:select>
						</td>
				</tr>
			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayObservationWeather" value="true">
				<tr>
					<td class="right"> 	Weather : &nbsp; </td>
					<td class="left"> 	
						<bean:define id="Weatherlist" name="instrumentBean" property="observationWeatherOptions"/> 
							<html:select name="instrumentBean" property="selectedObservationWeather" size="1">
								<html:options collection="Weatherlist" property="value" labelProperty="value"/>		
							</html:select>
						</td>
				</tr>
			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayObservationMode" value="true">
				<tr>
					<td class="right"> 	Mode : &nbsp; </td>
					<td class="left"> 	
						<bean:define id="Modelist" name="instrumentBean" property="observationModeOptions"/> 
							<html:select name="instrumentBean" property="selectedObservationMode" size="1">
								<html:options collection="Modelist" property="value" labelProperty="value"/>		
							</html:select>
						</td>
				</tr>
			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayObservationSeeing" value="true">
				<tr>
					<td class="right"> 	Seeing : &nbsp; </td>
					<td class="left"> 	
						<bean:define id="seeinglist" name="instrumentBean" property="observationSeeingOptions"/> 
							<html:select name="instrumentBean" property="selectedObservationSeeing" size="1">
								<html:options collection="seeinglist" property="value" labelProperty="value"/>		
							</html:select>
						</td>
				</tr>
			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayObservationDates" value="true">
				<tr>
					<td class="right"> 	Preffered Dates : &nbsp; </td>
					<td class="left"> 	
							<html:text name="instrumentBean" property="observationDates" size="45" />
							<span class="courier">&nbsp;(yyyy/mm/dd)&nbsp;</span>
						</td>
				</tr>
			</logic:equal>
			<logic:equal name="instrumentBean" property="displayTelescopeScheduling" value="true">
				<tr>
					<td class="right"> 	Scheduling : &nbsp; </td>
					<td class="left"> 	
						<bean:define id="schedulinglist" name="instrumentBean" property="telescopeSchedulingOptions"/> 
							<html:select name="instrumentBean" property="selectedTelescopeScheduling" size="1">
								<html:options collection="schedulinglist" property="value" labelProperty="value"/>		
							</html:select>
						</td>
				</tr>
			</logic:equal>
			<logic:equal name="instrumentBean" property="displayRequiredSchedConstraints" value="true">
				<tr>
					<td class="right" valign="top">Essential Scheduling	constraints&nbsp;:&nbsp;</td>
					<td class="left"><html:textarea name="instrumentBean" property="requiredSchedConstraints" cols="60" rows="4"/></td>
				</tr>
			</logic:equal>
			<logic:equal name="instrumentBean" property="displayPreferredSchedConstraints" value="true">
				<tr>
					<td class="right" valign="top">Preferred Scheduling	constraints&nbsp;:&nbsp;</td>
					<td class="left"><html:textarea name="instrumentBean" property="preferredSchedConstraints" cols="60" rows="4"/></td>
				</tr>
			</logic:equal>
			<logic:equal name="instrumentBean" property="displayBackupStrategy" value="true">
				<tr>
					<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.backup" /><!-- Backup strategy (for poor weather) :&nbsp;<br/>&nbsp;&nbsp;--></td>
					<td class="left"><html:textarea name="instrumentBean" property="backupStrategy" cols="60" rows="4"/></td>
				</tr>	
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>	
			</logic:equal>
			<logic:equal name="instrumentBean" property="displayCalibrationRequirements" value="true">
				<tr>
					<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.calibration" /><!-- Calibration Requirements :&nbsp; --></td>
					<td class="left"><html:textarea name="instrumentBean" property="calibrationRequirements" cols="60" rows="4"/></td>
				</tr>	
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>	
			</logic:equal>
			</table>
			<tiles:get name="box_footer" /></td>
			</tr>
		</logic:equal>
	</table>
	<tiles:get name="box_footer" /></td>
	
</tr>	

<tr>
	<td colspan="2">&nbsp;</td>
</tr>



<logic:equal name="instrumentBean" property="displayPiggyBack" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observation.piggyback" /></td>
			<td class="left">	
				<html:radio name="instrumentBean" property="enablePiggyBack" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
				<html:radio name="instrumentBean" property="enablePiggyBack" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
		</logic:equal>	
		<logic:equal name="instrumentBean" property="enablePiggyBack" value="true">
		<tr>
			<td class="right"> <astron:label key="label.opticon.observation.piggyback.specifics" /> <!-- list one or more organisations from which you are requesting sponsorship (e.g. GLOW) :&nbsp;--></td>
			<td class="left"><html:textarea name="instrumentBean" property="piggyBackSpecifics" cols="60" rows="4"/></td>
				
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
	</logic:equal>
<!-- 
			begin post processing
- Averageing y/n
  - how much in time
  - how much in frequency
- Flagging y/n
- Calibration y/n
- Imageing y/n
  - # polarisations
  - Fields size [deg x deg; prefilled but can be changed]
  - Pixel size [arcsec x arcsec; prefilled but can be changed]

Tied Array
Timeseries:
- Total power + Integration
  - Integration time [ms] 
  - I|I, Q, U, V
- Dedispersion
  - dedispersion range (check w. Jason)

VHECR
?

Data volume calculator
			
	 -->
<logic:equal name="instrumentBean" property="displayPostProcessing" value="true">

<tr>
	<td colspan="2">&nbsp;</td>
</tr>
 <tr>
	<td class="header" colspan="2"> <astron:label key="label.opticon.observation.post.processing" /></td><!-- Post Processing : -->
</tr>		
 
<tr>
	<td colspan="2"><tiles:get name="box_header_no_top" />
	<table width="100%">
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<logic:equal name="instrumentBean" property="displayPostImaging" value="true">
			<tr>
					<td class="right" valign="top"><astron:label key="label.opticon.observation.post.imaging" /></td>
					<td class="left"><html:radio styleClass="radio"name="instrumentBean"  property="imaging" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
									<html:radio styleClass="radio" name="instrumentBean" property="imaging" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
					</td>
			</tr>
			<logic:equal name="instrumentBean" property="imaging" value="true">
			  <tr>
			  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.pixels" /></td>
			  		<td class="left"><html:text name="instrumentBean" property="pixelSizeX" size="4" /> x
			  						 <html:text name="instrumentBean" property="pixelSizeY" size="4" />
			  						 <span class="error"><astron:errors property="pixelSize" ifnotexist="*" /></span>
			  		</td>
			  		
			  </tr>
			   <tr>
			  		<td align="right" valign="top"><astron:label key="label.opticon.observation.post.polarizations" /></td>
			  		<td class="left">	<logic:iterate id="polarizationItem" name="instrumentBean" property="polarizations">
			        	<html:radio name="instrumentBean" property="selectedPolarizations" idName="polarizationItem" value="value" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
			                 <bean:write name="polarizationItem" property="label"/>
			         	</html:radio>
			        </logic:iterate></td>
			  </tr>	
			  <tr>
			  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.frequencychannels" /></td>
			  		<td class="left"><html:text name="instrumentBean" property="frequencyChannels" size="6" /> 
			  		<span class="error"><astron:errors property="frequencyChannels" ifnotexist="*" /></span>
			  		</td>
			  </tr>
			</logic:equal>
		</logic:equal>	
		<logic:equal name="instrumentBean" property="displayPostAveraging" value="true">
			<tr>
					<td class="right" valign="top"><astron:label key="label.opticon.observation.post.averaging" /></td>
					<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="averaging" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
									<html:radio styleClass="radio" name="instrumentBean" property="averaging" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
					</td>
				</tr>
			<logic:equal name="instrumentBean" property="averaging" value="true">
			  <tr>
			  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.time" /></td>
			  		<td class="left"><html:text name="instrumentBean" property="averagingTime" size="6" /> [s]
			  		<span class="error"><astron:errors property="averagingTime" ifnotexist="*" /></span>
			  		</td>
			  </tr>
			  <tr>
			  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.frequency" /></td>
			  		<td class="left"><html:text name="instrumentBean" property="averagingFrequency" size="6" /> [KHz]
			  		<span class="error"><astron:errors property="averagingFrequency" ifnotexist="*" /></span>
			  		</td>
			  </tr>
			</logic:equal>
		</logic:equal>
		<logic:equal name="instrumentBean" property="displayPostBeamformed" value="true">
			<tr>
			  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.beams" /></td>
			  		<td class="left"><html:text name="instrumentBean" property="beams" size="6" />
			  		<span class="error"><astron:errors property="beams" ifnotexist="*" /></span>
			  		</td>
			  </tr>
			<tr>
			  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.samplerate" /></td>
			  		<td class="left"><html:text name="instrumentBean" property="samplerate" size="6" /> [KHz]
			  		<span class="error"><astron:errors property="samplerate" ifnotexist="*" /></span>
			  		</td>
			  </tr>
		</logic:equal>
		<logic:equal name="instrumentBean" property="displayPostTimeseries" value="true">
			<tr>
			  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.events" /> </td>
			  		<td class="left"><html:text name="instrumentBean" property="events" size="6" />
			  		<span class="error"><astron:errors property="events" ifnotexist="*" /></span>
			  		</td>
			  </tr>
		
		</logic:equal>
		
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observation.post.flagging" /></td>
			<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="flagging" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							<html:radio styleClass="radio" name="instrumentBean" property="flagging" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>
		</tr>
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observation.post.calibration" /></td>
			<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="calibration" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
							<html:radio styleClass="radio" name="instrumentBean" property="calibration" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>
		</tr>	
		  
		
 	</table>
	<tiles:get name="box_footer" /></td>
</tr>
	<logic:notEmpty name="instrumentBean" property="dataCalculationType">
	<tr>
	  		<td>Calculation type : </td>
	  		<td><bean:write name="instrumentBean" property="dataCalculationType"/>
			  		</td>
	</tr>
	</logic:notEmpty>	
	<tr>
	  		<td>Estimated data size : </td>
	  		<td><bean:write name="instrumentBean" property="estimatedDataStorage" /> GB
	  		&nbsp; &nbsp; 
	  		<html:link href="javascript:this.location.reload();" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()" onmouseover="boxopen('m1')" onmouseout="boxclosetime('m1')">recalculate</html:link>
	  		</td>
	</tr>
	<tr>
	<td colspan="2" >
	 <div id="m1" style="visibility:hidden; position: absolute; border: 1px solid; background: #E0F0FF;"> &nbsp;	&nbsp;  Calculation data used : 
	 &nbsp; <bean:write name="instrumentBean" property="dataStorageSpecifics" /></div></td>
	</tr>
	
</logic:equal> 

	
	