<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@page import="eu.radionet.northstar.business.configuration.NorthStarConfiguration" %>

<html:html>
<head>
	<link rel=stylesheet href="<%=request.getContextPath()%>/css/proposalLayout.css" type="text/css">
    <title>NorthStar Help File</title>
</head>

<body>

<jsp:include page="../pages/layout/box_header_noFullWidth.jsp" />

<h2> Short XML configuration file help </h2>
<br/>
The configuration file is an xml file, it consists of a structured list of tags with text in it.<br/>
Here is an example of the first section:<br/>
<pre>
<span style="font-weight: bold;color: #000000;">&lt;configuration</span><span style="color: #008000;"> telescope=</span><span style="color: #aa0000;">"INT"</span><span style="font-weight: bold;color: #000000;">&gt;</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;options</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"telescopeConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;option&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;value&gt;</span><span style="color: #000000;">INT</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;subfields&gt;</span>
<span style="color: #000000;">				</span><span style="font-weight: bold;color: #000000;">&lt;optionlist</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentConfigurations"</span><span style="font-weight: bold;color: #000000;">/&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;/subfields&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/option&gt;</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;/options&gt;</span>
<span style="color: #000000;">	...</span>
<span style="color: #000000;">	...</span>
<span style="font-weight: bold;color: #000000;">&lt;/configuration&gt;</span><span style="color: #000000;">  </span>
</pre>
It defines the telescope. You can change the capital letters INT to any other telescope.<br/>
In the second section the instruments are being defined. In the example below you can see the "Wide field camera" , <br/>and the "IDS Spectograph" Instruments are defined for the INT telescope.<br/>
notice that names always appear between "value" tags. ( &lt;value&gt; &nbsp; &lt;/value&gt; )
<pre>
<span style="font-weight: bold;color: #000000;">&lt;options</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;option&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;value&gt;</span><span style="color: #000000;">Wide Field Camera</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;subfields&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;optionlist</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentUrl"</span><span style="color: #000000;"> </span><span style="font-weight: bold;color: #000000;">/&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;text</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentComments"</span><span style="font-weight: bold;color: #000000;">/&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;optionlist</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentFilter"</span><span style="font-weight: bold;color: #000000;">/&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/subfields&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;dependencies&gt;&lt;or&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;select</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"telescopeConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;&lt;value&gt;</span><span style="color: #000000;">INT</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;&lt;/select&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/or&gt;&lt;/dependencies&gt;</span><span style="color: #000000;">      	   </span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;/option&gt;</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;option&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;value&gt;</span><span style="color: #000000;">IDS Spectograph</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;subfields&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;optionlist</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentUrl"</span><span style="color: #000000;"> </span><span style="font-weight: bold;color: #000000;">/&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;optionlist</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentMode"</span><span style="font-weight: bold;color: #000000;">/&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;optionlist</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentReadOut"</span><span style="font-weight: bold;color: #000000;">/&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;text</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentComments"</span><span style="font-weight: bold;color: #000000;">/&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/subfields&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;dependencies&gt;&lt;or&gt;</span>
<span style="color: #000000;">				</span><span style="font-weight: bold;color: #000000;">&lt;select</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"telescopeConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;&lt;value&gt;</span><span style="color: #000000;">INT</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;&lt;/select&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/or&gt;&lt;/dependencies&gt;</span><span style="color: #000000;">      	   </span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;/option&gt;</span>
<span style="font-weight: bold;color: #000000;">&lt;/options&gt;</span>
</pre>
The above two sections are required and will not change often. After that the instrument options sections should follow.<br/>
 one or more of the following sections should be present.<br/>
      instrumentMode<br/>
      instrumentReadOut<br/>
      instrumentOrderFilter<br/>
      instrumentFilter<br/>
      instrumentFilterw2<br/>
      instrumentGrating<br/>
      instrumentGrism<br/>
      instrumentSlit<br/>
      instrumentMicrostepping<br/>
      <br/>
Here is an example of the instrumentReadOut for the IDS instrument:<br/>
<pre>
<span style="font-weight: bold;color: #000000;">&lt;options</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentReadOut"</span><span style="font-weight: bold;color: #000000;">&gt;</span><span style="color: #000000;">		</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;option&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;value&gt;</span><span style="color: #000000;">Fast</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;dependencies&gt;&lt;or&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;select</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;&lt;value&gt;</span><span style="color: #000000;">IDS spectrograph</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;&lt;/select&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/or&gt;&lt;/dependencies&gt;</span><span style="color: #000000;">      	   </span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;/option&gt;</span><span style="color: #000000;">       	      	   	</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;option&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;value&gt;</span><span style="color: #000000;">Normal</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;dependencies&gt;&lt;or&gt;</span>
<span style="color: #000000;">				</span><span style="font-weight: bold;color: #000000;">&lt;select</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;&lt;value&gt;</span><span style="color: #000000;">IDS spectrograph</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;&lt;/select&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/or&gt;&lt;/dependencies&gt;</span><span style="color: #000000;">      	   </span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;/option&gt;</span><span style="color: #000000;">       	      	   	</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;option&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;value&gt;</span><span style="color: #000000;">Slow</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;dependencies&gt;&lt;or&gt;</span>
<span style="color: #000000;">				</span><span style="font-weight: bold;color: #000000;">&lt;select</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;&lt;value&gt;</span><span style="color: #000000;">IDS spectrograph</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;&lt;/select&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/or&gt;&lt;/dependencies&gt;</span><span style="color: #000000;">      	   </span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;/option&gt;</span><span style="color: #000000;">       	      	   	</span>
<span style="font-weight: bold;color: #000000;">&lt;/options&gt;</span>
</pre>
The example defines three options: Fast, Normal and Slow. <br/>
when another option is added, say "Very Slow", the section would look as follows:<br/>
<pre>
<span style="font-weight: bold;color: #000000;">&lt;options</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentReadOut"</span><span style="font-weight: bold;color: #000000;">&gt;</span><span style="color: #000000;">		</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;option&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;value&gt;</span><span style="color: #000000;">Fast</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;dependencies&gt;&lt;or&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;select</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;&lt;value&gt;</span><span style="color: #000000;">IDS spectrograph</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;&lt;/select&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/or&gt;&lt;/dependencies&gt;</span><span style="color: #000000;">      	   </span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;/option&gt;</span><span style="color: #000000;">       	      	   	</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;option&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;value&gt;</span><span style="color: #000000;">Normal</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;dependencies&gt;&lt;or&gt;</span>
<span style="color: #000000;">				</span><span style="font-weight: bold;color: #000000;">&lt;select</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;&lt;value&gt;</span><span style="color: #000000;">IDS spectrograph</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;&lt;/select&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/or&gt;&lt;/dependencies&gt;</span><span style="color: #000000;">      	   </span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;/option&gt;</span><span style="color: #000000;">       	      	   	</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;option&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;value&gt;</span><span style="color: #000000;">Slow</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;dependencies&gt;&lt;or&gt;</span>
<span style="color: #000000;">				</span><span style="font-weight: bold;color: #000000;">&lt;select</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;&lt;value&gt;</span><span style="color: #000000;">IDS spectrograph</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;&lt;/select&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;/or&gt;&lt;/dependencies&gt;</span><span style="color: #000000;">      	   </span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;/option&gt;</span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;option&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;value&gt;</span><span style="color: #000000;">Very Slow</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;</span>
<span style="color: #000000;">		</span><span style="font-weight: bold;color: #000000;">&lt;dependencies&gt;&lt;or&gt;</span>
<span style="color: #000000;">				</span><span style="font-weight: bold;color: #000000;">&lt;select</span><span style="color: #008000;"> name=</span><span style="color: #aa0000;">"instrumentConfigurations"</span><span style="font-weight: bold;color: #000000;">&gt;&lt;value&gt;</span><span style="color: #000000;">IDS spectrograph</span><span style="font-weight: bold;color: #000000;">&lt;/value&gt;&lt;/select&gt;</span>
<span style="color: #000000;">			</span><span style="font-weight: bold;color: #000000;">&lt;/or&gt;&lt;/dependencies&gt;</span><span style="color: #000000;">      	   </span>
<span style="color: #000000;">	</span><span style="font-weight: bold;color: #000000;">&lt;/option&gt;</span>
<span style="font-weight: bold;color: #000000;">&lt;/options&gt;</span>
</pre>
As you can see, an entire part starting with: &lt;option&gt; and ending with: &lt;/option&gt; has been added,<br/>
That has to be done for each extra item.<br/>
<br/>

<jsp:include page="../pages/layout/box_footer_noFullWidth.jsp" />

</body>
</html:html>