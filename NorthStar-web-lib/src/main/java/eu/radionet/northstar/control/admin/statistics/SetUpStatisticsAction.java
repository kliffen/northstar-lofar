package eu.radionet.northstar.control.admin.statistics;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.Proposal;

public class SetUpStatisticsAction extends Action {

	private NorthStarDelegate northStarDelegate = null;
	private ProposalDelegate proposalDelegate = null;
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		StatisticsForm statisticsForm = (StatisticsForm) form;
		northStarDelegate = NorthStarDelegate.getInstance();

		List proposalIds = (List) request.getSession().getAttribute("proposalIds");
		List proposals = getProposals(proposalIds);
		request.getSession().removeAttribute("proposalIds");
		
		calculateTotals(statisticsForm,proposals);
		calculateModes(statisticsForm,proposals);
		calculateCategories(statisticsForm,proposals);
		
		return mapping.findForward(Constants.SUCCESS);
	}


	private void calculateTotals(StatisticsForm form, List proposals) throws InvalidConfigurationException {
    	int targets = 0;
    	double time = 0;//new Double();
    	double storage =0;
    	
    	form.setTotalProposals(String.valueOf(proposals.size()));
    	Iterator proposalit = proposals.iterator();
		while (proposalit.hasNext()){
			Proposal proposal = (Proposal) proposalit.next();
			
			// get real allocations as the proposal will not show it by itself
			ProposalDelegate proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposal);
			Map allocations = proposalDelegate.getAllocations(proposal.getObservingRequest());
			
			targets += proposal.getObservingRequest().getTargets().size();
			Double global = NorthStarDelegate.getResourceValue(
					allocations, "LOFAR_Global");
			if (global != null){
				time += global.doubleValue();
			}
			
			Double lofarStorage = NorthStarDelegate.getResourceValue(
					allocations, "LOFAR_longTermStorage");
			if (lofarStorage != null){
				storage += lofarStorage.doubleValue();
			}
			
		}
		
		form.setTotalTargets(String.valueOf(targets));
		form.setTotalTime(String.valueOf(time));
		form.setTotalStorage(String.valueOf(storage));
	}

    private void calculateModes(StatisticsForm statisticsForm, List proposals) throws InvalidConfigurationException {
    	Map statisticsPerInstrument = new HashMap();
    	
    	Iterator proposalit = proposals.iterator();
		while (proposalit.hasNext()){
			Proposal proposal = (Proposal) proposalit.next();
			ProposalDelegate proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposal);
			//for (Observation observation : (Arraylist <Observation>) proposal.getObservingRequest().getObservations()){  }
			Iterator obsit = proposal.getObservingRequest().getObservations().iterator();
			while (obsit.hasNext()){
				Observation observation = (Observation) obsit.next();
				String name = proposalDelegate.getObservationInstrumentName(observation);

				StatisticsBean statisticsBean = null;
				if(statisticsPerInstrument.containsKey(name)){
					  statisticsBean = (StatisticsBean) statisticsPerInstrument.get(name);
					statisticsBean.incrementProposal(1);
					statisticsBean.incrementTargets(observation.getTargets().size());
					statisticsBean.incrementTimeSec(observation.getTotalObservationDuration());
					
				}else{
					  statisticsBean = new StatisticsBean();
					statisticsBean.setInsName(name);
					statisticsBean.setProposals("1");
					statisticsBean.setTargets(String.valueOf(observation.getTargets().size()));
					double time = observation.getTotalObservationDuration().doubleValue();
					if (time > 0){
						time=time/60;
						statisticsBean.setTime(String.valueOf(time));
					}
				}
				if(name != null && name != ""){
					statisticsPerInstrument.put(name,statisticsBean);
				}
			}
		}
		Iterator entryit = statisticsPerInstrument.values().iterator();
		while (entryit.hasNext()){
			statisticsForm.getStatisticsTotalsBeans().add(entryit.next());
		}
		
	}

    
	private void calculateCategories(StatisticsForm statisticsForm, List proposals) throws InvalidConfigurationException {
		//statisticsForm.setStatisticsBeans(new ArrayList());
		Map statisticsPerCategory = new HashMap();
		
		Iterator proposalit = proposals.iterator();
		while (proposalit.hasNext()){
			Proposal proposal = (Proposal) proposalit.next();
			ProposalDelegate proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposal);
			Map allocations = proposalDelegate.getAllocations(proposal.getObservingRequest());
			
			String name = proposal.getSemester().getCategory().getCode();
			StatisticsBean statisticsBean = null;
			
			if(statisticsPerCategory.containsKey(name)){
				  statisticsBean = (StatisticsBean) statisticsPerCategory.get(name);
				statisticsBean.incrementProposal(1);
				statisticsBean.incrementTargets(proposal.getObservingRequest().getTargets().size());
				//statisticsBean.incrementTime(proposal.getObservingRequest().get);
				Double global = NorthStarDelegate.getResourceValue(
						allocations, "LOFAR_Global");
				if (global != null){
					statisticsBean.incrementTime(global);
				}
				Double lofarStorage = NorthStarDelegate.getResourceValue(
						allocations, "LOFAR_longTermStorage");
				if (lofarStorage != null){
					statisticsBean.incrementStorage(lofarStorage);
				}
				
			}else{
				  statisticsBean = new StatisticsBean();
				statisticsBean.setInsName(name);
				statisticsBean.setProposals("1");
				statisticsBean.setTargets(String.valueOf(proposal.getObservingRequest().getTargets().size()));
				Double global = NorthStarDelegate.getResourceValue(
						allocations, "LOFAR_Global");
				if (global != null){
					statisticsBean.setTime(global.toString());
				}
				Double lofarStorage = NorthStarDelegate.getResourceValue(
						allocations, "LOFAR_longTermStorage");
				if (lofarStorage != null){
					statisticsBean.setStorage(lofarStorage.toString());
				}
			}
			if(name != null && name != ""){
				statisticsPerCategory.put(name,statisticsBean);
			}
		}
		
		Iterator entryit = statisticsPerCategory.values().iterator();
		while (entryit.hasNext()){
			statisticsForm.getStatisticsBeans().add(entryit.next());
		}
	}

	
	private List getProposals(List proposalIds) {
    	List proposals = new ArrayList();
    	if(proposalIds != null){
	    	Iterator proposalIt = proposalIds.iterator();
	    	while (proposalIt.hasNext()){
	    		Integer proposalId = (Integer) proposalIt.next();
	    		Proposal proposal = northStarDelegate.getProposal(proposalId);
	    		proposals.add(proposal);
	    	}
    	}
    	return proposals;
    }
	
	
}
