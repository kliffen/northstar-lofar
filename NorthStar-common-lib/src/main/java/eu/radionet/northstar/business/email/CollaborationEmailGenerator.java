// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.email;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.naming.NamingException;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronValidator;
import nl.astron.util.exception.AstronMailException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.radionet.northstar.business.configuration.CollaborationConfiguration;
import eu.radionet.northstar.business.configuration.EmailConfiguration;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.Telescope;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalStatus;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.collaboration.Invitation;

public class CollaborationEmailGenerator extends EmailGenerator {
	private Log log = LogFactory.getLog(this.getClass());

	private static final String INVITATION = "invitation";

	private static final String INVITATION_FOR_VIEWING = "invitation_for_viewing";

	//private static final String PROPOSAL_DELETED = "proposaldeleted";

	//private static final String PROPOSAL_SUBMITTED = "proposalsubmitted";

	//private static final String MEMBER_ADDED = "memberadded";

	private static final String NAME = "%name%";

	private static final String CONTACT_AUTHOR = "%contactauthor%";

	private static final String INVITING_AUTHOR = "%invitingauthor%";

	private static final String PROPOSAL_TOOL = "%proposaltool%";

	protected static final String COMMITTEE = "%committee%";

	protected static final String TELESCOPES = "%telescopes%";

	protected static final String TELESCOPE = "%telescope%";

	protected static final String PROPOSAL_TITLE = "%proposaltitle%";

	private static final String PI = "%pi%";

	private static final String ACCEPT_LINK = "%acceptlink%";

	private static final String NO_TITLE = "No title has been entered";

	//private static final String KEY = "%key%";
	
	protected static final String PROPOSAL_CODE = "%proposalcode%";


	protected static final String DATE = "%date%";

	private static final String INVITATION_FOR_NORTHSTAR = "invitation_for_northstar";

	private static final String PERSONAL_MESSAGE = "%personalmessage%";

	/**
	 * The sendInvitationEmail method sends an invitation email
	 * 
	 * @param invitation
	 * @throws AstronMailException
	 * @throws NamingException
	 * @throws DatabaseException
	 */
	public void sendInvitationEmail(Proposal proposal,
			NonRegisteredMember nonRegisteredMember, Invitation invitation,
			UserAccount userAccount) throws AstronMailException, NamingException {

		EmailConfiguration emailConfiguration = CollaborationConfiguration
				.getEmailConfiguration(INVITATION);
		String message = emailConfiguration.getMessage();
		message = getInvitationMail(proposal, nonRegisteredMember, invitation,
				message, userAccount);
		super.sendEmail(emailConfiguration, message, nonRegisteredMember
				.getEmail());
		log.info("Invitation mail for proposal with id: " + proposal.getId()
				+ " send to : " + nonRegisteredMember.getName() + "("
				+ nonRegisteredMember.getEmail() + ")");
	}

	public void sendInvitationForViewingEmail(Proposal proposal,
			NonRegisteredMember nonRegisteredMember, Invitation invitation,
			UserAccount userAccount) throws AstronMailException, NamingException {
		EmailConfiguration emailConfiguration = CollaborationConfiguration
				.getEmailConfiguration(INVITATION_FOR_VIEWING);
		String message = emailConfiguration.getMessage();
		message = getInvitationMail(proposal, nonRegisteredMember, invitation,
				message, userAccount);
		message = message.replaceAll(PROPOSAL_CODE, proposal.getCode());

		ProposalStatus proposalStatus = proposal.getCurrentStatus();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm z");
		Calendar UTCCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		UTCCal.setTime(proposalStatus.getStatusTime());
		simpleDateFormat.setCalendar(UTCCal);

		message = message.replaceAll(DATE, simpleDateFormat.format(UTCCal
				.getTime()));
		
		super.sendEmail(emailConfiguration, message, nonRegisteredMember
				.getEmail());
		log.info("Invitation for viewing mail for proposal with id: " + proposal.getId()
				+ " send to : " + nonRegisteredMember.getName() + "("
				+ nonRegisteredMember.getEmail() + ")");
	}
	
	public void sendInvitationForNorthStarEmail(NonRegisteredMember nonRegisteredMember, Invitation invitation,
			UserAccount userAccount, String personalMessage, String forwardPath) throws AstronMailException, NamingException {
		EmailConfiguration emailConfiguration = CollaborationConfiguration
				.getEmailConfiguration(INVITATION_FOR_NORTHSTAR);
		
		if(emailConfiguration == null || emailConfiguration.getMessage() ==null ){
			log.warn(" could not find email message template: "+INVITATION_FOR_NORTHSTAR);
			throw new AstronMailException();
		}
		String message = emailConfiguration.getMessage();
		message = message.replaceAll(NAME, nonRegisteredMember.getName());
		
		message = message.replaceAll(ACCEPT_LINK, forwardPath);
		
		message = message.replaceAll(PROPOSAL_TOOL, NorthStarConfiguration
				.getName());
		message = message.replaceAll(INVITING_AUTHOR, getEmailName(userAccount
				.getUser()));
		if(personalMessage != null || personalMessage.length() >8 ){
			personalMessage = " The author has included a message: "+System.getProperty("line.separator")+personalMessage;
			message = message.replaceAll(PERSONAL_MESSAGE, personalMessage);
		}else{
			message = message.replaceAll(PERSONAL_MESSAGE, "");
		}
				
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm z");
		Calendar UTCCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
		simpleDateFormat.setCalendar(UTCCal);

		message = message.replaceAll(DATE, simpleDateFormat.format(UTCCal
				.getTime()));
		
		super.sendEmail(emailConfiguration, message, nonRegisteredMember
				.getEmail());
		log.info("Invitation for using NorthStar "	+ " send to : " +
				nonRegisteredMember.getName() + "("	+ nonRegisteredMember.getEmail() + ")");
	}

	protected String getInvitationMail(Proposal proposal,
			NonRegisteredMember nonRegisteredMember, Invitation invitation,
			String message, UserAccount userAccount) {
		message = message.replaceAll(NAME, nonRegisteredMember.getName());
		if (proposal.getJustification() != null && !AstronValidator.isBlankOrNull(proposal.getJustification().getTitle())){
			message = message.replaceAll(PROPOSAL_TITLE, proposal.getJustification().getTitle());			
		}else {
			message = message.replaceAll(PROPOSAL_TITLE, NO_TITLE);			
		}
		message = message.replaceAll(ACCEPT_LINK, CollaborationConfiguration
				.getAcceptInvitation()
				+ invitation.getAcceptCode()+"&pid="+proposal.getId());
		message = message.replaceAll(PROPOSAL_TOOL, NorthStarConfiguration
				.getName());
		message = message.replaceAll(INVITING_AUTHOR, getEmailName(userAccount
				.getUser()));
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		message = message.replaceAll(TELESCOPE, proposal
				.getSemester().getTelescope());
		message = message.replaceAll(COMMITTEE, telescopeConfiguration
				.getCommittee());
		List availableTelescopes = NorthStarConfiguration
				.getTelescopes();
		String telescopes = null;
		Iterator availableTelescopesIterator = availableTelescopes.iterator();
		while (availableTelescopesIterator.hasNext()) {
			Telescope temp = (Telescope) availableTelescopesIterator
					.next();
			if (availableTelescopesIterator.hasNext()) {
				if (telescopes == null) {
					telescopes = temp.getName();
				} else {
					telescopes += ", " + temp.getName();
				}
			} else {
				if (telescopes == null) {
					telescopes = temp.getName();
				} else {
					telescopes += " and " + temp.getName();
				}
			}
		}
		message = message.replaceAll(TELESCOPES, telescopes);
		RegisteredMember contactAuthor = proposal.getContactAuthor();
		message = message.replaceAll(CONTACT_AUTHOR, getEmailName(contactAuthor
				.getUser()));
		Member pi = proposal.getPi();
		if (pi.getClass().equals(NonRegisteredMember.class)) {
			message = message.replaceAll(PI, pi.getName());
		} else if (pi.getClass().equals(RegisteredMember.class)) {
			RegisteredMember registeredMember = (RegisteredMember) pi;
			message = message.replaceAll(PI, getEmailName(registeredMember
					.getUser()));
		}
		message = message.replaceAll(CONTACT_AUTHOR, getEmailName(contactAuthor
				.getUser()));
		return message;
	}



}
