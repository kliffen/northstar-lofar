<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processAddRegisteredMember" method="GET">

<TABLE>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	<TR>
		<TD colspan="2">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif"/></TD>
				<TD valign='top'>
					Your acceptance of the invitation has been registered.<BR>
					The proposal will now appear in your list of proposals,<BR>
					although you may find it locked for editing by another proposer.
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	</TABLE>
	
<table width="100%">
	<TR>
		<td align="center"><html:submit styleClass="list_accept_yes" title="Ok">Ok</html:submit></TD>
	</TR>
</TABLE>
</html:form>