// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.data.entities;

import eu.radionet.northstar.data.entities.Target;

public class OpticonTarget extends Target
{
	/**
	 * 
	 */
	private static final long serialVersionUID = 13747344L;
	protected String moon = null;
	protected String seeing = null;
	protected String water = null;
	protected Double sn = null;
	protected Double magnitude = null;
	protected String comments = null;
	protected Double flux = null;
	protected Double fluxFrequency = null;
	protected Double spectralIndex = null;
	protected Double seeingUpper = null;
	protected Double seeingLower = null;
	protected String skyQuality = null;
	protected Double diameter = null;
	protected Double storage = null;
	protected boolean opportunity = false;
	protected boolean specifySubbandList = false;
	protected String subbandList = null;
	protected String totalSubbandList = null;
	protected Double bandwidth = null;
	protected Double centralFrequency = null;
	protected String observingrun = null;
	protected String pipeline = null;
	protected String runnumber = null;
	protected boolean calibrationBeam = false;
	protected boolean firstInRun = false;
	
	
	public Double getDiameter() {
		return diameter;
	}
	public void setDiameter(Double diameter) {
		this.diameter = diameter;
	}
	public String getMoon()
	{
		return moon;
	}
	public void setMoon(String moon)
	{
		this.moon = moon;
	}
	public String getSeeing()
	{
		return seeing;
	}
	public void setSeeing(String seeing)
	{
		this.seeing = seeing;
	}
	public String getWater()
	{
		return water;
	}
	public void setWater(String water)
	{
		this.water = water;
	}
	public String getComments()
	{
		return comments;
	}
	public void setComments(String comments)
	{
		this.comments = comments;
	}
	public Double getMagnitude()
	{
		return magnitude;
	}
	public void setMagnitude(Double magnitude)
	{
		this.magnitude = magnitude;
	}
	public Double getSn()
	{
		return sn;
	}
	public void setSn(Double sn)
	{
		this.sn = sn;
	}
	public Double getFlux()
	{
		return flux;
	}
	public void setFlux(Double flux)
	{
		this.flux = flux;
	}
	public final Double getFluxFrequency() {
		return fluxFrequency;
	}
	public final void setFluxFrequency(Double fluxFrequency) {
		this.fluxFrequency = fluxFrequency;
	}
	public final Double getSpectralIndex() {
		return spectralIndex;
	}
	public final void setSpectralIndex(Double spectralIndex) {
		this.spectralIndex = spectralIndex;
	}
	public Double getSeeingLower()
	{
		return seeingLower;
	}
	public void setSeeingLower(Double seeingLower)
	{
		this.seeingLower = seeingLower;
	}
	public Double getSeeingUpper()
	{
		return seeingUpper;
	}
	public void setSeeingUpper(Double seeingUpper)
	{
		this.seeingUpper = seeingUpper;
	}
	public String getSkyQuality()
	{
		return skyQuality;
	}
	public void setSkyQuality(String skyQuality)
	{
		this.skyQuality = skyQuality;
	}
	public Double getStorage() {
		return storage;
	}
	public void setStorage(Double storage) {
		this.storage = storage;
	}
	public boolean isOpportunity() {
		return opportunity;
	}
	public void setOpportunity(boolean opportunity) {
		this.opportunity = opportunity;
	}
	public boolean isSpecifySubbandList() {
		return specifySubbandList;
	}
	public void setSpecifySubbandList(boolean specifySubbandList) {
		this.specifySubbandList = specifySubbandList;
	}
	public String getSubbandList() {
		return subbandList;
	}
	public void setSubbandList(String subbandList) {
		this.subbandList = subbandList;
	}
	public String getTotalSubbandList() {
		return totalSubbandList;
	}	
	public void setTotalSubbandList(String totalSubbandList) {
		this.totalSubbandList = totalSubbandList;
	}
	public Double getBandwidth() {
		return bandwidth;
	}
	public void setBandwidth(Double bandwidth) {
		this.bandwidth = bandwidth;
	}
	public Double getCentralFrequency() {
		return centralFrequency;
	}
	public void setCentralFrequency(Double centralFrequency) {
		this.centralFrequency = centralFrequency;
	}
	public String getObservingrun() {
		return observingrun;
	}
	public void setObservingrun(String observingrun) {
		this.observingrun = observingrun;
	}
	public String getPipeline() {
		return pipeline;
	}
	public void setPipeline(String pipeline) {
		this.pipeline = pipeline;
	}
	public String getRunnumber() {
		return runnumber;
	}
	public void setRunnumber(String runnumber) {
		this.runnumber = runnumber;
	}
	public boolean isCalibrationBeam() {
		return calibrationBeam;
	}
	public void setCalibrationBeam(boolean calibrationBeam) {
		this.calibrationBeam = calibrationBeam;
	}
	public boolean isFirstInRun() {
		return firstInRun;
	}
	public void setFirstInRun(boolean firstInRun) {
		this.firstInRun = firstInRun;
	}
	
	


}
