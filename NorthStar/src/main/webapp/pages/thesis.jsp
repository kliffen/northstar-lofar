<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />
<html:form action="/processThesis" method="POST">

<html:hidden property="thesisId"/>
<table width="100%">
	<TR>
		<TD width="*">&nbsp;</TD>
		<TD class="help" width="60">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="60" height="25">
							<TR>
					  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#thesis\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
				  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#thesis\" target=\"help\">Help</a>"%></TD>
			  				</TR>
			  			</TABLE>
					<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
					</TD>
	</TR>
	</table>
<table>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="right" valign="top">Is the student an applicant? :&nbsp;</td>
		<td class="left"><html:radio styleClass="radio" property="studentApplicant" value="true"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
						<html:radio styleClass="radio" property="studentApplicant" value="false"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio></td>
	</tr>
	<logic:equal name="thesisForm" property="studentApplicant" value="true">
	<TR>
		<TD class="right">Student:&nbsp;</TD>	
		<TD>
			<bean:define id="applicants" name="thesisForm" property="memberBeans"/> 
			<html:select property="studentRef" styleClass="selectmember">
				<html:options collection="applicants" property="id" labelProperty="name"/>
			</html:select>	
		</TD>
	</TR>
	</logic:equal>
	<logic:notEqual name="thesisForm" property="studentApplicant" value="true">
	<tr>
		<td class="right">Student:&nbsp;</td>
		<td class="left"><html:text property="studentName"  tabindex="1" maxlength="255"/><span class="error"><astron:errors property="student" ifnotexist="*"/></span></td>
	</tr>
	</logic:notEqual>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="right">Student Level:&nbsp;</td>
		<td class="left"><bean:define id="studentLevels" name="thesisForm" property="studentLevels" /> 
			<html:select property="studentLevel">
			<html:options collection="studentLevels" property="value" labelProperty="label" />
			</html:select>
			<span class="error"><astron:errors property="studentLevel" ifnotexist="*"/></span>
		</td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="right" valign="top">Is the supervisor an applicant? :&nbsp;</td>
		<td class="left"><html:radio styleClass="radio" property="supervisorApplicant" value="true"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
						<html:radio styleClass="radio" property="supervisorApplicant" value="false"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio></td>
	</tr>
	<logic:equal name="thesisForm" property="supervisorApplicant" value="true">
	<TR>
		<TD class="right">Supervisor:&nbsp;</TD>	
		<TD>
			<bean:define id="applicants" name="thesisForm" property="memberBeans"/> 
			<html:select property="supervisorRef" styleClass="selectmember">
				<html:options collection="applicants" property="id" labelProperty="name"/>
			</html:select>	<span class="error"><astron:errors property="supervisor" ifnotexist=""/></span>
		</TD>
	</TR>
	</logic:equal>
	<logic:notEqual name="thesisForm" property="supervisorApplicant" value="true">
	<tr>
		<td class="right">Supervisor:&nbsp;</td>
		<td class="left"><html:text property="supervisorName"  tabindex="1" maxlength="255"/><span class="error"><astron:errors property="supervisor" ifnotexist="*"/></span></td>
	</tr>
	</logic:notEqual>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>	
	<tr>
		<td class="right">Data required for completion assignment?:&nbsp;</td>
		<td class="left"><html:radio styleClass="radio" property="dataRequired" value="true">Yes</html:radio>
							<html:radio styleClass="radio" property="dataRequired" value="false">No</html:radio></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="right">Expected completion Date:&nbsp;</td>
		<td class="left"><html:text property="expectedCompletionDate" tabindex="3"  maxlength="255"/>&nbsp;yyyy/mm&nbsp;<span class="error"><astron:errors property="expectedCompletionDate" ifnotexist="*"/></span></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>	
</table>
<table width="100%">
	<tr>
		<td align="left"><html:submit property="commitThesisButton"  tabindex="5" styleClass="list_accept_yes">Ok</html:submit>
 		</td>
		<td align="right"><html:cancel styleClass="list_decline_t">Cancel</html:cancel></TD>
	</tr>
</table>

</html:form>