// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Semester.java 
 *
 * Created on Feb 11, 2005
 *
 * Version $Id: Semester.java,v 1.3 2008-08-20 08:24:51 boelen Exp $
 *
 */
package eu.radionet.northstar.data.entities;

import java.io.Serializable;
import java.util.Date;

/**
 * The Semester provides
 *
 * @author Bastiaan Verhoef
 *
 */
public class Semester implements Serializable {
    private Integer id = null;
    private String prefix = null;
    private String semester = null;
    private Date availableDate = null;
    private Date startDate = null;
    private Date endDate = null;
    private Date deadLine = null;
    private Integer deadLineDelay = null;
    private Integer parentId = null;
    private Integer lastAssigned = null;
    private String telescope = null;
    private Category category = null;
	private String community = null;
	private boolean closed = false;
	private boolean immediate = false;

    /**
     * @return Returns the deadLine.
     */
    public Date getDeadLine() {
        return deadLine;
    }
    /**
     * @param deadLine The deadLine to set.
     */
    public void setDeadLine(Date deadLine) {
        this.deadLine = deadLine;
    }
    /**
     * @return Returns the endDate.
     */
    public Date getEndDate() {
        return endDate;
    }
    /**
     * @param endDate The endDate to set.
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    /**
     * @return Returns the id.
     */
    public Integer getId() {
        return id;
    }
    /**
     * @param id The id to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }
    /**
     * @return Returns the lastAssigned.
     */
    public Integer getLastAssigned() {
        return lastAssigned;
    }
    /**
     * @param lastAssigned The lastAssigned to set.
     */
    public void setLastAssigned(Integer lastAssigned) {
        this.lastAssigned = lastAssigned;
    }
    /**
     * @return Returns the prefix.
     */
    public String getPrefix() {
        return prefix;
    }
    /**
     * @param prefix The prefix to set.
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }
    /**
     * @return Returns the startDate.
     */
    public Date getStartDate() {
        return startDate;
    }
    /**
     * @param startDate The startDate to set.
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }
    /**
     * @return Returns the telescope.
     */
    public String getTelescope() {
        return telescope;
    }
    /**
     * @param telescope The telescope to set.
     */
    public void setTelescope(String telescope) {
        this.telescope = telescope;
    }
    /**
     * @return Returns the categoryId.
     */
	public Category getCategory() {
		return category;
	}
	public void setCategory(Category category) {
		this.category = category;
	}
	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
	public String getSemester() {
		return semester;
	}
	public void setSemester(String semester) {
		this.semester = semester;
	}
	public boolean isClosed() {
		return closed;
	}
	public void setClosed(boolean closed) {
		this.closed = closed;
	}
	public Date getAvailableDate() {
		return availableDate;
	}
	public void setAvailableDate(Date availableDate) {
		this.availableDate = availableDate;
	}
	public boolean isImmediate() {
		return immediate;
	}
	public void setImmediate(boolean immediate) {
		this.immediate = immediate;
	}
	public Integer getDeadLineDelay() {
		return deadLineDelay;
	}
	public void setDeadLineDelay(Integer deadLineDelay) {
		this.deadLineDelay = deadLineDelay;
	}
	public Integer getParentId() {
		return parentId;
	}
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	

}
