// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.collaboration.member;

import org.apache.struts.action.ActionForm;

public class AddRegisteredMemberForm extends ActionForm {
	protected String key = null;
	protected String pid = null;
	protected boolean invalidUser = false;
	protected boolean invalidUrl = false;
	protected boolean noInvitation = false;

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public boolean isInvalidUser() {
		return invalidUser;
	}

	public void setInvalidUser(boolean invalidUser) {
		this.invalidUser = invalidUser;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public boolean isInvalidUrl() {
		return invalidUrl;
	}

	public void setInvalidUrl(boolean invalidUrl) {
		this.invalidUrl = invalidUrl;
	}

	public boolean isNoInvitation() {
		return noInvitation;
	}

	public void setNoInvitation(boolean noInvitation) {
		this.noInvitation = noInvitation;
	}
}
