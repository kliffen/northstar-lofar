// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.observingrequest.observation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.sesameclient.SesameClient;
import nl.astron.sesameclient.SesameConnectionException;
import nl.astron.sesameclient.SesameException;
import nl.astron.sesameclient.SesameNoObjectException;
import nl.astron.sesameclient.SesameTimeOutException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.AstroCoordinate;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProcessProposalAction;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.control.proposal.observingrequest.ValueBean;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Target;

/**
 * @author Anton Smit
 */
public class ProcessObservationAction extends ProcessProposalAction  {
	protected Log log = LogFactory.getLog(this.getClass());
	protected ContextType contextConfiguration = null;
	/**
	 * Execute Details Actions
	 * 
	 * @param mapping -
	 *            The ActionMapping used to select this instance
	 * @param form -
	 *            The ActionForm bean for this request (if any)
	 * @param request -
	 *            The HTTP request we are processing
	 * @param response -
	 *            The HTTP response we are creating
	 */
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		ObservationForm observationForm = (ObservationForm) form;
		HttpSession session = request.getSession();
		ActionMessages errors = new ActionMessages();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
		contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());

		/*
		 * if pressed on cancel
		 */
		if (isCancelled(request)) {
            /*
             * Clear selected target
             */
			clearTargetFields(observationForm);
			observationForm.setTempTargets(new ArrayList());
            observationForm.setSelectedTargetId(-1);
            
            if (!telescopeConfiguration.isTargetsOnTabbedPage()){
    			// undo edits
            	List observations = proposalData.getProposal().getObservingRequest().getObservations();
            	int index = AstronConverter.toInteger(observationForm.getIndex()).intValue();
            	if (observations.size() > index) {
            		Observation observation = (Observation) session.getAttribute(Constants.OBSERVATION);
    				observations.set(index, observation);
    				session.setAttribute(Constants.OBSERVATION, null);
            	}
            	return mapping.findForward(Constants.SUCCESS);
    		}else{
    			//quit without save
            	return cancel(mapping,request, ownUserAccount);
    		}
		}
		
		/*
		 * validate fields that has configurable validation
		 */
		errors = validate(observationForm,
				mapping, request);
		if (errors.size() > 0) {
			saveErrors(request, errors);
			
			//return mapping.findForward(Constants.REFRESH);
			return mapping.getInputForward();
		}

		ActionForward forward=null;
		/*
		 * store observation info if observation is present on target page
		 */
		if (!telescopeConfiguration.isTargetsOnTabbedPage()){
			forward = storeObservation(request,mapping,proposalData,observationForm);	
		}
         
		/*
		 * if the 'Resolve' button is pressed then contact a webservice, add the
		 * requested information to the form
		 */
		if (observationForm.getResolveButton() != null) {
			forward = resolveTarget(request,errors,mapping,observationForm,telescopeConfiguration);
		}
		
		if (forward != null) {
			return forward;
		}
	
		FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();

		try {
			forward = processTargetButtons(observationForm, mapping, request);
		} catch (DatabaseException e) {
			observationForm.setSelectedTargetId(-1);
			clearTargetFields(observationForm);
			return mapping.getInputForward();
		}
		
		
		if (forward != null){
			if (!observationForm.isUploadTargetList()){
				ObservingRequest observingRequest = proposalData.getProposal().getObservingRequest();
				if (telescopeConfiguration.isTargetsOnTabbedPage()){
					// here the targets are stored 
	    			fillTargets(observationForm, observingRequest);
				}else{
					fillTargets(observationForm,observingRequest,true);
					
					// include the index else the form cannot determine which observation is in editing.
					return ParameterAppender.append(forward,
							"index", observationForm.getIndex()+"");
				}
    			
    		}
			return forward;
		}
		// clear targets here??
		observationForm.setTempTargets(new ArrayList());
		
		/*
		 * if forward in form is set, forward to an other page. (See jsp's for
		 * more info)
		 */
		if (!AstronValidator.isBlankOrNull(observationForm.getForward())) {
			return mapping.findForward(observationForm.getForward());
		}
		
		if(!telescopeConfiguration.isTargetsOnTabbedPage() ){
			// include the index else the form cannot determine which observation is in editing.
			return ParameterAppender.append(mapping.findForward("refresh"),
					"index", observationForm.getIndex()+"");
		}else{
			// save submit preview buttons
			forward = processGeneralButtons( observationForm, mapping, request, errors, proposalData,ownUserAccount);
			
			if (forward != null) {
				if(forward == mapping.findForward(Constants.SAVE_AND_VIEW)){
					// clear form and reload the temp targets
					// else the page will use the old target list, which will generate an error if the user wants to edit the target again.
					observationForm.setSelectedTargetId(-1);
					observationForm.setTempTargets(null);
					List targets= proposalData.getProposal().getObservingRequest().getTargets();
					List tempList = new ArrayList();
		    		tempList.addAll(targets);
		    		observationForm.setTempTargets(tempList);
		            clearTargetFields(observationForm);
				}
				
				return forward;
			}
		}
		
		// clear form?
		observationForm.setTempTargets(new ArrayList());
        observationForm.setSelectedTargetId(-1);
        //
        
		return (mapping.findForward(Constants.REFRESH));
	}
	
	private ActionForward resolveTarget(HttpServletRequest request, ActionMessages errors, ActionMapping mapping, ObservationForm observationForm, TelescopeConfiguration telescopeConfiguration) {
		UserAccount ownUserAccount = (UserAccount) request.getSession()
		.getAttribute(Constants.OWN_USERACCOUNT);
		/*
		 * clear target fields in case an error occurs
		 */
		observationForm.setRa(null);
		observationForm.setDec(null);
		observationForm.setEpoch(Constants.EPOCH_J2000);
		try {
			// get Sesame information for the object
			SesameClient sesame = new SesameClient();
			
			/* The line below is need to initialize SIMBAD! */
			sesame.getSesameSimbad(observationForm.getFieldName());
			// store the information in the form-bean
			//double dec = sesame.getDec();
			//double ra = sesame.getRA();
			
			observationForm.setRaDec(new Double(sesame.getRA()),
					new Double(sesame.getDec()));
			observationForm.setEpoch(sesame.getEpoch());
			addOrChangeTarget(observationForm,request, mapping);
		} catch (SesameConnectionException e) {

			log.warn(LogMessage.getMessage(ownUserAccount,
					"Sesame connection exception: " + e.getMessage()));
			errors.add("fieldName", new ActionMessage(
					"error.sesame.connection"));
			saveErrors(request, errors);
			return (mapping.getInputForward());
		} catch (SesameTimeOutException e) {

			log.warn(LogMessage.getMessage(ownUserAccount,
					"Sesame time out: " + e.getMessage()));
			errors.add("fieldName", new ActionMessage(
					"error.sesame.timeout"));
			saveErrors(request, errors);
			return (mapping.getInputForward());
		} catch (SesameNoObjectException e) {
			log.warn(LogMessage.getMessage(ownUserAccount, "Sesame Error: "
					+ e.getMessage()));
			errors.add("fieldName", new ActionMessage(
					"error.sesame.noobject"));
			saveErrors(request, errors);
			return (mapping.getInputForward());
		} catch (SesameException e) {
			log.debug(AstronConverter.stackTraceToString(e));
			log.warn(LogMessage.getMessage(ownUserAccount, "Sesame Error: "
					+ e.getMessage()));
			errors.add("fieldName", new ActionMessage("error.sesame"));
			saveErrors(request, errors);
			return (mapping.getInputForward());
		}
		
		//TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
		
		if(!telescopeConfiguration.isTargetsOnTabbedPage() ){
			// include the index else the form cannot determine which observation is in editing.
			return ParameterAppender.append(mapping.findForward("refresh"),
					"index", observationForm.getIndex()+"");
		}else{
			return mapping.findForward(Constants.REFRESH);
		}
	}

	

	private ActionForward storeObservation(HttpServletRequest request,
			ActionMapping mapping, ProposalData proposalData, ObservationForm observationForm) throws DatabaseException {
		if (AstronValidator.isPositiveInt(observationForm.getIndex())) {
			int index = AstronConverter.toInteger(
					observationForm.getIndex()).intValue();
			ObservingRequest observingRequest = proposalData.getProposal()
			.getObservingRequest();
	

			if (observingRequest.getObservations().size() > index) {
				Observation observation = (Observation) observingRequest
						.getObservations().get(index);
				fillObservation(request,observation, observationForm);
				
				FieldsDefinitionType fieldsDefinitionType = 
					(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();

				if(OptionsDelegate.allowedToDisplay(Constants.AUTO_TARGET_COMMIT,fieldsDefinitionType)){
					addOrChangeTarget(observationForm,request, mapping);
					//TODO check if it really should be temptargetss
					observation.setTargets( observationForm.getTempTargets() );
				}
				
				/*
				 * if pressed on saveButton
				 */
				if (observationForm.getSaveButton() != null) {
	
					if (observationForm.isUploadTargetList()){
						request.getSession().setAttribute(Constants.OBSERVATION,
								observation);
	
						return mapping.findForward(Constants.UPLOAD_TARGET_LIST);
					}
					
					else {
						// TODO fix this later on
						//if(observation.getId() == null){
							// remove from list because it will be added later on
							//observingRequest.getObservations().remove(index);
						//}
						if(OptionsDelegate.allowedToDisplay(Constants.AUTO_TARGET_COMMIT,fieldsDefinitionType)){
							clearTargetFields(observationForm);
						}else{
					        /*
					         * Clear selected target
					         */
					        observationForm.setSelectedTargetId(-1);
						}
						proposalDelegate.addChangeObservation(proposalData
								.getProposal(), observation,true);
						
						 Set set = new HashSet();
						 List newList = new ArrayList();
						 for (Iterator iter = observingRequest.getObservations().iterator();    iter.hasNext(); ) {
						 Object element = iter.next();
						   if (set.add(element)){
						      newList.add(element);
						   }else{
							  // duplicate found
						   }
						 }
						 observingRequest.getObservations().clear();
						 observingRequest.getObservations().addAll(newList);
						 request.getSession().setAttribute(Constants.OBSERVATION, null);
						 request.getSession().setAttribute(Constants.PROPOSAL_DATA, proposalData);
					     
						
						return mapping.findForward(Constants.SUCCESS);
					}
				}
				
			}
		}
		return null;
		
	}


	/**
	 * @param observationForm
	 * @param mapping
	 * @param observation
	 * @throws DatabaseException
	 */
    public void addOrChangeTarget(ObservationForm observationForm, HttpServletRequest request, ActionMapping mapping)
    	throws DatabaseException
    {
    	/* overwritten in the telescope dependant part. A target is telescope specific */
    }
	
	
	protected ActionForward processTargetButtons(ObservationForm observationForm, ActionMapping mapping,HttpServletRequest request)
    throws DatabaseException
	{
		// commit to list of targets...
		if (!AstronValidator.isBlankOrNull(observationForm.getNewTargetButton())) 
		{		
			
			ActionMessages errors = new ActionMessages();
			if(AstronValidator.isBlankOrNull(observationForm.getFieldName())){
				errors.add("fieldName", new ActionMessage(
						"error.required.applicant.name"));
				saveErrors(request, errors);
				return mapping.findForward("refresh");
				
			}
	        addOrChangeTarget(observationForm, request, mapping);
            observationForm.setSelectedTargetId(-1);
            clearTargetFields(observationForm);
               	
            return mapping.findForward("refresh");
		}
		if (!AstronValidator.isBlankOrNull(observationForm.getNewTargetListButton())) 
		{			

			return mapping.findForward("uploadtargetlist");
		}			
        else if (observationForm.getClearTargetButton() != null)
        {
            observationForm.setSelectedTargetId(-1);
            clearTargetFields(observationForm);
            return mapping.findForward("refresh");
        }
		else if (observationForm.getDeleteTargetButton(0) != null) 
		{
			// observationForm.getTargetSummaryValues().remove(observationForm.getSelectedTargetId());
			deleteTarget(observationForm);
			
			return mapping.findForward("refresh");
		}
        else if (observationForm.getCopyTargetButton(0) != null) 
        {
        	boolean errorMessage= copyTarget(observationForm, request, mapping);
        	if(!errorMessage){
        		return mapping.getInputForward();
        	}
        	return mapping.findForward("refresh");
        }
        else if (observationForm.getEditTargetButton(0) != null) 
        {
        	// new, append a index so the form is refreshed
        	//return ParameterAppender.append(mapping.findForward("refresh"),"index", "0");
        	int id = observationForm.getSelectedTargetId();
            return mapping.findForward("refresh");
        }
        else if (observationForm.getDeleteAllTargetButton() != null) 
        {
        	// new, append a index so the form is refreshed
        	//observationForm.setTempTargets(new ArrayList());
        	//for(int i=0;i<observationForm.getTempTargets().size();i++) {  
	         // 	 observationForm.getTempTargets().remove(i);
            //	 i--;
		    // }
        	while(observationForm.getTempTargets().size() > 0){
        		observationForm.getTempTargets().remove(0);
        	}
        	observationForm.setSelectedTargetId(-1);
            //clearTargetFields(observationForm);
        	//observingRequest.
            return mapping.findForward("refresh");
        }
        else
        {
        	return null;
        }
	}

	protected void deleteTarget(ObservationForm observationForm) {
		observationForm.getTempTargets().remove(observationForm.getSelectedTargetId());
        observationForm.setSelectedTargetId(-1);
	}
//changed return type to boolean to check whether nu,mber of maximum subbands in the same run is already exceed.
	protected boolean copyTarget(ObservationForm observationForm, HttpServletRequest request, ActionMapping mapping) {
		
		String temp = "";
		/*
		Target target = (Target) observationForm.getTempTargets().get(observationForm.getSelectedTargetId());
    	target.setId(null);
    	// hashmap cloning
    	Map allocs = new HashMap(target.getAllocations());
    	
    	target.setAllocations(allocs);
    	
    	observationForm.getTempTargets().add(target);
    	observationForm.setSelectedTargetId(-1);
		*/
		return true;
	}


	/**
	 * The fillObservation method fills the observation
	 * 
	 * @param observationForm
	 * @return
	 */
	protected Observation fillObservation(HttpServletRequest request, Observation observation,	ObservationForm observationForm) 
		throws DatabaseException 
	{
		if (observation == null) 
		{
			observation = new Observation();
		}
		
		observation.setRequiredSchedConstraints(observationForm.getRequiredSchedConstraints());
		observation.setPreferredSchedConstraints(observationForm.getPreferredSchedConstraints());
		return observation;
	}

	protected void processFormSubmit(ObservationForm observationForm)
			throws DatabaseException {

	}

	protected ActionMessages validate(ObservationForm observationForm,
			ActionMapping mapping, HttpServletRequest request) {
		ActionMessages errors = new ActionMessages();


		return errors;
	}
	
	protected void fillTargets(ObservationForm observationForm, ObservingRequest observingRequest, boolean targetsOnTabbedPage)
	throws DatabaseException
	{
		if (AstronValidator.isPositiveInt(observationForm.getIndex())) {
			int index = AstronConverter.toInteger(
					observationForm.getIndex()).intValue();
			
			if (observingRequest.getObservations().size() > index) {
				Observation observation = (Observation) observingRequest
						.getObservations().get(index);
				
				observation.setTargets(observationForm.getTempTargets());
				// update observing request targets..
				List allTargets = observingRequest.getTargets();
				List newTargets = observationForm.getTempTargets();
				//allTargets.addAll(observationForm.getTempTargets());
				//FIXME edited targets should not be here
				boolean found=false;
				Iterator tarit = allTargets.iterator();
				int previd=0;
				while(tarit.hasNext()){
					Target target = (Target) tarit.next();
					if(target != null && target.getId() != null) {
						Iterator newit = newTargets.iterator();
						while(newit.hasNext()){
							Target newTarget = (Target) newit.next();
							if(newTarget != null && newTarget.getId() != null) {
								if(target.getId().intValue() == newTarget.getId().intValue()){
									found=true; 
								}
							}
						}
					}
					if(found=false){
						newTargets.add(target);
					}else{
					 found=false;
					}
				}
				
				observingRequest.setTargets(newTargets);
			}
		}
	}
	
	/*
	 *  HERE the targets are being added to the request and runs
	 */
	protected void fillTargets(ObservationForm observationForm, ObservingRequest observingRequest)
		throws DatabaseException
	{
		observingRequest.setTargets(observationForm.getTempTargets());
		
		/*
		 * connect the runs...
		 */
		
		if(observingRequest.getObservations() != null && 
				observingRequest.getObservations().size() >0){
			int obsSize = observingRequest.getObservations().size();
			//TODO give a warning or make it infinite
			if(obsSize>50){
				obsSize=50;
			}
			for(int i=0;i< obsSize;i++){
				// each observation gets a character:
				Observation obs = (Observation) observingRequest.getObservations().get(i);
				char chr = (char) (i+65);
				
				Iterator targit = observationForm.getTempTargets().iterator();
				List targetList = new ArrayList();
				// now we go trough all targets and all their selected runs and check if the character matches.
				while (targit.hasNext()){
					Target target = (Target) targit.next();
					if(target.getSelectedRuns() != null && target.getSelectedRuns().size() >0 ){
						for (int j=0; j<target.getSelectedRuns().size(); j++) {
							String run = (String) target.getSelectedRuns().get(j);
							if(run.indexOf(chr) >= 0){
								targetList.add(target);
							}
						}
					}
				}
			
				obs.setTargets(targetList);
			}
			//observationForm.setTempTargets(new ArrayList());
		}
		//observation.setTargets(observationForm.getTempTargets());
		/*
		observation.setTargets(new ArrayList());
		Iterator targetValuesIt = observationForm.getTempTargetSummaryValues().iterator();
		while (targetValuesIt.hasNext())
		{
			List targetValues = (List) targetValuesIt.next();
			addNewTarget(observation, targetValues);
		}
		*/
	}
		
	protected String targetValue(List targetValues, int index)
	{
		if (targetValues.get(index)== null)
		{
			return null;
		}
		else
		{
			return ((ValueBean) targetValues.get(index)).getValue();
		}
	}
	protected void addNewTarget(Observation observation, List targetValues)
		throws DatabaseException
	{
		/* overwritten in the telescope specific code */
	}

	
	protected void clearTargetFields(ObservationForm observationForm)
	{
		/* overwritten in the telescope specific code */
	}

	
}