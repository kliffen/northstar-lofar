// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * UploadForm.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: UploadForm.java,v 1.10 2008-07-24 13:37:56 boelen Exp $
 *
 */
package eu.radionet.northstar.control.proposal.upload;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.tar.TarEntry;
import nl.astron.util.tar.TarInputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.business.util.PdfUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.FigureFile;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The UploadForm provides
 * 
 * @author Bastiaan Verhoef
 * 
 */
public class UploadForm extends ActionForm {
	private static final String UPLOAD_MESSAGE = "/text/host/upload.txt";

	private Log log = LogFactory.getLog(this.getClass());

	private String kindUploadFile = null;
	protected FormFile justificationFile = null;
	private String latexClassUrl = null;
	private String latexTemplateUrl = null;
	private boolean showLatex =false;
	private String uploadMessage = null;
	private List fileList=new ArrayList();
	private String selectedFile=null;
	private String deleteFileButton=null;
	private String attachFileButton=null;
	private String previewFileButton = null;
	private String finishButton = null;
	private boolean showSingleFileUpload=false;
	protected boolean variableMaxPageNumber=false;
	public String getUploadMessage() {
		return uploadMessage;
	}

	public void setUploadMessage(String uploadMessage) {
		this.uploadMessage = uploadMessage;
	}

	public boolean isShowLatex()
	{
		return showLatex;
	}
	
	public boolean getShowLatex()
	{
		return showLatex;
	}

	public void setShowLatex(boolean showLatex)
	{
		this.showLatex = showLatex;
	}

	/**
	 * @return Returns the justificationFile.
	 */
	public FormFile getJustificationFile() {
		return justificationFile;
	}

	/**
	 * @param justificationFile
	 *            The justificationFile to set.
	 */
	public void setJustificationFile(FormFile justificationFile) {
		this.justificationFile = justificationFile;
	}

	/**
	 * @return Returns the kindUploadFile.
	 */
	public String getKindUploadFile() {
		return kindUploadFile;
	}

	/**
	 * @param kindUploadFile
	 *            The kindUploadFile to set.
	 */
	public void setKindUploadFile(String kindUploadFile) {
		this.kindUploadFile = kindUploadFile;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {

		ActionErrors errors = new ActionErrors();
		ProposalData proposalData = (ProposalData) request.getSession().getAttribute(Constants.PROPOSAL_DATA);
		Proposal proposal = proposalData.getProposal();
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		if (!OptionsDelegate.allowedToDisplay(Constants.SINGLE_FILE_UPLOAD , fieldsDefinitionType)){
			if(this.attachFileButton == null){
				return errors;
			}
		}
		// Only set derived attributes if (non empty) file has been provided
		/*
		 * if file is not empty
		 */
		if (this.justificationFile != null ){
			/*
			 * if file is bigger then 10 MB -> error
			 */
			if (this.justificationFile.getFileSize() > 10485760) {
				log.warn("Too big file size: ");
				errors.add("justificationFile", new ActionMessage(
						"error.file.toobig"));
			}
			else if(this.justificationFile.getFileSize() ==0 ){
				errors.add("justificationFile", new ActionMessage(
				"error.file.empty"));
			}
			else {
				
				List urlList = new ArrayList();
				String allowedFigureFileTypes = null;
				String allowedJustificationFileTypes = null;
				urlList = OptionsUtils.getLabelValueBeans(Constants.ALLOWED_FIGURE_TYPES ,
							new HashMap(), contextConfiguration);
				if (urlList != null && urlList.size() > 1){
					LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
					allowedFigureFileTypes =  urlValue.getValue();
				}
				
				urlList = OptionsUtils.getLabelValueBeans(Constants.ALLOWED_JUSTIFICATION_TYPES ,
						new HashMap(), contextConfiguration);
				if (urlList != null && urlList.size() > 1){
					LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
					allowedJustificationFileTypes =  urlValue.getValue();
				}
				
				String fileType = justificationFile.getFileName();
				int lastIndex = fileType.lastIndexOf('.');
				// no file extension given
				if (lastIndex > 0){
					fileType = fileType.substring(lastIndex);
				}else{
					log.warn("no justification File wrong type: "
							+ this.justificationFile.getContentType());
					errors.add("justificationFile", new ActionMessage(
							"error.file.wrongtype.scientific"));
					return errors;
				}
				/*
				 * if it is an figure
				 */
				boolean figurefound = false;
				String[] allowedTypes = allowedFigureFileTypes.split(" ");
				for(int i =0; i<allowedTypes.length;i++){
					if(allowedTypes[i].equalsIgnoreCase(fileType)){
						figurefound=true;
					}
					
				}
			
				/*
				 * else a justification file 
				 */

				boolean justificationfound = false;
				allowedTypes = allowedJustificationFileTypes.split(" ");
				for(int i =0; i<allowedTypes.length;i++){
					if(allowedTypes[i].equalsIgnoreCase(fileType)){
						justificationfound=true;
					}
				}
				
				if(justificationfound || figurefound){
					//todo
				}else{
				
					if(!justificationfound || !figurefound){
						log.warn("no justification File wrong type: "
								+ this.justificationFile.getContentType());
						errors.add("justificationFile", new ActionMessage(
								"error.file.wrongtype.scientific"));
					}

					/*if(!figurefound){
						log.warn("figure File wrong type: "
								+ this.justificationFile.getContentType());
						errors.add("justificationFile", new ActionMessage(
								"error.file.wrongtype.figure"));
					}*/
				}
			}
		}
	
		
		if(errors.size() > 0){
			try {
				setUploadMessage(NorthStarUtils.read(UPLOAD_MESSAGE,request.getSession().getServletContext()));
			} catch (IOException e) {
				setUploadMessage("The upload file may not exceed 10MB ");
			}
		//  add scientific file name to the list
			if (proposalData.getProposal().getJustification().getScientificFileName() != null){
				LabelValueBean lvb = new LabelValueBean();
				lvb.setLabel(proposalData.getProposal().getJustification().getScientificFileName());
				lvb.setValue(proposalData.getProposal().getJustification().getScientificFileName());
				this.getFileList().add(lvb);
				}
			
			// extract tar file for figure names
			if(proposalData.getProposal().getJustification().getFigureFileName() != null){
				this.getFileList().addAll(addFigureNames(proposalData));

			}
			
			
		}

		return errors;

	}

	private List addFigureNames(ProposalData proposalData) {
		Proposal proposal = proposalData.getProposal();
		FigureFile figureFile=proposalData.getFigureFile();
		List figureNameList = new ArrayList();
		if (figureFile==null){
			try {
				ProposalDelegate proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposal);
				figureFile = proposalDelegate.getFigureFile(proposal.getJustification());
			} catch (DatabaseException e) {
				e.printStackTrace();
			} catch (InvalidConfigurationException e) {
				e.printStackTrace();
			}
		}
		InputStream inp = new ByteArrayInputStream(figureFile.getUploadFile());
		TarInputStream tis = new TarInputStream(inp);
		TarEntry ter;
		
		String fileName="";
		try {
			while( (ter=tis.getNextEntry()) != null){
				if(ter.isDirectory()){
					// pfff directories..
					fileName=ter.getName()+File.separator;
				}
				else{
					// get filename from entry
				    fileName += ter.getName();
				    LabelValueBean lvb = new LabelValueBean();
				    lvb.setLabel(fileName);
				    lvb.setValue(fileName);
				    figureNameList.add(lvb);
				    fileName="";
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		return figureNameList;

	}

	public String getLatexClassUrl()
	{
		return latexClassUrl;
	}

	public void setLatexClassUrl(String latexClassUrl)
	{
		this.latexClassUrl = latexClassUrl;
	}

	public String getLatexTemplateUrl()
	{
		return latexTemplateUrl;
	}

	public void setLatexTemplateUrl(String latexTemplateUrl)
	{
		this.latexTemplateUrl = latexTemplateUrl;
	}

	public String getPreviewFileButton() {
		return previewFileButton;
	}

	public void setPreviewFileButton(String previewFileButton) {
		this.previewFileButton = previewFileButton;
	}

	public List getFileList() {
		return fileList;
	}

	public void setFileList(List fileList) {
		this.fileList = fileList;
	}

	public String getSelectedFile() {
		return selectedFile;
	}

	public void setSelectedFile(String selectedFile) {
		this.selectedFile = selectedFile;
	}

	public String getDeleteFileButton() {
		return deleteFileButton;
	}

	public void setDeleteFileButton(String deleteFileButton) {
		this.deleteFileButton = deleteFileButton;
	}

	public String getAttachFileButton() {
		return attachFileButton;
	}

	public void setAttachFileButton(String attachFileButton) {
		this.attachFileButton = attachFileButton;
	}

	public String getFinishButton() {
		return finishButton;
	}

	public void setFinishButton(String finishButton) {
		this.finishButton = finishButton;
	}

	public boolean isShowSingleFileUpload() {
		return showSingleFileUpload;
	}

	public void setShowSingleFileUpload(boolean showSingleFileUpload) {
		this.showSingleFileUpload = showSingleFileUpload;
	}
	public boolean isVariableMaxPageNumber() {
		return variableMaxPageNumber;
	}
	public void setVariableMaxPageNumber(boolean variableMaxPageNumber) {
		this.variableMaxPageNumber = variableMaxPageNumber;
	}
}