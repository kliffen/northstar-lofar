// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SetUpProposalAction.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: SetUpProposalListAction.java,v 1.7 2008-06-17 07:43:01 boelen Exp $
 *
 */
package eu.radionet.northstar.control.admin.list;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.ArrayList;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeSet;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Cookie;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.Telescope;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.admin.upload.SetUpUploadConfigAction;
import eu.radionet.northstar.data.entities.Semester;
import eu.radionet.northstar.data.entities.UserTelescope;
import eu.radionet.northstar.data.entities.admin.AdminProposalSummary;

/**
 * The SetUpProposalAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class SetUpProposalListAction extends Action {
	private NorthStarDelegate northStarDelegate = null;
	private Log log = LogFactory.getLog(SetUpUploadConfigAction.class);
	private AdminProposalListForm adminProposalListForm = null;
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		adminProposalListForm = (AdminProposalListForm) form;

		HttpSession session = request.getSession();
		SelectionBean selectionBean = (SelectionBean)session.getAttribute(Constants.ADMIN_PROPOSAL_SELECTION);
		if (selectionBean != null){
			adminProposalListForm.setSelectedTelescopes(selectionBean.getSelectedTelescopes());
			adminProposalListForm.setSelectedCategories(selectionBean.getSelectedCategories());
			adminProposalListForm.setSelectedSemesters(selectionBean.getSelectedSemesters());
			adminProposalListForm.setSelectedStatusses(selectionBean.getSelectedStatusses());
			adminProposalListForm.setSelectedCommunities(selectionBean.getSelectedCommunities());
			adminProposalListForm.setSelectedYear(selectionBean.getSelectedYear());
			adminProposalListForm.setForceProjectId(selectionBean.isForceProjectId());
		} 
		else{
			Cookie[] cookies = request.getCookies();
			retrieveFromCookies(cookies);
		}
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		/*
		 * get instance of ProposalDelegate
		 */
		northStarDelegate = NorthStarDelegate.getInstance();
		/*
		 * unlock proposals
		 */
		// collaborationDelegate.unlockProposals(ownUserAccount,
		// session.getId());
		/*
		 * retrieve own proposals
		 */
		adminProposalListForm.setStatusses(northStarDelegate.getStatusses());
		adminProposalListForm.setCategories(northStarDelegate.getCategories());
		
		adminProposalListForm.setTelescopeList(getTelescopes(ownUserAccount.getId()));
		
		adminProposalListForm.setCommunities(NorthStarConfiguration.getCommunityConfigurations());			
		
		// ugly semester selection routine
		List semesters = northStarDelegate.getSemesters();
		Set semestersSet = new TreeSet();
		List yearList = new ArrayList();
		Calendar cal = new GregorianCalendar();
		int minyear = cal.get(Calendar.YEAR);
		int maxyear = cal.get(Calendar.YEAR);
		int selyear = cal.get(Calendar.YEAR);
		boolean allYears = false;
		
		if (adminProposalListForm.getSelectedYear() != null && adminProposalListForm.getSelectedYear() != ""){
			if (adminProposalListForm.getSelectedYear().equalsIgnoreCase("all")){
				allYears = true;
			}else{
				try{
					selyear = Integer.valueOf(adminProposalListForm.getSelectedYear()).intValue();
				}
				catch (NumberFormatException NFE){
					// if no integer can be made, select all years instead of internal server error
					allYears=true;
				}
			}
		}
		// iterate trough semesters and add to the treeSet when the years match.
		Iterator semestersIterator = semesters.iterator();
		while (semestersIterator.hasNext()){
			Semester semester = (Semester) semestersIterator.next();
			cal.setTime(semester.getStartDate());
			int tt = cal.get(Calendar.YEAR);
			if(tt < minyear) minyear=tt;
			if(tt > maxyear) maxyear=tt;
			if(tt == selyear || allYears) semestersSet.add(semester.getSemester());
			// check if end date is in the same year as the start date.
			cal.setTime(semester.getEndDate());
			if (tt != cal.get(Calendar.YEAR)){
				tt = cal.get(Calendar.YEAR);
				if(tt > maxyear) maxyear=tt;
				if(tt == selyear || allYears) semestersSet.add(semester.getSemester());
			}
		}
		if (minyear != maxyear){
			yearList.add(new Cookie("all","all"));
		}
		for (int i=minyear;i<=maxyear;i++){
			// abusing cookies as yearlist objects, because they have getters and setters for name and value
			Cookie tc = new Cookie(Integer.toString(i),Integer.toString(i));
			yearList.add(tc);
		}
		
		adminProposalListForm.setYearList(yearList);
//		semestersSet.add("Not assigned");
		adminProposalListForm.setSemesters(semestersSet);
		String[] selectedSemesters = new String[adminProposalListForm.getSelectedSemesters().length];
		for (int i = 0 ; i < selectedSemesters.length; i++){
			if (adminProposalListForm.getSelectedSemesters()[i].equals("Not assigned")){
				selectedSemesters[i] = null;
			}else {
				selectedSemesters[i] =  adminProposalListForm.getSelectedSemesters()[i];
			}
		}
		if (adminProposalListForm.getSelectedTelescopes() != null && adminProposalListForm.getSelectedCommunities() != null) {
			for (int i = 0; i < adminProposalListForm.getSelectedTelescopes().length; i++) {
				String telescope = adminProposalListForm.getSelectedTelescopes()[i];
				Telescope telescopeConfig = NorthStarConfiguration
						.getTelescope(telescope);
				if(telescopeConfig != null){
					String description = telescopeConfig.getDescription();
					for (int j = 0; j < adminProposalListForm.getSelectedCommunities().length; j++) {
						String community = adminProposalListForm.getSelectedCommunities()[j];
						String communityDescription = NorthStarConfiguration
						.getCommunityDescription(community);
						List proposalSummaries = northStarDelegate.getAdminProposalSummaries(adminProposalListForm.isForceProjectId() ,adminProposalListForm
								.getSelectedStatusses(), adminProposalListForm
								.getSelectedCategories(),telescope, selectedSemesters,community);
						AdminTelescopeBean telescopeBean = fillTelescopeBean(telescope, description,communityDescription,proposalSummaries,
								ownUserAccount);
						if (telescopeBean.getAdminProposalBeans().size()>0){
							adminProposalListForm.getAdminTelescopeBeans().add(telescopeBean);
						}
					}
				}

			}
		}

		return mapping.findForward("showProposalList");
	}


	private AdminTelescopeBean fillTelescopeBean(String telescope,
			String description, String community, List proposalSummaries, UserAccount ownUserAccount)
			throws DatabaseException {
		AdminTelescopeBean telescopeBean = new AdminTelescopeBean();
		telescopeBean.setTelescope(telescope);
		telescopeBean.setDescription(description);
		telescopeBean.setCommunity(community);
		Iterator proposalSummaryIterator = proposalSummaries.iterator();
		while (proposalSummaryIterator.hasNext()) {
			AdminProposalSummary proposal = (AdminProposalSummary) proposalSummaryIterator.next();
			AdminProposalBean proposalBean = new AdminProposalBean();
			proposalBean.setId(proposal.getId().toString());
			proposalBean.setCode(proposal.getCode());
			proposalBean.setTitle(proposal.getTitle());
			proposalBean.setCategory(proposal.getSemester().getCategory()
					.getCode());
			proposalBean.setStatus(proposal.getCurrentStatus().getStatus()
					.getCode());

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
					Constants.DATETIME_FORMAT);
			Calendar UTCCal = new GregorianCalendar(TimeZone
					.getTimeZone("UTC"));
			UTCCal.setTime(proposal.getCurrentStatus().getStatusTime());
			simpleDateFormat.setCalendar(UTCCal);

			proposalBean.setStatusTime(simpleDateFormat.format(UTCCal
					.getTime()));
			if (proposal.getContactAuthor()!= null){
			proposalBean.setContactAuthorName(proposal.getContactAuthor()
					.getName());
			}
			if (proposal.getPi()!= null){
			proposalBean.setPiName(proposal.getPi()
					.getName());
			}

			telescopeBean.getAdminProposalBeans().add(proposalBean);
		}
			//Iterator members = proposal.getMembers().iterator();
			/*
			 * if proposal has members
			 */
/*			while (members.hasNext()) {
				Member member = (Member) members.next();
*/				/*
				 * if member is an registered member
				 */
/*				if (member.getClass().equals(RegisteredMember.class)) {
					RegisteredMember registeredMember = (RegisteredMember) member;

					if (registeredMember.isContactAuthor()) {
						proposalBean.setContactAuthorName(registeredMember
								.getUser().getLastName());
					}
					if (member.isPi()) {
						proposalBean.setPiName(registeredMember.getUser()
								.getLastName());

					}
				} else if (member.isPi()) {
					proposalBean.setPiName(member.getName());

				}
			}
			TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
					.getTelescopeConfiguration(proposal);
			if (telescopeConfiguration != null) {
				proposalBean.setCategory(proposal.getSemester().getCategory()
						.getCode());
				proposalBean.setStatus(proposal.getCurrentStatus().getStatus()
						.getCode());

				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
						Constants.DATETIME_FORMAT);
				Calendar UTCCal = new GregorianCalendar(TimeZone
						.getTimeZone("UTC"));
				UTCCal.setTime(proposal.getCurrentStatus().getStatusTime());
				simpleDateFormat.setCalendar(UTCCal);

				proposalBean.setStatusTime(simpleDateFormat.format(UTCCal
						.getTime()));
				if (proposal.getJustification() != null) {
					proposalBean.setTitle(AstronConverter.resize(proposal
							.getJustification().getTitle(), 30));
				}



				telescopeBean.getProposalBeans().add(proposalBean);
			}
		}*/
		return telescopeBean;
	}
	
	private List getTelescopes(Integer userId){
		List telescopeNames = new ArrayList();
		try{
			if(!NorthStarConfiguration.getRestrictUserTelescope()){
				return NorthStarConfiguration.getTelescopeNames();
			}
			
			List accessRights = northStarDelegate.getTelescopeAccessRights(userId);
			Iterator accessIterator = accessRights.iterator();
			while(accessIterator.hasNext()){
				UserTelescope myTelescope = (UserTelescope) accessIterator.next();
				if(myTelescope.getTelescope().equalsIgnoreCase("all") ){
					return NorthStarConfiguration.getTelescopeNames();
				}
				else{
					telescopeNames.add(myTelescope.getTelescope());
				}
			}
		} catch (DatabaseException de) {
            log.error("Cannot retrieve user accounts: "
                    + de.getMessage());
        }
		return telescopeNames;
		
	}
	
	private void retrieveFromCookies(Cookie[] cookies){
		
		String[] sel = null;
		for (int i=0;i < cookies.length;i++){
			Cookie cookie = cookies[i];
			String cName = cookie.getName();

			if (cName.equalsIgnoreCase("categories")){
				sel= cookie.getValue().split("##");
				adminProposalListForm.setSelectedCategories(sel);
			}
			else if (cName.equalsIgnoreCase("semesters")){
				sel= cookie.getValue().split("##");
				adminProposalListForm.setSelectedSemesters(sel);
			}
			else if (cName.equalsIgnoreCase("telescopes")){
				sel= cookie.getValue().split("##");
				adminProposalListForm.setSelectedTelescopes(sel);
			}
			else if (cName.equalsIgnoreCase("statusses")){
				sel= cookie.getValue().split("##");
				adminProposalListForm.setSelectedStatusses(sel);
			}
			else if (cName.equalsIgnoreCase("communities")){
				sel= cookie.getValue().split("##");
				adminProposalListForm.setSelectedCommunities(sel);
			}
			else if (cName.equalsIgnoreCase("selectedYear")){
				adminProposalListForm.setSelectedYear(cookie.getValue());
			}
		}
	}
}
