// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.manage.invitation;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.collaboration.Invitation;

public class SetUpInvitationAction extends Action {
	
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		InvitationForm invitationForm = (InvitationForm) form;
		HttpSession session = request.getSession();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		CollaborationDelegate collaborationDelegate = CollaborationDelegate.getInstance();
		if(invitationForm.getKey() == null || invitationForm.getKey().length() != 20){
			// incomplete acceptation
			invitationForm.setAcceptMessage("The given acceptcode is invalid<br>"+
					"make sure that the url ends with key= and then 20 characters<br>"+
					"<br>");
			return mapping.findForward(Constants.REFRESH);
		}
		
		Invitation invitation = collaborationDelegate.getInvitation(invitationForm.getKey());
		
		String serverPath = request.getScheme() + "://"+ request.getServerName();
		if (request.getScheme().equals("http") && request.getServerPort() != 80) {
			serverPath += ":" + request.getServerPort();
		} else if (request.getScheme().equals("https") || request.getServerPort() != 443) {
			serverPath += ":" + request.getServerPort();
		}
		if(invitation != null){
			NonRegisteredMember member = null;
			member = collaborationDelegate.getMemberById(invitation.getMemberId());
			if(member != null){
				if (ownUserAccount.getUser().getEmail().equalsIgnoreCase(member.getEmail()) ){
					
					collaborationDelegate.acceptInvitation(invitation, member, ownUserAccount);
					
					
					invitationForm.setAcceptMessage("invitation accepted.<BR> "+
							"When you click on the ok button, you will be redirected to the manage page at:\n"+
							 serverPath	+ "/proposal/manager\n"+
							 "Go to that page for further instructions.");
					invitationForm.setKeyIsAccepted(true);
					return mapping.findForward(Constants.REFRESH);
				}else{
					
					invitationForm.setAcceptMessage("email adres mismatch, expected: "+member.getEmail()+" \n"+
							"please ask the person who invited you to use the name: \n" +
							ownUserAccount.getUser().getLastName()+
							" \n and the adres: \n"+
							ownUserAccount.getUser().getEmail()+" \n");
					return mapping.findForward(Constants.REFRESH);
				}
			}
		}
		invitationForm.setAcceptMessage("The invitation has not been found, \n"+
				"the invitation is probably already accepted. \n"+
				"go to "+ serverPath	+ "/proposal/manager \n"+
				"and check if you have access to manage a telescope if not, ask for a new invitation.");
		invitationForm.setKeyIsAccepted(true);
		return mapping.findForward(Constants.REFRESH);
		
	}

}
