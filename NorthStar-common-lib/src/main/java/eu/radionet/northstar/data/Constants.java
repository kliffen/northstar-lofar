// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Constants.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: Constants.java,v 1.1 2006-05-02 10:01:13 verhoef Exp $
 *
 */
package eu.radionet.northstar.data;

/**
 * The Constants provides
 *
 * @author Bastiaan Verhoef
 *
 */
public class Constants {
    public static final int PI = 1;
    public static final int CONTACT_AUTHOR = 2;
    public static final int POTENTIAL_OBSERVER = 3;
    public static final int REGULAR = 1;
    public static final int SERVICE = 2;
    public static final int URGENT =3;	
    public static final int IN_PREPARATION = 1; 
    public static final int SUBMITTED = 2;
    public static final int UNDER_REVIEW = 3;
    public static final int ACCEPTED = 4;
    public static final int REJECTED = 5;
    public static final String OTHER_SYSTEM = "Other system";
	public static final String ENABLE_FIRST_JUSTIFICATION = "enableFirstJustification";
	public static final String ENABLE_SECONOD_JUSTIFICATION = "enableSecondJustification";
	public static final String ENABLE_FIGURES_FILE = "enableFiguresFile";
	public static final String ENABLE_SINGLE_FILE = "singleFileUPload";
    

}
