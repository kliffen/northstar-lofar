// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business;

public class Constants {
	public static final String SUBJECT_START = "<SUBJECT>";
    public static final String SUBJECT_END = "</SUBJECT>";
    
    public static final String STATUS_IN_PREPARATION = "in preparation";
    public static final String STATUS_SUBMITTED = "submitted";
    public static final String STATUS_UNDER_REVIEW = "under review";
    public static final String STATUS_ACCEPTED = "accepted";
    public static final String STATUS_REJECTED = "rejected";
    public static final String STATUS_INVALID = "invalid";	
    public static final String CATEGORY_URGENT = "urgent";
    public static final String CATEGORY_REGULAR = "regular";
    public static final String CATEGORY_UMBRELLA = "umbrella";


}
