// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.configuration;

import java.lang.reflect.Constructor;
import java.util.Calendar;

import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.business.ProposalValidator;
import eu.radionet.northstar.business.ProposalXMLConverter;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.email.EmailGenerator;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.pdf.ProposalWriter;
import eu.radionet.northstar.business.targetlist.TargetListParser;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Semester;

public class ConfigurationUtil {
	protected static NorthStarConfiguration configuration = null;

	private static ConfigurationUtil configurationUtil = null;

	private ConfigurationUtil() {

	}

	public static ConfigurationUtil getInstance() {
		if (configurationUtil == null) {
			configurationUtil = new ConfigurationUtil();
			configuration = NorthStarConfiguration.getInstance();
		}
		return configurationUtil;
	}

	public ObservingRequest getObservingRequest(Semester semester,
			Calendar currentDate) throws InvalidConfigurationException {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(semester, currentDate);
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getObservingRequestClass().getConstructor(new Class[] {});
			return (ObservingRequest) sourceConstructor.newInstance(new Object[] {});

		} catch (Exception e) {
			throw new InvalidConfigurationException(e.getMessage());
		}

	}

	public ObservingRequest getObservingRequest(
			TelescopeConfiguration telescopeConfiguration)
			throws InvalidConfigurationException {
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getObservingRequestClass().getConstructor(new Class[] {});
			return (ObservingRequest) sourceConstructor.newInstance(new Object[] {});

		} catch (Exception e) {
			throw new InvalidConfigurationException(e.getMessage());
		}

	}
	public Observation getObservation(
			TelescopeConfiguration telescopeConfiguration)
			throws InvalidConfigurationException {
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getObservationClass().getConstructor(new Class[] {});
			return (Observation) sourceConstructor.newInstance(new Object[] {});

		} catch (Exception e) {
			throw new InvalidConfigurationException(e.getMessage());
		}

	}

	public TargetListParser getTargetListParser(Proposal proposal)
			throws InvalidConfigurationException {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		ProposalDelegate proposalDelegate = ProposalDelegateFactory
				.getProposalDelegate(proposal);
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getTargetListParserClass().getConstructor(
							new Class[] { ProposalDelegate.class });
			return (TargetListParser) sourceConstructor
					.newInstance(new Object[] { proposalDelegate });

		} catch (Exception e) {
			throw new InvalidConfigurationException(e.getMessage());
		}

	}

	public ProposalWriter getProposalPdfWriter(Proposal proposal)
			throws InvalidConfigurationException {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getPdfWriterClass().getConstructor(new Class[] {});
			return (ProposalWriter) sourceConstructor.newInstance(new Object[] {});

		} catch (Exception e) {
			throw new InvalidConfigurationException(e.getMessage());
		}

	}

	public ProposalXMLConverter getXMLWriter(Proposal proposal)
			throws InvalidConfigurationException {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getXmlWriterClass().getConstructor(new Class[] {});
			return (ProposalXMLConverter) sourceConstructor.newInstance(new Object[] {});

		} catch (Exception e) {
			throw new InvalidConfigurationException(e.getMessage());
		}

	}

	public ProposalValidator getProposalValidator(Proposal proposal)
			throws InvalidConfigurationException {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getProposalValidatorClass().getConstructor(new Class[] {ContextType.class});
			return (ProposalValidator) sourceConstructor.newInstance(new Object[] {contextConfiguration});

		} catch (Exception e) {
			try {
				Constructor sourceConstructor = telescopeConfiguration
						.getProposalValidatorClass().getConstructor(new Class[] {});

				return (ProposalValidator) sourceConstructor.newInstance(new Object[] {});

			} catch (Exception ee) {
				throw new InvalidConfigurationException(ee.getMessage());
			}
		}


	}

	public ProposalWriter getProposalPdfWriter(
			TelescopeConfiguration telescopeConfiguration)
			throws InvalidConfigurationException {
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getPdfWriterClass().getConstructor(new Class[] {});
			return (ProposalWriter) sourceConstructor.newInstance(new Object[] {});
		} catch (Exception e) {
			throw new InvalidConfigurationException(e.getMessage());
		}
	}

	public EmailGenerator getEmailGenerator(
			TelescopeConfiguration telescopeConfiguration)
			throws InvalidConfigurationException {
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getEmailGeneratorClass().getConstructor(new Class[] {});
			return (EmailGenerator) sourceConstructor.newInstance(new Object[] {});
		} catch (Exception e) {
			throw new InvalidConfigurationException(e.getMessage());
		}
	}

	public ProposalDelegate getProposalDelegate(
			TelescopeConfiguration telescopeConfiguration)
			throws InvalidConfigurationException {
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getProposalDelegateClass().getConstructor(new Class[] {});
			return (ProposalDelegate) sourceConstructor.newInstance(new Object[] {});
		} catch (Exception e) {
			throw new InvalidConfigurationException(e.getMessage());
		}
	}

	public ProposalDelegate getProposalDelegate(Proposal proposal)
			throws InvalidConfigurationException {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		try {
			Constructor sourceConstructor = telescopeConfiguration
					.getProposalDelegateClass().getConstructor(new Class[] {});
			return (ProposalDelegate) sourceConstructor.newInstance(new Object[] {});

		} catch (Exception e) {
			throw new InvalidConfigurationException(e.getMessage());
		}

	}

}
