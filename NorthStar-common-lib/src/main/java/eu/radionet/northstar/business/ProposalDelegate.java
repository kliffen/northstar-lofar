// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProposalDelegate.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: ProposalDelegate.java,v 1.5 2008-08-13 10:08:53 boelen Exp $
 *
 */
package eu.radionet.northstar.business;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.naming.NamingException;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.User;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import nl.astron.util.XMLBuilder;
import nl.astron.util.exception.AstronMailException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.configuration.ConfigurationUtil;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.deadline.TaskDaemon;
import eu.radionet.northstar.business.email.TelescopeEmailGenerator;
import eu.radionet.northstar.business.exception.BadJustificationFileException;
import eu.radionet.northstar.business.exception.DeadLinePassedException;
import eu.radionet.northstar.business.exception.DeprecatedException;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.exception.NorthStarException;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.Repository;
import eu.radionet.northstar.data.entities.Category;
import eu.radionet.northstar.data.entities.FigureFile;
import eu.radionet.northstar.data.entities.Justification;
import eu.radionet.northstar.data.entities.JustificationFile;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalError;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.Role;
import eu.radionet.northstar.data.entities.ScientificFile;
import eu.radionet.northstar.data.entities.Semester;
import eu.radionet.northstar.data.entities.Status;
import eu.radionet.northstar.data.entities.Target;
import eu.radionet.northstar.data.entities.TechnicalDetailsFile;
import eu.radionet.northstar.data.entities.Thesis;
import eu.radionet.northstar.data.entities.admin.StatusEmail;

/**
 * The ProposalDelegate provides
 * 
 * @author Bastiaan Verhoef
 */
public abstract class ProposalDelegate {
	protected Log log = LogFactory.getLog(this.getClass());

	// private static ProposalDelegate proposalDelegate = null;
	protected static CollaborationDelegate collaborationDelegate = null;

	protected static ConfigurationUtil configurationUtil = null;

	protected Repository repository = null;

	protected static NorthStarDelegate northStarDelegate = null;

	/**
	 * Default constructor
	 */
	public ProposalDelegate() throws ConnectionException {
		repository = getRepository();
		northStarDelegate = NorthStarDelegate.getInstance();
		collaborationDelegate = CollaborationDelegate.getInstance();
		configurationUtil = ConfigurationUtil.getInstance();
	}

	public void reassociate(Object object) throws DatabaseException {
		repository.reassociate(object);
	}

	protected abstract Repository getRepository() throws ConnectionException;

	/**
	 * The getInstance method provides
	 * 
	 * @return
	 * @throws ConnectionException
	 */
	/*
	 * public static ProposalDelegate getInstance() throws ConnectionException {
	 * if (null == proposalDelegate) { proposalDelegate = new
	 * ProposalDelegate(); } return proposalDelegate; }
	 */
	public Proposal store(Proposal proposal, UserAccount userAccount,
			String sessionId) throws DatabaseException, AstronMailException,
			NamingException {
		collaborationDelegate.addRegisteredMembers(proposal, userAccount);
		storeWithoutLocking(proposal, userAccount);
		if (!collaborationDelegate.isLocked(proposal.getId())) {
			collaborationDelegate.lockProposal(proposal.getId(), userAccount,
					sessionId);

		}
		collaborationDelegate.sendAndDeleteInvitations(proposal, userAccount);

		return proposal;
	}

	public Proposal storeWithoutSendingMails(Proposal proposal,
			UserAccount userAccount, String sessionId) throws DatabaseException {
		storeWithoutLocking(proposal, userAccount);
		if (!collaborationDelegate.isLocked(proposal.getId())) {
			collaborationDelegate.lockProposal(proposal.getId(), userAccount,
					sessionId);

		}
		return proposal;
	}

	/**
	 * The store method stores an proposal
	 * 
	 * @param proposal
	 * @return Proposal with identifier
	 * @throws DatabaseException
	 */
	protected Proposal storeWithoutLocking(Proposal proposal,
			UserAccount userAccount) throws DatabaseException {
		repository.store(proposal);
		log.info(LogMessage.getMessage(userAccount, "Store proposal with id: "
				+ proposal.getId()));
		return proposal;
	}

	/**
	 * The store method stores an justification file
	 * 
	 * @param justificationFile
	 * @throws DatabaseException
	 */
	public void store(JustificationFile justificationFile)
			throws DatabaseException {
		repository.store(justificationFile);
	}

	/**
	 * The delete method deletes an proposal
	 * 
	 * @param proposal
	 * @throws DatabaseException
	 */
	public void delete(Proposal proposal, String sessionId,
			UserAccount userAccount) throws DatabaseException {
		List itemsToBeDeleted = new ArrayList();
		if (proposal.getJustification() != null) {
			List justificationFiles = getJustificationFiles(proposal
					.getJustification());
			for (int i = 0; i < justificationFiles.size(); i++) {
				JustificationFile justificationFile = (JustificationFile) justificationFiles
						.get(i);
				if (justificationFile != null) {
					itemsToBeDeleted.add(justificationFile);
				}
			}

		}

		Integer proposalId = proposal.getId();
		itemsToBeDeleted.add(proposal);
		repository.delete(itemsToBeDeleted);
		collaborationDelegate.deleteInvitations(proposalId);
		log.info(LogMessage.getMessage(userAccount, "Delete proposal with id: "
				+ proposalId));
		collaborationDelegate
				.unlockProposal(proposalId, userAccount, sessionId);
	}

	public void deleteFigureFile(Justification justification)
			throws DatabaseException {
		if (justification != null) {
			FigureFile figureFile = repository.getFigureFile(justification
					.getId());
			if (figureFile != null) {
				repository.delete(figureFile);
			}

		}

	}

	public void deleteTechnicalDetailsFile(Justification justification)
			throws DatabaseException {
		if (justification != null) {
			TechnicalDetailsFile technicalDetailsFile = repository
					.getTechnicalDetailsFile(justification.getId());
			if (technicalDetailsFile != null) {
				repository.delete(technicalDetailsFile);
			}

		}

	}

	protected abstract void removeInvalidAllocations(
			ObservingRequest observingRequest) throws DatabaseException;



	/**
	 * The copyProposal method copies existing proposal and store it (include
	 * the justification files)
	 * 
	 * @param proposal
	 * @return
	 */
	public void copyProposalAndStore(Proposal oldProposal,
			UserAccount userAccount, Semester semester)
			throws DatabaseException, InvalidConfigurationException {
		Proposal newProposal = copyProposal("Copy of ", oldProposal,
				userAccount, semester);
		for (int i = 0; i < newProposal.getMembers().size(); i++) {
			Member member = (Member) newProposal.getMembers().get(i);
			/*
			 * if member is an registered member
			 */
			if (member.getClass().equals(RegisteredMember.class)) {
				RegisteredMember registeredMember = (RegisteredMember) member;
				/*
				 * if registeredMember is registered to ownUser
				 */
				if (registeredMember.getUserId().intValue() == userAccount
						.getUser().getId().intValue()) {
					registeredMember.addRole(repository.getContactAuthorRole());

				} else {
					registeredMember.removeRole(repository
							.getContactAuthorRole());
				}
			}
		}
		copyJustificationFilesAndStoreProposal(newProposal, oldProposal,
				userAccount);
	}

	private void copyTargets(Proposal newProposal, Proposal oldProposal) {
		List<Target> targets = oldProposal.getObservingRequest().getTargets();
		for (Target target : targets){
			Target newTarget = copyTarget(target);
			newProposal.getObservingRequest().getTargets().add(newTarget);
		}
		List<Observation> observations =  oldProposal.getObservingRequest().getObservations();
		for(Observation observation : observations){
			List observationTargets = observation.getTargets();
		}
		
	}

	/**
	 * The copyProposal method copies existing proposal (part 1 of a copy)
	 * 
	 * @param prefix
	 *            Prefix for the title
	 * @param oldProposal
	 * @param userAccount
	 * @param semester
	 * @return
	 * @throws InvalidConfigurationException
	 * @throws DatabaseException
	 */
	protected Proposal copyProposal(String prefix, Proposal oldProposal,
			UserAccount userAccount, Semester semester)
			throws InvalidConfigurationException, DatabaseException {
		TelescopeConfiguration config = NorthStarConfiguration
				.getTelescopeConfiguration(semester, NorthStarDelegate
						.getCurrentDate());

		Proposal newProposal = new Proposal();
		boolean differentTelescope = false;

		ObservingRequest observingRequest = configurationUtil
				.getObservingRequest(semester, NorthStarDelegate
						.getCurrentDate());
		/*
		 * if looks if observingRequest is changes and if there are some
		 * observation. \ Fill it.
		 */
		if (oldProposal.getObservingRequest() != null
				&& !oldProposal.getObservingRequest().getClass().equals(
						observingRequest.getClass())) {
			differentTelescope = true;
			/*
			 * fill the allocation maps for copy
			 */
			newProposal.addObservingRequest(observingRequest);
			List oldObservations = oldProposal.getObservingRequest()
					.getObservations();
			for (int i = 0; i < oldObservations.size(); i++) {
				Observation observation = configurationUtil.getObservation(config);
				/*
				 * fill the allocation maps for copy
				 */
				addChangeObservation(config,newProposal,
						observation,true);
			}

		}

		
		newProposal = (Proposal) repository.copy(oldProposal, newProposal);
		if (differentTelescope) {
			removeInvalidAllocations(newProposal.getObservingRequest());
		}
		// make hard copies of the targets, so they don't get messed up between 2 proposals.
		reassignTargets(oldProposal,newProposal);
		
		newProposal.setVersion(config.getVersion());
		newProposal.setCode(null);
		if (oldProposal.getAdditionalIssues() != null) {
			List oldTheses = oldProposal.getAdditionalIssues().getTheses();
			List newTheses = newProposal.getAdditionalIssues().getTheses();
			for (int i = 0; i < oldTheses.size(); i++) {
				Thesis oldThesis = (Thesis) oldTheses.get(i);
				Thesis newThesis = (Thesis) newTheses.get(i);
				if (oldThesis.getStudent() != null) {
					int index = getMemberIndex(oldThesis.getStudent(),
							oldProposal.getMembers());
					newThesis.setStudent((Member) newProposal.getMembers().get(
							index));
				}
				if (oldThesis.getSupervisor() != null) {
					int index = getMemberIndex(oldThesis.getSupervisor(),
							oldProposal.getMembers());
					newThesis.setSupervisor((Member) newProposal.getMembers()
							.get(index));
				}
			}
		}
		/*
		 * if justification not exist
		 */
		if (newProposal.getJustification() == null) {
			newProposal.setJustification(new Justification());

		}
		/*
		 * if title is not null
		 */
		if (newProposal.getJustification().getTitle() != null) {
			newProposal.getJustification().setTitle(
					prefix + newProposal.getJustification().getTitle());
		} else {
			newProposal.getJustification().setTitle(prefix);
		}
		newProposal.setSemester(semester);
		return newProposal;
	}
	

	private void reassignTargets(Proposal oldProposal, Proposal newProposal) {
		// copy the target list
		List<Target> oldTargets = oldProposal.getObservingRequest().getTargets();
		List<Observation> oldObservations = oldProposal.getObservingRequest().getObservations();
		List<Observation> newObservations = newProposal.getObservingRequest().getObservations();
		// clear targets and observations
		List<Target> newTargets = new ArrayList();
		for(Observation tempObs : newObservations){
			tempObs.setTargets(new ArrayList());
		}
		
		// loop trough old targets, and see if they are assigned to observations
		for(Target oldTarget : oldTargets){
			Target newTarget = copyTarget(oldTarget);
			newTargets.add(newTarget);
			// now see if this target was assigned to any observations...
			for(int i=0;i<oldObservations.size();i++){
				Observation observation = (Observation) oldObservations.get(i);
				List<Target> tempTargets = observation.getTargets();
				for (Target assignedTarget : tempTargets ){
					// if so, then add it to the same new observation.
					if(oldTarget.equals(assignedTarget)){
						Observation newObservation = (Observation) newObservations.get(i);
						newObservation.getTargets().add(newTarget);
					}
				}
			}
		}
		newProposal.getObservingRequest().setTargets(newTargets);
	}

	public void addChangeObservation(Proposal proposal, Observation observation, boolean isObservation) {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
		.getTelescopeConfiguration(proposal);
		addChangeObservation(telescopeConfiguration,proposal,observation, isObservation);
	}
	
	/**
	 * Copy justification files and store the proposal (part 2 of a copy)
	 * 
	 * @param newProposal
	 * @param oldProposal
	 * @param userAccount
	 * @throws DatabaseException
	 * @throws InvalidConfigurationException
	 */
	protected void copyJustificationFilesAndStoreProposal(Proposal newProposal,
			Proposal oldProposal, UserAccount userAccount)
			throws DatabaseException, InvalidConfigurationException {
		storeAsNewProposal(newProposal, userAccount);
		/*
		 * if justification exists
		 */
		if (oldProposal.getJustification() != null) {

			List justificationFiles = getJustificationFiles(oldProposal
					.getJustification());
			for (int i = 0; i < justificationFiles.size(); i++) {
				JustificationFile oldJustificationFile = (JustificationFile) justificationFiles
						.get(i);

				if (oldJustificationFile != null) {
					JustificationFile newJustificationFile = (JustificationFile) repository
							.copy(oldJustificationFile);
					newJustificationFile.setJustificationId(newProposal
							.getJustification().getId());

					store(newJustificationFile);
				}
			}
		}
	}

	protected int getMemberIndex(Member selectedMember, List members) {
		for (int i = 0; i < members.size(); i++) {
			Member member = (Member) members.get(i);
			if (member.getId().intValue() == selectedMember.getId().intValue()) {
				return i;
			}
		}
		return -1;
	}
	
	public boolean exceedMaxAllowedObservations(Proposal proposal, List observations) {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		List urlList = new ArrayList();
		String Url = null;
		
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_OBSERVATIONS  ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			  Url =  urlValue.getValue(); 
			}
			
		int maxNumberOfObservations = AstronConverter.toInteger(Url).intValue();
		int numberOfObservations = proposal.getObservingRequest()
				.getObservations().size();
		int numberOfNewObservations = observations.size();
		/*
		 * if number of new and existing observations are greater then max
		 * number of observation, add new observation until maximum is reached
		 */
		if (numberOfObservations + numberOfNewObservations > maxNumberOfObservations) {
			return true;
		} else {
			return false;
		}
	}
	
	protected void addChangeObservation(TelescopeConfiguration telescopeConfiguration,Proposal proposal, Observation observation, boolean isObservation) {
		/*
		 * if no id, add observation
		 */
		if (observation.getId() == null) {
			ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
			List urlList = new ArrayList();
			String Url = null;
			
			urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_OBSERVATIONS ,
						new HashMap(), contextConfiguration);
			int maxNumberOfObservations = 100;
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
				Url =  urlValue.getValue(); 
				maxNumberOfObservations = AstronConverter.toInteger(Url).intValue();
			}
				
			ObservingRequest observingRequest = proposal.getObservingRequest(); 
			int numberOfObservations = observingRequest.getObservations().size();
			/*
			 * if number of new and existing observations are greater then max
			 * number of observation, add new observation until maximum is
			 * reached
			 */
			if (numberOfObservations + 1 <= maxNumberOfObservations) {
				if(isObservation){
					observation.setObservationKind(null);
					//FIXME give the observation an id at the first store so the ordering stays correctly...
					//repository.store(observation);
					observingRequest.getObservations().add(observation);
				}else{
					observation.setObservationKind("pipeline");
					//FIXME give the observation an id at the first store so the ordering stays correctly...
					//repository.store(observation);
					observingRequest.getPipelines().add(observation);
				}
			}
		} else {
			
			if(isObservation){
				boolean found = false;
				for (int i = 0; i < proposal.getObservingRequest()
						.getObservations().size()
						&& !found; i++) {
					Observation observationTemp = (Observation) proposal
							.getObservingRequest().getObservations().get(i);
					if (observationTemp.getId() != null) {
						if (observation.getId().intValue() == observationTemp
								.getId().intValue()) {
							proposal.getObservingRequest().getObservations().set(i,observation);
							found = true;
						}
					}
				}
			}else{
				boolean found = false;
				for (int i = 0; i < proposal.getObservingRequest().getPipelines().size()
						&& !found; i++) {
					Observation observationTemp = (Observation) proposal
							.getObservingRequest().getPipelines().get(i);
					if (observationTemp.getId() != null) {
						if (observation.getId().intValue() == observationTemp
								.getId().intValue()) {
							proposal.getObservingRequest().getPipelines().set(i,observation);
							found = true;
						}
					}
				}
			}
		}

	}

	public void addObservations(Proposal proposal, List observations) {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		List urlList = new ArrayList();
		String Url = null;
		
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_OBSERVATIONS ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			  Url =  urlValue.getValue(); 
			}
			
		int maxNumberOfObservations = AstronConverter.toInteger(Url).intValue();
		int numberOfObservations = proposal.getObservingRequest()
				.getObservations().size();
		
		//can be null below!
		int numberOfNewObservations = 0;
		if (observations != null)
		{
			numberOfNewObservations = observations.size();
		}
		/*
		 * if number of new and existing observations are greater then max
		 * number of observation, add new observation until maximum is reached
		 */
		if (numberOfObservations + numberOfNewObservations > maxNumberOfObservations) {
			int index = 0;
			while (numberOfObservations < maxNumberOfObservations
					&& index < numberOfNewObservations) {
				Observation observation = (Observation) observations.get(index);
				proposal.getObservingRequest().getObservations().add(observation);
				index++;
				numberOfObservations++;
			}
		} else {
			proposal.getObservingRequest().getObservations().addAll(
					observations);
		}
	}

	/**
	 * The copyObservation method copies an observation
	 * 
	 * @param proposal
	 * @param observation
	 * @throws DatabaseException
	 */
	public void copyObservation(Proposal proposal, Observation observation)
			throws DatabaseException {
		Observation newObservation = (Observation) repository.copy(observation);
		addChangeObservation(proposal,newObservation,true);
	}
	
	public void copyPipeline(Proposal proposal, Observation observation)
			throws DatabaseException {
		Observation newObservation = (Observation) repository.copy(observation);
		addChangeObservation(proposal,newObservation,false);
	}
	
	/**
	 * The copyObservation method copies an observation
	 * 
	 * @param proposal
	 * @param observation
	 * @throws DatabaseException
	 */
	public void copyObservation(Proposal proposal, Observation observation, boolean includeTargets)
			throws DatabaseException {
		Observation newObservation = (Observation) repository.copy(observation);

		if(includeTargets){
			List<Target> newTargets = new ArrayList();
			Iterator tarit = newObservation.getTargets().iterator();
			while (tarit.hasNext()){
				Target oldTarget = (Target) tarit.next();
				Target newTarget = (Target) repository.copy(oldTarget);
				newTargets.add(newTarget);
				
			}
			newObservation.setTargets(newTargets);
		}
		addChangeObservation(proposal,newObservation,true);
	}
	
	public void copyPipeline(Proposal proposal, Observation observation, boolean includeTargets)
			throws DatabaseException {
		Observation newObservation = (Observation) repository.copy(observation);
		newObservation.setId(null);
		addChangeObservation(proposal,newObservation,false);
	}

	/**
	 * The copyObservation method copies an observation
	 * 
	 * @param observation
	 * @throws DatabaseException
	 */
	public Observation copyObservation(Observation observation)
			throws DatabaseException {
		Observation newObservation = (Observation) repository.copy(observation);
		return newObservation;
	}

    /**
     * The copyTarget method copies a target
     * 
     * @param observation
     * @param target
     * @throws DatabaseException
     */
    public void copyTarget(Observation observation, Target target)
            throws DatabaseException {
        Target newTarget = (Target) repository.copy(target);
       // observation.getTargets().add(newTarget);
    }

    /**
     * The copyTarget method copies a target
     * 
     * @param target
     * @throws DatabaseException
     */
    public Target copyTarget(Target target)
            throws DatabaseException {
        Target newTarget = (Target) repository.copy(target);
        return newTarget;
    }

	/**
	 * The submitAndStore method submit the proposal
	 * 
	 * @param proposal
	 * @param userAccount
	 * @throws DatabaseException
	 */
	public void submitAndStore(Proposal proposal, UserAccount userAccount)
			throws DatabaseException, AstronMailException, NamingException,
			NorthStarException {
		if (northStarDelegate.isDeprecated(proposal)) {
			throw new DeprecatedException();
		}
		this.setProposalCode(proposal);
		collaborationDelegate
				.deleteInvitationsOfProposal(proposal, userAccount);
		this.storeAsSubmitted(proposal, userAccount);
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		TelescopeEmailGenerator emailGenerator = (TelescopeEmailGenerator) configurationUtil
				.getEmailGenerator(telescopeConfiguration);
		StatusEmail statusEmail = emailGenerator.sendSubmittedEmail(
				telescopeConfiguration, proposal, userAccount);
		northStarDelegate.storeStatusEmail(statusEmail);
		collaborationDelegate.sendInvitationForViewing(proposal, userAccount);
		/*
		 * if is immediate such as urgent and service
		 */
		if (proposal.getSemester().isImmediate()) {
			UserAccount maintenanceAccount = northStarDelegate
					.getUserAccountByName(NorthStarConfiguration
							.getMaintenanceAccount());
			storeAsUnderReview(proposal, maintenanceAccount);
			TaskDaemon taskDaemon = TaskDaemon.getTaskDeamon();
			taskDaemon.addTask(proposal);
		}
	}

	/**
	 * The RetractAndStore method retracts the proposal
	 * 
	 * @param proposal
	 * @param userAccount
	 * @throws DatabaseException
	 */
	public void retractAndStore(Proposal proposal, UserAccount userAccount)
			throws DatabaseException, AstronMailException, NamingException,
			InvalidConfigurationException {
		Status retractedStatus = repository.getInPreparationStatus();
		northStarDelegate.logStatusChange(proposal, userAccount, proposal
				.getCurrentStatus().getStatus(), retractedStatus);
		proposal.setCurrentProposalStatus(retractedStatus, userAccount
				.getUser().getId());
		storeWithoutLocking(proposal, userAccount);
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		TelescopeEmailGenerator emailGenerator = (TelescopeEmailGenerator) configurationUtil
				.getEmailGenerator(telescopeConfiguration);
		StatusEmail statusEmail = emailGenerator.sendRetractedEmail(
				telescopeConfiguration, proposal, userAccount);
		northStarDelegate.storeStatusEmail(statusEmail);
	}

	/**
	 * Store the proposal as InPreparation
	 * 
	 * @param proposal
	 * @param userAccount
	 * @throws DatabaseException
	 */
	public void storeAsInPreparation(Proposal proposal, UserAccount userAccount)
			throws DatabaseException {
		Status status = repository.getInPreparationStatus();

		northStarDelegate.logStatusChange(proposal, userAccount, proposal
				.getCurrentStatus().getStatus(), status);
		proposal
				.setCurrentProposalStatus(status, userAccount.getUser().getId());
		storeWithoutLocking(proposal, userAccount);
	}

	/**
	 * Store the proposal as UnderReview
	 * 
	 * @param proposal
	 * @param userAccount
	 * @throws DatabaseException
	 * @author Nico Vermaas
	 * @since 22 feb 2005
	 */
	public void storeAsUnderReview(Proposal proposal,
			UserAccount maintenanceAccount) throws DatabaseException {

		Status status = repository.getUnderReviewStatus();
		northStarDelegate.logStatusChange(proposal, maintenanceAccount,
				proposal.getCurrentStatus().getStatus(), status);
		proposal.setCurrentProposalStatus(status, maintenanceAccount.getUser()
				.getId());
		storeWithoutLocking(proposal, maintenanceAccount);
	}

	/*
	 * public void storeAsUnderReview(Proposal proposal, Integer userId,
	 * UserAccount userAccount) throws DatabaseException { Status status =
	 * repository.getUnderReviewStatus();
	 * northStarDelegate.logStatusChange(proposal, userAccount, proposal
	 * .getCurrentProposalStatus().getStatus(), status);
	 * proposal.setCurrentProposalStatus(status, userId);
	 * storeWithoutLocking(proposal, userAccount); }
	 */

	/**
	 * The storeAsNewProposal method stores existing proposal as new
	 * 
	 * @param proposal
	 * @param userAccount
	 * @throws DatabaseException
	 */
	public void storeAsNewProposal(Proposal proposal, UserAccount userAccount)
			throws DatabaseException {
		Status status = repository.getInPreparationStatus();
		proposal.getStatusHistory().clear();
		proposal
				.setCurrentProposalStatus(status, userAccount.getUser().getId());
		storeWithoutLocking(proposal, userAccount);
	}

	public void storeAsSubmitted(Proposal proposal, UserAccount userAccount)
			throws DatabaseException {
		Status status = repository.getSubmittedStatus();
		northStarDelegate.logStatusChange(proposal, userAccount, proposal
				.getCurrentStatus().getStatus(), status);
		proposal
				.setCurrentProposalStatus(status, userAccount.getUser().getId());
		storeWithoutLocking(proposal, userAccount);
	}

	public void changeStatus(Integer newStatusId, Proposal proposal,
			String subject, String message, UserAccount userAccount)
			throws DatabaseException, AstronMailException, NamingException,
			InvalidConfigurationException {
		Status newStatus = repository.getStatus(newStatusId.intValue());
		northStarDelegate.logStatusChange(proposal, userAccount, proposal
				.getCurrentStatus().getStatus(), newStatus);
		proposal.setCurrentProposalStatus(newStatus, userAccount.getUser()
				.getId());
		storeWithoutLocking(proposal, userAccount);
		// TODO not so pretty constants
		if (newStatus.getCode().equals(eu.radionet.northstar.business.Constants.STATUS_UNDER_REVIEW)) {
			TaskDaemon taskDaemon = TaskDaemon.getTaskDeamon();
			taskDaemon.addTask(proposal);
		}
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		TelescopeEmailGenerator emailGenerator = (TelescopeEmailGenerator) configurationUtil
				.getEmailGenerator(telescopeConfiguration);
		StatusEmail statusEmail = emailGenerator
				.sendStatusChangeEmail(telescopeConfiguration, proposal,
						subject, message, userAccount);
		northStarDelegate.storeStatusEmail(statusEmail);

	}
	
	public void changeStatusWithoutMail(Integer newStatusId, Proposal proposal,
			UserAccount userAccount)
			throws DatabaseException, AstronMailException, NamingException,
			InvalidConfigurationException {
		Status newStatus = repository.getStatus(newStatusId.intValue());
		northStarDelegate.logStatusChange(proposal, userAccount, proposal
				.getCurrentStatus().getStatus(), newStatus);
		proposal.setCurrentProposalStatus(newStatus, userAccount.getUser()
				.getId());
		storeWithoutLocking(proposal, userAccount);
		// TODO not so pretty constants
		if (newStatus.getCode().equals(eu.radionet.northstar.business.Constants.STATUS_UNDER_REVIEW)) {
			TaskDaemon taskDaemon = TaskDaemon.getTaskDeamon();
			taskDaemon.addTask(proposal);
		}
	}

	public Role getPiRole() throws DatabaseException {
		return repository.getPiRole();
	}

	public Role getContactAuthorRole() throws DatabaseException {
		return repository.getContactAuthorRole();
	}

	public Role getPotentialObserverRole() throws DatabaseException {
		return repository.getPotentialObserverRole();
	}

	/**
	 * The getCategories method retrieves all proposal categories
	 * 
	 * @return List of proposal categories
	 * @throws DatabaseException
	 *             TODO retrieve categories for a specific telescope
	 */
	public List getCategories() throws DatabaseException {
		List categories = repository.getCategories();
		if (categories.size() > 0) {
			return categories;
		} else {
			return new ArrayList();
		}
	}

	/**
	 * The getJustificationFile method provides
	 * 
	 * @param justification
	 * @return
	 * @throws DatabaseException
	 */
	public TechnicalDetailsFile getTechnicalDetailsFile(
			Justification justification) throws DatabaseException {
		return repository.getTechnicalDetailsFile(justification.getId());
	}

	public FigureFile getFigureFile(Justification justification)
			throws DatabaseException {
		return repository.getFigureFile(justification.getId());
	}

	public ScientificFile getScientificFile(Justification justification)
			throws DatabaseException {
		return repository.getScientificFile(justification.getId());
	}

	public List getJustificationFiles(Justification justification)
			throws DatabaseException {
		return repository.getJustificationFiles(justification.getId());
	}

	public void addTargets(Proposal proposal, int observationIndex, List targets){
			//Observation observation =  (Observation) proposal.getObservingRequest().getObservations().get(observationIndex);
			//observation.getTargets().addAll(targets);
		ObservingRequest observingRequest = proposal.getObservingRequest();
		observingRequest.getTargets().addAll(targets);
	}

		
	/**
	 * The setProposalCode method provides
	 * 
	 * @param proposal
	 * @throws DatabaseException
	 */
	public void setProposalCode(Proposal proposal) throws DatabaseException,
			DeadLinePassedException {
		Semester semester = northStarDelegate.getSemester(proposal
				.getSemester().getId());
		/*
		 * no code is assigned or code starts with no valid prefix (e.g. prefix
		 * of last semester)
		 */
		if (proposal.getCode() == null
				|| !proposal.getCode().startsWith(semester.getPrefix())) {

			Calendar currentDate = NorthStarDelegate.getCurrentDate();
			if (!currentDate.getTime().before(semester.getDeadLine())) {
				throw new DeadLinePassedException();
			}

			semester.setLastAssigned(new Integer(semester.getLastAssigned()
					.intValue() + 1));
			repository.store(semester);
			proposal.setCode(getProposalCode(semester));
		}

	}

	protected Semester getNextSemester(Calendar date, List semesters) {
		Semester calculatedSemester = null;

		/*
		 * find current and next semester
		 */
		for (int i = 0; i < semesters.size(); i++) {
			Semester semester = (Semester) semesters.get(i);
			/*
			 * find next semester
			 */
			if (date.getTime().before(semester.getDeadLine())) {
				if (calculatedSemester == null
						|| calculatedSemester.getDeadLine().after(
								semester.getDeadLine())) {
					calculatedSemester = semester;
				}
			}
		}
		return calculatedSemester;
	}

	protected String getProposalCode(Semester semester) {
		String lastAssigned = semester.getLastAssigned().toString();
		while (lastAssigned.length() < 3) {
			lastAssigned = "0" + lastAssigned;
		}

		return semester.getPrefix() + lastAssigned;
	}

	public Category getDefaultCategory() throws DatabaseException {
		return repository.getDefaultCategory();
	}

	public Category getCategory(Integer id) throws DatabaseException {
		return repository.getCategory(id);
	}

	public List getAllStatuses() throws DatabaseException {
		return repository.retrieveStatuses();
	}

	public String getName(User user) {
		String title = "";

		if (user != null) {
			if (user.getTitle() != null) {
				title = user.getTitle();
			}

			return user.getLastName() + ", " + title + " "
					+ user.getFirstName();
		} else {
			return null;
		}
	}

	public String getEmailName(User user) {
		String name = null;
		if (AstronValidator.isBlankOrNull(user.getTitle())) {
			name = user.getFirstName();
		} else {
			name = user.getTitle();
		}
		name += " " + user.getLastName();
		return name;
	}

	public ProposalError validate(Proposal proposal) throws DatabaseException, InvalidConfigurationException {
		ProposalValidator validator = configurationUtil.getProposalValidator(proposal);
		return validator.validate(proposal);
	}

	public void buildXml(XMLBuilder xmlBuilder, Proposal proposal)
			throws DatabaseException, InvalidConfigurationException {
		ProposalXMLConverter converter = configurationUtil.getXMLWriter(proposal);
		converter.buildXml(xmlBuilder, proposal);
	}



	public void updateSemesters(Semester semester) throws DatabaseException {

	}

	public void checkJustificationFiles(Proposal proposal,
			Integer scientificFilePages, Integer technicalDetailsPages,
			Integer figureFilePages) throws BadJustificationFileException {
		boolean scientificFile = false;
		boolean technicalDetails = false;
		boolean figureFile = false;
		boolean overall = false;
		
		
		// if the config file has pages below zero and optionpagges has maxpages value, the use optionspages
	
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		/*
		int maxScientificPages=telescopeConfiguration.getMaxScientificPages();
		
		if( maxScientificPages <= 0){
			ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
			List urlList = new ArrayList();
		*/
		// ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		// FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		// OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVING_MODE,fieldsDefinitionType));	
		// int maxScientificFilePages = OptionsDelegate.getLabel(name, value, contextConfiguration);

		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		//FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		//OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVING_MODE,fieldsDefinitionType));	
		//int maxScientificFilePages = OptionsDelegate.getLabel(name, value, contextConfiguration);
		int maxScientificFilePages=0; //telescopeConfiguration.getMaxPublicationCount();
		List urlList = new ArrayList();
		String Url = null;
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_SCIENTIFIC_PAGECOUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			maxScientificFilePages = AstronConverter.toInteger(Url).intValue();
		}

		if (scientificFilePages != null && scientificFilePages.intValue() > maxScientificFilePages) {

			scientificFile = true;
		}
		
		
		int maxTechnicalFilePages=0; //telescopeConfiguration.getMaxPublicationCount();
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_TECHNICAL_PAGECOUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			maxTechnicalFilePages = AstronConverter.toInteger(Url).intValue();
		}
		
		if (technicalDetailsPages != null
				&& technicalDetailsPages.intValue() > maxTechnicalFilePages) {
			technicalDetails = true;
		}
		
		int maxFigureFilePages=0; //telescopeConfiguration.getMaxPublicationCount();
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_FIGURE_PAGECOUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			maxFigureFilePages = AstronConverter.toInteger(Url).intValue();
		}
		
		if (figureFilePages != null && figureFilePages.intValue() > maxFigureFilePages) {
			figureFile = true;
		}
		
		int maxTotalFilePages=0; //telescopeConfiguration.getMaxPublicationCount();
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_TOTAL_PAGECOUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			maxFigureFilePages = AstronConverter.toInteger(Url).intValue();
		}
		
		if(scientificFilePages.intValue() < 0 || technicalDetailsPages.intValue() < 0 || figureFilePages.intValue() < 0){
			throw new BadJustificationFileException("Wrong number of pages",
					scientificFile, technicalDetails, figureFile, overall);
		}
		
		if (scientificFilePages != null
				&& technicalDetailsPages != null
				&& scientificFilePages.intValue()
						+ technicalDetailsPages.intValue() > maxTotalFilePages) {
			overall = true;
		}
		if (scientificFile || technicalDetails || figureFile || overall) {
			throw new BadJustificationFileException("Wrong number of pages",
					scientificFile, technicalDetails, figureFile, overall);
		}
	}

	/**
	 * Convert a old proposal to a new one
	 * 
	 * @param oldProposal
	 * @param sessionId
	 * @param userAccount
	 * @return
	 * @throws DatabaseException
	 * @throws InvalidConfigurationException
	 */
	public Proposal convert(Proposal oldProposal, String sessionId,
			UserAccount userAccount) throws DatabaseException,
			InvalidConfigurationException {
		Proposal newProposal = copyProposal("", oldProposal, userAccount,
				oldProposal.getSemester());
		copyJustificationFilesAndStoreProposal(newProposal, oldProposal,
				userAccount);
		delete(oldProposal, sessionId, userAccount);
		return northStarDelegate.getOwnProposal(newProposal.getId(),
				userAccount);
	}
	
	public abstract Map getAllocations(ObservingRequest observingRequest);
	
	
	public abstract String getObservationInstrumentName(Observation observation);
	
	
}