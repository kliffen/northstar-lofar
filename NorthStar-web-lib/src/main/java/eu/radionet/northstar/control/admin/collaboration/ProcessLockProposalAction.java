// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessUploadAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: ProcessLockProposalAction.java,v 1.1 2006-05-03 10:17:19 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.admin.collaboration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The ProcessUploadAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessLockProposalAction extends Action {
	private Log log = LogFactory.getLog(this.getClass());

	private CollaborationDelegate collaborationDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();
		LockProposalForm lockProposalForm = (LockProposalForm) form;

		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
		Integer proposalId =AstronConverter
		.toInteger(lockProposalForm.getProposalId());
		collaborationDelegate = CollaborationDelegate.getInstance();
		if (isCancelled(request)) {
			if (log.isTraceEnabled()) {
				log.trace(LogMessage.getMessage(ownUserAccount,
						"Cancelled, forward to '" + Constants.CANCEL + "'"));
			}
			collaborationDelegate.deleteInvalidProposalLock(proposalId, ownUserAccount,
				session.getId());
			return mapping.findForward(Constants.CANCEL);
		}

		collaborationDelegate.lockProposal(proposalId, ownUserAccount,
				session.getId());
		if (collaborationDelegate.getAcceptedInvitation(proposalId, ownUserAccount)!= null){
			/*
			 * retrieve session object with information
			 */
			NorthStarDelegate northStarDelegate = NorthStarDelegate.getInstance();
			Proposal proposal = northStarDelegate.getProposal(proposalId);
			ProposalDelegate proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposal);
			proposalDelegate.store(proposal, ownUserAccount, session.getId());
		}
        ActionForward forward = new ActionForward();
        forward.setRedirect(true);
        forward.setPath(lockProposalForm.getForwardUri());
        return ParameterAppender.append(forward,"proposalId", proposalId.toString());

	}
}