package eu.radionet.northstar.business.pdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;

import eu.radionet.northstar.data.entities.Proposal;

public class AbstractsWriter extends DocumentWriter {
	private Log log = LogFactory.getLog(this.getClass());

	private static final float TITLE_HEIGHT=55;
	private static final float ABASTRACT_HEIGHT=290;
	private static final float SPACER_HEIGHT=10;

	class HeaderFooter extends PdfPageEventHelper {
		int pagenumber=0;
		public void onEndPage(PdfWriter writer, Document document) {
			pagenumber++;
			ColumnText.showTextAligned(writer.getDirectContent(),
                    Element.ALIGN_CENTER, new Phrase(String.format("page %d", pagenumber)),
                    560 , 50, 0);
			
		}
		
	}

	public void writeAbstract(List proposals, ByteArrayOutputStream outputStream,
			ServletContext servletContext) throws DocumentException, IOException {
		
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN); // A4=595,842
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				outputStream);
		if(proposals.size() >0){
			HeaderFooter event = new HeaderFooter();
			pdfWriter.setPageEvent(event);
			
			document.addTitle("Proposal abstracts");
			document.addCreator("NorthStar");
			document.open();
			Iterator proposalit = proposals.iterator();
			log.warn(" generating abstracts: "+proposals.size());
			while (proposalit.hasNext()){
				Proposal proposal = (Proposal) proposalit.next();
				addProposalAbstract(document,proposal);
				
			}
			document.close();
		}

	}


	private void addProposalAbstract(Document document, Proposal proposal) throws DocumentException {
		PdfPTable firstPageTable = getDefaultTable(1);
		
		// Title, author
		PdfPCell cell = getTableCell();
		cell.setFixedHeight(TITLE_HEIGHT);
		String code ="  ";
		if(proposal.getCode() != null){
			code = proposal.getCode()+": ";
		}
		String title = proposal.getJustification().getTitle();
		if(title != null && title.length() > 64){
			cell.addElement(getParagraphNormal(title,EMPTY));
		}else{
			cell.addElement(getParagraphDefaultTitle(title));
		}
		cell.addElement(getParagraphNormal(code+" "+proposal.getContactAuthor().getName(), EMPTY));
		firstPageTable.addCell(cell);
		// abstract
		cell = getTableCell();
		cell.addElement(getParagraphDefaultTitle("Abstract"));
		String abstractText = null;
		if (proposal.getJustification() != null) {
			abstractText = proposal.getJustification().getAbstractText();
			abstractText = abstractText.replace("\r\n", "");
		}
		cell.setFixedHeight(ABASTRACT_HEIGHT);
		cell.addElement(getParagraphNormal(abstractText, EMPTY));
		firstPageTable.addCell(cell);
		// split
		cell = getBorderlessCell();
		cell.setFixedHeight(SPACER_HEIGHT);
		firstPageTable.addCell(cell);
		document.add(firstPageTable);
	}
}
