ALTER TABLE lofar_proposal_astron_nl_northstar.opticoninstrument
Add column beam_mode TINYINT(1)  NOT NULL DEFAULT 0 ,
Add column interfero_mode TINYINT(1)  NOT NULL DEFAULT 0 ,
Add column tbb_mode TINYINT(1)  NOT NULL DEFAULT 0 ,
Add column other_mode TINYINT(1)  NOT NULL DEFAULT 0 ;