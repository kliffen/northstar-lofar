// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Mar 4, 2005
 *
 *
 */
package eu.radionet.northstar.business.exception;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Anton Smit
 */
public class ParseTargetListException extends NorthStarException
{

    protected int lineNumber = 0;
	protected List wrongHeaders = null;
	protected String missingHeader = null;
	protected int runNumber = 0;
	protected String  notavailableobservation ="";
	/**
     * 
     */
    public ParseTargetListException()
    {
        super();
        // TODO Auto-generated constructor stub
    }
    /**
     * @param arg0
     */
    public ParseTargetListException(String arg0)
    {
        super(arg0);
        // TODO Auto-generated constructor stub
    }

    /**
     * @param arg0
     * @param arg1
     */
    public ParseTargetListException(String arg0, Throwable arg1)
    {
        super(arg0, arg1);
        // TODO Auto-generated constructor stub
    }
    /**
     * @param arg0
     */
    public ParseTargetListException(Throwable arg0)
    {
        super(arg0);
        // TODO Auto-generated constructor stub
    }
    

    public ParseTargetListException(String arg0, int lineNumber)
    {
        super(arg0);
        this.lineNumber = lineNumber;    
    }
	
    public ParseTargetListException(String arg0, List wrongHeaders, int lineNumber)
    {
        super(arg0);
        this.lineNumber = lineNumber; 
		this.wrongHeaders = wrongHeaders;
		
    }
    public ParseTargetListException(String arg0, String missingHeader, int lineNumber)
    {
        super(arg0);
        this.lineNumber = lineNumber; 
		this.missingHeader = missingHeader;
		
    }
    
    public ParseTargetListException(String arg0, int runNumber, int lineNumber)
    {
		super(arg0);
		this.lineNumber = lineNumber;
		this.runNumber = runNumber;
	}
    
    public ParseTargetListException(String arg0,	ArrayList notavailableobservationList) {
    	super(arg0);
    	
    	for(int i=0;i<notavailableobservationList.size();i++)
    		notavailableobservation+=notavailableobservationList.get(i)+",";
	}
	public String getLineNumber()
    {
        return String.valueOf(lineNumber);
    }
	public boolean isRequiredHeader(){
		return missingHeader != null;
	}
	public boolean isWrongHeaders(){
		return wrongHeaders != null;
	}
    public List getWrongHeaders(){
		return wrongHeaders;
    }
	public String getRequiredHeader(){
		return missingHeader;
	}

	public int getRunNumber() {
		return runNumber;
	}

	public void setRunNumber(int runNumber) {
		this.runNumber = runNumber;
	}
	public String getNotavailableobservation() {
		return notavailableobservation;
	}
	public void setNotavailableobservation(String notavailableobservation) {
		this.notavailableobservation = notavailableobservation;
	}

	
	
}
