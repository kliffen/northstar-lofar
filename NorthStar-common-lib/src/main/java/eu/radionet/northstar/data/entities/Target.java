// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.data.entities;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Target implements Serializable
{
    /**
	 * 
	 */
	private static final long serialVersionUID = 368568364L;

	protected Integer id = null;

    protected String fieldName; // name of field in sky
   
    protected Map allocations = new HashMap();

    protected Double ra = null; // hh:mm:ss.ss

    protected Double decl = null; // +dd:mm:ss.s

    protected String epoch = null;
    
	protected List selectedRuns;

	public Map getAllocations()
	{
		return allocations;
	}

	public void setAllocations(Map allocations)
	{
		this.allocations = allocations;
	}

	public Double getDecl()
	{
		return decl;
	}

	public void setDecl(Double decl)
	{
		this.decl = decl;
	}

	public String getEpoch()
	{
		return epoch;
	}

	public void setEpoch(String epoch)
	{
		this.epoch = epoch;
	}

	public String getFieldName()
	{
		return fieldName;
	}

	public void setFieldName(String fieldName)
	{
		this.fieldName = fieldName;
	}

	public Integer getId()
	{
		return id;
	}

	public void setId(Integer id)
	{
		this.id = id;
	}

	public Double getRa()
	{
		return ra;
	}

	public void setRa(Double ra)
	{
		this.ra = ra;
	}

	public List getSelectedRuns() {
		return selectedRuns;
	}

	public void setSelectedRuns(List selectedRuns) {
		this.selectedRuns = selectedRuns;
	}

    
	
}
