// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.util;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import nl.astron.util.tar.TarArchive;
import nl.astron.util.tar.TarEntry;
import nl.astron.util.tar.TarInputStream;
import nl.astron.util.tar.TarOutputStream;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.LabelValueBean;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.business.configuration.ConfigurationUtil;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.pdf.AbstractsWriter;
import eu.radionet.northstar.business.pdf.DocumentWriter;
import eu.radionet.northstar.business.pdf.ProposalWriter;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.FigureFile;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ScientificFile;
import eu.radionet.northstar.data.entities.TechnicalDetailsFile;

public class PdfUtils {
	private Log log = LogFactory.getLog(this.getClass());
	private String convertErrors="";
	
	private ProposalDelegate proposalDelegate = null;

	private static HashMap headers = new HashMap();
	
	private static final String PDF_TYPE = ".pdf";
	private static final String POSTSCRIPT_TYPE = ".ps";
	private static final String TEXT_TYPE = ".txt";
	private static final String ARCHIVE = ".tar";
	private static final String LATEX_TYPE = ".tex";	
	
	public int getNumberLatexPages(byte[] latexFile, byte[] figureFile,
			Proposal proposal, ServletContext context) throws IOException{
		String latexClassFileName = getLatexClassFile(proposal, context);
		
		byte[] fout = latex2pdf(latexFile,figureFile,latexClassFileName);
		/* temporary test the result (bv. images included?)
		File outFile = File.createTempFile("toets_", ".pdf");
		FileOutputStream latexFileStream = new FileOutputStream(
				outFile);
		latexFileStream.write(fout);
		latexFileStream.flush();
		latexFileStream.close();
		*/
		if (fout != null){
		PdfReader reader = new PdfReader(fout);
			int totalsPages = reader.getNumberOfPages();
			return totalsPages;
		}else
		{
			// error force page length overflow
			return 1000; // just a big number, should be negative, in case someone decides to set page number to unlimited.
		}
	}

	public int getNumberOfPages(byte[] justificationFile,
			String fileName, HttpSession session) throws IOException, DocumentException {
		String contentType=fileName.substring(fileName.lastIndexOf('.'));
		
		if (!AstronValidator.isBlankOrNull(contentType)
				&& justificationFile != null) 
		{
			if (contentType.equalsIgnoreCase(PDF_TYPE)) {
				int size = DocumentWriter.getPdfSize(justificationFile).intValue();
				if(size < 0){
					log.warn("not a valid pdf file, could not get size");
					StringBuffer errorPage = new StringBuffer();
		        	errorPage.append("<html><head><link rel=stylesheet href=\"/proposal/css/proposalLayout.css\" type=\"text/css\"></head><body>");
		        	errorPage.append("<table align=\"center\" valign=\"middle\"><tr><td class=bTl><img src=\"/proposal/images/b.gif\" height=\"10\" width=\"10\"></td><td class=bT_noFullWidth></td><td class=bTr></td></tr>");
		        	errorPage.append("<tr><td class=bL></td><td class=bM_noFullWidth valign=\"top\"> &nbsp; <a href=\"#\" onclick=\"history.go(-1);return false; \">[ Back ]</a>");
        			errorPage.append("<br><br><b> There has been an error in the pdf conversion, the pdf might be a wrong version or broken, please try again </b><br>");
        			errorPage.append("</td><td class=bRR></td></tr><tr><td class=bBl></td><td class=bB_noFullWidth></td><td class=bBr><img src=\"/proposal/images/b.gif\" height=\"10\" width=\"10\"></td></tr></table>");
        			errorPage.append("</body></html>");
					convertErrors=errorPage.toString();
				}
				return size; 
			} //else 
				
			if (contentType.equalsIgnoreCase(POSTSCRIPT_TYPE)) {
				byte[] pdf = ps2pdf(justificationFile);
				return DocumentWriter.getPdfSize(pdf).intValue();
			} //else 
				
			if (contentType.equalsIgnoreCase(TEXT_TYPE)) {
				byte[] pdf = text2pdf(justificationFile);
				return DocumentWriter.getPdfSize(pdf).intValue();
			} //else 
			
			ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
			if (contentType.equalsIgnoreCase(LATEX_TYPE)){
				//Session session = 
		    	// get configuration value for second justification from options file
		    	Proposal proposal = proposalData.getProposal();
				String classFile=getLatexClassFile(proposal,session.getServletContext());
				FigureFile figureFile=proposalData.getFigureFile();
				if (figureFile==null){
					try {
						proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposal);
						figureFile = proposalDelegate.getFigureFile(proposal.getJustification());
					} catch (DatabaseException e) {
						e.printStackTrace();
					} catch (InvalidConfigurationException e) {
						e.printStackTrace();
					}
				}
				byte[] pdf = null;
				if(figureFile != null){
					pdf = latex2pdf(justificationFile,figureFile.getUploadFile(),classFile);
				}else{
					pdf = latex2pdf(justificationFile,null,classFile);
				}
				if (pdf==null){
					// if latex conversion went wrong, give -1 as result
					return -1;
				}
				
				return DocumentWriter.getPdfSize(pdf).intValue();
			}
			
			// if it is a figure file
			if (contentType.equalsIgnoreCase(".ps")
					|| contentType.equalsIgnoreCase(".eps")
					|| contentType.equalsIgnoreCase(".pdf")
					|| contentType.equalsIgnoreCase(".tar")) {
				// if there already is a scientific or tech justification
				Proposal proposal = proposalData.getProposal();
				
				ScientificFile scientificFile =proposalData.getScientificFile();
				TechnicalDetailsFile technicalDetailsFile =  proposalData.getTechnicalDetailsFile();
				
				try {
					proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposal);
					if(scientificFile == null){
						scientificFile = proposalDelegate.getScientificFile(proposal.getJustification());
					}
					if(technicalDetailsFile == null){
						technicalDetailsFile = proposalDelegate.getTechnicalDetailsFile(proposal.getJustification());
					}
					
				} catch (InvalidConfigurationException e) {
					e.printStackTrace();
				} catch (DatabaseException e) {
					e.printStackTrace();
				}
				
				if(scientificFile != null || technicalDetailsFile != null){
					
					if (!contentType.equalsIgnoreCase(".tar")){
						
						ByteArrayOutputStream bout = new ByteArrayOutputStream();
						TarOutputStream tout = new TarOutputStream(bout);
						
						// create tar of single image file.
						TarEntry tarEntry = new TarEntry(fileName);
						tarEntry.setSize(justificationFile.length);
						tout.putNextEntry(tarEntry);
						tout.write(justificationFile);
						tout.closeEntry();
						
						tout.flush();
						tout.close();
						justificationFile=bout.toByteArray();
						
					}
					
					String classFile=getLatexClassFile(proposal,session.getServletContext());
					
					byte[] pdf=null;
					if(scientificFile  != null){
						pdf = latex2pdf(scientificFile.getUploadFile() ,justificationFile,classFile);
						if(pdf==null){
							return -1;
						}
					}
					if(technicalDetailsFile != null){
						pdf = latex2pdf(technicalDetailsFile.getUploadFile(),justificationFile,classFile);
						if(pdf==null){
							return -1;
						}
					}
				}
				// found good figure file
				return 1;
				
			}
			// file was found, but of wrong type...
			log.warn("unknown file type: "+contentType);
			return -1;
		}
		return 0;
	}	
	
	
	public byte[] getPreviewPdf(ProposalData proposalData, HttpServletRequest request) throws InvalidConfigurationException, DatabaseException
	 {
		Proposal proposal = proposalData.getProposal();
		long startTime = System.currentTimeMillis();
		HttpSession session = request.getSession();
		if (proposalDelegate == null){
			proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposal);
		}
		
		ScientificFile scientificFile =proposalData.getScientificFile();
		if(scientificFile == null){
			scientificFile = proposalDelegate.getScientificFile(proposal.getJustification());
		}

		FigureFile figureFile=proposalData.getFigureFile();
		if (figureFile==null){
			figureFile = proposalDelegate.getFigureFile(proposal.getJustification());
			if(figureFile==null){
				figureFile=new FigureFile();
			}
		}
		
		String classFile=getLatexClassFile(proposal,session.getServletContext());
		
		String fileName = proposalData.getProposal().getJustification().getScientificFileName();
		
		if(scientificFile != null && fileName != null){
			if(fileName.endsWith(".pdf") || fileName.endsWith(".PDF")){
				return scientificFile.getUploadFile();
			}
			
			return latex2pdf(scientificFile.getUploadFile() ,figureFile.getUploadFile(),classFile);
		}else{
			StringBuffer errorPage = new StringBuffer();
        	
        	errorPage.append("<html><head><link rel=stylesheet href=\"/proposal/css/proposalLayout.css\" type=\"text/css\"></head><body>");
        	errorPage.append("<table align=\"center\" valign=\"middle\"><tr><td class=bTl><img src=\"/proposal/images/b.gif\" height=\"10\" width=\"10\"></td><td class=bT_noFullWidth></td><td class=bTr></td></tr>");
        	errorPage.append("<tr><td class=bL></td><td class=bM_noFullWidth valign=\"top\"><br> &nbsp; <a href=\"#\" onclick=\"history.go(-1);return false; \">[ Back ]</a><br><Br>");
        	errorPage.append("Please upload a latex file, browse for a .tex file and press the 'attach file' button <br><br>");
        	errorPage.append("</td><td class=bRR></td></tr><tr><td class=bBl></td><td class=bB_noFullWidth></td><td class=bBr><img src=\"/proposal/images/b.gif\" height=\"10\" width=\"10\"></td></tr></table>");
			errorPage.append("</body></html>");

        	
			convertErrors= errorPage.toString();
			return null;
		}
	}

	
	/**
	 * The generateProposalPdf method generates proposal from database
	 * 
	 * @param proposal
	 * @param request
	 * @return
	 * @throws DatabaseException
	 * @throws IOException
	 * @throws BadElementException
	 * @throws DocumentException
	 * @throws InvalidConfigurationException
	 */
	public byte[] getProposalPdf(Proposal proposal, HttpServletRequest request)
			throws DatabaseException, IOException, BadElementException,
			DocumentException, InvalidConfigurationException {

		long startTime = System.currentTimeMillis();
		HttpSession session = request.getSession();
		/*
		 * create document writer
		 */
		ProposalWriter documentWriter = null; // new ProposalWriter();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		documentWriter = configurationUtil
				.getProposalPdfWriter(telescopeConfiguration);
		documentWriter.setHeaderImage(getHeader(telescopeConfiguration
				.getPdfHeaderImageUri(), session.getServletContext()));

		/*
		 * if justification is not null
		 */
		if (proposal.getJustification() != null) 
		{
			addFilesToProposalWriter(documentWriter, proposal, session.getServletContext() );
		}
		/*
		 * write pdf
		 */
		documentWriter.write(proposal, baos, session.getServletContext());
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			UserAccount ownUserAccount = (UserAccount) session
					.getAttribute(Constants.OWN_USERACCOUNT);
			log.info(LogMessage
					.getMessage(ownUserAccount, "PDF generation duration: "
							+ (endTime - startTime) + " ms"));
		}
		
		return baos.toByteArray();
	}

	public byte[] getAbstractsPdf(List proposals, HttpServletRequest request)
			throws DatabaseException, IOException, BadElementException,
			DocumentException, InvalidConfigurationException {
		AbstractsWriter documentWriter = new AbstractsWriter();
		HttpSession session = request.getSession();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();

		documentWriter.writeAbstract(proposals, baos, session.getServletContext());
		
		return baos.toByteArray();
	}
	
	public void addFilesToProposalWriter(ProposalWriter proposalWriter,
			Proposal proposal, ServletContext servletContext) throws DatabaseException, DocumentException,
			InvalidConfigurationException
	{
		proposalDelegate = ProposalDelegateFactory.getProposalDelegate(proposal);
		FigureFile figureFile = proposalDelegate.getFigureFile(proposal.getJustification());
		ScientificFile scientificFile = 
			proposalDelegate.getScientificFile(proposal.getJustification());
		TechnicalDetailsFile technicalDetailsFile = 
			proposalDelegate.getTechnicalDetailsFile(proposal.getJustification());
		if (figureFile != null)
		{
			String contentType = proposal.getJustification().getFigureFileName();
			// FIXME contentType should not be null.
			if(contentType==null){
				contentType=ARCHIVE;
			}else{
				contentType = contentType.substring(contentType.lastIndexOf('.'));
			}
			
			if (contentType.equals(POSTSCRIPT_TYPE))
			{
				figureFile.setUploadFile(ps2pdf(figureFile.getUploadFile()));
			}
			else if (contentType.equals(TEXT_TYPE))
			{
				figureFile.setUploadFile(text2pdf(figureFile.getUploadFile()));
			}
			else if (contentType.equals(ARCHIVE))
			{
				if (scientificFile != null && proposal.getJustification().getScientificFileName() != null){
					// convert the uploaded files to the final pdf
					String scientificType = proposal.getJustification().getScientificFileName();
					scientificType = scientificType.substring(scientificType.lastIndexOf('.'));
					if (scientificType.equals(LATEX_TYPE)){
						String latexClassFile = getLatexClassFile(proposal, servletContext);
						byte[] finalLatexFile = latex2pdf(scientificFile.getUploadFile(),figureFile.getUploadFile(),latexClassFile);
						// generation failed so disable it?
						if (finalLatexFile == null || finalLatexFile.length < 20){
							scientificFile=null;
						}else{
							scientificFile.setUploadFile(finalLatexFile);
						}
					}
				}
				if (technicalDetailsFile != null && proposal.getJustification().getTechnicalDetailsFileName() != null){
					// convert the uploaded files to the final pdf
					String techType = proposal.getJustification().getTechnicalDetailsFileName();
					techType = techType.substring(techType.lastIndexOf('.'));
					if (techType.equals(LATEX_TYPE)){
						String latexClassFile = getLatexClassFile(proposal, servletContext);
						byte[] finalLatexFile = latex2pdf(technicalDetailsFile.getUploadFile(),figureFile.getUploadFile(),latexClassFile);
						// this should not be possible
						if (finalLatexFile.length < 20){
							technicalDetailsFile=null;
						}else{
							technicalDetailsFile.setUploadFile(finalLatexFile);
						}
					}
				}
			}
			else
			{
				figureFile.setUploadFile(figureFile.getUploadFile());
			}
		}
		if (scientificFile != null)
		{
			String contentType = proposal.getJustification().getScientificFileName();
			// contenttype can be null if scientific file has been deleted...
			if(contentType != null){
				contentType = contentType.substring(contentType.lastIndexOf('.'));
				if (contentType.equals(POSTSCRIPT_TYPE))
				{
					scientificFile.setUploadFile(ps2pdf(scientificFile.getUploadFile()));
				}
				else if (contentType.equals(TEXT_TYPE))
				{
					scientificFile.setUploadFile(text2pdf(scientificFile.getUploadFile()));
				}
				else if (contentType.equals(LATEX_TYPE))
				{
					// if figurefile exists, the the page has already been generated
					if (figureFile == null){
					/*String figureType = 
						getContentType(proposal.getJustification().getFigureFileName());
					if (!figureType.equals(ARCHIVE)){ */
						String latexClassFile = getLatexClassFile(proposal, servletContext);
						scientificFile.setUploadFile(latex2pdf(scientificFile.getUploadFile(),null,latexClassFile));
					}
	
				}
			}
		}
		if (technicalDetailsFile != null)
		{
			String contentType = proposal.getJustification().getTechnicalDetailsFileName();
			contentType = contentType.substring(contentType.lastIndexOf('.'));
			if (contentType.equals(POSTSCRIPT_TYPE))
			{
				technicalDetailsFile.setUploadFile(ps2pdf(technicalDetailsFile.getUploadFile()));
			}
			else if (contentType.equals(TEXT_TYPE))
			{
				technicalDetailsFile.setUploadFile(text2pdf(technicalDetailsFile.getUploadFile()));
			}
			else if (contentType.equals(LATEX_TYPE))
			{
				// if figurefile exists, the the page has already been generated... no its not, its the scientific...
				if (figureFile == null){
				/*String figureType = 
					getContentType(proposal.getJustification().getFigureFileName());
				if (!figureType.equals(ARCHIVE)){ */
					String latexClassFile = getLatexClassFile(proposal, servletContext);
					byte[] newPdfFile = latex2pdf(technicalDetailsFile.getUploadFile(),null,latexClassFile);
					technicalDetailsFile.setUploadFile(newPdfFile);
				}
			}
		}
		proposalWriter.setFigureFile(figureFile);
		proposalWriter.setScientificFile(scientificFile);
		proposalWriter.setTechnicalDetailsFile(technicalDetailsFile);

	}
	/**
	 * The getHeader methods retrieves header picture from war file
	 * 
	 * @param headerName
	 * @param servletcontext
	 * @return
	 */
	public byte[] getHeader(String headerName, ServletContext servletcontext) {
		byte[] data = (byte[]) headers.get(headerName);

		InputStream headerInputStream = servletcontext
				.getResourceAsStream(headerName);

		if (headerInputStream == null) {
			return null;
		}
		try {
			data = new byte[headerInputStream.available()];
			headerInputStream.read(data);
			headers.put(headerName, data);
		} catch (IOException e) {
			// ugly exception handling
			e.printStackTrace();
		} finally {
			if (headerInputStream != null) {
				try {
					headerInputStream.close();
				} catch (IOException e) {
					// ugly exception handling
					e.printStackTrace();
				}
			}
		}
		// }
		return data;
	}

	
	private byte[] latex2pdf(byte[] latexByteArray,byte[] archiveByteArray,String classFile) {
		try {
			/*
			 * create tex file
			 */
			String tempDir = System.getProperty("java.io.tmpdir")+File.separator+System.currentTimeMillis();
			File workingDir = new File(tempDir);
			workingDir.mkdir();
			
			File latexFile = File.createTempFile("temp_", ".tex",workingDir);
			/*
			 * create outputstream
			 */
			FileOutputStream latexFileStream = new FileOutputStream(
					latexFile);
			/*
			 * write byte array to file
			 */
			latexFileStream.write(latexByteArray);
			latexFileStream.flush();
			latexFileStream.close();
			
			/*
             *  copy & extract tar file,
             *  uses untar from gjt.org and is implemented in astronUtils
             */
			if(archiveByteArray != null && archiveByteArray.length > 0){
				InputStream inp = new ByteArrayInputStream(archiveByteArray);
				TarInputStream tis = new TarInputStream(inp);
				TarEntry ter;
				
				while( (ter=tis.getNextEntry()) != null){
					if(ter.isDirectory()){
						// pfff directories..
						 new File(ter.getName()).mkdir();
					}
					else{
						// get filename from entry
						File destPath = new File(workingDir.getPath() + File.separatorChar + ter.getName());
						OutputStream out = new BufferedOutputStream(new FileOutputStream(destPath));
						// get contents out of inputstream
						tis.copyEntryContents(out);
					    out.close();
					}
				}
				/* 
				File archiveFile = File.createTempFile("temp_", ".tar",workingDir);
				  old (inefficient) method
				latexFileStream = new FileOutputStream(
						archiveFile);
				latexFileStream.write(archiveByteArray);
				latexFileStream.flush();
				latexFileStream.close();
				String cmd="tar -x --file="+archiveFile.getName();
				//runProcess(cmd,workingDir);
				*/
			}
			//process.waitFor();
			
			/*
			 * copy latex class file
			 */
			if (classFile != null){
				File latexClassFile = new File(classFile);
				if(!latexClassFile.exists() ){
					log.warn("latex class file not found at: "+classFile);
				}
				String latexClassName = latexClassFile.getName();
				File latexClassCopyFile = new File(tempDir+File.separator+latexClassName);
				// somebody tried to include its own class file in the figures tar file
				if(latexClassCopyFile.exists()){
					log.warn("latex class file already exists at: "+classFile+" deleting...");
					latexClassCopyFile.delete();
				}
				latexClassCopyFile.createNewFile();
				FileInputStream fis = new FileInputStream(latexClassFile);
				latexFileStream = new FileOutputStream(
						latexClassCopyFile);
				try{
					byte [] buf = new byte[1024];
					int i = 0;
					while ((i=fis.read(buf)) != -1){
						latexFileStream.write(buf,0,i);
					}
				}catch(IOException err){
					log.error("Exception message : " + err.getMessage());
					log.error(AstronConverter.stackTraceToString(err));
					return null;
				}
				
				fis.close();
				latexFileStream.close();
				
				/* java 1.6 function
				FileChannel inChannel = new FileInputStream(latexClassFile).getChannel();
		        FileChannel outChannel = new FileOutputStream(latexClassCopyFile).getChannel();
		        inChannel.transferTo(0, inChannel.size(), outChannel);
	            if (inChannel != null) inChannel.close();
	            if (outChannel != null) outChannel.close();
	            */
			}
			/*
			 * retrieve path and filename
			 */
			String latexFileName = latexFile.getAbsolutePath();
			
			String latexOutput = "";
			String osName = System.getProperty("os.name");
			String cmd = "";
			
			/*
			 * make commands
			 */
			// use filename without .tex extension 
			latexOutput = latexFileName.substring(0,latexFileName.length()-4);
			if (osName.toLowerCase().startsWith("windows")) {
				cmd += "pdflatex "+ latexOutput +" -quiet ";
				
			} else {
				// latex --interaction=nonstopmode; dvipdf
				cmd += "latex2pdf "+latexOutput;
				//latexOutput = latexFileName.substring(0,latexFileName.length()-4);
			}
			/*
			 * execute command
			 */
			log.info("creating latex: "+cmd);
			runProcess(cmd,workingDir);
			/*
			 * make new file and file inputstream
			 */
			// just check for errors once more... (old version of pdftext generates invalid pdf files on error
			// read log file
			File texLogFile = new File(latexOutput+".log");
			String erl="";
			String errorLine="";
			if (texLogFile.exists()){
				BufferedReader in = new BufferedReader(new FileReader(latexOutput+".log"));
		        StringBuffer strb= new StringBuffer();
				String str;
				int linecount =0;
		        while ((str = in.readLine()) != null) {
		        	if(str.indexOf("Error:")>0){
		        		errorLine+=str+"<br>";
		        	}else if(linecount == 3 || linecount ==4 ){
		        		str="";
		        	}
		            strb.append(str+"\n");
		            linecount++;
		        }
		        in.close();
		        erl=strb.toString();
			}
			// 
			// check log file for error
			if(errorLine.length()==0){
				File pdfFile = new File(latexOutput+".pdf");
				if (pdfFile.exists()){
					
					FileInputStream fileInputStream = new FileInputStream(pdfFile);
					/*
					 * convert it to a byte array (hopefully there is enough heap memory)
					 */
					int byteChar = fileInputStream.read();
					ByteArrayOutputStream out = new ByteArrayOutputStream();
					while (byteChar != -1) {
						out.write(byteChar);
						byteChar = fileInputStream.read();
					}
					fileInputStream.close();
					// delete generated files
					deleteDirectory(workingDir);
					return out.toByteArray();
				}
				log.error(" latex generation error: Pdf file not found, cmd was: \n"+cmd);
			}
			else{
				log.warn(" latex generation error, files are in: \n"+workingDir.getPath());
				// read log file from pdflatex
		        	// generate a nice error page
		        	StringBuffer errorPage = new StringBuffer();
		        	
		        	errorPage.append("<html><head><link rel=stylesheet href=\"/proposal/css/proposalLayout.css\" type=\"text/css\"></head><body>");
		        	errorPage.append("<table align=\"center\" valign=\"middle\"><tr><td class=bTl><img src=\"/proposal/images/b.gif\" height=\"10\" width=\"10\"></td><td class=bT_noFullWidth></td><td class=bTr></td></tr>");
		        	errorPage.append("<tr><td class=bL></td><td class=bM_noFullWidth valign=\"top\"> &nbsp; <a href=\"#\" onclick=\"history.go(-1);return false; \">[ Back ]</a>");
        			errorPage.append("<br><br><b> There has been an error in the latex conversion, the error is: </b><br>");
        			errorPage.append(errorLine);
        			errorPage.append("<br><b>below is the latex output </b><br><br> <pre>");
        			errorPage.append(erl);
        			errorPage.append("</pre><br><a href=\"#\" onclick=\"history.go(-1);return false; \">[ Back ]</a></td><td class=bRR></td></tr><tr><td class=bBl></td><td class=bB_noFullWidth></td><td class=bBr><img src=\"/proposal/images/b.gif\" height=\"10\" width=\"10\"></td></tr></table>");
        			errorPage.append("</body></html>");
			       
			        convertErrors=errorPage.toString();
			        // clean up error file, error itself is in tomcat log.
				}
		        // maybe keep uploaded file, for forensic purposes
		        // texLogFile = new File(pdfPath);
		        // texLogFile.delete();
			return null;
		} catch (IOException err) {
			log.error("Exception message : " + err.getMessage());
			log.error(AstronConverter.stackTraceToString(err));
			return null;
		}

	}
	
	
	/**
	 * The ps2pdf converts postscript to pdf with GNU Ghostscript
	 * 
	 * @param postscriptByteArray
	 * @param sessionId
	 * @return
	 * @throws IOException
	 * @throws InterruptedException
	 */
	private byte[] ps2pdf(byte[] postscriptByteArray) {
		try {
			/*
			 * get runtime
			 */
			Runtime rt = Runtime.getRuntime();
			/*
			 * create temp file
			 */
			File postscriptFile = File.createTempFile(System.currentTimeMillis()+"_", null);
			/*
			 * create outputstream
			 */
			FileOutputStream postscriptFileStream = new FileOutputStream(
					postscriptFile);
			/*
			 * write byte array to file
			 */
			postscriptFileStream.write(postscriptByteArray);
			postscriptFileStream.flush();
			postscriptFileStream.close();
			/*
			 * retrieve path
			 */
			String postscriptPath = postscriptFile.getAbsolutePath();
			String pdfPath = postscriptPath + ".result";
			String osName = System.getProperty("os.name");
			String cmd = "";
			/*
			 * make command
			 */
			if (osName.toLowerCase().startsWith("windows")) {
				cmd += "ps2pdf.bat ";
			} else {
				cmd += "ps2pdf ";
			}
			/*
			 * make pdf file name
			 */
			cmd += postscriptPath + " " + pdfPath;
			// log.debug("Executing : " + cmd);
			/*
			 * execute command
			 */
			Process process = rt.exec(cmd);
			/*
			 * wait for the end of the process
			 */
			process.waitFor();
			/*
			 * make new file and file inputstream
			 */
			File pdfFile = new File(pdfPath);

			FileInputStream fileInputStream = new FileInputStream(pdfFile);
			/*
			 * convert it to and byte array
			 */
			int byteChar = fileInputStream.read();
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			while (byteChar != -1) {
				out.write(byteChar);
				byteChar = fileInputStream.read();
			}
			fileInputStream.close();
			postscriptFile.delete();
			pdfFile.delete();
			return out.toByteArray();
		} catch (Exception err) {
			log.error("Exception message : " + err.getMessage());
			log.error(AstronConverter.stackTraceToString(err));
			return null;
		}
	}
	
	
	protected byte[] text2pdf(byte[] text) throws DocumentException {
		/*
		 * create new document
		 */
		String justification = new String(text);
		Document document = new Document(PageSize.A4, DocumentWriter.LEFT_MARGIN,
				DocumentWriter.RIGHT_MARGIN, DocumentWriter.TOP_MARGIN, DocumentWriter.BOTTOM_MARGIN);
		document.setMarginMirroring(true);
		/*
		 * output it to and byte array output stream
		 */
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		/*
		 * create pdf writer
		 */
		PdfWriter.getInstance(document,
				byteArrayOutputStream);

		document.open();
		Paragraph result = new Paragraph(justification, DocumentWriter.TEXT_JUSTIFICATION_FONT);

		result.setIndentationLeft(DocumentWriter.NORMAL_PARAGRAPH_INDENTATION_LEFT);
		result.setIndentationRight(DocumentWriter.NORMAL_PARAGRAPH_INDENTATION_RIGHT);
		result.setAlignment(Paragraph.ALIGN_LEFT);
		document.add(result);

		document.close();
		return byteArrayOutputStream.toByteArray();
	}

	public String getConvertErrors()
	{
		return convertErrors;
	}
	
	private boolean deleteDirectory(File path) {
	    if( path.exists() ) {
	    	// make sure the path is in the temporary space, we don't want to accidentally delete the repository do we?
	    	if (path.getPath().startsWith(System.getProperty("java.io.tmpdir")))
	    	{
		      File[] files = path.listFiles();
		      for(int i=0; i<files.length; i++) {
		         if(files[i].isDirectory()) {
		           deleteDirectory(files[i]);
		         }
		         else {
		           files[i].delete();
		         }
		      }
		      return( path.delete() );
		    }
	  }
	    return false;
	}
	
	/**
	 *  This function will eventually kill the process if it takes to long
	 * @param cmd, workingDir
	 * @throws IOException
	 */
	private void runProcess(String cmd, File workingDir) throws IOException {
		Runtime rt = Runtime.getRuntime();
		Process process = rt.exec(cmd,null,workingDir);
		/*
		 * wait for the end of the process, first it will be 1 second, 
		 * it will add 2 seconds after each try, after 4 minutes, it kills the process the hard way.
		 */
		int wait=1;
		int exitVal=-1;
		for (int j=0; j<15;j++){
			try {
					// is it done yet?
					exitVal = process.exitValue();
			}catch (IllegalThreadStateException e) {
				try {
					// let the main thread sleep for a sec
					Thread.currentThread().sleep(wait*1000);
				} catch (InterruptedException e1) {
					log.debug("thread.sleep interrupted");
				} //
				wait =+ 2; // add 2 seconds to wait time
			} 
			if(exitVal==0){
				j=20;
			}
		}
		// destroy the process with a bit more force
		process.destroy();
		
	}

	/**
	 *   get the latex class file as defined in the telescope-config file, if a latex class is not defined, 
	 *   or the file does not exists it will return null. 
	 */
	private String getLatexClassFile(Proposal proposal, ServletContext servletContext){
		String realUrl = "";
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		
		List urlList = new ArrayList();
		String url = new String();
		urlList = OptionsUtils.getLabelValueBeans(Constants.LATEX_CLASS_FILE, new HashMap(), contextConfiguration);
		
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			url =  urlValue.getValue(); 
		} 
		
		//String url = telescopeConfiguration.getLatexClassFile();
		realUrl = servletContext.getRealPath(url);
		File latexFile = new File(realUrl);
		if (latexFile.exists()){
			return realUrl;
		}
		
		log.warn("latex class file not found "+realUrl);
		return null;
	}
}
