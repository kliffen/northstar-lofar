// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.semester.list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.struts.action.ActionForm;

import eu.radionet.northstar.business.configuration.CommunityConfiguration;

public class SemesterListForm extends ActionForm {
	protected String serverTime = null;

	protected String serverUTCTime = null;
    private String editButton = null;
	private String addButton = null;
	private String deleteButton = null;
	private boolean afterCurrentDate = true;
    private int selectedValue = 0;

	protected List semesterBeans = new ArrayList();
	private String[] selectedTelescopes = null;

	private List telescopes = new ArrayList();

	private String[] selectedCategories = null;

	private List categories = new ArrayList();
	private String[] selectedCommunities = null;
	
	private List communities = new ArrayList();
	public List getSemesterBeans() {
		return semesterBeans;
	}

	public void setSemesterBeans(List semesterBeans) {
		this.semesterBeans = semesterBeans;
	}

	public List getCategories() {
		return categories;
	}

	public void setCategories(List categories) {
		this.categories = categories;
	}

	public String[] getSelectedCategories() {
		return selectedCategories;
	}

	public void setSelectedCategories(String[] selectedCategories) {
		this.selectedCategories = selectedCategories;
	}

	public String[] getSelectedTelescopes() {
		return selectedTelescopes;
	}

	public void setSelectedTelescopes(String[] selectedTelescopes) {
		this.selectedTelescopes = selectedTelescopes;
	}

	public List getTelescopes() {
		return telescopes;
	}

	public void setTelescopes(List telescopes) {
		this.telescopes = telescopes;
	}

	public String getServerTime() {
		return serverTime;
	}

	public void setServerTime(String serverTime) {
		this.serverTime = serverTime;
	}

	public String getServerUTCTime() {
		return serverUTCTime;
	}

	public void setServerUTCTime(String serverUTCTime) {
		this.serverUTCTime = serverUTCTime;
	}

	public String getAddButton() {
		return addButton;
	}

	public void setAddButton(String addButton) {
		this.addButton = addButton;
	}
    public void setEditButton(int index, String value) {
        selectedValue = index;
        editButton = value;
    }

    public String getEditButton(int index) {
        return editButton;
    }

    public void setDeleteButton(int index, String value) {
        selectedValue = index;
        deleteButton = value;
    }

    public String getDeleteButton(int index) {
        return deleteButton;
    }

    public SemesterBean getSemesterBean(int index) {
		resizeSemesterBeans(index);
        return (SemesterBean) semesterBeans.get(index);

    }

    private void resizeSemesterBeans(int index) {
        int newSize = index + 1;
        if (semesterBeans.size() < newSize) {
            for (int i = semesterBeans.size(); semesterBeans.size() < newSize; i++){
				semesterBeans.add(new SemesterBean());
            }
        }
    }

	public int getSelectedValue() {
		return selectedValue;
	}

	public boolean isAfterCurrentDate() {
		return afterCurrentDate;
	}

	public void setAfterCurrentDate(boolean afterCurrentDate) {
		this.afterCurrentDate = afterCurrentDate;
	}

	public List getCommunities() {
		return communities;
	}

	public void setCommunities(List communities) {
		if (selectedCommunities == null) {
			String[] selectedCommunities = new String[communities.size()];
			Iterator communitiesIterator = communities.iterator();
			int i = 0;
			while (communitiesIterator.hasNext()){
				CommunityConfiguration communityConfiguration = (CommunityConfiguration) communitiesIterator.next();
				selectedCommunities[i] = communityConfiguration.getName();
				i++;
			}
			this.selectedCommunities = selectedCommunities;
		}	
		this.communities = communities;
	}

	public String[] getSelectedCommunities() {
		return selectedCommunities;
	}

	public void setSelectedCommunities(String[] selectedCommunities) {
		this.selectedCommunities = selectedCommunities;
	}
}
