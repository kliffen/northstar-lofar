// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.additionalissues;

import java.util.ArrayList;
import java.util.List;

import eu.radionet.northstar.control.proposal.ProposalForm;

public class AdditionalIssuesForm extends ProposalForm {
    protected boolean linkedProposals = false;
    protected String linkedProposalsSpecifics = null;
    protected boolean linkedProposalsElsewhere = false;
    protected String linkedProposalsElsewhereSpecifics = null;
    protected boolean relatedPublications = false;
    protected String relatedPublicationsText = null;
    protected boolean previousAllocations = false;
    protected String previousAllocationsSpecifics = null;
    protected boolean enablePublications = false;
    protected boolean enableGrantNumber = false;
    protected String grantNumber = null;
    protected Integer maxPublicationCount = null;
    protected String maxRelatedPreviousInvolvedProposalTextCount  = null;
    protected List thesisBeans = new ArrayList();
    protected String deleteThesisButton = null;
    protected String editThesisButton = null;
    protected String addThesisButton = null;
    protected int selectedThesisId = 0;
    protected boolean displaySponsoring = false;
    protected boolean enableSponsoring = false;
    protected boolean enableNewObserverExperience = false;
   
	protected String newObserverExperience = null;
    protected String sponsorDetails = null;
    protected String additionalRemarks = null;
    
    protected boolean relatedPreviousInvolvedProposal = false; 
    protected boolean enablePreviousInvolvedProposal = false;
    protected String relatedPreviousInvolvedProposalText = null;

    protected boolean enableLinkedProposalsThis = false;
    protected boolean enableLinkedProposalsOther = false;
    protected boolean enablePreviousAllocations = false;
    protected boolean enableRelatedPublications = false;
    
    protected boolean displayOpticonSupport = false;
    protected boolean opticonSupport = false;


    public boolean isEnableLinkedProposalsThis() {
		return enableLinkedProposalsThis;
	}
	public void setEnableLinkedProposalsThis(boolean enableLinkedProposalsThis) {
		this.enableLinkedProposalsThis = enableLinkedProposalsThis;
	}
	public boolean isEnableLinkedProposalsOther() {
		return enableLinkedProposalsOther;
	}
	public void setEnableLinkedProposalsOther(boolean enableLinkedProposalsOther) {
		this.enableLinkedProposalsOther = enableLinkedProposalsOther;
	}
	public boolean isEnablePreviousAllocations() {
		return enablePreviousAllocations;
	}
	public void setEnablePreviousAllocations(boolean enablePreviousAllocations) {
		this.enablePreviousAllocations = enablePreviousAllocations;
	}
	 public boolean isEnableNewObserverExperience() {
			return enableNewObserverExperience;
		}
		public void setEnableNewObserverExperience(boolean enableNewObserverExperience) {
			this.enableNewObserverExperience = enableNewObserverExperience;
		}
	public String getNewObserverExperience() {
		return newObserverExperience;
	}
	public void setNewObserverExperience(String newObserverExperience) {
		this.newObserverExperience = newObserverExperience;
	}
		public String getAdditionalRemarks() {
		return additionalRemarks;
	}
	public void setAdditionalRemarks(String additionalRemarks) {
		this.additionalRemarks = additionalRemarks;
	}
	public boolean isLinkedProposals() {
		return linkedProposals;
	}
	public void setLinkedProposals(boolean linkedProposals) {
		this.linkedProposals = linkedProposals;
	}
	public String getLinkedProposalsSpecifics() {
		return linkedProposalsSpecifics;
	}
	public void setLinkedProposalsSpecifics(String linkedProposalsSpecifics) {
		this.linkedProposalsSpecifics = linkedProposalsSpecifics;
	}

    /**
     * @return Returns the memberBeans.
     */
    public List getThesisBeans() {
        return thesisBeans;
    }

    public void setThesisBeans(List thesisBeans) {
        this.thesisBeans = thesisBeans;
    }

    /**
     * @return Returns the addThesisButton.
     */
    public String getAddThesisButton() {
        return addThesisButton;
    }

    /**
     * @param addThesisButton
     *            The addThesisButton to set.
     */
    public void setAddThesisButton(String addThesisButton) {
        this.addThesisButton = addThesisButton;
    }

    public void setDeleteThesisButton(int index, String value) {
        selectedThesisId = index;
        deleteThesisButton = value;
    }

    public String getDeleteThesisButton(int index) {
        return deleteThesisButton;
    }

    public void setEditThesisButton(int index, String value) {
        selectedThesisId = index;
        editThesisButton = value;
    }

    public String getEditThesisButton(int index) {
        return editThesisButton;
    }

   /**
     * @return Returns the selectedThesisId.
     */
    public int getSelectedThesisId() {
        return selectedThesisId;
    }
	public boolean isLinkedProposalsElsewhere() {
		return linkedProposalsElsewhere;
	}
	public void setLinkedProposalsElsewhere(boolean linkedProposalsElsewhere) {
		this.linkedProposalsElsewhere = linkedProposalsElsewhere;
	}
	public String getLinkedProposalsElsewhereSpecifics() {
		return linkedProposalsElsewhereSpecifics;
	}
	public void setLinkedProposalsElsewhereSpecifics(
			String linkedProposalsElsewhereSpecifics) {
		this.linkedProposalsElsewhereSpecifics = linkedProposalsElsewhereSpecifics;
	}
	
	public void setRelatedPublicationsText(String RelatedPublications){
		this.relatedPublicationsText = RelatedPublications;
	}

	public String getRelatedPublicationsText(){
		return this.relatedPublicationsText;
	}
	
	public void setRelatedPreviousInvolvedProposalText(String relatedPreviousInvolvedProposalText){
		this.relatedPreviousInvolvedProposalText = relatedPreviousInvolvedProposalText;
	}

	public String getRelatedPreviousInvolvedProposalText(){
		return this.relatedPreviousInvolvedProposalText;
	}	
	
	public int getMaxPublicationCount() {
		return maxPublicationCount.intValue();
	}
	public void setMaxPublicationCount(Integer maxPublicationCount) {
		this.maxPublicationCount = maxPublicationCount;
	}
	
	public String getMaxRelatedPreviousInvolvedProposalTextCount() {
		return maxRelatedPreviousInvolvedProposalTextCount;
	}
	public void setMaxRelatedPreviousInvolvedProposalTextCount(String maxRelatedPreviousInvolvedProposalTextCount) {
		this.maxRelatedPreviousInvolvedProposalTextCount = maxRelatedPreviousInvolvedProposalTextCount;
	}
	
	public boolean isEnablePublications() {
		return enablePublications;
	}
	public void setEnablePublications(boolean enablePublications) {
		this.enablePublications = enablePublications;
	}
	
	public boolean isPreviousAllocations() {
		return previousAllocations;
	}
	public void setPreviousAllocations(boolean previousAllocations) {
		this.previousAllocations = previousAllocations;
	}
	public String getPreviousAllocationsSpecifics() {
		return previousAllocationsSpecifics;
	}
	public void setPreviousAllocationsSpecifics(String previousAllocationsSpecifics) {
		this.previousAllocationsSpecifics = previousAllocationsSpecifics;
	}
	public boolean isEnableGrantNumber() {
		return enableGrantNumber;
	}
	public void setEnableGrantNumber(boolean enableGrantNumber) {
		this.enableGrantNumber = enableGrantNumber;
	}
	public String getGrantNumber() {
		return grantNumber;
	}
	public void setGrantNumber(String grantNumber) {
		this.grantNumber = grantNumber;
	}
	public boolean isDisplaySponsoring() {
		return displaySponsoring;
	}
	public void setDisplaySponsoring(boolean displaySponsoring) {
		this.displaySponsoring = displaySponsoring;
	}
	public boolean isEnableSponsoring() {
		return enableSponsoring;
	}
	public void setEnableSponsoring(boolean enableSponsoring) {
		this.enableSponsoring = enableSponsoring;
	}
	public String getSponsorDetails() {
		return sponsorDetails;
	}
	public void setSponsorDetails(String sponsordetails) {
		this.sponsorDetails = sponsordetails;
	}

	public boolean isOpticonSupport() {
		return opticonSupport;
	}
	public void setOpticonSupport(boolean opticonSupport) {
		this.opticonSupport = opticonSupport;
	}
	public boolean isDisplayOpticonSupport() {
		return displayOpticonSupport;
	}
	public void setDisplayOpticonSupport(boolean displayOpticonSupport) {
		this.displayOpticonSupport = displayOpticonSupport;
	}
	public boolean isRelatedPublications() {
		return relatedPublications;
	}
	public void setRelatedPublications(boolean relatedPublications) {
		this.relatedPublications = relatedPublications;
	}
	public boolean isEnableRelatedPublications() {
		return enableRelatedPublications;
	}
	public void setEnableRelatedPublications(boolean enableRelatedPublications) {
		this.enableRelatedPublications = enableRelatedPublications;
	}
	public void setSelectedThesisId(int selectedThesisId) {
		this.selectedThesisId = selectedThesisId;
	}
	public void setEnablePreviousInvolvedProposal(boolean enablePreviousInvolvedProposal) {
		this.enablePreviousInvolvedProposal = enablePreviousInvolvedProposal;
	}
	public boolean isEnablePreviousInvolvedProposal() {
		return enablePreviousInvolvedProposal;
	}
	public boolean isRelatedPreviousInvolvedProposal() {
		return relatedPreviousInvolvedProposal;
	}
	public void setRelatedPreviousInvolvedProposal(boolean relatedPreviousInvolvedProposal) {
		this.relatedPreviousInvolvedProposal = relatedPreviousInvolvedProposal;
	}
}
