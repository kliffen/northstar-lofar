// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * XMLConverter.java 
 *
 * Created on Mar 1, 2005
 *
 * Version $Id: XMLConverter.java,v 1.1 2006-05-02 10:01:13 verhoef Exp $
 *
 */
package eu.radionet.northstar.business.util;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.XMLBuilder;

import org.apache.xerces.parsers.DOMParser;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.business.ProposalXMLConverter;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The XMLConverter provides
 * 
 * @author Bastiaan Verhoef
 * 
 */
public class XMLConverter implements Serializable {

	private static XMLConverter converter = null;

	private XMLConverter() {
	}

	public static XMLConverter getInstance() {
		if (null == converter) {
			converter = new XMLConverter();

		}
		return converter;
	}

	public void convertDocument2OutputStream(Document document,
			OutputStream outputStream) throws IOException {

		OutputFormat format = new OutputFormat(document); // Serialize DOM
		format.setIndenting(true);
		XMLSerializer serial = new XMLSerializer(outputStream, format);
		serial.asDOMSerializer(); // As a DOM Serializer

		serial.serialize(document.getDocumentElement());
	}

	public void convertProposals2XML(List proposals,
			OutputStream outputStream) throws IOException, DatabaseException,
			InvalidConfigurationException, ParserConfigurationException {

		XMLBuilder xmlBuilder = new XMLBuilder();
		xmlBuilder.addNamespace("persistent",
				ProposalXMLConverter.PERSISTENT_NAMESPACE,
				"http://proposal.astron.nl/schemas/PersistentObjects.xsd");
		xmlBuilder.addNamespace("user",
				ProposalXMLConverter.USER_ADMIN_NAMESPACE,
				"http://proposal.astron.nl/schemas/UserAdmin.xsd");
		xmlBuilder.addNamespace("ns", ProposalXMLConverter.NORTHSTAR_NAMESPACE,
				"http://proposal.astron.nl/schemas/NorthStarProposal.xsd");		
		xmlBuilder.addRootElement(ProposalXMLConverter.NORTHSTAR_NAMESPACE,
				"proposals");
		for (int i = 0; i < proposals.size(); i++) {
			Proposal proposal = (Proposal) proposals.get(i);
			ProposalDelegate proposalDelegate = ProposalDelegateFactory
					.getProposalDelegate(proposal);
			proposalDelegate.buildXml(xmlBuilder, proposal);
		}

		Document document = xmlBuilder.getDocument();
		OutputFormat format = new OutputFormat(document); // Serialize DOM
		format.setIndenting(true);
		XMLSerializer serial = new XMLSerializer(outputStream, format);
		serial.asDOMSerializer(); // As a DOM Serializer

		serial.serialize(document.getDocumentElement());
	}

	public String convertDocument2String(Document document) throws IOException {

		OutputFormat format = new OutputFormat(document); // Serialize DOM
		format.setIndenting(true);
		StringWriter stringOut = new StringWriter(); // Writer will be a
		// String
		XMLSerializer serial = new XMLSerializer(stringOut, format);
		serial.asDOMSerializer(); // As a DOM Serializer

		serial.serialize(document.getDocumentElement());
		return stringOut.toString(); // Spit out DOM as a String
	}

	public Document convertStringToDocument(String myXML) throws Exception {
		// read an xml string into a domtree
		Document document;
		DOMParser itsParser = new DOMParser();

		StringReader reader = new StringReader(myXML);
		InputSource source = new InputSource(reader);
		itsParser.parse(source);

		// get document
		document = itsParser.getDocument();
		return document;
	}

	public Document convertStringToDocument(URL file) throws Exception {
		// read an xml string into a domtree
		Document document;
		DOMParser itsParser = new DOMParser();

		InputSource source = new InputSource(file.openStream());
		itsParser.parse(source);

		// get document
		document = itsParser.getDocument();
		return document;
	}
}
