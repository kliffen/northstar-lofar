<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>
<tiles:get name="errors" />

<html:form action="/processProposalList" method="POST">

<TABLE>

	<TR>
		<td><b><astron:label key="label.applicant.forceid" /></b>
				<html:radio styleClass="radio" property="forceProjectId" value="true">Yes</html:radio>
				<html:radio styleClass="radio" property="forceProjectId" value="false">No</html:radio>
		</td>
	</TR>
	<TR>
		<td><b><astron:label key="label.applicant.status" /></b>
	 <logic:iterate id="status" property="statusses" name="adminProposalListForm">
  	    <html:multibox property="selectedStatusses">
            <bean:write name="status" property="code"/>
		</html:multibox>
			<bean:write name="status" property="code"/>
	 </logic:iterate>   
		</td>
	</TR>
	<TR>
		<td><b><astron:label key="label.applicant.category" /></b>
 
	 <logic:iterate id="category" property="categories" name="adminProposalListForm">
  	    <html:multibox property="selectedCategories">
            <bean:write name="category" property="code"/>
		</html:multibox>
			<bean:write name="category" property="code"/>
	 </logic:iterate>   
		</td>
	</TR>
	<TR>
		<td><b><astron:label key="label.applicant.telescopes" /></b>
 
	 <logic:iterate id="telescope" property="telescopes" name="adminProposalListForm">
  	    <html:multibox property="selectedTelescopes">
            <bean:write name="telescope" />
		</html:multibox>
			<bean:write name="telescope"/>
	 </logic:iterate>   
		</td>
	</TR>
	<TR>
		<td><b><astron:label key="label.applicant.communities" /></b>
 
	 <logic:iterate id="community" property="communities" name="adminProposalListForm">
  	    <html:multibox property="selectedCommunities">
            <bean:write name="community" property="name" />
		</html:multibox>
			<bean:write name="community" property="description"/>
	 </logic:iterate>   
		</td>
	</TR>	
	<TR>
		<td><b><astron:label key="label.applicant.semesters" /></b> 
		
		<bean:define id="myyearlist" name="adminProposalListForm" property="yearList"/> 
		<html:select name="adminProposalListForm" property="selectedYear" size="1" style="width:5em;" onchange="document.forms[0].submit()">
			<html:options collection="myyearlist" property="name" labelProperty="value"/>		
		</html:select>
		
		 <logic:iterate id="semester" property="semesters" name="adminProposalListForm">
		 <html:multibox property="selectedSemesters">
		 	<bean:write name="semester" />
		 </html:multibox>
		 	<bean:write name="semester"/>
		 </logic:iterate><br>
		 
		</td>
	</TR>			
	<TR>
		<TD>
			
		<html:submit property="filterButton" styleClass="filter2" onclick="" tabindex="1">Filter</html:submit> 
		</TD>
	</TR>
	<TR>
		<TD>
			
		<html:submit property="listButton" styleClass="filter2" onclick="" tabindex="1">Download summary</html:submit> 
		<html:submit property="xmlButton" styleClass="filter2" onclick="" tabindex="2">Download XML</html:submit>
		<html:submit property="allButton" styleClass="filter2" onclick="" tabindex="3">Download All</html:submit>
		<html:submit property="abstractButton" styleClass="filter2" onclick="" tabindex="4">Download Abstracts</html:submit>
		<html:submit property="viewStatisticsButton" styleClass="filter2" onclick="" tabindex="5">View Statistics</html:submit>
		</TD>
	</TR>
	<logic:notEmpty name="adminProposalListForm" property="adminTelescopeBeans">
	<TR>
		<TD>
			<table align="center" class="projecttable">
				<TR>
     				<td class="proposaltableheader"><astron:label key="label.applicant.projid" /></td>
     				<td class="proposaltableheader"><astron:label key="label.applicant.author" /></td>
     				<td class="proposaltableheader"><astron:label key="label.applicant.pi" /></td>
     				<td class="proposaltableheader"><astron:label key="label.applicant.title" /></td>
     				<td class="proposaltableheader"><astron:label key="label.applicant.status" /></td>
     				<td class="proposaltableheader"><astron:label key="label.applicant.since" /></td>     				
					<td class="proposaltableheader"><astron:label key="label.applicant.category" /></td>
     				<td class="proposaltableheader" nowrap="nowrap"><astron:label key="label.applicant.options" /></td>
				</TR>
				<logic:iterate id="adminTelescopeBean" name="adminProposalListForm"
						property="adminTelescopeBeans" indexId="telescopeIndex">
					<tr>
						<td class="proposalsubtableheader" colspan="11"><bean:write
								name="adminTelescopeBean" property="description" /> Proposals (<bean:write
								name="adminTelescopeBean" property="community" />)</td>
					</tr>
			<%
			int i=0;
			String styleClass = "";
			%>
	 			<logic:iterate id="adminProposalBean" name="adminTelescopeBean" property="adminProposalBeans" indexId="proposalIndex">
	 			<%
	 				styleClass = (i++ % 2 == 0 ? "" : "_odd");	 				
	 			%>
 	 			<TR>
					<html:hidden property='<%= "adminTelescopeBean[" + telescopeIndex + "].adminProposalBean[" + proposalIndex + "].id" %>'/>
					<TD class="tablefield_pri<%=styleClass%>" valign="top"><bean:write name="adminProposalBean" property="code"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="adminProposalBean" property="contactAuthorName"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="adminProposalBean" property="piName"/></TD>
					<TD class="tablefield<%=styleClass%>" valign="top"><bean:write name="adminProposalBean" property="title"/></TD>
					<TD nowrap="true"  valign="top" class="status_<bean:write name="adminProposalBean" property="statusClass"/><%=styleClass%>">
						<bean:write name="adminProposalBean" property="status"/>
					</TD>
					<TD nowrap="true"   valign="top" class="status_<bean:write name="adminProposalBean" property="statusClass"/><%=styleClass%>">	<bean:write name="adminProposalBean" property="statusTime"/></TD>			
					<TD class="tablefield<%=styleClass%>"  valign="top"><bean:write name="adminProposalBean" property="category"/></TD>
						<TD   valign="top" class="tablefield<%=styleClass%>"  nowrap="nowrap">
						<html:submit indexed="false" property='<%= "adminTelescopeBean[" + telescopeIndex + "].changeStatusButton[" + proposalIndex + "]" %>' title="Change status and compose email" styleClass="list_change_status">Change status</html:submit>
						<html:submit indexed="false" property='<%= "adminTelescopeBean[" + telescopeIndex + "].viewStatusHistoryButton[" + proposalIndex + "]" %>' title="View status history" styleClass="list_view_statushistory">View status history</html:submit>
						<html:submit indexed="false" property='<%= "adminTelescopeBean[" + telescopeIndex + "].viewProposalButton[" + proposalIndex + "]" %>' title="View your proposal. (pdf)" styleClass="list_view_pdf">View</html:submit>
						</TD>


	 			</TR>
				</logic:iterate>
	 			</logic:iterate>
				<tr><td colspan="9" class="spacer">&nbsp;</td></tr>
			</table>
		</TD>
	</TR>
	
	</logic:notEmpty>
	
	<logic:empty name="adminProposalListForm" property="adminTelescopeBeans">
		<TR>
		<TD align="center">- No results -</TD>
	</TR>
	</logic:empty>
	
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	
</TABLE>

</html:form>

