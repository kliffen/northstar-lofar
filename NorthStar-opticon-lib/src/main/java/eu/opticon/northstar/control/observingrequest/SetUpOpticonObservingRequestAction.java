// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;

import org.apache.struts.util.LabelValueBean;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import nl.astron.database.exception.DatabaseException;
import nl.astron.service.lofar.StorageCalculator;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import eu.opticon.northstar.business.OpticonConstants;
import eu.opticon.northstar.business.OpticonTargetUtils;
import eu.opticon.northstar.control.observingrequest.instrument.InstrumentBean;
import eu.opticon.northstar.control.observingrequest.instrument.SetUpInstrumentBean;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.ConfigurationUtil;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.observingrequest.ObservingRequestForm;
import eu.radionet.northstar.control.proposal.observingrequest.SetUpObservingRequestAction;
import eu.radionet.northstar.control.proposal.observingrequest.ValueBean;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Target;

/**
 * This class retrieves telescope specific observation information out of the
 * database and enters them in the form bean.
 * 
 * @author Anton Smit
 */
public class SetUpOpticonObservingRequestAction extends SetUpObservingRequestAction {
	StorageCalculator sCalc = null;

	public void fillOptions(ObservingRequestForm observingRequestForm) throws DatabaseException {
		OpticonObservingRequestForm opticonObservingRequestForm = (OpticonObservingRequestForm) observingRequestForm;
		List observingModes = OptionsUtils.getLabelValueBeansWithoutNoneSpecified(OpticonConstants.OBSERVING_MODE,
				new HashMap(), contextConfiguration);
		opticonObservingRequestForm.setObservingModes(observingModes);

		FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration
				.getFieldsDefinitionType();
		opticonObservingRequestForm.setDisplayNoiseLevel(
				OptionsDelegate.allowedToDisplay(OpticonConstants.NOISE_LEVEL, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayNightTime(
				OptionsDelegate.allowedToDisplay(OpticonConstants.NIGHT_TIME, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayObservingMode(
				OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVING_MODE, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayAllocationJustification(
				OptionsDelegate.allowedToDisplay(OpticonConstants.ALLOCATION_JUSTIFICATION, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayLongTermProposal(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_LONGTERM_PROPOSAL, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayScienceCategory(
				OptionsDelegate.allowedToDisplay(OpticonConstants.SCIENCE_CATEGORY, fieldsDefinitionType));
		opticonObservingRequestForm
				.setDisplayTravel(OptionsDelegate.allowedToDisplay(OpticonConstants.TRAVEL, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayProprietaryException(
				OptionsDelegate.allowedToDisplay(OpticonConstants.PROPRIETARY, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayRequestedTime(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_REQUESTED_TIME, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayUsefulTime(
				OptionsDelegate.allowedToDisplay(OpticonConstants.USEFUL_TIME, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayAwardedTime(
				OptionsDelegate.allowedToDisplay(OpticonConstants.AWARDED_TIME, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayLongTermTime(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_LONGTERM_TIME, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayLongTermStorage(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_LONGTERM_STORAGE, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayProcessingTime(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_PROCESSING_TIME, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayTotalTime(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_TOTAL_TIME, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayObservingDetails(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_OBSERVING_DETAILS, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayChildren(
				OptionsDelegate.allowedToDisplay(OpticonConstants.SHOW_CHILDREN, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplaySelectParent(
				OptionsDelegate.allowedToDisplay(OpticonConstants.SELECT_PARENT, fieldsDefinitionType));
		opticonObservingRequestForm.setDisplayPipeline(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_PIPELINE, fieldsDefinitionType));

		if (OptionsDelegate.allowedToDisplay(Constants.ENABLE_COMBINED_OUTPUT_SELECTION, fieldsDefinitionType)) {
			opticonObservingRequestForm.setEnableCombinedOutputProductSelection(true);
		} else {
			opticonObservingRequestForm.setEnableCombinedOutputProductSelection(false);
		}

		// do not show any allocation box if there arre no allocations to fill
		// in
		boolean displayLongtermTime = false;
		if (opticonObservingRequestForm.isLongTermProposal() && opticonObservingRequestForm.isDisplayLongTermTime()) {
			displayLongtermTime = true;
		}

		if (opticonObservingRequestForm.isDisplayAwardedTime() || displayLongtermTime
				|| opticonObservingRequestForm.isDisplayRequestedTime()
				|| opticonObservingRequestForm.isDisplayUsefulTime()
				|| opticonObservingRequestForm.isDisplayTotalTime()) {
			opticonObservingRequestForm.setDisplayAllocations(true);
		} else {
			opticonObservingRequestForm.setDisplayAllocations(false);
		}
	}

	/**
	 * fills the form with information out of the data layer. Already
	 * implemented in this skeleton are the observation details and the
	 * requested time the proposer wants to use.
	 * 
	 * @see eu.radionet.northstar.control.proposal.observingrequest.SetUpObservingRequestAction#fillForm(javax.servlet.http.HttpServletRequest,
	 *      eu.radionet.northstar.data.entities.ObservingRequest,
	 *      eu.radionet.northstar.control.proposal.observingrequest.ObservingRequestForm)
	 */
	String contextPath = "";
	protected void fillForm(HttpServletRequest request, ObservingRequest observingRequest,
			ObservingRequestForm observingRequestForm) throws DatabaseException, InvalidConfigurationException {
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
		OpticonObservingRequestForm opticonObservingRequestForm = (OpticonObservingRequestForm) observingRequestForm;

		ProposalData proposalData = (ProposalData) request.getSession().getAttribute(Constants.PROPOSAL_DATA);
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposalData.getProposal());
		
		contextPath = request.getSession().getServletContext().getRealPath("/");
		
		if (contextConfiguration == null) {
			contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());
		}
		FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration
				.getFieldsDefinitionType();
		/*
		 * sets the headers of the observation table in the ObservingRequest
		 * page.
		 */
		String lastString = "Time";

		boolean totalTime = false;
		boolean observationPhase = false;
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVATION_PHASE, fieldsDefinitionType)) {
			lastString += "/condition";
			observationPhase = true;
		}

		if (OptionsDelegate.allowedToDisplay(OpticonConstants.SUMMARY_TOTALTIME, fieldsDefinitionType)) {
			lastString = "Exposure (Hours)"; // change to hour
			totalTime = true;
		}

		String idLabel = "id";
		String telescopeLabel = "Telescope";
		String instrumentLabel = "Instrument";
		String targetsLabel = "Targets";
		String runsLabel = "Runs";
		String storageLabel = "Total LTA Storage (TB)";
		String beamStorageLabel = "BF Data (TB)";
		String cvStorageLabel = "UV Data (TB)";
		String tbbStorageLabel = "TBB Data (TB)";
		String timeLabel = "av time";
		String freqLabel = "av freq";
		String demixLabel = "demixing";
		String allowStoreRawDataLabel = "Store Raw BF";
		String allowCVDataStoragelabel = "Store Raw UV";
		try {
			idLabel = NorthStarUtils.getLabel(request.getSession().getServletContext(), proposalData.getProposal(),
					"label.opticon.observingrequest.id");
			telescopeLabel = NorthStarUtils.getLabel(request.getSession().getServletContext(),
					proposalData.getProposal(), "label.opticon.observingrequest.telescope");
			instrumentLabel = NorthStarUtils.getLabel(request.getSession().getServletContext(),
					proposalData.getProposal(), "label.opticon.observingrequest.instrument");
			targetsLabel = NorthStarUtils.getLabel(request.getSession().getServletContext(), proposalData.getProposal(),
					"label.opticon.observingrequest.targets");
		} catch (FileNotFoundException e) {
			log.warn("no label file found");
		}

		if (OptionsDelegate.allowedToDisplay(Constants.AUTO_TARGET_COMMIT, fieldsDefinitionType)) {

			opticonObservingRequestForm
					.setObservationSummaryLabels(new String[] { idLabel, telescopeLabel, instrumentLabel, lastString });
		} else {
			opticonObservingRequestForm.setObservationSummaryLabels(new String[] {
					// "id","targets", "Telescope","Instrument",lastString });
					idLabel, targetsLabel, runsLabel, telescopeLabel, instrumentLabel, lastString, beamStorageLabel,
					allowStoreRawDataLabel, cvStorageLabel, allowCVDataStoragelabel, tbbStorageLabel, storageLabel }); // add
																														// one
																														// more
																														// column,allowStoreRawDataLabel,beamStorageLabel,cvStorageLabel

			opticonObservingRequestForm.setPipelineSummaryLabels(new String[] { idLabel, targetsLabel, telescopeLabel,
					instrumentLabel, timeLabel, freqLabel, demixLabel, storageLabel,
					OpticonConstants.PROCESSING_TIME_RATIO, OpticonConstants.PROCESSING_TIME_TOTAL });
		}

		List observationSummaryValues = new ArrayList();
		Iterator observingRequestIterator = opticonObservingRequest.getObservations().iterator();
		int totalSpecifiedTime = 0;
		double totalprocessingTimeSpecified = 0d;// total time specified by all
													// the observations
		double dataSize = 0d;
		double cepLoad = 0d;
		int observationcount = 0;
		int pipelinecount = 0;
		int targetCount = 0;
		int runsCount = 0;
		Map<String, Double> observationStorageMap = new HashMap();
		Map<String, Double> observationDurationMap = new HashMap();

		Map<String, Boolean> observationVisitedMap = new HashMap();
		Map<String, Integer> observationSubbandMap = new HashMap();
		Map<String, Integer> observationStationMap = new HashMap();

		while (observingRequestIterator.hasNext()) {

			List targetDurationMap = new ArrayList();
			List targetSubbandMap = new ArrayList();
			List targetThisObservation = new ArrayList();
			List observationValues = new ArrayList();

			Double totalDuration = 0d;
			int targetSubbands = 0;

			OpticonObservation opticonObservation = (OpticonObservation) observingRequestIterator.next();
			// set observation label
			char chr = (char) (observationcount + 65);
			observationValues.add(new ValueBean(String.valueOf(chr)));
			observationcount++;
			// set amount of targets
			if (!OptionsDelegate.allowedToDisplay(Constants.AUTO_TARGET_COMMIT, fieldsDefinitionType)) {
				String targets = null;
				String runs = "no run";
				targets = "no target";
				Integer Id = opticonObservation.getId();

				targetCount = 0;
				runsCount = 0;
				for (Target target : opticonObservingRequest.getTargets()) {
					OpticonTarget optarget = (OpticonTarget) target;
					if (optarget.getObservingrun() != null
							&& optarget.getObservingrun().equalsIgnoreCase(String.valueOf(chr))) {
						targetCount++;
						if (optarget.isFirstInRun()) {
							runsCount++;
						}
					}
				}

				targets = Integer.toString(targetCount) + " targets";
				runs = Integer.toString(runsCount) + " runs";
				observationValues.add(new ValueBean(String.valueOf(targets)));
				observationValues.add(new ValueBean(String.valueOf(runs)));
			}
			// set telescope name
			observationValues.add(new ValueBean(opticonObservation.getTelescopeConfiguration()));
			// set instrument name
			if (opticonObservation.getInstrument() != null) {
				observationValues.add(new ValueBean(opticonObservation.getInstrument().getName()));
			} else {
				observationValues.add(new ValueBean(null));
			}

			Double presentationTotalDuration = 0d;
			if (totalTime) {

				for (Target target : opticonObservingRequest.getTargets()) {
					OpticonTarget optarget = (OpticonTarget) target;

					if (optarget.getAllocations() != null) {
						Double targetDuration = NorthStarDelegate.getResourceValue(optarget.getAllocations(),
								OpticonConstants.TOTAL_DURATION);
						if (targetDuration != null && optarget.getObservingrun() != null
								&& optarget.getObservingrun().equalsIgnoreCase(String.valueOf(chr))) {
							if (optarget.isFirstInRun())
								presentationTotalDuration += targetDuration.doubleValue();
							totalDuration += targetDuration.doubleValue();
							int targetSubband = SetUpInstrumentBean
									.convertSubbandListDescription(optarget.getSubbandList());
							targetSubbands += targetSubband;
							targetDurationMap.add(targetDuration);
							targetSubbandMap.add(targetSubband);
							targetThisObservation.add(optarget.getRunnumber());
						}
					}
				}

				if (opticonObservation.getInstrument().getTbbExposureTime() != null
						&& opticonObservation.getInstrument().isPiggyBack()) {
					presentationTotalDuration = totalDuration = opticonObservation.getInstrument().getTbbExposureTime();
					observationValues.add(new ValueBean(String.valueOf(presentationTotalDuration)));
				} else
					observationValues
							.add(new ValueBean(OpticonTargetUtils.getHoursFromSeconds(presentationTotalDuration)));
			} else if (observationPhase) {
				// set specified time/conditions
				String timeCond = null;
				Double specifiedTime = NorthStarDelegate.getResourceValue(opticonObservation.getAllocations(),
						opticonObservation.getTelescopeConfiguration() + "_" + OpticonConstants.REQUESTEDTIME);
				if (specifiedTime != null) {
					timeCond = AstronConverter.toString(specifiedTime);
				} else {
					timeCond = "0";
				}
				timeCond += " / " + opticonObservation.getObservationPhase(); // observationtime
																				// is
																				// the
																				// moonphase
				observationValues.add(new ValueBean(timeCond));
			} else {
				String timeCond = null;
				Double specifiedTime = NorthStarDelegate.getResourceValue(opticonObservation.getAllocations(),
						opticonObservation.getTelescopeConfiguration() + "_" + OpticonConstants.REQUESTEDTIME);
				if (specifiedTime != null) {
					timeCond = AstronConverter.toString(specifiedTime);
				} else {
					timeCond = "0";
				}
				observationValues.add(new ValueBean(timeCond));
			}

			HashMap dataStore = null;// calculateBFAndUVSize(opticonObservation,totalDuration,
										// targetSubbands);

			double storage = 0.0;
			log.info("instrument name : "+opticonObservation.getInstrument().getName());
			if (opticonObservation.getInstrument().getName().startsWith(OpticonConstants.TBB_PIGGYBACK))
				dataStore = calculateBFAndUVSize(opticonObservation,
						opticonObservation.getInstrument().getTbbExposureTime(), targetSubbands);
			else
				dataStore = calculateBFAndUVSizePerTarget(opticonObservation, targetDurationMap, targetSubbandMap,
						targetThisObservation);
			log.info("dataStore :"+dataStore.toString());
			if (dataStore != null) {

				BigDecimal beam = (BigDecimal) dataStore.get(OpticonConstants.BEAM_DATA_SIZE);
				if (beam != null) {
					observationValues
							.add(new ValueBean(String.valueOf(OpticonTargetUtils.convertToTB(beam).doubleValue())));
					storage += beam.doubleValue();
				} else
					observationValues.add(new ValueBean("0.0"));
			} else
				observationValues.add(new ValueBean("0.0"));

			// add new column to store raw data yes/no to observing request list
			if (opticonObservation.getInstrument().isStoreRawData()) {
				observationValues.add(new ValueBean("YES"));
			} else {
				observationValues.add(new ValueBean("NO"));
			}
			BigDecimal uv = new BigDecimal(0);
			if (dataStore != null) {
				uv = (BigDecimal) dataStore.get(OpticonConstants.CORRELATED_VISIBILITY_DATA_SIZE);
				if (uv != null) {
					observationValues
							.add(new ValueBean(String.valueOf(OpticonTargetUtils.convertToTB(uv).doubleValue())));
					storage += uv.doubleValue();
				} else
					observationValues.add(new ValueBean("0.0"));
			} else
				observationValues.add(new ValueBean("0.0"));

			if (opticonObservation.getInstrument().isStoreUVData()) {
				observationValues.add(new ValueBean("YES"));
			} else {
				observationValues.add(new ValueBean("NO"));
			}

			BigDecimal tbb = new BigDecimal(0);
			if (dataStore != null) {
				tbb = (BigDecimal) dataStore.get(OpticonConstants.TBB_DATA_SIZE);
				if (tbb != null) {
					storage += tbb.doubleValue();
					observationValues
							.add(new ValueBean(String.valueOf(OpticonTargetUtils.convertToTB(tbb).doubleValue())));
				} else
					observationValues.add(new ValueBean("0.0"));
			} else
				observationValues.add(new ValueBean("0.0"));
			// double storage
			// =calculateStorage(opticonObservation,totalDuration,
			// targetSubbands);
			BigDecimal totalData = new BigDecimal(0);

			if (dataStore != null)
				totalData = (BigDecimal) dataStore.get(OpticonConstants.TOTAL_DATA_SIZE);
			if (totalData.doubleValue() > 0.0)
				storage = totalData.doubleValue();
			totalData = OpticonTargetUtils.convertToTB(totalData);
			totalData = totalData.setScale(2, BigDecimal.ROUND_UP);
			// result = String.valueOf(bd.doubleValue());
			observationValues.add(new ValueBean(String.valueOf(totalData.doubleValue())));

			dataSize += totalData.doubleValue();

			observationSummaryValues.add(observationValues);

			/*
			 * //FIXME ugly bypass to calculate pipeline data for intefero
			 * if(opticonObservation.getInstrument() != null &&
			 * !opticonObservation.getInstrument().isStoreRawData() &&
			 * opticonObservation.getInstrument().getName()!= null &&
			 * opticonObservation.getInstrument().getName().startsWith(
			 * "Interfero") ){
			 * opticonObservation.getInstrument().setStoreRawData(true); storage
			 * =
			 * calculateStorage(opticonObservation,totalDuration,targetSubbands)
			 * ; opticonObservation.getInstrument().setStoreRawData(false); }
			 */

			observationStorageMap.put(String.valueOf(chr), storage);
			observationSubbandMap.put(String.valueOf(chr), targetSubbands);
			observationDurationMap.put(String.valueOf(chr), totalDuration);
			observationVisitedMap.put(String.valueOf(chr), false);
			// get the station number
			String stationDescription = opticonObservation.getInstrument()
					.getStation();
			if(stationDescription!=null && stationDescription.startsWith("Custom")){
				String stationNumber = "";
				int lbegin = stationDescription.indexOf("(");
				int lend = stationDescription.indexOf(")");
				stationNumber = opticonObservation.getInstrument().getCustomStationSpecifics();
				if(!AstronValidator.isBlankOrNull(stationNumber)){
					lbegin = stationNumber.indexOf("(");
					lend = stationNumber.indexOf(")");
					if(lbegin >=0 && lend >=0){
						stationNumber=stationNumber.substring(lbegin+1,lend);
					} else {
						lbegin = stationNumber.indexOf("[");
						lend = stationNumber.indexOf("]");
						if(lbegin >=0 && lend >=0){
							stationNumber=stationNumber.substring(lbegin+1,lend);
							stationNumber =String.valueOf(stationNumber.split(",").length);
						}
					}
				}
				try {
					observationStationMap.put(String.valueOf(chr),Integer.parseInt(stationNumber));
					observationVisitedMap.put(String.valueOf(chr),false);
				} catch (NumberFormatException e) {
					log.warn("There is no station selected");
					observationStationMap.put(String.valueOf(chr), 0);
				}
			}else{
				if (!AstronValidator.isBlankOrNull(stationDescription)) {
					int stationNumberStartIndex = stationDescription.indexOf('(');
					int stationNumberEndIndex = stationDescription.indexOf(')');
					String stationNumber = "";
					int coreStations = 0;
					int remoteStations = 0;
					int internationalStations = 0;
					if(!AstronValidator.isBlankOrNull(stationDescription) && stationNumberStartIndex>=0){
						stationNumber = stationDescription.substring(
								stationNumberStartIndex + 1, stationNumberEndIndex);
						String[] stationsArray = stationNumber.split(",");
				        String coreStationsString = stationsArray[0].trim();
				        String remoteStationsString = stationsArray[1].trim();
				        String internationalStationsString = stationsArray[2].trim();
				        try{
							coreStations = new Integer(coreStationsString).intValue();
						}catch(NumberFormatException e){
							log.warn(" "+coreStationsString+" is not an integer");
						}
				        try{
				        	remoteStations = new Integer(remoteStationsString).intValue();
						}catch(NumberFormatException e){
							log.warn(" "+remoteStationsString+" is not an integer");
						}
				        try{
				        	internationalStations = new Integer(internationalStationsString).intValue();
						}catch(NumberFormatException e){
							log.warn(" "+internationalStationsString+" is not an integer");
						}
				        
				        int stations = coreStations + remoteStations + internationalStations;
				        stationNumber = String.valueOf(stations);
					}
					else{
						String array[]=stationDescription.split(",");
						stationNumber=String.valueOf(array.length);
					}
					try {
						observationStationMap.put(String.valueOf(chr),Integer.parseInt(stationNumber));
						observationVisitedMap.put(String.valueOf(chr),false);
					} catch (NumberFormatException e) {
						log.warn("There is no station selected");
						observationStationMap.put(String.valueOf(chr), 0);
					}
				}
			}
			/*String stationDescription = opticonObservation.getInstrument().getStation();
			if (stationDescription != null && stationDescription.startsWith("Custom")) {
				stationDescription = opticonObservation.getInstrument().getCustomStationSpecifics();
			}
			if (!AstronValidator.isBlankOrNull(stationDescription)) {
				int stationNumberStartIndex = stationDescription.indexOf('(');
				int stationNumberEndIndex = stationDescription.indexOf(')');
				String stationNumber = "0";
				if (!AstronValidator.isBlankOrNull(stationDescription) && stationNumberStartIndex >= 0) {
					stationNumber = stationDescription.substring(stationNumberStartIndex + 1, stationNumberEndIndex);
				} else {
					String array[] = stationDescription.split(",");
					stationNumber = String.valueOf(array.length);
				}

				try {
					observationStationMap.put(String.valueOf(chr), Integer.parseInt(stationNumber));
				} catch (NumberFormatException e) {
					log.warn("There is no station selected");
					observationStationMap.put(String.valueOf(chr), 0);
				}
			}*/
			/*
			 * List urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.
			 * STORAGECALCULATORURL , new HashMap(), contextConfiguration); if
			 * (urlList != null && urlList.size() > 1){
			 */

		}

		opticonObservingRequestForm.setObservationSummaryValues(observationSummaryValues);

		// fill pipeline summary
		List pipelineSummaryValues = new ArrayList();

		Iterator pipelineIterator = opticonObservingRequest.getPipelines().iterator();
		while (pipelineIterator.hasNext()) {
			OpticonObservation opticonObservation = (OpticonObservation) pipelineIterator.next();

			List pipelineValues = new ArrayList();
			// set observation label
			char chr = (char) (pipelinecount + 65);
			// set amount of targets
			String targets = "no target";
			String mode = "";
			String avtime = "";
			String avfreq = "";
			String demix = "N";
			String pipeStorage = "0.0";
			HashMap processTimeResult = null;
			HashMap<String, Integer> poRatioList = new HashMap();

			if (opticonObservation.getInstrument() != null) {

				targetCount = 0;
				int totalTargetSubband = 0;
				int totalStation = 0;
				double totalObservingDuration = 0;
				double pipelineStorage = 0d;
				double pipelineAveraging = 0d;
				double pipelineAveragingFreq = 1d;
				mode = opticonObservation.getInstrument().getProcessingMode();

				// populate processingtime
				List uvrangeList = new ArrayList();
				String uvRangeValue = new String();
				uvrangeList = OptionsUtils.getLabelValueBeans(Constants.UV_RANGE_IMAGING, new HashMap(),
						contextConfiguration);

				if (uvrangeList != null && uvrangeList.size() > 1) {
					LabelValueBean urlValue = (LabelValueBean) uvrangeList.get(1);
					uvRangeValue = urlValue.getValue();
				}
				double uvRange = 0;
				if (!AstronValidator.isBlankOrNull(uvRangeValue)) {
					uvRange = new Double(uvRangeValue).doubleValue();
				}

				List factors = new ArrayList();
				String factorValue = new String();
				factors = OptionsUtils.getLabelValueBeans(OpticonConstants.PROCESSING_TIME_BASELINE_FACTOR,
						new HashMap(), contextConfiguration);

				if (factors != null && factors.size() > 1) {
					LabelValueBean lvb = (LabelValueBean) factors.get(1);
					factorValue = lvb.getValue();
				}
				double baselineFactor = 1;
				if (!AstronValidator.isBlankOrNull(factorValue)) {
					baselineFactor = new Double(factorValue).doubleValue();
				}

				if (opticonObservation.getInstrument().getAveragingTime() != null) {
					pipelineAveraging = opticonObservation.getInstrument().getAveragingTime().doubleValue();
				}
				if (opticonObservation.getInstrument().getAveragingFrequency() != null) {
					pipelineAveragingFreq = opticonObservation.getInstrument().getAveragingFrequency().doubleValue();
				}
				double totalProcessingTime = 0;
				for (Target target : opticonObservingRequest.getTargets()) {
					OpticonTarget optarget = (OpticonTarget) target;
					if (optarget != null && optarget.getObservingrun() != null && optarget.getPipeline() != null
							&& optarget.getPipeline().equalsIgnoreCase(String.valueOf(chr))) {
						log.info("****targetCount :"+targetCount);
						// pipelinestorage is the sum data of the targets
						// divided by integration time,
						// each target generates the amount of raw data to the
						// observation it belongs for its share of subbands...
						double observationStorage = 0d;
						if (optarget.getObservingrun() != null && optarget.getObservingrun().length() > 0
								&& observationStorageMap.get(optarget.getObservingrun()) != null) {
							observationStorage = observationStorageMap.get(optarget.getObservingrun());
							if (observationStationMap.get(optarget.getObservingrun()) != null
									&& !observationVisitedMap.get(optarget.getObservingrun())) {
								totalStation = observationStationMap.get(optarget.getObservingrun());
								observationVisitedMap.put(optarget.getObservingrun(), true);
							}
							if (observationDurationMap.get(optarget.getObservingrun()) != null) {
								totalObservingDuration += observationDurationMap.get(optarget.getObservingrun());

							}
						}
						int thisTargetSubband = SetUpInstrumentBean
								.convertSubbandListDescription(optarget.getSubbandList());
						totalTargetSubband += thisTargetSubband;
						int observationSubbands = 0;
						if (observationSubbandMap.get(optarget.getObservingrun()) != null) {
							observationSubbands = observationSubbandMap.get(optarget.getObservingrun());
						}
						if (observationSubbands * pipelineAveraging > 0) {

							double pipelineStorageTarget = (observationStorage * thisTargetSubband)
									/ (observationSubbands * pipelineAveraging * pipelineAveragingFreq);
									//calculateStorage(opticonObservation,opticonObservation.getTotalObservationDuration(),thisTargetSubband);
							pipelineStorage += pipelineStorageTarget;
							
							 log.info("["+optarget.getObservingrun()+","+
							 observationStorage+","+
							 thisTargetSubband+","+observationSubbands+","+
							 pipelineAveraging+","+pipelineAveragingFreq+","+
							 pipelineStorageTarget+","+pipelineStorage+", duration: "+opticonObservation.getTotalObservationDuration().doubleValue()+"]");
							 log.warn("&&observationStorage :"+observationStorage);
						}
						Double targetDuration = NorthStarDelegate.getResourceValue(optarget.getAllocations(),
								OpticonConstants.TOTAL_DURATION);

						processTimeResult = calculateProcessingTime(mode,
								opticonObservation.getInstrument().getFlaggingStrategy(),
								opticonObservation.getInstrument().getAntenna() == null ? ""
										: opticonObservation.getInstrument().getAntenna(),
								opticonObservation.getInstrument().isDemixing(),
								opticonObservation.getInstrument().getDemixingSources(), totalStation,
								thisTargetSubband,
								// targetDurationMap.get(optarget.getRunnumber())!=null?targetDurationMap.get(optarget.getRunnumber()).doubleValue():0
								// ,
								targetDuration != null ? targetDuration.doubleValue() : 0,
								opticonObservation.getInstrument().getFieldOfView() != null
										? opticonObservation.getInstrument().getFieldOfView().doubleValue() : 0,
								uvRange, baselineFactor, opticonObservation.getInstrument().getProcessingMode());

						String key = String.valueOf(processTimeResult.get(OpticonConstants.PROCESSING_TIME_RATIO));
						
						if (mode != null && mode.equals(OpticonConstants.PROCESSING_MODE_CALIBRATION_AND_IMAGING))
							key = (String) processTimeResult.get(OpticonConstants.PROCESSING_TIME_RATIO_IMAGING);
						int value = 0;
						if (key != null && poRatioList.get(key) == null)
							value = 1;
						if (poRatioList.get(key) != null) {

							value = poRatioList.get(key).intValue() + 1;
						}
						log.info("PO/Ratio value : "+value);
						poRatioList.put(key, value);
						String totalprocessingtimeValue = String
								.valueOf(processTimeResult.get(OpticonConstants.PROCESSING_TIME_TOTAL));
						if (!AstronValidator.isBlankOrNull(totalprocessingtimeValue)
								&& !totalprocessingtimeValue.equals("null"))
							totalProcessingTime += Double.parseDouble(totalprocessingtimeValue.replace(",", ""));
						// log.warn("set pipeline:"+chr+" target:"+
						// targetCount+" run:"+optarget.getObservingrun() +"
						// subb:"+observationSubbands+" av:"+pipelineAveraging+
						// " storage:"+ observationStorage+"
						// thissubb:"+thisTargetSubband);

						targetCount++;
					}
				}

				targets = Integer.toString(targetCount) + " targets";
				if (processTimeResult != null) {
					NumberFormat nf = NumberFormat.getInstance();
					nf.setMaximumFractionDigits(2);

					processTimeResult.put(OpticonConstants.PROCESSING_TIME_TOTAL, nf.format(totalProcessingTime));
				}
				if (opticonObservation.getInstrument().getAveragingTime() != null) {
					avtime = opticonObservation.getInstrument().getAveragingTime().toString();
				}
				if (opticonObservation.getInstrument().getAveragingFrequency() != null) {
					avfreq = opticonObservation.getInstrument().getAveragingFrequency().toString();
				}
				if (opticonObservation.getInstrument().isDemixing()) {
					demix = "Y";
					if (opticonObservation.getInstrument().getDemixingSources() != null
							&& opticonObservation.getInstrument().getDemixingSources().size() > 0) {
						demix += "(";
						for (int in = 0; in < opticonObservation.getInstrument().getDemixingSources().size();) {
							demix += opticonObservation.getInstrument().getDemixingSources().get(in);
							in++;
							if (in < opticonObservation.getInstrument().getDemixingSources().size())
								demix += ",";
						}
						demix += ")";
					}
				}
				
				double pipelineStorageFactor = getPipelineStorageFactor(opticonObservation.getInstrument().getProcessingMode());
				if(pipelineStorageFactor == 0){
					List pipelineStorageFactorList = new ArrayList();
					String pipelineStorageFactorValue = new String();
					pipelineStorageFactorList = OptionsUtils.getLabelValueBeans(Constants.PIPELINE_STORAGE_FACTOR,
							new HashMap(), contextConfiguration);

					if (pipelineStorageFactorList != null && pipelineStorageFactorList.size() > 1) {
						LabelValueBean urlValue = (LabelValueBean) pipelineStorageFactorList.get(1);
						pipelineStorageFactorValue = urlValue.getValue();
					}
					if (!AstronValidator.isBlankOrNull(pipelineStorageFactorValue)) {
						pipelineStorageFactor = new Double(pipelineStorageFactorValue).doubleValue();
					}
				}
				pipelineStorage = pipelineStorage * pipelineStorageFactor;
				log.warn("total storage for:" + String.valueOf(chr) + "is:" + pipelineStorage+" and pipelineStorageFactor is : "+pipelineStorageFactor);
				BigDecimal bd = new BigDecimal(pipelineStorage);
				bd = bd.setScale(2, BigDecimal.ROUND_UP);
				bd = OpticonTargetUtils.convertToTB(bd);
				dataSize += bd.doubleValue();
				// result = String.valueOf(bd.doubleValue());
				pipeStorage = String.valueOf(bd);

			}
			pipelineValues.add(new ValueBean(String.valueOf(chr)));
			pipelineValues.add(new ValueBean(targets));
			pipelineValues.add(new ValueBean(opticonObservation.getTelescopeConfiguration()));
			pipelineValues.add(new ValueBean(mode));
			pipelineValues.add(new ValueBean(avtime));
			pipelineValues.add(new ValueBean(avfreq));
			pipelineValues.add(new ValueBean(demix));
			pipelineValues.add(new ValueBean(pipeStorage));

			/**
			 * Add two new column for processing time and p/o ratio
			 */

			String EMPTY_STRING = " ";
			String ratioDescription = EMPTY_STRING;
			if (poRatioList != null) {
				for (String key : poRatioList.keySet()) {
					ratioDescription += key + "(" + poRatioList.get(key) + ")\n";
				}
			}
			if (processTimeResult != null) {

				pipelineValues.add(new ValueBean(String.valueOf(ratioDescription)));
				String sValue = String.valueOf(processTimeResult.get(OpticonConstants.PROCESSING_TIME_TOTAL)).replaceAll(",", "");
				Double pTime = Double.valueOf(sValue);
				if (pTime != null)
					totalprocessingTimeSpecified += pTime.doubleValue();
				pipelineValues.add(
						new ValueBean(String.valueOf(processTimeResult.get(OpticonConstants.PROCESSING_TIME_TOTAL))));

			} else {
				pipelineValues.add(new ValueBean(String.valueOf(EMPTY_STRING)));
				pipelineValues.add(new ValueBean(String.valueOf(EMPTY_STRING)));
			}
			pipelinecount++;
			pipelineSummaryValues.add(pipelineValues);
		}

		opticonObservingRequestForm.setPipelineSummaryValues(pipelineSummaryValues);

		if (OptionsDelegate.allowedToDisplay(OpticonConstants.SHOW_CHILDREN, fieldsDefinitionType)) {
			Proposal proposal = proposalData.getProposal();
			List children = northStarDelegate.getChildsList(proposal);

			opticonObservingRequestForm.setChildren(children);
		}

		if (OptionsDelegate.allowedToDisplay(OpticonConstants.SELECT_PARENT, fieldsDefinitionType)) {
			Proposal proposal = proposalData.getProposal();
			List parents = northStarDelegate.getParentList(proposal.getSemester());
			opticonObservingRequestForm.setParents(parents);
			opticonObservingRequestForm.setSelectedParent(AstronConverter.toString(proposal.getParentId()));
		}

		opticonObservingRequestForm
				.setTotalSpecifiedTime(AstronConverter.getHoursFromSeconds(new Integer(totalSpecifiedTime)));

		/*
		 * Fill or create instrument form if needed, FIXME needs to be more
		 * understandable
		 */
		if (telescopeConfiguration.isTargetsOnTabbedPage()) {
			if (observingRequestForm.getCommitObservationButton() != null
					|| observingRequestForm.getCommitPipelineButton() != null) {
				opticonObservingRequestForm.setShowInstrumentBean("false");
				opticonObservingRequestForm.setSelectedObservation("");
				opticonObservingRequestForm.setSelectedPipeline("");
				opticonObservingRequestForm.setDisplayCommitPipeline(false);
				String index = observingRequestForm.getSelectedObservation();
				
			} else {
				// if an observation has been selected for editing
				String selectedObservation = opticonObservingRequestForm.getSelectedObservation();
				String selectedPipeline = opticonObservingRequestForm.getSelectedPipeline();
				// the index parameter is only set when the edit observation
				// button is pressed, it overrules the selectedobservation value
				// as a new observation has been selected for editing.
				if (observingRequestForm.getEditObservationButton(0) != null) {
					if (AstronValidator.isPositiveInt(request.getParameter("index"))) {
						selectedObservation = request.getParameter("index");
					}
				} else if (observingRequestForm.getEditPipelineButton(0) != null) {
					if (AstronValidator.isPositiveInt(request.getParameter("index"))) {
						selectedObservation = "-1";// request.getParameter("index");//
													// + observations?
						selectedPipeline = request.getParameter("index");
						// selected
					}
				}

				if (AstronValidator.isPositiveInt(selectedObservation)) {
					List obs = opticonObservingRequest.getObservations();
					int index = Integer.valueOf(selectedObservation).intValue();// getCorrectIndex(selectedObservation,obs,observingRequestForm.getEditPipelineButton());//
					if (obs != null && obs.size() > index) {
						OpticonObservation opticonObservation = (OpticonObservation) obs.get(index);

						InstrumentBean instrBean = new InstrumentBean();
						instrBean.setTelescope(opticonObservation.getTelescopeConfiguration());
						instrBean.setEnableCombinedOutputProductSelection(
								observingRequestForm.isEnableCombinedOutputProductSelection());
						SetUpInstrumentBean.fillForm(request, opticonObservation, instrBean);
						// FIXME need to set defaults again as not visible
						// attributes aren't stored by processinstrumentbean
						SetUpInstrumentBean.setDefaults(contextConfiguration, instrBean);
						SetUpInstrumentBean.fillOptions(request, instrBean);

						// if(opticonObservation.getInstrument() != null &&
						// opticonObservation.getInstrument().getName() != null
						// &&
						// opticonObservation.getInstrument().getName().equalsIgnoreCase("pipeline")
						// ){
						// instrBean.setDisplayPipelineOptions(true);
						// opticonObservingRequestForm.setDisplayCommitPipeline(true);
						// }

						if (instrBean.isDisplayCalculatedSize()) {
							StorageCalculator sCalc = SetUpInstrumentBean.bindCalc(contextConfiguration);
							if (sCalc != null) {
								double calcSize = 0d;
								try {
									calcSize = SetUpInstrumentBean.calculateStorage(opticonObservation, 0, 0, sCalc);
									// dataSize += calcSize;
									BigDecimal bd = new BigDecimal(calcSize);
									bd = bd.setScale(2, BigDecimal.ROUND_UP);
									instrBean.setEstimatedDataStorage(bd.toString()); // bd.toEngineeringString()
																						// not
																						// java
																						// 1.4
																						// compatible
									instrBean.setDataStorageSpecifics(
											SetUpInstrumentBean.getDataStorageSpecifics(opticonObservation));

									double cepload = SetUpInstrumentBean.calculateCepLoad(opticonObservation,
											contextConfiguration, dataSize);
									bd = new BigDecimal(cepload);
									bd = bd.setScale(2, BigDecimal.ROUND_UP);
									instrBean.setEstimatedCepLoad(bd.toString());

								} catch (ServiceException e) {
									e.printStackTrace();
								}
							}

						}

						// this makes sure that the instrument bean is filled
						// with the data....
						request.setAttribute("instrumentBean", instrBean);

						opticonObservingRequestForm.setShowInstrumentBean("true");
						opticonObservingRequestForm.setDisplayAllocations(false);
						opticonObservingRequestForm.setSelectedObservation(selectedObservation);
					}
				}

				if (AstronValidator.isPositiveInt(selectedPipeline)) {
					List pipes = opticonObservingRequest.getPipelines();
					int index = Integer.valueOf(selectedPipeline).intValue();// getCorrectIndex(selectedObservation,obs,observingRequestForm.getEditPipelineButton());//
					if (pipes != null && pipes.size() > index) {
						OpticonObservation opticonObservation = (OpticonObservation) pipes.get(index);

						InstrumentBean instrBean = new InstrumentBean();
						instrBean.setTelescope(opticonObservation.getTelescopeConfiguration());
						instrBean.setSelectedInstrumentConfiguration("Pipeline");
						opticonObservation.getInstrument().setName("Pipeline");
						SetUpInstrumentBean.fillForm(request, opticonObservation, instrBean);
						// FIXME need to set defaults again as not visible
						// attributes aren't stored by processinstrumentbean
						SetUpInstrumentBean.setDefaults(contextConfiguration, instrBean);
						SetUpInstrumentBean.fillOptions(request, instrBean);

						request.setAttribute("instrumentBean", instrBean);

						opticonObservingRequestForm.setShowInstrumentBean("true");
						opticonObservingRequestForm.setDisplayAllocations(false);
						opticonObservingRequestForm.setSelectedPipeline(selectedPipeline);

						instrBean.setDisplayPipelineOptions(true);
						opticonObservingRequestForm.setDisplayCommitPipeline(true);
					}
				}

				// if get new observation button has been pressed
				if (opticonObservingRequestForm.getNewObservationButton() != null) {

					// ProposalData proposalData = (ProposalData)
					// request.getSession().getAttribute(Constants.PROPOSAL_DATA);
					// TelescopeConfiguration telescopeConfiguration =
					// NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());

					ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
					OpticonObservation opticonObservation;
					List observations = proposalData.getProposal().getObservingRequest().getObservations();
					try {
						opticonObservation = (OpticonObservation) configurationUtil
								.getObservation(telescopeConfiguration);
						// opticonObservation.setTelescopeConfiguration(proposalData.getProposal().getSemester().getTelescope());

						opticonObservation.setInstrument(new OpticonInstrument());
						opticonObservation.setTargets(new ArrayList());
						InstrumentBean instrBean = new InstrumentBean();
						instrBean.setEnableCombinedOutputProductSelection(
								opticonObservingRequestForm.isEnableCombinedOutputProductSelection());
						// if a new pipeline is selected...
						// if(opticonObservingRequestForm.getNewPipelineButton()
						// != null){
						// //opticonObservation.setProcessingPipeline(true); //
						// is done via a hidden field in jsp
						// instrBean.setDisplayPipelineOptions(true);
						// set pipeline commit button
						// opticonObservingRequestForm.setProcessingPipeline(true);
						// opticonObservingRequestForm.setDisplayCommitPipeline(true);
						// opticonObservation.getInstrument().setName("Pipeline");
						// instrBean.setSelectedInstrumentConfiguration("Pipeline");
						// }else{
						opticonObservation.getInstrument().setName("");
						instrBean.setSelectedInstrumentConfiguration("");
						// }

						// fill standard options

						SetUpInstrumentBean.fillOptions(request, instrBean);

						SetUpInstrumentBean.setDefaults(contextConfiguration, instrBean);

						observations.add(opticonObservation);
						// this makes sure that the instrument bean is filled
						// with the data....
						request.setAttribute("instrumentBean", instrBean);
					} catch (InvalidConfigurationException e) {

						e.printStackTrace();
					}
					opticonObservingRequest.setObservations(observations);
					String index = new Integer(observations.size() - 1).toString();
					opticonObservingRequestForm.setSelectedObservation(index);
					opticonObservingRequestForm.setSelectedPipeline("");
					opticonObservingRequestForm.setShowInstrumentBean("true");

				}

				// if get new pipeline button has been pressed
				if (opticonObservingRequestForm.getNewPipelineButton() != null) {
					// ProposalData proposalData = (ProposalData)
					// request.getSession().getAttribute(Constants.PROPOSAL_DATA);
					// TelescopeConfiguration telescopeConfiguration =
					// NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());

					ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
					OpticonObservation opticonObservation;
					List pipelines = proposalData.getProposal().getObservingRequest().getPipelines();
					try {
						opticonObservation = (OpticonObservation) configurationUtil
								.getObservation(telescopeConfiguration);
						// opticonObservation.setTelescopeConfiguration(proposalData.getProposal().getSemester().getTelescope());

						opticonObservation.setInstrument(new OpticonInstrument());
						opticonObservation.setTargets(new ArrayList());
						InstrumentBean instrBean = new InstrumentBean();

						// if a new pipeline is selected...
						// opticonObservation.setProcessingPipeline(true); // is
						// done via a hidden field in jsp
						instrBean.setDisplayPipelineOptions(true);
						// set pipeline commit button
						opticonObservingRequestForm.setProcessingPipeline(true);
						opticonObservingRequestForm.setDisplayCommitPipeline(true);
						opticonObservation.getInstrument().setName("Pipeline");
						opticonObservation.setObservationKind("pipeline");
						instrBean.setSelectedInstrumentConfiguration("Pipeline");

						// fill standard options
						SetUpInstrumentBean.fillOptions(request, instrBean);
						// SetUpInstrumentBean.fillForm(request,
						// opticonObservation, instrForm);
						SetUpInstrumentBean.setDefaults(contextConfiguration, instrBean);

						pipelines.add(opticonObservation);
						// this makes sure that the instrument bean is filled
						// with the data....
						request.setAttribute("instrumentBean", instrBean);
					} catch (InvalidConfigurationException e) {

						e.printStackTrace();
					}
					opticonObservingRequest.setPipelines(pipelines);
					String index = new Integer(pipelines.size() - 1).toString();
					opticonObservingRequestForm.setSelectedPipeline(index);
					opticonObservingRequestForm.setSelectedObservation("");
					opticonObservingRequestForm.setShowInstrumentBean("true");
				}

			}

		}

		BigDecimal bd = new BigDecimal(dataSize);
		bd = bd.setScale(2, BigDecimal.ROUND_UP);
		log.warn("total DATA SIZE final:" + dataSize);
		opticonObservingRequestForm.setSpecifiedStorage(bd.toString()); // toEngineeringString());
																		// not
																		// java1.4
																		// compatible
		bd = new BigDecimal(cepLoad);
		bd = bd.setScale(0, BigDecimal.ROUND_UP);
		opticonObservingRequestForm.setSpecifiedCepLoad(bd.toString());
		opticonObservingRequestForm.setSpecifiedProcessingTime(String.valueOf(totalprocessingTimeSpecified));
		/*
		 * Initializing the form fields with information from the corresponding
		 * ObservingRequest data object properties.
		 */
		if (getErrors(request).size() == 0) {
			opticonObservingRequestForm.setLongTermProposal(opticonObservingRequest.isLongTermProposal());
			opticonObservingRequestForm
					.setLongTermProposalSpecifics(opticonObservingRequest.getLongTermProposalSpecifics());

			opticonObservingRequestForm.setScienceCategories(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.SCIENCE_CATEGORIES, new HashMap(), contextConfiguration));
			opticonObservingRequestForm.setCategory(opticonObservingRequest.getCategory());

			if (opticonObservingRequest.getObservingMode() != null) {
				opticonObservingRequestForm.setSelectedObservingMode(opticonObservingRequest.getObservingMode());
			} else {
				opticonObservingRequestForm.setSelectedObservingMode(OptionsUtils
						.getDefaultSelectedOptionOrValue(OpticonConstants.OBSERVING_MODE, null, contextConfiguration));
			}
			opticonObservingRequestForm
					.setNoiseLevel(AstronConverter.toString(opticonObservingRequest.getNoiseLevel()));
			opticonObservingRequestForm.setNightTime(opticonObservingRequest.isNightTime());
			opticonObservingRequestForm.setLargeProposal(opticonObservingRequest.isLargeProposal());
			opticonObservingRequestForm
					.setAllocationJustification(opticonObservingRequest.getAllocationJustification());
			// opticonObservingRequestForm.setNewObserverExperience(opticonObservingRequest.getNewObserverExperience());
			// opticonObservingRequestForm.setBackupStrategy(opticonObservingRequest.getBackupStrategy());
			opticonObservingRequestForm.setTravel(opticonObservingRequest.getTravel());
			opticonObservingRequestForm.setOtherExpenditure(opticonObservingRequest.getOtherExpenditure());
			opticonObservingRequestForm
					.setProprietaryExceptionSpecifics(opticonObservingRequest.getProprietaryExceptionSpecifics());
			opticonObservingRequestForm.setEnableProprietaryException(opticonObservingRequest.isProprietaryException());
			// opticonObservingRequestForm.setDisplayUsefulTime(opticonObservingRequest.isUsefulTime());
			updateAllocationBeans(opticonObservingRequestForm, opticonObservingRequest);
		}

		List urlList = new ArrayList();
		String Url = new String();
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_OBSERVATIONS, new HashMap(), contextConfiguration);

		if (urlList != null && urlList.size() > 1) {
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url = urlValue.getValue();
			if (proposalData.getProposal().getObservingRequest().getObservations().size() >= AstronConverter
					.toInteger(Url).intValue()) {
				observingRequestForm.setMaxNumberOfObservationsReached(true);
			}
		}
		// do not show these values when editing an instrument FIXME should not
		// be done here
		if (opticonObservingRequestForm.getShowInstrumentBean() != null
				&& opticonObservingRequestForm.getShowInstrumentBean().equals("true")) {
			opticonObservingRequestForm.setDisplayScienceCategory(false);
			opticonObservingRequestForm.setDisplayLongTermProposal(false);
			opticonObservingRequestForm.setDisplaySelectParent(false);
			opticonObservingRequestForm.setDisplayObservingDetails(false);
			opticonObservingRequestForm.setObservationSummaryValues(new ArrayList());
			opticonObservingRequestForm.setPipelineSummaryValues(new ArrayList());
			opticonObservingRequestForm.setDisplayAllocations(false);
			opticonObservingRequestForm.setDisplayNightTime(false);
		}
		/*
		 * Fill the form with generic information from the telescope independant
		 * part.
		 */
		super.fillForm(request, observingRequest, observingRequestForm);

	}

	private double getPipelineStorageFactor(String processingMode) {
		double pipelineStorageFactor = 0;
		if(processingMode != null && !processingMode.equalsIgnoreCase("")){
			try {
				File fXmlFile = new File(contextPath+OpticonConstants.CONFIG_FILE_LOCATION);
				if(fXmlFile.exists()){
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc = dBuilder.parse(fXmlFile);
					doc.getDocumentElement().normalize();
					
					NodeList nListPipelineStorageFactor = doc.getElementsByTagName("PipelineStorageFactor");
					
					for (int temp = 0; temp < nListPipelineStorageFactor.getLength(); temp++) {
						Node nNode = nListPipelineStorageFactor.item(temp);
						if (nNode.getNodeType() == Node.ELEMENT_NODE) {
							Element eElement = (Element) nNode;
							double factor = eElement.getTextContent()!= null ? Double.parseDouble(eElement.getTextContent()) : 0;
							String processingModeStr = eElement.getAttribute("processingMode") != null ? eElement.getAttribute("processingMode") : "";
							String[] processingModeArray = processingModeStr.split(",");
							for (String finalProcessingMode : processingModeArray) {
								if(processingMode.equalsIgnoreCase(finalProcessingMode.trim())){
									pipelineStorageFactor = factor;
								}
							}
						}
					}
				}
			} catch (RemoteException e1) {
				log.error(e1.getMessage());
			} catch (ParserConfigurationException e1) {
				log.error(e1.getMessage());
			} catch (SAXException e1) {
				log.error(e1.getMessage());
			} catch (IOException e1) {
				log.error(e1.getMessage());
			}
		}
		return pipelineStorageFactor;
	}

	private HashMap calculateBFAndUVSizePerTarget(OpticonObservation opticonObservation, List targetDurationMap,
			List targetSubbandMap, List targetThisObservation) {
		// TODO Auto-generated method stub
		log.info("calculateBFAndUVSizePerTarget[ targetDurationMap:"+targetDurationMap.toString()
		+" targetSubbandMap:"+targetSubbandMap+" targetThisObservation:"+targetThisObservation.toString());
		HashMap finalResult = new HashMap();
		BigDecimal beam = new BigDecimal(0);
		BigDecimal uv = new BigDecimal(0);
		BigDecimal tbb = new BigDecimal(0);
		BigDecimal total = new BigDecimal(0);
		int i = 0;
		while (i < targetThisObservation.size()) {
			double totalDuration = (Double) targetDurationMap.get(i);
			int subbands = (Integer) targetSubbandMap.get(i);
			HashMap perTargetDataSize = calculateBFAndUVSize(opticonObservation, totalDuration, subbands);
			if (perTargetDataSize != null) {
				BigDecimal thisBeam = (BigDecimal) perTargetDataSize.get(OpticonConstants.BEAM_DATA_SIZE);
				if (thisBeam != null)
					beam = beam.add(thisBeam);
				BigDecimal thisUv = (BigDecimal) perTargetDataSize
						.get(OpticonConstants.CORRELATED_VISIBILITY_DATA_SIZE);
				if (thisUv != null)
					uv = uv.add(thisUv);

				BigDecimal thisTbb = (BigDecimal) perTargetDataSize.get(OpticonConstants.TBB_DATA_SIZE);
				if (thisTbb != null)
					tbb = tbb.add(thisTbb);

				BigDecimal thistotal = (BigDecimal) perTargetDataSize.get(OpticonConstants.TOTAL_DATA_SIZE);
				if (thistotal != null)
					total = total.add(thistotal);

				finalResult.put(OpticonConstants.DATA_STORE_ALLOW,
						perTargetDataSize.get(OpticonConstants.DATA_STORE_ALLOW));
				finalResult.put(OpticonConstants.DATA_STORE_MODE,
						perTargetDataSize.get(OpticonConstants.DATA_STORE_MODE));
				finalResult.put(OpticonConstants.UV_DATA_STORE_ALLOWE,
						perTargetDataSize.get(OpticonConstants.UV_DATA_STORE_ALLOWE));
			}
			i++;
		}

		finalResult.put(OpticonConstants.BEAM_DATA_SIZE, beam);
		finalResult.put(OpticonConstants.CORRELATED_VISIBILITY_DATA_SIZE, uv);
		finalResult.put(OpticonConstants.TBB_DATA_SIZE, tbb);
		finalResult.put(OpticonConstants.TOTAL_DATA_SIZE, total);

		return finalResult;
	}

	public HashMap calculateProcessingTime(String mode, String flaggingStrategy, String antenna, boolean isDemixing,
			List<String> demixingSources, int totalStation, int totalTargetSubband, double totalObservingDuration,
			double fieldOfView, double uvRange, double baselineFactor, String processingMode) {

		HashMap result = new HashMap();

		if (sCalc == null) {
			log.info("binding calculator");
			sCalc = SetUpInstrumentBean.bindCalc(contextConfiguration);
		}
		if (sCalc != null) {
			try {
				result = SetUpInstrumentBean.calculateProcessingTime(mode, flaggingStrategy, antenna, isDemixing,
						demixingSources, totalStation, totalTargetSubband, totalObservingDuration, fieldOfView, uvRange,
						sCalc, baselineFactor, contextPath, processingMode);
			} catch (ServiceException e) {
				log.warn(e.getMessage());
				// throw new InvalidConfigurationException(e);
			}
		} else {
			log.warn("could not bind calculator");
		}

		return result;

	}

	private double calculateStorage(OpticonObservation opticonObservation, Double totalDuration, int subbands) {

		double result = 0;
		if (sCalc == null) {
			log.info("binding calculator");
			sCalc = SetUpInstrumentBean.bindCalc(contextConfiguration);
		}
		if (sCalc != null) {
			try {
				result = SetUpInstrumentBean
						.calculateStorage(opticonObservation, totalDuration.doubleValue(), subbands, sCalc)
						.doubleValue();
				// if (storage > 0){
				// BigDecimal bd = new BigDecimal(storage);
				// bd = bd.setScale(2,BigDecimal.ROUND_UP);
				// result = String.valueOf(bd.doubleValue());
				// }
				// dataSize += storage;
				// double cep =
				// SetUpInstrumentBean.calculateCepLoad(opticonObservation,
				// contextConfiguration, storage);
				// cepLoad += cep;
			} catch (ServiceException e) {
				log.warn(e.getMessage());
				// throw new InvalidConfigurationException(e);
			}
		} else {
			log.warn("could not bind calculator");
		}

		return result;
	}

	// two separate methods to calculate

	public HashMap calculateBFAndUVSize(OpticonObservation opticonObservation, Double totalDuration, int subbands) {

		HashMap result = new HashMap();
		if (totalDuration == null)
			totalDuration = 0d;
		if (sCalc == null) {
			log.warn("binding calculator for bf and uv size");
			sCalc = SetUpInstrumentBean.bindCalc(contextConfiguration);
		}
		if (sCalc != null) {
			try {
				result = SetUpInstrumentBean.calculateBFAndUVSize(opticonObservation, totalDuration.doubleValue(),
						subbands, sCalc);

			} catch (ServiceException e) {
				log.warn(e.getMessage());
				// throw new InvalidConfigurationException(e);
			}
		} else {
			log.warn("could not bind calculator");
		}

		return result;
	}

	protected void updateAllocationBeans(OpticonObservingRequestForm opticonObservingRequestForm,
			OpticonObservingRequest opticonObservingRequest) {
		opticonObservingRequestForm.getAllocationBeans().clear();
		Set telescopes = new HashSet();
		Iterator observationsIt = opticonObservingRequest.getObservations().iterator();
		while (observationsIt.hasNext()) {
			OpticonObservation observation = (OpticonObservation) observationsIt.next();
			if (!AstronValidator.isBlankOrNull(observation.getTelescopeConfiguration())) {
				telescopes.add(observation.getTelescopeConfiguration());
			}
		}
		// if only one telescope is specified, just show it
		if (telescopes.size() == 0) {
			List telescopeConfigurations = OptionsUtils.getLabelValueBeans(OpticonConstants.TELESCOPE_CONFIGURATIONS,
					new HashMap(), contextConfiguration);
			if (telescopeConfigurations.size() == 2) {
				opticonObservingRequestForm.setDisplayUseResources(true);
				opticonObservingRequestForm.setMultipleTelescopes(true);
				LabelValueBean lvb = (LabelValueBean) telescopeConfigurations.get(1);
				telescopes.add(lvb.getLabel());
			}
		}

		observationsIt = opticonObservingRequest.getObservations().iterator();
		double totalDuration = 0d;

		for (Target target : opticonObservingRequest.getTargets()) {
			OpticonTarget optarget = (OpticonTarget) target;
			// totalDuration = totalDuration + target.setAllocations( )
			Double targetDuration = NorthStarDelegate.getResourceValue(target.getAllocations(),
					OpticonConstants.TOTAL_DURATION);
			if (targetDuration != null && optarget.isFirstInRun()) {
				totalDuration += targetDuration.doubleValue();
			}
		}
		while (observationsIt.hasNext()) {
			OpticonObservation opticonObservation = (OpticonObservation) observationsIt.next();

			if (opticonObservation.getInstrument().getName().startsWith("TBB")
					&& opticonObservation.getInstrument().getTbbExposureTime() != null) {
				Double tbbDuration = opticonObservation.getInstrument().getTbbExposureTime();
				tbbDuration = tbbDuration * 3600;
				totalDuration += tbbDuration;
			}
		}

		String obsGlobalTime = OpticonTargetUtils.getHoursFromSeconds(Double.valueOf(totalDuration));

		Iterator telescopesIt = telescopes.iterator();
		while (telescopesIt.hasNext()) {

			String telescopeName = (String) telescopesIt.next();
			AllocationBean bean = fillBean(telescopeName, opticonObservingRequest);
			bean.setObsGlobalTime(obsGlobalTime);
			opticonObservingRequestForm.getAllocationBeans().add(bean);
		}
		/*
		 * TODO: iterate again? To fix this we need a complete rewrite to merge
		 * both moonphase times in the bean. The method call below needs to be
		 * done after the beans have been initialized (as has happened above)
		 */
		if (opticonObservingRequest.getPipelines() != null) {
			Iterator pipelinesIt = opticonObservingRequest.getPipelines().iterator();
			while (pipelinesIt.hasNext()) {
				OpticonObservation pipeline = (OpticonObservation) pipelinesIt.next();

			}
		}

		/*
		 * if(OptionsDelegate.allowedToDisplay(OpticonConstants.
		 * SUMMARY_TOTALTIME, contextConfiguration.getFieldsDefinitionType())){
		 * // is actually for !targetsontabbedpage because the observations are
		 * being iterated here and not the observingrequest. Double
		 * totalDuration = opticonObservation.getTotalObservationDuration(); if
		 * (totalDuration != null) { totalSpecifiedTime +=
		 * totalDuration.doubleValue(); } }else{
		 * 
		 * updateObservationAllocations(opticonObservation ,
		 * opticonObservingRequest, opticonObservingRequestForm); }
		 * 
		 * // is actually for !targetsontabbedpage
		 * if(OptionsDelegate.allowedToDisplay(OpticonConstants.
		 * SUMMARY_TOTALTIME, contextConfiguration.getFieldsDefinitionType())){
		 * telescopesIt = telescopes.iterator(); while (telescopesIt.hasNext())
		 * { String telescopeName = (String) telescopesIt.next();
		 * updateObservationAllocationBeans(opticonObservingRequestForm,
		 * telescopeName,"", new Double(totalSpecifiedTime)); } }
		 */
		// determine which time modes are visible for entering values
		List enabledTimes = OptionsUtils.getLabelValueBeansWithoutNoneSpecified(OpticonConstants.OBSERVATION_TIME,
				new HashMap(), contextConfiguration);
		Iterator observationTimesIt = enabledTimes.iterator();
		while (observationTimesIt.hasNext()) {
			LabelValueBean lvb = (LabelValueBean) observationTimesIt.next();
			if (lvb.getValue().startsWith("Global")) {
				opticonObservingRequestForm.setDisplayGlobal(true);
			}
			if (lvb.getValue().startsWith("Dark")) {
				opticonObservingRequestForm.setDisplayDark(true);
			}
			if (lvb.getValue().startsWith("Grey")) {
				opticonObservingRequestForm.setDisplayGrey(true);
			}
			if (lvb.getValue().startsWith("Bright")) {
				opticonObservingRequestForm.setDisplayBright(true);
			}
		}

	}

	protected AllocationBean fillBean(String telescopeName, OpticonObservingRequest opticonObservingRequest) {
		AllocationBean bean = new AllocationBean();
		bean.setTelescope(telescopeName);
		//
		bean.setBrightTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.BRIGHT)));
		bean.setDarkTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.DARK)));
		bean.setGreyTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.GREY)));
		/*  */
		bean.setFirstQTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.FIRST_Q)));
		bean.setLastQTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.LAST_Q)));

		bean.setGlobalTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.GLOBAL)));

		bean.setAwardedTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.AWARDED)));

		bean.setLongTermTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.LONGTERM)));

		bean.setAwardedBrightTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.AWARDEDBRIGHT)));
		bean.setLongTermBrightTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.LONGTERMBRIGHT)));
		bean.setUsefulBrightTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.USEFULBRIGHT)));

		bean.setAwardedGreyTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.AWARDEDGREY)));
		bean.setLongTermGreyTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.LONGTERMGREY)));
		bean.setUsefulGreyTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.USEFULGREY)));

		bean.setAwardedDarkTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.AWARDEDDARK)));
		bean.setLongTermDarkTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.LONGTERMDARK)));
		bean.setUsefulDarkTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.USEFULDARK)));

		bean.setUsefulGlobalTime(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.USEFULGLOBAL)));
		bean.setLongTermStorage(AstronConverter.toString(NorthStarDelegate.getResourceValue(
				opticonObservingRequest.getAllocations(), telescopeName + '_' + OpticonConstants.LONGTERMSTORAGE)));

		bean.setTotalProcessingTime(
				AstronConverter.toString(NorthStarDelegate.getResourceValue(opticonObservingRequest.getAllocations(),
						telescopeName + '_' + OpticonConstants.TOTAL_PROCESSING_TIME)));
		return bean;

	}

	/*
	 * For the allocations a label is needed. The label is the telescop
	 * configuration with the whitespaces removed and all the characters to
	 * upper case.
	 */
	protected String createLabel(String input) {
		String output = input.replaceAll("\\s", "");
		return output.toUpperCase();
	}

	protected void updateObservationAllocations(OpticonObservation opticonObservation,
			OpticonObservingRequest opticonObservingRequest, OpticonObservingRequestForm opticonObervingRequestForm) {
		if (!AstronValidator.isBlankOrNull(opticonObservation.getTelescopeConfiguration())) {
			// remove empty targets....
			List targets = opticonObservingRequest.getTargets();
			for (int i = 0; i < targets.size(); i++) {
				while (targets.get(i) == null) {
					targets.remove(i);
				}
			}
			opticonObservingRequest.setTargets(targets);

			Iterator targetsIt = targets.iterator();
			while (targetsIt.hasNext()) {
				OpticonTarget opticonTarget = (OpticonTarget) targetsIt.next();

				if (!AstronValidator.isBlankOrNull(opticonObservation.getObservationPhase())) {
					Double exposureTime = NorthStarDelegate.getResourceValue(opticonTarget.getAllocations(),
							OpticonConstants.TOTAL_DURATION);
					if (exposureTime != null) {
						updateObservationAllocationBeans(opticonObervingRequestForm,
								opticonObservation.getTelescopeConfiguration(),
								opticonObservation.getObservationPhase(), exposureTime);
					}
				}
				// only if observationTime is blank

				if (AstronValidator.isBlankOrNull(opticonObservation.getObservationPhase())
						&& !AstronValidator.isBlankOrNull(opticonTarget.getMoon())) {
					Double exposureTime = NorthStarDelegate.getResourceValue(opticonTarget.getAllocations(),
							OpticonConstants.TOTAL_DURATION);
					if (exposureTime != null) {
						updateObservationAllocationBeans(opticonObervingRequestForm,
								opticonObservation.getTelescopeConfiguration(), opticonTarget.getMoon(), exposureTime);
					}
				}
			}
		}
	}

	protected void updateObservationAllocationBeans(OpticonObservingRequestForm opticonObservingRequestForm,
			String telescope, String moonPhase, Double exposureTime) {
		Iterator allocationBeansIt = opticonObservingRequestForm.getAllocationBeans().iterator();
		String exposureTimeHours = OpticonTargetUtils.getHoursFromSeconds(exposureTime);
		double exposureTimeHoursAsDouble = AstronConverter.toDouble(exposureTimeHours).doubleValue();
		boolean found = false;
		while (allocationBeansIt.hasNext() && !found) {
			AllocationBean allocationBean = (AllocationBean) allocationBeansIt.next();
			if (allocationBean.getTelescope().equals(telescope)) {
				found = true;
				if (moonPhase.equals(OpticonConstants.TARGET_FIRST_Q)) {
					if (AstronValidator.isBlankOrNull(allocationBean.getObsFirstQTime())) {
						allocationBean.setObsFirstQTime(exposureTimeHours);
					} else {
						Double newTime = new Double(exposureTimeHoursAsDouble
								+ AstronConverter.toDouble(allocationBean.getObsFirstQTime()).doubleValue());
						allocationBean.setObsFirstQTime(AstronConverter.toString(newTime));
					}
				} else if (moonPhase.startsWith(OpticonConstants.DARK)) {
					if (AstronValidator.isBlankOrNull(allocationBean.getObsDarkTime())) {
						allocationBean.setObsDarkTime(exposureTimeHours);
					} else {
						Double newTime = new Double(exposureTimeHoursAsDouble
								+ AstronConverter.toDouble(allocationBean.getObsDarkTime()).doubleValue());
						allocationBean.setObsDarkTime(AstronConverter.toString(newTime));
					}
				} else if (moonPhase.equals(OpticonConstants.TARGET_LAST_Q)) {
					if (AstronValidator.isBlankOrNull(allocationBean.getObsLastQTime())) {
						allocationBean.setObsLastQTime(exposureTimeHours);
					} else {
						Double newTime = new Double(exposureTimeHoursAsDouble
								+ AstronConverter.toDouble(allocationBean.getObsLastQTime()).doubleValue());
						allocationBean.setObsLastQTime(AstronConverter.toString(newTime));
					}
				} else if (moonPhase.startsWith(OpticonConstants.BRIGHT)) {
					if (AstronValidator.isBlankOrNull(allocationBean.getObsBrightTime())) {
						allocationBean.setObsBrightTime(exposureTimeHours);
					} else {
						Double newTime = new Double(exposureTimeHoursAsDouble
								+ AstronConverter.toDouble(allocationBean.getObsBrightTime()).doubleValue());
						allocationBean.setObsBrightTime(AstronConverter.toString(newTime));
					}
				} else if (moonPhase.startsWith(OpticonConstants.GREY)) {
					if (AstronValidator.isBlankOrNull(allocationBean.getObsGreyTime())) {
						allocationBean.setObsGreyTime(exposureTimeHours);
					} else {
						Double newTime = new Double(exposureTimeHoursAsDouble
								+ AstronConverter.toDouble(allocationBean.getObsGreyTime()).doubleValue());
						allocationBean.setObsGreyTime(AstronConverter.toString(newTime));
					}
				}

				if (AstronValidator.isBlankOrNull(allocationBean.getObsGlobalTime())) {
					allocationBean.setObsGlobalTime(exposureTimeHours);
				} else {
					// EB not working....
					// Double newTime = new Double(exposureTimeMinutesAsDouble +
					// AstronConverter.toDouble(allocationBean.getObsGlobalTime()).doubleValue());
					// allocationBean.setObsGlobalTime(AstronConverter.toString(newTime));
				}

			}
		}
		if (!found) {
			/*
			 * SHOULD NOT HAPPEN AllocationBean allocationBean = new
			 * AllocationBean(); allocationBean.setTelescope(telescope); if
			 * (moonPhase.equals(OpticonConstants.FIRST_Q)) {
			 * allocationBean.setFirstQTime(AstronConverter.toString(
			 * exposureTime)); } else if
			 * (moonPhase.equals(OpticonConstants.DARK)) {
			 * allocationBean.setDarkTime(AstronConverter.toString(exposureTime)
			 * ); } else if (moonPhase.equals(OpticonConstants.LAST_Q)) {
			 * allocationBean.setLastQTime(AstronConverter.toString(exposureTime
			 * )); } else if (moonPhase.equals(OpticonConstants.BRIGHT)) {
			 * allocationBean.setBrightTime(AstronConverter.toString(
			 * exposureTime)); }
			 * opticonObservingRequestForm.getObservationAllocations().add(
			 * allocationBean);
			 */

		}
	}

}
