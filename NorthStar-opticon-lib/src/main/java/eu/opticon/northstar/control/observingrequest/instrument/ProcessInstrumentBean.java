// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest.instrument;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import eu.opticon.northstar.business.OpticonConstants;
import eu.opticon.northstar.control.observingrequest.instrument.InstrumentBean;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.data.entities.ResourceType;

public class ProcessInstrumentBean {

	public static void processForm(HttpServletRequest request,OpticonObservation opticonObservation){
		OpticonInstrument instrument = opticonObservation.getInstrument(); 
		String value = null; 
		String instrumentName = "";
		Map params = request.getParameterMap();
		boolean newInstrument=false;
		//instrument.setName((String) params.get("selectedInstrumentConfiguration"));
		//instrument.setAntenna((String) params.get("selectedAntenna"));
		value  = request.getParameter("selectedTelescopeConfiguration");
		if (value != null){
			opticonObservation.setTelescopeConfiguration(value);
		}
		
		
		value  = request.getParameter("selectedInstrumentConfiguration");
		if (value != null && !value.equalsIgnoreCase(instrument.getName())){
			// clear form if a new instrument has been selected
			instrument = new OpticonInstrument(value);
			
		}else{
		

			value  = request.getParameter("enablePiggyBack");
			if (value != null && value.equalsIgnoreCase("on")){
					instrumentName =OpticonConstants.TBB_PIGGYBACK;
					instrument.setPiggyBack(true);
					instrument.setBeamConf(false);
					instrument.setInterferoConf(false);
					instrument.setOtherConf(false);
					instrument.setTbbConf(false);
					instrument.getSelectedInstConfs().clear();
					instrument.getSelectedInstConfs().add(instrumentName);
					if(instrument.getDeSelectedInstConfs().contains(instrumentName))
						instrument.getDeSelectedInstConfs().remove(instrumentName);	
				}
			else{
					instrument.setPiggyBack(false);
					if(!instrument.getDeSelectedInstConfs().contains(OpticonConstants.TBB_PIGGYBACK))
						instrument.getDeSelectedInstConfs().add(OpticonConstants.TBB_PIGGYBACK);	
					if(instrument.getSelectedInstConfs().contains(OpticonConstants.TBB_PIGGYBACK))
						instrument.getSelectedInstConfs().remove(OpticonConstants.TBB_PIGGYBACK);			
				}
			
			
			value  = request.getParameter("beamConf");
			if (value != null && value.equalsIgnoreCase("on")){
					instrumentName +="Beam Observation";
					instrument.setBeamConf(true);
					if(!instrument.getSelectedInstConfs().contains("Beam Observation"))
					instrument.getSelectedInstConfs().add("Beam Observation");
					if(instrument.getDeSelectedInstConfs().contains("Beam Observation"))
						instrument.getDeSelectedInstConfs().remove("Beam Observation");	
				}
			else{
					instrument.setBeamConf(false);
					if(!instrument.getDeSelectedInstConfs().contains("Beam Observation"))
						instrument.getDeSelectedInstConfs().add("Beam Observation");	
					if(instrument.getSelectedInstConfs().contains("Beam Observation"))
						instrument.getSelectedInstConfs().remove("Beam Observation");			
				}
			
			
			value  = request.getParameter("interferoConf");
			if (value != null && value.equalsIgnoreCase("on")){
					if(instrumentName.length()>0)
						instrumentName+="-";
					instrumentName+="Interferometer";
					instrument.setInterferoConf(true);
					if(!instrument.getSelectedInstConfs().contains("Interferometer"))
					instrument.getSelectedInstConfs().add("Interferometer");
					if(instrument.getDeSelectedInstConfs().contains("Interferometer"))
						instrument.getDeSelectedInstConfs().remove("Interferometer");
				}else{
					instrument.setInterferoConf(false);
					if(!instrument.getDeSelectedInstConfs().contains("Interferometer"))
						instrument.getDeSelectedInstConfs().add("Interferometer");
					if(instrument.getSelectedInstConfs().contains("Interferometer"))
						instrument.getSelectedInstConfs().remove("Interferometer");
				}
			
			
			value  = request.getParameter("tbbConf");
			if (value != null && value.equalsIgnoreCase("on")){
					instrument.setTbbConf(true);
					if(instrumentName.length()>0)
						instrumentName+="-";
					instrumentName+="TBB";
					if(!instrument.getSelectedInstConfs().contains("TBB"))
					instrument.getSelectedInstConfs().add("TBB");
					if(instrument.getDeSelectedInstConfs().contains("TBB"))
						instrument.getDeSelectedInstConfs().remove("TBB");
					
				}else{
					instrument.setTbbConf(false);
					if(!instrument.getDeSelectedInstConfs().contains("TBB"))
						instrument.getDeSelectedInstConfs().add("TBB");
					if(instrument.getSelectedInstConfs().contains("TBB"))
						instrument.getSelectedInstConfs().remove("TBB");
				}
					
			value  = request.getParameter("otherConf");
			if (value != null && value.equalsIgnoreCase("on")){
					instrument.setOtherConf(true);
					if(instrumentName.length()>0)
						instrumentName+="-";
					instrumentName+="Other";
					if(!instrument.getSelectedInstConfs().contains("Other"))
					instrument.getSelectedInstConfs().add("Other");
					if(instrument.getDeSelectedInstConfs().contains("Other"))
						instrument.getDeSelectedInstConfs().remove("Other");
				}else{
					instrument.setOtherConf(false);
					if(!instrument.getDeSelectedInstConfs().contains("Other"))
						instrument.getDeSelectedInstConfs().add("Other");
					if(instrument.getSelectedInstConfs().contains("Other"))
						instrument.getSelectedInstConfs().remove("Other");
				}
			
				instrument.setName(instrumentName);	
			
			value  = request.getParameter("selectedAntenna");
			if (value != null){
				instrument.setAntenna(value);
			}
			
			
			value  = request.getParameter("selectedProcessingMode");
			if (value != null){
				instrument.setProcessingMode(value);
			}
			
			value  = request.getParameter("subbandsPerImage");
			if (value != null){
				try{
					instrument.setSubbandsPerImage(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("fieldOfView");
			if (value != null){
				try{
					instrument.setFieldOfView(new Double (value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("selectedFlaggingStrategy");
			if (value != null){
				instrument.setFlaggingStrategy(value);
			}
			
			value  = request.getParameter("averagingFrequency");
			if (value != null){
				try{
					instrument.setAveragingFrequency(new Double(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("averagingTime");
			if (value != null){
				try{
					instrument.setAveragingTime(new Double (value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
						
			value  = request.getParameter("demixing");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setDemixing(true);
				}else{
					instrument.setDemixing(false);
				}
			}
			
			value  = request.getParameter("demixingFrequency");
			if (value != null){
				try{
					instrument.setDemixingFrequency(new Double(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("demixingTime");
			if (value != null){
				try{
					instrument.setDemixingTime(new Double (value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("selectedDemixingSources");
			if (value != null){
				if(params.get("selectedDemixingSources") instanceof String[]){
					String[] values =  (String[]) params.get("selectedDemixingSources") ;
					List lst = new ArrayList();
					for(int i=0;i< values.length;i++ ){
						lst.add(values[i]);
					}
					instrument.setDemixingSources(lst);
				}
			}else{
				if(instrument.getDemixingSources().size() > 0){
					instrument.setDemixingSources(new ArrayList());
				}
			}	
			
			value  = request.getParameter("averaging");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setAveraging(true);
				}else{
					instrument.setAveraging(false);
				}
			}
			
			value  = request.getParameter("calibration");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setCalibration(true);
				}else{
					instrument.setCalibration(false);
				}
			}
			
			value  = request.getParameter("summaryPlots");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setSummaryPlots(true);
				}else{
					instrument.setSummaryPlots(false);
				}
			}
			
			value  = request.getParameter("selectedClock");
			if (value != null){
				instrument.setClock(value);
			}
			
			value  = request.getParameter("coronagraphicMask");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setCoronagraphicMask(true);
				}else{
					instrument.setCoronagraphicMask(false);
				}
			}
			
			value  = request.getParameter("comments");
			if (value != null){
				instrument.setComments(value);
			}	
			
			value  = request.getParameter("customStationSpecifics");
			if (value != null){
				instrument.setCustomStationSpecifics(value);
			}
			
			value  = request.getParameter("fieldSizeX");
			if (value != null){
				try{
					instrument.setFieldSizeX(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("fieldSizeY");
			if (value != null){
				try{
					instrument.setFieldSizeY(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("filterBandwidth");
			if (value != null){
					try{
					instrument.setFilterBandwidth(new Double(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("filterContiguousCoverage");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setFilterContiguousCoverage(true);
				}else{
					instrument.setFilterContiguousCoverage(false);
				}
			}
			
			value  = request.getParameter("filterDetails");
			if (value != null){
				instrument.setFilterDetails(value);
			}
			
			value  = request.getParameter("filterFrequency");
			if (value != null){
				try{
					instrument.setFilterFrequency(new Double(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			/*
			 * this is the routine to get selection arrays
			 */
			value  = request.getParameter("selectedFilters");
			if (value != null){
				if(params.get("selectedFilters") instanceof String[]){
					String[] values =  (String[]) params.get("selectedFilters") ;
					List lst = new ArrayList();
					for(int i=0;i< values.length;i++ ){
						lst.add(values[i]);
					}
					instrument.setFilters(lst);
				}
			}else{
				if(instrument.getFilters().size() > 0){
					instrument.setFilters(new ArrayList());
				}
			}
			
			value  = request.getParameter("selectedFiltersw2");
			if (value != null){
				if(params.get("selectedFiltersw2") instanceof String[]){
					String[] values =  (String[]) params.get("selectedFiltersw2") ;
					List lst = new ArrayList();
					for(int i=0;i< values.length;i++ ){
						lst.add(values[i]);
					}
					instrument.setFiltersw2(lst);
				}
			}else{
				if(instrument.getFiltersw2().size() > 0){
					instrument.setFiltersw2(new ArrayList());
				}
			}	
			
			value  = request.getParameter("selectedGrismw");
			if (value != null){
				if(params.get("selectedGrismw") instanceof String[]){
					String[] values =  (String[]) params.get("selectedGrismw") ;
					List lst = new ArrayList();
					for(int i=0;i< values.length;i++ ){
						lst.add(values[i]);
					}
					instrument.setGrismw(lst);
				}
			}else{
				if(instrument.getGrismw().size() > 0){
					instrument.setGrismw(new ArrayList());
				}
			}
			
			value  = request.getParameter("selectedSlitw");
			if (value != null){
				if(params.get("selectedSlitw") instanceof String[]){
					String[] values =  (String[]) params.get("selectedSlitw") ;
					List lst = new ArrayList();
					for(int i=0;i< values.length;i++ ){
						lst.add(values[i]);
					}
					instrument.setSlitw(lst);
				}
			}else{
				if(instrument.getSlitw().size() > 0){
					instrument.setSlitw(new ArrayList());
				}
			}
			
			value  = request.getParameter("selectedGratingw");
			if (value != null){
				if(params.get("selectedGratingw") instanceof String[]){
					String[] values =  (String[]) params.get("selectedGratingw") ;
					List lst = new ArrayList();
					for(int i=0;i< values.length;i++ ){
						lst.add(values[i]);
					}
					instrument.setGratingw(lst);
				}
			}else{
				if(instrument.getGratingw().size() > 0){
					instrument.setGratingw(new ArrayList());
				}
			}
			
			value  = request.getParameter("flagging");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setFlagging(true);
				}else{
					instrument.setFlagging(false);
				}
			}
			
			value  = request.getParameter("frameExposureTime");
			if (value != null){
				try{
					instrument.setFrameExposureTime(new Double(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("selectedGrating");
			if (value != null){
				instrument.setGrating(value);
			}
			
			value  = request.getParameter("selectedGrism");
			if (value != null){
				instrument.setGrism(value);
			}
				
			value  = request.getParameter("guideLine");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setGuideline(true);
				}else{
					instrument.setGuideline(false);
				}
			}
	
			value  = request.getParameter("imaging");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setImaging(true);
				}else{
					instrument.setImaging(false);
				}
			}
			
			value  = request.getParameter("defaultSubarrayPointings");
			if (value != null) {
				if (value.equalsIgnoreCase("true")) {
					instrument.setSubarrayPointings(null);
				} else {
					
					if (instrument.getSubarrayPointings() == null){
						instrument.setSubarrayPointings(1);
					}

					value  = request.getParameter("subarrayPointings");
					if (value != null) {
						try{
							instrument.setSubarrayPointings(Integer.parseInt(value));
						}catch(NumberFormatException e){
							value = null;
						}
					}
				}
			}

			value  = request.getParameter("integrationTime");
			if (value != null){
				try{
					instrument.setIntegrationTime(new Double (value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("storeRawData");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setStoreRawData(true);
				}else{
					instrument.setStoreRawData(false);
				}
			}
			
			value  = request.getParameter("correlatedVisibilities");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setCorrelatedVisibilities(true);
				}else{
					instrument.setCorrelatedVisibilities(false);
				}
			}
			
			value  = request.getParameter("storeUVData");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setStoreUVData(true);;
				}else{
					instrument.setStoreUVData(false);
				}
			}
			
			value  = request.getParameter("selectedMicrostepping");
			if (value != null){
				instrument.setMicrostepping(value);
			}
			
			value  = request.getParameter("selectedMode");
			if (value != null){
				instrument.setMode(value);
			}
			
			value  = request.getParameter("nonDefaultFrameExposureTime");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setNonDefaultFrameExposureTime(true);
				}else{
					instrument.setNonDefaultFrameExposureTime(false);
				}
			}
			
			value  = request.getParameter("nonDefaultFrameExposureTimeDetails");
			if (value != null){
				instrument.setNonDefaultFrameExposureTimeDetails(value);
			}
			
			value  = request.getParameter("selectedOneFilter");
			if (value != null){
				instrument.setOneFilter(value);
			}
			
			value  = request.getParameter("order");
			if (value != null){
				try{
					instrument.setOrder(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("selectedOrderFilter");
			if (value != null){
				instrument.setOrderFilter(value);
			}
			
			value  = request.getParameter("pixelSizeX");
			if (value != null){
				try{
					instrument.setPixelSizeX(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("pixelSizeY");
			if (value != null){
				try{
					instrument.setPixelSizeY(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("polarimetry");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setPolarimetry(true);
				}else{
					instrument.setPolarimetry(false);
				}
			}
			
			//instrument.setPolarizations(lst);
			value  = request.getParameter("selectedPolarizations");
			if (value != null){
				
				instrument.setPolarization(value);
				//instrument.setPolarizations( );
			}
			
			value  = request.getParameter("selectedIncPolarizations");
			if (value != null){
				
				instrument.setIncPolarization(value);
			}
			
			value  = request.getParameter("selectedReadOut");
			if (value != null){
				instrument.setReadOut(value);
			}
			
			value  = request.getParameter("singleStationSpecifics");
			if (value != null){
				instrument.setSingleStationSpecifics(value);
			}
			
			value  = request.getParameter("selectedSlit");
			if (value != null){
				instrument.setSlit(value);
			}
			
			value  = request.getParameter("slitPositionAngle");
			if (value != null){
				try{
					instrument.setSlitPositionAngle(new Double(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("selectedStation");
			if (value != null){
				instrument.setStation(value);
			}
			
			value  = request.getParameter("wavelength");
			if (value != null){
				instrument.setWavelength(value);
			}
			
		/*This is no more a radio button it is a checkbox
		 * 	value  = request.getParameter("enablePiggyBack");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setPiggyBack(true);
				}else{
					instrument.setPiggyBack(false);
				}
			}*/
			
			value  = request.getParameter("piggyBackSpecifics");
			if (value != null){
				instrument.setPiggyBackSpecifics(value);
			}
			
			value  = request.getParameter("directDataStorage");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setDirectDataStorage(true);
				}else{
					instrument.setDirectDataStorage(false);
				}
			}
			
			value  = request.getParameter("beams");
			if (value != null){
				try{
					instrument.setBeams(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("cohstokes");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setCohstokes(true);
				}else{
					instrument.setCohstokes(false);
				}
			}
			value  = request.getParameter("incohstokes");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setIncohstokes(true);
				}else{
					instrument.setIncohstokes(false);
				}
			}
			
			value  = request.getParameter("cohTaBeams");
			if (value != null){
				try{
					instrument.setCohTaBeams(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("incohTaBeams");
			if (value != null){
				try{
					instrument.setInCohTaBeams(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			} 
			
			value  = request.getParameter("rings");
			if (value != null){
				try{
					instrument.setRings(new Integer (value.replaceAll("\\D.*", "")));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			value  = request.getParameter("complexVoltage");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setComplexVoltage(true);
				}else{
					instrument.setComplexVoltage(false);
				}
			}
			value  = request.getParameter("flysEye");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setFlysEye(true);
				}else{
					instrument.setFlysEye(false);
				}
			}
			value  = request.getParameter("rawVoltage");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setRawVoltage(true);
				}else{
					instrument.setRawVoltage(false);
				}
			}
			value  = request.getParameter("channels");
			if (value != null){
				try{
					instrument.setChannels(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			value  = request.getParameter("incchannels");
			if (value != null){
				try{
					instrument.setIncchannels(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			value  = request.getParameter("intsteps");
			if (value != null){
				try{
					instrument.setIntsteps(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("incohIntsteps");
			if (value != null){
				try{
					instrument.setIncohIntsteps(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			value  = request.getParameter("samplerate");
			if (value != null){
				try{
					instrument.setSamplerate(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
		
			
			value  = request.getParameter("tbbTriggerRate");
			if (value != null){
				try{
					instrument.setTbbTriggerRate(new Double(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
		
			value  = request.getParameter("tbbTriggerLength");
			if (value != null){
				try{
					instrument.setTbbTriggerLength(new Double(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			value  = request.getParameter("events");
			if (value != null){
				try{
					instrument.setEvents(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			value  = request.getParameter("tbbExposureTime");
			if (value != null){
				try{
					instrument.setTbbExposureTime(new Double(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("tbbSelectedObservationId");
			if (value != null){
				instrument.setTbbSelectedObservationId(value);
			}
			value  = request.getParameter("tbbTriggerSourceId");
			if (value != null){
				instrument.setTbbTriggerSourceId(value);
			}
			
			value  = request.getParameter("selectedTelescopeScheduling");
			if (value != null){
				instrument.setTelescopeScheduling(value);
			}
			
			value  = request.getParameter("frequencyChannels");
			if (value != null){
				try{
					//instrument.setSubbandsPerImage(new Integer(value.replaceAll("\\D.*", "")));
					instrument.setFrequencyChannels(Integer.parseInt(value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			// observation details:
			// fill observation options
			value  = request.getParameter("selectedObservationPhase");
			if (value != null){
				opticonObservation.setObservationPhase(value);
			}
			value  = request.getParameter("selectedObservationMode");
			if (value != null){
				opticonObservation.setObservationMode(value);
			}
			value  = request.getParameter("selectedObservationSeeing");
			if (value != null){
				opticonObservation.setObservationSeeing(value);
			}
			String[] values  = request.getParameterValues("selectedObservationWeather");
			if (values != null){
				String tmp = "" ;
				for(int i = 0;i < values.length;i++){
					if( i != 0 )
						tmp += InstrumentBean.MULTITEM_SEPARATOR;
					tmp += values[i];
				}
				opticonObservation.setObservationWeather(tmp);
			}
			value  = request.getParameter("observationDates");
			if (value != null){
				opticonObservation.setObservationDates(value);
			}
			
			value  = request.getParameter("requiredSchedConstraints");
			if (value != null){
				opticonObservation.setRequiredSchedConstraints(value);
			}
			value  = request.getParameter("preferredSchedConstraints");
			if (value != null){
				opticonObservation.setPreferredSchedConstraints(value);
			}
			
			value  = request.getParameter("observationRequestedTime");
			if (value != null){
				try{
					// fill it in the allocations system
					NorthStarDelegate northStarDelegate = NorthStarDelegate.getInstance();
					ResourceType resourceType = 
				    	   northStarDelegate.getResourceType(OpticonConstants.TIME, OpticonConstants.NIGHTS );
	
					NorthStarDelegate.setResourceValue(
							 opticonObservation.getAllocations(), 
							 opticonObservation.getTelescopeConfiguration() + '_' + OpticonConstants.REQUESTEDTIME ,
				                AstronConverter.toDouble(value), resourceType);
					
				//opticonObservation.setObservationDuration(new Double(value));
				}catch(NumberFormatException e){
					value = null;
				} catch (DatabaseException e) {
					value=null;
				}
				
			}
			
			value  = request.getParameter("observationMinimumTime");
			if (value != null){
				try{
					// fill it in the allocations system
					NorthStarDelegate northStarDelegate = NorthStarDelegate.getInstance();
					ResourceType resourceType = 
				    	   northStarDelegate.getResourceType(OpticonConstants.TIME, OpticonConstants.NIGHTS );
	
					NorthStarDelegate.setResourceValue(
							 opticonObservation.getAllocations(), 
							 opticonObservation.getTelescopeConfiguration() + '_' + OpticonConstants.MINIMUMTIME ,
				                AstronConverter.toDouble(value), resourceType);
				}catch(NumberFormatException e){
					value = null;
				} catch (DatabaseException e) {
					value=null;
				}
				
			}
			
			value  = request.getParameter("backupStrategy");
			if (value != null){
				opticonObservation.setBackupStrategy(value);
			}
		 		
			value  = request.getParameter("calibrationRequirements");
			if (value != null){
				opticonObservation.setCalibrationRequirements(value);
			}
		
			value  = request.getParameter("observationNoiseLevel");
			if (value != null){
				try {
					opticonObservation.setNoiseLevel(new Double(value));
				} catch (NumberFormatException e) {
					value = null;
					opticonObservation.setNoiseLevel(new Double(0d));
				}
			}
			
			value  = request.getParameter("skipRFI");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setSkipRFI(true);
				}else{
					instrument.setSkipRFI(false);
				}
			}
			
			value  = request.getParameter("skipFolding");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setSkipFolding(true);
				}else{
					instrument.setSkipFolding(false);
				}
			}
			
			value  = request.getParameter("skipPdmp");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setSkipPdmp(true);
				}else{
					instrument.setSkipPdmp(false);
				}
			}
			
			value  = request.getParameter("skipDspsr");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setSkipDspsr(true);
				}else{
					instrument.setSkipDspsr(false);
				}
			}
			
			value  = request.getParameter("skipPrepfold");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setSkipPrepfold(true);
				}else{
					instrument.setSkipPrepfold(false);
				}
			}
			
			value  = request.getParameter("singlePulse");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setSinglePulse(true);
				}else{
					instrument.setSinglePulse(false);
				}
			}
			
			value  = request.getParameter("rratsAnalysis");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setRratsAnalysis(true);
				}else{
					instrument.setRratsAnalysis(false);
				}
			}
			
			value  = request.getParameter("skipDynamicAverage");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setSkipDynamicAverage(true);
				}else{
					instrument.setSkipDynamicAverage(false);
				}
			}
			
			value  = request.getParameter("subintegrationLength");
			if (value != null){
				try{
					instrument.setSubintegrationLength(new Double (value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("convertRawData");
			if (value != null){
				if( value.equalsIgnoreCase("true")){
					instrument.setConvertRawData(true);
				}else{
					instrument.setConvertRawData(false);
				}
			}
			
			value  = request.getParameter("threshold");
			if (value != null){
				try{
					instrument.setThreshold(new Double (value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("sigmaLimit");
			if (value != null){
				try{
					instrument.setSigmaLimit(new Double (value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("numberOfBlocks");
			if (value != null){
				try{
					instrument.setNumberOfBlocks(new Double (value));
				}catch(NumberFormatException e){
					value = null;
				}
			}
			
			value  = request.getParameter("prepfoldOptions");
				instrument.setPrepfoldOptions(value);
			
			value  = request.getParameter("presubbandOptions");
				instrument.setPresubbandOptions(value);
			
			value  = request.getParameter("rfifind");
				instrument.setRfifind(value);
			
			value  = request.getParameter("dspsrOptions");
				instrument.setDspsrOptions(value);
			
			value  = request.getParameter("digifilOptions");
				instrument.setDigifilOptions(value);
			
			value  = request.getParameter("predataOptions");
				instrument.setPredataOptions(value);
			
			value  = request.getParameter("bf2fitsOptions");
				instrument.setBf2fitsOptions(value);
				
			value  = request.getParameter("pulsarDesc");
				instrument.setPulsarDesc(value);
			
			opticonObservation.setInstrument(instrument);
			
		}
	}

/*	private static List removeConfiguration(List<String> selectedInstConfs,
			String string) {
		List<String> list = new ArrayList<String>();
		for(String oi : selectedInstConfs) {
			if(oi.equals(string))
				list.add(oi);
		}
		selectedInstConfs.removeAll(list);
		return selectedInstConfs;
	}*/
}
