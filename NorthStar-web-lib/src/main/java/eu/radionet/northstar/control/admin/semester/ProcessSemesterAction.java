// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.semester;

import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.control.ActionServlet;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.Category;
import eu.radionet.northstar.data.entities.Semester;

public class ProcessSemesterAction extends Action {

	private NorthStarDelegate northStarDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SemesterForm semesterForm = (SemesterForm) form;
		ActionMessages errors = new ActionMessages();
		if (isCancelled(request)) {
			return mapping.findForward(Constants.SUCCESS);
		}
		/*
		 * retrieve own_useraccount info from session
		 */
		if (semesterForm.getStoreButton() != null) {

			northStarDelegate = NorthStarDelegate.getInstance();
			if (!AstronValidator.isPositiveInt(semesterForm.getSemesterId())) {
				Category category = northStarDelegate
				.getCategory(AstronConverter.toInteger(semesterForm
						.getCategory()));
				boolean alreadyExist = northStarDelegate.semesterAlreadyExist(semesterForm.getTelescope(),
						semesterForm.getCommunity(), semesterForm.getSemester(),
						category.getCode());
				if (alreadyExist){
					errors.add("semesters", new ActionMessage("error.notunique.semester"));
					saveErrors(request, errors);
					return mapping.getInputForward();
				}				
			}

			semesterForm.setCategories(northStarDelegate.getCategories());
			semesterForm.setTelescopes(NorthStarConfiguration
					.getTelescopeNames());

			SimpleDateFormat longDateFormat = new SimpleDateFormat();
			longDateFormat.applyPattern(Constants.SEMESTER_DATETIME_FORMAT);
			longDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
			Semester semester = new Semester();
			if (AstronValidator.isPositiveInt(semesterForm.getSemesterId())) {
				semester = northStarDelegate.getSemester(AstronConverter
						.toInteger(semesterForm.getSemesterId()));
				semester.setLastAssigned(AstronConverter.toInteger(semesterForm
						.getLastAssignedNumber()));

			} else {
				semester.setPrefix(semesterForm.getPrefix());
				semester.setTelescope(semesterForm.getTelescope());
				Category category = northStarDelegate
				.getCategory(AstronConverter.toInteger(semesterForm
						.getCategory()));
				semester.setCategory(category);
				semester.setCommunity(semesterForm.getCommunity());
				semester.setSemester(semesterForm.getSemester());
				semester.setLastAssigned(new Integer(0));
			}

			semester.setStartDate(longDateFormat.parse(semesterForm
					.getStartTime()));
			semester
					.setEndDate(longDateFormat.parse(semesterForm.getEndTime()));
			semester.setDeadLine(longDateFormat.parse(semesterForm
					.getDeadLineTime()));
			semester.setDeadLineDelay(AstronConverter.toInteger(semesterForm.getDeadLineDelay()));
			semester.setClosed(false);
			semester.setAvailableDate(longDateFormat.parse(semesterForm
					.getAvailableTime()));
			semester.setParentId(AstronConverter.toInteger(semesterForm.getSelectedParentId()));
			semester.setImmediate(semesterForm.isImmediate());
			northStarDelegate.storeSemester(semester);
			List semesters = northStarDelegate.getNotClosedSemesters();
			ActionServlet.taskDaemon.cancelAllTasks();
			Iterator semesterIterator = semesters.iterator();
			while (semesterIterator.hasNext()) {
				Semester temp = (Semester) semesterIterator.next();
				ActionServlet.taskDaemon.addDeadlineTask(temp);
			}
			return mapping.findForward(Constants.SUCCESS);
		}
		return mapping.findForward(Constants.REFRESH);
	}

}
