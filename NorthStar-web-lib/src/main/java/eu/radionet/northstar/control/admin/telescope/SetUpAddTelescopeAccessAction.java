// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.telescope;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import eu.radionet.northstar.business.NorthStarDelegate;

import nl.astron.useradministration.business.UserManagement;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.useradministration.data.entities.User;
import nl.astron.useradministration.data.entities.UserAccountSummary;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.Telescope;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalStatus;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.UserTelescope;

public class SetUpAddTelescopeAccessAction extends Action {
    private Log log = LogFactory.getLog(SetUpAddTelescopeAccessAction.class);
	private NorthStarDelegate northstarDelegate = null;
	//private AddTelescopeAccessForm addTelescopeAccessForm;
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		
		// if done button is pressed, return to previous screen
		if (isCancelled(request)){
			return mapping.findForward(Constants.CANCEL);
		}
		
		if(northstarDelegate == null){
			try {
	            northstarDelegate = NorthStarDelegate.getInstance();
	        } catch (ConnectionException ce) {
	            log.fatal("Cannot instantiate AdminDelegate: " + ce.getMessage());
	            return (mapping.findForward("fatalError"));
	        }
		}
		if (form != null)
		{
			AddTelescopeAccessForm addTelescopeAccessForm = (AddTelescopeAccessForm) form;
	        	        
	        // get selected telescope
	        if(request.getParameter("selectedTelescope")!=null){
	        	String telescopeName = (String) request.getParameter("selectedTelescope");
	        	
	        	// if a user is selected, add to the telescope
	        	String selectedUser = addTelescopeAccessForm.getSelectedUser();
	        	if (selectedUser != null && !selectedUser.equalsIgnoreCase(""))
				{
					UserTelescope userTelescope = new UserTelescope();
					userTelescope.setTelescope(telescopeName);
					if(Integer.valueOf(selectedUser) > 0){
						userTelescope.setUserId(Integer.valueOf(selectedUser));
					}else{
						userTelescope.setMemberId(Integer.valueOf(selectedUser));
					}
					northstarDelegate.addUserTelescope(userTelescope);
				}
	        	
	        	// fill list
	        	if(!telescopeName.equalsIgnoreCase("")){
	        		List users = fillUserNameList(telescopeName);
	        		addTelescopeAccessForm.setUserNameList(users);
	        	}
	        }
		}
		return mapping.findForward(Constants.SUCCESS);
	}
		
	
	private List fillUserNameList(String telescopeName) throws DatabaseException{
		//get useraccount summaries
	 	List userList =  northstarDelegate.getUserAccountSummaries(true);
	 		 	
        List IdList = northstarDelegate.getUsersTelescope(telescopeName);
      	List resultList = new ArrayList();
      	Iterator resultit = userList.iterator();
      	while(resultit.hasNext()){
      		UserAccountSummary sum = ( UserAccountSummary ) resultit.next();
      		// see if it is already in list
      		boolean found=false;
      		Iterator idit = IdList.iterator();
      		while(idit.hasNext()){
      			UserTelescope id = (UserTelescope) idit.next();
      			if(id.getUserId() != null && id.getUserId().intValue()==sum.getUserId().intValue()){
      				found=true;
      			}
      			
      		}
      		// if not add to list
      		if(!found){
      			User myUser = new User();
      			myUser.setId(sum.getUserId());
      			myUser.setLastName(sum.getLastName()+", "+sum.getFirstName());
       			resultList.add(myUser);
      		}
      	}
      	
      	// get users who have not accepted invitations yet.
	 	List memberList = northstarDelegate.getMembersNotProposal();
	 	resultit = memberList.iterator();
	 	while(resultit.hasNext()){
	 		NonRegisteredMember member = (NonRegisteredMember) resultit.next();
	 		// see if it is already in list
	 		if(member != null && member.getId() != null){
	 			boolean found=false;
	      		Iterator idit = IdList.iterator();
	      		while(idit.hasNext()){
	      			UserTelescope id = (UserTelescope) idit.next();
	      			int memberid = member.getId().intValue();
	      			if(id.getMemberId() != null && id.getMemberId().intValue() == memberid){
	      				found=true;
	      			}
	      			memberid = memberid*-1;
	      			if(id.getMemberId() != null && id.getMemberId().intValue() == memberid){
	      				found=true;
	      			}
	      			
	      		}
	      		// if not add to list
	      		if(!found){
	      			
	      			User myUser = new User();
	      			myUser.setId(member.getId().intValue() * -1);
	      			myUser.setLastName(member.getName());
	       			resultList.add(myUser);
	      		}
	 		}
	 	}
	 	
    	return resultList;
      
      //  return IdList;
        
	}
	
}
