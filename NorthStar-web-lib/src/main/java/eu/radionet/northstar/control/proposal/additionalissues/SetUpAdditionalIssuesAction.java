// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.additionalissues;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.SetUpProposalAction;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.AdditionalIssues;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Thesis;

public class SetUpAdditionalIssuesAction extends SetUpProposalAction {
    private Log log = LogFactory.getLog(this.getClass());
    protected ContextType contextConfiguration = null;
	
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
        /*
         * needed for tabs
         */
        request.setAttribute(Constants.CURRENT_ACTION, request
                        .getServletPath());
        AdditionalIssuesForm additionalIssuesForm = (AdditionalIssuesForm) form;
		HttpSession session = request.getSession();
        /*
         * retrieve own_useraccount info from session
         */
        UserAccount ownUserAccount = (UserAccount) session
                .getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
		
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);

		// check which options this telescope has enabled,
		// options are:
		// linked proposals this tac
		// linked proposals this tac
		// previous allocations
		// new observer experience
		// grant number
		// related publications
		// sponsoring
		
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
		if(contextConfiguration == null){
			contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());
		}
		FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		
		
		additionalIssuesForm.setEnableLinkedProposalsThis(
				OptionsDelegate.allowedToDisplay(Constants.ENABLE_LINKED_PROPOSALS_THIS,
						fieldsDefinitionType));	
		additionalIssuesForm.setEnableLinkedProposalsOther(
				OptionsDelegate.allowedToDisplay(Constants.ENABLE_LINKED_PROPOSALS_OTHER,
						fieldsDefinitionType));	
		additionalIssuesForm.setEnablePreviousAllocations(
				OptionsDelegate.allowedToDisplay(Constants.ENABLE_PREVIOUS_ALLOCATIONS,
						fieldsDefinitionType));	
		additionalIssuesForm.setEnableNewObserverExperience(
				OptionsDelegate.allowedToDisplay(Constants.ENABLE_NEW_OBSERVER_EXPERIENCE,
						fieldsDefinitionType));	
		additionalIssuesForm.setEnableGrantNumber(
				OptionsDelegate.allowedToDisplay(Constants.ENABLE_GRANT_NUMBER,
						fieldsDefinitionType));	
		additionalIssuesForm.setEnableRelatedPublications(
				OptionsDelegate.allowedToDisplay(Constants.ENABLE_RELATED_PUBLICATIONS,
						fieldsDefinitionType));	
		additionalIssuesForm.setEnablePreviousInvolvedProposal(
				OptionsDelegate.allowedToDisplay(Constants.ENABLE_RELATED_PREVIOUS_INVOLVED_PROPOSAL, 
						fieldsDefinitionType));
		additionalIssuesForm.setEnableSponsoring(
				OptionsDelegate.allowedToDisplay(Constants.ENABLE_SPONSORING,
						fieldsDefinitionType));	
		
		
		
		Integer maxPublicationCount = null; //telescopeConfiguration.getMaxPublicationCount();
		List urlList = new ArrayList();
		String Url = null;
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_PUBLICATION_COUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
				maxPublicationCount = AstronConverter.toInteger(Url);
		}
		
		if(maxPublicationCount != null && maxPublicationCount.intValue() > 0 ){
			additionalIssuesForm.setMaxPublicationCount(maxPublicationCount);
			}else{
			additionalIssuesForm.setRelatedPublications(false);
			
		}
		
				
		// Fill in the data
		
		AdditionalIssues additionalIssues = proposalData.getProposal().getAdditionalIssues();
		if (additionalIssues!= null && getErrors(request).size() == 0){
			
			additionalIssuesForm.setLinkedProposals(additionalIssues.isLinkedProposals());
			if (additionalIssuesForm.isLinkedProposals()){
				additionalIssuesForm.setLinkedProposalsSpecifics(additionalIssues.getLinkedProposalsSpecifics());
			}

			additionalIssuesForm.setLinkedProposalsElsewhere(additionalIssues.isLinkedProposalsElsewhere());
			if (additionalIssuesForm.isLinkedProposalsElsewhere()){
				additionalIssuesForm.setLinkedProposalsElsewhereSpecifics(additionalIssues.getLinkedProposalsElsewhereSpecifics());
			}
			
			additionalIssuesForm.setPreviousAllocations(additionalIssues.isPreviousAllocations());
			if (additionalIssuesForm.isPreviousAllocations()){
				additionalIssuesForm.setPreviousAllocationsSpecifics(additionalIssues.getPreviousAllocationsSpecifics());
			}
			
			if (additionalIssues.getRelatedPublications() != null && additionalIssuesForm.isEnableRelatedPublications()){
				additionalIssuesForm.setRelatedPublications(true);
				additionalIssuesForm.setRelatedPublicationsText(additionalIssues.getRelatedPublications());
			}
			
			if (additionalIssues.getRelatedPreviousInvolvedProposal() != null && additionalIssuesForm.isEnablePreviousInvolvedProposal()){
				additionalIssuesForm.setRelatedPreviousInvolvedProposal(true);
				additionalIssuesForm.setRelatedPreviousInvolvedProposalText(additionalIssues.getRelatedPreviousInvolvedProposal());
			}
			
			additionalIssuesForm.setGrantNumber(additionalIssues.getGrantNumber());

			additionalIssuesForm.setSponsorDetails(additionalIssues.getSponsorDetails());
			additionalIssuesForm.setEnableSponsoring(additionalIssues.isSponsoring() );
			additionalIssuesForm.setNewObserverExperience(additionalIssues.getNewObserverExperience());
			
			//boolean enableOpticonSupport=telescopeConfiguration.isEnableOpticonSupport() ;
			if(telescopeConfiguration.getTacCountry() != null){
				additionalIssuesForm.setDisplayOpticonSupport(true);
				additionalIssuesForm.setOpticonSupport(calculateOpticonSupport(proposalData.getProposal(), telescopeConfiguration.getPreferredCountries(),telescopeConfiguration.getTacCountry() ));
			}
            /*
             * thesis specific information
             */ 
            Iterator thesisIterator = additionalIssues.getTheses().iterator();

            List thesisBeans = new ArrayList();

            int i = 0;
            while (thesisIterator.hasNext()) {
                Thesis thesis = (Thesis) thesisIterator.next();
                
                ThesisBean thesisBean = new ThesisBean();
                thesisBean.setId(new Integer(i));
                thesisBean.setThesis(thesis);
                
                i++;
                thesisBeans.add(thesisBean);
			}
            
            additionalIssuesForm.setThesisBeans(thesisBeans);
			additionalIssuesForm.setAdditionalRemarks(additionalIssues.getAdditionalRemarks());	
		}
        return super.lockedExecute(mapping,form,request,response);
	}

	private boolean calculateOpticonSupport(Proposal proposal, String countries, String tacCountry) {
		int opticonMember=0;
		int nonOpticonMember=0;
		//String community = proposal.getSemester().getCommunity();

		List applicants = proposal.getMembers();
		String[] countryList = countries.split(",");
		String countryTAC = tacCountry;
		
		Iterator memberit = applicants.iterator();
		while (memberit.hasNext()){
			Member member = (Member) memberit.next();
			String countrylong = member.getCountry();
			
			// find 3 letter iso name for selected country
			Locale[] locales = Locale.getAvailableLocales();
			String countryShort="";
			for (int i=0; i< locales.length;i++){
				if(locales[i].getDisplayCountry().equalsIgnoreCase(countrylong)){
					countryShort=locales[i].getISO3Country();
				}
			}
			
			// check if 3 letter name is in the preferred countries list.
			for(int i=0;i<countryList.length;i++ ){
				if(countryList[i].equalsIgnoreCase(countryShort) && !countryList[i].equalsIgnoreCase(countryTAC) ){
					opticonMember++;
				}else{
					nonOpticonMember++;
				}
					
			}
		}
		if (nonOpticonMember!=0){
			if(opticonMember/nonOpticonMember > 0.5 )
			{
				return true;
			}
		}else{
			// no non opticon members, so ratio is 100%
			return true;
		}
		
		return false;
	}
}
