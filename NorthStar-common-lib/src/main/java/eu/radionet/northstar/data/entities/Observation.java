// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * Created on Dec 16, 2004
 *
 *
 */
package eu.radionet.northstar.data.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author Anton Smit
 */
public class Observation implements Serializable {
    protected Integer id = null;

/*
    protected String fieldName; // name of field in sky
   

    protected Double ra = null; // hh:mm:ss.ss

    protected Double decl = null; // +dd:mm:ss.s

    protected String epoch = null;
*/
    protected Map allocations = new HashMap();
       
    protected String requiredSchedConstraints = null; // vrij text veld

    protected String preferredSchedConstraints = null; // vrij text veld
    
    protected List targets;
    
    protected String observationKind = null;
    
    /**
     * @return Returns the id.
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id
     *            The id to set.
     */
    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * @param preferredSchedConstraints
     *            The preferredSchedConstraints to set.
     */
    public void setPreferredSchedConstraints(String preferredSchedConstraints) {
        this.preferredSchedConstraints = preferredSchedConstraints;
    }


    /**
     * @return Returns the requiredSchedConstraints.
     */
    public String getRequiredSchedConstraints() {
        return requiredSchedConstraints;
    }

    /**
     * @param requiredSchedConstraints
     *            The requiredSchedConstraints to set.
     */
    public void setRequiredSchedConstraints(String requiredSchedConstraints) {
        this.requiredSchedConstraints = requiredSchedConstraints;
    }

	public String getPreferredSchedConstraints()
	{
		return preferredSchedConstraints;
	}
	
	public Double getTotalObservationDuration()
	{
		/* 
		 * Allocations of a target are telescope specific, this needs to be implemented in
		 * telescope specific parts. 
		 */
		return null;
	}

	public Map getAllocations()
	{
		return allocations;
	}

	public void setAllocations(Map allocations)
	{
		this.allocations = allocations;
	}
	
	public List getTargets() {
		return targets;
	}

	public void setTargets(List targets) {
		this.targets = targets;
	}

	public String getObservationKind() {
		return observationKind;
	}

	public void setObservationKind(String observationKind) {
		this.observationKind = observationKind;
	}
	
}
