// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest.observation;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import eu.opticon.northstar.control.observingrequest.instrument.InstrumentBean;
import eu.opticon.northstar.control.observingrequest.instrument.ProcessInstrumentBean;
import eu.opticon.northstar.control.observingrequest.instrument.SetUpInstrumentBean;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.radionet.northstar.control.proposal.observingrequest.observation.ObservationForm;

public class OpticonObservationForm extends ObservationForm {

	protected String showInstrumentBean="false";
	protected String targetId =null;
    protected String targetExposureTime = null;
    protected boolean displayResourceAllocation;
    protected String selectedTargetMoon = "bright";
    protected String selectedTargetSeeing = ">0.9";
    protected String selectedTargetWater = "don't care";     

    protected boolean displayTargetMagnitude =false;
    protected boolean displayTargetSn =false;
    protected String selectedTargetSn = null;
	protected boolean requiredSn = false;

	protected String selectedTargetMagnitude = null;
    protected String selectedTargetComments = null;
       
	protected boolean maxNumberOfTargetsReached = false;
	protected String  maxNumberOfTargetsonSameRunReached =null;
	protected boolean displaySeeingRange = false;
	protected boolean requiredSeeingRange = false;
	protected String targetSeeingLower = null;
	protected String targetSeeingUpper = null;
	
	protected boolean displayTargetMoon = false;
	protected boolean displayTargetWater = false;
	protected boolean displayTargetExposureTime = false;
	protected boolean requiredTargetExposureTime = false;
	protected boolean displayTargetSkyQuality = false;
    protected String selectedTargetSkyQuality = null;
	protected boolean requiredTargetSkyQuality = false;

	protected List skyQualities = new ArrayList();
	
	protected boolean displayTargetFlux = false;
	protected boolean requiredTargetFlux = false;
	protected String targetFlux = null;

	protected boolean displayTargetFluxFrequency = false;
	protected boolean requiredTargetFluxFrequency = false;
	protected String targetFluxFrequency = null;

	protected boolean displayTargetSpectralIndex = false;
	protected boolean requiredTargetSpectralIndex = false;
	protected String targetSpectralIndex = null;

	protected boolean displayTargetDiameter = false;
	protected boolean requiredTargetDiameter = false;
	protected String targetDiameter = null;
	
	protected List moonOptions = new ArrayList();
	
	protected boolean displayObservationTime = false;
	protected List observationTimeOptions = new ArrayList();
	protected String selectedObservationTime = null;
	protected String targetStorage = null;
	protected boolean displayTargetStorage;

	protected boolean displaySeeingOptions = false;
	protected List seeingOptions = new ArrayList();

//    protected boolean displayObservationNoiseLevel =false;
//    protected String observationNoiseLevel = null;
//	protected boolean requiredObservationNoiseLevel = false;

	protected List waterOptions = new ArrayList();
	protected String totalDuration = null;
	protected List observingRuns = new ArrayList();
	protected String[] selectedRuns = null;
	protected String selectedObservingRun = null;
	
	protected boolean requiredTargetRa = true;
	protected boolean requiredTargetDec = true;
	protected boolean displayTargetRa = false;
	protected boolean displayTargetDec = false;
	protected boolean displayTargetRuns = false;
	protected boolean displayTargetOpportunity = false;
	protected boolean targetOpportunity = false;
	protected boolean autoCommit = false;
	
	protected boolean singleTargetPerRun = false;
	protected boolean displaySpecifyTargetSubbandList = false;
	protected boolean displayTargetCentralFrequency = false;
	protected boolean requiredTargetCentralFrequency = false;
	protected String targetCentralFrequency = null;
	protected boolean displayTargetBandwidth = false;
	protected boolean requiredTargetBandwidth = false;
	protected String targetBandwidth = null;
	protected boolean displayTargetSubbandList = false;
	protected boolean requiredTargetSubbandList = false;
	protected String targetSubbandList = null;
	protected boolean targetSpecifySubbandList = false;
	
	protected boolean displayTargetTotalSubbandList = false;
	protected String targetTotalSubbandList = null;
	
	protected boolean displayTargetSelectPipeline = false;
	protected List pipelines = new ArrayList();
	//protected String selectedPipeline = null;
	protected String[] selectedPipeline = null;
	
	protected boolean displayTargetSelectRunnumber = false;
	protected List runnumbers = new ArrayList();
	protected String selectedRunnumber = null;
	
	protected boolean displayTargetCalibrationBeam = false;
	protected boolean calibrationBeam = false;
	
	protected boolean enableTimeAndObservation = false;
	protected boolean targetFirstInRun = false;
	
	protected List tbbObservations = new ArrayList();
	protected String tbbSelectedObservationId =null;
	
	/*
	protected String selectedTelescopeConfiguration = null;
	protected List telescopeConfigurations = new ArrayList();
	protected boolean displayTelescopeDropbox = false;
	
    protected String selectedInstrumentConfiguration  = null;
	protected List instrumentConfigurations = new ArrayList();
	
	
	
	protected boolean displayInstrumentConfiguration = false;
	
	protected boolean displayFilters = false;
	protected boolean displayFilterDetails = false;
	protected boolean displayReadOut = false;
	protected boolean displayOrderFilter = false;

	protected boolean displayMode = false;
	
	protected boolean displayPolarimetry = false;
	protected boolean displayGuideline = false;
	protected String guidelineUrl = null;
	
	protected boolean displayWavelength = false;
	protected boolean displayOrder = false;
	protected boolean displaySlits = false;
	protected boolean displayGrating = false;
	protected boolean displayGrism = false;
	protected boolean displayComments = false;
	
	protected boolean displayFrameExposureTime = false;
	protected boolean displayMicrostepping = false;
	protected boolean displayCoronagraphicMask = false;
	protected boolean displaySlitPositionAngle = false;

	protected boolean requiredInstrumentConfiguration = false;
	
	protected boolean requiredFilters = false;
	protected boolean requiredFilterDetails = false;
	protected boolean requiredReadOut = false;
	protected boolean requiredOrderFilter = false;
	
	protected boolean requiredMode = false;

	protected boolean requiredPolarimetry = false;
	protected boolean requiredGuideline = false;

	protected boolean requiredWavelength = false;
	protected boolean requiredOrder = false;
	protected boolean requiredSlits = false;
	protected boolean requiredGrating = false;
	protected boolean requiredGrism = false;
	protected boolean requiredComments = false;

	protected boolean requiredNonDefaultFrameExposureTimeDetails = false;
	protected boolean requiredMicrostepping = false;
	protected boolean requiredCoronagraphicMask = false;
	protected boolean requiredSlitPositionAngle = false;
	
	protected String comments = null;
	protected boolean nonDefaultFrameExposureTime = false;
	protected String nonDefaultFrameExposureTimeDetails = null;
	protected String frameExposureTime = null;
	protected String selectedMicrostepping = null;
	protected boolean coronagraphicMask = false;
	protected List microstepping = new ArrayList();

	protected String[] selectedFilters = null;
	protected String selectedReadOut = null;
	protected String selectedMode = null;
	protected String selectedOrderFilter = null;

	protected List filters = new ArrayList();
	protected List readOuts = new ArrayList();
	protected List orderFilters = new ArrayList();
	protected List modes = new ArrayList();

	protected boolean polarimetry = false;
	protected boolean guideline = false;
	
	protected String wavelength = null;

	protected String filterDetails = null;

	protected String order = null;

	protected String selectedSlit = null;
	protected List slits = new ArrayList();
	protected String slitPositionAngle = "0";

	protected String selectedGrating = null;
	
	protected List gratings = new ArrayList();
	
	protected String selectedGrism = null;
	protected List grisms = new ArrayList();
	
		
	protected boolean displayObservationWeather = false;
	protected List observationWeatherOptions = new ArrayList();
	protected String selectedObservationWeather = null;
	
	protected boolean displayObservationMode = false;
	protected List observationModeOptions = new ArrayList();
	protected String selectedObservationMode = null;
	
	protected boolean displayObservationSeeing = false;
	protected List observationSeeingOptions = new ArrayList();
	protected String selectedObservationSeeing = null;
	
	protected boolean displayObservationDates = false;
	protected String observationDates = null;

	protected boolean displayRequiredSchedConstraints = false;
	protected boolean displayPreferredSchedConstraints = false;
	
	// for lofar
	protected boolean displayPostProcessing = false;
	protected boolean averaging = false;
	protected boolean flagging = false;
	protected boolean calibration = false;
	protected boolean imaging= false;
	
	protected String filterFrequency = null;
	protected String filterBandwidth = null;
	protected boolean filterContiguousCoverage = true;
	
	protected String customStationSpecifics = null;
	protected String averagingFrequency = null;
	protected String averagingTime = null;
	protected String fieldSizeX = null;
	protected String pixelSizeX = null;
	protected String fieldSizeY = null;
	protected String pixelSizeY = null;
	
	protected boolean displayStation = false;
	protected List stations = new ArrayList();
	protected String selectedStation = null;
	protected boolean requiredStation = false;

	protected boolean displayClock = false;
	protected List clocks = new ArrayList();
	protected String selectedClock = null;
	protected boolean requiredClock = false;
	
	protected boolean displayOneFilter = false;
	protected List oneFilters = new ArrayList();
	protected String selectedOneFilter = null;
	protected boolean requiredOneFilter = false;

	protected boolean displayAntenna = false;
	protected List antennas = new ArrayList();
	protected String selectedAntenna = null;
	protected boolean requiredAntenna = false;

	protected boolean displayIntegrationTime = false;
	protected String integrationTime = null;
	protected boolean requiredIntegrationTime = false;
	
	protected boolean displayInstrumentDetails = false;
	protected String instrumentDetails = null;
	
	protected boolean displayTargetStorage = false;
	protected boolean requiredTargetStorage = false;

	
	protected boolean customFilter = false;
	protected boolean contiguousCoverage = true;
	
	protected boolean displaySingleStationSpecifics = false;
	protected String singleStationSpecifics = null;
	
	protected List polarizations = new ArrayList();
	protected String[] selectedPolarizations = null;
	protected boolean displaySubFilter=false;
	
	protected String estimatedDataStorage = null;
	protected String dataStorageSpecifics = null;
	protected String dataCalculationType = null;
	*/
	
    public boolean isDisplayTargetRuns() {
		return displayTargetRuns;
	}


	public void setDisplayTargetRuns(boolean displayTargetRuns) {
		this.displayTargetRuns = displayTargetRuns;
	}

	public void reset()
    {
        targetExposureTime = null;
        //selectedTelescopeConfiguration = null;

        selectedTargetMoon = "bright";
	    selectedTargetSeeing = ">0.9";
	    selectedTargetWater = "don't care";     

	    selectedTargetSn = null;
		requiredSn = false;

	    selectedTargetMagnitude = null;
	    selectedTargetComments = null;
		
		//displaySeeingOptions = false;
		
		displaySeeingRange = false;
		requiredSeeingRange = false;
		targetSeeingLower = null;
		targetSeeingUpper = null;

		displayTargetSkyQuality = false;
	    selectedTargetSkyQuality = null;
		requiredTargetSkyQuality = false;
		
		displayTargetFlux = false;
		requiredTargetFlux = false;
		targetFlux = null;		

		displayTargetFluxFrequency = false;
		requiredTargetFluxFrequency = false;
		targetFluxFrequency = null;		

		displayTargetSpectralIndex = false;
		requiredTargetSpectralIndex = false;
		targetSpectralIndex = null;		

		displayTargetMagnitude =false;
	    displayTargetSn =false;
	    
	    selectedRuns = null;
	    targetOpportunity=false;
	    
//		displayObservationNoiseLevel = false;
//		requiredObservationNoiseLevel = false;
//		observationNoiseLevel = null;		

	    super.reset();
    }
 
    
    public boolean isDisplayResourceAllocation() {
		
		return displayResourceAllocation;
	}

	public void setDisplayResourceAllocation(boolean displayResourceAllocation) {
		this.displayResourceAllocation = displayResourceAllocation;
	}
	
	public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        errors.add(super.validate(mapping, request));
      
        
        if(true){
        	return errors;
        }
        
        Map params = request.getParameterMap();
       ////TO DO: temporary dissabled 
       //errors.add(InstrumentBean.validate(mapping,request));
        errors.add(InstrumentBean.staticValidate(mapping,request));
        
        
        if (!AstronValidator.isBlankOrNull(targetExposureTime)) {
            if (!AstronValidator.isPositiveInt(targetExposureTime)) {
                errors.add("totalDuration", new ActionMessage(
                        "error.noint.positive"));
            }

        }
        
        if (!AstronValidator.isBlankOrNull(ra)) {
			if (!AstronValidator.isRa(ra)) {
				errors
						.add("ra", new ActionMessage(
								"error.nora.observation.ra"));
			}

		}
		/*
		 * dec checking
		 */
		if (!AstronValidator.isBlankOrNull(dec)) {
			if (!AstronValidator.isDec(dec)) {
				errors.add("dec", new ActionMessage(
						"error.nodec.observation.dec"));
			}

		}
		if (AstronValidator.isBlankOrNull(ra)
				&& !AstronValidator.isBlankOrNull(dec)
				|| !AstronValidator.isBlankOrNull(ra)
				&& AstronValidator.isBlankOrNull(dec)) {
			errors.add("dec", new ActionMessage(
					"error.novalidradec.observation"));
		}

//        if (!AstronValidator.isBlankOrNull(observationNoiseLevel)) {
//            if (!AstronValidator.isPositiveDouble(observationNoiseLevel)) {
//                errors.add("observationNoiseLevel", new ActionMessage(
//                        "error.nodouble.positive"));
//            }
//
//        }

        if (errors.size() > 0){
        	/* only on 
        	
        	InstrumentBean instrBean = new InstrumentBean();
    		// make a temporary observation with instrument to get the data.
        	OpticonObservation opticonObservation = new OpticonObservation();
        	opticonObservation.setInstrument(new OpticonInstrument());
        	// get data from request
        	ProcessInstrumentBean.processForm(request, opticonObservation);
        	// insert the data in the attribute.
        	SetUpInstrumentBean.fillForm(request, opticonObservation, instrBean);
        	request.setAttribute("instrumentBean",instrBean);
        	*/
        }
        return errors;
    }
    
    public boolean isDisplayTargetMagnitude() {
		return displayTargetMagnitude;
	}

	public void setDisplayTargetMagnitude(boolean displayTargetMagnitude) {
		this.displayTargetMagnitude = displayTargetMagnitude;
	}

	public boolean isDisplayTargetSn() {
		return displayTargetSn;
	}

	public void setDisplayTargetSn(boolean displayTargetSn) {
		this.displayTargetSn = displayTargetSn;
	}
	
	
	public List getMoonOptions()
	{
		return moonOptions;
	}

	public void setMoonOptions(List moonOptions)
	{
		this.moonOptions = moonOptions;
	}

	
	public String getSelectedTargetMoon()
	{
		return selectedTargetMoon;
	}

	public void setSelectedTargetMoon(String selectedTargetMoon)
	{
		this.selectedTargetMoon = selectedTargetMoon;
	}

	public String getSelectedTargetSeeing()
	{
		return selectedTargetSeeing;
	}

	public void setSelectedTargetSeeing(String selectedTargetSeeing)
	{
		this.selectedTargetSeeing = selectedTargetSeeing;
	}

	public String getSelectedTargetWater()
	{
		return selectedTargetWater;
	}

	public void setSelectedTargetWater(String selectedTargetWater)
	{
		this.selectedTargetWater = selectedTargetWater;
	}

	public String getSelectedTargetComments()
	{
		return selectedTargetComments;
	}

	public void setSelectedTargetComments(String selectedTargetComments)
	{
		this.selectedTargetComments = selectedTargetComments;
	}

	public String getSelectedTargetMagnitude()
	{
		return selectedTargetMagnitude;
	}

	
	public void setSelectedTargetMagnitude(String selectedTargetMagnitude)
	{
		this.selectedTargetMagnitude = selectedTargetMagnitude;
	}

	public String getSelectedTargetSn()
	{
		return selectedTargetSn;
	}

	public void setSelectedTargetSn(String selectedTargetSn)
	{
		this.selectedTargetSn = selectedTargetSn;
	}

		
	public boolean isDisplayTargetSkyQuality()
	{
		return displayTargetSkyQuality;
	}
	
	public void setDisplayTargetSkyQuality(boolean displayTargetSkyQuality)
	{
		this.displayTargetSkyQuality = displayTargetSkyQuality;
	}
	
	public boolean isRequiredTargetSkyQuality()
	{
		return requiredTargetSkyQuality;
	}

	public void setRequiredTargetSkyQuality(boolean requiredTargetSkyQuality)
	{
		this.requiredTargetSkyQuality = requiredTargetSkyQuality;
	}

	public String getSelectedTargetSkyQuality()
	{
		return selectedTargetSkyQuality;
	}

	public void setSelectedTargetSkyQuality(String selectedTargetSkyQuality)
	{
		this.selectedTargetSkyQuality = selectedTargetSkyQuality;
	}

	public List getSkyQualities()
	{
		return skyQualities;
	}

	public void setSkyQualities(List skyQualities)
	{
		this.skyQualities = skyQualities;
	}

	public boolean isRequiredSn()
	{
		return requiredSn;
	}

	public void setRequiredSn(boolean requiredSn)
	{
		this.requiredSn = requiredSn;
	}

	public boolean isDisplayTargetMoon()
	{
		return displayTargetMoon;
	}

	public void setDisplayTargetMoon(boolean displayTargetMoon)
	{
		this.displayTargetMoon = displayTargetMoon;
	}

	public boolean isDisplayTargetWater()
	{
		return displayTargetWater;
	}

	public void setDisplayTargetWater(boolean displayTargetWater)
	{
		this.displayTargetWater = displayTargetWater;
	}

	public boolean isMaxNumberOfTargetsReached()
	{
		return maxNumberOfTargetsReached;
	}

	public void setMaxNumberOfTargetsReached(boolean maxNumberOfTargetsReached)
	{
		this.maxNumberOfTargetsReached = maxNumberOfTargetsReached;
	}

	public boolean isDisplayTargetDiameter() {
		return displayTargetDiameter;
	}

	public void setDisplayTargetDiameter(boolean displayDiameter) {
		this.displayTargetDiameter = displayDiameter;
	}

	public boolean isRequiredTargetDiameter() {
		return requiredTargetDiameter;
	}

	public void setRequiredTargetDiameter(boolean requiredDiameter) {
		this.requiredTargetDiameter = requiredDiameter;
	}

	public String getTargetDiameter() {
		return targetDiameter;
	}

	public void setTargetDiameter(String diameter) {
		this.targetDiameter = diameter;
	}

	public void setTargetExposureTime(String value) {
		this.targetExposureTime= value;
		
	}

	public String getTargetExposureTime() {
		
		return this.targetExposureTime;
	}

	
	public String getShowInstrumentBean() {
		return showInstrumentBean;
	}


	public void setShowInstrumentBean(String showInstrumentBean) {
		this.showInstrumentBean = showInstrumentBean;
	}


	public boolean isDisplaySeeingRange() {
		return displaySeeingRange;
	}


	public void setDisplaySeeingRange(boolean displaySeeingRange) {
		this.displaySeeingRange = displaySeeingRange;
	}


	public boolean isRequiredSeeingRange() {
		return requiredSeeingRange;
	}


	public void setRequiredSeeingRange(boolean requiredSeeingRange) {
		this.requiredSeeingRange = requiredSeeingRange;
	}


	public String getTargetSeeingLower() {
		return targetSeeingLower;
	}


	public void setTargetSeeingLower(String targetSeeingLower) {
		this.targetSeeingLower = targetSeeingLower;
	}


	public String getTargetSeeingUpper() {
		return targetSeeingUpper;
	}


	public void setTargetSeeingUpper(String targetSeeingUpper) {
		this.targetSeeingUpper = targetSeeingUpper;
	}


	public boolean isDisplayTargetExposureTime() {
		return displayTargetExposureTime;
	}


	public void setDisplayTargetExposureTime(boolean displayTargetExposureTime) {
		this.displayTargetExposureTime = displayTargetExposureTime;
	}


	public boolean isDisplayTargetFlux() {
		return displayTargetFlux;
	}


	public void setDisplayTargetFlux(boolean displayTargetFlux) {
		this.displayTargetFlux = displayTargetFlux;
	}


	public boolean isRequiredTargetFlux() {
		return requiredTargetFlux;
	}


	public void setRequiredTargetFlux(boolean requiredTargetFlux) {
		this.requiredTargetFlux = requiredTargetFlux;
	}


	public String getTargetFlux() {
		return targetFlux;
	}


	public void setTargetFlux(String targetFlux) {
		this.targetFlux = targetFlux;
	}


	public boolean isDisplayTargetFluxFrequency() {
		return displayTargetFluxFrequency;
	}


	public void setDisplayTargetFluxFrequency(boolean displayTargetFluxFrequency) {
		this.displayTargetFluxFrequency = displayTargetFluxFrequency;
	}


	public boolean isRequiredTargetFluxFrequency() {
		return requiredTargetFluxFrequency;
	}


	public void setRequiredTargetFluxFrequency(boolean requiredTargetFluxFrequency) {
		this.requiredTargetFluxFrequency = requiredTargetFluxFrequency;
	}


	public String getTargetFluxFrequency() {
		return targetFluxFrequency;
	}


	public void setTargetFluxFrequency(String targetFluxFrequency) {
		this.targetFluxFrequency = targetFluxFrequency;
	}


	public boolean isDisplayTargetSpectralIndex() {
		return displayTargetSpectralIndex;
	}


	public void setDisplayTargetSpectralIndex(boolean displayTargetSpectralIndex) {
		this.displayTargetSpectralIndex = displayTargetSpectralIndex;
	}


	public boolean isRequiredTargetSpectralIndex() {
		return requiredTargetSpectralIndex;
	}


	public void setRequiredTargetSpectralIndex(boolean requiredTargetSpectralIndex) {
		this.requiredTargetSpectralIndex = requiredTargetSpectralIndex;
	}


	public String getTargetSpectralIndex() {
		return targetSpectralIndex;
	}


	public void setTargetSpectralIndex(String targetSpectralIndex) {
		this.targetSpectralIndex = targetSpectralIndex;
	}


	public boolean isDisplayObservationTime() {
		return displayObservationTime;
	}


	public void setDisplayObservationTime(boolean displayObservationTime) {
		this.displayObservationTime = displayObservationTime;
	}


	public List getObservationTimeOptions() {
		return observationTimeOptions;
	}


	public void setObservationTimeOptions(List observationTimeOptions) {
		this.observationTimeOptions = observationTimeOptions;
	}


	public String getSelectedObservationTime() {
		return selectedObservationTime;
	}


	public void setSelectedObservationTime(String selectedObservationTime) {
		this.selectedObservationTime = selectedObservationTime;
	}


	public String getTargetStorage() {
		return targetStorage;
	}


	public void setTargetStorage(String targetStorage) {
		this.targetStorage = targetStorage;
	}


	public boolean isDisplaySeeingOptions() {
		return displaySeeingOptions;
	}


	public void setDisplaySeeingOptions(boolean displaySeeingOptions) {
		this.displaySeeingOptions = displaySeeingOptions;
	}


	public boolean isDisplayTargetStorage() {
		return displayTargetStorage;
	}


	public void setDisplayTargetStorage(boolean displayTargetStorage) {
		this.displayTargetStorage = displayTargetStorage;
	}


	public List getSeeingOptions() {
		return seeingOptions;
	}


	public void setSeeingOptions(List seeingOptions) {
		this.seeingOptions = seeingOptions;
	}


	public List getWaterOptions() {
		return waterOptions;
	}


	public void setWaterOptions(List waterOptions) {
		this.waterOptions = waterOptions;
	}


	public String getTotalDuration() {
		return totalDuration;
	}


	public void setTotalDuration(String totalDuration) {
		this.totalDuration = totalDuration;
	}


	public List getObservingRuns() {
		return observingRuns;
	}


	public void setObservingRuns(List observingRuns) {
		this.observingRuns = observingRuns;
	}


	public String[] getSelectedRuns() {
		return selectedRuns;
	}


	public void setSelectedRuns(String[] selectedRuns) {
		this.selectedRuns = selectedRuns;
	}

//	public final boolean isDisplayObservationNoiseLevel() {
//		return displayObservationNoiseLevel;
//	}
//
//
//	public final void setDisplayObservationNoiseLevel(boolean displayObservationNoiseLevel) {
//		this.displayObservationNoiseLevel = displayObservationNoiseLevel;
//	}
//
//
//	public final String getObservationNoiseLevel() {
//		return observationNoiseLevel;
//	}
//
//
//	public final void setObservationNoiseLevel(String observationNoiseLevel) {
//		this.observationNoiseLevel = observationNoiseLevel;
//	}
//
//
//	public final boolean isRequiredObservationNoiseLevel() {
//		return requiredObservationNoiseLevel;
//	}
//
//
//	public final void setRequiredObservationNoiseLevel(boolean requiredObservationNoiseLevel) {
//		this.requiredObservationNoiseLevel = requiredObservationNoiseLevel;
//	}


	public String getTargetId() {
		return targetId;
	}


	public void setTargetId(String targetId) {
		this.targetId = targetId;
	}


	public boolean isRequiredTargetRa() {
		return requiredTargetRa;
	}

	public void setRequiredTargetRa(boolean requiredTargetRa) {
		this.requiredTargetRa = requiredTargetRa;
	}


	public boolean isRequiredTargetDec() {
		return requiredTargetDec;
	}


	public void setRequiredTargetDec(boolean requiredTargetDec) {
		this.requiredTargetDec = requiredTargetDec;
	}


	public boolean isDisplayTargetRa() {
		return displayTargetRa;
	}


	public void setDisplayTargetRa(boolean displayTargetRa) {
		this.displayTargetRa = displayTargetRa;
	}


	public boolean isDisplayTargetDec() {
		return displayTargetDec;
	}


	public void setDisplayTargetDec(boolean displayTargetDec) {
		this.displayTargetDec = displayTargetDec;
	}


	public boolean isDisplayTargetOpportunity() {
		return displayTargetOpportunity;
	}


	public void setDisplayTargetOpportunity(boolean displayTargetOpportunity) {
		this.displayTargetOpportunity = displayTargetOpportunity;
	}


	public boolean isTargetOpportunity() {
		return targetOpportunity;
	}


	public void setTargetOpportunity(boolean targetOpportunity) {
		this.targetOpportunity = targetOpportunity;
	}


	public boolean isAutoCommit() {
		return autoCommit;
	}


	public void setAutoCommit(boolean autocommit) {
		this.autoCommit = autocommit;
	}


	public boolean isSingleTargetPerRun() {
		return singleTargetPerRun;
	}


	public void setSingleTargetPerRun(boolean singleTargetPerRun) {
		this.singleTargetPerRun = singleTargetPerRun;
	}


	public boolean isDisplaySpecifyTargetSubbandList() {
		return displaySpecifyTargetSubbandList;
	}


	public void setDisplaySpecifyTargetSubbandList(
			boolean displaySpecifyTargetSubbandList) {
		this.displaySpecifyTargetSubbandList = displaySpecifyTargetSubbandList;
	}


	public boolean isDisplayTargetCentralFrequency() {
		return displayTargetCentralFrequency;
	}


	public void setDisplayTargetCentralFrequency(
			boolean displayTargetCentralFrequency) {
		this.displayTargetCentralFrequency = displayTargetCentralFrequency;
	}


	public boolean isRequiredTargetCentralFrequency() {
		return requiredTargetCentralFrequency;
	}


	public void setRequiredTargetCentralFrequency(
			boolean requiredTargetCentralFrequency) {
		this.requiredTargetCentralFrequency = requiredTargetCentralFrequency;
	}


	public String getTargetCentralFrequency() {
		return targetCentralFrequency;
	}


	public void setTargetCentralFrequency(String targetCentralFrequency) {
		this.targetCentralFrequency = targetCentralFrequency;
	}


	public boolean isDisplayTargetBandwidth() {
		return displayTargetBandwidth;
	}


	public void setDisplayTargetBandwidth(boolean displayTargetBandwidth) {
		this.displayTargetBandwidth = displayTargetBandwidth;
	}


	public boolean isRequiredTargetBandwidth() {
		return requiredTargetBandwidth;
	}


	public void setRequiredTargetBandwidth(boolean requiredTargetBandwidth) {
		this.requiredTargetBandwidth = requiredTargetBandwidth;
	}


	public String getTargetBandwidth() {
		return targetBandwidth;
	}


	public void setTargetBandwidth(String targetBandwidth) {
		this.targetBandwidth = targetBandwidth;
	}


	public boolean isDisplayTargetSubbandList() {
		return displayTargetSubbandList;
	}


	public void setDisplayTargetSubbandList(boolean displayTargetSubbandList) {
		this.displayTargetSubbandList = displayTargetSubbandList;
	}

	public boolean isDisplayTargetTotalSubbandList() {
		return displayTargetTotalSubbandList;
	}	
	
	public void setDisplayTargetTotalSubbandList(boolean displayTargetTotalSubbandList) {
		this.displayTargetTotalSubbandList = displayTargetTotalSubbandList;
	}	

	public boolean isRequiredTargetSubbandList() {
		return requiredTargetSubbandList;
	}


	public void setRequiredTargetSubbandList(boolean requiredTargetSubbandList) {
		this.requiredTargetSubbandList = requiredTargetSubbandList;
	}


	public String getTargetSubbandList() {
		return targetSubbandList;
	}

	public void setTargetSubbandList(String targetSubbandList) {
		this.targetSubbandList = targetSubbandList;
	}

	public String getTargetTotalSubbandList() {
		return targetTotalSubbandList;
	}	

	public void setTargetTotalSubbandList(String targetTotalSubbandList) {
		this.targetTotalSubbandList = targetTotalSubbandList;
	}

	public boolean isTargetSpecifySubbandList() {
		return targetSpecifySubbandList;
	}


	public void setTargetSpecifySubbandList(boolean targetSpecifySubbandList) {
		this.targetSpecifySubbandList = targetSpecifySubbandList;
	}


	public boolean isDisplayTargetSelectPipeline() {
		return displayTargetSelectPipeline;
	}
	
	public boolean getDisplayTargetSelectPipeline() {
		return displayTargetSelectPipeline;
	}


	public void setDisplayTargetSelectPipeline(boolean displayTargetSelectPipeline) {
		this.displayTargetSelectPipeline = displayTargetSelectPipeline;
	}


	public List getPipelines() {
		return pipelines;
	}


	public void setPipelines(List pipelines) {
		this.pipelines = pipelines;
	}


	public List getTbbObservations() {
		return tbbObservations;
	}


	public void setTbbObservations(List tbbObservations) {
		this.tbbObservations = tbbObservations;
	}


	public String getTbbSelectedObservationId() {
		return tbbSelectedObservationId;
	}


	public void setTbbSelectedObservationId(String tbbSelectedObservationId) {
		this.tbbSelectedObservationId = tbbSelectedObservationId;
	}


	//public String getSelectedPipeline() {
	public String[] getSelectedPipeline() {
		return selectedPipeline;
	}


	//public void setSelectedPipeline(String selectedPipeline) {
	public void setSelectedPipeline(String[] selectedPipeline) {
		this.selectedPipeline = selectedPipeline;
	}


	public boolean isDisplayTargetSelectRunnumber() {
		return displayTargetSelectRunnumber;
	}


	public void setDisplayTargetSelectRunnumber(boolean displayTargetSelectRunnumber) {
		this.displayTargetSelectRunnumber = displayTargetSelectRunnumber;
	}


	public List getRunnumbers() {
		return runnumbers;
	}


	public void setRunnumbers(List runnumbers) {
		this.runnumbers = runnumbers;
	}


	public String getSelectedRunnumber() {
		return selectedRunnumber;
	}


	public void setSelectedRunnumber(String selectedRunnumber) {
		this.selectedRunnumber = selectedRunnumber;
	}

	public String getSelectedObservingRun() {
		return selectedObservingRun;
	}


	public boolean isCalibrationBeam() {
		return calibrationBeam;
	}


	public void setCalibrationBeam(boolean calibrationBeam) {
		this.calibrationBeam = calibrationBeam;
	}


	public void setSelectedObservingRun(String selectedObservingRun) {
		this.selectedObservingRun = selectedObservingRun;
	}


	public boolean isDisplayTargetCalibrationBeam() {
		return displayTargetCalibrationBeam;
	}


	public void setDisplayTargetCalibrationBeam(boolean displayTargetCalibrationBeam) {
		this.displayTargetCalibrationBeam = displayTargetCalibrationBeam;
	}


	public boolean isEnableTimeAndObservation() {
		return enableTimeAndObservation;
	}


	public void setEnableTimeAndObservation(boolean enableTimeAndObservation) {
		this.enableTimeAndObservation = enableTimeAndObservation;
	}


	public boolean isTargetFirstInRun() {
		return targetFirstInRun;
	}


	public void setTargetFirstInRun(boolean targetFirstInRun) {
		this.targetFirstInRun = targetFirstInRun;
	}


	public boolean isRequiredTargetExposureTime() {
		return requiredTargetExposureTime;
	}


	public void setRequiredTargetExposureTime(boolean requiredTargetExposureTime) {
		this.requiredTargetExposureTime = requiredTargetExposureTime;
	}


	public String getMaxNumberOfTargetsonSameRunReached() {
		return maxNumberOfTargetsonSameRunReached;
	}


	public void setMaxNumberOfTargetsonSameRunReached(
			String maxNumberOfTargetsonSameRunReached) {
		this.maxNumberOfTargetsonSameRunReached = maxNumberOfTargetsonSameRunReached;
	}



	
	
}