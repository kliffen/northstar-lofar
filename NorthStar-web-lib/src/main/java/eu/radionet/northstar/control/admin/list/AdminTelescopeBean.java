// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.list;

import java.util.ArrayList;
import java.util.List;

public class AdminTelescopeBean  {
	private String telescope = null;
	private String description = null;
	private String community = null;
	private List adminProposalBeans = new ArrayList();
    protected int selectedValue = 0;
	protected String changeStatusButton = null;
	protected String viewStatusHistoryButton = null;
    private String viewProposalButton = null;
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public List getAdminProposalBeans() {
		return adminProposalBeans;
	}
	public void setAdminProposalBeans(List proposalBeans) {
		this.adminProposalBeans = proposalBeans;
	}
	public String getTelescope() {
		return telescope;
	}
	public void setTelescope(String telescope) {
		this.telescope = telescope;
	}


    public int getIndex() {
        return selectedValue;
    }



    public AdminProposalBean getAdminProposalBean(int index) {
        resizeAdminProposalBeans(index);
        return (AdminProposalBean) adminProposalBeans.get(index);

    }

    public int getSelectedValue() {
		return selectedValue;
	}
	public void setSelectedValue(int selectedValue) {
		this.selectedValue = selectedValue;
	}
	private void resizeAdminProposalBeans(int index) {
        int newSize = index + 1;
        if (adminProposalBeans.size() < newSize) {
            for (int i = adminProposalBeans.size(); adminProposalBeans.size() < newSize; i++){
                adminProposalBeans.add(new AdminProposalBean());
            }
        }
    }


    /**
     * @return Returns the ViewProposalButton.
     */
    public String getViewProposalButton(int index) {
        return viewProposalButton;
    }

    /**
     * @param submitButton
     *            The submitButton to set.
     */
    public void setViewProposalButton(int index, String value) {
        selectedValue = index;
        viewProposalButton = value;
    }
    public void setChangeStatusButton(int index, String value) {
        selectedValue = index;
        changeStatusButton = value;
    }

    public String getChangeStatusButton(int index) {
        return changeStatusButton;
    }

    public void setViewStatusHistoryButton(int index, String value) {
        selectedValue = index;
        viewStatusHistoryButton = value;
    }

    public String getViewStatusHistoryButton(int index) {
        return viewStatusHistoryButton;
    }

	public String getCommunity() {
		return community;
	}
	public void setCommunity(String community) {
		this.community = community;
	}
}
