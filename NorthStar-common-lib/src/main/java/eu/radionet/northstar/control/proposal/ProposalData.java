// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal;

import java.io.Serializable;

import org.apache.struts.action.ActionForward;

import eu.radionet.northstar.data.entities.FigureFile;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ScientificFile;
import eu.radionet.northstar.data.entities.TechnicalDetailsFile;

public class ProposalData implements Serializable {
	protected String selectedTelescope = null;

	protected String strutsModule = null;
	
	protected String headerImageUri = null;
	
	protected Proposal proposal = null;

	protected TechnicalDetailsFile technicalDetailsFile = null;

	protected ScientificFile scientificFile = null;

	protected FigureFile figureFile = null;
	
	protected String forwardUriAfterSubmit = null;
	
	protected String forwardAfterDeadLinePassed = null;
	protected boolean ignoreDeadLineWarning = false;
	protected boolean ignoreInvalidConfigurationWarning = false;
	protected ActionForward cancelWarningForward = null;
	protected ActionForward successWarningForward = null;
	protected boolean deleteTechnicalDetailsFile = false;
	protected boolean deleteFigureFile = false;
	protected SemesterBean proposalBean = null;

	public Proposal getProposal() {
		return proposal;
	}

	public void setProposal(Proposal proposal) {
		proposalBean = new SemesterBean(proposal.getSemester());
		this.proposal = proposal;
	}

	public String getSelectedTelescope() {
		return selectedTelescope;
	}

	public void setSelectedTelescope(String selectedTelescope) {
		this.selectedTelescope = selectedTelescope;
	}

	public String getStrutsModule() {
		return strutsModule;
	}

	public void setStrutsModule(String strutsModule) {
		this.strutsModule = strutsModule;
	}

	public boolean isDeleteFigureFile() {
		return deleteFigureFile;
	}

	public void setDeleteFigureFile(boolean deleteFigureFile) {
		this.deleteFigureFile = deleteFigureFile;
	}

	public String getHeaderImageUri() {
		return headerImageUri;
	}

	public void setHeaderImageUri(String headerImageUri) {
		this.headerImageUri = headerImageUri;
	}

	public FigureFile getFigureFile() {
		return figureFile;
	}

	public void setFigureFile(FigureFile figureFile) {
		this.figureFile = figureFile;
	}

	public ScientificFile getScientificFile() {
		return scientificFile;
	}

	public void setScientificFile(ScientificFile scientificFile) {
		this.scientificFile = scientificFile;
	}

	public TechnicalDetailsFile getTechnicalDetailsFile() {
		return technicalDetailsFile;
	}

	public void setTechnicalDetailsFile(TechnicalDetailsFile technicalDetailsFile) {
		this.technicalDetailsFile = technicalDetailsFile;
	}

	public boolean isDeleteTechnicalDetailsFile() {
		return deleteTechnicalDetailsFile;
	}

	public void setDeleteTechnicalDetailsFile(boolean deleteTechnicalDetailsFile) {
		this.deleteTechnicalDetailsFile = deleteTechnicalDetailsFile;
	}

	public String getForwardUriAfterSubmit() {
		return forwardUriAfterSubmit;
	}

	public void setForwardUriAfterSubmit(String forwardUriAfterSubmit) {
		this.forwardUriAfterSubmit = forwardUriAfterSubmit;
	}

	public SemesterBean getProposalBean() {
		return proposalBean;
	}

	public void setProposalBean(SemesterBean proposalBean) {
		this.proposalBean = proposalBean;
	}

	public String getForwardAfterDeadLinePassed() {
		return forwardAfterDeadLinePassed;
	}

	public void setForwardAfterDeadLinePassed(String forwardAfterDeadLinePassed) {
		this.forwardAfterDeadLinePassed = forwardAfterDeadLinePassed;
	}

	public boolean isIgnoreDeadLineWarning() {
		return ignoreDeadLineWarning;
	}

	public void setIgnoreDeadLineWarning(boolean ignoreDeadLineWarning) {
		this.ignoreDeadLineWarning = ignoreDeadLineWarning;
	}


	public boolean isIgnoreInvalidConfigurationWarning() {
		return ignoreInvalidConfigurationWarning;
	}

	public void setIgnoreInvalidConfigurationWarning(
			boolean ignoreInvalidConfigurationWarning) {
		this.ignoreInvalidConfigurationWarning = ignoreInvalidConfigurationWarning;
	}

	public ActionForward getCancelWarningForward() {
		return cancelWarningForward;
	}

	public void setCancelWarningForward(ActionForward cancelWarningForward) {
		this.cancelWarningForward = cancelWarningForward;
	}

	public ActionForward getSuccessWarningForward() {
		return successWarningForward;
	}

	public void setSuccessWarningForward(ActionForward successWarningForward) {
		this.successWarningForward = successWarningForward;
	}




}
