// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ApplicantForm.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: ApplicantForm.java,v 1.1 2006-05-03 10:17:19 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.applicants.addedit;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

/**
 * The ApplicantForm provides
 * 
 * @author Bastiaan Verhoef
 *  
 */
public class ApplicantForm extends ActionForm {
	protected Log log = LogFactory.getLog(this.getClass());
	
    private String memberId = null;
    private String forward = null;

    private String name = null;

    private String affiliation = null;

    private String email = null;
    private List countries = new ArrayList();
    private String country = null;
    private String selectedCountry = null;
	
    private String invite = null;
    private List preferredCountries = new ArrayList();

    // inner class for comparing countries
    private class LocaleComparator implements Comparator{
    	public int compare(Object emp1, Object emp2){
    		if(emp1 instanceof Locale && emp2 instanceof Locale){
		    	//parameters are of type Object, so we have to downcast it to Locale objects
		    	String emp1Name = ( (Locale) emp1 ).getDisplayCountry();
		    	String emp2Name = ( (Locale) emp2 ).getDisplayCountry();
		
		    	//uses compareTo method of String class to compare names of the countries
		    	return emp1Name.compareTo(emp2Name);
    		}
    		else{
    			log.warn(" the compare objects are not of the type: Locale ");
    			return -1;
    		}
    	}
    }
    /**
     * @return Returns the affiliation.
     */
    public String getAffiliation() {
        return affiliation;
    }

    /**
     * @param affiliation
     *            The affiliation to set.
     */
    public void setAffiliation(String affiliation) {
        this.affiliation = affiliation;
    }

    /**
     * @return Returns the country.
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country
     *            The country to set.
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return Returns the email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email
     *            The email to set.
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return name;
    }

    /**
     * @param name
     *            The name to set.
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Returns the forward.
     */
    public String getForward() {
        return forward;
    }

    /**
     * @param forward
     *            The forward to set.
     */
    public void setForward(String forward) {
        this.forward = forward;
    }

    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        
        if (AstronValidator.isBlankOrNull(name)) {
            errors.add("name", new ActionMessage(
                    "error.required.applicant.name"));
        } 
        if (AstronValidator.isBlankOrNull(affiliation)) {
            errors.add("affiliation", new ActionMessage(
                    "error.required.applicant.affiliation"));
        }    
        if (AstronValidator.isBlankOrNull(selectedCountry)) {
            errors.add("country", new ActionMessage(
                    "error.required.applicant.country"));
        }
        if (AstronValidator.isBlankOrNull(email)) {
            errors.add("email", new ActionMessage(
                    "error.required.applicant.email"));
        }        
        else if (!AstronValidator.isEmail(email)) {
            errors.add("email", new ActionMessage(
                    "error.invalid.applicant.email"));
        } 

        return errors;
    }

    /**
     * @return Returns the memberId.
     */
    public String getMemberId() {
        return memberId;
    }
    /**
     * @param memberId The memberId to set.
     */
    public void setMemberId(String memberId) {
        this.memberId = memberId;
    }

	public String getInvite() {
		return invite;
	}

	public void setInvite(String invite) {
		this.invite = invite;
	}

	public List getCountries() {
		/// fill the list with countries here, this is not the standard place,
		// but else the list won't be filed after validation fails.
		if(countries.size() ==0){
			try{
				Locale[] localit = Locale.getAvailableLocales();
			    //Set countrySet =  new HashSet(); // Set is the same as list, but allows no duplicates  
		        for(int i=0; i < localit.length; i++){
		        	Locale locale = localit[i]; 
		        	try{
			       		final String contry = locale.getDisplayCountry();
			       		final String abrev = locale.getISO3Country();
			            if (contry.length() > 0 && abrev.length() > 0) {
			               	countries.add(locale);
			            }
		        	}catch(MissingResourceException e){
		           		// do not add country...
		           	}
		        }
			}catch(Exception e){
				log.warn(" error in fetching locales ");
				if(countries.size() ==0){
					countries.add(new Locale("NL"));
					countries.add(new Locale("US"));
				}
			}
			// sort countries
	        try{
	        	LocaleComparator loc = new LocaleComparator();
	        	Collections.sort(countries,loc);
	        }catch(Exception e){
				log.warn(" error in sorting locales ");
	        }
				
	        // remove duplicates
	        try{
	        	Set set = new HashSet();
	       	 	List newList = new ArrayList();
	       	 	for (Iterator iter = countries.iterator(); iter.hasNext(); ) {
	       	 		Locale locas = (Locale) iter.next();
	       	 		if (set.add(locas.getDisplayCountry())){
	       	 			newList.add(locas);
	       	 		}
	       	 	}
	       	 	
	       	 	countries.clear();
	       	 	// add preferred to the top.
		        countries.addAll(preferredCountries);
		        
	       	 	countries.addAll(newList);
	       	 	
	        }catch(Exception e){
				log.warn(" error in removing duplicates ");
	        }
	        
		}
		return countries;
	}
	
	public void setCountries(List countries) {
		this.countries = countries;
	}

	public String getSelectedCountry() {
		return selectedCountry;
	}

	public void setSelectedCountry(String selectedCountry) {
		this.selectedCountry = selectedCountry;
	}

	public List getPreferredCountries() {
		return preferredCountries;
	}

	public void setPreferredCountries(List preferredCountries) {
		this.preferredCountries = preferredCountries;
	}
}