// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.business;

import javax.naming.NamingException;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.exception.AstronMailException;
import eu.radionet.northstar.business.configuration.EmailConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.email.TelescopeEmailGenerator;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.admin.StatusEmail;

/**
 * Class for generating telescope specific emails
 * @author Anton Smit
 */
public class OpticonEmailGenerator extends TelescopeEmailGenerator 
{
    /* These constants are used in the northstar-config.xml. If you change 
     * the tags over there you should change the values here too. This skeleton has
     * the functionality to send out emails to all the particpants of the proposal than
     * only the contact author.
     */
	private static String SUBMIT_OTHER_MEMBERS = "submitted_othermembers";
	private static String SUBMIT_IMMEDIATE = "submitted_immediate";
	private static String RETRACT_OTHER_MEMBERS = "retracted_othermembers";

	
    /**
     * sends the email out to the contact author and the other proposers when a proposal 
     * is submitted
     * @see eu.radionet.northstar.business.email.TelescopeEmailGenerator#sendSubmittedEmail(eu.radionet.northstar.business.configuration.TelescopeConfiguration, eu.radionet.northstar.data.entities.Proposal, nl.astron.useradministration.data.entities.UserAccount)
     */
    public StatusEmail sendSubmittedEmail(
			TelescopeConfiguration telescopeConfiguration, Proposal proposal,
			UserAccount userAccount) throws AstronMailException, NamingException {
    	EmailConfiguration emailConfiguration = getSubmittedEmailConfiguration(proposal,telescopeConfiguration);
    	if (proposal.getSemester().isImmediate()) {
    		emailConfiguration = telescopeConfiguration
			.getEmailConfiguration(SUBMIT_IMMEDIATE);
		}
		String message = emailConfiguration.getMessage();

		EmailConfiguration mailToOthers = null;
		mailToOthers = telescopeConfiguration
			.getEmailConfiguration(SUBMIT_OTHER_MEMBERS);
			
		return super.sendSubmittedEmail(telescopeConfiguration, emailConfiguration,mailToOthers,
				message, mailToOthers.getMessage(), proposal, userAccount);

	}

    /**
     * sends the email out to the contact author and the other proposers when a proposal 
     * is retracted
     */
    public StatusEmail sendRetractedEmail(
			TelescopeConfiguration telescopeConfiguration, Proposal proposal,
			UserAccount userAccount) throws AstronMailException, NamingException {
		EmailConfiguration emailConfiguration = this.getRetractedEmailConfiguration(proposal,telescopeConfiguration);
		String message = emailConfiguration.getMessage();

		EmailConfiguration mailToOthers = telescopeConfiguration
		.getEmailConfiguration(RETRACT_OTHER_MEMBERS);
		return super.sendRetractedEmail(telescopeConfiguration, emailConfiguration,mailToOthers,
				message, mailToOthers.getMessage(),proposal, userAccount);

	}
}
