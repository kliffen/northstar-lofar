// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.additionalissues;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProcessProposalAction;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.AdditionalIssues;
import eu.radionet.northstar.data.entities.Proposal;

public class ProcessAdditionalIssuesAction extends ProcessProposalAction {
	protected Log log = LogFactory.getLog(this.getClass());

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		AdditionalIssuesForm additionalIssuesForm = (AdditionalIssuesForm) form;
		HttpSession session = request.getSession();
		ActionMessages errors = new ActionMessages();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}

		/*
		 * if pressed on Discard changes
		 */
		if (isCancelled(request)) {

			return cancel(mapping, request, ownUserAccount);

		}
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		
		// copy Form data to proposaldata
		Proposal proposal = proposalData.getProposal();
		AdditionalIssues additionalIssues = proposal.getAdditionalIssues();
				
		if (additionalIssues == null) {
			additionalIssues = new AdditionalIssues();
			proposalData.getProposal().setAdditionalIssues(additionalIssues);
		}

		additionalIssues.setLinkedProposals(additionalIssuesForm.isLinkedProposals());
		additionalIssues.setLinkedProposalsSpecifics(additionalIssuesForm.getLinkedProposalsSpecifics());

		additionalIssues.setLinkedProposalsElsewhere(additionalIssuesForm.isLinkedProposalsElsewhere());
		additionalIssues.setLinkedProposalsElsewhereSpecifics(additionalIssuesForm.getLinkedProposalsElsewhereSpecifics());

		additionalIssues.setPreviousAllocations(additionalIssuesForm.isPreviousAllocations());
		additionalIssues.setPreviousAllocationsSpecifics(additionalIssuesForm.getPreviousAllocationsSpecifics());

		additionalIssues.setAdditionalRemarks(additionalIssuesForm.getAdditionalRemarks());
		additionalIssues.setRelatedPublications(additionalIssuesForm.getRelatedPublicationsText());
		additionalIssues.setRelatedPreviousInvolvedProposal(additionalIssuesForm.getRelatedPreviousInvolvedProposalText());
		/**/
		
		
		additionalIssues.setGrantNumber(additionalIssuesForm.getGrantNumber());

		additionalIssues.setNewObserverExperience(additionalIssuesForm.getNewObserverExperience());
		additionalIssues.setSponsoring(additionalIssuesForm.isEnableSponsoring() );
		if(additionalIssuesForm.isEnableSponsoring() ){
			additionalIssues.setSponsorDetails(additionalIssuesForm.getSponsorDetails() );
		}

		/*
		 * if forward in form is set, forward to an other pages. (See jsp's for
		 * more info)
		 */
		if (!AstronValidator.isBlankOrNull(additionalIssuesForm.getForward())) {
			return mapping.findForward(additionalIssuesForm.getForward());
		}
        /*
         * if pressed on add thesis button
         */
        else if (additionalIssuesForm.getAddThesisButton() != null) {
            return mapping.findForward("addthesis");
        }
        /*
         * if pressed on edit thesis button
         */
        else if (additionalIssuesForm.getEditThesisButton(0) != null) {
            return ParameterAppender.append(mapping
                    .findForward("editthesis"), "thesisId", additionalIssuesForm
                    .getSelectedThesisId()
                    + "");
        }
        /*
         * if pressed on delete applicant button
         */
        else if (additionalIssuesForm.getDeleteThesisButton(0) != null) {
            return ParameterAppender.append(mapping
                    .findForward("deletethesis"), "thesisId", additionalIssuesForm
                    .getSelectedThesisId()
                    + "");
        }

		ActionForward forward = processGeneralButtons(additionalIssuesForm,
				mapping, request, errors, proposalData, ownUserAccount);
		if (forward != null) {
			return forward;
		}
		return mapping.getInputForward();
	}

}
