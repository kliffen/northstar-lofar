// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.deadline;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lowagie.text.BadElementException;
import com.lowagie.text.DocumentException;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.ConfigurationUtil;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.pdf.ProposalWriter;
import eu.radionet.northstar.business.util.PdfUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.WebDAVFileRepository;
import eu.radionet.northstar.data.entities.Proposal;

public class WebDAVPdfUtils {
	private Log log = LogFactory.getLog(this.getClass());

	private static HashMap headers = new HashMap();


	public void storeProposalAsPdf(Proposal proposal,
			ServletContext servletcontext) throws DatabaseException {

		try {
			WebDAVFileRepository fileRepository = new WebDAVFileRepository();
			byte[] proposalPdf = getProposalPdf(proposal, servletcontext);
			fileRepository.store(proposal, proposalPdf);
		} catch (ConnectionException ce) {
			log
					.fatal("Cannot instantiate ProposalDelegate: "
							+ ce.getMessage());
		} catch (IOException ioe) {
			log.error(AstronConverter.stackTraceToString(ioe));
		} catch (BadElementException ioe) {
			log.error(AstronConverter.stackTraceToString(ioe));
		} catch (DocumentException ioe) {
			log.error(AstronConverter.stackTraceToString(ioe));
		} catch (InvalidConfigurationException icf) {
			log.error(AstronConverter.stackTraceToString(icf));
		} catch (Exception icf) {
			log.error(AstronConverter.stackTraceToString(icf));
		}
	}

	protected byte[] getProposalPdf(Proposal proposal,
			ServletContext servletcontext) throws DatabaseException,
			IOException, BadElementException, DocumentException,
			InvalidConfigurationException {

		long startTime = System.currentTimeMillis();
		/*
		 * create document writer
		 */
		ProposalWriter documentWriter = null; // new ProposalWriter();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		documentWriter = configurationUtil
				.getProposalPdfWriter(telescopeConfiguration);
		documentWriter.setHeaderImage(getHeader(telescopeConfiguration
				.getPdfHeaderImageUri(), servletcontext));

		/*
		 * if justification is not null
		 */
		if (proposal.getJustification() != null) 
		{
			PdfUtils pdfUtils = new PdfUtils();
			pdfUtils.addFilesToProposalWriter(documentWriter, proposal, servletcontext);
		}
		/*
		 * write pdf
		 */
		documentWriter.write(proposal, baos, servletcontext);
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			log.info("PDF generation duration: " + (endTime - startTime)
					+ " ms");
		}
		return baos.toByteArray();
	}

	/**
	 * The generateProposalPdf method generates proposal from database
	 * 
	 * @param proposal
	 * @param request
	 * @return
	 * @throws DatabaseException
	 * @throws IOException
	 * @throws BadElementException
	 * @throws DocumentException
	 * @throws InvalidConfigurationException
	 */
	public byte[] getProposalPreviewPdf(Proposal proposal,
			HttpServletRequest request) throws DatabaseException, IOException,
			BadElementException, DocumentException,
			InvalidConfigurationException {

		long startTime = System.currentTimeMillis();
		HttpSession session = request.getSession();
		/*
		 * create document writer
		 */
		ProposalWriter documentWriter = null; // new ProposalWriter();

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ConfigurationUtil configurationUtil = ConfigurationUtil.getInstance();
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
				.getTelescopeConfiguration(proposal);
		documentWriter = configurationUtil
				.getProposalPdfWriter(telescopeConfiguration);
		documentWriter.setHeaderImage(getHeader(telescopeConfiguration
				.getPdfHeaderImageUri(), session.getServletContext()));

		/*
		 * if justification is not null
		 */
		if (proposal.getJustification() != null) 
		{
			PdfUtils pdfUtils = new PdfUtils();
			pdfUtils.addFilesToProposalWriter(documentWriter, proposal, session.getServletContext());
		}
		/*
		 * write pdf
		 */
		documentWriter.write(proposal, baos, session.getServletContext());
		long endTime = System.currentTimeMillis();
		if (log.isDebugEnabled()) {
			UserAccount ownUserAccount = (UserAccount) session
					.getAttribute(Constants.OWN_USERACCOUNT);
			log.info(LogMessage
					.getMessage(ownUserAccount, "PDF generation duration: "
							+ (endTime - startTime) + " ms"));
		}
		return baos.toByteArray();
	}

	/**
	 * The getHeader methods retrieves header picture from war file
	 * 
	 * @param headerName
	 * @param servletcontext
	 * @return
	 */
	protected byte[] getHeader(String headerName, ServletContext servletcontext) {
		byte[] data = (byte[]) headers.get(headerName);

		InputStream headerInputStream = servletcontext
				.getResourceAsStream(headerName);

		if (headerInputStream == null) {
			return null;
		}
		try {
			data = new byte[headerInputStream.available()];
			headerInputStream.read(data);
			headers.put(headerName, data);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (headerInputStream != null) {
				try {
					headerInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		// }
		return data;
	}


}
