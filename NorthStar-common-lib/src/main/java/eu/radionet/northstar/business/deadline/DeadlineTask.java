// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.deadline;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;

import javax.servlet.ServletConfig;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Semester;

public class DeadlineTask extends TimerTask {
	private Log log = LogFactory.getLog(this.getClass());

	private NorthStarDelegate northStarDelegate = null;

	protected Semester deadLineSemester = null;

	protected ServletConfig servletConfig = null;

	public DeadlineTask(Semester semester, ServletConfig servletConfig) {
		this.deadLineSemester = semester;
		this.servletConfig = servletConfig;
	}

	public void run() {
		String semesterString = "(" + deadLineSemester.getSemester() + ","
		+ deadLineSemester.getTelescope() + ","
		+ deadLineSemester.getCategory().getCode() + ","
		+ deadLineSemester.getCommunity() + ") ";

		List proposals = new ArrayList();
		UserAccount maintenanceAccount = null;
		try {
			northStarDelegate = NorthStarDelegate.getInstance();
			Semester semester = northStarDelegate.getSemester(deadLineSemester.getId());
			log.info("Deadline passed of semester at " + semesterString
					+ AstronConverter.toDateString(semester.getDeadLine()));		

			ProposalDelegate proposalDelegate = ProposalDelegateFactory
					.getProposalDelegate(semester);
			proposalDelegate.updateSemesters(semester);

			proposals = northStarDelegate
					.getSubmittedProposalsBySemesterId(semester.getId());
			maintenanceAccount = northStarDelegate
					.getUserAccountByName(NorthStarConfiguration
							.getMaintenanceAccount());
			int numberOfProposals = 0;

			WebDAVPdfUtils pdfUtils = new WebDAVPdfUtils();
			/*
			 * iterate through all own proposals
			 */
			for (int i = 0; i < proposals.size(); i++) {
				Proposal proposal = (Proposal) proposals.get(i);
				try {

					proposalDelegate.storeAsUnderReview(proposal,
							maintenanceAccount);
					pdfUtils.storeProposalAsPdf(proposal, servletConfig
							.getServletContext());
					numberOfProposals++;

				} catch (Exception e) {
					log.error(LogMessage
							.getMessage(
									"Cannot store proposal " + proposal.getId() + " of semester id "
											+ semester.getId() + " : "
											+ e.getMessage(), e));
				}

			}
			log.info(semesterString
					+ " #proposal status changed under review: "
					+ numberOfProposals);
			semester.setClosed(true);
			northStarDelegate.storeSemester(semester);
		} catch (DatabaseException de) {
			log.error(LogMessage.getMessage(
					"Database problems with " + semesterString + " : " + de.getMessage(), de));
		} catch (InvalidConfigurationException de) {
			log.error(LogMessage.getMessage(
					"Configuration problems with " + semesterString + " : " + de.getMessage(), de));
		} catch (Exception e) {
			log.error(LogMessage.getMessage(
					"Problems with " + semesterString + " : " + e.getMessage(), e));
		}
	}

	public boolean cancel() {
		String semesterString = "(" + deadLineSemester.getSemester() + ","
		+ deadLineSemester.getTelescope() + ","
		+ deadLineSemester.getCategory().getCode() + ","
		+ deadLineSemester.getCommunity() + ") ";
		log.info("Cancel deadline thread of " + semesterString);
		return super.cancel();
	}
}