// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SetUpApplicantAction.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: SetUpEditApplicantAction.java,v 1.3 2008-04-08 08:02:58 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.applicants.addedit;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The SetUpEditApplicantAction provides
 * 
 * @author Hanno Holties
 *  
 */
public class SetUpEditApplicantAction extends LockedAction {
	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

		  String storedCountry = null;
        ApplicantForm applicantForm = (ApplicantForm) form;
        if (!AstronValidator.isBlankOrNull(applicantForm.getMemberId())) {
    		HttpSession session = request.getSession();
        	ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
			Proposal proposal = proposalData.getProposal();
			NonRegisteredMember member = (NonRegisteredMember) proposal.getMembers().get(AstronConverter.toInteger(applicantForm.getMemberId()).intValue());
            applicantForm.setName(member.getName());
            applicantForm.setEmail(member.getEmail());
            //applicantForm.setCountry(member.getCountry());
            storedCountry = member.getCountry();
            applicantForm.setAffiliation(member.getAffiliation());
/*			if (member.isInvite()){
				return mapping.findForward(Constants.CONFIRM);
			}*/
			if (collaborationDelegate.getInvitation(proposal, member) != null){
				return mapping.findForward(Constants.CONFIRM);
			}
			

        }
        Locale[] localit = Locale.getAvailableLocales();
        
        for(int i=0; i < localit.length; i++){
        	Locale locale = localit[i]; 
       		final String contry = locale.getDisplayCountry();
           if (contry.length() > 0) {
	           	if(storedCountry != null && contry.equalsIgnoreCase(storedCountry)){
	           		applicantForm.setSelectedCountry(locale.getISO3Country());
	           	}
           }
        }
        if(applicantForm.getSelectedCountry() == null){
        	applicantForm.setSelectedCountry(request.getLocale().getISO3Country());
        }
        

        return mapping.findForward(Constants.SUCCESS);

    }
}