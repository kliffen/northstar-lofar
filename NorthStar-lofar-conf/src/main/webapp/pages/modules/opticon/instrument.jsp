<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/astron.tld" prefix="astron"%>

<logic:equal name="instrumentBean" property="displayPipelineOptions" value="false">

<tr>
	<td class="header" colspan="2"><astron:label key="label.opticon.observation.telescope.configuration" /><!--Telescope configuration :--></td>
</tr>	

<tr>
	<td colspan="2"><tiles:get name="box_header_no_top" />
	<table width="100%">
		<tr>
		    <td width="40"> &nbsp; </td>
			<td width="60%"> &nbsp; </td> 
		</tr>
		

	<!-- begin telescope config -->
	
		<!-- the instrument bean is defined as a request attribute in the observationSetUp page -->
		<logic:equal name="instrumentBean" property="displayTelescopeDropbox" value="true">		
			<tr>
				<td class="right"><astron:label key="label.opticon.observation.choose.telescope" /><!--Choose a telescope&nbsp;:&nbsp; --></td>
				<td class="left"><bean:define id="telescopeConfigurations" name="instrumentBean" property="telescopeConfigurations" /> 
					<html:select name="instrumentBean" property="selectedTelescopeConfiguration" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
						<html:options collection="telescopeConfigurations" property="value"	labelProperty="label" />
					</html:select><span class="error">*</span>
				</td>
			</tr>
		</logic:equal>
		<logic:equal name="instrumentBean" property="displayTelescopeDropbox" value="false">		
			<tr>
				<td class="right"><astron:label key="label.opticon.observation.telescope" /><!--Telescope&nbsp;:&nbsp;--></td>
				<td class="left"><bean:write name="instrumentBean" property="selectedTelescopeConfiguration"/>
					<html:hidden name="instrumentBean" property="selectedTelescopeConfiguration" />
				</td>
			</tr>
		</logic:equal>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
	<logic:equal name="instrumentBean" property="enableCombinedOutputProductSelection" value="true">
        	<jsp:include page="combinedoutputproducts.jsp" />
	</logic:equal>		
	
	<logic:equal name="instrumentBean" property="enableCombinedOutputProductSelection"  value="false">	
		<logic:notEmpty name="instrumentBean" property="selectedTelescopeConfiguration">
				<tr>
					<td class="right"><astron:label key="label.opticon.observation.choose.instrument" /><!--Choose an instrument&nbsp;:&nbsp;--></td>
					<td class="left"><bean:define id="instrumentConfigurations" name="instrumentBean" property="instrumentConfigurations" /> 
						<html:select name="instrumentBean" property="selectedInstrumentConfiguration" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
							<html:options collection="instrumentConfigurations" property="value"	labelProperty="label" />
						</html:select><span class="error">*</span>
					</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
				<logic:notEmpty name="instrumentBean" property="selectedInstrumentConfiguration">
							<logic:notEmpty name="instrumentBean" property="instrumentUrl">
								 <tr>
			  							<td class="right" valign="top"><astron:label key="label.opticon.observation.documentation" /><!--  Instrument documentation : --></td>
			  							<td class="left"><a href="<bean:write name="instrumentBean" property="instrumentUrl" />" target="_blank" >
																 <bean:write name="instrumentBean" property="instrumentUrl" /> </a>
			  							</td>
			 					 </tr><tr>
										<td colspan="2">&nbsp;</td>
								 </tr>
		 					 </logic:notEmpty>
							<logic:equal name="instrumentBean" property="displayStation" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.station" /><!--Choose Station&nbsp;:&nbsp;--></td>
								<td class="left"><bean:define id="stations"
									name="instrumentBean" property="stations" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedStation" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
									<html:options collection="stations" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredStation" value="true">
										<span class="error"><astron:errors property="selectedStation" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredStation" value="false">
										<span class="error"><astron:errors property="selectedStation" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displaySingleStationSpecifics" value="true">
							 <tr>
		  							<td class="right" valign="top"><astron:label key="label.opticon.observation.specifics" /><!--  Any preferences? : --></td>
		  							<td class="left"><html:text name="instrumentBean" property="singleStationSpecifics" size="44" />
		  							</td>
		 					 </tr>
		 					 </logic:equal>
							<logic:equal name="instrumentBean" property="selectedStation" value="Custom">
							 <tr>
		  							<td class="right" valign="top"><astron:label key="label.opticon.observation.specify" /><!-- Please Specify : --></td>
		  							<td class="left"><html:text name="instrumentBean" property="customStationSpecifics" size="44" />
		  							</td>
		 					 </tr>
							 <tr>
		  							<td class="right" valign="top">Format :&nbsp;</td>
		  							<td class="left">Optional free text (no brackets!) and either a number between brackets, e.g."(10)", or the list of requested station between square brackets; selection from:<br>
		  							[CS001,CS002,CS003,CS004,CS005,CS006,CS007,CS011,CS013,CS017,CS021,CS024,CS026,CS028,CS030,CS031,CS032,CS101,CS103,CS201,CS301,CS302,CS401,CS501,<br>
		  							RS106,RS205,RS208,RS210,RS305,RS306,RS307,RS310,RS406,RS407,RS409,RS503,RS508,RS509,DE601,DE602,DE603,DE604,DE605,FR606,SE607,UK608,DE609,PL610,PL611,PL612,IE613]
		  							</td>
		 					 </tr>
		 					 <tr><td clospan="2">&nbsp;</td></tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayClock" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.clock" /><!--Choose Clock speed&nbsp;:&nbsp;--></td>
								<td class="left"><bean:define id="clocks"
									name="instrumentBean" property="clocks" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedClock" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
									<html:options collection="clocks" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredClock" value="true">
										<span class="error"><astron:errors property="selectedClock" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredClock" value="false">
										<span class="error"><astron:errors property="selectedClock" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayAntenna" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.antenna" /><!-- Choose Antenna&nbsp;:&nbsp;--></td>
								<td class="left"><bean:define id="antennas"
									name="instrumentBean" property="antennas" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedAntenna"  onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
									<html:options collection="antennas" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredAntenna" value="true">
										<span class="error"><astron:errors property="selectedAntenna" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredAntenna" value="false">
										<span class="error"><astron:errors property="selectedAntenna" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayOneFilter" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.filter" /><!--Choose filter&nbsp;:&nbsp;--></td>
								<td class="left"><bean:define id="oneFilters"
									name="instrumentBean" property="oneFilters" /> 
									<html:select styleClass="selectwide" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()" name="instrumentBean" property="selectedOneFilter">
									<html:options collection="oneFilters" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredOneFilter" value="true">
										<span class="error"><astron:errors property="selectedOneFilter" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredOneFilter" value="false">
										<span class="error"><astron:errors property="selectedOneFilter" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displaySubarrayPointings" value="true">
								  <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.choose.defaultsubarraypointings" /></td>
								  		<td class="left">
								  			<html:radio name="instrumentBean" property="defaultSubarrayPointings" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
											<html:radio name="instrumentBean" property="defaultSubarrayPointings" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
								  		</td>
								  </tr>
								<logic:equal name="instrumentBean" property="defaultSubarrayPointings" value="false">
								  <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.choose.subarraypointings" /></td>
								  		<td class="left"> <html:text name="instrumentBean" property="subarrayPointings" size="6" /></td>
								  </tr>
								</logic:equal>
							</logic:equal>

							<logic:equal name="instrumentBean" property="displayObservationNoiseLevel" value="true">
								<tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.channels" /></td>
								  		<td class="left"> 
								  		<logic:iterate id="channelItem" name="instrumentBean" property="channellist">
								        	<html:radio name="instrumentBean" property="channels" idName="channelItem" value="value" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
								                 <bean:write name="channelItem" property="label"/>
								         	</html:radio>
								        </logic:iterate>
								  		 
								  		<span class="error"><astron:errors property="channels" ifnotexist="*" /></span>
								  		</td>
								  </tr>
								<tr>
									<td class="right" valign="top"><astron:label key="label.opticon.observation.noiselevel" /></td>
									<td class="left"> <html:text name="instrumentBean" property="observationNoiseLevel" size="6" />
									<logic:equal name="instrumentBean" property="requiredObservationNoiseLevel" value="true">
										<span class="error"><astron:errors property="observationNoiseLevel" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredObservationNoiseLevel" value="false">
										<span class="error"><astron:errors property="observationNoiseLevel" ifnotexist="" /></span>
									</logic:equal>	
									</td>
								</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayDirectDataStorage" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.direct.datastorage" /><!--Integration time (seconds)&nbsp;:&nbsp;--></td>
								<td class="left">									
									<html:radio styleClass="radio" name="instrumentBean" property="directDataStorage" value="true" onclick="document.forms[0].submit()" >yes</html:radio>
									<html:radio styleClass="radio" name="instrumentBean" property="directDataStorage" value="false"  onclick="document.forms[0].submit()">no</html:radio></td>

								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayInstrumentDetails" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.specify" /><!--Please specify&nbsp;:&nbsp; --></td>
								<td class="left">
									<html:textarea property="instrumentDetails" cols="60" rows="2"/>
									<logic:equal name="instrumentBean" property="requiredFilterDetails" value="true">
										<span class="error">*</span>
									</logic:equal>
								</td>
							</tr>
							</logic:equal>		
							<logic:equal name="instrumentBean" property="displayFrameExposureTime" value="true">
								<tr>
									<td class="right" valign="top"><astron:label key="label.opticon.observation.special.exposuretime" /></td>
									<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="nonDefaultFrameExposureTime" value="true"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
													<html:radio styleClass="radio" name="instrumentBean" property="nonDefaultFrameExposureTime" value="false"  onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
									</td>
								</tr>
								<logic:equal name="instrumentBean" property="nonDefaultFrameExposureTime" value="true">								
								<tr>
									<td class="right"><astron:label key="label.opticon.observation.specify" /><!--Please specify&nbsp;:&nbsp; --></td>
									<td class="left">
										<html:textarea name="instrumentBean" property="nonDefaultFrameExposureTimeDetails" cols="60" rows="2"/>
										<logic:equal name="instrumentBean" property="requiredNonDefaultFrameExposureTimeDetails" value="true">
											<span class="error"><astron:errors property="nonDefaultFrameExposureTimeDetails" ifnotexist="*" /></span>
										</logic:equal>
										<logic:equal name="instrumentBean" property="requiredNonDefaultFrameExposureTimeDetails" value="false">
											<span class="error"><astron:errors property="nonDefaultFrameExposureTimeDetails" ifnotexist="" /></span>
										</logic:equal>									
									</td>
								</tr>							
								</logic:equal>	
							</logic:equal>			
							<% String styleClass = ""; %>			
							<logic:equal name="instrumentBean" property="displayFilters" value="true">
							<tr>
								<td> &nbsp; </td>
								<td>
									<table align="left" class="projecttable">
										<TR>
							 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.filters" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
										</TR>							
										<%	int i=0; %>									
										<logic:iterate id="filterItem" name="instrumentBean" property="filters">
											<% styleClass = (i++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedFilters">
 													<bean:write name="filterItem" property="value" filter="false"/>
												</html:multibox></td><td class="tablefield_pri<%=styleClass%>">
												<bean:write name="filterItem" property="label"/>
											</td></tr>
										</logic:iterate>
										<logic:equal name="instrumentBean" property="displayCustomFilter" value="true">
											<% styleClass = (i++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedFilters" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Other/Custom</html:multibox>
												</td><td class="tablefield_pri<%=styleClass%>">
												Other/Custom
											</td></tr>
										</logic:equal>
									</table>
								<logic:equal name="instrumentBean" property="displayFiltersw2" value="true">
									<table align="left" class="projecttable">
										<TR>
							 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.filtersw2" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
										</TR>							
										<%	int j=0; %>										
										<logic:iterate id="filterItemw2" name="instrumentBean" property="filtersw2">
											<% styleClass = (j++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedFiltersw2">
 													<bean:write name="filterItemw2" property="value" filter="false"/>
												</html:multibox></td><td class="tablefield_pri<%=styleClass%>">
												<bean:write name="filterItemw2" property="label"/>
											</td></tr>
										</logic:iterate>
																						
									</table>
								</logic:equal>
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayGrismw" value="true">
							<tr><<td> &nbsp; </td>
								<td>
									<table align="left" class="projecttable">
										<TR>
							 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.choose.grism" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
										</TR>							
										<%	int j=0; %>										
										<logic:iterate id="grismwItem" name="instrumentBean" property="grismw">
											<% styleClass = (j++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedGrismw" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
 													<bean:write name="grismwItem" property="value" filter="false"/>
												</html:multibox></td><td class="tablefield_pri<%=styleClass%>">
												<bean:write name="grismwItem" property="label"/>
											</td></tr>
										</logic:iterate>
																						
									</table>
								</td></tr>	
							</logic:equal>
							<logic:equal name="instrumentBean" property="displaySlitw" value="true">
							<tr><<td> &nbsp; </td>
								<td>
									<table align="left" class="projecttable">
									<TR>
							 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.choose.slits" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
										</TR>							
										<%	int j=0; %>										
										<logic:iterate id="slitwItem" name="instrumentBean" property="slitw">
											<% styleClass = (j++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedSlitw" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
 													<bean:write name="slitwItem" property="value" filter="false"/>
												</html:multibox></td><td class="tablefield_pri<%=styleClass%>">
												<bean:write name="slitwItem" property="label"/>
											</td></tr>
										</logic:iterate>
																						
									</table>
								</td></tr>	
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayGratingw" value="true">
							<tr><<td> &nbsp; </td>
								<td>
									<table align="left" class="projecttable">
									<TR>
							 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.choose.gratings" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
										</TR>							
										<%	int j=0; %>										
										<logic:iterate id="gratingwItem" name="instrumentBean" property="gratingw">
											<% styleClass = (j++ % 2 == 0 ? "" : "_odd"); %>
											<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
												<html:multibox name="instrumentBean" property="selectedGratingw" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
 													<bean:write name="gratingwItem" property="value" filter="false"/>
												</html:multibox></td><td class="tablefield_pri<%=styleClass%>">
												<bean:write name="gratingwItem" property="label"/>
											</td></tr>
										</logic:iterate>
																						
									</table>
								</td></tr>	
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayFilterDetails" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.specify" /><!--Please specify&nbsp;:&nbsp; --></td>
								<td class="left">
									<html:textarea name="instrumentBean" property="filterDetails" cols="60" rows="2"/>
									<logic:equal name="instrumentBean" property="requiredFilterDetails" value="true">
										<span class="error">*</span>
									</logic:equal>
								</td>
							</tr>
							</logic:equal>							
							<logic:equal name="instrumentBean" property="displayMode" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.mode" /><!--Choose mode&nbsp;:&nbsp; --></td>
								<td class="left">
									<bean:define id="modes" name="instrumentBean" property="modes" /> 
									<html:select name="instrumentBean" styleClass="selectwide" property="selectedMode">
										<html:options collection="modes" property="value" labelProperty="label" />
									</html:select>
									<logic:equal name="instrumentBean" property="requiredMode" value="true">
										<span class="error">*</span>
									</logic:equal>
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayPolarimetry" value="true">
							<tr>
								<td class="right" valign="top"><astron:label key="label.opticon.observation.polarimetry" /></td>
								<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="polarimetry" value="true" >Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="polarimetry" value="false" >No</html:radio></td>
							</tr>	
							</logic:equal>							
							<logic:equal name="instrumentBean" property="displayGuideline" value="true">
							<tr>
							<td class="right"><astron:label key="label.opticon.observation.guideline" /></td>
							<td class="left">
							<a href="<bean:write name="instrumentBean" property="guidelineUrl" />" >
							 <bean:write name="instrumentBean" property="guidelineUrl" /> </a>
							</td> </tr>
							<tr>
								<td class="right" valign="top">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Accept guideline? :&nbsp;</td>
								<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="guideline" value="true" >Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="guideline" value="false" >No</html:radio>
												<span class="error"><astron:errors property="guideline" ifnotexist="*" /></span>				
								</td>
							</tr>	
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayWavelength" value="true">
							<tr>
								<td class="right">
									<astron:label key="label.opticoninstrument.wavelength"/>&nbsp;
								</td>
								<td class="left"><html:text name="instrumentBean" property="wavelength" size="15" />
									<logic:equal name="instrumentBean" property="requiredWavelength" value="true">
										<span class="error"><astron:errors property="wavelength" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredWavelength" value="false">
										<span class="error"><astron:errors property="wavelength" ifnotexist="" /></span>
									</logic:equal>										
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayOrder" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.order" /></td>
								<td class="left"><html:text name="instrumentBean" property="order" size="15" />
									<logic:equal name="instrumentBean" property="requiredOrder" value="true">
										<span class="error"><astron:errors property="order" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredOrder" value="false">
										<span class="error"><astron:errors property="order" ifnotexist="" /></span>
									</logic:equal>		
								</td>									
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displaySlits" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.slits" /></td>
								<td class="left"><bean:define id="slits"
									name="instrumentBean" property="slits" /> <html:select
									 styleClass="selectwide" name="instrumentBean" property="selectedSlit">
									<html:options collection="slits" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredSlits" value="true">
										<span class="error"><astron:errors property="selectedSlit" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredSlits" value="false">
										<span class="error"><astron:errors property="selectedSlit" ifnotexist="" /></span>
									</logic:equal>									
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displaySlitPositionAngle" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.slit.angle" /></td>
								<td class="left"><html:text name="instrumentBean" property="slitPositionAngle" size="15" />
									<logic:equal name="instrumentBean" property="requiredSlitPositionAngle" value="true">
										<span class="error"><astron:errors property="slitPositionAngle" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredSlitPositionAngle" value="false">
										<span class="error"><astron:errors property="slitPositionAngle" ifnotexist="" /></span>
									</logic:equal>										
								</td>
							</tr>
							</logic:equal>							
							<logic:equal name="instrumentBean" property="displayGrating" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.grating" /></td>
								<td class="left"><bean:define id="gratings"
									name="instrumentBean" property="gratings" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedGrating">
									<html:options collection="gratings" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredGrating" value="true">
										<span class="error"><astron:errors property="selectedGrating" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredGrating" value="false">
										<span class="error"><astron:errors property="selectedGrating" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayCCD" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.ccd" /></td>
								<td class="left"><bean:define id="CCDs"
									name="instrumentBean" property="CCDs" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedCCD">
									<html:options collection="CCDs" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredCCD" value="true">
										<span class="error"><astron:errors property="selectedCCD" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredCCD" value="false">
										<span class="error"><astron:errors property="selectedCCD" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayOrderFilter" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.separation.filter" /></td>
								<td class="left">
									<bean:define id="orderFilters" name="instrumentBean" property="orderFilters" /> 
									<html:select name="instrumentBean" property="selectedOrderFilter">
										<html:options collection="orderFilters" property="value" labelProperty="label" />
									</html:select>
									<logic:equal name="instrumentBean" property="requiredOrderFilter" value="true">
										<span class="error">*</span>
									</logic:equal>
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayReadOut" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.read.mode" /></td>
								<td class="left">
									<bean:define id="readOuts" name="instrumentBean" property="readOuts" /> 
									<html:select name="instrumentBean" property="selectedReadOut">
										<html:options collection="readOuts" property="value" labelProperty="label" />
									</html:select>
									<logic:equal name="instrumentBean" property="requiredReadOut" value="true">
										<span class="error">*</span>
									</logic:equal>
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayGrism" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.choose.grism" /></td>
								<td class="left"><bean:define id="grisms"
									name="instrumentBean" property="grisms" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedGrism">
									<html:options collection="grisms" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredGrism" value="true">
										<span class="error"><astron:errors property="selectedGrism" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredGrism" value="false">
										<span class="error"><astron:errors property="selectedGrism" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>							
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayMicrostepping" value="true">
							<tr>
								<td class="right">Choose microstepping&nbsp;:&nbsp;</td>
								<td class="left"><bean:define id="microstepping"
									name="instrumentBean" property="microstepping" /> <html:select
									styleClass="selectwide" name="instrumentBean" property="selectedMicrostepping">
									<html:options collection="microstepping" property="value"
										labelProperty="label" />
								</html:select>
									<logic:equal name="instrumentBean" property="requiredMicrostepping" value="true">
										<span class="error"><astron:errors property="selectedMicrostepping" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredMicrostepping" value="false">
										<span class="error"><astron:errors property="selectedMicrostepping" ifnotexist="" /></span>
									</logic:equal>									
								</td>
							</tr>						
							</logic:equal>									
							<logic:equal name="instrumentBean" property="displayCoronagraphicMask" value="true">
							<tr>
								<td class="right" valign="top">Coronagraphic Mask? :&nbsp;</td>
								<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="coronagraphicMask" value="true" >Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="coronagraphicMask" value="false" >No</html:radio></td>
							</tr>							
							</logic:equal>	
							
							<logic:equal name="instrumentBean" property="displayPipelineOptions" value="false">
							<logic:equal name="instrumentBean" property="displayComments" value="true">														
							<tr>
								<td class="right" valign="top">Configuration comments&nbsp;:&nbsp;</td>
								<td class="left"><html:textarea name="instrumentBean" property="comments" cols="50" rows="2"/>
									<logic:equal name="instrumentBean" property="requiredComments" value="true">
										<span class="error"><astron:errors property="comments" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredComments" value="false">
										<span class="error"><astron:errors property="comments" ifnotexist="" /></span>
									</logic:equal>										
								</td>
							</tr>	
							</logic:equal>
							</logic:equal>	
							<logic:equal name="instrumentBean" property="displayPostBeamformed" value="true">
								<tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.cohstokes" /></td>
								  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="cohstokes" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="cohstokes" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
					
								  		<span class="error"><astron:errors property="cohstokes"/></span>
								  		</td>
								  </tr>
								  <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.incohstokes" /></td>
								  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="incohstokes" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="incohstokes" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
					
								  		<span class="error"><astron:errors property="incohstokes"/></span>
								  		</td>
								  </tr>
								  <!-- Disabled as requested by science support 6/9/12 
								  <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.complexvoltage" /></td>
								  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="complexVoltage" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="complexVoltage" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
					
								  		<span class="error"><astron:errors property="complexVoltage" ifnotexist="*" /></span>
								  		</td>
								  </tr>
								  -->
								  <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.flyseye" /></td>
								  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="flysEye" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="flysEye" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
								  		<span class="error"><astron:errors property="flysEye"/></span>
								  		This mode implies that all stations point in the same direction. For a proper Fly's Eye, select Parallel observations.
								  		</td>
								  </tr>
								  <!-- Display explanation as requested by science support 12/9/12 FIXME implemented as a non configurable quick fix --> 
								  <logic:equal name="instrumentBean" property="flysEye" value="true">
								  <tr>
								  		<td align="right" valign="top">&nbsp;</td>
								  		<td class="left">
<pre>
In order to include "Fly's eye" observations in the data size calculation,
the total number of stations that is being used needs to be added to the
number of coherent stokes beams, if any, in "Tied array beams"
</pre>
								        </td>
								  </tr>	
								  			  
								  </logic:equal>
								  <!-- enabled again as stated on the website-->
								  <!--
								  <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.rawvoltage" /></td>
								  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="rawVoltage" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
												<html:radio styleClass="radio" name="instrumentBean" property="rawVoltage" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
					
								  		<span class="error"><astron:errors property="rawVoltage" /></span>
								  		</td>
								  </tr>
								  -->
								  <logic:equal name="instrumentBean" property="cohstokes" value="true">
									  <tr>
									  		<td align="right" valign="top"><astron:label key="label.opticon.observation.post.polarizations" /></td>
									  		<td class="left">
									  		    <bean:define id="polarizationlist" name="instrumentBean" property="polarizations"/>	
									        	<html:select name="instrumentBean" property="selectedPolarizations" size="1" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
									                 <html:options collection="polarizationlist" property="value" labelProperty="value" />
									         	</html:select>
									        </td>
									  </tr>	
									   <tr>
									  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.cohtabeams" /></td>
									  		<td class="left"> <html:text name="instrumentBean" property="cohTaBeams" size="6" />
									  				<span class="error"><astron:errors property="cohTaBeams" ifnotexist="*"/></span>
									  		</td>
									  </tr> 
								  </logic:equal>
								  <!--  Share polarisation setting CS and FE as requested by science support 6/9/12 -->
								  <logic:notEqual name="instrumentBean" property="cohstokes" value="true">
								  <logic:equal name="instrumentBean" property="flysEye" value="true">
								  <tr>
								  		<td align="right" valign="top"><astron:label key="label.opticon.observation.post.polarizations" /></td>
								  		<td class="left">
								  		    <bean:define id="polarizationlist" name="instrumentBean" property="polarizations"/>	
								        	<html:select name="instrumentBean" property="selectedPolarizations" size="1" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
								                 <html:options collection="polarizationlist" property="value" labelProperty="value" />
								         	</html:select>
								        </td>
								  </tr>	
								  			  
								  </logic:equal>
								  </logic:notEqual>
								  <!-- End change request 6/9/12 -->
								   <logic:equal name="instrumentBean" property="incohstokes" value="true">
								   <tr>
								  		<td align="right" valign="top"><astron:label key="label.opticon.observation.post.incpolarizations" /></td>
								  		<td class="left">
								  		    <bean:define id="incpolarizationlist" name="instrumentBean" property="incPolarizations"/>	
								        	<html:select name="instrumentBean" property="selectedIncPolarizations" size="1" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
								                 <html:options collection="incpolarizationlist" property="value" labelProperty="value" />
								         	</html:select>
								        </td>
								   </tr>
								     <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.incohtabeams" /></td>
								  		<td class="left"> <html:text name="instrumentBean" property="incohTaBeams" size="6" />
								  		<span class="error"><astron:errors property="incohTaBeams" ifnotexist="*"/></span>
								  	</td>
								  </tr>
								   </logic:equal>
								 
								  <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.rings" /></td>
								  		<td class="left"> <html:text name="instrumentBean" property="rings" size="6" />
								  		</td>
								  </tr>
								<tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.channels" /></td>
								  		<td class="left"> 
								  		<logic:iterate id="channelItem" name="instrumentBean" property="channellist">
								        	<html:radio name="instrumentBean" property="channels" idName="channelItem" value="value" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
								                 <bean:write name="channelItem" property="label"/>
								         	</html:radio>
								        </logic:iterate>
								  		 
								  		<span class="error"><astron:errors property="channels" ifnotexist="*" /></span>
								  		</td>
								  </tr>
								  <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.intsteps" /></td>
								  		<td class="left"> <html:text name="instrumentBean" property="intsteps" size="6" /> 
								  		<span class="error"><astron:errors property="intsteps" ifnotexist="*" /></span>
								  		</td>
								  </tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayPostTimeseries" value="true">
								<tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.events" /> </td>
								  		<td class="left"><html:text name="instrumentBean" property="events" size="6" />
								  		<span class="error"><astron:errors property="events" ifnotexist="*" /></span>
								  		</td>
								  </tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayStoreRawData" value="true">
								   <tr>
								  		<td align="right" valign="top"><astron:label key="label.opticon.observation.storerawdata" /></td>
								  		<td class="left">
								  			<html:radio styleClass="radio" name="instrumentBean" property="storeRawData" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
											<html:radio styleClass="radio" name="instrumentBean" property="storeRawData" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
								        </td>
								  </tr>
						  </logic:equal>
						  <logic:equal name="instrumentBean" property="displayCorrelatedVisibilities" value="true">
								   <tr>
								  		<td align="right" valign="top"><astron:label key="label.opticon.observation.correlatedvisibilities" /></td>
								  		<td class="left">
								  			<html:radio styleClass="radio" name="instrumentBean" property="correlatedVisibilities" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
											<html:radio styleClass="radio" name="instrumentBean" property="correlatedVisibilities" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
								        </td>
								  </tr>
						  </logic:equal>
						  <logic:equal name="instrumentBean" property="displayIntegrationTime" value="true">
							<tr>
								<td class="right"><astron:label key="label.opticon.observation.integration.time" /><!--Integration time (seconds)&nbsp;:&nbsp;--></td>
								<td class="left"><html:text styleId="integrationTime" name="instrumentBean" property="integrationTime" size="4" />
									<logic:equal name="instrumentBean" property="requiredIntegrationTime" value="true">
										<span class="error"><astron:errors property="integrationTime" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredIntegrationTime" value="false">
										<span class="error"><astron:errors property="integrationTime" ifnotexist="" /></span>
									</logic:equal>	
								</td>
							</tr>
							</logic:equal>
							<logic:equal name="instrumentBean" property="displayStoreUVData" value="true">
								   <tr>
								  		<td align="right" valign="top"><astron:label key="label.opticon.observation.storeUVdata" /></td>
								  		<td class="left">
								  			<html:radio styleClass="radio" name="instrumentBean" property="storeUVData" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
											<html:radio styleClass="radio" name="instrumentBean" property="storeUVData" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
								        </td>
								  </tr>
						  </logic:equal>
						  
						  <logic:equal name="instrumentBean" property="displayPiggyBackFeatures" value="true">
						  
						  	<tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.tbb.piggyback.enable" /> </td>
								  		<td class="left">
								  			<html:radio styleClass="radio" name="instrumentBean" property="enablePiggyBack" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
											<html:radio styleClass="radio" name="instrumentBean" property="enablePiggyBack" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
								         	<span class="error"><astron:errors property="enablePiggyBack" ifnotexist="*" /></span>
								  		</td>
								  </tr>
							
							
							
							
							<logic:equal name="instrumentBean" property="enablePiggyBack" value="false">
								
								  <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.tbb.standalone.observation" /> </td>
								  		
								  		<td class="left">
								  		<bean:define id="tbbObservations" name="instrumentBean" property="tbbObservations" /> 
											<html:select name="instrumentBean" property="tbbSelectedObservationId">
												<html:options collection="tbbObservations" property="value"	labelProperty="label" />
											</html:select>
								  		</td>
								  			  
							</logic:equal>
						<logic:equal name="instrumentBean" property="enablePiggyBack" value="true">
								  		<tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.tbb.trigger.exposure.time" /> </td>
								  		
								  		<td class="left">
								  		<html:text name="instrumentBean" property="tbbExposureTime" size="6"/>
								  		</td>
								  		</tr>	
								  		</logic:equal>	
						   			<tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.tbb.trigger.length" /> </td>
								  		<td class="left">
								  			<html:text name="instrumentBean" property="tbbTriggerLength" size="6" />
								  			<span class="error"><astron:errors property="tbbTriggerLength" ifnotexist="*" /></span>
								  			
								  		</td>
								  	</tr>
								  	<tr>
								  	<td></td>
								  	<td><astron:label key="label.opticon.observation.tbb.trigger.length.help" /> </td>
								  	</tr>
								  <tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.tbb.trigger.rate" /> </td>
								  		<td class="left">
								  			<html:text name="instrumentBean" property="tbbTriggerRate" size="6" />
								  			<span class="error"><astron:errors property="tbbTriggerRate" ifnotexist="*" /></span>
								  			
								  		</td>
								  	</tr>
									<tr>
								  		<td class="right" valign="top"><astron:label key="label.opticon.observation.tbb.trigger.source" /> </td>

								  		<td class="left">
								  		<bean:define id="tbbTriggerSource" name="instrumentBean" property="tbbTriggerSource" /> 
											<html:select name="instrumentBean" property="tbbTriggerSourceId" >
												<html:options collection="tbbTriggerSource" property="value"	labelProperty="label" />
											</html:select>
								  		</td>
								  </tr>		  
						  </logic:equal>
						  
						  
							<logic:equal name="instrumentBean" property="displayObservationRequestedTime" value="true">
								<tr>
									<td class="right"><astron:label key="label.opticon.observation.requestedTime" /><!-- Total time for this run : &nbsp; --></td>
									<td class="left"> 
											<bean:write name="instrumentBean" property="observationRequestedTime" />	
											<!-- <html:text name="instrumentBean" property="observationRequestedTime" disabled="true" size="10" />
											<logic:equal name="instrumentBean" property="requiredObservationRequestedTime" value="true">
														<span class="error"><astron:errors property="comments" ifnotexist="*" /></span>
											</logic:equal> -->
											 <bean:write name="instrumentBean" property="timeSuffix" />		
										</td>
								</tr>
							</logic:equal> 
							<logic:equal name="instrumentBean" property="displayCalculatedSize" value="true">
								<logic:notEmpty name="instrumentBean" property="dataCalculationType">
								<tr>
								  		<td>Calculation type : </td>
								  		<td><bean:write name="instrumentBean" property="dataCalculationType"/>
										  		</td>
								</tr>
								</logic:notEmpty>	
								<tr>
								  		<td class="right">Estimated data size : </td>
								  		<td><bean:write name="instrumentBean" property="estimatedDataStorage" /> GB
								  		&nbsp; &nbsp; 
								  		<html:link href="javascript:document.forms[0].submit();" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()" onmouseover="boxopen('m1')" onmouseout="boxclosetime('m1')">recalculate</html:link>
								  		</td>
								</tr>
								<tr>
								<td colspan="2" >
								 <div id="m1" style="visibility:hidden; position: absolute; border: 1px solid; background: #E0F0FF;"> &nbsp;	&nbsp;  Calculation data used : 
								 &nbsp; <bean:write name="instrumentBean" property="dataStorageSpecifics" /></div></td>
								</tr>
							</logic:equal>
							
				</logic:notEmpty>
				
		</logic:notEmpty>
		</logic:equal>
		
		<!--  
			begin observation info
 -->
<logic:equal name="instrumentBean" property="displaySchedulingInfo" value="true">
			 <tr>
				<td class="header" colspan="2"><astron:label key="label.opticon.observation.scheduling" /></td>
			</tr>	
			<tr>
				<td colspan="2">
				<tiles:get name="box_header_no_top" />
	
	
				<table width="100%">	
			<tr>
				<td colspan="2">&nbsp;</td>
			</tr>
			
			
			<logic:equal name="instrumentBean" property="displayObservationMinimumTime" value="true">
				<tr>
					<td class="right"> 	Minimum useful time for this run : &nbsp; </td>
					<td class="left"> 	
							<html:text name="instrumentBean" property="observationMinimumTime" size="10" />
							<logic:equal name="instrumentBean" property="requiredObservationRequestedTime" value="true">
										<span class="error"><astron:errors property="comments" ifnotexist="*" /></span>
							</logic:equal>	
							 <bean:write name="instrumentBean" property="timeSuffix" />
						</td>
				</tr>
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>
			</logic:equal>
							  
			<logic:equal name="instrumentBean" property="displayObservationPhase" value="true">
				<tr>
					<td class="right"> 	Time : &nbsp; </td>
					<td class="left"> 	
						<bean:define id="phaselist" name="instrumentBean" property="observationPhaseOptions"/> 
							<html:select name="instrumentBean" property="selectedObservationPhase" size="1">
								<html:options collection="phaselist" property="value" labelProperty="value"/>		
							</html:select>
						</td>
				</tr>
			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayObservationWeather" value="true">
				<tr>
					<td class="right"> 	Weather : &nbsp; </td>
					<td class="left"> 	
						<bean:define id="Weatherlist" name="instrumentBean" property="observationWeatherOptions"/> 
							<html:select name="instrumentBean" property="selectedObservationWeather" size="1">
								<html:options collection="Weatherlist" property="value" labelProperty="value"/>		
							</html:select>
						</td>
				</tr>
			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayObservationMode" value="true">
				<tr>
					<td class="right"> 	Mode : &nbsp; </td>
					<td class="left"> 	
						<bean:define id="Modelist" name="instrumentBean" property="observationModeOptions"/> 
							<html:select name="instrumentBean" property="selectedObservationMode" size="1">
								<html:options collection="Modelist" property="value" labelProperty="value"/>		
							</html:select>
						</td>
				</tr>
			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayObservationSeeing" value="true">
				<tr>
					<td class="right"> 	Seeing : &nbsp; </td>
					<td class="left"> 	
						<bean:define id="seeinglist" name="instrumentBean" property="observationSeeingOptions"/> 
							<html:select name="instrumentBean" property="selectedObservationSeeing" size="1">
								<html:options collection="seeinglist" property="value" labelProperty="value"/>		
							</html:select>
						</td>
				</tr>
			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayObservationDates" value="true">
				<tr>
					<td class="right"> 	Preffered Dates : &nbsp; </td>
					<td class="left"> 	
							<html:text name="instrumentBean" property="observationDates" size="45" />
							<span class="courier">&nbsp;(yyyy/mm/dd)&nbsp;</span>
						</td>
				</tr>
			</logic:equal>
			<logic:equal name="instrumentBean" property="displayTelescopeScheduling" value="true">
				<tr>
					<td class="right"> 	Scheduling : &nbsp; </td>
					<td class="left"> 	
						<bean:define id="schedulinglist" name="instrumentBean" property="telescopeSchedulingOptions"/> 
							<html:select name="instrumentBean" property="selectedTelescopeScheduling" size="1">
								<html:options collection="schedulinglist" property="value" labelProperty="value"/>		
							</html:select>
						</td>
				</tr>
			</logic:equal>
			<logic:equal name="instrumentBean" property="displayRequiredSchedConstraints" value="true">
				<tr>
					<td class="right" valign="top">Essential Scheduling	constraints&nbsp;:&nbsp;</td>
					<td class="left"><html:textarea name="instrumentBean" property="requiredSchedConstraints" cols="60" rows="4"/></td>
				</tr>
			</logic:equal>
			<logic:equal name="instrumentBean" property="displayPreferredSchedConstraints" value="true">
				<tr>
					<td class="right" valign="top">Preferred Scheduling	constraints&nbsp;:&nbsp;</td>
					<td class="left"><html:textarea name="instrumentBean" property="preferredSchedConstraints" cols="60" rows="4"/></td>
				</tr>
			</logic:equal>
			<logic:equal name="instrumentBean" property="displayBackupStrategy" value="true">
				<tr>
					<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.backup" /><!-- Backup strategy (for poor weather) :&nbsp;<br/>&nbsp;&nbsp;--></td>
					<td class="left"><html:textarea name="instrumentBean" property="backupStrategy" cols="60" rows="4"/></td>
				</tr>	
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>	
			</logic:equal>
			<logic:equal name="instrumentBean" property="displayCalibrationRequirements" value="true">
				<tr>
					<td class="right" valign="top"><astron:label key="label.opticon.observingrequest.calibration" /><!-- Calibration Requirements :&nbsp; --></td>
					<td class="left"><html:textarea name="instrumentBean" property="calibrationRequirements" cols="60" rows="4"/></td>
				</tr>	
				<tr>
					<td colspan="2">&nbsp;</td>
				</tr>	
			</logic:equal>
			</table>
			<tiles:get name="box_footer" /></td>
			</tr>
		
			<logic:equal name="instrumentBean" property="displayPiggyBack" value="true">
		<tr>
			<td class="right" valign="top"><astron:label key="label.opticon.observation.piggyback" /></td>
			<td class="left">	
				<html:radio name="instrumentBean" property="enablePiggyBack" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
				<html:radio name="instrumentBean" property="enablePiggyBack" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
		</logic:equal>	
		<logic:equal name="instrumentBean" property="enablePiggyBack" value="true">
		<tr>
			<td class="right"> <astron:label key="label.opticon.observation.piggyback.specifics" /> <!-- list one or more organisations from which you are requesting sponsorship (e.g. GLOW) :&nbsp;--></td>
			<td class="left"><html:textarea name="instrumentBean" property="piggyBackSpecifics" cols="60" rows="4"/></td>
				
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>	
		</logic:equal>
			

</logic:equal>


		
		<!-- end telescope config -->
</logic:equal>		
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
					



<logic:notEmpty name="instrumentBean" property="selectedTelescopeConfiguration">
	<logic:equal name="instrumentBean" property="displayPipelineOptions" value="true">
	
<tr>
	<td class="header" colspan="2"><astron:label key="label.opticon.observation.pipeline.configuration" /><!--pipeline configuration :--></td>
</tr>	

<tr>
	<td colspan="2"><tiles:get name="box_header_no_top" />
	<table width="100%">
		<tr>
		    <td width="40"> &nbsp; </td>
			<td width="60%"> &nbsp; </td> 
		</tr>
		
<tr>
	<!-- begin pipeline  -->
		<html:hidden name="instrumentBean" property="selectedTelescopeConfiguration" />
		<html:hidden name="instrumentBean" property="selectedInstrumentConfiguration" />

	<td class="header" colspan="2">Pre processing parameters</td>
</tr>	
<tr>
	<td colspan="2">
	
	<tiles:get name="box_header_no_top" />
	<table width="100%"> 
		<tr>
			    <td width="40"> &nbsp; </td>
				<td width="60%"> &nbsp; </td> 
		</tr>
				
			<!--<bean:write name="instrumentBean" property="selectedTelescopeConfiguration"/>, 
			<bean:write name="instrumentBean" property="selectedInstrumentConfiguration" />-->
			
			<logic:equal name="instrumentBean" property="displayProcessingMode" value="true">
				 <tr>
				  		<td align="right" valign="top"><astron:label key="label.opticon.observation.post.processingmodes" /></td>
				  		<td class="left">	<!--<logic:iterate id="processingModeItem" name="instrumentBean" property="processingModes">
				        	<html:radio name="instrumentBean" property="selectedProcessingMode" idName="processingModeItem" value="value" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
				                 <bean:write name="processingModeItem" property="label"/>
				         	</html:radio>
				        </logic:iterate> -->
				        <bean:define id="processingModes" name="instrumentBean" property="processingModes" /> 
					<html:select name="instrumentBean" property="selectedProcessingMode" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
						<html:options collection="processingModes" property="value"	labelProperty="label" />
					</html:select><span class="error">*</span>
				        </td>
				  </tr>
			
			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayFlaggingStrategy" value="true">
				 <tr>
				  		<td align="right" valign="top"><astron:label key="label.opticon.observation.post.flaggingstrategies" /></td>
				  		<td class="left"><bean:define id="flaggingStrategies" name="instrumentBean" property="flaggingStrategies" /> 
					<html:select name="instrumentBean" property="selectedFlaggingStrategy" onchange="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
						<html:options collection="flaggingStrategies" property="value"	labelProperty="label" />
					</html:select><span class="error">*</span></td>
				  </tr>
			
			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayPostAveraging" value="true">
				
				  <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.time" /></td>
				  		<td class="left"><html:text name="instrumentBean" property="averagingTime" size="6" /> [steps]
				  		<span class="error"><astron:errors property="averagingTime" ifnotexist="*" /></span>
				  		</td>
				  </tr>
				  <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.frequency" /></td>
				  		<td class="left"><html:text name="instrumentBean" property="averagingFrequency" size="6" /> [steps]
				  		<span class="error"><astron:errors property="averagingFrequency" ifnotexist="*" /></span>
				  		</td>
				  </tr>

			</logic:equal>
			
			<logic:equal name="instrumentBean" property="displayPostDemixing" value="true">
					<tr>
						<td class="right" valign="top"><astron:label key="label.opticon.observation.post.demixing" /></td>
						<td class="left"><html:radio styleClass="radio" name="instrumentBean" property="demixing" value="true" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">Yes</html:radio>
										<html:radio styleClass="radio" name="instrumentBean" property="demixing" value="false" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">No</html:radio>
						</td>
					</tr>
					 
			  		  <tr><td> &nbsp; </td>
						<td>
						  <% String styleClass = ""; %>	
							<table align="left" class="projecttable">
								<TR>
					 				<td colspan="2" class="tableheader"><astron:label key="label.opticon.observation.choose.demixing" /><!--Filters&nbsp;:&nbsp; --><span class="error">*</span>
								</TR>							
								<%	int j=0; %>	
								<logic:iterate id="demixingItem" name="instrumentBean" property="demixingSources">
									<% styleClass = (j++ % 2 == 0 ? "" : "_odd"); %>
									<tr><td align="right" nowrap="true"  class="tablefield_pri<%=styleClass%>">
										<logic:equal name="instrumentBean" property="demixing" value="true">
										<html:multibox name="instrumentBean" property="selectedDemixingSources" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
												<bean:write name="demixingItem" property="value" filter="false"/>
										</html:multibox>
										</logic:equal>
										
										<logic:equal name="instrumentBean" property="demixing" value="false">
										<html:multibox name="instrumentBean" disabled="true" property="selectedDemixingSources" onclick="setNorthStarCookie(getScrollCoordinates());document.forms[0].submit()">
												<bean:write name="demixingItem" property="value" filter="false"/>
										</html:multibox>
										</logic:equal>
										
										</td><td class="tablefield_pri<%=styleClass%>">
										<bean:write name="demixingItem" property="label"/>
									</td></tr>
								</logic:iterate>
																				
							</table>
						</td></tr>	
						
				  </logic:equal>
				  <logic:equal name="instrumentBean" property="selectedProcessingMode" value="Pulsar pipeline">
					<tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.skiprfi" /></td>
				  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="skipRFI" value="true">Yes</html:radio>
								<html:radio styleClass="radio" name="instrumentBean" property="skipRFI" value="false" >No</html:radio>
				  		<span class="error"><astron:errors property="skipRFI"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.skipfolding" /></td>
				  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="skipFolding" value="true" >Yes</html:radio>
								<html:radio styleClass="radio" name="instrumentBean" property="skipFolding" value="false" >No</html:radio>
				  		<span class="error"><astron:errors property="skipFolding"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.skippdmp" /></td>
				  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="skipPdmp" value="true" >Yes</html:radio>
								<html:radio styleClass="radio" name="instrumentBean" property="skipPdmp" value="false" >No</html:radio>
				  		<span class="error"><astron:errors property="skipPdmp"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.skipdspsr" /></td>
				  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="skipDspsr" value="true" >Yes</html:radio>
								<html:radio styleClass="radio" name="instrumentBean" property="skipDspsr" value="false" >No</html:radio>
				  		<span class="error"><astron:errors property="skipDspsr"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.skipprepfold" /></td>
				  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="skipPrepfold" value="true" >Yes</html:radio>
								<html:radio styleClass="radio" name="instrumentBean" property="skipPrepfold" value="false" >No</html:radio>
				  		<span class="error"><astron:errors property="skipPrepfold"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.singlepulse" /></td>
				  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="singlePulse" value="true" >Yes</html:radio>
								<html:radio styleClass="radio" name="instrumentBean" property="singlePulse" value="false" >No</html:radio>
				  		<span class="error"><astron:errors property="singlePulse"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.rrats" /></td>
				  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="rratsAnalysis" value="true" >Yes</html:radio>
								<html:radio styleClass="radio" name="instrumentBean" property="rratsAnalysis" value="false" >No</html:radio>
				  		<span class="error"><astron:errors property="rratsAnalysis"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.skipDynamicAverage" /></td>
				  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="skipDynamicAverage" value="true" >Yes</html:radio>
								<html:radio styleClass="radio" name="instrumentBean" property="skipDynamicAverage" value="false" >No</html:radio>
				  		<span class="error"><astron:errors property="skipDynamicAverage"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.subintegrationlength" /></td>
				  		<td class="left">
				  		<html:text title="Typical values are 5 for complex-voltage mode and 60 for CS/IS."  name="instrumentBean" property="subintegrationLength" size="6"/> 
				  		<span class="error"><astron:errors property="subintegrationLength"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.convertrawdata" /></td>
				  		<td class="left"> <html:radio styleClass="radio" name="instrumentBean" property="convertRawData" value="true" >Yes</html:radio>
								<html:radio styleClass="radio" name="instrumentBean" property="convertRawData" value="false" >No</html:radio>
				  		<span class="error"><astron:errors property="convertRawData"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.threshold" /></td>
				  		<td class="left"> <html:text name="instrumentBean" property="threshold" size="6"/> 
				  		<span class="error"><astron:errors property="threshold"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.sigmalimit" /></td>
				  		<td class="left"> <html:text name="instrumentBean" property="sigmaLimit" size="6"/> 
				  		<span class="error"><astron:errors property="sigmaLimit"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.numberofblocks" /></td>
				  		<td class="left"> <html:text name="instrumentBean" property="numberOfBlocks" size="6"/> 
				  		<span class="error"><astron:errors property="numberOfBlocks"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.prepfoldoptions" /></td>
				  		<td class="left"><html:text name="instrumentBean" property="prepfoldOptions"/>
				  		<span class="error"><astron:errors property="prepfoldOptions"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.presubbandoptions" /></td>
				  		<td class="left"><html:text name="instrumentBean" property="presubbandOptions"/>
				  		<span class="error"><astron:errors property="presubbandOptions"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.rfifind" /></td>
				  		<td class="left"><html:text name="instrumentBean" property="rfifind"/>
				  		<span class="error"><astron:errors property="rfifind"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td colspan="2" class="center" valign="top">
				  			*Expert settings: not all options may result in a successful pipeline, therefore the Observatory may decline setting certain parameters.
				  		</td>
				  	 </tr>
				  	 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.dspsroptions" /></td>
				  		<td class="left"><html:text name="instrumentBean" property="dspsrOptions"/>
				  		<span class="error"><astron:errors property="dspsrOptions"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.digifiloptions" /></td>
				  		<td class="left"><html:text name="instrumentBean" property="digifilOptions"/>
				  		<span class="error"><astron:errors property="digifilOptions"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.predataoptions" /></td>
				  		<td class="left"><html:text name="instrumentBean" property="predataOptions"/>
				  		<span class="error"><astron:errors property="predataOptions"/></span>
				  		</td>
					 </tr>
					 <tr>
				  		<td class="right" valign="top"><astron:label key="label.opticon.observation.2bf2fitsoptions" /></td>
				  		<td class="left"><html:text name="instrumentBean" property="bf2fitsOptions"/>
				  		<span class="error"><astron:errors property="bf2fitsOptions"/></span>
				  		</td>
					 </tr>
					 <tr>
						<td class="right" valign="top"><astron:label key="label.opticon.observation.pulsar" /></td>
						<td class="left"><html:textarea title="Pulsar name or comma-seperated list of pulsars. See the 'HOW TO' for further details." name="instrumentBean" property="pulsarDesc" cols="50" rows="2"/>
							<span class="error"><astron:errors property="pulsarDesc" ifnotexist="" /></span>
						</td>
					</tr>	
				</logic:equal> 
				  <logic:equal name="instrumentBean" property="displayComments" value="true">														
							<tr>
								<td class="right" valign="top">Configuration comments&nbsp;:&nbsp;</td>
								<td class="left"><html:textarea name="instrumentBean" property="comments" cols="50" rows="2"/>
									<logic:equal name="instrumentBean" property="requiredComments" value="true">
										<span class="error"><astron:errors property="comments" ifnotexist="*" /></span>
									</logic:equal>
									<logic:equal name="instrumentBean" property="requiredComments" value="false">
										<span class="error"><astron:errors property="comments" ifnotexist="" /></span>
									</logic:equal>										
								</td>
							</tr>	
				   </logic:equal>
<!-- HAH 20200210 No imaging modes available, this subsection is obsolete
			  <logic:equal name="instrumentBean" property="displayImagingParameters" value="true">
			  <tr>
				<td class="header" colspan="2">Imaging parameters</td>
			  </tr>		
			  <tr><td colspan="2">
			   <tiles:get name="box_header_no_top" />
				<table width="100%"> 
					<tr>
						    <td width="40"> &nbsp; </td>
							<td width="60%"> &nbsp; </td> 
					</tr>
					<tr>
						    <td> &nbsp; </td>
							<td><logic:notEqual name="instrumentBean" property="selectedProcessingMode" value="Calibration + imaging">
							<span class="error">No imaging selected in processing mode</span>
							</logic:notEqual> </td> 
					</tr>
					<tr>
					  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.subbandsperimage" /></td>
					  		<logic:equal name="instrumentBean" property="displaySubbandsPerImage" value="true">
					  		<td class="left"><html:text name="instrumentBean" disabled="false" property="subbandsPerImage" size="6" /> [int]
					  		<span class="error"><astron:errors property="subbandsPerImage" ifnotexist="*" /></span></td>
					  		</logic:equal>
					  		<logic:equal name="instrumentBean" property="displaySubbandsPerImage" value="false">
					  		<td class="left"><html:text name="instrumentBean" disabled="true" property="subbandsPerImage" size="6" /> [int]
					  		<span class="error"><astron:errors property="subbandsPerImage" ifnotexist="*" /></span></td>
					  		</logic:equal>
					  		
					  </tr>
			
				<tr>
					  		<td class="right" valign="top"><astron:label key="label.opticon.observation.post.fieldofview" /></td>
					  		<logic:equal name="instrumentBean" property="displaySubbandsPerImage" value="true">
					  		<td class="left"><html:text name="instrumentBean" disabled="false" property="fieldOfView" size="6" /> [deg]
					  		<span class="error"><astron:errors property="fieldOfView" ifnotexist="*" /></span></td>
					  		</logic:equal>
					  		<logic:equal name="instrumentBean" property="displaySubbandsPerImage" value="false">
					  		<td class="left"><html:text name="instrumentBean" disabled="true" property="fieldOfView" size="6" /> [deg]
					  		<span class="error"><astron:errors property="fieldOfView" ifnotexist="*" /></span></td>
					  		</logic:equal>
					  </tr>
				</table>
			   <tiles:get name="box_footer" />
			   </td></tr>
				</logic:equal>
-->				
				
	</table>
	<tiles:get name="box_footer" />
	</td>
</tr>
<!-- end pipeline -->	
</logic:equal> 
</logic:notEmpty>




	