package eu.radionet.northstar.control.admin.statistics;

public class StatisticsBean {
	private String insName = null;
	private String proposals = null;
	private String time = null;
	private String targets = null;
	private String storage = null;
	
	public String getProposals() {
		return proposals;
	}
	public void setProposals(String proposals) {
		this.proposals = proposals;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public String getTargets() {
		return targets;
	}
	public void setTargets(String targets) {
		this.targets = targets;
	}
	public String getStorage() {
		return storage;
	}
	public void setStorage(String storage) {
		this.storage = storage;
	}
	
	public void incrementProposal(int i) {
		if (this.proposals != null){
			try{
				Integer amount =  Integer.parseInt(this.proposals);
				this.proposals = String.valueOf(amount.intValue()+i );
			}catch (NumberFormatException e){
				i=0;
			}
		}
		
	}
	public void incrementTargets(int size) {
		if (this.targets != null){
			try{
				Integer amount =  Integer.parseInt(this.targets);
				this.targets = String.valueOf(amount.intValue()+size );
			}catch (NumberFormatException e){
				size=0;
			}
		}
		
	}
	public void incrementTimeSec(Double totalObservationDuration) {
		if (this.time != null){
			try{
				Double amount =  Double.parseDouble(this.time);
				double mins = totalObservationDuration.doubleValue();
				if(mins >0){
					mins = mins /60;
				}
				this.time = String.valueOf(amount.doubleValue()+mins );
			}catch (NumberFormatException e){
				totalObservationDuration=null;
			}
		}
	}
	
	public void incrementTime(Double totalObservationDuration) {
		if (this.time != null){
			try{
				Double amount =  Double.parseDouble(this.time);
				double mins = totalObservationDuration.doubleValue();
				this.time = String.valueOf(amount.doubleValue()+mins );
			}catch (NumberFormatException e){
				totalObservationDuration=null;
			}
		}else{
			this.time = totalObservationDuration.toString();
		}
	}
	
	public void incrementStorage(Double lstorage) {
		if (this.storage != null){
			try{
				Double amount =  Double.parseDouble(this.storage);
				double stor = lstorage.doubleValue();
				this.storage = String.valueOf(amount.doubleValue()+stor );
			}catch (NumberFormatException e){
				lstorage=null;
			}
		}else{
			this.storage = lstorage.toString();
		}
	}
	public String getInsName() {
		return insName;
	}
	public void setInsName(String insName) {
		this.insName = insName;
	}
	
	
	
}
