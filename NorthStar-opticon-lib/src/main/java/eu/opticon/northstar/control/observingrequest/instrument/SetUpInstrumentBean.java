// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest.instrument;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.RemoteException;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.rpc.ServiceException;

import nl.astron.service.lofar.StorageCalculator;
import nl.astron.service.lofar.client.StorageCalculatorBinding;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.util.LabelValueBean;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.opticon.northstar.business.OpticonConstants;
import eu.opticon.northstar.business.OpticonTargetUtils;
import eu.opticon.northstar.control.observingrequest.observation.OpticonObservationForm;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.OptionsUtils;

public class SetUpInstrumentBean {
	
	private static Log log = LogFactory.getLog(SetUpInstrumentBean.class);
	private static final String BEAMFORMED = "Beam";
	private static final String TIEDARRAY = "Tied";
	private static final String INTERFERO = "Interfero";
	private static final String TRANSIENT = "Transient";
	private static final String TBB = "TBB";
	private static final String SINGLE = "Single";
	private static final String NONSTANDARD = "Non";
	private static final String CUSTOM = "Custom";
	
	protected ContextType contextConfiguration = null;

	// fill the form with the options from the configuration file.
	public static void fillOptions(HttpServletRequest request, InstrumentBean instrumentBean){
		ProposalData proposalData = (ProposalData) request.getSession().getAttribute(Constants.PROPOSAL_DATA);
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposalData.getProposal());
		
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposalData.getProposal());
		
		List telescopeConfigurations = OptionsUtils.getLabelValueBeans(
        		OpticonConstants.TELESCOPE_CONFIGURATIONS, new HashMap(), contextConfiguration);
		if (telescopeConfigurations.size() < 3)
		{
			//remove the Non specified
			telescopeConfigurations.remove(0);
			// set the selected telescope to the only element that is left
			instrumentBean.setSelectedTelescopeConfiguration(
					((LabelValueBean) telescopeConfigurations.get(0)).getLabel());
		} else
		{
			instrumentBean.setDisplayTelescopeDropbox(true);
		}
		instrumentBean.setTelescopeConfigurations(telescopeConfigurations);
		Map enteredTelescopes = new HashMap();
		enteredTelescopes.put(OpticonConstants.TELESCOPE_CONFIGURATIONS,
				OptionsUtils.getList(instrumentBean.getSelectedTelescopeConfiguration()));
	    

		 
		List<LabelValueBean> instruments = OptionsUtils.getLabelValueBeans(OpticonConstants.INSTRUMENT_CONFIGURATIONS,
	            enteredTelescopes , contextConfiguration);
	    List<LabelValueBean> ninstruments = new ArrayList();
	    // skip pipeline
	    for(LabelValueBean lvb : instruments){
	    	if(!lvb.getValue().equalsIgnoreCase("pipeline" ) ){
	    		ninstruments.add(lvb);
	    	}
	    }
	    
	    instrumentBean.setInstrumentConfigurations(ninstruments);
	    String telescope = instrumentBean.getSelectedTelescopeConfiguration();
	    
        /*
         * fill observation lists
         */
        		
        instrumentBean.setObservationPhaseOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.OBSERVATION_PHASE, new HashMap(), contextConfiguration));
        instrumentBean.setObservationWeatherOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.OBSERVATION_WEATHER, new HashMap(), contextConfiguration));
        instrumentBean.setObservationModeOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.OBSERVATION_MODE, new HashMap(), contextConfiguration));
        instrumentBean.setObservationSeeingOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.OBSERVATION_SEEING, new HashMap(), contextConfiguration));
        
        instrumentBean.setMoonOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.TARGET_MOON, new HashMap(), contextConfiguration));
        instrumentBean.setSeeingOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.TARGET_SEEING, new HashMap(), contextConfiguration));
        instrumentBean.setWaterOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.TARGET_WATER, new HashMap(), contextConfiguration));
        instrumentBean.setSkyQualities(OptionsUtils.getLabelValueBeans(
        		OpticonConstants.TARGET_SKY_QUALITIES, new HashMap(), contextConfiguration));

        instrumentBean.setFlaggingStrategies(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.FLAGGING_STRATEGIES, new HashMap(), contextConfiguration) );
        instrumentBean.setTbbTriggerSource(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.TBB_TRIGGER_SOURCE, new HashMap(), contextConfiguration) );
        
        instrumentBean.setProcessingModes(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.PROCESSING_MODES, new HashMap(), contextConfiguration) );
        
        FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
        // set options to display 
        instrumentBean.setDisplayObservationRequestedTime(
				OptionsDelegate.allowedToDisplay(OpticonConstants.REQUESTEDTIME,
						fieldsDefinitionType));
        instrumentBean.setRequiredObservationRequestedTime(
				OptionsDelegate.isRequired(OpticonConstants.REQUESTEDTIME,
						fieldsDefinitionType));
        instrumentBean.setDisplayObservationPhase(
				OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVATION_PHASE,
						fieldsDefinitionType));
		instrumentBean.setDisplayObservationWeather(
				OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVATION_WEATHER,
						fieldsDefinitionType));
		instrumentBean.setDisplayObservationMode(
				OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVATION_MODE,
						fieldsDefinitionType));
		instrumentBean.setDisplayObservationSeeing(
				OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVATION_SEEING,
						fieldsDefinitionType));
		instrumentBean.setDisplayObservationDates(
				OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVATION_DATES,
						fieldsDefinitionType));
		instrumentBean.setDisplayBackupStrategy(
				OptionsDelegate.allowedToDisplay(OpticonConstants.BACKUPSTRATEGY,
						fieldsDefinitionType));
		instrumentBean.setDisplayCalibrationRequirements(
				OptionsDelegate.allowedToDisplay(OpticonConstants.CALIBRATION_REQUIREMENTS,
						fieldsDefinitionType));

		instrumentBean.setDisplayRequiredSchedConstraints(
				OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVATION_REQUIRED_SCHEDULING_CONSTRAINTS,
						fieldsDefinitionType));
		instrumentBean.setDisplayPreferredSchedConstraints(
				OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVATION_PREFERRED_SCHEDULING_CONSTRAINTS,
						fieldsDefinitionType));
		
		// set the display of the scheduling info box
		if(instrumentBean.isDisplayObservationDates() || instrumentBean.isDisplayObservationMode() || instrumentBean.isDisplayObservationPhase() ||
				instrumentBean.isDisplayObservationRequestedTime() || instrumentBean.isDisplayObservationSeeing() ||
				instrumentBean.isDisplayObservationWeather() || instrumentBean.isDisplayRequiredSchedConstraints() || 
				instrumentBean.isDisplayPreferredSchedConstraints() || instrumentBean.isDisplayBackupStrategy()){
			instrumentBean.setDisplaySchedulingInfo(true);	
		}
			
		instrumentBean.setDisplayPostProcessing(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_POSTPROCESSING,
						fieldsDefinitionType));
		instrumentBean.setDisplayCalculatedSize(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_CALCULATED_SIZE,
						fieldsDefinitionType));
		// lofar
		instrumentBean.setDisplayProcessingMode(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_PROCESSING_MODE,
						fieldsDefinitionType));
		instrumentBean.setDisplaySubbandsPerImage(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_SUBBANDS_PER_IMAGE,
						fieldsDefinitionType));
		instrumentBean.setDisplayFieldOfView(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_FIELD_OF_VIEW,
						fieldsDefinitionType));
		instrumentBean.setDisplayFlaggingStrategy(
				OptionsDelegate.allowedToDisplay(OpticonConstants.FLAGGING_STRATEGIES,
						fieldsDefinitionType));
		instrumentBean.setDisplayPostAveraging(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_POST_AVERAGING,
						fieldsDefinitionType));
		instrumentBean.setDisplayPostDemixing(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_POST_DEMIXING,
						fieldsDefinitionType));
		instrumentBean.setDisplayDemixingSource(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DEMIXING_SOURCE,
						fieldsDefinitionType) );
		
		instrumentBean.setDisplayPiggyBack(
				OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_PIGGYBACK,
						fieldsDefinitionType));
		
		//FIXME also set the time suffix and scheduling options from the first instrument if no instrument has been selected.
		String instrumentName = instrumentBean.getSelectedInstrumentConfiguration();
		instrumentBean.setTimeSuffix("(minutes)");
		if(instruments != null && instruments.size() >1){
			LabelValueBean ilvb = (LabelValueBean) instruments.get(1);
			String firstInstrument = ilvb.getLabel();
			List configurations = OptionsDelegate.getOptionsConfigurationTypes(
					OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredTelescopes,
					contextConfiguration);
			
			ConfigureOptionType configureOptionType = (ConfigureOptionType) OptionsDelegate
			.getOption(firstInstrument , configurations);
			
			Map enteredInstruments = new HashMap();
			enteredInstruments.put(OpticonConstants.INSTRUMENT_CONFIGURATIONS,
					OptionsUtils.getList(firstInstrument));

			if (configureOptionType != null	&& configureOptionType.getSubfields() != null) {
				FieldsDefinitionType instrumentFieldsDefinitionType = (FieldsDefinitionType) configureOptionType
							.getSubfields();
				if(OptionsDelegate.allowedToDisplay(OpticonConstants.TELESCOPE_MODE,instrumentFieldsDefinitionType)){
					instrumentBean.setObservationModeOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
							OpticonConstants.TELESCOPE_MODE, enteredInstruments,
							contextConfiguration));
				}
				if(OptionsDelegate.allowedToDisplay(OpticonConstants.TELESCOPE_MOON ,instrumentFieldsDefinitionType)){
					instrumentBean.setObservationPhaseOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
							OpticonConstants.TELESCOPE_MOON, enteredInstruments,
							contextConfiguration));
				}
				if(OptionsDelegate.allowedToDisplay(OpticonConstants.TELESCOPE_SCHEDULING ,instrumentFieldsDefinitionType)){
					instrumentBean.setDisplayTelescopeScheduling(true);
					instrumentBean.setTelescopeSchedulingOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
							OpticonConstants.TELESCOPE_SCHEDULING, enteredInstruments,
							contextConfiguration));
				}else{
					instrumentBean.setDisplayTelescopeScheduling(false);
				}

				if(OptionsDelegate.allowedToDisplay(OpticonConstants.TELESCOPE_HOURS ,fieldsDefinitionType)){
					instrumentBean.setTimeSuffix("(hours)");
				}
				if(OptionsDelegate.allowedToDisplay(OpticonConstants.TELESCOPE_HOURS ,instrumentFieldsDefinitionType)){
					instrumentBean.setTimeSuffix("(hours)");
				}
			}
		}
			
		
		if(instrumentName!= null){
			/*
	         * fill instrument lists
	         */
			// enteredInstruments can be used for required values.
			Map enteredInstruments = new HashMap();
			enteredInstruments.put(OpticonConstants.INSTRUMENT_CONFIGURATIONS,
					OptionsUtils.getList(instrumentName));
			//
			if(instrumentBean.getSelectedClock() != null){
				enteredInstruments.put(OpticonConstants.INSTRUMENT_CLOCK,OptionsUtils.getList(instrumentBean.getSelectedClock()));
			}
			if(instrumentBean.getSelectedAntenna() != null){
				if(instrumentBean.getSelectedAntenna().startsWith("LBA")){
					enteredInstruments.put(OpticonConstants.INSTRUMENT_ANTENNA,OptionsUtils.getList("LBA"));
				}
				if(instrumentBean.getSelectedAntenna().startsWith("HBA")){
					enteredInstruments.put(OpticonConstants.INSTRUMENT_ANTENNA,OptionsUtils.getList("HBA"));
				}
			}
			if(instrumentBean.getSelectedStation() != null){
				enteredInstruments.put(OpticonConstants.INSTRUMENT_STATION,OptionsUtils.getList(instrumentBean.getSelectedStation()));
			}
	        instrumentBean.setFilters(
					OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.INSTRUMENT_FILTER, enteredInstruments,
					contextConfiguration));
	        instrumentBean.setFiltersw2(
					OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.INSTRUMENT_FILTERW2, enteredInstruments,
					contextConfiguration));
	        instrumentBean.setSlitw(
					OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.INSTRUMENT_SLITW, enteredInstruments,
					contextConfiguration));
	        instrumentBean.setGrismw(
					OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.INSTRUMENT_GRISMW, enteredInstruments,
					contextConfiguration));
	        instrumentBean.setGratingw(
					OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.INSTRUMENT_GRATINGW, enteredInstruments,
					contextConfiguration));
	        
	        instrumentBean.setDemixingSources(
					OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.DEMIXING_SOURCE, enteredInstruments,
					contextConfiguration));
	        
			instrumentBean.setReadOuts(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_READOUT, enteredInstruments,
					contextConfiguration));
			instrumentBean.setModes(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_MODE, enteredInstruments,
					contextConfiguration));
			instrumentBean.setOrderFilters(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_ORDER_FILTER, enteredInstruments,
					contextConfiguration));
			instrumentBean.setGratings(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_GRATING, enteredInstruments,
					contextConfiguration));
			instrumentBean.setGrisms(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_GRISM, enteredInstruments,
					contextConfiguration));
			instrumentBean.setMicrostepping(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_MICROSTEPPING, enteredInstruments,
					contextConfiguration));		
			instrumentBean.setSlits(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_SLIT, enteredInstruments,
					contextConfiguration));	
			instrumentBean.setCCDs(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_CCD, enteredInstruments,
					contextConfiguration));	
			
	       
			
			List urlList =  OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_URL, enteredInstruments,
					contextConfiguration);	
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
				instrumentBean.setInstrumentUrl(urlValue.getValue());
			}		
			urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.INSTRUMENT_GUIDELINE ,
					new HashMap(), contextConfiguration);
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
				instrumentBean.setGuidelineUrl(urlValue.getValue());
			}
			// for lofar
			List stations = OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_STATION, enteredInstruments,
					contextConfiguration);
			instrumentBean.setStations(stations);
			instrumentBean.setClocks(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_CLOCK, enteredInstruments,
					contextConfiguration));
			instrumentBean.setAntennas(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_ANTENNA, enteredInstruments,
					contextConfiguration));
			instrumentBean.setOneFilters(OptionsUtils.getLabelValueBeans(
					OpticonConstants.INSTRUMENT_ONE_FILTER, enteredInstruments,
					contextConfiguration));
			instrumentBean.setProcessingModes(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.PROCESSING_MODES, enteredInstruments,
					contextConfiguration) );
			instrumentBean.setFlaggingStrategies(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.FLAGGING_STRATEGIES, enteredInstruments,
					contextConfiguration)
					);
			instrumentBean.setPolarizations(
					OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.POLARIZATIONS, enteredInstruments,
					contextConfiguration));
			
			instrumentBean.setIncPolarizations(
					OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.INCPOLARIZATIONS, enteredInstruments,
					contextConfiguration));
			
			instrumentBean.setChannellist(
					OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
					OpticonConstants.CHANNELS, enteredInstruments,
					contextConfiguration));
			/**
			 * add observations for TBB
			 */
			
			LabelValueBean lvb = new LabelValueBean(Constants.NONE_SPECIFIED_LABEL,
					Constants.NONE_SPECIFIED_VALUE);
			instrumentBean.getTbbObservations().add(lvb);
			int observations=0;
			List obs =proposalData.getProposal().getObservingRequest().getObservations();
			for(int j=0;j<obs.size();j++){
				OpticonObservation ob = (OpticonObservation) obs.get(j);
					if(!ob.getInstrument().getName().startsWith(TBB)){
					char chr = (char) (observations+65);
					lvb = new LabelValueBean();
					lvb.setLabel(String.valueOf(chr));
					lvb.setValue(String.valueOf(chr));
					instrumentBean.getTbbObservations().add(lvb);
					observations++;
					}
			}
			
			
			/*
			 * set instrument display options
			 */
			List configurations = OptionsDelegate.getOptionsConfigurationTypes(
					OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredTelescopes,
					contextConfiguration);

			ConfigureOptionType configType = (ConfigureOptionType) OptionsDelegate
			.getOption(instrumentName , configurations);
			
			List <ConfigureOptionType>cType = new ArrayList<ConfigureOptionType>();
			if(instrumentName.equals("Pipeline")|| !AstronValidator.isBlankOrNull(instrumentBean.getSelectedProcessingMode()))
			{
				configType = (ConfigureOptionType) OptionsDelegate.getOption("Pipeline" , configurations);
				cType.add(configType);
				
			}
			
			else{
			
			for(int i=0;i<instrumentBean.getDeSelectedInstConf().size();i++)
			{  instrumentName =(String) instrumentBean.getDeSelectedInstConf().get(i);
			configType = (ConfigureOptionType) OptionsDelegate
					.getOption(instrumentName , configurations);
			cType.add(configType);
			}
			
			for (ConfigureOptionType configureOptionType: cType) {
				fieldsDefinitionType.remove(configureOptionType.getSubfields());
			}
			cType = new ArrayList<ConfigureOptionType>();
			for(int i=0;i<instrumentBean.getSelectedInstrumentConfigurations().size();i++)
			{  instrumentName =(String) instrumentBean.getSelectedInstrumentConfigurations().get(i);
			configType = (ConfigureOptionType) OptionsDelegate
					.getOption(instrumentName , configurations);
			cType.add(configType);
			}
			}
					
			for (ConfigureOptionType configureOptionType: cType) {
				
				if (configureOptionType != null
						&& configureOptionType.getSubfields() != null) {
					fieldsDefinitionType.add(configureOptionType.getSubfields());
					instrumentBean.setDisplayFilters(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_FILTER,
									fieldsDefinitionType));
					instrumentBean.setDisplayCustomFilter(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_CUSTOM_FILTER,
									fieldsDefinitionType));
					instrumentBean.setDisplayFiltersw2(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_FILTERW2,
									fieldsDefinitionType));
					instrumentBean.setDisplayGrismw(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_GRISMW,
									fieldsDefinitionType));
					instrumentBean.setDisplayGratingw(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_GRATINGW,
									fieldsDefinitionType));
					instrumentBean.setDisplaySlitw(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_SLITW,
									fieldsDefinitionType));
					instrumentBean.setDisplayReadOut(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_READOUT,
									fieldsDefinitionType));
					instrumentBean.setDisplayMode(OptionsDelegate
							.allowedToDisplay(OpticonConstants.INSTRUMENT_MODE,
									fieldsDefinitionType));
					instrumentBean.setDisplayOrderFilter(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_ORDER_FILTER,
									fieldsDefinitionType));
					instrumentBean.setDisplayPolarimetry(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_POLARIMETRY,
									fieldsDefinitionType));

					instrumentBean.setDisplayWavelength(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_WAVELENGTH,
									fieldsDefinitionType));
					instrumentBean.setDisplayOrder(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_ORDER,
									fieldsDefinitionType));
					instrumentBean.setDisplaySlits(OptionsDelegate
							.allowedToDisplay(OpticonConstants.INSTRUMENT_SLIT,
									fieldsDefinitionType));
					instrumentBean.setDisplayCCD(OptionsDelegate
							.allowedToDisplay(OpticonConstants.INSTRUMENT_CCD,
									fieldsDefinitionType));
					instrumentBean.setDisplayGrating(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_GRATING,
									fieldsDefinitionType));
					instrumentBean.setDisplayGrism(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_GRISM,
									fieldsDefinitionType));
					instrumentBean.setDisplayComments(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_COMMENTS,
									fieldsDefinitionType));
					instrumentBean
							.setDisplayCoronagraphicMask(OptionsDelegate
									.allowedToDisplay(
											OpticonConstants.INSTRUMENT_CORONAGRAPHIC_MASK,
											fieldsDefinitionType));
					instrumentBean
							.setDisplayFrameExposureTime(OptionsDelegate
									.allowedToDisplay(
											OpticonConstants.INSTRUMENT_FRAME_EXPOSURE_TIME_DETAILS,
											fieldsDefinitionType));
					instrumentBean.setDisplayMicrostepping(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_MICROSTEPPING,
									fieldsDefinitionType));
					instrumentBean
							.setDisplaySlitPositionAngle(OptionsDelegate
									.allowedToDisplay(
											OpticonConstants.INSTRUMENT_SLIT_POSITION_ANGLE,
											fieldsDefinitionType));
					instrumentBean.setDisplayGuideline(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_GUIDELINE,
									fieldsDefinitionType));
					instrumentBean.setRequiredFilters(OptionsDelegate
							.isRequired(OpticonConstants.INSTRUMENT_FILTER,
									fieldsDefinitionType));
					instrumentBean.setRequiredFilterDetails(OptionsDelegate
							.isRequired(
									OpticonConstants.INSTRUMENT_FILTER_DETAILS,
									fieldsDefinitionType));
					instrumentBean.setRequiredReadOut(OptionsDelegate
							.isRequired(OpticonConstants.INSTRUMENT_READOUT,
									fieldsDefinitionType));
					instrumentBean.setRequiredMode(OptionsDelegate.isRequired(
							OpticonConstants.INSTRUMENT_MODE,
							fieldsDefinitionType));
					instrumentBean.setRequiredOrderFilter(OptionsDelegate
							.isRequired(
									OpticonConstants.INSTRUMENT_ORDER_FILTER,
									fieldsDefinitionType));
					instrumentBean.setRequiredPolarimetry(OptionsDelegate
							.isRequired(
									OpticonConstants.INSTRUMENT_POLARIMETRY,
									fieldsDefinitionType));
					instrumentBean.setRequiredWavelength(OptionsDelegate
							.isRequired(OpticonConstants.INSTRUMENT_WAVELENGTH,
									fieldsDefinitionType));
					instrumentBean.setRequiredOrder(OptionsDelegate.isRequired(
							OpticonConstants.INSTRUMENT_ORDER,
							fieldsDefinitionType));
					instrumentBean.setRequiredSlits(OptionsDelegate.isRequired(
							OpticonConstants.INSTRUMENT_SLIT,
							fieldsDefinitionType));
					instrumentBean.setRequiredGrating(OptionsDelegate
							.isRequired(OpticonConstants.INSTRUMENT_GRATING,
									fieldsDefinitionType));
					instrumentBean.setRequiredGrism(OptionsDelegate.isRequired(
							OpticonConstants.INSTRUMENT_GRISM,
							fieldsDefinitionType));
					instrumentBean.setRequiredComments(OptionsDelegate
							.isRequired(OpticonConstants.INSTRUMENT_COMMENTS,
									fieldsDefinitionType));
					instrumentBean
							.setRequiredCoronagraphicMask(OptionsDelegate
									.isRequired(
											OpticonConstants.INSTRUMENT_CORONAGRAPHIC_MASK,
											fieldsDefinitionType));
					instrumentBean
							.setRequiredNonDefaultFrameExposureTimeDetails(OptionsDelegate
									.isRequired(
											OpticonConstants.INSTRUMENT_FRAME_EXPOSURE_TIME_DETAILS,
											fieldsDefinitionType));
					instrumentBean.setRequiredMicrostepping(OptionsDelegate
							.isRequired(
									OpticonConstants.INSTRUMENT_MICROSTEPPING,
									fieldsDefinitionType));
					instrumentBean
							.setRequiredSlitPositionAngle(OptionsDelegate
									.isRequired(
											OpticonConstants.INSTRUMENT_SLIT_POSITION_ANGLE,
											fieldsDefinitionType));
					// for lofar
					instrumentBean.setDisplayStation(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_STATION,
									fieldsDefinitionType));
					instrumentBean.setRequiredStation(OptionsDelegate
							.isRequired(OpticonConstants.INSTRUMENT_STATION,
									fieldsDefinitionType));

					instrumentBean.setDisplayClock(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_CLOCK,
									fieldsDefinitionType));
					instrumentBean
							.setDisplaySingleStationSpecifics(OptionsDelegate
									.allowedToDisplay(
											OpticonConstants.INSTRUMENT_SINGLE_STATION,
											fieldsDefinitionType));
					instrumentBean.setRequiredClock(OptionsDelegate.isRequired(
							OpticonConstants.INSTRUMENT_CLOCK,
							fieldsDefinitionType));

					instrumentBean.setDisplayOneFilter(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_ONE_FILTER,
									fieldsDefinitionType));
					instrumentBean.setRequiredOneFilter(OptionsDelegate
							.isRequired(OpticonConstants.INSTRUMENT_ONE_FILTER,
									fieldsDefinitionType));

					instrumentBean.setDisplayAntenna(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_ANTENNA,
									fieldsDefinitionType));
					instrumentBean.setRequiredAntenna(OptionsDelegate
							.isRequired(OpticonConstants.INSTRUMENT_ANTENNA,
									fieldsDefinitionType));

					instrumentBean
							.setDisplaySubarrayPointings(OptionsDelegate
									.allowedToDisplay(
											OpticonConstants.INSTRUMENT_SUBARRAY_POINTINGS,
											fieldsDefinitionType));
					instrumentBean
							.setRequiredSubarrayPointings(OptionsDelegate
									.isRequired(
											OpticonConstants.INSTRUMENT_SUBARRAY_POINTINGS,
											fieldsDefinitionType));

					instrumentBean.setDisplayStoreRawData(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_STORE_RAW_DATA,
									fieldsDefinitionType));

					instrumentBean
							.setDisplayObservationNoiseLevel(OptionsDelegate
									.allowedToDisplay(
											OpticonConstants.OBSERVATION_NOISE_LEVEL,
											fieldsDefinitionType));
					instrumentBean
							.setRequiredObservationNoiseLevel(OptionsDelegate
									.isRequired(
											OpticonConstants.OBSERVATION_NOISE_LEVEL,
											fieldsDefinitionType));

					instrumentBean
							.setDisplayIntegrationTime(OptionsDelegate
									.allowedToDisplay(
											OpticonConstants.INSTRUMENT_INTEGRATION_TIME,
											fieldsDefinitionType));
					instrumentBean
							.setRequiredIntegrationTime(OptionsDelegate
									.isRequired(
											OpticonConstants.INSTRUMENT_INTEGRATION_TIME,
											fieldsDefinitionType));
					instrumentBean
							.setDisplayStoreUVData(OptionsDelegate
									.allowedToDisplay(
											OpticonConstants.INSTRUMENT_STORE_CORRELATED_VISIBILITIES,
											fieldsDefinitionType));
					instrumentBean.setDisplayPiggyBackFeatures(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.DISPLAY_PIGGYBACK_MODE,
									fieldsDefinitionType));

					instrumentBean.setDisplayPiggyBack(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.DISPLAY_PIGGYBACK,
									fieldsDefinitionType));

					if (OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.INSTRUMENT_CORRELATED_VISIBILITIES,
									fieldsDefinitionType)) {
						// for beam observation, show only integration time if correlated visibilities is true
						instrumentBean.setDisplayCorrelatedVisibilities(true);
						if (instrumentBean.isCorrelatedVisibilities()) {
							instrumentBean.setDisplayIntegrationTime(true);
							instrumentBean.setDisplayStoreUVData(true);
						} else {
							instrumentBean.setDisplayIntegrationTime(false);
							instrumentBean.setDisplayStoreUVData(false);
						}
					}
					instrumentBean
							.setDisplayDirectDataStorage(OptionsDelegate
									.allowedToDisplay(
											OpticonConstants.INSTRUMENT_DIRECT_DATASTORAGE,
											fieldsDefinitionType));
					instrumentBean.setDisplayDemixingSource(OptionsDelegate
							.allowedToDisplay(OpticonConstants.DEMIXING_SOURCE,
									fieldsDefinitionType));

					// LOFAR Post Processing
					instrumentBean.setDisplayProcessingMode(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.DISPLAY_PROCESSING_MODE,
									fieldsDefinitionType));

					if (instrumentBean.getSelectedProcessingMode() != null
							&& instrumentBean.getSelectedProcessingMode()
									.equalsIgnoreCase("Calibration + imaging")) {
						instrumentBean
								.setDisplaySubbandsPerImage(OptionsDelegate
										.allowedToDisplay(
												OpticonConstants.DISPLAY_SUBBANDS_PER_IMAGE,
												fieldsDefinitionType));
						instrumentBean.setDisplayFieldOfView(OptionsDelegate
								.allowedToDisplay(
										OpticonConstants.DISPLAY_FIELD_OF_VIEW,
										fieldsDefinitionType));
					}

					instrumentBean.setDisplayFlaggingStrategy(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.FLAGGING_STRATEGIES,
									fieldsDefinitionType));
					instrumentBean.setDisplayPostAveraging(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.DISPLAY_POST_AVERAGING,
									fieldsDefinitionType));
					instrumentBean.setDisplayPostDemixing(OptionsDelegate
							.allowedToDisplay(
									OpticonConstants.DISPLAY_POST_DEMIXING,
									fieldsDefinitionType));

					instrumentBean.setDisplayPostAveraging(OptionsDelegate
							.allowedToDisplay(OpticonConstants.POST_AVERAGING,
									fieldsDefinitionType));
					instrumentBean.setDisplayPostImaging(OptionsDelegate
							.allowedToDisplay(OpticonConstants.POST_IMAGING,
									fieldsDefinitionType));
					instrumentBean.setDisplayPostBeamformed(OptionsDelegate
							.allowedToDisplay(OpticonConstants.POST_BEAMFORMED,
									fieldsDefinitionType));
					instrumentBean.setDisplayPostInterfero(OptionsDelegate
							.allowedToDisplay(OpticonConstants.POST_INTERFERO,
									fieldsDefinitionType));
					instrumentBean.setDisplayPostTimeseries(OptionsDelegate
							.allowedToDisplay(OpticonConstants.POST_TIMESERIES,
									fieldsDefinitionType));

					// special exception for pulsar pipeline mode...
					if (instrumentBean.getSelectedProcessingMode() != null
							&& (instrumentBean.getSelectedProcessingMode()
									.equalsIgnoreCase("Pulsar pipeline")
									//|| instrumentBean.getSelectedProcessingMode().equalsIgnoreCase(OpticonConstants.PROCESSING_MODE_BASELINE)
									|| instrumentBean.getSelectedProcessingMode()
									.equalsIgnoreCase(OpticonConstants.PROCESSING_MODE_USER_SPECIFIED))) {
						
						if(instrumentBean.getSelectedProcessingMode()
								.equalsIgnoreCase("Pulsar pipeline") || instrumentBean.getSelectedProcessingMode()
								.equalsIgnoreCase(OpticonConstants.PROCESSING_MODE_USER_SPECIFIED) )
						{
						instrumentBean.setDisplayFlaggingStrategy(false);
						instrumentBean.setDisplayPostAveraging(false);
						}
						instrumentBean.setDisplayPostDemixing(false);
						instrumentBean.setDisplayComments(true);
						instrumentBean.setDisplayImagingParameters(false);
						instrumentBean.setDisplayDemixingSource(false);
					} else {
						instrumentBean.setDisplayImagingParameters(true);
					}
					


					// For LT and UKIRT
					// if telescope mode instrument is defined, then the original mode will be overwritten...
					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.TELESCOPE_MODE,
							fieldsDefinitionType)) {
						instrumentBean.setObservationModeOptions(OptionsUtils
								.getLabelValueBeansWithoutNoneSpecified(
										OpticonConstants.TELESCOPE_MODE,
										enteredInstruments,
										contextConfiguration));
					}
					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.TELESCOPE_MOON,
							fieldsDefinitionType)) {
						instrumentBean.setObservationPhaseOptions(OptionsUtils
								.getLabelValueBeansWithoutNoneSpecified(
										OpticonConstants.TELESCOPE_MOON,
										enteredInstruments,
										contextConfiguration));
					}
					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.TELESCOPE_SCHEDULING,
							fieldsDefinitionType)) {
						instrumentBean.setDisplayTelescopeScheduling(true);
						instrumentBean
								.setTelescopeSchedulingOptions(OptionsUtils
										.getLabelValueBeansWithoutNoneSpecified(
												OpticonConstants.TELESCOPE_SCHEDULING,
												enteredInstruments,
												contextConfiguration));
					} else {
						instrumentBean.setDisplayTelescopeScheduling(false);
					}
					if (OptionsDelegate.allowedToDisplay(
							OpticonConstants.TELESCOPE_HOURS,
							fieldsDefinitionType)) {
						instrumentBean.setTimeSuffix("(hours)");
					}

				} else {
					instrumentBean.setInstrumentUrl(null);
				}
			}
		}
    	instrumentBean.setDisplayFilterDetails(false);
    	if (instrumentBean.getSelectedFilters()!= null){
        	Set filters = new HashSet();
        	Iterator filtersIt = instrumentBean.getFilters().iterator();
        	while (filtersIt.hasNext())
        	{
        		filters.add(((LabelValueBean) filtersIt.next()).getValue());
        	}
        	/*
        	boolean containsAllFilters = true;
        	for (int i = 0; i < instrumentBean.getSelectedFilters().length;i++)
        	{
        		containsAllFilters = containsAllFilters && 
        			filters.contains(instrumentBean.getSelectedFilters()[i]);
        	}
            
             * if containsAllFilters is false there is a filter in the selectedFilter list
             * that is not in the new list anymore, thus the instrument probably has been changed
             * after the reload. So we need to reset the selected filters.
            
        
        	if (!containsAllFilters){
            	instrumentBean.setSelectedFilters(null);
            }
             */
        	
           	// check if per filter other fields need to be displayed 
           	for (int i = 0; i < instrumentBean.getSelectedFilters().length;i++)
           	{
           		 //displayFilterDetails(instrumentBean.getSelectedFilters()[i], instrumentBean,contextConfiguration);
           		if(instrumentBean.getSelectedFilters()[i].startsWith("Other")){
           			instrumentBean.setDisplayFilterDetails(true);
           		}
           	}
        
        }
    	if(instrumentBean.getFilters() != null && instrumentBean.getFilters().size() > 0){
    		if(instrumentBean.getFilters().get(instrumentBean.getFilters().size()-1) instanceof LabelValueBean){
    			LabelValueBean lastfilter = (LabelValueBean) instrumentBean.getFilters().get(instrumentBean.getFilters().size()-1);
    			if(lastfilter.getLabel().startsWith("Other")){
    				instrumentBean.getFilters().remove(instrumentBean.getFilters().size()-1);
    			}
    		}
    	}
		if (instrumentBean.getSelectedFiltersw2()!= null){
        	Set filtersw2 = new HashSet();
        	Iterator filtersIt = instrumentBean.getFiltersw2().iterator();
        	while (filtersIt.hasNext())
        	{
        		filtersw2.add(((LabelValueBean) filtersIt.next()).getValue());
        	}
        	boolean containsAllFiltersw2 = true;
        	for (int i = 0; i < instrumentBean.getSelectedFiltersw2().length;i++){
        		containsAllFiltersw2 = containsAllFiltersw2 && 
        			filtersw2.contains(instrumentBean.getSelectedFiltersw2()[i]);
        	} 
         	 if (!containsAllFiltersw2) {
             	instrumentBean.setSelectedFiltersw2(null);
             }
		}
		
		if (instrumentBean.getSelectedSlitw()!= null){
        	Set slits = new HashSet();
        	Iterator slitsIt = instrumentBean.getSlitw().iterator();
        	while (slitsIt.hasNext()){
        		slits.add(((LabelValueBean) slitsIt.next()).getValue());
        	}
        	boolean containsAllSlits = true;
        	for (int i = 0; i < instrumentBean.getSelectedFiltersw2().length;i++){
        		containsAllSlits = containsAllSlits && 
        			slits.contains(instrumentBean.getSelectedFiltersw2()[i]);
        	} 
        	
        	 if (!containsAllSlits){
             	instrumentBean.setSelectedSlitw(null);
             }
		}
		
		if (instrumentBean.getSelectedGrismw()!= null){
        	Set grismw = new HashSet();
        	Iterator grismwIt = instrumentBean.getGrismw().iterator();
        	while (grismwIt.hasNext()){
        		grismw.add(((LabelValueBean) grismwIt.next()).getValue());
        	}
        	boolean containsAllgrismw = true;
        	for (int i = 0; i < instrumentBean.getSelectedGrismw().length;i++){
        		containsAllgrismw = containsAllgrismw && 
        		grismw.contains(instrumentBean.getSelectedGrismw()[i]);
        	} 
        	 if (!containsAllgrismw){
             	instrumentBean.setSelectedGrismw(null);
             }
       	}
		
		if (instrumentBean.getSelectedDemixingSources() != null){
        	Set demixingsourcesset = new HashSet();
        	Iterator demiIt = instrumentBean.getDemixingSources().iterator();
        	while (demiIt.hasNext()){
        		demixingsourcesset.add(((LabelValueBean) demiIt.next()).getValue());
        	}
        	boolean containsAlldemi = true;
        	for (int i = 0; i < instrumentBean.getSelectedDemixingSources().length;i++){
        		containsAlldemi = containsAlldemi && 
        				demixingsourcesset.contains(instrumentBean.getSelectedDemixingSources()[i]);
        	} 
        	 if (!containsAlldemi){
             	instrumentBean.setSelectedDemixingSources(null);
             }
       	}
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	public static void fillForm(HttpServletRequest request,
			OpticonObservation opticonObservation, InstrumentBean instrForm) {
			
			instrForm.setSelectedTelescopeConfiguration(opticonObservation.getTelescopeConfiguration());
		 	if (opticonObservation.getInstrument() != null) 
			{
				OpticonInstrument instrument = opticonObservation.getInstrument();
				
				instrForm.setSelectedInstrumentConfiguration(instrument.getName());
				instrForm.setSelectedFilters(
						OptionsUtils.getArray(instrument.getFilters()));
				instrForm.setSelectedFiltersw2(
						OptionsUtils.getArray(instrument.getFiltersw2()));
				instrForm.setSelectedGrismw(
						OptionsUtils.getArray(instrument.getGrismw()));
				instrForm.setSelectedSlitw(
						OptionsUtils.getArray(instrument.getSlitw()));
				instrForm.setSelectedGratingw(
						OptionsUtils.getArray(instrument.getGratingw()));
				
				instrForm.setSelectedDemixingSources( 
						OptionsUtils.getArray(instrument.getDemixingSources()));
				
				instrForm.setSelectedReadOut(instrument.getReadOut());
				instrForm.setSelectedMode(instrument.getMode());
				instrForm.setSelectedOrderFilter(instrument.getOrderFilter());
				instrForm.setPolarimetry(instrument.isPolarimetry());
				instrForm.setGuideline(instrument.isGuideline());
				instrForm.setWavelength(instrument.getWavelength());
				instrForm.setFilterDetails(instrument.getFilterDetails());
				instrForm.setOrder(AstronConverter.toString(instrument.getOrder()));
				instrForm.setSelectedSlit(instrument.getSlit());
				instrForm.setSelectedGrating(instrument.getGrating());
				instrForm.setSelectedGrism(instrument.getGrism());
				instrForm.setSelectedCCD(instrument.getCCD());
				instrForm.setComments(instrument.getComments());
				instrForm.setFrameExposureTime(AstronConverter.toString(instrument.getFrameExposureTime()));
				instrForm.setSelectedMicrostepping(instrument.getMicrostepping());
				instrForm.setCoronagraphicMask(instrument.isCoronagraphicMask());
				instrForm.setNonDefaultFrameExposureTime(instrument.isNonDefaultFrameExposureTime());
				instrForm.setNonDefaultFrameExposureTimeDetails(instrument.getNonDefaultFrameExposureTimeDetails());
				instrForm.setSlitPositionAngle(AstronConverter.toString(instrument.getSlitPositionAngle()));
				//for lofar
				instrForm.setSelectedStation(instrument.getStation());
				instrForm.setSingleStationSpecifics((instrument.getSingleStationSpecifics()));
				instrForm.setCustomStationSpecifics(instrument.getCustomStationSpecifics());
				instrForm.setSelectedClock(instrument.getClock());
				instrForm.setSelectedOneFilter(instrument.getOneFilter());
				instrForm.setFilterFrequency(AstronConverter.toString(instrument.getFilterFrequency()));
				instrForm.setFilterBandwidth(AstronConverter.toString(instrument.getFilterBandwidth()));
				instrForm.setFilterContiguousCoverage(instrument.isFilterContiguousCoverage());
				instrForm.setSelectedAntenna(instrument.getAntenna());
				instrForm.setSubarrayPointings(AstronConverter.toString(instrument.getSubarrayPointings()));
				instrForm.setStoreRawData(instrument.isStoreRawData());
				instrForm.setCorrelatedVisibilities(instrument.isCorrelatedVisibilities() );
				instrForm.setIntegrationTime(AstronConverter.toString(instrument.getIntegrationTime()));
				instrForm.setStoreUVData(instrument.isStoreUVData());
			 	instrForm.setObservationNoiseLevel(AstronConverter.toString(opticonObservation.getNoiseLevel()));
				instrForm.setAveraging(instrument.isAveraging() );
				instrForm.setCalibration(instrument.isCalibration());
				instrForm.setFlagging(instrument.isFlagging());
				instrForm.setImaging(instrument.isImaging());
				instrForm.setSummaryPlots(instrument.isSummaryPlots() );
				instrForm.setSelectedPolarizations(instrument.getPolarization());
				instrForm.setSelectedIncPolarizations(instrument.getIncPolarization());
				instrForm.setFieldSizeX(AstronConverter.toString(instrument.getFieldSizeX()));
				instrForm.setFieldSizeY(AstronConverter.toString(instrument.getFieldSizeY()));
				instrForm.setPixelSizeX(AstronConverter.toString(instrument.getPixelSizeX()));
				instrForm.setPixelSizeY(AstronConverter.toString(instrument.getPixelSizeY()));
				
				instrForm.setPiggyBackSpecifics(instrument.getPiggyBackSpecifics());
				instrForm.setDirectDataStorage(instrument.isDirectDataStorage());
				
				instrForm.setCohstokes(instrument.isCohstokes());
				instrForm.setIncohstokes(instrument.isIncohstokes());
				instrForm.setComplexVoltage(instrument.isComplexVoltage());
				instrForm.setFlysEye(instrument.isFlysEye());
				instrForm.setRawVoltage(instrument.isRawVoltage());
				instrForm.setBeams(AstronConverter.toString(instrument.getBeams()));
				
				instrForm.setCohTaBeams(AstronConverter.toString(instrument.getCohTaBeams()));
				instrForm.setIncohTaBeams(AstronConverter.toString(instrument.getInCohTaBeams()));
				instrForm.setRings(AstronConverter.toString(instrument.getRings()));
				//Added by Roel
				int totalBeams = 0;
				Integer ringsValue = instrument.getRings()!=null?new Integer(instrument.getRings()):null;
				Integer beamsValue = instrument.getCohTaBeams()!=null?new Integer(instrument.getCohTaBeams()):null;
				if(ringsValue != null && beamsValue != null){
					if(ringsValue != 0 && beamsValue !=0){
						totalBeams = 3*(ringsValue*ringsValue + ringsValue) + 1 + beamsValue;
					} else if(ringsValue != 0){
						totalBeams = 3*(ringsValue*ringsValue + ringsValue) + 1;
					} else if(beamsValue != 0){
						totalBeams = beamsValue;
					}
				} else if(ringsValue != null && ringsValue!= 0){
					totalBeams = 3*(ringsValue*ringsValue + ringsValue) + 1;
				} else if(beamsValue != null && beamsValue != 0){
					totalBeams = beamsValue;
				} 
				instrForm.setTotalSum(AstronConverter.toString(totalBeams));
				
				instrForm.setIntsteps(AstronConverter.toString(instrument.getIntsteps() ));
				instrForm.setChannels(AstronConverter.toString(instrument.getChannels()));
				
				instrForm.setIncohIntsteps(AstronConverter.toString(instrument.getIncohIntsteps() ));
				instrForm.setIncchannels(AstronConverter.toString(instrument.getIncchannels()));
				if(instrument.getIncchannels() == null){
					instrForm.setIncchannels("64");
				}
				
				if(instrument.getChannels() == null){
					instrForm.setChannels("64");
				}
				instrForm.setEvents(AstronConverter.toString(instrument.getEvents()));
				instrForm.setSamplerate(AstronConverter.toString(instrument.getSamplerate()));
				instrForm.setFrequencyChannels(AstronConverter.toString(instrument.getFrequencyChannels()));
				
				instrForm.setSelectedProcessingMode(instrument.getProcessingMode() );
				instrForm.setSubbandsPerImage(AstronConverter.toString(instrument.getSubbandsPerImage()) );
				instrForm.setFieldOfView(AstronConverter.toString(instrument.getFieldOfView() ));
				instrForm.setSelectedFlaggingStrategy(instrument.getFlaggingStrategy());
				instrForm.setAveragingTime(AstronConverter.toString(instrument.getAveragingTime()));
				instrForm.setAveragingFrequency(AstronConverter.toString(instrument.getAveragingFrequency()));
				instrForm.setDemixing(instrument.isDemixing());
				
				if(instrument.getDemixingTime() == null){
					instrForm.setDemixingTime("10");
				}else{
					instrForm.setDemixingTime(AstronConverter.toString(instrument.getDemixingTime()));
				}
				if(instrument.getDemixingFrequency() == null){
					instrForm.setDemixingFrequency("64");
				}else{
					instrForm.setDemixingFrequency(AstronConverter.toString(instrument.getDemixingFrequency()));
				}
				// for beam observation, show only integration time if correlated visibilities is true
				if(instrForm.isDisplayCorrelatedVisibilities()){
					if(instrument.isCorrelatedVisibilities()){
						instrForm.setDisplayIntegrationTime(true);
					}else{
						instrForm.setDisplayIntegrationTime(false);
					}
				}
				
				/**
				 * FILL FORM FOR PIPELINE PARAMETERS.
				 */
				instrForm.setSkipRFI(instrument.isSkipRFI());
				instrForm.setSkipFolding(instrument.isSkipFolding());
				instrForm.setSkipPdmp(instrument.isSkipPdmp());
				instrForm.setSkipDspsr(instrument.isSkipDspsr());
				instrForm.setSkipPrepfold(instrument.isSkipPrepfold());
				instrForm.setSinglePulse(instrument.isSinglePulse());
				instrForm.setRratsAnalysis(instrument.isRratsAnalysis());
				instrForm.setSkipDynamicAverage(instrument.isSkipDynamicAverage());
				if(instrument.getSubintegrationLength() == null){
					instrForm.setSubintegrationLength(AstronConverter.toString(0));
				}else{
					instrForm.setSubintegrationLength(AstronConverter.toString(instrument.getSubintegrationLength()));
				}
				instrForm.setConvertRawData(instrument.isConvertRawData());
				
				if(instrument.getThreshold() == null){
					instrForm.setThreshold(AstronConverter.toString(0));
				}else{
					instrForm.setThreshold(AstronConverter.toString(instrument.getThreshold()));
				}
				
				if(instrument.getSigmaLimit() == null){
					instrForm.setSigmaLimit(AstronConverter.toString(0));
				}else{
					instrForm.setSigmaLimit(AstronConverter.toString(instrument.getSigmaLimit()));
				}
				
				if(instrument.getNumberOfBlocks() == null){
					instrForm.setNumberOfBlocks(AstronConverter.toString(0));
				}else{
					instrForm.setNumberOfBlocks(AstronConverter.toString(instrument.getNumberOfBlocks()));
				}
				instrForm.setPrepfoldOptions(instrument.getPrepfoldOptions());
				instrForm.setPresubbandOptions(instrument.getPresubbandOptions());
				instrForm.setRfifind(instrument.getRfifind());
				instrForm.setDspsrOptions(instrument.getDspsrOptions());
				instrForm.setDigifilOptions(instrument.getDigifilOptions());
				instrForm.setPredataOptions(instrument.getPredataOptions());
				instrForm.setBf2fitsOptions(instrument.getBf2fitsOptions());
				instrForm.setPulsarDesc(instrument.getPulsarDesc());
				
				
				
				//instrForm.setEvents(instrument.getEvents());
				//instrForm.setEnablePiggyBack(instrument.isPiggyBack());
				//instrForm.setTbbRequestedMinimumTime(AstronConverter.toString(instrument.getTbbRequestedMinimumTime()));
				//instrForm.setTbbRequestedSATime(AstronConverter.toString(instrument.getTbbRequestedSATime()));
				instrForm.setTbbTriggerLength(AstronConverter.toString(instrument.getTbbTriggerLength()));
				instrForm.setTbbSelectedObservationId(instrument.getTbbSelectedObservationId());
				instrForm.setTbbTriggerRate(AstronConverter.toString(instrument.getTbbTriggerRate()));
				instrForm.setTbbTriggerSourceId(instrument.getTbbTriggerSourceId());
				instrForm.setTbbExposureTime(AstronConverter.toString(instrument.getTbbExposureTime()));
				
				instrForm.setEnablePiggyBack(instrument.isPiggyBack());
				if(instrument.isPiggyBack())
				{	instrument.setBeamConf(false);
					instrument.setInterferoConf(false);
					instrument.setOtherConf(false);
					instrument.setTbbConf(false);
					instrument.getSelectedInstConfs().clear();
					instrument.getSelectedInstConfs().add(OpticonConstants.TBB_PIGGYBACK);
					if(instrument.getDeSelectedInstConfs().contains(OpticonConstants.TBB_PIGGYBACK))
						instrument.getDeSelectedInstConfs().remove(OpticonConstants.TBB_PIGGYBACK);		
				}
				
				instrForm.setBeamConf(instrument.isBeamConf());
				if(instrument.isBeamConf())
				{
					if(!instrument.getSelectedInstConfs().contains("Beam Observation"))
						instrument.getSelectedInstConfs().add("Beam Observation");
					instrument.getDeSelectedInstConfs().remove("Beam Observation");
				}
				instrForm.setInterferoConf(instrument.isInterferoConf());
				if(instrument.isInterferoConf())
				{
					if(!instrument.getSelectedInstConfs().contains("Interferometer"))
						instrument.getSelectedInstConfs().add("Interferometer");
						if(instrument.getDeSelectedInstConfs().contains("Interferometer"))
							instrument.getDeSelectedInstConfs().remove("Interferometer");
				}
				instrForm.setTbbConf(instrument.isTbbConf());
				if(instrument.isTbbConf())
				{
					if(!instrument.getSelectedInstConfs().contains("TBB"))
						instrument.getSelectedInstConfs().add("TBB");
						if(instrument.getDeSelectedInstConfs().contains("TBB"))
							instrument.getDeSelectedInstConfs().remove("TBB");
				}
				instrForm.setOtherConf(instrument.isOtherConf());
				if(instrument.isOtherConf())
					
				{
					if(!instrument.getSelectedInstConfs().contains("Other"))
						instrument.getSelectedInstConfs().add("Other");
						if(instrument.getDeSelectedInstConfs().contains("Other"))
							instrument.getDeSelectedInstConfs().remove("Other");
				}
				instrForm.setDeSelectedInstConf(instrument.getDeSelectedInstConfs());
				instrForm.setSelectedInstrumentConfigurations(instrument.getSelectedInstConfs());
			}
		 	// fill observation allocations
		 	Double specifiedTime = NorthStarDelegate.getResourceValue(
					opticonObservation.getAllocations(),opticonObservation.getTelescopeConfiguration()+"_"+ OpticonConstants.REQUESTEDTIME );
		 	instrForm.setObservationRequestedTime(AstronConverter.toString(specifiedTime));
		 	specifiedTime = NorthStarDelegate.getResourceValue(
					opticonObservation.getAllocations(),opticonObservation.getTelescopeConfiguration()+"_"+OpticonConstants.MINIMUMTIME );	
		 	instrForm.setObservationMinimumTime(AstronConverter.toString(specifiedTime));
		 	// fill observation options
		 	instrForm.setSelectedObservationMode(opticonObservation.getObservationMode() );
		 	instrForm.setSelectedObservationSeeing(opticonObservation.getObservationSeeing() );
		 	instrForm.setSelectedObservationPhase(opticonObservation.getObservationPhase());
		 	instrForm.setSelectedObservationWeather(opticonObservation.getObservationWeather());
		 	
		 	instrForm.setPreferredSchedConstraints(opticonObservation.getPreferredSchedConstraints());
		 	instrForm.setRequiredSchedConstraints(opticonObservation.getRequiredSchedConstraints());
		 	instrForm.setObservationDates(opticonObservation.getObservationDates());
		 	instrForm.setBackupStrategy(opticonObservation.getBackupStrategy() );
		 	instrForm.setCalibrationRequirements(opticonObservation.getCalibrationRequirements());
		 	
		 
		 	if(instrForm.isCohstokes()) {
		 		if(instrForm.getCohTaBeams() == null){
		 			instrForm.setCohTaBeams("0");
		 		}
		 		if(instrForm.getRings() == null){
		 			instrForm.setRings("0");
		 		}
		 		if(instrForm.getIntsteps() == null) {
		 			instrForm.setIntsteps("1");
		 		}
		 	}
		 	
		 	if(instrForm.isIncohstokes()) {
		 		if(instrForm.getIncohIntsteps() == null) {
		 			instrForm.setIncohIntsteps("1");
		 		}
		 	}
	}
	
	public static void setDefaults(ContextType contextConfiguration, InstrumentBean instrForm) {
		if(instrForm.getSelectedClock() == null){
			instrForm.setSelectedClock(OptionsUtils.getDefaultSelectedOptionOrValue(
				OpticonConstants.INSTRUMENT_CLOCK, null,contextConfiguration) );
		}
		
		
	}

	public static Double calculateStorage(OpticonObservation opticonObservation, URL serviceLocation) throws ServiceException {
		/*
		if(serviceLocation != null){
			StorageCalculatorBinding binding = new StorageCalculatorBinding();
			binding.setService(serviceLocation.toString());
			StorageCalculator sCalc = (StorageCalculator) binding;
			try{
				String result = sCalc.sayHello();
				if(result == null || result.indexOf("hello") < 0){
					throw new ServiceException(" incorrect response from "+serviceLocation.toString());
				}
			}catch(RemoteException e ){
				throw new ServiceException("could not connect to "+serviceLocation.toString()+
						" "+e.getMessage());
				
			}
			return calculateStorage(opticonObservation, sCalc);
		}
		*/
		return new Double(0.0);
	}
	
	public static Double calculateStorage(OpticonObservation opticonObservation, double totalDuration, int targetSubbands, StorageCalculator sCalc) throws ServiceException {
 			double result=0d;
			OpticonInstrument instr = opticonObservation.getInstrument();
			if (instr == null){
				return new Double(0d);
			}
			String instrConfig=instr.getName(); //getSelectedInstrumentConfiguration();
			if(AstronValidator.isBlankOrNull(instrConfig)){
				return new Double(0d);
			}
			//clock
			int clock=160;
			if(instr.getClock() != null){
				if(instr.getClock().startsWith("200")){
					clock=200;
				}
			}
			// bandwidth
			double bandw = 0d;
			if(instr.getFilterBandwidth() != null){
				bandw = instr.getFilterBandwidth().doubleValue();
			}
			// observation time
			double totalSpecifiedTime=opticonObservation.getTotalObservationDuration().doubleValue();
			log.info("observationTime : "+totalSpecifiedTime);
			if (totalDuration > 0){
				totalSpecifiedTime = totalDuration;
			}
			//stations
			int stations=0;
			int coreStations = 0;
			int remoteStations = 0;
			int internationalStations = 0;
			String selectedStation= instr.getStation();
			String selectedAntenna =  instr.getAntenna();
			boolean isDual = false;
			if(!AstronValidator.isBlankOrNull(selectedStation)){
				if (!AstronValidator.isBlankOrNull(selectedAntenna) && (selectedAntenna.indexOf("Dual") >= 0)) isDual = true;
				int lbegin = selectedStation.indexOf("(");
				int lend = selectedStation.indexOf(")");
				if(lbegin >=0 && lend >=0){
					selectedStation=selectedStation.substring(lbegin+1,lend);
/*					try{
						stations = new Integer(selectedStation).intValue();
					}catch(NumberFormatException e){
						log.warn(" "+selectedStation+" is not an integer");
					}
					if (isDual) {
						if (stations < 25) {
							stations = stations * 2;
						} else {
							stations = 48 + stations - 24;
						}
						if (stations > 64) stations = 64;
					}*/
					String[] stationsArray = selectedStation.split(",");
			        String coreStationsString = stationsArray[0].trim();
			        String remoteStationsString = stationsArray[1].trim();
			        String internationalStationsString = stationsArray[2].trim();
			        try{
						coreStations = new Integer(coreStationsString).intValue();
					}catch(NumberFormatException e){
						log.warn(" "+coreStationsString+" is not an integer");
					}
			        
			        try{
			        	remoteStations = new Integer(remoteStationsString).intValue();
					}catch(NumberFormatException e){
						log.warn(" "+remoteStationsString+" is not an integer");
					}
			        
			        try{
			        	internationalStations = new Integer(internationalStationsString).intValue();
					}catch(NumberFormatException e){
						log.warn(" "+internationalStationsString+" is not an integer");
					}
			        
			        stations = coreStations + remoteStations + internationalStations;
				}
				if (selectedStation.startsWith(CUSTOM)){
					if(!AstronValidator.isBlankOrNull(instr.getCustomStationSpecifics())){
						lbegin = instr.getCustomStationSpecifics().indexOf("(");
						lend = instr.getCustomStationSpecifics().indexOf(")");
						if(lbegin >=0 && lend >=0){
							selectedStation=instr.getCustomStationSpecifics().substring(lbegin+1,lend);
							try{
								stations = new Integer(selectedStation).intValue();
							}catch(NumberFormatException e){
								log.warn(" "+instr.getCustomStationSpecifics()+" is not an integer");
							}
							if (isDual) {
								if (stations < 25) {
									stations = stations * 2;
								} else {
									stations = 48 + stations - 24;
								}
								if (stations > 64) stations = 64;
							}
						} else {
							lbegin = instr.getCustomStationSpecifics().indexOf("[");
							lend = instr.getCustomStationSpecifics().indexOf("]");
							if(lbegin >=0 && lend >=0){
								selectedStation=instr.getCustomStationSpecifics().substring(lbegin+1,lend);
								stations =selectedStation.split(",").length;
								if (isDual) {
									int nDual = selectedStation.split("CS").length - 1;
									stations = stations + nDual;
									if (stations > 64) stations = 64;
								}
							}
						}
					}
				}
			}
			//antennas
			int antennas=0;
			if(!AstronValidator.isBlankOrNull(selectedAntenna)){
				int lbegin = selectedAntenna.indexOf("(");
				int lend = selectedAntenna.indexOf(")");
				if(lbegin >0 && lend >0){
					selectedAntenna=selectedAntenna.substring(lbegin+1,lend);
					try{
						antennas = new Integer(selectedAntenna).intValue();
					}catch(NumberFormatException e){
						log.warn(" "+selectedAntenna+" is not an integer");
					}
				}
			}
			// max number of events
			int maxEvents = 1;
			
			if(instr.getEvents() != null){
				maxEvents = instr.getEvents().intValue();
			}
			
			
			int cohstokes=0;
			if((instr.isCohstokes() || instr.isFlysEye()) && instr.getPolarization() != null){
				if(instr.getPolarization().equalsIgnoreCase("I")){
					cohstokes=1;
				}else{
					cohstokes=4;
				}
			}
			int incohstokes=0;
			if(instr.isIncohstokes() && instr.getIncPolarization() != null){
				if(instr.getIncPolarization().equalsIgnoreCase("I")){
					incohstokes=1;
				}else{
					incohstokes=4;
				}
			}
			// samples
			int samples=131000;
			
			int channels = 64;
			/*if(instr.getChannels() != null){
				channels = instr.getChannels().intValue();
			}*/
			if(instr.getFrequencyChannels() != null){
				channels = instr.getFrequencyChannels().intValue();
			}
			
			//instr.setDataStorageSpecifics(" no configuration selected");
			if(instrConfig.contains(BEAMFORMED)){
				//
				//  Tied array calculates beamformed data : bandwidth & observation time
				int subbands=targetSubbands;
				int samplespersec=196608;
		    	if(clock==160){
		    		samplespersec=156250;
		    	}
		    	//subbands= (int) Math.round(1000000*bandw/bandwidth);
		    	int bitDepth=4;
		    	double overhead=0;
		    	int intSteps=1;
		    	
		    	if(instr.getIntsteps() != null){
		    		intSteps=instr.getIntsteps().intValue();
		    	}
		    	
				if(totalSpecifiedTime >0 && (instr.isCohstokes() || instr.isIncohstokes() || instr.isFlysEye())){
					try {
						
						/*
						 public double calcBeamSize(int cohTaBeams, int taRingBeams,
			int flysEyeBeams, int cohStokes, int incohTaBeams,
			int incohStokes, int subbands, int samplesPerSec,
			int bytesPerSample, int channelsPerSubband,  
			int cohChannelsPerSubband, int cohIntegrationTime, 
			int incohChannelsPerSubband, int incohIntegrationTime, double observationTime)
						 totalSpecifiedTime
						 */
						int taRings = 1;
						if(instr.getRings() != null){
							taRings = instr.getRings().intValue();
						}
						int cohtabeams =1;
						if(instr.getCohTaBeams() != null){
							cohtabeams = instr.getCohTaBeams().intValue();
						}
						
						int inCohBeams = 1;
						if(instr.getInCohTaBeams() != null){
							inCohBeams = instr.getInCohTaBeams().intValue();
						}
						
					
						//int stations = 0;
						int incohIntSteps = 1;
						if(instr.getIncohIntsteps()!=null)
							incohIntSteps=instr.getIncohIntsteps().intValue();
						boolean isflysEye = instr.isFlysEye();
						
						int cohChannelsPerSubband = channels;
						int incohChannelsPerSubband = 64;
						if(instr.getIncchannels()!=null)
							incohChannelsPerSubband =instr.getIncchannels().intValue();
						
						double out = sCalc.calcBeamSize(cohtabeams, taRings, stations, isflysEye, cohstokes, inCohBeams, 
								incohstokes, subbands, samplespersec, bitDepth, 
								channels, cohChannelsPerSubband, intSteps, 
								incohChannelsPerSubband, incohIntSteps, totalSpecifiedTime);
						result += (out);// / 1048576);
						
						//correlated visiblilities:
						if(instr.isCorrelatedVisibilities() ){
							double integrationTime= 1;
							if(instr.getIntegrationTime() != null){
								integrationTime = instr.getIntegrationTime().doubleValue();
							}
							result += sCalc.calcUVSize(coreStations,
									remoteStations, // nl stations
									internationalStations, // remote
									isDual, // HBA dual
									channels, 
									subbands,
									bitDepth,
									totalSpecifiedTime, 
									integrationTime);
						}
								
								//calcBeamSize(clock, totalSpecifiedTime, 1, 0, 1, , ); //.calcBeamSize(clock, totalSpecifiedTime, 1,1,1);
					} catch (RemoteException e) {
						log.error(e.getMessage());
						return new Double(-1.0);
					}
				}
			}
			if(instrConfig.contains(INTERFERO)){
				// interferro meter uses UV size: stations, observationtime,integrationtime,
				if(instr.isStoreRawData()){
					double integrationTime= 0;
					if(instr.getIntegrationTime() != null){
						integrationTime = instr.getIntegrationTime().doubleValue();
					}
					int polarizations=4;
					
					int subbands=targetSubbands;
					if(targetSubbands > 1){
						//result += 0.5;
					}
					if(integrationTime > 0){
						//result += 100;
					}
					int beams=1;
					int bitDepth=4;
					double overhead=0.2;
					
					double channelwidth=0;
					
					if(instr.getFrequencyChannels() != null){
						channels = instr.getFrequencyChannels().intValue();
					}
					try {
						//result += 1;
						/*
						 public double calcUVSize
						 (int coreStations, int remStations, int intStations, 
						 boolean hbaDual, int channelsPerSubband, int subbands, 
						 int bytesPerSample, double observationTime, double integrationTime) throws RemoteException;
    
						 */
						result += sCalc.calcUVSize(coreStations,
								remoteStations, // nl stations
								internationalStations, // remote
								isDual, // HBA dual
								channels, 
								subbands,
								bitDepth,
								totalSpecifiedTime, 
								integrationTime);
					} catch (RemoteException e) {
						log.error(e.getMessage());
						return new Double(-1.0);
					}
				}
			}
			
			if(instrConfig.contains(TBB)){
				//
				//  Transient buffer board calculates the time series data antennas, stations, samples
				if( stations >0 && opticonObservation.getInstrument().getTbbTriggerLength()!=null && opticonObservation.getInstrument().getTbbTriggerRate()!=null){
					double time=0;
					try {
						String seconds=OpticonTargetUtils.getHoursFromSeconds(totalDuration);
						seconds=seconds.replaceAll(",","");
						time = Double.valueOf(seconds);
					} catch (NumberFormatException e) {
						time=0;
					}		
							
					if(opticonObservation.getInstrument().getTbbExposureTime()!=null)
							time=opticonObservation.getInstrument().getTbbExposureTime();
						double triggerLength = opticonObservation.getInstrument().getTbbTriggerLength().doubleValue();
						result += stations*time*opticonObservation.getInstrument().getTbbTriggerRate()*(190*triggerLength/5);
						log.warn("[tbb:"+result+","+stations+","+time+","+opticonObservation.getInstrument().getTbbTriggerRate()+","+triggerLength);
					
				}
			}
			/*
			if(instrConfig.startsWith(SINGLE)){
				//
				// single station calculates the time series data antennas, stations, samples
				if(antennas > 0 && stations >0){
					try {
						result += sCalc.calcTimeSize(antennas, stations, samples)*maxEvents;
					} catch (RemoteException e) {
						log.error(e.getMessage());
						return new Double(-1.0);
					}
				}
			}
			*/
			if(instrConfig.startsWith(NONSTANDARD)){
				// calculate yourself?
				
				
			}
			//
			//  This is the image size calculation
			// pixelx*pixely*channels*polarizations
			/*
			if(instr.isImaging()){
				//List pols = instr.getPolarizations();
				Integer pixelsx = instr.getPixelSizeX();
				Integer pixelsy = instr.getPixelSizeY();
				if(pixelsx != null && pixelsy != null){
					int polarizations=0;
					
								polarizations=1;
					
					int pixelx= pixelsx.intValue();
					int pixely= pixelsy.intValue();
					int pixelz=0;
					double channels = 65535 /clock;
					channels=channels*bandw;// 
					// channels = bandw * 327 (200MHz)  
					try {
						result += sCalc.calcImageSize(pixelx, pixely, pixelz, polarizations, channels);
					} catch (RemoteException e) {
						log.error(e.getMessage());
						return new Double(-1.0);
					}
					//return new Double(result);
					//result = result
				}
			}
			
			// Averaged visibilities calculation
			if(instr.isAveraging()){
				if ((instr.getAveragingFrequency() != null) && (instr.getAveragingTime() != null)) {
					double integrationTime= instr.getAveragingTime().doubleValue();
					int polarizations=4;
					int subbands=0;
					int beams=1;
					int bitDepth=64;
					double overhead=0.2;
					
					// Determine number of channels from provided channelwidth (scaled to same unit as bandwidth, i.e. MHz), not from clock
					clock = 0;
					double channelwidth=instr.getAveragingFrequency().doubleValue()/1000;
					try {
						result += sCalc.calcUVSize(stations,
								polarizations, 
								integrationTime, 
								subbands, 
								totalSpecifiedTime, 
								bandw, 
								channelwidth, 
								clock, 
								beams, 
								bitDepth, 
								overhead);
					} catch (RemoteException e) {
						log.error(e.getMessage());
						return new Double(-1.0);
					}
				}
			}
		*/
			return new Double(result);
		
	}


	public static String getDataStorageSpecifics(OpticonObservation opticonObservation) {
		OpticonInstrument instr = opticonObservation.getInstrument();
		if (instr == null){
			return "no instrument";
		}
		String instrConfig=instr.getName(); //getSelectedInstrumentConfiguration();
		if(AstronValidator.isBlankOrNull(instrConfig)){
			return "no instrument configuration";
		}
		//clock
		int clock=160;
		if(instr.getClock() != null){
			if(instr.getClock().startsWith("200")){
				clock=200;
			}
		}
		// bandwidth
		double bandw = 0d;
		if(instr.getFilterBandwidth() != null){
			bandw = instr.getFilterBandwidth().doubleValue();
		}
		// observation time
		double totalSpecifiedTime=opticonObservation.getTotalObservationDuration().doubleValue();
		
		//stations
		int stations=0;
		int coreStations = 0;
		int remoteStations = 0;
		int internationalStations = 0;
		String selectedStation= instr.getStation();
		if(!AstronValidator.isBlankOrNull(selectedStation)){
			int lbegin = selectedStation.indexOf("(");
			int lend = selectedStation.indexOf(")");
			if(lbegin >=0 && lend >=0){
				selectedStation=selectedStation.substring(lbegin+1,lend);
				/*try{
					stations = new Integer(selectedStation).intValue();
				}catch(NumberFormatException e){
					log.warn(" "+selectedStation+" is not an integer");
				}*/
				String[] stationsArray = selectedStation.split(",");
		        String coreStationsString = stationsArray[0].trim();
		        String remoteStationsString = stationsArray[1].trim();
		        String internationalStationsString = stationsArray[2].trim();
		        try{
					coreStations = new Integer(coreStationsString).intValue();
				}catch(NumberFormatException e){
					log.warn(" "+coreStationsString+" is not an integer");
				}
		        
		        try{
		        	remoteStations = new Integer(remoteStationsString).intValue();
				}catch(NumberFormatException e){
					log.warn(" "+remoteStationsString+" is not an integer");
				}
		        
		        try{
		        	internationalStations = new Integer(internationalStationsString).intValue();
				}catch(NumberFormatException e){
					log.warn(" "+internationalStationsString+" is not an integer");
				}
		        
		        stations = coreStations + remoteStations + internationalStations;
			}
			if (selectedStation.startsWith(CUSTOM)){
				if(!AstronValidator.isBlankOrNull(instr.getCustomStationSpecifics())){
					lbegin = instr.getCustomStationSpecifics().indexOf("(");
					lend = instr.getCustomStationSpecifics().indexOf(")");
					if(lbegin >=0 && lend >=0){
						selectedStation=instr.getCustomStationSpecifics().substring(lbegin+1,lend);
						try{
							stations = new Integer(selectedStation).intValue();
						}catch(NumberFormatException e){
							log.warn(" "+instr.getCustomStationSpecifics()+" is not an integer");
						}
					} else {
						lbegin = instr.getCustomStationSpecifics().indexOf("[");
						lend = instr.getCustomStationSpecifics().indexOf("]");
						if(lbegin >=0 && lend >=0){
							selectedStation=instr.getCustomStationSpecifics().substring(lbegin+1,lend);
							stations =selectedStation.split(",").length;
						}
					}
				}
			}
		}
		//antennas
		int antennas=0;
		String selectedAntenna =  instr.getAntenna();
		if(!AstronValidator.isBlankOrNull(selectedAntenna)){
			int lbegin = selectedAntenna.indexOf("(");
			int lend = selectedAntenna.indexOf(")");
			if(lbegin >0 && lend >0){
				selectedAntenna=selectedAntenna.substring(lbegin+1,lend);
				try{
					antennas = new Integer(selectedAntenna).intValue();
				}catch(NumberFormatException e){
					log.warn(" "+selectedAntenna+" is not an integer");
				}
			}
		}
		// samples
		int samples=131000;
		int maxEvents = 1;
		if(instr.getEvents() != null){
			maxEvents=instr.getEvents().intValue();
		}
		int beams =1;
		if(instr.getBeams() != null){
			beams = instr.getBeams().intValue();
		}
		String cohstokes="None";
		if(instr.getPolarization() != null){
			cohstokes=instr.getPolarization();
		}
		String incohstokes="None";
		if(instr.getIncPolarization() != null){
			incohstokes=instr.getIncPolarization();
		}
		
		String result = "";
		if(instrConfig.startsWith(BEAMFORMED)){
			//
			//  Tied array calculates beamformed data : bandwidth & observation time
			result =("Observation time = "+totalSpecifiedTime+" s bandwidth = "+ bandw + " MHz coherent Stokes = "+cohstokes+" incoherent stokes = "+incohstokes+" beams = "+beams);
			
		}
		if(instrConfig.startsWith(INTERFERO)){
			// 
			// interferro meter uses UV size: stations, observationtime,integrationtime,
			if(!(instr.getIntegrationTime()==null)){
				result =("Number of stations = "+stations+" Observation time = "+totalSpecifiedTime+
						" s Integration time = "+instr.getIntegrationTime().toString()+" s bandwidth = "+bandw+" MHz clock = "+clock+" MHz");
				
			}else{
				result = ("Number of stations = "+stations+" Observation time = "+totalSpecifiedTime+
						" s Integration time = 0 bandwidth = "+bandw+" MHz clock = "+clock+" MHz");
			}
			if (!instr.isStoreRawData()) {
				result += " raw observation data not stored";
			}
		}
		
		if(instrConfig.startsWith(TBB)){
			//
			//  Transient buffer board calculates the time series data antennas, stations, samples
			result = "Amount of stations = "+stations+" amount of antennas = "+antennas+" samples per event = "+samples;
		}
		if(instrConfig.startsWith(SINGLE)){
			//
			// single station calculates the time series data antennas, stations, samples
			result = "Number of stations = "+stations+" number of antennas = "+antennas+" samples per event = "+samples;
		}
		if(instrConfig.startsWith(NONSTANDARD)){
			// calculate yourself?
			
			
		}
		//
		//  This is the image size calculation
		// pixelx*pixely*channels*polarizations
		if(instr.isImaging()){
			if(instr.getPixelSizeX() != null && instr.getPixelSizeY() != null){
				result += ", pixels="+instr.getPixelSizeX().intValue()*instr.getPixelSizeY().intValue();
			}
		}

		if(instr.isAveraging()){
			if(instr.getAveragingTime() != null && instr.getAveragingFrequency() != null){
				result += ", averaging time interval ="+instr.getAveragingTime().doubleValue() + "s, averaging channel width ="+instr.getAveragingFrequency().doubleValue()*1000 + " Hz";
			}

		}
		
		return result;
	}

	public static StorageCalculator bindCalc(ContextType contextConfiguration) {
		//
		// get calculator url from config file
		StorageCalculator sCalc = null;
		String Url = null;
		java.net.URL serviceLocation = null;
		List urlList = urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.STORAGECALCULATORURL ,
				new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			log.info("Service calling URL : "+Url);
			try {
				serviceLocation = new java.net.URL(Url);
			} catch (MalformedURLException e) {
				log.warn("invalid astron service Url: "+Url);
				serviceLocation = null;
			}
		}
		
		if( serviceLocation == null){
			log.warn("no calculator service location given ");
		}else{
			StorageCalculatorBinding binding = new StorageCalculatorBinding();
			if(binding.setService(serviceLocation.toString()) != 0){
				log.warn("could not connect to "+serviceLocation.toString());
			} else{
				sCalc = (StorageCalculator) binding;
				try {
					String result = sCalc.sayHello();
					if(result == null || result.indexOf("hello") < 0){
						log.warn("incorrect response from "+serviceLocation.toString());
						sCalc = null;
					}else{
						log.info("connected to "+serviceLocation.toString());
					}
				} catch (RemoteException e) {
					log.warn("incorrect response from "+serviceLocation.toString());
					sCalc = null;
				}
			}
		}
		return sCalc;
	}

	public static double calculateCepLoad(OpticonObservation opticonObservation, ContextType contextConfiguration, double dataSize){
		double result =0.0;
		if (opticonObservation.getInstrument() != null) {
			Map enteredInstruments = new HashMap();
			// default offset
			result = new Double(OptionsUtils.getValue("cep-offset", contextConfiguration)).doubleValue();
			
			if(opticonObservation.getInstrument().getName().startsWith(BEAMFORMED)){
				// imaging
				if(opticonObservation.getInstrument().isImaging() ){
					Double value = new Double(OptionsUtils.getValue("cep-imaging", contextConfiguration));
					result += value.doubleValue();
				}
				// rfi checker
				if(opticonObservation.getInstrument().isFlagging() ){
					Double value = new Double(OptionsUtils.getValue("cep-rfichecker", contextConfiguration));
					result += value.doubleValue();
				}
				// foldall
				if(opticonObservation.getInstrument().isCalibration() ){
					Double value = new Double(OptionsUtils.getValue("cep-foldall", contextConfiguration));
					result += value.doubleValue();
				}
				// diagnostic summary plots
				if(opticonObservation.getInstrument().isSummaryPlots() ){
					Double value = new Double(OptionsUtils.getValue("cep-summaryplots", contextConfiguration));
					result += value.doubleValue();
				}
			}
			
			if(opticonObservation.getInstrument().getName().startsWith(INTERFERO)){
				// imaging
				if(opticonObservation.getInstrument().isImaging() ){
					Double value = new Double(OptionsUtils.getValue("cep-imaging", contextConfiguration));
					result += value.doubleValue();
				}
				// flagging
				if(opticonObservation.getInstrument().isFlagging() ){
					Double value = new Double(OptionsUtils.getValue("cep-flagging", contextConfiguration));
					result += value.doubleValue();
				}
				// calibration
				if(opticonObservation.getInstrument().isCalibration() ){
					Double value = new Double(OptionsUtils.getValue("cep-calibration", contextConfiguration));
					result += value.doubleValue();
				}
				// 
				
			}
		}
		
		return result*dataSize;
	}
	
	public static int convertSubbandListDescription(String subbandList) {
		int result = 0;
		List<String> parts = new ArrayList();
		if(subbandList != null &&  subbandList.contains(",")){
			String[] pars = subbandList.split(",");
			for (int i=0;i<pars.length;i++){
				parts.add(pars[i]);
			}
		}else if(subbandList != null && subbandList.length() > 1){
			parts.add(subbandList);
		}
		
		for(String listPart : parts){
			if(listPart.contains("-") || listPart.contains("..")){
				String[] splitparts;
				if(listPart.contains("-")){
					splitparts = listPart.split("-");
				}else{
					splitparts = listPart.split("\\.\\.");
				}
				String begin = splitparts[0]; // 004
				String end = splitparts[1]; // 034556
				try{
					if(Integer.valueOf(begin).intValue() < Integer.valueOf(end).intValue()){
						// 1..10 means 1 t/m 10  so 10 subbands
						result += (Integer.valueOf(end).intValue() - Integer.valueOf(begin).intValue()) + 1;
					}
				}catch(NumberFormatException nex){
					result += 0;
				}
			}
		}
		return result;
	}

	public static HashMap calculateBFAndUVSize(
			OpticonObservation opticonObservation, double totalDuration,
			int targetSubbands, StorageCalculator sCalc) throws ServiceException{
		// TODO Auto-generated method stub
		
		HashMap result = new HashMap();
		
		BigDecimal totalDataSize=new BigDecimal(0);
		OpticonInstrument instr = opticonObservation.getInstrument();
		
				
		if (instr == null){
			return  null;
		}
		
		result.put(OpticonConstants.DATA_STORE_ALLOW, instr.isStoreRawData());
		log.info("allow data"+instr.isStoreRawData());
		result.put(OpticonConstants.DATA_STORE_MODE, instr.getMode());
		result.put(OpticonConstants.UV_DATA_STORE_ALLOWE, instr.isStoreUVData());
		String instrConfig=instr.getName(); //getSelectedInstrumentConfiguration();
		if(AstronValidator.isBlankOrNull(instrConfig)){
			return  null;
		}
		//clock
		int clock=160;
		if(instr.getClock() != null){
			if(instr.getClock().startsWith("200")){
				clock=200;
			}
		}
		// bandwidth
		double bandw = 0d;
		if(instr.getFilterBandwidth() != null){
			bandw = instr.getFilterBandwidth().doubleValue();
		}
		// observation time
		double totalSpecifiedTime=opticonObservation.getTotalObservationDuration().doubleValue();
		if (totalDuration > 0){
			totalSpecifiedTime = totalDuration;
		}
		//stations
		int stations=0;
		int coreStations = 0;
		int remoteStations = 0;
		int internationalStations = 0;
		String selectedStation= instr.getStation();
		String selectedAntenna =  instr.getAntenna();
		boolean isDual = false;
		if(!AstronValidator.isBlankOrNull(selectedStation)){
			if (!AstronValidator.isBlankOrNull(selectedAntenna) && (selectedAntenna.indexOf("Dual") >= 0)) isDual = true;
			int lbegin = selectedStation.indexOf("(");
			int lend = selectedStation.indexOf(")");
			if(lbegin >=0 && lend >=0){
				selectedStation=selectedStation.substring(lbegin+1,lend).trim();
/*				try{
					stations = new Integer(selectedStation).intValue();
				}catch(NumberFormatException e){
					log.warn(" "+selectedStation+" is not an integer");
				}
				if (isDual) {
					if (stations < 25) {
						stations = stations * 2;
					} else {
						stations = 48 + stations - 24;
					}
					if (stations > 64) stations = 64;
				}*/
				String[] stationsArray = selectedStation.split(",");
		        String coreStationsString = stationsArray[0].trim();
		        String remoteStationsString = stationsArray[1].trim();
		        String internationalStationsString = stationsArray[2].trim();
		        try{
					coreStations = new Integer(coreStationsString).intValue();
				}catch(NumberFormatException e){
					log.warn(" "+coreStationsString+" is not an integer");
				}
		        
		        try{
		        	remoteStations = new Integer(remoteStationsString).intValue();
				}catch(NumberFormatException e){
					log.warn(" "+remoteStationsString+" is not an integer");
				}
		        
		        try{
		        	internationalStations = new Integer(internationalStationsString).intValue();
				}catch(NumberFormatException e){
					log.warn(" "+internationalStationsString+" is not an integer");
				}
		        
		        stations = coreStations + remoteStations + internationalStations;
			}
			if (selectedStation.startsWith(CUSTOM)){
				if(!AstronValidator.isBlankOrNull(instr.getCustomStationSpecifics())){
					lbegin = instr.getCustomStationSpecifics().indexOf("(");
					lend = instr.getCustomStationSpecifics().indexOf(")");
					if(lbegin >=0 && lend >=0){
						selectedStation=instr.getCustomStationSpecifics().substring(lbegin+1,lend);
						try{
							stations = new Integer(selectedStation).intValue();
						}catch(NumberFormatException e){
							log.warn(" "+instr.getCustomStationSpecifics()+" is not an integer");
						}
						if (isDual) {
							if (stations < 25) {
								stations = stations * 2;
							} else {
								stations = 48 + stations - 24;
							}
							if (stations > 64) stations = 64;
						}
					} else {
						lbegin = instr.getCustomStationSpecifics().indexOf("[");
						lend = instr.getCustomStationSpecifics().indexOf("]");
						if(lbegin >=0 && lend >=0){
							selectedStation=instr.getCustomStationSpecifics().substring(lbegin+1,lend);
							stations =selectedStation.split(",").length;
							if (isDual) {
								int nDual = selectedStation.split("CS").length - 1;
								stations = stations + nDual;
								if (stations > 64) stations = 64;
							}
						}
					}
				}
			}
		}
		//antennas
		int antennas=0;
		if(!AstronValidator.isBlankOrNull(selectedAntenna)){
			int lbegin = selectedAntenna.indexOf("(");
			int lend = selectedAntenna.indexOf(")");
			if(lbegin >0 && lend >0){
				selectedAntenna=selectedAntenna.substring(lbegin+1,lend);
				try{
					antennas = new Integer(selectedAntenna).intValue();
				}catch(NumberFormatException e){
					log.warn(" "+selectedAntenna+" is not an integer");
				}
			}
		}
		// max number of events
		int maxEvents = 1;
		
		if(instr.getEvents() != null){
			maxEvents = instr.getEvents().intValue();
		}
		
		
		int cohstokes=0;
		if((instr.isCohstokes() || instr.isFlysEye()) && instr.getPolarization() != null){
			if(instr.getPolarization().equalsIgnoreCase("I")){
				cohstokes=1;
			}else{
				cohstokes=4;
			}
		}
		int incohstokes=0;
		if(instr.isIncohstokes() && instr.getIncPolarization() != null){
			if(instr.getIncPolarization().equalsIgnoreCase("I")){
				incohstokes=1;
			}else{
				incohstokes=4;
			}
		}
		// samples
		int samples=131000;
		
		int channels = 64;
		/*if(instr.getChannels() != null){
			channels = instr.getChannels().intValue();
		}*/
		if(instr.getFrequencyChannels() != null){
			channels = instr.getFrequencyChannels().intValue();
		}
		
		//instr.setDataStorageSpecifics(" no configuration selected");
		if(instrConfig.contains(BEAMFORMED)){
			//
			//  Tied array calculates beamformed data : bandwidth & observation time
			log.info("beam formed is allows new method");
			int subbands=targetSubbands;
			int samplespersec=196608;
	    	if(clock==160){
	    		samplespersec=156250;
	    	}
	    	//subbands= (int) Math.round(1000000*bandw/bandwidth);
	    	int bitDepth=4;
	    	double overhead=0;
	    	int intSteps=1;
	    	
	    	if(instr.getIntsteps() != null){
	    		intSteps=instr.getIntsteps().intValue();
	    	}
	    	
			if(totalSpecifiedTime >0 && (instr.isCohstokes() || instr.isIncohstokes() || instr.isFlysEye())){
				try {
					
					
					int taRings = 1;
					if(instr.getRings() != null){
						taRings = instr.getRings().intValue();
					}
					int cohtabeams =1;
					if(instr.getCohTaBeams() != null){
						cohtabeams = instr.getCohTaBeams().intValue();
					}
					
					int inCohBeams = 1;
					if(instr.getInCohTaBeams() != null){
						inCohBeams = instr.getInCohTaBeams().intValue();
					}
					//int stations = 0;
					int incohIntSteps = 1;
					if(instr.getIncohIntsteps()!=null)
						incohIntSteps=instr.getIncohIntsteps().intValue();
					boolean isflysEye = instr.isFlysEye();
					
					int cohChannelsPerSubband = channels;
					int incohChannelsPerSubband = 64;
					if(instr.getIncchannels()!=null)
						incohChannelsPerSubband =instr.getIncchannels().intValue();
					
					double out = 0;
					out=sCalc.calcBeamSize(cohtabeams, taRings, stations, isflysEye, cohstokes, inCohBeams, 
							incohstokes, subbands, samplespersec, bitDepth, 
							channels, cohChannelsPerSubband, intSteps, 
							incohChannelsPerSubband, incohIntSteps, totalSpecifiedTime);
					
					BigDecimal bd = new BigDecimal(out);
					bd = bd.setScale(0, RoundingMode.CEILING);
					if(instr.isStoreRawData())
						totalDataSize=totalDataSize.add(bd);// / 1048576);
					
					result.put(OpticonConstants.BEAM_DATA_SIZE, bd);
					
					
					//correlated visiblilities:
					if(instr.isCorrelatedVisibilities() ){
						double integrationTime= 1;
						if(instr.getIntegrationTime() != null){
							integrationTime = instr.getIntegrationTime().doubleValue();
						}
						double uvSize= 0;
						uvSize=sCalc.calcUVSize(coreStations,
								remoteStations, // nl stations
								internationalStations, // remote
								isDual, // HBA dual
								channels, 
								subbands,
								bitDepth,
								totalSpecifiedTime, 
								integrationTime);
						BigDecimal uvSizebd = new BigDecimal(uvSize);
						uvSizebd = uvSizebd.setScale(0, RoundingMode.CEILING);
						result.put(OpticonConstants.CORRELATED_VISIBILITY_DATA_SIZE, uvSizebd);
						if(instr.isStoreUVData())
							totalDataSize=totalDataSize.add(uvSizebd);
						
					}
					
				
							//calcBeamSize(clock, totalSpecifiedTime, 1, 0, 1, , ); //.calcBeamSize(clock, totalSpecifiedTime, 1,1,1);
				} catch (RemoteException e) {
					log.error(e.getMessage());
					//return null;
				}
			}
		}
		
		
		if(instrConfig.contains(INTERFERO)){
	
				double integrationTime= 0;
				if(instr.getIntegrationTime() != null){
					integrationTime = instr.getIntegrationTime().doubleValue();
				}
				int polarizations=4;
				
				int subbands=targetSubbands;
				if(targetSubbands > 1){
					//result += 0.5;
				}
				if(integrationTime > 0){
					//result += 100;
				}
				int beams=1;
				int bitDepth=4;
				double overhead=0.2;
				
				double channelwidth=0;
				try {
					//result += 1;
					/*
					 public double calcUVSize
					 (int coreStations, int remStations, int intStations, 
					 boolean hbaDual, int channelsPerSubband, int subbands, 
					 int bytesPerSample, double observationTime, double integrationTime) throws RemoteException;

					 */
					log.info("Calculate UVSize invoked method data : coreStations: "+coreStations + " remoteStations: "+remoteStations
							+"internationalStations : "+ internationalStations +" channels:"+channels+" isDual :"+isDual
							+" subbands:"+subbands+" bitDepth:"+bitDepth+" totalSpecifiedTime/ObservationTime:"
							+totalSpecifiedTime+" integrationTime:"+integrationTime);
					double infSize= 0;
					infSize += sCalc.calcUVSize(coreStations,
							remoteStations, 
							internationalStations,
							isDual,
							channels, 
							subbands,
							bitDepth,
							totalSpecifiedTime, 
							integrationTime);
					
					BigDecimal infSizebd = new BigDecimal(infSize);
					log.info("returned data from the service call : "+ infSizebd.doubleValue());
					infSizebd = infSizebd.setScale(0,RoundingMode.CEILING);
					result.put(OpticonConstants.CORRELATED_VISIBILITY_DATA_SIZE, infSizebd)	;
					if(instr.isStoreUVData())
						totalDataSize=totalDataSize.add(infSizebd);
				} catch (RemoteException e) {
					log.error(e.getMessage());
					//return null;
				}
			//}
		}
		
		if(instrConfig.contains(TBB)){//&& opticonObservation.getInstrument()!=null
			result.put(OpticonConstants.DATA_STORE_MODE, TBB);
			if( stations > 0 &&  opticonObservation.getInstrument().getTbbTriggerLength()!=null  && opticonObservation.getInstrument().getTbbTriggerRate()!=null){
				
				double time=0;
				try {
					String seconds=OpticonTargetUtils.getHoursFromSeconds(totalDuration);
					seconds=seconds.replaceAll(",","");
					time = Double.valueOf(seconds);
				} catch (NumberFormatException e) {
					time=0;
				}
				if(opticonObservation.getInstrument().getTbbExposureTime()!=null && opticonObservation.getInstrument().isPiggyBack())
					time=opticonObservation.getInstrument().getTbbExposureTime();
				double triggerLength = opticonObservation.getInstrument().getTbbTriggerLength();
				
				double tbbdataSizeresult = stations*time*opticonObservation.getInstrument().getTbbTriggerRate()*(190*triggerLength/5);
				BigDecimal tbbSizebd = new BigDecimal(tbbdataSizeresult);
				tbbSizebd = tbbSizebd.setScale(0,RoundingMode.CEILING);
				log.warn("[tbb:"+tbbSizebd+",station:"+stations+",time:"+time+",rate:"+opticonObservation.getInstrument().getTbbTriggerRate()+",length:"+triggerLength+",event:"+maxEvents);
				totalDataSize=totalDataSize.add(tbbSizebd);
				result.put(OpticonConstants.TBB_DATA_SIZE, tbbSizebd);
			
			}
		}
			result.put(OpticonConstants.TOTAL_DATA_SIZE, totalDataSize);
		return result;
	}
	

	public static HashMap calculateProcessingTime(String mode,
			String flaggingStrategy, String antenna, boolean isDemixing,
			List<String> demixingSources, int totalStation,
			int totalTargetSubband, double totalObservingDuration,
			double fieldOfView, double uvRange, StorageCalculator sCalc, double baselineFactor, String contextPath, String  processingMode)
			throws ServiceException {
		
		HashMap processingTimeResult = new HashMap();
		double totalProcessingTime = 0;
		double ratio=0;
		double cRatio=1d;
		String type = "";
		int numberSource = 0;

		if (!AstronValidator.isBlankOrNull(antenna)) {
			if (antenna.contains("HBA"))
				type = "HBA";
			else if (antenna.contains("LBA"))
				type = "LBA";
			else
				type = "HBA";
		}
		else if (!AstronValidator.isBlankOrNull(flaggingStrategy)){
			if(flaggingStrategy.contains("HBA")||flaggingStrategy.equals("default"))
				type = "HBA";
			else if ( flaggingStrategy.contains("LBA"))
				type = "LBA";
			else
				type = "HBA";
				
		}
		
		if (isDemixing && demixingSources != null) {
			numberSource = demixingSources.size();
		}

		try {
			//log.info("Service Invoked for po ratio : "+sCalc.get);
			File fXmlFile = new File(contextPath+OpticonConstants.CONFIG_FILE_LOCATION);
			if(fXmlFile.exists()){
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(fXmlFile);
				doc.getDocumentElement().normalize();
				
				NodeList nListPORatio = doc.getElementsByTagName("PO");
				NodeList nListPOFactor = doc.getElementsByTagName("PO-Factor");
				
				double poRatio = getPORatio(nListPORatio, type, numberSource, totalTargetSubband, processingMode);
				double xFactor = getPOFactor(nListPOFactor, type, totalTargetSubband, processingMode);
				/*int xFactor = 1;
				if (totalTargetSubband > 160 && totalTargetSubband <= 320)
					xFactor = 2;
				else if (totalTargetSubband > 320)
					xFactor = 3;*/
				cRatio = xFactor * poRatio;
			}else{
				//cRatio = sCalc.getCPORatio(type, numberSource, totalTargetSubband);
				cRatio = 0;
			}
			NumberFormat nf = NumberFormat.getInstance();
            nf.setMaximumFractionDigits(2);
            nf.format(cRatio);
			processingTimeResult.put(OpticonConstants.PROCESSING_TIME_RATIO,nf.format(cRatio));
			/*double calibarationProcessingTime = sCalc.calcCPT(type, isDemixing,
					numberSource, totalStation, totalTargetSubband,
					totalObservingDuration);*/
			
			double calibarationProcessingTime = totalObservingDuration*cRatio;
			
			processingTimeResult.put(
					OpticonConstants.PROCESSING_TIME_CALIBRATION,
					OpticonTargetUtils
							.getHoursFromSeconds(calibarationProcessingTime));
			totalProcessingTime += calibarationProcessingTime;
		} catch (RemoteException e1) {
			log.error(e1.getMessage());
		} catch (ParserConfigurationException e1) {
			log.error(e1.getMessage());
		} catch (SAXException e1) {
			log.error(e1.getMessage());
		} catch (IOException e1) {
			log.error(e1.getMessage());
		}

		if (mode!=null && mode.equals(OpticonConstants.PROCESSING_MODE_CALIBRATION_AND_IMAGING)) {
			

		
			try {
				ratio=sCalc.getIPORatio(type, fieldOfView, uvRange);
				NumberFormat nf = NumberFormat.getInstance();
	            nf.setMaximumFractionDigits(2);
	            nf.format(ratio);
	            nf.format(cRatio);
				processingTimeResult.put(OpticonConstants.PROCESSING_TIME_RATIO_IMAGING,"c:"+nf.format(cRatio)+"i:"+nf.format(ratio));
				double imagingProcessingtime = 0;

				imagingProcessingtime = sCalc.calcIPT(type, fieldOfView,
						uvRange, totalObservingDuration);
				imagingProcessingtime = totalObservingDuration*cRatio;
				totalProcessingTime += imagingProcessingtime;

				processingTimeResult.put(
						OpticonConstants.PROCESSING_TIME_IMAGING,
						OpticonTargetUtils
								.getHoursFromSeconds(imagingProcessingtime));
			} catch (RemoteException e) {
				log.error(e.getMessage());
				// return null;
			}
		}
		
		if (mode!=null && mode.equals(OpticonConstants.PROCESSING_MODE_BASELINE)) {
			
			// HAH 20160913: Commented out next line as it causes quadratic application of the cRatio
			// and baselinefactor seems not to be used any longer
			
			//totalProcessingTime = totalProcessingTime*(baselineFactor*cRatio);
		}
		
		if(mode!=null && mode.equals(OpticonConstants.PROCESSING_MODE_PULSAR))
		{
			//totalProcessingTime=totalProcessingTime;
		}
		NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        nf.format(totalProcessingTime);
		processingTimeResult.put(OpticonConstants.PROCESSING_TIME_TOTAL,
				OpticonTargetUtils.getHoursFromSeconds(totalProcessingTime));
		return processingTimeResult;

	}

	private static double getPOFactor(NodeList nListPOFactor, String type, int totalTargetSubband, String processingMode) {
		double poFactor = 0;
		processingMode = processingMode != null ? processingMode : "";
		for (int temp = 0; temp < nListPOFactor.getLength(); temp++) {
			Node nNode = nListPOFactor.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				String antennaType = eElement.getAttribute("type") != null ? eElement.getAttribute("type") : "";
				int minNoOfSubband = eElement.getAttribute("minNoOfSubband")!= null && !eElement.getAttribute("minNoOfSubband").isEmpty()? Integer.parseInt(eElement.getAttribute("minNoOfSubband")) : -1;
				int maxNoOfSubband = eElement.getAttribute("maxNoOfSubband")!= null && !eElement.getAttribute("maxNoOfSubband").isEmpty() ? Integer.parseInt(eElement.getAttribute("maxNoOfSubband")) : -1;
				double factor = eElement.getTextContent()!= null ? Double.parseDouble(eElement.getTextContent()) : -1;
				String processingModeStr = eElement.getAttribute("processingMode") != null ? eElement.getAttribute("processingMode") : "";
				String[] processingModeArray = processingModeStr.split(",");
				for (String finalProcessingMode : processingModeArray) {
					if(processingMode.equalsIgnoreCase(finalProcessingMode.trim()) && type.equals(antennaType)){
						if(totalTargetSubband > minNoOfSubband && totalTargetSubband <= maxNoOfSubband){
							poFactor = factor;
						}
					}
				}
			}
		}
		return poFactor;
	}

	private static double getPORatio(NodeList nList, String type, int numberSource, int totalTargetSubband, String processingMode) {
		double poRatio = 0;
		processingMode = processingMode != null ? processingMode : "";
		List<POValuesBean> poValuesBeanList = new ArrayList<POValuesBean>();
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				String antennaType = eElement.getAttribute("type") != null ? eElement.getAttribute("type") : "";
				int numberofDemixingSources = eElement.getAttribute("numberofDemixingSources")!= null && !eElement.getAttribute("numberofDemixingSources").isEmpty()? Integer.parseInt(eElement.getAttribute("numberofDemixingSources")) : -1;
				int totalTargetSubbandMin = eElement.getAttribute("totalTargetSubbandMin")!= null && !eElement.getAttribute("totalTargetSubbandMin").isEmpty() ? Integer.parseInt(eElement.getAttribute("totalTargetSubbandMin")) : -1;
				double poRatioConf = eElement.getElementsByTagName("Ratio").item(0).getTextContent()!= null ? Double.parseDouble(eElement.getElementsByTagName("Ratio").item(0).getTextContent()) : -1;
				String processingModeStr = eElement.getAttribute("processingMode") != null ? eElement.getAttribute("processingMode") : "";
				POValuesBean poValuesBean = new POValuesBean();
				poValuesBean.setAntennaType(antennaType);
				poValuesBean.setNumberofDemixingSources(numberofDemixingSources);
				poValuesBean.setTotalTargetSubbandMin(totalTargetSubbandMin);
				poValuesBean.setPoRatio(poRatioConf);
				poValuesBean.setProcessingMode(processingModeStr);
				poValuesBeanList.add(poValuesBean);
			}
		}
		Map<Integer, List<Map>> demixingMap = new HashMap<Integer, List<Map>>();
		Map<Integer, Double> finalMap = new HashMap<Integer, Double>();
		int noOfDemxSrc = -10;
		for (POValuesBean poValuesBean : poValuesBeanList) {
			String[] processingModeArray = poValuesBean.getProcessingMode().split(",");
			for (String finalProcessingMode : processingModeArray) {
				if(processingMode.equalsIgnoreCase(finalProcessingMode.trim()) && type.equals(poValuesBean.getAntennaType())){
					if(demixingMap.get(poValuesBean.getNumberofDemixingSources())!=null){
						List<Map> list = demixingMap.get(poValuesBean.getNumberofDemixingSources());
						Map<Integer, Double> valueMap = new HashMap<Integer, Double>();
						valueMap.put(poValuesBean.getTotalTargetSubbandMin(), poValuesBean.getPoRatio());
						list.add(valueMap);
					}else{
						List<Map> list = new ArrayList<Map>();
						Map<Integer, Double> valueMap = new HashMap<Integer, Double>();
						valueMap.put(poValuesBean.getTotalTargetSubbandMin(), poValuesBean.getPoRatio());
						list.add(valueMap);
						demixingMap.put(poValuesBean.getNumberofDemixingSources(),list);
					}
				}
			}
			/*if(type.equals(poValuesBean.getAntennaType()) && type.equals("LBA")){
				if(demixingMap.get(poValuesBean.getNumberofDemixingSources())!=null){
					List<Map> list = demixingMap.get(poValuesBean.getNumberofDemixingSources());
					Map<Integer, Double> valueMap = new HashMap<Integer, Double>();
					valueMap.put(poValuesBean.getTotalTargetSubbandMin(), poValuesBean.getPoRatio());
					list.add(valueMap);
				}else{
					List<Map> list = new ArrayList<Map>();
					Map<Integer, Double> valueMap = new HashMap<Integer, Double>();
					valueMap.put(poValuesBean.getTotalTargetSubbandMin(), poValuesBean.getPoRatio());
					list.add(valueMap);
					demixingMap.put(poValuesBean.getNumberofDemixingSources(),list);
				}
			}*/
		}
		if(demixingMap != null && !demixingMap.isEmpty()){
			if(demixingMap.get(numberSource) != null && demixingMap.get(numberSource).size() > 0){
				List<Map> listMap = demixingMap.get(numberSource);
				int mapKey = -99;
				for (Map<Integer, Double> map : listMap) {
					for ( Integer key : map.keySet() ) {
					    if(totalTargetSubband >= key){
					    	mapKey = key;
					    }else{
					    	if(mapKey == -99){
					    		mapKey = key;
					    	}
					    }
					}
					if(map.get(mapKey) != null){
						poRatio = map.get(mapKey);
					}
				}
			}else{
				int maxKey = findMax(demixingMap.keySet());
				if(numberSource >= maxKey)
					numberSource = maxKey;
				List<Map> listMap = demixingMap.get(maxKey);
				int mapKey = -99;
				if(listMap != null && listMap.size() > 0){
					for (Map<Integer, Double> map : listMap) {
						for ( Integer key : map.keySet() ) {
						    if(totalTargetSubband >= key){
						    	mapKey = key;
						    }else{
						    	if(mapKey == -99){
						    		mapKey = key;
						    	}
						    }
						}
						if(!map.isEmpty() && map.get(mapKey) != null){
							poRatio = map.get(mapKey);
						}
					}
				}
			}
		}
		return poRatio;
	}
	
	public static int findMax(Set<Integer> keySet) {
	   int max = Integer.MIN_VALUE;

	   for (int d : keySet) {
	      if (d > max) max = d;
	   }

	   return max;
	}

}

class POValuesBean{
	private String antennaType;
	private int numberofDemixingSources;
	private int totalTargetSubbandMin;
	private double poRatio;
	private String processingMode;
	public int getNumberofDemixingSources() {
		return numberofDemixingSources;
	}
	public void setNumberofDemixingSources(int numberofDemixingSources) {
		this.numberofDemixingSources = numberofDemixingSources;
	}
	public int getTotalTargetSubbandMin() {
		return totalTargetSubbandMin;
	}
	public void setTotalTargetSubbandMin(int totalTargetSubbandMin) {
		this.totalTargetSubbandMin = totalTargetSubbandMin;
	}
	public double getPoRatio() {
		return poRatio;
	}
	public void setPoRatio(double poRatio) {
		this.poRatio = poRatio;
	}
	public String getAntennaType() {
		return antennaType;
	}
	public void setAntennaType(String antennaType) {
		this.antennaType = antennaType;
	}
	
	public String getProcessingMode() {
		return processingMode;
	}
	public void setProcessingMode(String processingMode) {
		this.processingMode = processingMode;
	}
	@Override
	public String toString() {
		return "POValuesBean [antennaType=" + antennaType + ", numberofDemixingSources=" + numberofDemixingSources
				+ ", totalTargetSubbandMin=" + totalTargetSubbandMin + ", poRatio=" + poRatio + ", processingMode=" + processingMode + "]";
	}
	
	
}

