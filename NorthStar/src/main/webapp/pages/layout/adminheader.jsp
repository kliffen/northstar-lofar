<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<TABLE width="100%">


	<TR>
		<TD width="100%">
		<TABLE width="100%">
		 		<TR>
		 			<TD width="100">
		 			
					<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		 					<TABLE width="100" height="25">
								<TR>
						  			<TD width='20'><%= "<a title=\"Change personal information. Opens in new window.\" href=\"" + "/useradministration/user/setUpUserAccountOverview.do\" target=\"_blank\"><img width='20'src=\"" +request.getContextPath() + "/images/list/user.gif\"></a>"%></TD>
					  				<TD width="80"><%= "<a title=\"Change personal information. Opens in new window.\" href=\"" + "/useradministration/user/setUpUserAccountOverview.do\" target=\"_blank\">My Account</a>"%></TD>
				  				</TR>
					  			</TABLE>
									  					  		
			  		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />		  		
			  		
			  		</TD>	  		
			  						
					
					<TD width="*" class="center"><html:img page="<%=eu.radionet.northstar.business.configuration.NorthStarConfiguration.getBannerImageUri()%>" module=""/></TD>
					<TD width="120">
						
						<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
						<TABLE width="120" height="25">
							<TR>
				  				<TD width="120" height="25"><a title="Administrative index" href="<%=request.getContextPath()%>/administrative">Administrative index</a></TD>
			  				</TR>
			  			</TABLE>
						<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
					
					<TD width="70">
		 			
					<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		 				<TABLE width="70" height="25">
							<TR>
					  			<TD width='20' height="25"><a title="Logout" href="<%=request.getContextPath()%>/logout.do"><img width="20" src="<%=request.getContextPath()%>/images/list/logout.gif"></a></TD>
				  				<TD width="50" height="25"><a title="Logout" href="<%=request.getContextPath()%>/logout.do">Logout</a></TD>
			  				</TR>
			  			</TABLE>
			  			<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
			  		</TD>
				</TR>
			</TABLE>
		</TD>
	</TR>
</TABLE>
