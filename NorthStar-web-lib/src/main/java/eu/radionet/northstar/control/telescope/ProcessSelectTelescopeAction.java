// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessSelectTelescopeAction.java 
 *
 * Created on Feb 17, 2005
 *
 * Version $Id: ProcessSelectTelescopeAction.java,v 1.1 2006-05-03 10:17:21 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.telescope;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.Semester;

/**
 * ProcessSelectTelescopeAction
 * 
 * @author Nico Vermaas
 */

public class ProcessSelectTelescopeAction extends Action {
	private Log log = LogFactory.getLog(this.getClass());

	private NorthStarDelegate northStarDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();
		SelectTelescopeForm telescopeForm = (SelectTelescopeForm) form;
		ActionMessages errors = new ActionMessages();
		if (isCancelled(request)) {
			return mapping.findForward("back");
		}
		UserAccount myUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (telescopeForm.getSelectButton() != null) {
			// get the selected telescope from the form-bean and put it on the
			// session
			// session.setAttribute(Constants.TELESCOPE,myTelescope);

			northStarDelegate = NorthStarDelegate.getInstance();

			Semester semester = northStarDelegate.getSemester(AstronConverter
					.toInteger(telescopeForm.getSelectedSemesterId()));

			/*
			 * if id is given, it must be copy
			 */
			if (AstronValidator.isPositiveInt(telescopeForm.getProposalId())) {
				Proposal proposal = null;
				/*
				 * retrieve oldProposal
				 */
				try {
					proposal = northStarDelegate.getOwnProposal(new Integer(
							telescopeForm.getProposalId()), myUserAccount);
				} catch (DatabaseException de) {
					log
							.error(LogMessage.getMessage(myUserAccount,
									"Cannot retrieve proposal: "
											+ de.getMessage(), de));
					errors.add(Constants.ERROR, new ActionMessage(
							"error.database.retrieve.proposal"));
					saveErrors(request, errors);
					return mapping.findForward("refresh");
				}

				// copy button
				try {
					ProposalDelegate proposalDelegate = ProposalDelegateFactory
							.getProposalDelegate(semester);
					proposalDelegate.copyProposalAndStore(proposal,
							myUserAccount, semester);
				} catch (Exception e) {
					log.error(LogMessage
							.getMessage(myUserAccount,
									"Cannot copy and store proposal: "
											+ e.getMessage(), e));
					errors.add(Constants.ERROR, new ActionMessage(
							"error.database.copy.proposal"));
					saveErrors(request, errors);
					return mapping.findForward("refresh");
				}
				return mapping.findForward("back");
			} else {

				if (!AstronValidator.isBlankOrNull(semester.getTelescope())) {
					try {

						Proposal proposal = null;
						proposal = northStarDelegate.getNewProposal(
								myUserAccount, semester);
						ProposalData proposalData = NorthStarUtils
								.getNewProposalData(proposal);
						session.setAttribute(Constants.PROPOSAL_DATA,
								proposalData);
						session.setMaxInactiveInterval(28800);
						return mapping.findForward("success");

					} catch (DatabaseException de) {
						log.error(LogMessage.getMessage(myUserAccount,
								"Error creating new proposal: "
										+ de.getMessage(), de));
						errors.add(Constants.ERROR, new ActionMessage(
								"error.database.retrieve.proposal.initial"));
						saveErrors(request, errors);
						return mapping.findForward("refresh");
					}
				} else {
					errors.add(Constants.ERROR, new ActionMessage(
							"error.required.proposal.telescope"));
					saveErrors(request, errors);
					return mapping.findForward("refresh");
				}
			}
		}
		return mapping.findForward("refresh");

	}

}