insert into `category` (code) values ('ddt');
insert into `category` (code) values ('envelope_sheet');

ALTER TABLE `opticontarget` 
 ADD COLUMN `subband_list` VARCHAR(255)  DEFAULT NULL,
 ADD COLUMN `specify_subband_list` TINYINT(1)  NOT NULL DEFAULT 0,
 ADD COLUMN `central_frequency` DOUBLE  DEFAULT NULL,
 ADD COLUMN `bandwidth` DOUBLE  DEFAULT NULL,
 ADD COLUMN `observingrun` VARCHAR(255)  DEFAULT NULL,
 ADD COLUMN `runnumber` VARCHAR(255)  DEFAULT NULL,
 ADD COLUMN `pipeline` VARCHAR(255)  DEFAULT NULL,
 ADD COLUMN `calibration_beam` TINYINT(1)  NOT NULL DEFAULT 0,
 ADD COLUMN `firstinrun` TINYINT(1)  NOT NULL DEFAULT 0;

ALTER TABLE `observation`
 ADD COLUMN `observationkind` VARCHAR(255)  DEFAULT NULL;
 
 ALTER TABLE `justification`
 ADD COLUMN `envelopesheet` VARCHAR(255)  DEFAULT NULL;
 
 ALTER TABLE `opticoninstrument`
 ADD COLUMN `processing_mode` VARCHAR(255)  DEFAULT NULL,
 ADD COLUMN `subbands_image` integer  DEFAULT NULL,
 ADD COLUMN `field_view` double  DEFAULT NULL,
 ADD COLUMN `demixing` tinyint(1)  Not NULL default 0,
 ADD COLUMN `demixing_time` double  DEFAULT NULL,
 ADD COLUMN `demixing_frequency` double  DEFAULT NULL,
 ADD COLUMN `flagging_strategy` VARCHAR(255)  DEFAULT NULL,
  ADD COLUMN `cohtabeams` INTEGER  DEFAULT NULL,
   ADD COLUMN `incohtabeams` INTEGER  DEFAULT NULL,
    ADD COLUMN `rings` INTEGER  DEFAULT NULL,
DROP TABLE IF EXISTS `opticoninstrumentdemixingsources`;
CREATE TABLE `opticoninstrumentdemixingsources` (
  `id` int(11) NOT NULL auto_increment,
  `instrumentid` int(11) default NULL,
  `indexid` int(11) default NULL,
  `value` varchar(255) default NULL,
  PRIMARY KEY  (`id`),
  INDEX  `instrument_opticondemixingsources_IND` (`instrumentid`),
  INDEX  `instrument_index_opticondemixingsources_IND` (`indexid`),
  CONSTRAINT `instrument_opticondemixingsources_FK` FOREIGN KEY (`instrumentid`) REFERENCES `opticoninstrument` (`id`) ON DELETE CASCADE
) TYPE=InnoDB;
