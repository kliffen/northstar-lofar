// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProposalWriter.java 
 *
 * Created on Mar 7, 2005
 *
 * Version $Id: ProposalWriter.java,v 1.18 2008-12-04 11:14:25 boelen Exp $
 *
 */
package eu.radionet.northstar.business.pdf;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.ServletContext;

import nl.astron.useradministration.data.entities.UserAffiliation;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BadPdfFormatException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfPageEvent;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

import eu.radionet.northstar.business.Converter;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.data.Constants;
import eu.radionet.northstar.data.entities.AdditionalIssues;
import eu.radionet.northstar.data.entities.Category;
import eu.radionet.northstar.data.entities.FigureFile;
import eu.radionet.northstar.data.entities.Justification;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.ScientificFile;
import eu.radionet.northstar.data.entities.TechnicalDetailsFile;
import eu.radionet.northstar.data.entities.Thesis;

/**
 * The ProposalWriter provides general pdf support
 * 
 * @author Bastiaan Verhoef
 */
public class ProposalWriter extends DocumentWriter {
	private Log log = LogFactory.getLog(this.getClass());
	
	protected Image headerImage = null;

	protected ScientificFile scientificFile = null;

	protected TechnicalDetailsFile technicalDetailsFile = null;

	protected FigureFile figureFile = null;

	protected RegisteredMember correspondingAuthor = null;

	protected int header_title_length = 0;

	protected Member pi = null;

	protected String dateString = null;

	protected PdfPTable firstHeader = null;

	protected PdfPTable header = null;

	protected int pageNumber = 1;
	protected ContextType contextConfiguration = null;
	protected String contextPath = "";

	public ProposalWriter() {
		super();
	}

	public static int getNumberOfPages(String fileName, byte[] file)
			throws IOException {
		if (file != null && fileName != null) {
			String fileNameTemp = fileName.toLowerCase();
			/*
			 * if it ends with jpg or gif, it must be an picture
			 */
			if (fileNameTemp.endsWith(".jpg") 
					|| fileNameTemp.endsWith(".jpeg")
					|| fileNameTemp.endsWith(".gif")
					|| fileNameTemp.endsWith(".png")) 
			{
				return 1;

			}
			PdfReader reader = new PdfReader(file);
			return reader.getNumberOfPages();
		}
		return -1;
	}

	/**
	 * The setHeader set the header image
	 * 
	 * @param image
	 * @throws BadElementException
	 * @throws IOException
	 */
	public void setHeaderImage(byte[] image) throws BadElementException,
			IOException {
		headerImage = Image.getInstance(image);
		headerImage.scalePercent(60);
		headerImage.setAlignment(Image.MIDDLE);
	}

	/**
	 * The setMetaData set the metadata in the pdf document
	 * 
	 * @param proposal
	 * @param document
	 */
	protected void setMetaData(Proposal proposal, Document document) {
		/*
		 * get contact author of the proposal
		 */
		RegisteredMember registeredMember = getContactAuthor(proposal
				.getMembers());
		/*
		 * author is contact author of the proposal
		 */
		document.addAuthor(registeredMember.getUser().getName());
		/*
		 * add the title
		 */
		if (proposal.getJustification() != null
				&& !AstronValidator.isBlankOrNull(proposal.getJustification()
						.getTitle())) {
			document.addTitle(proposal.getJustification().getTitle());
		} else {
			document.addTitle("not specified");
		}
		/*
		 * add subject
		 */
		document.addSubject(getSubject(proposal));
		document.addCreator("NorthStar");
	}

    protected String getSubject(Proposal proposal) {
    	String telescope = proposal.getSemester().getTelescope();
        if (!AstronValidator.isBlankOrNull(proposal.getCode())) {
            return "Proposal: " + proposal.getCode() + " for " + telescope + "-telescope";
        } else {
            return "Proposal for " + telescope + "-telescope";
        }

    }

	/**
	 * The write method creates the master pdf document
	 * 
	 * @param proposal
	 * @param outputStream
	 * @throws DocumentException
	 * @throws IOException
	 */
	public void write(Proposal proposal, OutputStream outputStream, ServletContext ServletContext)
			throws DocumentException, IOException {
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		firstHeader = getFirstHeader(proposal);
		
		contextPath = ServletContext.getRealPath("/");

		/*
		 * create the master document
		 */
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN);
		header = getHeader(proposal, document);

		/*
		 * instantiate pdf copy for merging pdf documents
		 */

		PdfCopy pdfCopy = new PdfCopy(document, outputStream);

		setMetaData(proposal, document);
		document.open();

		/*
		 * add our generated pdf
		 */
		addPdf(pdfCopy, new PdfReader(getProposalPdf(proposal)));
		addPdf(pdfCopy, new PdfReader(getFiles(proposal)));

		/*
		 * add our generated pdf
		 */
		if (proposal.getObservingRequest().getObservations().size() > 0) {
			try {
				addPdf(pdfCopy, new PdfReader(
						getObservationsDetailsPdf(proposal
								.getObservingRequest().getObservations())));
			} catch (DocumentException de) {
				if (log.isDebugEnabled()) {
					// log.debug("Has no observation details");
				}
			}
		}

		document.close();

	}

	protected byte[] getFiles(Proposal proposal) throws DocumentException {
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN);
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		/*
		 * instantiate outputstream
		 */
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				byteArrayOutputStream);
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(header,
				HEADER_SIZE);

		pdfWriter.setPageEvent(pageEvent);
		document.open();
		/*
		 * if scientific file exist, try to merge it
		 */
		if (proposal.getJustification() == null){
			proposal.setJustification(new Justification()); 
		}
		
		String scientificFileName = proposal.getJustification().getScientificFileName();
		if (this.scientificFile != null && scientificFileName != null && this.scientificFile.getUploadFile() != null) {
			String footer = scientificFileName + " uploaded on ";
			footer += getDateString(proposal.getJustification()
					.getScientificFileDate());
			try {
				PdfReader reader = new PdfReader(getScientificFilePdf());
				addPdf(pdfWriter, reader,
						document, footer);
				/*
				 * disable intentional blank page for now
				
				if (reader.getNumberOfPages() == 1){
					document.add(new Paragraph(
							"This page is intentionally left blank",
							HEADER_NORMAL_FONT));
					PdfContentByte cb = pdfWriter.getDirectContent();
					PdfPTable footerTable = getFooter("");
					footerTable.setTotalWidth(document.right() - document.left());
					footerTable.writeSelectedRows(0, -1, document.left(),
							FOOTER_POSITION, cb);
					document.newPage();
				}*
				 */
			} catch (IOException bpfe) {
				log.error("Error converting scientific file : " + bpfe.getMessage());
				log.error(AstronConverter.stackTraceToString(bpfe));
			}

		} else {
			if(OptionsDelegate.allowedToDisplay(Constants.ENABLE_FIRST_JUSTIFICATION ,fieldsDefinitionType)){	
				document.add(new Paragraph(
						"No Scientific Justification File uploaded",
						HEADER_REQUIRED_FONT));
				PdfContentByte cb = pdfWriter.getDirectContent();
				PdfPTable footerTable = getFooter("");
				footerTable.setTotalWidth(document.right() - document.left());
				footerTable.writeSelectedRows(0, -1, document.left(),
						FOOTER_POSITION, cb);
				document.newPage();
			}
		}

		/*
		 * if the technical file exist, try to merge it
		 */
		if (this.technicalDetailsFile != null) {
			String footer = proposal.getJustification()
					.getTechnicalDetailsFileName()
					+ " uploaded on ";
			footer += getDateString(proposal.getJustification()
					.getTechnicalDetailsFileDate());
			try {
				addPdf(pdfWriter, new PdfReader(getTechnicalDetailsFilePdf()),
						document, footer);
			} catch (IOException bpfe) {
				log.error("Error converting technical details file");
				log.error(AstronConverter.stackTraceToString(bpfe));
			}
		} 
/*
		else { 
			//TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
			//ContextType contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
			//FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();

			if(OptionsDelegate.allowedToDisplay(Constants.ENABLE_SECONOD_JUSTIFICATION ,fieldsDefinitionType)){	
				document.add(new Paragraph( "No Technical Justification File uploaded", HEADER_REQUIRED_FONT)); 
				PdfContentByte cb = pdfWriter.getDirectContent(); 
				PdfPTable footerTable = getFooter(""); 
				footerTable.setTotalWidth(document.right() - document.left()); 
				footerTable.writeSelectedRows(0, -1, document.left(), FOOTER_POSITION, cb); 
				  document.newPage(); 
			}
		}
 */

		/*
		 * the figures file, will be included for lofar.
		 */
		if(OptionsDelegate.allowedToDisplay(Constants.ENABLE_SINGLE_FILE ,fieldsDefinitionType)){	
			if (this.figureFile != null) {
				String footer = proposal.getJustification().getFigureFileName()
						+ " uploaded on ";
				footer += getDateString(proposal.getJustification().getFigureFileDate());
				try {
					addPdf(pdfWriter, new PdfReader(getFiguresFilePdf() ),
							document, footer);
				} catch (IOException bpfe) {
					log.error("Error converting technical details file");
					log.error(AstronConverter.stackTraceToString(bpfe));
				}
			} 
			else { 
				/* Figures file is optional..
				if(OptionsDelegate.allowedToDisplay(Constants.ENABLE_FIGURES_FILE ,fieldsDefinitionType)){	
					document.add(new Paragraph( "No Figures File uploaded", HEADER_REQUIRED_FONT)); 
					PdfContentByte cb = pdfWriter.getDirectContent(); 
					PdfPTable footerTable = getFooter(""); 
					footerTable.setTotalWidth(document.right() - document.left()); 
					footerTable.writeSelectedRows(0, -1, document.left(), FOOTER_POSITION, cb); 
					  document.newPage(); 
				}
				*/
			}
		}
		
		document.close();
		return byteArrayOutputStream.toByteArray();
	}

	/**
	 * The getScientificFilePdf method provides
	 * 
	 * @return
	 */
	protected byte[] getScientificFilePdf() {
		return this.scientificFile.getUploadFile();
	}

	/**
	 * The getTechnicalDetailsFilePdf method provides
	 * 
	 * @return
	 */
	protected byte[] getTechnicalDetailsFilePdf() {
		return this.technicalDetailsFile.getUploadFile();

	}
	
	protected byte[] getFiguresFilePdf() {
		return this.figureFile.getUploadFile();

	}

	/**
	 * The getFigureFilePdf method generates an pdf from an picture file
	 * 
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected void addFigureFilePdf(Proposal proposal, PdfWriter pdfWriter,
			Document document, String footer) throws DocumentException,
			IOException {

		String fileName = proposal.getJustification().getFigureFileName()
				.toLowerCase();
		/*
		 * if it ends with jpg or gif, it must be an picture
		 */
		if (fileName.endsWith(".jpg") || fileName.endsWith(".jpeg")
				|| fileName.endsWith(".png") || fileName.endsWith(".gif"))
		{
			PdfPTable footerTable = getFooter(footer);
			/*
			 * retrieve image
			 */
			Image figure = Image.getInstance(figureFile.getUploadFile());
			/*
			 * scale
			 */
			// figure.scalePercent(scalePercent);
			/*
			 * add figure to document and return pdf document
			 */
			document.add(figure);
			PdfContentByte cb = pdfWriter.getDirectContent();
			footerTable.setTotalWidth(document.right() - document.left());
			footerTable.writeSelectedRows(0, -1, document.left(),
					FOOTER_POSITION, cb);
			document.newPage();

		}
		/*
		 * if no picture, but pdf: return pdf
		 */
		else if (!fileName.endsWith(".tar") ){
			addPdf(pdfWriter, new PdfReader(figureFile.getUploadFile()),
					document, footer);

		}
	}

	protected float getScalingPercent(Document document, float objectWidth,
			float objectHeight) {
		float width = document.getPageSize().getWidth() - document.leftMargin()
				- document.rightMargin();
		/*
		 * get height - margins
		 */
		float height = document.getPageSize().getHeight() - document.topMargin()
				- document.bottomMargin();

		/*
		 * if figure is larger than allowed width, it must be scaled
		 */
		if (objectWidth > width) {
			/*
			 * if figure larger than allowed width and height
			 */
			if (objectHeight > height) {
				/*
				 * calculate the difference
				 */
				float diffWidth = (objectWidth - width) / objectWidth;
				float diffHeight = (objectHeight - height) / objectHeight;
				/*
				 * if difference width is larger than difference height scale
				 * with width
				 */
				if (diffWidth > diffHeight) {
					return width / objectWidth;

				}
				/*
				 * else scale with height
				 */
				else {
					return height / objectHeight;
				}
			}
			/*
			 * if only figure is larger than allowed width, scale with width
			 */
			else {
				return width / objectWidth;
			}

		}
		/*
		 * if only figure is larger than allowed height, scale with heigth
		 */
		else if (objectHeight > height) {
			return height / objectHeight;

		} else {
			return 1f;
		}
	}

	/**
	 * The getProposalPdf method generates pdf with proposal info
	 * 
	 * @param proposal
	 * @return
	 * @throws DocumentException
	 * @throws IOException
	 */
	protected byte[] getProposalPdf(Proposal proposal) throws DocumentException {
		/*
		 * create new document
		 */
		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN + FIRST_HEADER_SIZE, BOTTOM_MARGIN);

		/*
		 * output it to and byte array output stream
		 */
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		float firstHeaderSize = 0;
		firstHeaderSize = FIRST_HEADER_SIZE;
/*		if (proposal.getObservingRequest().isLongTermProject()
				|| proposal.getObservingRequest().isLargeProposal()) {
			firstHeaderSize = FIRST_HEADER_SIZE;
		} else {
			firstHeaderSize = FIRST_HEADER_SIZE_WITHOUT_LONGTERM;
		}*/

		/*
		 * create pdf writer
		 */
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				byteArrayOutputStream);
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(firstHeader,
				header, FOOTER_POSITION, firstHeaderSize, HEADER_SIZE, this);

		pdfWriter.setPageEvent(pageEvent);
		/*
		 * it must before open, then all pages has footer. if after
		 * document.open, first page is without footer
		 */

		document.open();

		// addFirstHeader(proposal, document);
		document.setMargins(LEFT_MARGIN, RIGHT_MARGIN, TOP_MARGIN,
				BOTTOM_MARGIN);
		/*
		 * create root table.
		 */
		PdfPTable firstPageTable = getDefaultTable(1);

		// addTitle(proposal, firstPageTable);
		addAbstract(proposal, firstPageTable);
		addRequestedTimeInfo(proposal.getObservingRequest(), firstPageTable);
		
		document.add(firstPageTable);
		// end of page 1
		
		document.newPage();
		PdfPTable secondPageTable = getDefaultTable(1);
		addApplicants(proposal, secondPageTable);
		addObservationsSummaries(proposal, secondPageTable);
		addObservingRequest(proposal.getObservingRequest(), secondPageTable);
		addAdditionalIssues(proposal, secondPageTable);
		//addContactAuthor(proposal, secondPageTable);
		// addAppendicesInfo(proposal, rootTable);
		document.add(secondPageTable);
		document.close();
		return byteArrayOutputStream.toByteArray();
	}

	protected byte[] getObservationsDetailsPdf(List observations)
			throws DocumentException {
		/*
		 * create new document
		 */

		Document document = new Document(PageSize.A4, LEFT_MARGIN,
				RIGHT_MARGIN, TOP_MARGIN, BOTTOM_MARGIN);
		document.setMarginMirroring(true);
		/*
		 * output it to and byte array output stream
		 */
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
		/*
		 * create pdf writer
		 */
		PdfWriter pdfWriter = PdfWriter.getInstance(document,
				byteArrayOutputStream);
		/*
		 * it must before open, then all pages has footer. if after
		 * document.open, first page is without footer
		 */
		PdfPageEvent pageEvent = new ProposalPdfPageEventHelper(null, header,
				FOOTER_POSITION, HEADER_SIZE, HEADER_SIZE, this);

		pdfWriter.setPageEvent(pageEvent);
		document.open();

		addObservationsDetails(observations, document);
		// addAppendicesInfo(proposal, rootTable);

		document.close();
		return byteArrayOutputStream.toByteArray();
	}

	protected String getCurrentDateString() {
		if (dateString == null) {
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
					"yyyy/MM/dd HH:mm z");
			Calendar UTCCal = new GregorianCalendar(TimeZone.getTimeZone("UTC"));
			simpleDateFormat.setCalendar(UTCCal);
			dateString = "Print view prepared on "
					+ simpleDateFormat.format(UTCCal.getTime());
		}
		return dateString;

	}

	protected String getDateString(Date date) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
				"yyyy/MM/dd HH:mm z");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));

		return simpleDateFormat.format(date);

	}

	public void fillHeaderText(Proposal proposal,PdfPTable headerTable){

		
 		PdfPCell cell = getBorderlessCell();
		Category category = proposal.getSemester().getCategory();
		if (category.getId().intValue() != Constants.REGULAR) {
			cell.addElement(this.getParagraph(category.getCode()
					.toUpperCase(), EMPTY, HEADER_NORMAL_FONT,
					HEADER_REQUIRED_FONT, 113, 0, Paragraph.ALIGN_LEFT));
		}
		headerTable.addCell(cell);
		cell = getBorderlessCell();
		headerTable.addCell(cell);
	}
	protected PdfPTable getFirstHeader(Proposal proposal) {
		int columns = 2;
		PdfPTable headerTable = getDefaultTable(columns);

		fillHeaderText(proposal,headerTable);
		PdfPCell cell = getBorderlessCell();

		/*
		 * add image
		 */
		cell = getBorderlessCell();
		cell.setColspan(columns);
		if (headerImage != null) {
			cell.addElement(headerImage);
		}
		headerTable.addCell(cell);
		/*
		 * add contact author name
		 */
		cell = getBorderlessCell();
		Member pi = getPi(proposal.getMembers());
		cell.addElement(getParagraph(Converter.getLastName(pi.getName()),
				EMPTY, HEADER_BIG_ITALIC_FONT, HEADER_REQUIRED_FONT, 0, 0,
				Paragraph.ALIGN_LEFT));
		headerTable.addCell(cell);

		/*
		 * add proposal code
		 */
		cell = getBorderlessCell();

		cell.addElement(this.getParagraph(proposal.getCode(), "No code",
				HEADER_BIG_FONT, HEADER_REQUIRED_FONT, 0, 0,
				Paragraph.ALIGN_RIGHT));
		headerTable.addCell(cell);

		/*
		 * add proposal title
		 */
		cell = getBorderlessCell();
		cell.setColspan(columns);

		// Chunk titleChunk = null;
		if (proposal.getJustification() != null) {
			String title = proposal.getJustification().getTitle();
			cell.addElement(this.getParagraph(title, EMPTY, HEADER_NORMAL_FONT,
					HEADER_REQUIRED_FONT, 0, 0, Paragraph.ALIGN_CENTER));
		} else {
			cell.addElement(this.getParagraph(null, EMPTY, HEADER_NORMAL_FONT,
					HEADER_REQUIRED_FONT, 0, 0, Paragraph.ALIGN_CENTER));
		}
		headerTable.addCell(cell);
		return headerTable;
	}

	public PdfPTable getFooter() throws DocumentException {
		return getFooter(getCurrentDateString());
	}

	protected PdfPTable getFooter(String footer) throws DocumentException {
		int columns = 3;
		PdfPTable footerTable = getDefaultTable(columns);
		footerTable.setWidths(new float[] { 0.1f, 0.8f, 0.1f });
		PdfPCell cell = getBorderlessCell();
		/*
		 * if even
		 */
		if (pageNumber % 2 == 0) {
			cell.addElement(this.getParagraph("" + pageNumber,
					EMPTY_NOT_REQUIRED, FOOTER, FOOTER_REQUIRED, 0, 0,
					Paragraph.ALIGN_LEFT));
		}
		footerTable.addCell(cell);
		cell = getBorderlessCell();
		cell.addElement(this.getParagraph(footer, EMPTY_NOT_REQUIRED, FOOTER,
				FOOTER_REQUIRED, 0, 0, Paragraph.ALIGN_CENTER));
		footerTable.addCell(cell);
		cell = getBorderlessCell();
		/*
		 * if odd
		 */
		if (pageNumber % 2 != 0) {
			cell.addElement(this.getParagraph("" + pageNumber,
					EMPTY_NOT_REQUIRED, FOOTER, FOOTER_REQUIRED, 0, 0,
					Paragraph.ALIGN_RIGHT));
		}
		footerTable.addCell(cell);
		pageNumber++;
		return footerTable;
	}
	protected PdfPTable getHeader(Proposal proposal, Document document)
			throws DocumentException {
		int columns = 3;
		PdfPTable headerTable = getDefaultTable(columns);
		float titleWidth = 0.5f;
		headerTable.setWidths(new float[] { 0.25f, titleWidth, 0.25f });
		
		Rectangle pageSize = document.getPageSize();
		float pageWidth = pageSize.getWidth();
		float margin = document.leftMargin();
		float w = pageWidth - margin;
		float width = document.getPageSize().getWidth() - document.leftMargin()
				- document.rightMargin();
		/*
		 * add PI name
		 */
		PdfPCell cell = getBorderlessCell();
		Member pi = getPi(proposal.getMembers());
		cell.addElement(getParagraph(Converter.getLastName(pi.getName()),
				EMPTY, HEADER_NORMAL_FONT, HEADER_REQUIRED_FONT, 0, 0,
				Paragraph.ALIGN_LEFT));
		headerTable.addCell(cell);

		/*
		 * add proposal title
		 */
		cell = getBorderlessCell();
		Chunk titleChunk = null;
		if (proposal.getJustification() != null) {
			String title = proposal.getJustification().getTitle();
			title = Converter.removeReturnsAndTabs(title);
			titleChunk = getChunk(title + "...", EMPTY, HEADER_SMALL_FONT,
					HEADER_SMALL_REQUIRED_FONT);
			boolean toLarge = false;
			while (titleChunk.getWidthPoint() > titleWidth * width - 20) {
				toLarge = true;
				title = title.substring(0, title.length() - 1);
				titleChunk = getChunk(title + "...", EMPTY, HEADER_SMALL_FONT,
						HEADER_SMALL_REQUIRED_FONT);
			}
			if (toLarge) {
				title += "...";
			}

			cell.addElement(this.getParagraph(title, EMPTY, HEADER_SMALL_FONT,
					HEADER_SMALL_REQUIRED_FONT, 0, 0, Paragraph.ALIGN_CENTER));
		} else {
			cell.addElement(this.getParagraph(null, EMPTY, HEADER_SMALL_FONT,
					HEADER_SMALL_REQUIRED_FONT, 0, 0, Paragraph.ALIGN_CENTER));
		}
		headerTable.addCell(cell);

		/*
		 * add proposal code
		 */
		cell = getBorderlessCell();

		cell.addElement(this.getParagraph(proposal.getCode(), "No code",
				HEADER_NORMAL_FONT, HEADER_REQUIRED_FONT, 0, 0,
				Paragraph.ALIGN_RIGHT));
		headerTable.addCell(cell);

		return headerTable;
	}

	/**
	 * The addPdf method adds an pdf to an other pdf
	 * 
	 * @param pdfCopy
	 * @param pdfReader
	 * @throws IOException
	 * @throws BadPdfFormatException
	 */
	protected void addPdf(PdfCopy pdfCopy, PdfReader pdfReader)
			throws IOException, BadPdfFormatException {

		PdfImportedPage page = null;
		/*
		 * add pages to pdf
		 */
		for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
			page = pdfCopy.getImportedPage(pdfReader, i);
			pdfCopy.addPage(page);

		}
	}

	protected void addPdf(PdfWriter writer, PdfReader pdfReader,
			Document document, String footer) throws BadPdfFormatException,
			DocumentException {
		/*
		 * get height - margins
		 */

		PdfImportedPage page = null;
		/*
		 * add pages to pdf
		 */
		for (int i = 1; i <= pdfReader.getNumberOfPages(); i++) {
			PdfPTable footerTable = getFooter(footer);

			PdfContentByte cb = writer.getDirectContent();

			page = writer.getImportedPage(pdfReader, i);
			cb.addTemplate(page, 0, 0);
			footerTable.setTotalWidth(document.right() - document.left());
			footerTable.writeSelectedRows(0, -1, document.left(),
					FOOTER_POSITION, cb);
			document.newPage();

		}

	}

	/**
	 * The addTitle method add justification title to root table
	 * 
	 * @param proposal
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addTitle(Proposal proposal, PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		cell
				.addElement(getParagraphDefaultTitle("Title of proposed programme"));
		Paragraph title = null;
		if (proposal.getJustification() != null) {
			String titleText = Converter.removeReturnsAndTabs(proposal
					.getJustification().getTitle());
			title = getParagraph(titleText, EMPTY, PROPOSAL_TITLE_FONT,
					REQUIRED_FONT, NORMAL_PARAGRAPH_INDENTATION_LEFT,
					NORMAL_PARAGRAPH_INDENTATION_RIGHT);

		} else {
			title = getParagraphNormal(null, EMPTY);
		}
		cell.addElement(title);
		rootTable.addCell(cell);
	}

	/**
	 * The addAbstract method add justification abstract to root table
	 * 
	 * @param proposal
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addAbstract(Proposal proposal, PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		cell.addElement(getParagraphDefaultTitle("Abstract"));
		String abstractText = null;
		if (proposal.getJustification() != null) {
			abstractText = proposal.getJustification().getAbstractText();

		}
		cell.addElement(getParagraphNormal(abstractText, EMPTY));
		rootTable.addCell(cell);
	}

	/**
	 * The addApplicants method adds applicants to pdf
	 * 
	 * @param proposal
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addApplicants(Proposal proposal, PdfPTable rootTable)
			throws DocumentException {
		int columns = 6;

		PdfPCell applicantsCell = getBorderlessCell();
		applicantsCell.addElement(getParagraphDefaultTitle("Applicants"));
		
		PdfPTable applicantsTable = getTableNoSplit(columns);
		applicantsTable.setWidths(new int[] { 30, 20, 25, 15, 3, 8 });
		/*
		 * add table header
		 */
		applicantsTable.addCell(getTableHeaderCell("Name"));
		applicantsTable.addCell(getTableHeaderCell("Affiliation"));
		applicantsTable.addCell(getTableHeaderCell("Email"));
		applicantsTable.addCell(getTableHeaderCell("Country"));
		applicantsTable.addCell(getTableHeaderCell(""));
		applicantsTable.addCell(getTableHeaderCell("Potential observer"));
		/*
		 * add applicants to table
		 */
		for (int i = 0; i < proposal.getMembers().size(); i++) {
			Member member = (Member) proposal.getMembers().get(i);
			applicantsTable.addCell(getTableRequiredTextCell(member.getName()
					.trim()));
			applicantsTable.addCell(getTableRequiredTextCell(member
					.getAffiliation()));
			applicantsTable
					.addCell(getTableRequiredTextCell(member.getEmail()));
			applicantsTable.addCell(getTableRequiredTextCell(member
					.getCountry()));
			if (member.isPi()) {
				applicantsTable.addCell(getTableTextCell("Pi"));
			} else {
				applicantsTable.addCell(getTableTextCell(null));
			}

			if (member.isPotentialObserver()) {
				applicantsTable.addCell(getTableTextCell("Yes"));
			} else {
				applicantsTable.addCell(getTableTextCell(null));
			}
		}

		applicantsCell.addElement(applicantsTable);
		addContactAuthor(proposal.getMembers(),applicantsCell);
		rootTable.addCell(applicantsCell);

	}

	/**
	 * The addApplicants method adds applicants to pdf
	 * 
	 * @param proposal
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addContactAuthor(Proposal proposal, PdfPTable rootTable)
			throws DocumentException {

		PdfPCell applicantsCell = getBorderlessCell();
		applicantsCell.addElement(getParagraphDefaultTitle("Contact Author"));
		PdfPTable contactAuthorTable = getTableNoSplit(2);
		PdfPTable userTable = getTableNoSplit(2);
		PdfPTable affiliationTable = getTableNoSplit(2);

		/*
		 * add personal information
		 */
		RegisteredMember registeredMember = getContactAuthor(proposal
				.getMembers());
		userTable.addCell(getTableTitleTextCell("Title", false));
		userTable.addCell(getTableTextCell(registeredMember.getUser()
				.getTitle(), false));
		userTable.addCell(getTableTitleTextCell("Name", false));
		userTable.addCell(getTableTextCell(
				registeredMember.getUser().getName(), false));
		userTable.addCell(getTableTitleTextCell("Email", false));
		userTable.addCell(getTableTextCell(registeredMember.getUser()
				.getEmail(), false));
		userTable.addCell(getTableTitleTextCell("Phone(first)", false));
		userTable.addCell(getTableTextCell(registeredMember.getUser()
				.getPhone1(), false));
		userTable.addCell(getTableTitleTextCell("Phone(second)", false));
		userTable.addCell(getTableTextCell(registeredMember.getUser()
				.getPhone2(), false));
		userTable.addCell(getTableTitleTextCell("Fax", false));
		userTable.addCell(getTableTextCell(registeredMember.getUser().getFax(),
				false));
		/*
		 * add affiliation info
		 */
		Iterator userAffiliations = registeredMember.getUser()
				.getUserAffiliations().iterator();
		boolean foundAffiliation = false;
		while (userAffiliations.hasNext() && !foundAffiliation) {
			UserAffiliation userAffiliation = (UserAffiliation) userAffiliations
					.next();
			if (userAffiliation.isPreferred()) {
				foundAffiliation = true;
				affiliationTable.addCell(getTableTitleTextCell("Institute",
						false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getName(), false));
				affiliationTable.addCell(getTableTitleTextCell("Department",
						false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getDepartment(), false));
				affiliationTable
						.addCell(getTableTitleTextCell("Address", false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getAddress(), false));
				affiliationTable
						.addCell(getTableTitleTextCell("Zipcode", false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getZipcode(), false));
				affiliationTable.addCell(getTableTitleTextCell("City", false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getCity(), false));
				affiliationTable.addCell(getTableTitleTextCell("State", false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getState(), false));
				affiliationTable
						.addCell(getTableTitleTextCell("Country", false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getCountry(), false));
				if (!AstronValidator.isBlankOrNull(userAffiliation.getRemarks())) {
					affiliationTable.addCell(getTableTitleTextCell("Remarks",
							false));
					affiliationTable.addCell(getTableTextCell(userAffiliation
							.getRemarks(), false));
				}
			}
		}
		/*
		 * add corresponding author table with
		 */
		userTable.setWidths(new int[] { 2, 5 });
		affiliationTable.setWidths(new int[] { 2, 5 });
		PdfPCell userCell = getBorderlessCell();
		userCell.addElement(userTable);
		contactAuthorTable.addCell(userCell);
		PdfPCell affiliationCell = getBorderlessCell();
		affiliationCell.addElement(affiliationTable);
		contactAuthorTable.addCell(affiliationCell);
		// correspondingAuthorTable.setWidths(new int[] { 1, 3 });
		applicantsCell.addElement(contactAuthorTable);

		rootTable.addCell(applicantsCell);

	}

	/**
	 * The getCorrespondingAuthor get corresponding author
	 * 
	 * @param members
	 * @return
	 */
	protected RegisteredMember getContactAuthor(List members) {
		/*
		 * if corresponding author already retreived, return it
		 */
		if (correspondingAuthor != null) {
			return correspondingAuthor;
		}
		/*
		 * search for corresponding author
		 */
		for (int i = 0; i < members.size(); i++) {
			Member member = (Member) members.get(i);
			if (member.isContactAuthor()) {
				/*
				 * set global corresponding author
				 */
				correspondingAuthor = (RegisteredMember) member;
				return correspondingAuthor;
			}
		}
		return null;
	}

	protected Member getPi(List members) {
		/*
		 * if corresponding author already retreived, return it
		 */
		if (pi != null) {
			return pi;
		}
		/*
		 * search for corresponding author
		 */
		for (int i = 0; i < members.size(); i++) {
			Member member = (Member) members.get(i);
			if (member.isPi()) {
				/*
				 * set global corresponding author
				 */
				pi = member;
				return pi;
			}
		}
		return null;
	}

	/**
	 * The addCorrespondingAuthor method adds contact author
	 * 
	 * @param members
	 * @param applicantsCell
	 * @throws DocumentException
	 */
	protected void addContactAuthor(List members, PdfPCell applicantsCell)
			throws DocumentException {
		applicantsCell
				.addElement(getParagraphDefaultSubTitle("Contact Author"));
		PdfPTable correspondingAuthorTable = getTableNoSplit(2);
		PdfPTable userTable = getTableNoSplit(2);
		PdfPTable affiliationTable = getTableNoSplit(2);

		/*
		 * add personal information
		 */
		RegisteredMember registeredMember = getContactAuthor(members);
		userTable.addCell(getTableTitleTextCell("Title", false));
		userTable.addCell(getTableTextCell(registeredMember.getUser()
				.getTitle(), false));
		userTable.addCell(getTableTitleTextCell("Name", false));
		userTable.addCell(getTableTextCell(
				registeredMember.getUser().getName(), false));
		userTable.addCell(getTableTitleTextCell("Email", false));
		userTable.addCell(getTableTextCell(registeredMember.getUser()
				.getEmail(), false));
		userTable.addCell(getTableTitleTextCell("Phone(first)", false));
		userTable.addCell(getTableTextCell(registeredMember.getUser()
				.getPhone1(), false));
		userTable.addCell(getTableTitleTextCell("Phone(second)", false));
		userTable.addCell(getTableTextCell(registeredMember.getUser()
				.getPhone2(), false));
		userTable.addCell(getTableTitleTextCell("Fax", false));
		userTable.addCell(getTableTextCell(registeredMember.getUser().getFax(),
				false));
		/*
		 * add affiliation info
		 */
		Iterator userAffiliations = registeredMember.getUser()
				.getUserAffiliations().iterator();
		boolean foundAffiliation = false;
		while (userAffiliations.hasNext() && !foundAffiliation) {
			UserAffiliation userAffiliation = (UserAffiliation) userAffiliations
					.next();
			if (userAffiliation.isPreferred()) {
				foundAffiliation = true;
				affiliationTable.addCell(getTableTitleTextCell("Institute",
						false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getName(), false));
				affiliationTable.addCell(getTableTitleTextCell("Department",
						false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getDepartment(), false));
				affiliationTable
						.addCell(getTableTitleTextCell("Address", false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getAddress(), false));
				affiliationTable
						.addCell(getTableTitleTextCell("Zipcode", false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getZipcode(), false));
				affiliationTable.addCell(getTableTitleTextCell("City", false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getCity(), false));
				affiliationTable.addCell(getTableTitleTextCell("State", false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getState(), false));
				affiliationTable
						.addCell(getTableTitleTextCell("Country", false));
				affiliationTable.addCell(getTableTextCell(userAffiliation
						.getAffiliation().getCountry(), false));
				if (!AstronValidator.isBlankOrNull(userAffiliation.getRemarks())) {
					affiliationTable.addCell(getTableTitleTextCell("Remarks",
							false));
					affiliationTable.addCell(getTableTextCell(userAffiliation
							.getRemarks(), false));
				}
			}
		}
		/*
		 * add corresponding author table with
		 */
		userTable.setWidths(new int[] { 2, 5 });
		affiliationTable.setWidths(new int[] { 2, 5 });
		PdfPCell userCell = getBorderlessCell();
		userCell.addElement(userTable);
		correspondingAuthorTable.addCell(userCell);
		PdfPCell affiliationCell = getBorderlessCell();
		affiliationCell.addElement(affiliationTable);
		correspondingAuthorTable.addCell(affiliationCell);
		// correspondingAuthorTable.setWidths(new int[] { 1, 3 });
		applicantsCell.addElement(correspondingAuthorTable);
	}

	/**
	 * The addLargeProposal adds a large proposal
	 * 
	 * @param proposal
	 * @param rootTable
	 */
	/*
	 * protected void addLargeProposal(ObservingRequest observingRequest,
	 * PdfPTable rootTable) {
	 * 
	 * PdfPCell cell = getBorderlessCell(); if
	 * (observingRequest.isLargeProposal()) {
	 * cell.addElement(getParagraphDefaultTitle("Large proposal: Yes")); } else {
	 * cell.addElement(getParagraphDefaultTitle("Large proposal: No")); }
	 * rootTable.addCell(cell);
	 *  }
	 */

	/**
	 * The addLongTermProject adds a longterm project
	 * 
	 * @param observingRequest
	 * @param rootTable
	 */
	/*
	 * protected void addLongTermProject(ObservingRequest observingRequest,
	 * PdfPTable rootTable) {
	 * 
	 * PdfPCell cell = getBorderlessCell(); if
	 * (observingRequest.isLongTermProject()) {
	 * cell.addElement(getParagraphDefaultTitle("Long term project: Yes"));
	 *  } else { cell.addElement(getParagraphDefaultTitle("Long term project:
	 * No")); } rootTable.addCell(cell);
	 *  }
	 */

	/**
	 * The addOverallSchedulingRequirements adds Overall scheduling requirements
	 * 
	 * @param observingRequest
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addOverallSchedulingRequirements(
			ObservingRequest observingRequest, PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		if (!AstronValidator.isBlankOrNull(observingRequest
				.getOverallRequiredSchedConstraints())) {

			cell
					.addElement(getParagraphDefaultTitle("Overall scheduling requirements"));
			cell.addElement(getParagraphNormal(observingRequest
					.getOverallRequiredSchedConstraints(), EMPTY_NOT_REQUIRED));

		} else {
			cell
					.addElement(getParagraphDefaultTitle("No overall scheduling requirements"));
		}
		rootTable.addCell(cell);

	}

	/**
	 * The addOverallSchedulingPreferences adds Overall scheduling preferences
	 * 
	 * @param observingRequest
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addOverallSchedulingPreferences(
			ObservingRequest observingRequest, PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		if (!AstronValidator.isBlankOrNull(observingRequest
				.getOverallPreferredSchedConstraints())) {

			cell
					.addElement(getParagraphDefaultTitle("Overall scheduling preferences"));
			cell
					.addElement(getParagraphNormal(observingRequest
							.getOverallPreferredSchedConstraints(),
							EMPTY_NOT_REQUIRED));

		} else {
			cell
					.addElement(getParagraphDefaultTitle("No overall scheduling preferences"));
		}
		rootTable.addCell(cell);

	}

	/**
	 * The addAdditionalIssues adds the additional issues
	 * 
	 * @param proposal
	 * @param rootTable
	 * @throws DocumentException
	 */
	private void addAdditionalIssues(Proposal proposal, PdfPTable rootTable)
                    throws DocumentException {
		if (proposal.getAdditionalIssues() == null) {
			/*
			 * make default additionalIssues
			 */
			proposal.setAdditionalIssues(new AdditionalIssues());
		}
		addTheses(proposal.getAdditionalIssues(), rootTable);
		addLinkedProposals(proposal.getAdditionalIssues(), rootTable);
		addLinkedProposalsElsewhere(proposal.getAdditionalIssues(), rootTable);
		addRelatedPublications(proposal.getAdditionalIssues(), rootTable);
		addPreviousAllocations(proposal.getAdditionalIssues(), rootTable);
		addRelatedPreviousInvolvedProposal(proposal.getAdditionalIssues(), rootTable);
		addAdditionalRemarks(proposal.getAdditionalIssues(), rootTable);
	}

	/**
	 * The addLinkedProposals adds the linked proposals
	 * 
	 * @param additionalIssues
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addLinkedProposals(AdditionalIssues additionalIssues,
			PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		/*
		 * if linked proposals
		 */
		if (additionalIssues.isLinkedProposals()) {
			cell.addElement(getParagraphDefaultTitle("Linked proposal submitted to this TAC: Yes"));
			if (!AstronValidator.isBlankOrNull(additionalIssues
					.getLinkedProposalsSpecifics())) {
				cell.addElement(getParagraphNormal(additionalIssues
						.getLinkedProposalsSpecifics(), EMPTY_NOT_REQUIRED));

			}
		} else {
			cell.addElement(getParagraphDefaultTitle("Linked proposal submitted to this TAC: No"));
		}
		rootTable.addCell(cell);
	}

	/**
	 * The addLinkedProposalsElsewhere adds the linked proposals from other TAC's
	 * 
	 * @param additionalIssues
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addLinkedProposalsElsewhere(AdditionalIssues additionalIssues,
			PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		/*
		 * if linked proposals
		 */
		if (additionalIssues.isLinkedProposalsElsewhere()) {
			cell.addElement(getParagraphDefaultTitle(
					"Linked proposal submitted to other TACs: Yes"));
			if (!AstronValidator.isBlankOrNull(additionalIssues
					.getLinkedProposalsElsewhereSpecifics())) {
				cell.addElement(getParagraphNormal(additionalIssues
						.getLinkedProposalsElsewhereSpecifics(), EMPTY_NOT_REQUIRED));

			}
		} else {
			cell.addElement(getParagraphDefaultTitle("Linked proposal submitted to other TACs: No"));
		}
		rootTable.addCell(cell);
	}
	
	/**
	 * TheaddPreviousAllocations adds the relevant other allocations
	 * 
	 * @param additionalIssues
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addPreviousAllocations(AdditionalIssues additionalIssues,
			PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		/*
		 * if linked proposals
		 */
		if (additionalIssues.isPreviousAllocations()) {
			cell.addElement(getParagraphDefaultTitle("Relevant previous Allocations: Yes"));
			if (!AstronValidator.isBlankOrNull(additionalIssues
					.getPreviousAllocationsSpecifics())) {
				cell.addElement(getParagraphNormal(additionalIssues
						.getPreviousAllocationsSpecifics(), EMPTY_NOT_REQUIRED));

			}
		} else {
			cell.addElement(getParagraphDefaultTitle("Relevant previous Allocations: No"));
		}
		rootTable.addCell(cell);
	}

	/**
	 * The addTheses add theses table to the pdf document
	 * 
	 * @param additionalIssues
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addTheses(AdditionalIssues additionalIssues,
			PdfPTable rootTable)throws DocumentException {
        int columns = 7;

        PdfPCell thesesCell = getBorderlessCell();

        if (additionalIssues.getTheses().size() > 0) {
            thesesCell.addElement(getParagraphDefaultTitle("Students involved"));

            PdfPTable thesesTable = getTableNoSplit(columns);
            thesesTable.setWidths(new int[] { 23, 14, 10, 23, 10, 15, 8 });
            /*
             * add table header
             */
            thesesTable.addCell(getTableHeaderCell("Student"));
            thesesTable.addCell(getTableHeaderCell("Level"));
            thesesTable.addCell(getTableHeaderCell("Applicant"));
            thesesTable.addCell(getTableHeaderCell("Supervisor"));
            thesesTable.addCell(getTableHeaderCell("Applicant"));
            thesesTable.addCell(getTableHeaderCell("Expected completion date"));
            thesesTable.addCell(getTableHeaderCell("Data required"));
            /*
             * add theses to table
             */
            for (int i = 0; i < additionalIssues.getTheses().size(); i++) {
                Thesis thesis = (Thesis) additionalIssues.getTheses().get(i);
                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy/MM");

                thesesTable.addCell(getTableRequiredTextCell(thesis.getStudentName()
                        .trim()));
                if (thesis.getStudentLevel()!= null) {
                	// null check to be compatible with old proposals.
                    thesesTable.addCell(getTableRequiredTextCell(thesis.getStudentLevel().trim()));
                } else {
                    thesesTable.addCell(getTableRequiredTextCell(null));
                }
                if (thesis.getStudent() != null) {
                    thesesTable.addCell(getTableTextCell("Yes", true));
                } else {
                    thesesTable.addCell(getTableTextCell("No", true));
                }
                thesesTable.addCell(getTableRequiredTextCell(thesis.getSupervisorName()
                        .trim()));
                if (thesis.getSupervisor() != null) {
                    thesesTable.addCell(getTableTextCell("Yes", true));
                } else {
                    thesesTable.addCell(getTableTextCell("No", true));
                }
                if (thesis.getExpectedCompletionDate() != null) {
                    thesesTable.addCell(getTableRequiredNumbersCell(
                            simpleDateFormat.format(thesis
                                    .getExpectedCompletionDate()), true));
                } else {
                    thesesTable.addCell(getTableRequiredNumbersCell(null, true));
                }
                if (thesis.isDataRequired()) {
                    thesesTable.addCell(getTableTextCell("Yes", true));
                } else {
                    thesesTable.addCell(getTableTextCell("No", true));
                }
            }

            thesesCell.addElement(thesesTable);
            
        } else {
            
            thesesCell.addElement(getParagraphDefaultTitle("No PhD Students involved"));
            
        }

        rootTable.addCell(thesesCell);
	}

	/**
	 * The addRelatedPublications add related publications
	 * 
	 * @param additionalIssues
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addRelatedPublications(AdditionalIssues additionalIssues,
			PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();

		/* 
		  */
		if(additionalIssues.getRelatedPublications() != null ){
			cell.addElement(getParagraphDefaultTitle("Related Publications"));

			cell.addElement(getParagraphNormal(additionalIssues.getRelatedPublications(), EMPTY_NOT_REQUIRED));
		
		}
		
		rootTable.addCell(cell);

	}

	/**
	 * The addAdditionalRemarks add additional remarks
	 * 
	 * @param additionalIssues
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addAdditionalRemarks(AdditionalIssues additionalIssues,
			PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		if (!AstronValidator.isBlankOrNull(additionalIssues
				.getAdditionalRemarks())) {
			
			cell.addElement(getParagraphDefaultTitle("Additional remarks"));

			cell.addElement(getParagraphNormal(additionalIssues
					.getAdditionalRemarks(), EMPTY_NOT_REQUIRED));

		} else {
			cell.addElement(getParagraphDefaultTitle("No additional remarks"));
		}
		rootTable.addCell(cell);

	}
	
	/**
	 * The addRelatedPreviousInvolvedProposal add related previous involved proposal
	 * 
	 * @param additionalIssues
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addRelatedPreviousInvolvedProposal(AdditionalIssues additionalIssues,
			PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		if (!AstronValidator.isBlankOrNull(additionalIssues
				.getRelatedPreviousInvolvedProposal())) {
			
			cell.addElement(getParagraphDefaultTitle("Previous involved proposals"));
			
			cell.addElement(getParagraphNormal(additionalIssues
					.getRelatedPreviousInvolvedProposal(), EMPTY_NOT_REQUIRED));
		} else {
			cell.addElement(getParagraphDefaultTitle("No previous involved proposals"));
		}
		rootTable.addCell(cell);
	}

	/**
	 * The addObservingRequest add
	 * 
	 * @param observingRequest
	 * @param rootTable
	 * @throws DocumentException
	 */
	protected void addObservingRequest(ObservingRequest observingRequest,
			PdfPTable rootTable) throws DocumentException {
		addOverallSchedulingRequirements(observingRequest, rootTable);
		addOverallSchedulingPreferences(observingRequest, rootTable);
	}

	protected void addRequestedTimeInfo(ObservingRequest observingRequest,
			PdfPTable table) throws DocumentException {
		addTotalRequestedTime(observingRequest, table);

	}

	protected void addObservationsSummaries(Proposal proposal,
			PdfPTable rootTable) {
		PdfPCell observationsListCell = getBorderlessCell();
		observationsListCell
				.addElement(getParagraphDefaultTitle("Summary of observations"));

		/*
		 * if observations exists show them
		 */
		if (proposal.getObservingRequest().getObservations().size() > 10) {

			observationsListCell
					.addElement(getParagraphNormal(
							"More than 10 observations. Go to observation details for more information.",
							EMPTY));
		} else if (proposal.getObservingRequest().getObservations().size() > 0) {

			addObservationsList(proposal.getObservingRequest()
					.getObservations(), observationsListCell);
		} else {
			observationsListCell.addElement(getParagraphNormal(
					"No details given", EMPTY));
		}

		rootTable.addCell(observationsListCell);
	}

	protected void addObservationsList(List observations,
			PdfPCell observationsListCell) {

	}

	protected void addObservationsDetails(List observations, Document document)
			throws DocumentException {
		document.add(getParagraphDefaultTitle("Observation details"));
		for (int i = 0; i < observations.size(); i++) {
			Observation observation = (Observation) observations.get(i);
			addObservationDetails(observation, document);
			document.add(getParagraphDefaultTitle(" "));

		}

	}

	protected void addObservationDetails(Observation observation,
			Document document) throws DocumentException {
		throw new DocumentException();

	}

	protected void addTotalRequestedTime(ObservingRequest observingRequest,
			PdfPTable rootTable) {
	}

	/**
	 * The addAppendicesInfo method add info about merged pdfs
	 * 
	 * @param proposal
	 * @param rootTable
	 */
	protected void addAppendicesInfo(Proposal proposal, PdfPTable rootTable) {
		PdfPCell cell = getBorderlessCell();
		cell
				.addElement(getParagraphDefaultTitle("Scientific Justification File"));
		if (this.scientificFile != null) {
			cell.addElement(getParagraphNormal(proposal.getJustification()
					.getScientificFileName(), EMPTY));
		} else {
			cell.addElement(getParagraphNormal(null, EMPTY));
		}
		rootTable.addCell(cell);
		cell = getBorderlessCell();
		cell
				.addElement(getParagraphDefaultTitle("Technical Justification File"));
		if (technicalDetailsFile != null) {
			cell.addElement(getParagraphNormal(proposal.getJustification()
					.getTechnicalDetailsFileName(), EMPTY));
		} else {
			cell.addElement(getParagraphNormal(null, EMPTY));
		}
		rootTable.addCell(cell);

		if (figureFile != null) {
			cell = getBorderlessCell();
			cell.addElement(getParagraphDefaultTitle("Figure(s) File"));
			cell.addElement(getParagraphNormal(proposal.getJustification()
					.getFigureFileName(), EMPTY));
			rootTable.addCell(cell);
		}

	}

	/**
	 * @return Returns the figureFile.
	 */
	public FigureFile getFigureFile() {
		return figureFile;
	}

	/**
	 * @param figureFile
	 *            The figureFile to set.
	 */
	public void setFigureFile(FigureFile figureFile) {
		this.figureFile = figureFile;
	}

	/**
	 * @return Returns the scientificFile.
	 */
	public ScientificFile getScientificFile() {
		return scientificFile;
	}

	/**
	 * @param scientificFile
	 *            The scientificFile to set.
	 */
	public void setScientificFile(ScientificFile scientificFile) {
		this.scientificFile = scientificFile;
	}

	/**
	 * @return Returns the technicalDetailsFile.
	 */
	public TechnicalDetailsFile getTechnicalDetailsFile() {
		return technicalDetailsFile;
	}

	/**
	 * @param technicalDetailsFile
	 *            The technicalDetailsFile to set.
	 */
	public void setTechnicalDetailsFile(
			TechnicalDetailsFile technicalDetailsFile) {
		this.technicalDetailsFile = technicalDetailsFile;
	}

}