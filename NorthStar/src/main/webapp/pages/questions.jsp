<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />

<html:form action="/processSendQuestionsProblemsAction" focus="subject" method="POST">

<table align="center" width="100%">
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>	
	<tr>
		<td class="right">Subject&nbsp;:&nbsp;</td>
		<td class="left"><html:text property="subject" size="62" maxlength="255" /><span class="error"><astron:errors property="subject" ifnotexist="*"/></span></td>
	</tr>
	<tr>
		<td class="right" valign="top">Message&nbsp;:&nbsp;</td>
		<td class="left"><html:textarea property="message" cols="60" rows="4"/><span class="error"><astron:errors property="message" ifnotexist="*"/></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	</table>
	<table  align="center" width="100%">
	<tr>
		<td class="right"><html:submit property="SendButton" styleClass="list_send" title="Send">Send</html:submit></td>
		<td class="left"><html:cancel >Cancel</html:cancel></td>
	</tr>


	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
</table>

</html:form>
