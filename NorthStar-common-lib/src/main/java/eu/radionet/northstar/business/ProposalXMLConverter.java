// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronValidator;
import nl.astron.util.XMLBuilder;

import org.w3c.dom.Element;

import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.data.WebDAVConfiguration;
import eu.radionet.northstar.data.entities.AdditionalIssues;
import eu.radionet.northstar.data.entities.Justification;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalStatus;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.ResourceType;
import eu.radionet.northstar.data.entities.Semester;
import eu.radionet.northstar.data.entities.Target;
import eu.radionet.northstar.data.entities.Thesis;

public abstract class ProposalXMLConverter {
	public static final String NORTHSTAR_NAMESPACE = "http://www.radionet-eu.org/NorthStar";

	public static final String PERSISTENT_NAMESPACE = "http://www.astron.nl/PersistentObjects";

	public static final String USER_ADMIN_NAMESPACE = "http://www.astron.nl/useradmin";

	// protected String currentNameSpace = NORTHSTAR_NAMESPACE;
	protected XMLBuilder xmlBuilder = null;
	protected ContextType contextConfiguration = null;
	public void buildXml(XMLBuilder xmlBuilder, Proposal proposal)
			throws DatabaseException {
		this.xmlBuilder = xmlBuilder;
		TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration.getTelescopeConfiguration(proposal);
		contextConfiguration = telescopeConfiguration.getContextConfiguration(proposal);
		
		Element root = xmlBuilder.getRootElement();
		addXmlProposalElement(root, proposal);
	}

	/*
	 * y
	 */
	protected String getCurrentNamespace() {
		return NORTHSTAR_NAMESPACE;
	}

	protected Element addXmlProposalElement(Element root, Proposal proposal)
			throws DatabaseException {
		Element proposalElement = xmlBuilder.addElement(root,
				getCurrentNamespace(), "proposal");
		return addXmlProposalContents(proposalElement, proposal);
	}

	private Element addXmlProposalContents(Element proposalElement,
			Proposal proposal) throws DatabaseException {

		xmlBuilder.addTextElement(proposalElement, "code", proposal.getCode());
		Element membersElement = xmlBuilder.addElement(proposalElement,
				"members");
		for (int i = 0; i < proposal.getMembers().size(); i++) {

			Member member = (Member) proposal.getMembers().get(i);
			addXmlElement(membersElement, member, i);

		}
		if (proposal.getJustification() != null) {
			addXmlElement(proposalElement, proposal.getJustification());
		}
		if (proposal.getObservingRequest() != null) {
			addXmlObservingRequestElement(proposalElement, proposal
					.getObservingRequest());
		}
		if (proposal.getAdditionalIssues() != null) {
			addXmlElement(proposalElement, proposal.getAdditionalIssues());
		}
		if (proposal.getSemester() != null) {
			addXmlElement(proposalElement, proposal.getSemester());
		}
		addXmlStatussesElement(proposalElement, proposal.getStatusHistory(),
				proposal.getCurrentStatus());
		if (NorthStarDelegate.isStatusUnderReviewOrHigher(proposal)) {
			addXmlDocumentElement(proposalElement, proposal);
		}
		return proposalElement;
	}

	private Element addXmlDocumentElement(Element root, Proposal proposal) {
		Element documentElement = xmlBuilder.addElement(root,
				"document");
		xmlBuilder.addTextElement(documentElement, "name", proposal.getCode());
		String pdfLocation = WebDAVConfiguration.getRedirectUrl() + WebDAVConfiguration.getPdfLocation(proposal);
		xmlBuilder.addTextElement(documentElement, "Uri", pdfLocation);

		return documentElement;

	}

	protected Element addXmlElement(Element root,
			AdditionalIssues additionalIssues) {
		Element additionalIssuesElement = xmlBuilder.addElement(root,
				"additionalIssues");
		if (additionalIssues.isLinkedProposals()) {
			xmlBuilder.addTextElement(additionalIssuesElement,
					"linkedProposals", additionalIssues
							.getLinkedProposalsSpecifics());
		}		
		if (additionalIssues.isLinkedProposalsElsewhere()) {
			xmlBuilder.addTextElement(additionalIssuesElement,
					"linkedProposalsElsewhere", additionalIssues
							.getLinkedProposalsElsewhereSpecifics());
		}		
		if (additionalIssues.isPreviousAllocations()) {
			xmlBuilder.addTextElement(additionalIssuesElement,
					"previousAllocations", additionalIssues
							.getPreviousAllocationsSpecifics());
		}
		if (additionalIssues.getTheses().size() > 0) {
			Element thesesElement = xmlBuilder.addElement(
					additionalIssuesElement, "theses");
			for (int i = 0; i < additionalIssues.getTheses().size(); i++) {

				Thesis thesis = (Thesis) additionalIssues.getTheses().get(i);
				addXmlElement(thesesElement, thesis, i);

			}
		}
		xmlBuilder.addTextElement(additionalIssuesElement, "additionalRemarks",
				additionalIssues.getAdditionalRemarks());
		return additionalIssuesElement;

	}

	protected void addXmlElement(Element root, Thesis thesis, int index) {

		Element thesisElement = xmlBuilder.addIndexedElement(root, "thesis",
				index);
		Element studentElement = xmlBuilder
				.addElement(thesisElement, "student");
		Member student = thesis.getStudent();
		if (student != null) {
			Element applicantElement = xmlBuilder.addElementWithAttribute(
					studentElement, "applicant", "id", student.getId()
							.toString());
			xmlBuilder.addTextElement(applicantElement, "name", student
					.getName());
		} else {
			xmlBuilder.addTextElement(studentElement, "name", thesis
					.getStudentName());
		}
		Element supervisorElement = xmlBuilder.addElement(thesisElement,
				"supervisor");
		Member supervisor = thesis.getSupervisor();
		if (supervisor != null) {
			Element applicantElement = xmlBuilder.addElementWithAttribute(
					supervisorElement, "applicant", "id", supervisor.getId()
							.toString());
			xmlBuilder.addTextElement(applicantElement, "name", supervisor
					.getName());
		} else {
			xmlBuilder.addTextElement(supervisorElement, "name", thesis
					.getSupervisorName());
		}
		xmlBuilder.addTextDateElement(thesisElement, "expectedCompletionDate",
				thesis.getExpectedCompletionDate());
		xmlBuilder.addTextElement(thesisElement, "dataRequired", thesis
				.isDataRequired());
	}

	private Element addXmlElement(Element root, Justification justification) {
		Element justificationElement = xmlBuilder.addElement(root,
				"justification");

		xmlBuilder.addTextElement(justificationElement, "title", justification
				.getTitle());
		String abstractText = justification.getAbstractText();
		xmlBuilder.addTextElement(justificationElement, "abstract", abstractText);

		if (!AstronValidator.isBlankOrNull(justification.getScientificFileName())) {
			Element scientific = xmlBuilder.addElement(justificationElement,
					"scientificFileMetaData");
			xmlBuilder.addTextElement(scientific, "name", justification
					.getScientificFileName());
			xmlBuilder.addTextDateTimeElement(scientific, "date", justification
					.getScientificFileDate());
			xmlBuilder.addTextElement(scientific, "size", justification
					.getScientificFileSize());
		}

		if (!AstronValidator.isBlankOrNull(justification
				.getTechnicalDetailsFileName())) {
			Element technical = xmlBuilder.addElement(justificationElement,
					"technicalDetailsFileMetaData");
			xmlBuilder.addTextElement(technical, "name", justification
					.getTechnicalDetailsFileName());
			xmlBuilder.addTextDateTimeElement(technical, "date", justification
					.getTechnicalDetailsFileDate());
			xmlBuilder.addTextElement(technical, "size", justification
					.getTechnicalDetailsFileSize());
		}

		if (!AstronValidator.isBlankOrNull(justification.getFigureFileName())) {
			Element figure = xmlBuilder.addElement(justificationElement,
					"figureFileMetaData");
			xmlBuilder.addTextElement(figure, "name", justification
					.getFigureFileName());
			xmlBuilder.addTextDateTimeElement(figure, "date", justification
					.getFigureFileDate());
			xmlBuilder.addTextElement(figure, "size", justification
					.getFigureFileSize());
		}

		xmlBuilder.addTextElement(justificationElement, "nighttime", justification
				.isNighttime());
		if (!AstronValidator.isBlankOrNull(justification.getNightTimeReason()))
		xmlBuilder.addTextElement(justificationElement, "nighttimereason", justification
				.getNightTimeReason());

		xmlBuilder.addTextElement(justificationElement, "parallelobservation", justification
				.isParallelObservation());
		if (!AstronValidator.isBlankOrNull(justification.getParallelObservationReason()))
		xmlBuilder.addTextElement(justificationElement, "parallelobservationreason", justification
				.getParallelObservationReason());
		xmlBuilder.addTextElement(justificationElement, "roprocessing", justification
				.isRoProcessing());
		if (!AstronValidator.isBlankOrNull(justification.getRoProcessingReason()))
		xmlBuilder.addTextElement(justificationElement, "roprocessingreason", justification
				.getRoProcessingReason());
		
		xmlBuilder.addTextElement(justificationElement, "internationalstation", justification
				.isInternationalStation());
		xmlBuilder.addTextElement(justificationElement, "essentialinternationalstation", justification
				.isInternationalStationEssential());
		if (!AstronValidator.isBlankOrNull(justification.getInternationalStationEssentialReason()))
		xmlBuilder.addTextElement(justificationElement, "essentialinternationalstationreason", justification
				.getInternationalStationEssentialReason());

		xmlBuilder.addTextElement(justificationElement, "externalroprocessing", justification
				.isExternalProcessing());
		if (!AstronValidator.isBlankOrNull(justification.getExternalProcessingReason()))
		xmlBuilder.addTextElement(justificationElement, "externalroprocessingreason", justification
				.getExternalProcessingReason());
		xmlBuilder.addTextElement(justificationElement, "offlineroprocessing", justification
				.isOfflineROProcessing());
		if (!AstronValidator.isBlankOrNull(justification.getOfflineROProcessingReason()))
		xmlBuilder.addTextElement(justificationElement, "offlineroprocessingreason", justification
				.getOfflineROProcessingReason());
		
		xmlBuilder.addTextElement(justificationElement, "otherschedulingconstraists", justification
				.isOtherSchedulingConstraints());
		if (!AstronValidator.isBlankOrNull(justification.getOtherSchedulingConstraintsReason()))
		xmlBuilder.addTextElement(justificationElement, "otherschedulingconstraistsreason", justification
				.getOtherSchedulingConstraintsReason());
		
		xmlBuilder.addTextElement(justificationElement, "combineddataproductrequest", justification
				.isCombinedDataProductRequest());
		if (!AstronValidator.isBlankOrNull(justification.getCombinedDataProductRequestReason()))
		xmlBuilder.addTextElement(justificationElement, "combineddataproductrequestreason", justification
				.getCombinedDataProductRequestReason());
		
		if (justification.getSensitivity()!=null)
		xmlBuilder.addTextElement(justificationElement, "sensitivity", justification
				.getSensitivity());
		if (justification.getMaxDataRate()!=null)
		xmlBuilder.addTextElement(justificationElement, "maxdatarate", justification
				.getMaxDataRate());
		
		return justificationElement;
	}

	private Element addXmlElement(Element root, Semester semester) {
		Element semesterElement = xmlBuilder.addElement(root, "semester");

		xmlBuilder.addTextElement(semesterElement, "telescope", semester
				.getTelescope().toLowerCase());
		if (semester.getCategory() != null) {
			Element category = xmlBuilder.addElement(semesterElement,
					"category");
			xmlBuilder.addTextElement(category, "code", semester.getCategory()
					.getCode());
		}
		xmlBuilder.addTextElement(semesterElement, "semester", semester
				.getSemester());
		xmlBuilder.addTextElement(semesterElement, "community", semester
				.getCommunity());

		return semesterElement;
	}

	private void addXmlStatussesElement(Element root, List statusHistory,
			ProposalStatus currentStatus) {
		Element statusHistoryElement = xmlBuilder.addElement(root,
				"statusHistory");
		int currentStatusIndex = 0;
		for (int i = 0; i < statusHistory.size(); i++) {
			ProposalStatus proposalStatus = (ProposalStatus) statusHistory
					.get(i);
			Element proposalStatusElement = xmlBuilder.addIndexedElement(
					statusHistoryElement, "proposalStatus", i);
			addXmlProposalStatusElement(proposalStatusElement, proposalStatus);
			if (proposalStatus.getId().intValue() == currentStatus.getId()
					.intValue())
				;
			{
				currentStatusIndex = i;
			}
		}
		Element currentStatusElement = xmlBuilder.addElement(root,
				"currentStatus");
		xmlBuilder.addElementWithAttribute(currentStatusElement,
				"statusHistoryItem", "index", currentStatusIndex + "");

	}

	protected void addXmlProposalStatusElement(Element parent,
			ProposalStatus proposalStatus) {

		xmlBuilder.addElementWithAttribute(parent, "user", "id", proposalStatus
				.getUserId().toString());
		xmlBuilder.addElement(parent, NORTHSTAR_NAMESPACE,
				getStatusStringFromCode(proposalStatus.getStatus().getCode()));
		xmlBuilder.addTextDateTimeElement(parent, "timestamp", proposalStatus
				.getStatusTime());

	}

	protected String getStatusStringFromCode(String code) {
		String[] splitted = code.split(" ");
		String result = splitted[0];
		if (splitted.length > 1) {
			for (int i = 1; i < splitted.length; i++) {
				String split = splitted[i];
				String firstChar = split.substring(0, 1).toUpperCase();
				String otherChars = split.substring(1);
				result += firstChar + otherChars;
			}
		}
		return result + "Status";
	}

	private Element addXmlElement(Element parent, Member member, String title,
			int index) {
		Element memberElement = xmlBuilder.addIndexedElement(parent, title,
				index);
        xmlBuilder.addTextElement(memberElement, "name",
                member.getName());
        xmlBuilder.addTextElement(memberElement, "affiliation",
                member.getAffiliation());
        xmlBuilder.addTextElement(memberElement, "email",
                member.getEmail());
        xmlBuilder.addTextElement(memberElement, "country",
                member.getCountry());
		addXmlRolesElement(memberElement, member);
		return memberElement;

	}

	protected void addXmlRolesElement(Element parent, Member member) {
		if (member.getRoles().size() > 0) {
			Element rolesElement = xmlBuilder.addElement(parent, "roles");
			if (member.isContactAuthor()) {
				xmlBuilder.addElement(rolesElement, NORTHSTAR_NAMESPACE,
						"contactAuthor");
			}
			if (member.isPi()) {
				xmlBuilder.addElement(rolesElement, NORTHSTAR_NAMESPACE, "pi");
			}
			if (member.isPotentialObserver()) {
				xmlBuilder.addElement(rolesElement, NORTHSTAR_NAMESPACE,
						"potentialObserver");
			}
		}
	}

	private void addXmlElement(Element parent,
			NonRegisteredMember nonRegisteredMember, int index) {
		addXmlElement(parent, nonRegisteredMember,
				"nonRegisteredMember", index);

	}

	private void addXmlElement(Element parent,
			RegisteredMember registeredMember, int index) {
		Element memberElement = addXmlElement(parent, registeredMember,
				"registeredMember", index);
		xmlBuilder.addElementWithAttribute(memberElement, "user", "id",
				registeredMember.getUserId().toString());

	}

	private void addXmlElement(Element parent, Member member, int index) {
		if (member.getClass().equals(RegisteredMember.class)) {
			addXmlElement(parent, (RegisteredMember) member, index);
		} else if (member.getClass().equals(NonRegisteredMember.class)) {
			addXmlElement(parent, (NonRegisteredMember) member, index);
		}

	}

	/**
	 * The getXmlElement method provides
	 * 
	 * @param document
	 * @return
	 */
	private void addXmlObservingRequestElement(Element parent,
			ObservingRequest observingRequest) {
		Element observingRequestElement = xmlBuilder.addElement(parent,
				getCurrentNamespace(), "observingRequest");
		addXmlObservations(observingRequestElement, observingRequest
				.getObservations(), observingRequest);
		/*
		 * this one must be implemented
		 */
		addXmlObservingRequestAllocation(observingRequestElement,
				observingRequest, "requestedAllocation");

		xmlBuilder.addTextElement(observingRequestElement,
				"overallRequiredSchedConstraints", observingRequest
						.getOverallRequiredSchedConstraints());
		xmlBuilder.addTextElement(observingRequestElement,
				"overallPreferredSchedConstraints", observingRequest
						.getOverallPreferredSchedConstraints());
		/*
		 * this one must be implemented
		 */
		addXmlSpecificObservingRequest(observingRequestElement,
				observingRequest, "specificObservingRequest");

	}

	private void addXmlObservations(Element parent, List observations,ObservingRequest observingRequest) {
		Element observationsElement = xmlBuilder.addElement(parent,
				"observations");
		for (int i = 0; i < observations.size(); i++) {
			Observation observation = (Observation) observations.get(i);
			addXmlObservation(observationsElement, observation, observingRequest, i);
		}
	}

	protected void addXmlObservation(Element parent, Observation observation,ObservingRequest observingRequest,
			int index) {
		Element observationElement = xmlBuilder.addIndexedElement(parent,
				getCurrentNamespace(), "observation", index);
		addXmlTargets(observationElement, observation, observingRequest);
		xmlBuilder.addTextElement(observationElement,
				"requiredSchedConstraints", observation
						.getRequiredSchedConstraints());
		xmlBuilder.addTextElement(observationElement,
				"preferredSchedConstraints", observation
						.getPreferredSchedConstraints());

		/*
		 * this one must be implemented
		 */
		addXmlSpecificObservation(observationElement, observation,
				"specificObservation");
	}

	private void addXmlTargets(Element parent, Observation observation, ObservingRequest observingRequest) {
		
		List targets = observingRequest.getTargetsByObservationId(observation.getId().intValue());
		if(targets.size() >0){
			Element targetsElement = xmlBuilder.addElement(parent, "targets");
			Iterator targetsit =  targets.iterator();
			while (targetsit.hasNext())
			{
				addXmlTargetRequest(targetsElement, (Target) targetsit.next());
				
			}
		}

	}

	private void addXmlTargetRequest(Element parent, Target target) {
		Element targetRequestElement = xmlBuilder.addElement(parent,
				"targetRequest");
		addXmlTarget(targetRequestElement, target);
		/*
		 * this one must be implemented
		 */
		addXmlObservationAllocation(targetRequestElement, target,
				"requestedAllocation");

	}

	protected void addXmlTarget(Element parent, Target target) 
	{
		Element targetElement = xmlBuilder.addElement(parent, getCurrentNamespace(), "target");
		xmlBuilder.addTextElement(targetElement, "fieldName", target
				.getFieldName());
		xmlBuilder.addTextElement(targetElement, "ra", target.getRa());
		xmlBuilder.addTextElement(targetElement, "dec", target.getDecl());
		addXmlEpoch(targetElement, target);
		addXmlVelocity(targetElement, target);
		/*
		 * this one must be implemented
		 */
		addXmlSpecificTarget(targetElement,
				target, "specificTarget");
	}

	protected void addXmlVelocity(Element parent, Target target) {
	}

	protected void addXmlEpoch(Element parent, Target target) 
	{
		Element epochElement = xmlBuilder.addElement(parent, "epoch");
		xmlBuilder.addTextElement(epochElement, "definition", target.getEpoch());
	}

	protected void addResource(Element parent, String name, String value,
			ResourceType resourceType) {
		List names = new ArrayList();
		names.add("name");
		names.add("unit");
		List values = new ArrayList();
		values.add(resourceType.getName());
		values.add(resourceType.getUnit());
		xmlBuilder.addTextElementWithAttributes(parent, NORTHSTAR_NAMESPACE,
				name, value, names, values);
	}

	protected abstract void addXmlObservingRequestAllocation(Element parent,
			ObservingRequest observingRequest, String name);

	protected abstract void addXmlSpecificObservingRequest(Element parent,
			ObservingRequest observingRequest, String name);

	protected abstract void addXmlObservationAllocation(Element parent,
			Target target, String name);

	protected abstract void addXmlSpecificObservation(Element parent,
			Observation observation, String name);

	protected abstract void addXmlSpecificTarget(Element parent,
			Target target, String name);

}
