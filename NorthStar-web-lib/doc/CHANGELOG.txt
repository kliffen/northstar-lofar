Changelog

v1.1.1 
	* [203] Concurrency problems with proposalData and other local files in Action classes
v1.1.0 (release_2008_02_29_v1_1_0)
	* [203] Longer session time when write status emails in admin area

v1.0.9 (release_2007_05_10_v1_0_9)
	* [421] Include more email addressses in the admin email configuration field
	* [203] Version label shows build date instead of version date

v1.0.8 (release_2006_12_22_v1_0_8)
	* png support added as figure upload
	* Stuff done by Hanno

v1.0.7 (release_2006_07_17_v1_0_7)
	* Proper read out of proposal view button in admin pages
	* Only allow positive integers within semester code

v1.0.6 (release_2006_07_06_v1_0_6)
	* [324] Refactoring layout proposal lists
	* [318] Validate if semester already exists
v1.0.5 (release_2006_06_30_v1_0_5)
	* [324] Refactoring layout proposal lists
	* [318] Validate if semester already exists
	* [309] Remove semester validation
v1.0.4 (release_2006_06_16_v1_0_3)
    * [306] Upload plain text
v1.0.2 (release_2006_06_12_v1_0_2)
	* [296] Reload northstar configuration
v1.0.1 (release_2006_05_16_v1_0_1)
	* [282] process accepted invitations
	* [268] Acceptance of invitations revised
	* [238] Two windows problems
v1.0.0 (release_2006_05_08_v1_0_0)
    * [275] Add communities to selection criteria
    * [274] Remove all warnings
