// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ObservationForm.java 
 *
 * Created on Feb 9, 2005
 *
 * Version $Id: ObservationForm.java,v 1.10 2008-07-21 10:24:27 boelen Exp $
 *
 */
package eu.radionet.northstar.control.proposal.observingrequest.observation;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import eu.radionet.northstar.business.AstroCoordinate;
import eu.radionet.northstar.control.proposal.ProposalForm;

/**
 * The ObservationForm provides
 * 
 * @author Bastiaan Verhoef
 */
public abstract class ObservationForm extends ProposalForm {
	protected String index = null;
	
	protected String fieldName = null;

	protected String ra = null; // hh:mm:ss.ss

	protected String dec = null; // +dd:mm:ss.s

	protected String epoch = null;

	protected String requiredSchedConstraints = null;

	protected String preferredSchedConstraints = null;

	protected String saveButton = null;

	protected String resolveButton = null;
	
	protected boolean uploadTargetList = false;
	
	protected List epochs = new ArrayList();
	
    protected String editTargetButton = null;
    protected String copyTargetButton = null;
    protected String deleteTargetButton = null;
    protected String deleteAllTargetButton = null;

	protected String newTargetButton = null;
    protected String clearTargetButton = null;
	protected String newTargetListButton = null;
    
    
    protected int selectedTargetId = -1;
    
    protected boolean maxNumberOfTargetsReached = false;
    protected String maxNumberOfTargetsonSameRunReached = null;

	protected boolean newTargetsAdded = false;

    protected String[] targetSummaryLabels = new String[0];
    protected List targetSummaryValues = new ArrayList();
    protected List tempTargets = new ArrayList();
    protected List columnNames = new ArrayList();
    
	/**
	 * @return Returns the dec.
	 */
	public String getDec() {
		return dec;
	}

	/**
	 * @param dec
	 *            The dec to set.
	 */
	public void setDec(String dec) {
		this.dec = dec;
	}

    /**
     * @param ra
     *            The ra to set.
     * @param dec
     *            The dec to set.
     */
    public void setRaDec(Double ra, Double dec) {

        AstroCoordinate raDec =
            new AstroCoordinate(ra.doubleValue(), dec.doubleValue());
        this.ra = raDec.RAString();
        this.dec = raDec.DecString();
    }

	/**
	 * @return Returns the epoch.
	 */
	public String getEpoch() {
		return epoch;
	}

	/**
	 * @param epoch
	 *            The epoch to set.
	 */
	public void setEpoch(String epoch) {
		this.epoch = epoch;
	}

	/**
	 * @return Returns the fieldName.
	 */
	public String getFieldName() {
		return fieldName;
	}

	/**
	 * @param fieldName
	 *            The fieldName to set.
	 */
	public void setFieldName(String fieldName) {
		this.fieldName = fieldName;
	}

	/**
	 * @return Returns the preferredSchedConstraints.
	 */
	public String getPreferredSchedConstraints() {
		return preferredSchedConstraints;
	}

	/**
	 * @param preferredSchedConstraints
	 *            The preferredSchedConstraints to set.
	 */
	public void setPreferredSchedConstraints(String preferredSchedConstraints) {
		this.preferredSchedConstraints = preferredSchedConstraints;
	}

	/**
	 * @return Returns the ra.
	 */
	public String getRa() {
		return ra;
	}

	/**
	 * @param ra
	 *            The ra to set.
	 */
	public void setRa(String ra) {
		this.ra = ra;
	}

	/**
	 * @return Returns the requiredSchedConstraints.
	 */
	public String getRequiredSchedConstraints() {
		return requiredSchedConstraints;
	}

	/**
	 * @param requiredSchedConstraints
	 *            The requiredSchedConstraints to set.
	 */
	public void setRequiredSchedConstraints(String requiredSchedConstraints) {
		this.requiredSchedConstraints = requiredSchedConstraints;
	}



	/**
	 * @return Returns the saveButton.
	 */
	public String getSaveButton() {
		return saveButton;
	}

	/**
	 * @param saveButton
	 *            The saveButton to set.
	 */
	public void setSaveButton(String saveButton) {
		this.saveButton = saveButton;
	}

	/*
	 * public void fillInitial() { setFieldName(observation.getFieldName()); if
	 * ((observation.getDecl() != null) && (observation.getRa() != null)) {
	 * AstroCoordinate raDec = new AstroCoordinate(observation.getRa()
	 * .doubleValue(), observation.getDecl().doubleValue());
	 * setRa(raDec.RAString()); setDec(raDec.DecString()); }
	 * setEpoch(observation.getEpoch());
	 * setRequiredSchedConstraints(observation.getRequiredSchedConstraints());
	 * setPreferredSchedConstraints(observation.getPreferredSchedConstraints()); }
	 */

	/**
	 * @return Returns the resolveButton.
	 */
	public String getResolveButton() {
		return resolveButton;
	}

	/**
	 * @param resolveButton
	 *            The resolveButton to set.
	 */
	public void setResolveButton(String resolveButton) {
		this.resolveButton = resolveButton;
	}

	public ActionErrors validate(ActionMapping mapping,
			HttpServletRequest request) {
		ActionErrors errors = new ActionErrors();
		/*
		 * totalduration is different for the childs, validation must be don
		 * there
		 */
		/*
		 * ra checking
		  is done in opticonObservation
		if (!AstronValidator.isBlankOrNull(ra)) {
			if (!AstronValidator.isRa(ra)) {
				errors
						.add("ra", new ActionMessage(
								"error.nora.observation.ra"));
			}

		}

		if (!AstronValidator.isBlankOrNull(dec)) {
			if (!AstronValidator.isDec(dec)) {
				errors.add("dec", new ActionMessage(
						"error.nodec.observation.dec"));
			}

		}
		if (AstronValidator.isBlankOrNull(ra)
				&& !AstronValidator.isBlankOrNull(dec)
				|| !AstronValidator.isBlankOrNull(ra)
				&& AstronValidator.isBlankOrNull(dec)) {
			errors.add("dec", new ActionMessage(
					"error.novalidradec.observation"));
		}
		*/
		return errors;
	}

	public void reset(ActionMapping mapping, HttpServletRequest request) {
		//this.resolveButton = null;
		this.saveButton = null;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public void reset() {
		selectedTargetId = -1;
		tempTargets = new ArrayList();
		fieldName = null;

		ra = null; // hh:mm:ss.ss

		dec = null; // +dd:mm:ss.s

		epoch = null;

		requiredSchedConstraints = null;

		preferredSchedConstraints = null;

		targetSummaryLabels = new String[0];
	    targetSummaryValues = new ArrayList();
		
		uploadTargetList = false;
		resetButtons();

	}

	public boolean isUploadTargetList() {
		return uploadTargetList;
	}

	public void setUploadTargetList(boolean uploadTargetList) {
		this.uploadTargetList = uploadTargetList;
	}

	public List getEpochs()
	{
		return epochs;
	}

	public void setEpochs(List epochs)
	{
		this.epochs = epochs;
	}

	
    public void setDeleteTargetButton(int index, String value) {
        selectedTargetId = index;
        deleteTargetButton = value;
    }

    public String getDeleteTargetButton(int index) {
        return deleteTargetButton;
    }

    public void setEditTargetButton(int index, String value) {
        selectedTargetId = index;
        editTargetButton = value;
    }

    public String getEditTargetButton(int index) {
        return editTargetButton;
    }

    public void setCopyTargetButton(int index, String value) {
        selectedTargetId = index;
        copyTargetButton = value;
    }

    public String getCopyTargetButton(int index) {
        return copyTargetButton;
    }

    /**
     * @return Returns the selectedTargetId.
     */
    public int getSelectedTargetId() {
        return selectedTargetId;
    }

	public void setSelectedTargetId(int selectedTargetId) {
		this.selectedTargetId = selectedTargetId;
	}
	
	public String getNewTargetButton() {
		return newTargetButton;
	}

	public void setNewTargetButton(String newTargetButton) {
		this.newTargetButton = newTargetButton;
	}

	/**
     * @return Returns the clearTargetButton.
     */
    public String getClearTargetButton() {
        return clearTargetButton;
    }

    /**
     * @param clearTargetButton The clearTargetButton to set.
     */
    public void setClearTargetButton(String clearTargetButton) {
        this.clearTargetButton = clearTargetButton;
    }

    public boolean isMaxNumberOfTargetsReached()
	{
		return maxNumberOfTargetsReached;
	}

	public void setMaxNumberOfTargetsReached(boolean maxNumberOfTargetsReached)
	{
		this.maxNumberOfTargetsReached = maxNumberOfTargetsReached;
	}

	public String[] getTargetSummaryLabels()
	{
		return targetSummaryLabels;
	}

	public void setTargetSummaryLabels(String[] targetSummaryLabels)
	{
		this.targetSummaryLabels = targetSummaryLabels;
	}

	public List getTargetSummaryValues()
	{
		return targetSummaryValues;
	}

	public void setTargetSummaryValues(List targetSummaryValues)
	{
		this.targetSummaryValues = targetSummaryValues;
	}

	public String getNewTargetListButton()
	{
		return newTargetListButton;
	}

	public void setNewTargetListButton(String newTargetListButton)
	{
		this.newTargetListButton = newTargetListButton;
	}

	public boolean isNewTargetsAdded()
	{
		return newTargetsAdded;
	}

	public void setNewTargetsAdded(boolean newTargetsAdded)
	{
		this.newTargetsAdded = newTargetsAdded;
	}
	
	public List getColumnNames()
	{
		return columnNames;
	}

	public void setColumnNames(List columnNames)
	{
		this.columnNames = columnNames;
	}
	
	public List getTempTargets()
	{
		return tempTargets;
	}

	public void setTempTargets(List newTargets)
	{
		this.tempTargets = newTargets;
	}
	
	public void resetButtons()
	{
		forward = null;
		saveButton = null;
		resolveButton = null;
		editTargetButton = null;
	    copyTargetButton = null;
	    deleteTargetButton = null;

	    newTargetButton = null;
        clearTargetButton = null;
		newTargetListButton = null;	
		deleteAllTargetButton = null;
		super.reset(null, null);
	}

	public String getDeleteAllTargetButton() {
		return deleteAllTargetButton;
	}

	public void setDeleteAllTargetButton(String deleteAllTargetButton) {
		this.deleteAllTargetButton = deleteAllTargetButton;
	}

	public String getMaxNumberOfTargetsonSameRunReached() {
		return maxNumberOfTargetsonSameRunReached;
	}

	public void setMaxNumberOfTargetsonSameRunReached(
			String maxNumberOfTargetsonSameRunReached) {
		this.maxNumberOfTargetsonSameRunReached = maxNumberOfTargetsonSameRunReached;
	}



}