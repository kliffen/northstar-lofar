// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.telescope;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.ConnectionException;
import nl.astron.database.exception.DatabaseException;
import eu.radionet.northstar.business.NorthStarDelegate;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.useradministration.data.entities.User;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;
import org.apache.struts.util.LabelValueBean;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.Telescope;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalStatus;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.UserTelescope;

public class SetUpTelescopeAccessAction extends Action {
    private Log log = LogFactory.getLog(SetUpTelescopeAccessAction.class);
	private NorthStarDelegate northstarDelegate = null;
	private CollaborationDelegate collaborationDelegate = null;
	
	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		if (form != null)
		{
			 TelescopeAccessForm  telescopeAccessForm = (TelescopeAccessForm) form;
			
			//instantiate AdminDelegate
	        try {
	            northstarDelegate = NorthStarDelegate.getInstance();
	            collaborationDelegate = CollaborationDelegate.getInstance();
	        } catch (ConnectionException ce) {
	            log.fatal("Cannot instantiate AdminDelegate: " + ce.getMessage());
	            return (mapping.findForward("fatalError"));
	        }
	        
	        // get telescopes	
			List telescopes = fillTelescopeList();
			telescopeAccessForm.settelescopeNameList(telescopes);

			// get user account info
	        HttpSession session = request.getSession();
			UserAccount ownUserAccount = (UserAccount) session.getAttribute(Constants.OWN_USERACCOUNT);
			
	        // returns a list of usertelescope objects
			List currentAccessRights = getCurrentAccessRights(ownUserAccount);

			telescopeAccessForm.setHasMultipleAccess(true);
			
			// set selected telescope if needed
			if (telescopeAccessForm.getSelectedTelescope() == null){
				telescopeAccessForm.setSelectedTelescope(getFirstTelescopeName(currentAccessRights));
			}
			
			// get other users with access to selected telescope 
			List usernames = fillUserNameList(telescopeAccessForm.getSelectedTelescope(),session);
			telescopeAccessForm.setUserNameList(usernames);
			
		}
		return mapping.findForward(Constants.SUCCESS);
	}
		
	private List getCurrentAccessRights(UserAccount userAccount){
		List AccessRights = new ArrayList();
		try{
			AccessRights = northstarDelegate.getTelescopeAccessRights(userAccount.getId());
		} catch (DatabaseException de) {
            log.error("Cannot retrieve user accounts: "
                    + de.getMessage());
        }
        return AccessRights;
	}
	
	private List fillTelescopeList(){	
		//get list of telescope objects

		List telescopeConfigurations = new ArrayList();
		
		Iterator telescopeIterator = NorthStarConfiguration.getTelescopes().iterator();
		while (telescopeIterator.hasNext()){
			Telescope telescope = (Telescope) telescopeIterator.next();
			Iterator telescopeConfigurationsIterator = telescope.getTelescopeConfigurations().iterator();
			while (telescopeConfigurationsIterator.hasNext()){
				TelescopeConfiguration telescopeConfiguration = (TelescopeConfiguration) telescopeConfigurationsIterator.next();
				Map contexts = telescopeConfiguration.getOptionsContexts();
				//telescopeConfiguration.getContextConfiguration("regular");
				Iterator contextIt = contexts.keySet().iterator();
				while (contextIt.hasNext()){
					String key = (String) contextIt.next();
					ContextType contextConfiguration = (ContextType) contexts.get(key);
					List telescopes = OptionsUtils.getLabelValueBeans("telescopeConfigurations", new HashMap(), contextConfiguration);
					Iterator labelIt = telescopes.iterator();
					while(labelIt.hasNext()){
						LabelValueBean lvb= (LabelValueBean) labelIt.next();
						
						if(lvb.getValue() != null){
							UserTelescope oTelescope = new UserTelescope();
							oTelescope.setTelescope(lvb.getValue());
							telescopeConfigurations.add(oTelescope);
						}
					}
				}
			}
		}
		
				
		return telescopeConfigurations;
	}
	
	private List fillUserNameList(String telescopeName, HttpSession session){
		//get useraccount summaries
		
        	List userList = new ArrayList();
        try {	
        	List IdList = northstarDelegate.getUsersTelescope(telescopeName);
        	// find user names with the id's
        	Iterator IdIterator = IdList.iterator();
        	while (IdIterator.hasNext()){
        		UserTelescope lut = (UserTelescope) IdIterator.next();
        		if(lut.getUserId() != null){
        			User myUser = northstarDelegate.getUser(lut.getUserId());
        			userList.add(myUser);
        		}else if(lut.getMemberId() != null){
        			NonRegisteredMember member = collaborationDelegate.getMemberById(lut.getMemberId() * -1);
        			if(member==null){
        				member = collaborationDelegate.getMemberById(lut.getMemberId());
        			}
        			if(member!=null){
	        			User myUser = new User();
		      			myUser.setId(member.getId().intValue() * -1);
		      			myUser.setLastName(member.getName());
		      			userList.add(myUser);
        			}else{
        				log.warn("could not retrieve member: "+lut.getMemberId().toString());
        			}
        		}
        	}
        	
        	return userList;
        	
        } catch (DatabaseException de) {
            log.error("Cannot retrieve user accounts: "
                    + de.getMessage());
        }
        
        UserAccount ownUserAccount = (UserAccount) session.getAttribute(Constants.OWN_USERACCOUNT);
        User myUser;
		try {
			myUser = northstarDelegate.getUser(ownUserAccount.getId());
			userList.add(myUser);
		} catch (DatabaseException e) {
			log.error("Cannot retrieve user"
                    + e.getMessage());
		}
		
        return userList;
	}
	
	private String getFirstTelescopeName(List accessList)
	{	// get item 0 from accesslist
		if(!accessList.isEmpty())
		{
			UserTelescope myTelescope = (UserTelescope) accessList.get(0);
			if (myTelescope != null){
				return myTelescope.getTelescope();
			}
		}
		return "";
	}
	
}
