// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessApplicantAction.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: ProcessApplicantAction.java,v 1.3 2008-04-08 08:02:58 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.applicants.addedit;

import java.util.Locale;
import java.util.MissingResourceException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The ProcessApplicantAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessApplicantAction extends LockedAction {


	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		/*
		 * if pressed on Discard changes
		 */
		if (isCancelled(request)) {
			return mapping.findForward("applicants");

		}
		HttpSession session = request.getSession();
		ApplicantForm applicantForm = (ApplicantForm) form;
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		Proposal proposal = proposalData.getProposal();

		NonRegisteredMember nonRegisteredMember = new NonRegisteredMember();
		/*
		 * if memberId is not blank, it must be edit.
		 */
		if (!AstronValidator.isBlankOrNull(applicantForm.getMemberId())) {
			Member member = (Member) proposal.getMembers().get(
					AstronConverter.toInteger(applicantForm.getMemberId())
							.intValue());
			/*
			 * if it is an nonregistered member
			 */
			if (member.getClass().equals(NonRegisteredMember.class)) {
				nonRegisteredMember = (NonRegisteredMember) member;
				if (!nonRegisteredMember.getEmail().equalsIgnoreCase(
						applicantForm.getEmail())) {
					nonRegisteredMember.setRemoveInvitation(true);
				}
				else if (AstronValidator.isBlankOrNull(applicantForm.getInvite())) {
					nonRegisteredMember.setRemoveInvitation(true);
				}
			}
		}
		if (!AstronValidator.isBlankOrNull(applicantForm.getInvite())) {
			nonRegisteredMember.setInvite(true);
		}else {
			nonRegisteredMember.setInvite(false);
		}
		nonRegisteredMember.setName(applicantForm.getName());
		nonRegisteredMember.setAffiliation(applicantForm.getAffiliation());
		nonRegisteredMember.setEmail(applicantForm.getEmail());
		
		 // now the country name is in ISO format.
        String countr = applicantForm.getSelectedCountry();
        Locale[] localit = Locale.getAvailableLocales();
        
        for(int i=0; i < localit.length; i++){
        	Locale locale = localit[i]; 
        	try{
	       		if (locale.getISO3Country().equalsIgnoreCase(countr)) {
	       			nonRegisteredMember.setCountry(locale.getDisplayCountry());
	       		}
        	}catch(MissingResourceException e){
           		// do not add country...
	       	}
        }
		//applicantForm.getCountry());

		proposal.addChangeMember(nonRegisteredMember);
		session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
		return mapping.findForward("applicants");
	}

}
