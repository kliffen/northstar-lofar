// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.struts.util.LabelValueBean;

import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;
import eu.opticon.northstar.control.observingrequest.instrument.SetUpInstrumentBean;
import eu.opticon.northstar.data.entities.OpticonInstrument;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonObservingRequest;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.ProposalValidator;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.Justification;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalError;
import eu.radionet.northstar.data.entities.Resource;
import eu.radionet.northstar.data.entities.Target;

/*
 * lofar validations:
 * 
 * VL1  there should be at least 1 observation specified
 * VL1b except for umbrella/envelope proposals
 * VL2  if it is a long term proposal, then the long term details should be given
 * VL3  total amount of hours should be specified
 * VL3b except for umbrella proposals
 * VL4  total amount of storage should be specified
 * VL4b except for umbrella proposals
 * VL5  each observation should have an instrument specified 
 * VL6  each observation should have a station, clock, filter and antenna selected
 * VL7  each observation should have central freq and bandwidth specified
 * VL7b except for TBB measurements
 * VL8  
 * VL9  interferometer and direct storage observations should have the integration time specified
 * VL10 tied array observations should have the amount of beams and sample rate defined
 * VL11 if imaging is used, field size, pixel size, amount of frequency channels should be specified
 * VL12 if averaging is used, averaging time and averaging frequency should be specified.
 * VL13 if TBB instrument is selected, the total nr. of events should be specified.
 * VL14 if piggy-back mode is enabled, then the details should be given
 * VL15 a non-standard instrument should be specified.
 * VL16 targets should have ra,dec and time specified.
 * VL17 
 * VL18
 * 
 * VL20 target should have a observation connected
 * VL21 max 488 subbands per run
 * 
 * VL22 the choice of station should not contradict the limitation of international stations
 */

public class LofarProposalValidator extends ProposalValidator{

	public ProposalError validate(Proposal proposal) {
		ProposalError error = super.validate(proposal);
		//return error;
	//}

		int totalDiscrepanties = 0;
		if (proposal.getJustification() != null) {
			for (Observation observation : proposal.getObservingRequest().getObservations()) {
				if (observation != null) {
					OpticonInstrument instrument = ((OpticonObservation) observation).getInstrument();
					totalDiscrepanties += validateChoiceOfStation(proposal.getJustification(), instrument);
				}
			}
			if (proposal.getJustification().isInternationalStation()) {
				if (totalDiscrepanties == proposal.getObservingRequest().getObservations().size()) {
					error.getErrors().add(new ProposalError("justification",
							"selected stations. No international stations were selected under the 'Observation request' tab while the 'Justification' tab stated the contrary."));
				}
			} else if (totalDiscrepanties != 0) {
					error.getErrors().add(new ProposalError("justification",
							"selected stations. Chosing no international stations under the 'Justication' tab contradicts at least one choice of sations under 'Observation request' tab."));
			}
		}
		
		return error;
	}
	
	public ProposalError validatePageNumber(Justification justification,
			ObservingRequest observingRequest, boolean fixed) {
		ProposalError error = new ProposalError("justification","Justification");
		String telescopeName  ="";
		if (observingRequest.getObservations().size()>0) {
            OpticonObservation observation = (OpticonObservation) observingRequest.getObservations().get(0);
            telescopeName = observation.getTelescopeConfiguration();
		} 
		
		int totalPageNumber=0;
		if(justification.getTechnicalDetailsFilePages()!=null)
			totalPageNumber+=justification.getTechnicalDetailsFilePages();
		
		if(justification.getScientificFileSize()!=null)
			totalPageNumber+=justification.getScientificFilePages();
		if(justification.getFigureFilePages()!=null)
			totalPageNumber+=justification.getFigureFilePages();
		  
		
		List urlList = new ArrayList();
		String Url = null;
		
		int maxTotalPages=0; //telescopeConfiguration.getMaxPublicationCount();
		urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_TOTAL_PAGECOUNT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			maxTotalPages = AstronConverter.toInteger(Url).intValue();
		
		}
	
		if(fixed)
		{
			if(totalPageNumber>maxTotalPages)
				error.getErrors().add(new ProposalError("maximumFileSize","Maximum Number of page should not exceed "+ maxTotalPages));
			return error;
		}
		
		int timeGap =0;
		
		urlList = OptionsUtils.getLabelValueBeans(Constants.TIME_GAP_PAGE_COUNT_INCREMENT ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			if(AstronValidator.isPositiveInt(Url)){
				timeGap = new Integer(Url).intValue();
			}
		}
		
		 urlList = new ArrayList();
		 Url = null;
		int minimumPageNumber =0;
		
		urlList = OptionsUtils.getLabelValueBeans(Constants.MINIMUM_JUSTIFICATION_PAGE_NUMBER ,
					new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			if(AstronValidator.isPositiveInt(Url)){
				minimumPageNumber = new Integer(Url).intValue();
			}
		}
		
		double requestedTimeVal =timeGap; // null pointer check for existing proposal who does not have minimum hours
		String requestedTimeHours=AstronConverter.toString(NorthStarDelegate.getResourceValue(
				observingRequest.getAllocations(),
				telescopeName+'_'+OpticonConstants.GLOBAL));
		if(requestedTimeHours!=null)
			requestedTimeVal =AstronConverter.toDouble(requestedTimeHours);
		
		/***
		 * < 250 hours: up to 4 pages
		 * < 500 hours: up to 5 pages
		 * < 750 hours: up to 6 pages
		 * < 1000 hours: up to 7 pages
		 * > =1000 hours: up to 8 pages
		 */
		if(requestedTimeVal<=timeGap) {
			if(totalPageNumber>minimumPageNumber)
				error.getErrors().add(new ProposalError("maximumFileSize",
						"Page limits are dependent on the amount of observing time requested. Observing time requested is less than "+ timeGap +" hours. Therefore, page limit must not exceed "+minimumPageNumber+"." ));	
		}
		else if(timeGap<requestedTimeVal && requestedTimeVal <=(2*timeGap)) {
			
			if(totalPageNumber>(minimumPageNumber+1))
				error.getErrors().add(new ProposalError("maximumFileSize",
						"Page limits are dependent on the amount of observing time requested. Observing time requested is less than "+2*timeGap +" hours. Therefore, page limit must not exceed "+(minimumPageNumber+1)+"."));	
		}
		else if((2*timeGap)<requestedTimeVal && requestedTimeVal <=(3*timeGap)) {
			if(totalPageNumber>(minimumPageNumber+2))
				error.getErrors().add(new ProposalError("maximumFileSize",
						"Page limits are dependent on the amount of observing time requested. Observing time requested is less than "+3* timeGap +" hours. Therefore, page limit must not exceed "+(minimumPageNumber+2)+"."));	
		} 
		else if((3*timeGap)<requestedTimeVal && requestedTimeVal <=(4*timeGap)) {
			if(totalPageNumber>(minimumPageNumber+3))
				error.getErrors().add(new ProposalError("maximumFileSize",
						"Page limits are dependent on the amount of observing time requested. Observing time requested is less than "+4* timeGap +" hours. Therefore, page limit must not exceed "+(minimumPageNumber+3)+"."));	
		} 
		else if(requestedTimeVal>(4*timeGap)) {
			int maxTotalFilePages=0; //telescopeConfiguration.getMaxPublicationCount();
			urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_TOTAL_PAGECOUNT ,
						new HashMap(), contextConfiguration);
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
				Url =  urlValue.getValue();
				maxTotalFilePages = AstronConverter.toInteger(Url).intValue();
			}
			
			if(totalPageNumber>maxTotalFilePages)
				error.getErrors().add(new ProposalError("maximumFileSize",
						"Page limits are dependent on the amount of observing time requested. Observing time requested is more than 1000 hours. Therefore, page limit must not exceed 8."));	
		}
		
		return error;
	}
	
	
	
	public ProposalError validate(ObservingRequest observingRequest)
	{
		OpticonObservingRequest opticonObservingRequest = (OpticonObservingRequest) observingRequest;
		List errors = new ArrayList();
		FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		
		ProposalError error = new ProposalError("observingRequest",
                "Observing Request");
		//VL1
		if(observingRequest.getObservations().size() == 0){
			//if(opticonObservingRequest.getCategory() != eu.radionet.northstar.business.Constants.CATEGORY_UMBRELLA){
			//VL1b 
			/*
			 
			//disable this for LTA proposals 
			List urlList = new ArrayList();
			String Url = new String();
			urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_OBSERVATIONS, new HashMap(), contextConfiguration);
			
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
				Url =  urlValue.getValue(); 
				if (AstronConverter.toInteger(Url).intValue() > 0){
					errors.add(new ProposalError("noObservations", "No observations have been created"));
				}
			}
			*/
		}

		//VL2
		if (opticonObservingRequest.isLongTermProposal()
				&& AstronValidator.isBlankOrNull(opticonObservingRequest
						.getLongTermProposalSpecifics())){
			errors.add(new ProposalError("longTermProposalSpecifics",
					"Long term proposal specifics"));
		}
		
		//VL3, VL4
		/* HAH 2019-01-18 - enabled storage and time validation, validating when associated resource configured in options file */
		boolean storage = !OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_LONGTERM_STORAGE,fieldsDefinitionType);
		boolean processing = !OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_PROCESSING_TIME,fieldsDefinitionType);
		boolean timerequest = !OptionsDelegate.allowedToDisplay(OpticonConstants.DISPLAY_REQUESTED_TIME,fieldsDefinitionType);
		boolean mintimerequest = !OptionsDelegate.allowedToDisplay(OpticonConstants.USEFUL_TIME,fieldsDefinitionType);
		if(observingRequest.getAllocations() != null){
			Set allocations = observingRequest.getAllocations().entrySet();
			Iterator allocIt = allocations.iterator();
			while (allocIt.hasNext()){
				Map.Entry allocation = (Map.Entry) allocIt.next();
				Resource resource = (Resource) allocation.getValue();
				if ((resource != null) && (resource.getValue().doubleValue() >= 0)){
					if(allocation.getKey().toString().equalsIgnoreCase("LOFAR_Global")){
						timerequest=true;
					}
					if(allocation.getKey().toString().equalsIgnoreCase("LOFAR_longTermStorage")){
						storage=true;
					}
					if(allocation.getKey().toString().equalsIgnoreCase("LOFAR_totalProcessingTime")){
						processing=true;
					}
					if(allocation.getKey().toString().equalsIgnoreCase("LOFAR_usefulGlobal")){
						mintimerequest=true;
					}
				}
			}
			if (!storage)	{
				errors.add(new ProposalError("requestedStorage", "Long term storage requested this semester"));
			}
			if (!timerequest)	{
				errors.add(new ProposalError("requestedTime", "Time requested this semester"));
			}
			if (!processing)	{
				errors.add(new ProposalError("requestedProcessing", "Processing time requested this semester"));
			}
			if (!mintimerequest)	{
				errors.add(new ProposalError("usefulTime", "Minimum useful time requested this semester"));
			}
			
		}

		boolean mintimejustification =
				( !OptionsDelegate.allowedToDisplay(OpticonConstants.ALLOCATION_JUSTIFICATION,fieldsDefinitionType) ||
						(opticonObservingRequest.getAllocationJustification() != null
						&& !opticonObservingRequest.getAllocationJustification().isEmpty() )
				);
		
		if (!mintimejustification)	{
			errors.add(new ProposalError("usefulTimeJustification", "Justification for minimum useful time requested this semester"));
		}
		/* */
		
		
		// check observations
        for (int i = 0; i < observingRequest.getObservations().size(); i++) {
            Observation observation = (Observation) observingRequest.getObservations().get(i);
            ProposalError errorChild = validate(observation, observingRequest);
            if (!errorChild.isEmpty()) {
                error.getErrors().add(errorChild);
            }
            
            //VL17 
            List urlList = new ArrayList();
    		String Url = null;
    		int maxTargets =1;
    		
    		urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.MAX_TARGETS ,
    					new HashMap(), contextConfiguration);
    		if (urlList != null && urlList.size() > 1){
    			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
    			Url =  urlValue.getValue();
    			if(AstronValidator.isPositiveInt(Url)){
    				maxTargets = new Integer(Url).intValue();
    			}
    		}
        }
		
		//  Check observingrequest Targets
        if (opticonObservingRequest.getTargets() != null){
	        Iterator targetIt = opticonObservingRequest.getTargets().iterator();
	        while (targetIt.hasNext()){
	            Target target = (Target) targetIt.next();
	        	ProposalError errorChild = validate((target) );
	            if (!errorChild.isEmpty()){
	               error.getErrors().add(errorChild);
	            }
	        }
	        
	        
	        
	        
	        
	        if (OptionsDelegate.isRequired(OpticonConstants.NOISE_LEVEL, fieldsDefinitionType)) {
	        	if (opticonObservingRequest.getNoiseLevel() == null) {
	        		errors.add(new ProposalError("noNoiseLevel","Noise level is required for observing request"));
	        	}
	        }
        }
        
     // count subbands per run (max 488)  
     // loop through all observations and get their duration and storage
        Map<String,Integer> SubbandsRunMap = new HashMap();
     	for(Target target : observingRequest.getTargets()){
     			OpticonTarget optarget = (OpticonTarget) target;
     			if(optarget.getSubbandList() != null && optarget.getSubbandList().matches("^[A-Za-z]+$")){
     				errors.add(new ProposalError("target subbands","target "+optarget.getFieldName()+" contains invalid subbandlist: "+optarget.getSubbandList()));
     			}
     			else{
	     			int targetSubbands = SetUpInstrumentBean.convertSubbandListDescription(optarget.getSubbandList());
	     			if(optarget.getRunnumber() != null){	
	     				if(SubbandsRunMap.get(optarget.getRunnumber()) == null){
	     					SubbandsRunMap.put(optarget.getRunnumber(), new Integer(0));
	     				}
	     				Integer amount = SubbandsRunMap.get(optarget.getRunnumber());
	     				amount = Integer.valueOf(targetSubbands + amount.intValue() );
	     				SubbandsRunMap.put(optarget.getRunnumber(), amount);
	     			}
     			}
     			
     	}
     	
     	for (Map.Entry<String, Integer> entry : SubbandsRunMap.entrySet())
     	{
     	    //maximum target subband on same run (488) should be taken from config file
     		List urlList = new ArrayList();
			String Url = new String();
			urlList = OptionsUtils.getLabelValueBeans(Constants.MAX_TARGETS_SAME_RUN, new HashMap(), contextConfiguration);
			
			if (urlList != null && urlList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
				Url =  urlValue.getValue(); 
				}
     		if(entry.getValue().intValue() > AstronConverter.toInteger(Url).intValue()){
     			errors.add(new ProposalError("maxSubbands","Run "+entry.getKey()+" has more than "+ Url+" subbands: ("+entry.getValue().toString()+")"));
     		}
     	}


     	
     	/*
     		char chr = (char) (observationcount+65);
     		Integer amount = SubbandsRunMap.get(String.valueOf(chr) );
     		if(amount != null && amount > 488){
     			errors.add(new ProposalError("maxSubbands","Run "+String.valueOf(chr)+" has more than 488 subbands: ("+amount.toString()+")"));
     		}
     		observationcount++;
     		
     	*/
		error.setDescription("Observing Request");
		error.getErrors().addAll(0, errors);
		return error;

	}

	/**
	 * Validates the telescope specific observation
	 * 
	 * @see eu.radionet.northstar.business.ProposalValidator#validate(eu.radionet.northstar.data.entities.Observation)
	 */
	public ProposalError validate(Observation observation,ObservingRequest observingRequest)
	{
		List errors = new ArrayList();
		OpticonObservation opticonObservation = (OpticonObservation) observation;
		
			
		// this should not be possible as the lofar telescope is automatically selected, but check anyway.		
		if (AstronValidator.isBlankOrNull(opticonObservation
				.getTelescopeConfiguration())){
			errors.add(new ProposalError("telescopeConfiguration",
					"No telescope specified"));
		}

		//VL5 
		if (opticonObservation.getInstrument() != null
				&& !AstronValidator.isBlankOrNull(opticonObservation
						.getInstrument().getName())){
			// check instrument
			ProposalError errorChild = validate(opticonObservation
					.getInstrument(), opticonObservation.getTelescopeConfiguration());
			if (!errorChild.isEmpty()){
				errors.add(errorChild);
			}
		}
		else{
			errors.add(new ProposalError("instrumentConfiguration",
					"No instrument specified"));
		}
		

		
		//ProposalError error = super.validate(observation,observingRequest);
		ProposalError error = new ProposalError("observation", "Observing run");
		error.setDescription("Observing run ");
		error.getErrors().addAll(0, errors);
		return error;
	}
    
	

	protected boolean isFilterDetailsRequired(OpticonInstrument instrument)
	{
		return false;
		
	}
	
	
	public ProposalError validate(OpticonInstrument instrument, String telescope) 
	{
		HashMap enteredValues = new HashMap();
		enteredValues.put(OpticonConstants.TELESCOPE_CONFIGURATIONS,
				OptionsUtils.getList(telescope));
		ProposalError error = new ProposalError("instrumentConfiguration", instrument
				.getName() + " Instrument");
		List configurations = OptionsDelegate.getOptionsConfigurationTypes(
				OpticonConstants.INSTRUMENT_CONFIGURATIONS, enteredValues,
				contextConfiguration);
		ConfigureOptionType configureOptionType = (ConfigureOptionType) OptionsDelegate
				.getOption(instrument.getName(), configurations);

		if (configureOptionType != null
				&& configureOptionType.getSubfields() != null) {
			FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) configureOptionType
					.getSubfields();
			
			//VL6
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_STATION, 
					fieldsDefinitionType)){
				if (AstronValidator.isBlankOrNull(instrument.getStation())){
					error.getErrors()
						.add(new ProposalError("instrumentStation", "Instrument station"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_ANTENNA, 
					fieldsDefinitionType)){
				if (AstronValidator.isBlankOrNull(instrument.getAntenna())){
					error.getErrors()
						.add(new ProposalError("instrumentAntenna", "Instrument antenna"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_CLOCK, 
					fieldsDefinitionType)){
				if (AstronValidator.isBlankOrNull(instrument.getClock())){
					error.getErrors()
						.add(new ProposalError("instrumentClock", "Instrument clock"));
				}
			}
			
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_ONE_FILTER,
					fieldsDefinitionType)) {
				if (AstronValidator.isBlankOrNull(instrument.getOneFilter())) {
					error.getErrors()
							.add(new ProposalError("instrumentFilters", "Filter"));
				}
			}

			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_SUBARRAY_POINTINGS,
					fieldsDefinitionType)) {
				if (instrument.getSubarrayPointings() == null) {
					error.getErrors()
							.add(new ProposalError("instrumentSubarrayPointings", "Subarray Pointings"));
				}
			}

			if (OptionsDelegate.isRequired(OpticonConstants.OBSERVATION_NOISE_LEVEL,
					fieldsDefinitionType)) {
				if (instrument.getObservation().getNoiseLevel() == null) {
					error.getErrors()
							.add(new ProposalError("observationNoiseLevel", "Noise level"));
				}
			}

			//VL7 VL7b
			/*if(!instrument.getName().startsWith("TBB")){
				if (instrument.getFilterBandwidth() == null || instrument.getFilterBandwidth().doubleValue() < 0 ){
					error.getErrors()
						.add(new ProposalError("filterBandwidth", "Instrument bandwidth"));
				}
				
				if (instrument.getFilterFrequency() == null || instrument.getFilterFrequency().doubleValue() < 0 ) {
					error.getErrors()
						.add(new ProposalError("filterFrequency", "Instrument frequency"));
				}
			}
			*/
			
			//VL9
			if(instrument.getName().startsWith("Interfero") || instrument.getName().startsWith("Direct") ){
				if(instrument.getIntegrationTime()==null){
					error.getErrors()
						.add(new ProposalError("integrationTime", "Integration time"));
				}
			}
			
			//VL11
			if(instrument.isImaging() ){
				if(instrument.getPixelSizeX() ==null || instrument.getPixelSizeY() == null){ 
					error.getErrors().add(new ProposalError("instrumentImaging", "Pixel size"));
				}
				if(instrument.getFieldSizeX() ==null || instrument.getFieldSizeY()==null){
					error.getErrors().add(new ProposalError("instrumentImaging", "Field size"));
				}
				if(instrument.getFrequencyChannels() == null){
					error.getErrors().add(new ProposalError("instrumentImaging", "Frequency channels"));
				}
			}
			
			
			//VL12
			if(instrument.isAveraging()){
				if(instrument.getAveragingTime() == null || instrument.getAveragingTime() <0){
					error.getErrors().add(new ProposalError("instrumentAveraging","Averaging time"));
				}
				if(instrument.getAveragingFrequency() == null || instrument.getAveragingFrequency() <0){
					error.getErrors().add(new ProposalError("instrumentAveraging","Averaging frequency"));
				}
			}
			
			//VL14
			if(instrument.isPiggyBack()){
				if(AstronValidator.isBlankOrNull(instrument.getPiggyBackSpecifics() ) ){
					error.getErrors().add(new ProposalError("instrumentPiggyBack","Piggyback specifics"));
				}
			}
			
			//VL15
			if(instrument.getName() != null && instrument.getName().startsWith("Non")){
				if(AstronValidator.isBlankOrNull(instrument.getComments() )){
					error.getErrors().add(new ProposalError("instrumentNonStandard","Non standard specifics"));
				}
			}
			if (OptionsDelegate.isRequired(OpticonConstants.INSTRUMENT_FRAME_EXPOSURE_TIME_DETAILS,
					fieldsDefinitionType)) {
				if (instrument.isNonDefaultFrameExposureTime()
						&& instrument.getNonDefaultFrameExposureTimeDetails() == null) {
					error.getErrors().add(
							new ProposalError("nonDefaultFrameExposureTimeDetails",
									"Non Default Frame Exposure Time Details"));
				}
			}
			
			//VL16
			
			
		}
		return error;
	}	

		
    public ProposalError validate(Target target)
    {
   	List errors = new ArrayList();
    	OpticonTarget opticonTarget = (OpticonTarget) target;
        FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
    	
		Double totalDuration = NorthStarDelegate.getResourceValue(
				opticonTarget.getAllocations(),
				OpticonConstants.TOTAL_DURATION);
		// if exposure time is not visible, then it cannot be validated.
		if (OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_EXPOSURE_TIME,
				fieldsDefinitionType)){
	        if (opticonTarget.isFirstInRun() && totalDuration == null){
	            errors.add(new ProposalError("totalDuration", "Exposure time"));
	        }
		}
        
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SN, fieldsDefinitionType)){
			if (opticonTarget.getSn()== null){
	            errors.add(new ProposalError("selectedTargetSn", "S/N"));
				
			}
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_MAGNITUDE,
				fieldsDefinitionType)){
	        if (opticonTarget.getMagnitude()== null){
	            errors.add(new ProposalError("selectedTargetMagnitude", "Magnitude"));
	        }
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SEEING_RANGE,
						fieldsDefinitionType)){
			if (opticonTarget.getSeeingLower()== null || opticonTarget.getSeeingLower() == null){
	            errors.add(new ProposalError("targetSeeingUpper", "Seeing"));
				
			}
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_FLUX, fieldsDefinitionType))
		{
			OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX,
					fieldsDefinitionType);
			
			if (opticonTarget.getFlux() == null)
			{
				errors.add(new ProposalError("targetFlux", "Flux"));

			}
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_FLUX_FREQUENCY, fieldsDefinitionType))
		{
			OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX_FREQUENCY,
					fieldsDefinitionType);
			
			if (opticonTarget.getFluxFrequency() == null)
			{
				errors.add(new ProposalError("targetFluxFrequency", "Flux reference frequency"));

			}
		}
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SPECTRAL_INDEX, fieldsDefinitionType))
		{
			OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECTRAL_INDEX,
					fieldsDefinitionType);
			
			if (opticonTarget.getSpectralIndex() == null)
			{
				errors.add(new ProposalError("targetSpectralIndex", "Spectral index"));

			}
		}
		
		//VL20 target should have a observation connected
		if (opticonTarget.getObservingrun() == null || opticonTarget.getObservingrun().length() ==0)
		{
			errors.add(new ProposalError("targets", "Target has no observation"));

		}
		
		
		
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_SKY_QUALITIES, fieldsDefinitionType)){
			if (AstronValidator.isBlankOrNull(opticonTarget.getSkyQuality())){
				errors.add(new ProposalError("selectedTargetSkyQuality", "Sky quality"));

			}
		}
		ProposalError error = new ProposalError("Target", "Target ("
                + target.getFieldName() + ")");
		if (OptionsDelegate.isRequired(OpticonConstants.TARGET_RA, fieldsDefinitionType)){
			error = super.validate(target);
		}
		 if (AstronValidator.isBlankOrNull(target.getFieldName())) {
	            error.setDescription("Target ()");
	        } else {
	            error.setDescription(" Target (" + target.getFieldName() + ")");
	        }
        error.getErrors().addAll(0, errors);        
        return error;       
  
    }
    
    public int validateChoiceOfStation(Justification justification, OpticonInstrument opticonInstrument) {
    	if(justification.isInternationalStation()) {
    		if( opticonInstrument.getStation() != null){
    			if(	opticonInstrument.getStation().contains("Single") || 
    	    		opticonInstrument.getStation().contains("Superterp") ||
    	    		opticonInstrument.getStation().contains("Core") ||
    	    		opticonInstrument.getStation().contains("Dutch")) {
    	    		return 1;
    	    	} else if( 
    	    			opticonInstrument.getStation().contains("Custom") && 
            	    	opticonInstrument.getCustomStationSpecifics() != null) {
    	    			String restString = opticonInstrument.getCustomStationSpecifics()
            	    			.replaceAll("[^A-Za-z]", "")
            	   				.replaceAll("RS", "").replaceAll("rs", "")
            	   				.replaceAll("CS","").replaceAll("cs","");
            	    	if(restString.length() == 0) {
            	    		return 1;	
            	   		} else {
            	   			return 0;
            	   		}
    			} else {
    				return 0;
    			}
    		}
    	} else {
    		if(opticonInstrument.getStation() != null) {
    	   		if( 	
    	   			opticonInstrument.getStation().contains("Single") || 
    	   			opticonInstrument.getStation().contains("Superterp") ||
    	    		opticonInstrument.getStation().contains("Core") ||
    	    		opticonInstrument.getStation().contains("Dutch") ) {
    	   			return 0;
    	   		} else if( 
        	    	opticonInstrument.getStation().contains("Custom") && 
        	    	opticonInstrument.getCustomStationSpecifics() != null) {
    	   			String restString = opticonInstrument.getCustomStationSpecifics()
        	    			.replaceAll("[^A-Za-z]", "")
        	   				.replaceAll("RS", "").replaceAll("rs", "")
        	   				.replaceAll("CS","").replaceAll("cs","");
        	    	if(restString.length() == 0) {
        	    		return 0;	
        	   		} else {
        	   			return 1;
        	   		}
    	   		} else {
    	   			return 1;
    	   		}
    		}
    	}
    	return 0;
    }


}
