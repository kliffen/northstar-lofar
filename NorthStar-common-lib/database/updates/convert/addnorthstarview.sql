CREATE VIEW `privileges` AS
SELECT p.id as proposalid, r.userid  FROM proposal p, member m, registeredmember r
WHERE m.proposalid=p.id AND r.memberid=m.id;