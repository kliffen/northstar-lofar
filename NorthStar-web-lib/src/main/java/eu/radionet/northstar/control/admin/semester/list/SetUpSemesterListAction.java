// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.semester.list;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.data.entities.Category;
import eu.radionet.northstar.data.entities.Semester;

public class SetUpSemesterListAction extends Action {


	private NorthStarDelegate northStarDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		SemesterListForm semesterListForm = (SemesterListForm) form;

		Calendar currentDate = NorthStarDelegate.getCurrentDate();
		HttpSession session = request.getSession();

		northStarDelegate = NorthStarDelegate.getInstance();
		SelectionBean selectionBean = (SelectionBean)session.getAttribute(Constants.ADMIN_SEMESTER_SELECTION);
		if (selectionBean != null){
			semesterListForm.setSelectedTelescopes(selectionBean.getSelectedTelescopes());
			semesterListForm.setSelectedCategories(selectionBean.getSelectedCategories());
			semesterListForm.setSelectedCommunities(selectionBean.getSelectedCommunities());
			semesterListForm.setAfterCurrentDate(selectionBean.isAfterCurrentDate());
		}		
		List categories = northStarDelegate.getCategories();

		semesterListForm.setCategories(categories);
		if (semesterListForm.getSelectedCategories() == null) {
			String[] selectedCategories = new String[categories.size()];
			for (int i = 0; i < categories.size(); i++) {
				selectedCategories[i] = ((Category) categories.get(i))
						.getCode();
			}
			semesterListForm.setSelectedCategories(selectedCategories);
		}
		semesterListForm.setTelescopes(NorthStarConfiguration
				.getTelescopeNames());
		semesterListForm.setCommunities(NorthStarConfiguration.getCommunityConfigurations());			
		
		if (semesterListForm.getSelectedTelescopes() == null) {
			String[] selectedTelescopes = new String[semesterListForm
					.getTelescopes().size()];
			for (int i = 0; i < semesterListForm.getTelescopes().size(); i++) {
				selectedTelescopes[i] = (String) semesterListForm
						.getTelescopes().get(i);
			}
			semesterListForm.setSelectedTelescopes(selectedTelescopes);
		}
		Calendar date = null;
		if (semesterListForm.isAfterCurrentDate()) {
			date = currentDate;
		}
		List semesters = northStarDelegate.getSemesters(semesterListForm
				.getSelectedCategories(), semesterListForm
				.getSelectedTelescopes(),semesterListForm.getSelectedCommunities(), date);
		Iterator semesterIterator = semesters.iterator();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat();
		simpleDateFormat.applyPattern("dd MMM yyyy");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		SimpleDateFormat longDateFormat = new SimpleDateFormat();
		longDateFormat.applyPattern("dd MMM yyyy HH:mm:ss");
		longDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		List semesterBeans = new ArrayList();
		semesterListForm.setServerUTCTime(longDateFormat.format(currentDate
				.getTime()));
		while (semesterIterator.hasNext()) {

			Semester semester = (Semester) semesterIterator.next();
			SemesterBean semesterBean = new SemesterBean();
			semesterBean.setCategory(semester.getCategory().getCode());
			semesterBean.setId(semester.getId().toString());
			semesterBean.setPrefix(semester.getPrefix());
			semesterBean.setTelescope(semester.getTelescope());
			semesterBean.setStartTime(simpleDateFormat.format(semester
					.getStartDate()));
			semesterBean.setEndTime(simpleDateFormat.format(semester
					.getEndDate()));
			semesterBean.setDeadLineTime(longDateFormat.format(semester
					.getDeadLine()));
			semesterBean.setLastAssignedNumber(AstronConverter.toString(semester
					.getLastAssigned()));
			semesterBean.setAllowedToDelete((semester.getLastAssigned()
					.intValue() == 0));
			semesterBean.setImmediate(semester.isImmediate());
			Date temp = currentDate.getTime();
			semesterBean.setAvailable(temp.after(semester
					.getAvailableDate()) && temp.before(semester.getDeadLine())
					);
			semesterBeans.add(semesterBean);
		}
		semesterListForm.setSemesterBeans(semesterBeans);
		return mapping.findForward(Constants.SUCCESS);
	}

	protected String getCategory(List categories, Integer id)
			throws DatabaseException {
		if (id == null) {
			return northStarDelegate.getDefaultCategory().getCode();
		}
		Iterator categoriesIterator = categories.iterator();
		while (categoriesIterator.hasNext()) {
			Category category = (Category) categoriesIterator.next();
			if (category.getId().intValue() == id.intValue()) {
				return category.getCode();
			}
		}
		return null;

	}

}
