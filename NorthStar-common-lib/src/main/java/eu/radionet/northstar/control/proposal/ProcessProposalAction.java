// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal;

import javax.naming.NamingException;
import javax.servlet.http.HttpServletRequest;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.exception.AstronMailException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.exception.InvalidConfigurationException;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.ProposalError;

public abstract class ProcessProposalAction extends LockedAction {
	protected Log log = LogFactory.getLog(this.getClass());

	protected ActionForward processGeneralButtons(ProposalForm proposalForm,
			ActionMapping mapping, HttpServletRequest request,
			ActionMessages errors, ProposalData proposalData,
			UserAccount ownUserAccount) throws DatabaseException,
			AstronMailException, NamingException, InvalidConfigurationException {
		/*
		 * store proposal to the database
		 */
		Proposal proposal = proposalData.getProposal();
		/*
		 * if one of the save buttons is pressed, save the proposal
		 */
		if (proposalForm.getSaveButton() != null
				|| proposalForm.getSaveAndExitButton() != null
				|| proposalForm.getSaveAndViewButton() != null) {

			try {
				proposal = proposalDelegate.store(proposal, ownUserAccount,
						request.getSession().getId());
			} catch (DatabaseException de) {
				log.error(LogMessage.getMessage(ownUserAccount,
						"Cannot store proposal: " + de.getMessage(), de));
				errors.add(Constants.ERROR, new ActionMessage(
						"error.database.store.proposal"));
				saveErrors(request, errors);
				return mapping.findForward(Constants.REFRESH);
			}
			NorthStarUtils northStarUtils = new NorthStarUtils();
			northStarUtils.storeJustificationFiles(proposalDelegate, request,
					proposalData);

			if (proposalForm.getSaveAndExitButton() != null) {
				removeLock(request, ownUserAccount);
				return mapping.findForward(Constants.SAVE_AND_EXIT);
			} else if (proposalForm.getSaveAndViewButton() != null) {
				return mapping.findForward(Constants.SAVE_AND_VIEW);
			} else {
				return mapping.findForward(Constants.REFRESH);
			}

		} else if (proposalForm.getSaveAndSubmitButton() != null) {
			ProposalError proposalError = proposalDelegate.validate(proposal);
			/*
			 * errors occurred
			 */
			if (!proposalError.isEmpty()) {
				try {

					proposal = proposalDelegate.storeWithoutSendingMails(proposal, ownUserAccount,
							request.getSession().getId());
				} catch (DatabaseException de) {
					log.error(LogMessage.getMessage(ownUserAccount,
							"Cannot store proposal: " + de.getMessage(), de));
					errors.add(Constants.ERROR, new ActionMessage(
							"error.database.store.proposal"));
					saveErrors(request, errors);
					return mapping.findForward(Constants.REFRESH);
				}
				NorthStarUtils northStarUtils = new NorthStarUtils();
				northStarUtils.storeJustificationFiles(proposalDelegate, request,
						proposalData);
			}
			ActionForward forward =  mapping.findForward(Constants.SAVE_AND_SUBMIT);
			proposalData.setIgnoreDeadLineWarning(false);
			proposalData.setSuccessWarningForward(forward);
			ActionForward cancel = new ActionForward();
			cancel.setModule("");
			cancel.setRedirect(true);
			cancel.setPath(proposalData.getForwardUriAfterSubmit());
			proposalData.setCancelWarningForward(cancel);
			return forward;
		} else {
			return null;
		}
	}
}
