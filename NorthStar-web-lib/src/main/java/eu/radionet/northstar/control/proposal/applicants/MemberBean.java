// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * MemberBean.java 
 *
 * Created on Feb 7, 2005
 *
 * Version $Id: MemberBean.java,v 1.1 2006-05-03 10:17:19 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.applicants;

import java.io.Serializable;

import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.RegisteredMember;

/**
 * The MemberBean provides
 *
 * @author Bastiaan Verhoef
 *
 */
public class MemberBean implements Serializable{
  //  private String id = null;
  //  private String name = null;
  //  private boolean pi = false;
  //  private boolean correspondingAuthor = false;
    //private boolean potentialObserver = false;
    private Member member = null;
    private Integer id = null;
	private boolean isCurrentUser = false;
	private boolean first = false;
	private boolean last = false;
	private String activeApplicant = null;
 /*   String memberId = null;
    if (member.getId() != null) {
        memberId = member.getId().toString();
    } else {
        memberId = member.getFakeId().toString();
    }

    if (member.isPi()) {
        pi = memberId;
    }
    if (member.isCorrespondingAuthor()) {
        memberBean.setCorrespondingAuthor( member.isCorrespondingAuthor());
        correspondingAuthor = memberId;
    }
    memberBean.setId(memberId);
    memberBean.setName(member.getName());

*/
	public boolean isNonRegisteredMember(){
		return NonRegisteredMember.class.equals(member.getClass());
	}
	public boolean isRegisteredMember(){
		return RegisteredMember.class.equals(member.getClass());
	}
    /**
     * @return Returns the correspondingAuthor.
     */
    public boolean isContactAuthor() {
        return member.isContactAuthor();
    }
    /**
     * @return Returns the id.
     */
    public String getId() {
        return id.toString();
    }
    public void setId(Integer id){
        this.id=id;
    }

    /**
     * @return Returns the name.
     */
    public String getName() {
        return member.getName();
    }

    /**
     * @return Returns the affiliation.
     */
    public String getAffiliation() {
        return member.getAffiliation();
    }

    /**
     * @return Returns the email.
     */
    public String getEmail() {
        return member.getEmail();
    }

    /**
     * @return Returns the country.
     */
    public String getCountry() {
        return member.getCountry();
    }

    /**
     * @return Returns the pi.
     */
    public boolean isPi() {
        return member.isPi();
    }

    /**
     * @return Returns the potentialObserver.
     */
    public boolean isPotentialObserver() {
        return member.isPotentialObserver();
    }

    /**
     * @return Returns the member.
     */
    public Member getMember() {
        return member;
    }
    /**
     * @param member The member to set.
     */
    public void setMember(Member member) {
        this.member = member;
    }
	public boolean isCurrentUser() {
		return isCurrentUser;
	}
	public void setCurrentUser(boolean isCurrentUser) {
		this.isCurrentUser = isCurrentUser;
	}
	public boolean isFirst() {
		return first;
	}
	public void setFirst(boolean first) {
		this.first = first;
	}
	public boolean isLast() {
		return last;
	}
	public void setLast(boolean last) {
		this.last = last;
	}
	public String getActiveApplicant() {
		return activeApplicant;
	}
	public void setActiveApplicant(String activeApplicant) {
		this.activeApplicant = activeApplicant;
	}

}
