// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest.observation.targetfile;

import java.util.HashMap;

import nl.astron.database.exception.DatabaseException;
import eu.opticon.northstar.business.OpticonConstants;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.control.proposal.observingrequest.observation.targetfile.SetUpTargetFileAction;
import eu.radionet.northstar.control.proposal.observingrequest.observation.targetfile.TargetFileForm;
import eu.radionet.northstar.control.util.OptionsUtils;

/**
 * This class contains telescope specific form data of an observation
 */
public class SetUpOpticonTargetFileAction extends SetUpTargetFileAction 
{

    /**
     * telescope specific options. Extract the "example option" options 
     * from /web/conf/modules/skeleton/skeleton-options.xml
     * @see eu.radionet.northstar.control.proposal.observingrequest.SetUpObservingRequestAction#fillOptions(eu.radionet.northstar.control.proposal.observingrequest.ObservingRequestForm)
     */
    public void fillOptions(TargetFileForm targetFileForm)
        throws DatabaseException
    {
        OpticonTargetFileForm opticonTargetFileForm = (OpticonTargetFileForm) targetFileForm;

        /* set default values when they are not set */
        if (opticonTargetFileForm.getSelectedTargetMoon()== null)
        {
            opticonTargetFileForm.setSelectedTargetMoon(
    				OptionsUtils.getDefaultSelectedOptionOrValue(OpticonConstants.TARGET_MOON,
    						null,contextConfiguration));
        	
        }
        if (opticonTargetFileForm.getSelectedTargetSeeing()== null)
        {
        	opticonTargetFileForm.setSelectedTargetSeeing(
        			OptionsUtils.getDefaultSelectedOptionOrValue(OpticonConstants.TARGET_SEEING,
        					null,contextConfiguration));
        }
        if (opticonTargetFileForm.getSelectedTargetWater()== null)
        {
        	opticonTargetFileForm.setSelectedTargetWater(
				OptionsUtils.getDefaultSelectedOptionOrValue(OpticonConstants.TARGET_WATER,
	                    null,contextConfiguration));
        }
    	
        /* set the options */
		opticonTargetFileForm.setMoonOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.TARGET_MOON, new HashMap(), contextConfiguration));
		opticonTargetFileForm.setSeeingOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.TARGET_SEEING, new HashMap(), contextConfiguration));
		opticonTargetFileForm.setWaterOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.TARGET_WATER, new HashMap(), contextConfiguration));
		opticonTargetFileForm.setSkyQualities(OptionsUtils.getLabelValueBeans(
        		OpticonConstants.TARGET_SKY_QUALITIES, new HashMap(), contextConfiguration));
		
       FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
       opticonTargetFileForm.setDisplaySeeingOptions(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING,
						fieldsDefinitionType));	
       opticonTargetFileForm.setDisplaySeeingRange(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING_RANGE,
						fieldsDefinitionType));	
       opticonTargetFileForm.setDisplayTargetFlux(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX,
						fieldsDefinitionType));	
       opticonTargetFileForm.setDisplayTargetFluxFrequency(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX_FREQUENCY,
						fieldsDefinitionType));	
       opticonTargetFileForm.setDisplayTargetSpectralIndex(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECTRAL_INDEX,
						fieldsDefinitionType));	
       opticonTargetFileForm.setDisplayTargetSkyQuality(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SKY_QUALITIES,
						fieldsDefinitionType));	

    
       opticonTargetFileForm.setRequiredSeeingRange(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_SEEING_RANGE,
						fieldsDefinitionType));	
       opticonTargetFileForm.setRequiredTargetFlux(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_FLUX,
						fieldsDefinitionType));	
       opticonTargetFileForm.setRequiredTargetFluxFrequency(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_FLUX_FREQUENCY,
						fieldsDefinitionType));	
       opticonTargetFileForm.setRequiredTargetSpectralIndex(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_SPECTRAL_INDEX,
						fieldsDefinitionType));	
       opticonTargetFileForm.setRequiredTargetSkyQuality(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_SKY_QUALITIES,
						fieldsDefinitionType));	
       opticonTargetFileForm.setRequiredSn(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_SN,
						fieldsDefinitionType));	    
    
    }
    
    
}

