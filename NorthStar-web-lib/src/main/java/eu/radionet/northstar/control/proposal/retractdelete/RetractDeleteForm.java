// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * RetractDeleteForm.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: RetractDeleteForm.java,v 1.1 2006-05-03 10:17:21 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.retractdelete;

import org.apache.struts.action.ActionForm;

/**
 * The RetractDeleteForm provides
 *
 * @author Nico Vermaas
 *
 */
public class RetractDeleteForm extends ActionForm {
    private String proposalCode = null;
    private String proposalTitle = null;
    private String proposalCategory = null;
    private String telescope = null;
    private String deleteButton = null;
    private String retractButton = null;
    /**
     * @return Returns the proposalCode.
     */
    public String getProposalCode() {
        return proposalCode;
    }
    /**
     * @param proposalCode The proposalCode to set.
     */
    public void setProposalCode(String proposalCode) {
        this.proposalCode = proposalCode;
    }
    /**
     * @return Returns the proposalTitle.
     */
    public String getProposalTitle() {
        return proposalTitle;
    }
    /**
     * @param proposalTitle The proposalTitle to set.
     */
    public void setProposalTitle(String proposalTitle) {
        this.proposalTitle = proposalTitle;
    }
    /**
     * @return Returns the deleteButton.
     */
    public String getDeleteButton() {
        return deleteButton;
    }
    /**
     * @param deleteButton The deleteButton to set.
     */
    public void setDeleteButton(String deleteButton) {
        this.deleteButton = deleteButton;
    }
    /**
     * @return Returns the retractButton.
     */
    public String getRetractButton() {
        return retractButton;
    }
    /**
     * @param retractButton The retractButton to set.
     */
    public void setRetractButton(String retractButton) {
        this.retractButton = retractButton;
    }
    /**
     * @return Returns the proposalCategory.
     */
    public String getProposalCategory() {
        return proposalCategory;
    }
    /**
     * @param proposalCategory The proposalCategory to set.
     */
    public void setProposalCategory(String proposalCategory) {
        this.proposalCategory = proposalCategory;
    }
    /**
     * @return Returns the telescope.
     */
    public String getTelescope() {
        return telescope;
    }
    /**
     * @param telescope The telescope to set.
     */
    public void setTelescope(String telescope) {
        this.telescope = telescope;
    }
}
