// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.admin.telescope;

import java.util.ArrayList;
import java.util.List;
import nl.astron.useradministration.data.entities.User;

import org.apache.struts.action.ActionForm;

public class TelescopeAccessForm extends ActionForm {
	private List telescopeNameList = new ArrayList();
	private List userNameList = new ArrayList();
	private String selectedTelescope = null;
	private String selectedUser = null;
	private String operation = null;
	private boolean hasMultipleAccess = false;
	
	public List getTelescopeNameList() {
		return telescopeNameList;
	}
	
	public void settelescopeNameList(List inputList) {
        this.telescopeNameList = inputList;
    }
	
	public List getUserNameList() {
		return userNameList;
	}
	
	public void setUserNameList(List inputList) {
        this.userNameList = inputList;
    }
	
	public void setSelectedTelescope(String selectedTelescopeLabel) {
        this.selectedTelescope = selectedTelescopeLabel;
    }
	
	 public String getSelectedTelescope() {
	        return selectedTelescope;
	    }
	 
	 public void setSelectedUser(String selectedUserLabel) {
	        this.selectedUser = selectedUserLabel;
	    }
		
	 public String getSelectedUser() {
	        return selectedUser;
	    }
	 
	 public void setOperation(String selectedOperation) {
	        this.operation = selectedOperation;
	    }
		
	 public String getOperation() {
	        return operation;
	    }
		 
	 public void setHasMultipleAccess(boolean inp){
		 this.hasMultipleAccess = inp;
	 }
	 
	 public boolean getHasMultipleAccess(){
		 return hasMultipleAccess;
	 }
	
}
