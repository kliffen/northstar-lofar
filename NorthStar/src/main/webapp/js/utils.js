

function selectOrUnselectAll(selectedCheckbox) {
	checked = selectedCheckbox.checked;
	checkboxValue = selectedCheckbox.value;
	with(document.forms[0]) {
		for(i=0;i<elements.length;i++) {
			thiselm = elements[i];
			if(thiselm.type == "checkbox"){
					thiselm.checked = checked;
					//thiselm.visible = checked; //testing
			}
		}
	}
}

function getCoordinates(array) {
	for (var i = 0; i < array.length;i++){
		var elem = array[i];
		var elemArray = elem.split("=");
		if (elemArray[0] == "coordinates"){
			return elemArray[1];
		}
	}
	return null;

}

function scrollToCoordinates(position) {
	var value = position;
	var array=value.split("_");
   window.scrollTo(array[0], array[1]);

}

function CountWords (this_field, writefield) {
	var fullStr = this_field.value + " ";
	var initial_whitespace_rExp = /^[^A-Za-z0-9]+/gi;
	var left_trimmedStr = fullStr.replace(initial_whitespace_rExp, "");
	var non_alphanumerics_rExp = rExp = /[^A-Za-z0-9]+/gi;
	var cleanedStr = left_trimmedStr.replace(non_alphanumerics_rExp, " ");
	var splitString = cleanedStr.split(" ");
	var word_count = splitString.length-1;
	writefield.value = word_count;
	return word_count;
}

function CountWordsRed (this_field, writefield, max_words) {
	var fullStr = this_field.value + " ";
	var initial_whitespace_rExp = /^[^A-Za-z0-9]+/gi;
	var left_trimmedStr = fullStr.replace(initial_whitespace_rExp, "");
	var non_alphanumerics_rExp = rExp = /[^A-Za-z0-9]+/gi;
	var cleanedStr = left_trimmedStr.replace(non_alphanumerics_rExp, " ");
	var splitString = cleanedStr.split(" ");
	var word_count = splitString.length-1;
	writefield.value = word_count;
	if(word_count > max_words ){
		
		this_field.style.color='red';
	} else {
		this_field.style.color='blue'
	}
	return word_count;
}



function SumupRingsAndBeams(beams, rings, outputfield) {
	var result = "";
	var ringsValue = parseInt(rings.value);
	var beamsValue = parseInt(beams.value);
	if(!isNaN(ringsValue) && !(isNaN(beamsValue))){
		if(ringsValue != 0 && beamsValue !=0){
			result = (3*(ringsValue*ringsValue + ringsValue) + 1 + beamsValue).toString();
		} else if(ringsValue != 0){
			result = (3*(ringsValue*ringsValue + ringsValue) + 1).toString();
		} else if(beamsValue != 0){
			result = beamsValue.toString();
		}
	} else if(ringsValue != 0){
		result = (3*(ringsValue*ringsValue + ringsValue) + 1).toString();
	} else if(beamsValue != 0){
		result = beamsValue.toString();
	} else {
		result = "Atleast one of the fields 'Coherent tied array beams' and 'tied array rings' should be none empty";
	}
	outputfield.value = result;
	return result;
}

function CountChars (this_field, writefield, max_chars) {
	var char_count = this_field.value.length;
	writefield.value = char_count;
	if(char_count > max_chars ){
		
		this_field.style.color='red';
	} else {
		this_field.style.color='blue'
	}
	return char_count;
}

function CountRows (this_field, max_rows, row_length){
	var rtext = this_field.value;
	var rsplit = rtext.split("\n");
	var rcount = rsplit.length;
	// check for line overflows:
	for (skey in rsplit){
		   rcount += Math.floor(rsplit[skey].length / row_length);
	}
	// for debugging show rcount
	//document.forms[0].previousAllocationsSpecifics.value=rcount;
	if (rcount > max_rows){ // if too long show it in red.
			this_field.style.color='red';
	}
	else{
		this_field.style.color=document.body.style.color;
	}
}

var closetimer	= 0;
var ddmenuitem	= 0;

// open hidden layer
function boxopen(id)
{	
	// close old layer
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';

	// get new layer and show it
	ddmenuitem = document.getElementById(id);
	ddmenuitem.style.visibility = 'visible';

}
// close showed layer
function mclose()
{
	if(ddmenuitem) ddmenuitem.style.visibility = 'hidden';
}

// go close timer
function boxclosetime(id)
{
	ddmenuitem = document.getElementById(id);
	closetimer = window.setTimeout(mclose, 200);
}

//addEvent function by John Resig:
//http://ejohn.org/projects/flexible-javascript-events/
function addEvent( obj, type, fn ) {
if ( obj.attachEvent ) {
 obj['e'+type+fn] = fn;
 obj[type+fn] = function(){obj['e'+type+fn]( window.event );}
 obj.attachEvent( 'on'+type, obj[type+fn] );
} else
 obj.addEventListener( type, fn, false );
}



