// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.configuration.options.config;

import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;


public class ConfigureOptionType {
	protected String value = null;
	protected String label = null;
	protected FieldsDefinitionType subfields = null;
	protected DependenciesType dependenciesConfiguration = null;;

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public DependenciesType getDependenciesConfiguration() {
		return dependenciesConfiguration;
	}
	public void setDependenciesConfiguration(
			DependenciesType dependenciesConfiguration) {
		this.dependenciesConfiguration = dependenciesConfiguration;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public FieldsDefinitionType getSubfields() {
		return subfields;
	}
	public void setSubfields(FieldsDefinitionType subfields) {
		this.subfields = subfields;
	}
}
