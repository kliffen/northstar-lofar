// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProcessWarningAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: ProcessWarningAction.java,v 1.2 2006-06-08 07:01:05 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.observingrequest.observationfile.warning;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.Proposal;

/**
 * The ProcessWarningAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class ProcessWarningAction extends LockedAction {
	private Log log = LogFactory.getLog(this.getClass());

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {

		HttpSession session = request.getSession();

		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
		/*
		 * retrieve proposal data from session with information
		 */
		ProposalData proposalData = (ProposalData) session
				.getAttribute(Constants.PROPOSAL_DATA);
		Proposal proposal = proposalData.getProposal();
		List observations = (List) session.getAttribute(Constants.OBSERVATIONS);
		session.removeAttribute(Constants.OBSERVATIONS);
		session.removeAttribute(Constants.OBSERVATION);
		/*
		 * if not pressed on cancel, it must be an submit
		 */
		if (isCancelled(request)) {
			return mapping.findForward(Constants.CANCEL);

		}

		proposalDelegate.addObservations(proposal, observations);
		proposalData.setProposal(proposal);
		session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
		return mapping.findForward(Constants.SUCCESS);

	}

}