// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SetUpProposalAction.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: SetUpProposalListAction.java,v 1.5 2006-08-04 09:43:18 holties Exp $
 *
 */
package eu.radionet.northstar.control.proposal.list;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.SystemRole;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronConverter;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.configuration.NorthStarConfiguration;
import eu.radionet.northstar.business.configuration.Telescope;
import eu.radionet.northstar.business.configuration.TelescopeConfiguration;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.RegisteredMember;
import eu.radionet.northstar.data.entities.Semester;
import eu.radionet.northstar.data.entities.collaboration.Invitation;

/**
 * The SetUpProposalAction provides
 * 
 * @author Bastiaan Verhoef
 */
public class SetUpProposalListAction extends Action {

	private NorthStarDelegate northStarDelegate = null;

	private CollaborationDelegate collaborationDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ProposalListForm proposalListForm = (ProposalListForm) form;

		HttpSession session = request.getSession();

		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);

		northStarDelegate = NorthStarDelegate.getInstance();
		collaborationDelegate = CollaborationDelegate.getInstance();
		proposalListForm.setTelescopes(NorthStarConfiguration
				.getTelescopeNames());
		SelectionBean selectionBean = null;
		if (!AstronValidator.isBlankOrNull(proposalListForm.getTelescope())) {
			String[] selectedTelescopes = new String[] { proposalListForm.getTelescope() };
			selectionBean = new SelectionBean();
			selectionBean.setSelectedTelescopes(selectedTelescopes);
			selectionBean.setShowReviewedProposals(proposalListForm.isShowReviewedProposals());
			session.setAttribute(Constants.OWN_PROPOSAL_SELECTION,
					selectionBean);
		} else {
			selectionBean = (SelectionBean) session
					.getAttribute(Constants.OWN_PROPOSAL_SELECTION);
		}
		
		// check reviewer privileges
		List roles = ownUserAccount.getSystemRoles();
		if(roles != null && roles.size() > 1){
			Iterator roleit =  ownUserAccount.getSystemRoles().iterator();
			while(roleit.hasNext()){
				SystemRole role = (SystemRole) roleit.next();
				if(role.getName().equalsIgnoreCase("reviewer") ){
					proposalListForm.setHasReviewerPrivileges(true);
				}
			}
		}
		// check for pending invitations
		//ownUserAccount.getUser().getEmail();
		
		if (selectionBean != null) {
			proposalListForm.setSelectedTelescopes(selectionBean.getSelectedTelescopes());
			proposalListForm.setShowReviewedProposals(selectionBean.isShowReviewedProposals());
		}
		List acceptedInvitations = collaborationDelegate
				.getAcceptedInvitations(ownUserAccount);
		String sessionId = session.getId();
		Iterator acceptedInvitationsIterator = acceptedInvitations.iterator();
		while (acceptedInvitationsIterator.hasNext()) {
			Invitation invitation = (Invitation) acceptedInvitationsIterator
					.next();
			if (!collaborationDelegate.isLocked(invitation.getProposalId())) {
				collaborationDelegate.lockProposal(invitation.getProposalId(),
						ownUserAccount, sessionId);
				collaborationDelegate.unlockProposal(
						invitation.getProposalId(), ownUserAccount, sessionId);

			}

		}
		/*
		 * TODO: accepted invitations
		 */
		int ownProposalCount = 0;
		int telescopeCount = 0;
		Map proposalsMap = new HashMap();
		Iterator telIt = proposalListForm.getTelescopes().iterator();
		while (telIt.hasNext()){
			String telescopeName = (String) telIt.next();
			List proposals = northStarDelegate.getOwnProposals(
                    proposalListForm.isShowReviewedProposals(), telescopeName,
                    ownUserAccount);
			if(proposals != null && proposals.size() >0){
				ownProposalCount +=  proposals.size();
				telescopeCount++;
				proposalsMap.put(telescopeName, proposals);
			}
			
		}
		
		if(ownProposalCount > 7 && telescopeCount > 1){
			proposalListForm.setEnableSelectTelescope(true);
			if (proposalListForm.getSelectedTelescopes() != null) {
				for (int i = 0; i < proposalListForm.getSelectedTelescopes().length; i++) {
					String telescope = proposalListForm.getSelectedTelescopes()[i];
					Telescope telescopeConfig = NorthStarConfiguration
							.getTelescope(telescope);
	                if (telescopeConfig!=null) {
	                    /*
	                     * Only add the selected telescope if there is a corresponding configuration
	                     * This may not be the case if the configuration has been reloaded during the session
	                     */
	                    String description = telescopeConfig.getDescription();
	                   /* List proposals = northStarDelegate.getOwnProposals(
	                            proposalListForm.isShowReviewedProposals(), telescope,
	                            ownUserAccount);
	                   */
	                    List proposals = (List) proposalsMap.get(telescope);
	                    if(proposals != null){
		                    TelescopeBean telescopeBean = fillTelescopeBean(telescope, description, proposals,
		                            ownUserAccount);
		                    if (telescopeBean.getProposalBeans().size()>0){
		                        proposalListForm.getTelescopeBeans().add(telescopeBean);
		                    }
	                    }
	                }
				}
			}
		}else{
			proposalListForm.setEnableSelectTelescope(false);
			telIt = proposalListForm.getTelescopes().iterator();
			while (telIt.hasNext()){
				String telescope = (String) telIt.next();
				Telescope telescopeConfig = NorthStarConfiguration.getTelescope(telescope);
				if (telescopeConfig!=null) {
					String description = telescopeConfig.getDescription();
					List proposals = (List) proposalsMap.get(telescope);
					if(proposals != null){
			            TelescopeBean telescopeBean = fillTelescopeBean(telescope, description, proposals,
			                    ownUserAccount);
			            if (telescopeBean.getProposalBeans().size()>0){
			                proposalListForm.getTelescopeBeans().add(telescopeBean);
			            }
					}
				}
			}
		}


		proposalListForm.setBannerImageUri(NorthStarConfiguration
				.getBannerImageUri());
		proposalListForm.setWelcomeMessage(NorthStarConfiguration
				.getWelcomeMessage());
		proposalListForm.setWelcomeMessageUri(NorthStarConfiguration
				.getWelcomeUri());
		ProposalData proposalData = (ProposalData) session
				.getAttribute(Constants.PROPOSAL_DATA);
		proposalListForm.setProposalDataOnSession(proposalData != null);
		return mapping.findForward("showProposalList");

	}

	private int countTelescopes(List proposals) {
		java.util.Set telescopes = new HashSet();
		Iterator proposalIterator = proposals.iterator();
		while (proposalIterator.hasNext()) {
			Proposal proposal = (Proposal) proposalIterator.next();
			//String telescope = proposal.getSemester().getTelescope();		
			telescopes.add(proposal.getSemester().getTelescope());
		}
		return telescopes.size();
	}

	private TelescopeBean fillTelescopeBean(String telescope,
			String description, List proposals, UserAccount ownUserAccount)
			throws DatabaseException {
		TelescopeBean telescopeBean = new TelescopeBean();
		telescopeBean.setTelescope(telescope);
		telescopeBean.setDescription(description);
		Iterator proposalIterator = proposals.iterator();
		List currentSemester =northStarDelegate.getAvailableSemesters(telescope);
		while (proposalIterator.hasNext()) {
			Proposal proposal = (Proposal) proposalIterator.next();
			ProposalBean proposalBean = new ProposalBean();
			proposalBean.setId(proposal.getId().toString());
			proposalBean.setCode(proposal.getCode());
			proposalBean.setLocked(collaborationDelegate.isLocked(proposal
					.getId()));
			if(!northStarDelegate.isDeadLinePassed(proposal)){
				proposalBean.setAvailable(true);
			}
			else
				proposalBean.setAvailable(false);
			Iterator members = proposal.getMembers().iterator();
			/*
			 * if proposal has members
			 */
			while (members.hasNext()) {
				Member member = (Member) members.next();
				/*
				 * if member is an registered member
				 */
				
				
				
				
				if(member!=null){
				if ( member.getClass()!=null && member.getClass().equals(RegisteredMember.class)) {
					RegisteredMember registeredMember = (RegisteredMember) member;
					/*
					 * if registeredMember is registered to ownUser
					 */
					if (registeredMember.getUserId().intValue() == ownUserAccount
							.getUser().getId().intValue()) {
						proposalBean.setContactAuthor(registeredMember
								.isContactAuthor());

					}
					if (member.isPi()) {
						proposalBean.setPiName(registeredMember.getUser()
								.getLastName());

					}
				} else if (member!=null && member.isPi()) {
					proposalBean.setPiName(member.getName());

				}
				
				}
			}
			TelescopeConfiguration telescopeConfiguration = NorthStarConfiguration
					.getTelescopeConfiguration(proposal);
			if (telescopeConfiguration != null) {
				proposalBean.setCategory(proposal.getSemester().getCategory()
						.getCode());
				String community = proposal.getSemester().getCommunity();
				proposalBean.setCommunity(NorthStarConfiguration
						.getCommunityDescription(community));
				proposalBean.setStatus(proposal.getCurrentStatus().getStatus()
						.getCode());

				SimpleDateFormat simpleDateFormat = new SimpleDateFormat(
						Constants.DATETIME_FORMAT);
				Calendar UTCCal = new GregorianCalendar(TimeZone
						.getTimeZone("UTC"));
				UTCCal.setTime(proposal.getCurrentStatus().getStatusTime());
				simpleDateFormat.setCalendar(UTCCal);

				proposalBean.setStatusTime(simpleDateFormat.format(UTCCal
						.getTime()));
				if (proposal.getJustification() != null) {
					proposalBean.setTitle(AstronConverter.resize(proposal
							.getJustification().getTitle(), 30));
				}

				/*
				 * if status is in preparation
				 */
				if (northStarDelegate.isStatusInPreparation(proposal)) {
					proposalBean.setCanSubmitOrRetract("Submit");

				}
				/*
				 * if status is submitted
				 */
				else if (northStarDelegate.isStatusSubmitted(proposal)) {
					/*
					 * if deadline is passed and it is never been under review,
					 * no retraction is possible if immediate no retraction is
					 * possible
					 */
					if ((northStarDelegate.isDeadLinePassed(proposal) && !northStarDelegate
							.hasUnderReviewStatus(proposal))
							|| northStarDelegate.isImmediate(proposal)) {
						proposalBean.setCanSubmitOrRetract("");
					} else {
						proposalBean.setCanSubmitOrRetract("Retract");
					}

				} else {
					proposalBean.setCanSubmitOrRetract("");
				}

				telescopeBean.getProposalBeans().add(proposalBean);
			}
		}
		return telescopeBean;
	}




}
