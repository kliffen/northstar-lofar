// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest.observation.targetfile;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

import eu.radionet.northstar.control.proposal.observingrequest.observation.targetfile.TargetFileForm;

public class OpticonTargetFileForm extends TargetFileForm 
{

    protected String targetExposureTime = null;

    protected String selectedTargetMoon = "bright";
    protected String selectedTargetSeeing = ">0.9";
    protected String selectedTargetWater = "don't care";     
    protected String selectedTargetSn = null;

    protected String bundle= "ol";
	protected boolean requiredSn = false;

    protected String selectedTargetMagnitude = null;
    protected String selectedTargetComments = null;
    
	protected List moonOptions = new ArrayList();
	protected boolean displaySeeingOptions = false;
	protected List seeingOptions = new ArrayList();
	

	
	protected boolean displaySeeingRange = false;
	protected boolean requiredSeeingRange = false;
	protected String targetSeeingLower = null;
	protected String targetSeeingUpper = null;
	
	protected boolean displayTargetFlux = false;
	protected boolean requiredTargetFlux = false;
	protected String targetFlux = null;
	
	protected boolean displayTargetFluxFrequency = false;
	protected boolean requiredTargetFluxFrequency = false;
	protected String targetFluxFrequency = null;
	
	protected boolean displayTargetSpectralIndex = false;
	protected boolean requiredTargetSpectralIndex= false;
	protected String targetSpectralIndex = null;
	
	protected boolean displayTargetSkyQuality = false;
	protected boolean requiredTargetSkyQuality = false;
    protected String selectedTargetSkyQuality = null;	

	protected List skyQualities = new ArrayList();

	protected List waterOptions = new ArrayList();
    
	
	
    
    public void reset()
    {
        targetExposureTime = null;
        selectedTargetMoon = "bright";
	    selectedTargetSeeing = ">0.9";
	    selectedTargetWater = "don't care";     
	    selectedTargetSn = null;
	    selectedTargetMagnitude = null;
	    selectedTargetComments = null;
	    
		displaySeeingOptions = false;

		requiredSn = false;

		
		displaySeeingRange = false;
		requiredSeeingRange = false;
		targetSeeingLower = null;
		targetSeeingUpper = null;
		
		displayTargetFlux = false;
		requiredTargetFlux = false;
		targetFlux = null;		

		displayTargetFluxFrequency = false;
		requiredTargetFluxFrequency = false;
		targetFluxFrequency = null;		

		displayTargetSpectralIndex = false;
		requiredTargetSpectralIndex = false;
		targetSpectralIndex = null;		

		displayTargetSkyQuality = false;
		requiredTargetSkyQuality = false;
	    selectedTargetSkyQuality = null;	

        super.reset();
    }

    public String getTargetExposureTime() {
        return targetExposureTime;
    }

    public void setTargetExposureTime(String targetExposureTime) {
        this.targetExposureTime = targetExposureTime;
    }
    
    
 
    public ActionErrors validate(ActionMapping mapping,
            HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        errors.add(super.validate(mapping, request));
        if (!AstronValidator.isBlankOrNull(targetExposureTime)) {
            if (!AstronValidator.isPositiveInt(targetExposureTime)) {
                errors.add("totalDuration", new ActionMessage(
                        "error.noint.positive"));
            }

        }
        return errors;
    }

	public List getMoonOptions()
	{
		return moonOptions;
	}

	public void setMoonOptions(List moonOptions)
	{
		this.moonOptions = moonOptions;
	}

	public List getSeeingOptions()
	{
		return seeingOptions;
	}

	public void setSeeingOptions(List seeingOptions)
	{
		this.seeingOptions = seeingOptions;
	}

	public String getSelectedTargetMoon()
	{
		return selectedTargetMoon;
	}

	public void setSelectedTargetMoon(String selectedTargetMoon)
	{
		this.selectedTargetMoon = selectedTargetMoon;
	}

	public String getSelectedTargetSeeing()
	{
		return selectedTargetSeeing;
	}

	public void setSelectedTargetSeeing(String selectedTargetSeeing)
	{
		this.selectedTargetSeeing = selectedTargetSeeing;
	}

	public String getSelectedTargetWater()
	{
		return selectedTargetWater;
	}

	public void setSelectedTargetWater(String selectedTargetWater)
	{
		this.selectedTargetWater = selectedTargetWater;
	}

	public List getWaterOptions()
	{
		return waterOptions;
	}

	public void setWaterOptions(List waterOptions)
	{
		this.waterOptions = waterOptions;
	}

	public String getSelectedTargetComments()
	{
		return selectedTargetComments;
	}

	public void setSelectedTargetComments(String selectedTargetComments)
	{
		this.selectedTargetComments = selectedTargetComments;
	}

	public String getSelectedTargetMagnitude()
	{
		return selectedTargetMagnitude;
	}

	public void setSelectedTargetMagnitude(String selectedTargetMagnitude)
	{
		this.selectedTargetMagnitude = selectedTargetMagnitude;
	}

	public String getSelectedTargetSn()
	{
		return selectedTargetSn;
	}

	public void setSelectedTargetSn(String selectedTargetSn)
	{
		this.selectedTargetSn = selectedTargetSn;
	}

	public boolean isDisplaySeeingOptions()
	{
		return displaySeeingOptions;
	}

	public void setDisplaySeeingOptions(boolean displaySeeingOptions)
	{
		this.displaySeeingOptions = displaySeeingOptions;
	}

	public boolean isDisplaySeeingRange()
	{
		return displaySeeingRange;
	}

	public void setDisplaySeeingRange(boolean displaySeeingRange)
	{
		this.displaySeeingRange = displaySeeingRange;
	}

	public boolean isDisplayTargetFlux()
	{
		return displayTargetFlux;
	}

	public void setDisplayTargetFlux(boolean displayTargetFlux)
	{
		this.displayTargetFlux = displayTargetFlux;
	}

	public boolean isDisplayTargetFluxFrequency()
	{
		return displayTargetFluxFrequency;
	}

	public void setDisplayTargetFluxFrequency(boolean displayTargetFluxFrequency)
	{
		this.displayTargetFluxFrequency = displayTargetFluxFrequency;
	}

	public boolean isDisplayTargetSpectralIndex()
	{
		return displayTargetSpectralIndex;
	}

	public void setDisplayTargetSpectralIndex(boolean displayTargetSpectralIndex)
	{
		this.displayTargetSpectralIndex = displayTargetSpectralIndex;
	}

	public boolean isRequiredSeeingRange()
	{
		return requiredSeeingRange;
	}

	public void setRequiredSeeingRange(boolean requiredSeeingRange)
	{
		this.requiredSeeingRange = requiredSeeingRange;
	}

	public boolean isRequiredTargetFlux()
	{
		return requiredTargetFlux;
	}

	public void setRequiredTargetFlux(boolean requiredTargetFlux)
	{
		this.requiredTargetFlux = requiredTargetFlux;
	}

	public String getTargetFlux()
	{
		return targetFlux;
	}

	public void setTargetFlux(String targetFlux)
	{
		this.targetFlux = targetFlux;
	}

	public boolean isRequiredTargetFluxFrequency()
	{
		return requiredTargetFluxFrequency;
	}

	public void setRequiredTargetFluxFrequency(boolean requiredTargetFluxFrequency)
	{
		this.requiredTargetFluxFrequency = requiredTargetFluxFrequency;
	}

	public String getTargetFluxFrequency()
	{
		return targetFluxFrequency;
	}

	public void setTargetFluxFrequency(String targetFluxFrequency)
	{
		this.targetFluxFrequency = targetFluxFrequency;
	}

	public boolean isRequiredTargetSpectralIndex()
	{
		return requiredTargetSpectralIndex;
	}

	public void setRequiredTargetSpectralIndex(boolean requiredTargetSpectralIndex)
	{
		this.requiredTargetSpectralIndex = requiredTargetSpectralIndex;
	}

	public String getTargetSpectralIndex()
	{
		return targetSpectralIndex;
	}

	public void setTargetSpectralIndex(String targetSpectralIndex)
	{
		this.targetSpectralIndex = targetSpectralIndex;
	}

	public String getTargetSeeingLower()
	{
		return targetSeeingLower;
	}

	public void setTargetSeeingLower(String targetSeeingLower)
	{
		this.targetSeeingLower = targetSeeingLower;
	}

	public String getTargetSeeingUpper()
	{
		return targetSeeingUpper;
	}

	public void setTargetSeeingUpper(String targetSeeingUpper)
	{
		this.targetSeeingUpper = targetSeeingUpper;
	}

	public boolean isDisplayTargetSkyQuality()
	{
		return displayTargetSkyQuality;
	}

	public void setDisplayTargetSkyQuality(boolean displayTargetSkyQuality)
	{
		this.displayTargetSkyQuality = displayTargetSkyQuality;
	}

	public boolean isRequiredTargetSkyQuality()
	{
		return requiredTargetSkyQuality;
	}

	public void setRequiredTargetSkyQuality(boolean requiredTargetSkyQuality)
	{
		this.requiredTargetSkyQuality = requiredTargetSkyQuality;
	}

	public String getSelectedTargetSkyQuality()
	{
		return selectedTargetSkyQuality;
	}

	public void setSelectedTargetSkyQuality(String selectedTargetSkyQuality)
	{
		this.selectedTargetSkyQuality = selectedTargetSkyQuality;
	}

	public List getSkyQualities()
	{
		return skyQualities;
	}

	public void setSkyQualities(List skyQualities)
	{
		this.skyQualities = skyQualities;
	}

	public boolean isRequiredSn()
	{
		return requiredSn;
	}

	public void setRequiredSn(boolean requiredSn)
	{
		this.requiredSn = requiredSn;
	}

	public String getBundle() {
		return bundle;
	}

	public void setBundle(String bundle) {
		this.bundle = bundle;
	}
	
}