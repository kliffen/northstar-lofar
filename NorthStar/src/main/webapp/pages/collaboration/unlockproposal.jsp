<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<tiles:get name="errors" />
<html:form action="/processUnlockProposal" method="POST">

<TABLE>
	<TR>
		<TD>&nbsp;<html:hidden name = "lockProposalForm" property="proposalId"/><html:hidden name="lockProposalForm" property="forwardUri"/></TD>
	</TR>
	<TR>
		<TD class="right">Title:</TD>
		<TD class="bold" align="left"><bean:write name="lockProposalForm" property="title"/></TD>
	</TR>
	<TR>
		<TD class="right">Locked by:</TD>
		<TD class="bold" align="left"><bean:write name="lockProposalForm" property="lockOwner"/></TD>
	</TR>
	<TR>
		<TD class="right">Locked since:</TD>
		<TD class="bold" align="left"><bean:write name="lockProposalForm" property="since"/></TD>
	</TR>
	<TR>
		<TD class="right">Last action:</TD>
		<TD class="bold" align="left"><bean:write name="lockProposalForm" property="lastAction"/></TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>		
	<TR>
		<TD colspan="2">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
		<TABLE width="100%">
			<TR>
				<TD><html:img page="/images/list/alert.gif" module=''/></TD>
				<TD valign='top'><PRE>
 Are you really sure you want to override the Edit lock on this proposal ?
 Changes made by the user/session listed above since the previous Save will be lost !

 It may be more advisable to contact the other user first.</PRE>
				</TD>
			</TR>
		</TABLE>
		<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
		</TD>
	</TR>
	<TR>
		<TD>&nbsp;</TD>
	</TR>
	</TABLE>
	
<table width="100%">
	<TR>
		<TD class="left"><html:submit property="okButton" styleClass="list_accept_yes" title="Ok">Ok</html:submit></td>
		<td  class="right"><html:cancel styleClass="list_decline_t" title="Cancel">Cancel</html:cancel></TD>
	</TR>
</TABLE>
</html:form>