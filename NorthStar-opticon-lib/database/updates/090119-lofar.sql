ALTER TABLE `opticonobservingrequest`
ADD `data_reduction` tinyint(1) NOT NULL default '0',
ADD `data_reduction_specifics` text;

ALTER TABLE `opticoninstrument` 
ADD  `station` varchar(255) default NULL,
ADD  `custom_station_specifics` varchar(255) default NULL,
ADD  `clock` varchar(255) default NULL,
ADD  `antenna` varchar(255) default NULL,
ADD  `integrationtime` varchar(255) default NULL,
ADD  `averaging` tinyint(1) NOT NULL default '0',
ADD  `flagging` tinyint(1) NOT NULL default '0',
ADD  `calibration` tinyint(1) NOT NULL default '0',
ADD  `imaging` tinyint(1) NOT NULL default '0',
ADD  `one_filter` varchar(255) default NULL,
ADD  `averaging_frequency` double default NULL,
ADD  `averaging_time` double default NULL,
ADD  `polarization_amount` int(11) default NULL,
ADD  `fieldsize_x` int(11) default NULL,
ADD  `fieldsize_y` int(11) default NULL,
ADD  `pixelsize_x` int(11) default NULL,
ADD  `pixelsize_y` int(11) default NULL;