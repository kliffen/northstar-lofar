ALTER TABLE semester MODIFY COLUMN telescope VARCHAR(20) NOT NULL;
ALTER TABLE observingrequest MODIFY COLUMN observingrequesttype VARCHAR(20) NOT NULL;
ALTER TABLE observation MODIFY COLUMN observationtype VARCHAR(20) NOT NULL;