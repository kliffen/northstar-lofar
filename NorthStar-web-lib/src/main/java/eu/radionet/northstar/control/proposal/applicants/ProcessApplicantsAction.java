// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.applicants;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.database.exception.DatabaseException;
import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.util.AstronValidator;

import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProcessProposalAction;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.Member;
import eu.radionet.northstar.data.entities.NonRegisteredMember;
import eu.radionet.northstar.data.entities.Proposal;
import eu.radionet.northstar.data.entities.RegisteredMember;

public class ProcessApplicantsAction extends ProcessProposalAction {

	public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		ApplicantsForm applicantsForm = (ApplicantsForm) form;
		HttpSession session = request.getSession();
		ActionMessages errors = new ActionMessages();

		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		if (log.isTraceEnabled()) {
			log.trace(LogMessage.getMessage(ownUserAccount,
					Constants.ENTER_CLASS));
		}
		/*
		 * if pressed on Discard changes
		 */
		if (isCancelled(request)) {

			return cancel(mapping, request, ownUserAccount);

		}
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
		Proposal proposal = proposalData.getProposal();
		// null pointer exception was throwing
		int pi =0;
		if(!AstronValidator.isBlankOrNull(applicantsForm.getPi()))
		pi = new Integer(applicantsForm.getPi()).intValue();
		int contactAuthor =0;
		if(!AstronValidator.isBlankOrNull(applicantsForm.getContactAuthor()))
		 contactAuthor = new Integer(applicantsForm.getContactAuthor())
				.intValue();
		/*
		 * check if data is valid
		 */
		int membersSize = proposal.getMembers().size();
		if (pi >= membersSize
				|| contactAuthor >= membersSize
				|| (applicantsForm.getPotentialObservers() != null && applicantsForm
						.getPotentialObservers().length > membersSize)) {
			applicantsForm.setValidData(false);
		}else {
			Member member = (Member) proposal.getMembers().get(contactAuthor);
			if (member == null || member.getClass().equals(NonRegisteredMember.class)){
				applicantsForm.setValidData(false);
			}
		}
		if (applicantsForm.isValidData()) {
			for (int i = 0; i < proposal.getMembers().size(); i++) {
				boolean foundPotentialObserver = false;
				Member member = (Member) proposal.getMembers().get(i);
				int memberId = i;
				/*
				 * if this member is selected as pi
				 */
				if (pi == memberId) {
					/*
					 * check if member wasn't pi
					 */
					if (!member.isPi()) {
						try {
							member.addRole(proposalDelegate.getPiRole());
						} catch (DatabaseException de) {
							log.error("Cannot retrieve role: "
									+ de.getMessage());
							errors.add(Constants.ERROR, new ActionMessage(
									"error.database.retrieve.role"));
							saveErrors(request, errors);
							return mapping.findForward("refresh");
						}
					}
				} else {
					/*
					 * if member was pi, remove role
					 */
					if (member.isPi()) {
						/*
						 * remove pi role
						 */
						member
								.removeRole(eu.radionet.northstar.data.Constants.PI);
					}
				}
				/*
				 * if this member is selected as contactAuthor
				 */
				if (contactAuthor == memberId) {
					/*
					 * check if member wasn't contactAuthor
					 */
					if (!member.isContactAuthor()) {
						try {
							member.addRole(proposalDelegate
									.getContactAuthorRole());
						} catch (DatabaseException de) {
							log.error("Cannot retrieve role: "
									+ de.getMessage());
							errors.add(Constants.ERROR, new ActionMessage(
									"error.database.retrieve.role"));
							saveErrors(request, errors);
							return mapping.findForward("refresh");
						}
					}
				} else {
					/*
					 * if member was contactAuthor, remove role
					 */
					if (member.isContactAuthor()) {
						/*
						 * remove contactAuthor role
						 */
						member
								.removeRole(eu.radionet.northstar.data.Constants.CONTACT_AUTHOR);
					}
				}
				/*
				 * search if member is potential observer
				 */
				for (int j = 0; applicantsForm.getPotentialObservers() != null
						&& j < applicantsForm.getPotentialObservers().length; j++) {

					/*
					 * if entry is integer
					 */
					if (AstronValidator.isInt(applicantsForm
							.getPotentialObservers()[j])) {
						/*
						 * retrieve intvalue
						 */
						int potentialObserver = new Integer(applicantsForm
								.getPotentialObservers()[j]).intValue();
						/*
						 * if memberId = potentialObserver
						 */
						if (potentialObserver == memberId) {
							foundPotentialObserver = true;
							/*
							 * if member wasn't potentialObserver
							 */
							if (!member.isPotentialObserver()) {
								/*
								 * retrieve potential observer role
								 */
								try {
									member.addRole(proposalDelegate
											.getPotentialObserverRole());
								} catch (DatabaseException de) {
									log.error("Cannot retrieve role: "
											+ de.getMessage());
									errors
											.add(
													Constants.ERROR,
													new ActionMessage(
															"error.database.retrieve.role"));
									saveErrors(request, errors);
									return mapping.findForward("refresh");
								}
							}

						}

					}

				}
				/*
				 * if no potential observer found
				 */
				if (!foundPotentialObserver) {
					/*
					 * member was an potential observer
					 */
					if (member.isPotentialObserver()) {
						/*
						 * remove potential observer role
						 */
						member
								.removeRole(eu.radionet.northstar.data.Constants.POTENTIAL_OBSERVER);
					}
				}
			}
		}else {
			return mapping.findForward(Constants.REFRESH);
		}
		session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
		/*
		 * if forward in form is set, forward to an other pages. (See jsp's for
		 * more info)
		 */
		if (!AstronValidator.isBlankOrNull(applicantsForm.getForward())) {
			return mapping.findForward(applicantsForm.getForward());
		}

		/*
		 * if pressed on add applicant button
		 */
		else if (applicantsForm.getAddMemberButton() != null) {
			return mapping.findForward("addapplicant");
		}
		/*
		 * if pressed on edit applicant button
		 */
		else if (applicantsForm.getEditMemberButton(0) != null) {

			return ParameterAppender.append(mapping
					.findForward("editapplicant"), "memberId", applicantsForm
					.getSelectedMemberId()
					+ "");
		}
		/*
		 * if pressed on delete applicant button
		 */
		else if (applicantsForm.getDeleteMemberButton(0) != null) {
			return ParameterAppender.append(mapping
					.findForward("deleteapplicant"), "memberId", applicantsForm
					.getSelectedMemberId()
					+ "");
		}
		/*
		 * if pressed on invite applicant button
		 */
		else if (applicantsForm.getInviteMemberButton(0) != null) {
			return ParameterAppender.append(mapping
					.findForward("reinviteapplicant"), "memberId",
					applicantsForm.getSelectedMemberId() + "");

		}
		/*
		 * if pressed on reinvite applicant button
		 */
		else if (applicantsForm.getReInviteMemberButton(0) != null) {
			return ParameterAppender.append(mapping
					.findForward("reinviteapplicant"), "memberId",
					applicantsForm.getSelectedMemberId() + "");

		}
		/*
		 * move down member
		 */
		else if (applicantsForm.getMoveDownMemberButton(0) != null) {
			Member first = (Member) proposal.getMembers().get(
					applicantsForm.getSelectedMemberId());
			Member second = (Member) proposal.getMembers().get(
					applicantsForm.getSelectedMemberId() + 1);

			proposal.getMembers().set(applicantsForm.getSelectedMemberId(),
					second);
			proposal.getMembers().set(applicantsForm.getSelectedMemberId() + 1,
					first);
			session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
			return mapping.findForward(Constants.REFRESH);
		}
		/*
		 * move member up
		 */
		else if (applicantsForm.getMoveUpMemberButton(0) != null) {
			Member first = (Member) proposal.getMembers().get(
					applicantsForm.getSelectedMemberId() - 1);
			Member second = (Member) proposal.getMembers().get(
					applicantsForm.getSelectedMemberId());
			proposal.getMembers().set(applicantsForm.getSelectedMemberId(),
					first);
			proposal.getMembers().set(applicantsForm.getSelectedMemberId() - 1,
					second);
			session.setAttribute(Constants.PROPOSAL_DATA, proposalData);
			return mapping.findForward(Constants.REFRESH);
		}
		
		else if(applicantsForm.getAddApplicantsButton() != null ){
			Proposal selectedProposal =null;
			if(applicantsForm.getSelectedProposal()!=null&&applicantsForm.getSelectedProposal().length()>0)
			{String selectedProposalId = applicantsForm.getSelectedProposal();
			List existingProposalList =proposal.getMembers();
			
			selectedProposal = northStarDelegate.getProposal(Integer.parseInt(selectedProposalId));
			List memberList =selectedProposal.getMembers();
			
			List otherMembers= new ArrayList();
			Iterator memberIt = memberList.iterator();
			while (memberIt.hasNext()){
				Member member = (Member) memberIt.next();
			   if(!member.getEmail().equalsIgnoreCase(ownUserAccount.getUser().getEmail()) && !alredyAddedasmember(otherMembers,member)
					   && !alredyAddedasmember(existingProposalList, member)){
					NonRegisteredMember nonRegisteredMember = new NonRegisteredMember();
					nonRegisteredMember.setName(member.getName());
					nonRegisteredMember.setEmail(member.getEmail());
					nonRegisteredMember.setAffiliation(member.getAffiliation());
					nonRegisteredMember.setCountry(member.getCountry());
					nonRegisteredMember.setInvite(false);
					proposal.addChangeMember(nonRegisteredMember);
					otherMembers.add(nonRegisteredMember);   
			   }
				 
			}
			
			//proposal.getMembers().addAll(otherMembers);
			}
			else{
				getErrors(request).add(
						Constants.ERROR,
						new ActionMessage(
								"error.no.proposal.selected"));
				saveErrors(request, errors);
			}
			return mapping.findForward(Constants.REFRESH);
		}
		
		ActionForward forward = processGeneralButtons(applicantsForm, mapping,
				request, errors, proposalData, ownUserAccount);
		if (forward != null) {
			return forward;
		}
		return mapping.findForward(Constants.REFRESH);
	}

	private boolean alredyAddedasmember(List memberList, Member newmember) {
		
		Iterator memberIt = memberList.iterator();
		while (memberIt.hasNext()){
			Member addedmember = (Member) memberIt.next();
			if(addedmember.getEmail().equalsIgnoreCase(newmember.getEmail()))
					return true;
		}
		
		return false;
	}

}
