// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * SetUpUploadAction.java 
 *
 * Created on Feb 10, 2005
 *
 * Version $Id: SetUpSubmitAction.java,v 1.2 2008-04-08 08:02:15 smit Exp $
 *
 */
package eu.radionet.northstar.control.proposal.submit;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.action.ActionMessages;

import eu.radionet.northstar.business.LogMessage;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.collaboration.LockedAction;
import eu.radionet.northstar.data.entities.ProposalError;

/**
 * The SetUpUploadAction provides
 * 
 * @author Bastiaan Verhoef
 *  
 */
public class SetUpSubmitAction extends LockedAction {
    protected Log log = LogFactory.getLog(this.getClass());


    public ActionForward lockedExecute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        SubmitForm submitForm = (SubmitForm) form;
        HttpSession session = request.getSession();
		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
        ActionMessages errors = new ActionMessages();
		ProposalData proposalData = (ProposalData) session.getAttribute(Constants.PROPOSAL_DATA);
        ProposalError proposalErrors = proposalDelegate.validate(proposalData.getProposal());
        submitForm.setProposalErrors(proposalErrors);
        submitForm.setProposalCode(proposalData.getProposal().getCode());
        if (proposalData.getProposal().getJustification() != null) {
            submitForm.setProposalTitle(proposalData.getProposal()
                    .getJustification().getTitle());
        }
        submitForm.setTelescope(proposalData.getSelectedTelescope());
        submitForm.setProposalCategory(proposalData.getProposal()
                .getSemester().getCategory().getCode());
		try {

			proposalDelegate.storeWithoutSendingMails(proposalData.getProposal(), ownUserAccount, session.getId());
		} catch (DatabaseException de) {
			log.error(LogMessage.getMessage(ownUserAccount,
					"Cannot store proposal: " + de.getMessage(), de));
			errors.add(Constants.ERROR, new ActionMessage(
					"error.database.store.proposal"));
			saveErrors(request, errors);
			return mapping.findForward(Constants.REFRESH);
		}
		NorthStarUtils northStarUtils = new NorthStarUtils();
		northStarUtils.storeJustificationFiles(proposalDelegate, request,
				proposalData);
        return mapping.findForward(Constants.SUCCESS);

    }

}

