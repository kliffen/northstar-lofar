// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

/*
 * ProposalListForm.java 
 *
 * Created on Feb 2, 2005
 *
 * Version $Id: ProposalListForm.java,v 1.4 2006-06-30 10:01:00 verhoef Exp $
 *
 */
package eu.radionet.northstar.control.proposal.list;

import java.util.ArrayList;
import java.util.List;

import org.apache.struts.action.ActionForm;

/**
 * The ProposalListForm provides
 * 
 * @author Bastiaan Verhoef
 *  
 */
public class ProposalListForm extends ActionForm {
	private String[] selectedTelescopes = null;
	private String telescope =  null;

	private List telescopes = new ArrayList();
    protected List telescopeBeans = new ArrayList();
    protected boolean showReviewedProposals = false;
    protected String bannerImageUri = null;
	protected String welcomeMessage = null;
	protected String welcomeMessageUri = null;
	private boolean enableSelectTelescope = false;

    private String newButton = null;
	private String filterButton = null;
	private boolean proposalDataOnSession = false;
	protected boolean hasReviewerPrivileges = false;


    public TelescopeBean getTelescopeBean(int index) {
    	resizeTelescopeBeans(index);
        return (TelescopeBean) telescopeBeans.get(index);

    }

    private void resizeTelescopeBeans(int index) {
        int newSize = index + 1;
        if (telescopeBeans.size() < newSize) {
            for (int i = telescopeBeans.size(); telescopeBeans.size() < newSize; i++){
            	telescopeBeans.add(new TelescopeBean());
            }
        }
    }



    /**
     * @return Returns the newButton.
     */
    public String getNewButton() {
        return newButton;
    }

    /**
     * @param newButton
     *            The newButton to set.
     */
    public void setNewButton(String newButton) {
        this.newButton = newButton;
    }

 

	/**
     * @return Returns the bannerImageUri.
     */
    public String getBannerImageUri() {
        return bannerImageUri;
    }


    /**
     * @param bannerImageUri The bannerImageUri to set.
     */
    public void setBannerImageUri(String bannerImageUri) {
        this.bannerImageUri = bannerImageUri;
    }


    public String getWelcomeMessage() {
		return welcomeMessage;
	}


	public void setWelcomeMessage(String welcomeMessage) {
		this.welcomeMessage = welcomeMessage;
	}


	public String getWelcomeMessageUri() {
		return welcomeMessageUri;
	}


	public void setWelcomeMessageUri(String welcomeMessageUri) {
		this.welcomeMessageUri = welcomeMessageUri;
	}


	public String getFilterButton() {
		return filterButton;
	}


	public void setFilterButton(String filterButton) {
		this.filterButton = filterButton;
	}


	public String[] getSelectedTelescopes() {
		return selectedTelescopes;
	}


	public void setSelectedTelescopes(String[] selectedTelescopes) {
		this.selectedTelescopes = selectedTelescopes;
	}


	public List getTelescopes() {
		return telescopes;
	}


	public void setTelescopes(List telescopes) {
		if (selectedTelescopes == null) {
			String[] selectedTelescopes = new String[telescopes.size()];
			for (int i = 0; i < telescopes.size(); i++) {
				selectedTelescopes[i] = (String) telescopes.get(i);
			}
			this.selectedTelescopes = selectedTelescopes;
		}
		this.telescopes = telescopes;
	}


	public String getTelescope() {
		return telescope;
	}


	public void setTelescope(String telescope) {

		this.telescope = telescope;
	}


	public boolean isProposalDataOnSession() {
		return proposalDataOnSession;
	}


	public void setProposalDataOnSession(boolean proposalDataOnSession) {
		this.proposalDataOnSession = proposalDataOnSession;
	}


	public List getTelescopeBeans() {
		return telescopeBeans;
	}


	public void setTelescopeBeans(List telescopeBeans) {
		this.telescopeBeans = telescopeBeans;
	}


	public boolean isShowReviewedProposals() {
		return showReviewedProposals;
	}


	public void setShowReviewedProposals(boolean showReviewedProposals) {
		this.showReviewedProposals = showReviewedProposals;
	}

	public boolean isEnableSelectTelescope() {
		return enableSelectTelescope;
	}

	public void setEnableSelectTelescope(boolean enableSelectTelescope) {
		this.enableSelectTelescope = enableSelectTelescope;
	}

	public boolean isHasReviewerPrivileges() {
		return hasReviewerPrivileges;
	}

	public void setHasReviewerPrivileges(boolean hasReviewerPrivileges) {
		this.hasReviewerPrivileges = hasReviewerPrivileges;
	}
}