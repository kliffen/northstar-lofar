<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />
<html:form action="/processApplicant" focus="name" method="POST">

<html:hidden property="memberId"/>
<table width="100%">
	<TR>
		<TD width="*">&nbsp;</TD>
		<TD class="help" width="60">
		<jsp:include page="/pages/layout/box_header_noFullWidth.jsp" />
				<TABLE width="60" height="25">
					<TR>
			  			<TD width='20' height="25"><%= "<a title=\"Opens the help window\" href=\"" + request.getContextPath() + "/proposalHelp.do#applicant\" target=\"help\"><img width='24'src=\"" +request.getContextPath() + "/images/list/help.gif\"></a>"%></TD>
		  				<TD width="40" height="25"><%= "<a title=\"Opens the help window.\" href=\"" + request.getContextPath() + "/proposalHelp.do#applicant\" target=\"help\">Help</a>"%></TD>
	  				</TR>
	  			</TABLE>
			<jsp:include page="/pages/layout/box_footer_noFullWidth.jsp" />
			</TD>
	</TR>
	</table>
	<table>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.applicant.name" /></td>
		<td class="left"><html:text property="name"  tabindex="1" maxlength="255"/><span class="error"><astron:errors property="name" ifnotexist="*"/></span></td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.applicant.affiliation" /></td>
		<td class="left"><html:text property="affiliation" tabindex="2"  maxlength="255"/><span class="error"><astron:errors property="affiliation" ifnotexist="*"/></span></td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.applicant.email" /></td>
		<td class="left"><html:text property="email" tabindex="3"  maxlength="255"/><span class="error"><astron:errors property="email" ifnotexist="*"/></span></td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.applicant.country" /></td>
		<td class="left"><bean:define id="mcountry" name="applicantForm" property="countries"/> 
			<html:select name="applicantForm" property="selectedCountry">
				<html:options collection="mcountry" property="ISO3Country" labelProperty="displayCountry"/>
			</html:select>
			<span class="error"><astron:errors property="country" ifnotexist="*"/></span></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>
	<tr>
		<td class="right"><astron:label key="label.applicant.invite" /></td>
		<td class="left"><html:multibox property="invite" value="yes"></html:multibox></td>
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>	
	<tr>	
		<TD colspan="2">
		<tiles:get name="box_header" />
			<TABLE width="100%">
				<TR>
					<TD>
					<astron:label key="label.applicant.note" />
					</TD>
				</TR>
 			</TABLE>
		<tiles:get name="box_footer" />
		</TD>
	</TR>
	<tr>
		<td colspan="2">&nbsp;</td>
	</tr>		
</table>
<table width="100%">
	<tr>
		<td align="left"><html:submit property="addApplicantButton"  tabindex="5" styleClass="list_accept_yes">Ok</html:submit>
 		</td>
		<td align="right"><html:cancel styleClass="list_decline_t">Cancel</html:cancel></TD>
	</tr>
</table>

</html:form>