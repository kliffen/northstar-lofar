// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.control.proposal.collaboration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import nl.astron.useradministration.data.entities.UserAccount;
import nl.astron.database.exception.DatabaseException;
import nl.astron.util.AstronConverter;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import eu.radionet.northstar.business.CollaborationDelegate;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.ProposalDelegate;
import eu.radionet.northstar.business.ProposalDelegateFactory;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.util.ParameterAppender;
import eu.radionet.northstar.data.entities.Proposal;

public abstract class LockedAction extends Action {
	protected CollaborationDelegate collaborationDelegate = null;

	protected ProposalDelegate proposalDelegate = null;

	//protected ProposalData proposalData = null;

	protected NorthStarDelegate northStarDelegate = null;

	public ActionForward execute(ActionMapping mapping, ActionForm form,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		HttpSession session = request.getSession();

		/*
		 * retrieve own_useraccount info from session
		 */
		UserAccount ownUserAccount = (UserAccount) session
				.getAttribute(Constants.OWN_USERACCOUNT);
		collaborationDelegate = CollaborationDelegate.getInstance();
		northStarDelegate = NorthStarDelegate.getInstance();
		/*
		 * retrieve session object with information
		 */
		ProposalData proposalData = (ProposalData) session
				.getAttribute(Constants.PROPOSAL_DATA);
		if (proposalData == null) {
			ActionForward actionForward = new ActionForward();
			actionForward.setPath("/setUpSessionExpired.do");
			actionForward.setModule("");
			actionForward.setRedirect(true);
			return actionForward;
		}

		Proposal proposal = proposalData.getProposal();
		proposalDelegate = ProposalDelegateFactory
				.getProposalDelegate(proposal);
		/*
		 * if dead line is passed.
		 */
		if (!proposalData.isIgnoreDeadLineWarning()
				&& northStarDelegate.isDeadLinePassed(proposal)
				&& !northStarDelegate.hasUnderReviewStatus(proposal)) {
			proposalData.setIgnoreDeadLineWarning(true);
			String path = "/setUpDeadLinePassed.do";
			String requestString = request.getServletPath();
			/*
			 * if it is not the dead line passed uri
			 */
			if (!requestString.startsWith(path)) {
				ActionForward actionForward = new ActionForward();
				actionForward.setPath(path);
				actionForward.setModule("");
				actionForward.setRedirect(true);
				return actionForward;
			}
		}
		if (!proposalData.isIgnoreDeadLineWarning()
				&& !proposalData.isIgnoreInvalidConfigurationWarning()
				&& northStarDelegate.isDeprecated(proposal)) {
			proposalData.setIgnoreInvalidConfigurationWarning(true);
			String path = "/setUpInvalidConfiguration.do";
			String requestString = request.getServletPath();
			/*
			 * if it is not the dead line passed uri
			 */
			if (!requestString.startsWith(path)) {
				ActionForward actionForward = new ActionForward();
				actionForward.setPath(path);
				actionForward.setModule("");
				actionForward.setRedirect(true);
				return actionForward;
			}
		}

		/*
		 * checks if another session locks the proposal, if not it locks
		 * automaticly the proposal
		 */
		if (!collaborationDelegate.checkAndUpdateProposalLock(proposal.getId(),
				ownUserAccount, session.getId())) {

			return ParameterAppender.append(mapping
					.findForward(Constants.LOCKED_PROPOSAL), "proposalId",
					AstronConverter.toString(proposal.getId()));
		}
		return lockedExecute(mapping, form, request, response);

	}

	protected ActionForward cancel(ActionMapping mapping,
			HttpServletRequest request, UserAccount ownUserAccount)
			throws DatabaseException {
		removeLock(request, ownUserAccount);
		return mapping.findForward(Constants.DISCARD);
	}

	protected void removeLock(HttpServletRequest request,
			UserAccount ownUserAccount) throws DatabaseException {
		collaborationDelegate.unlockProposals(ownUserAccount, request
				.getSession().getId());
		request.getSession().removeAttribute(Constants.PROPOSAL_DATA);
	}

	public abstract ActionForward lockedExecute(ActionMapping mapping,
			ActionForm form, HttpServletRequest request,
			HttpServletResponse response) throws Exception;
}
