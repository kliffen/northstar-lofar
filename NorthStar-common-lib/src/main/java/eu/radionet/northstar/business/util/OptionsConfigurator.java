// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.radionet.northstar.business.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;

import nl.astron.util.AstronValidator;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;

import eu.radionet.northstar.business.configuration.options.ConfigurationType;
import eu.radionet.northstar.business.configuration.options.ContextType;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.configuration.options.config.ConfigureOptionType;
import eu.radionet.northstar.business.configuration.options.config.DependenciesType;
import eu.radionet.northstar.business.configuration.options.config.OptionsConfigurationType;
import eu.radionet.northstar.business.configuration.options.definition.BooleanType;
import eu.radionet.northstar.business.configuration.options.definition.DoubleType;
import eu.radionet.northstar.business.configuration.options.definition.FieldType;
import eu.radionet.northstar.business.configuration.options.definition.IntegerType;
import eu.radionet.northstar.business.configuration.options.definition.OptionListType;
import eu.radionet.northstar.business.configuration.options.definition.StringType;
import eu.radionet.northstar.business.configuration.options.definition.TextType;
import eu.radionet.northstar.business.configuration.options.definition.ValidationType;

public class OptionsConfigurator {
	private Log log = LogFactory.getLog(this.getClass());

	private static final String CONTEXTS = "contexts";

	private static final String CONTEXT = "context";

	private static final String CATEGORY = "category";

	private static final String CONFIGURATION = "configuration";
	private static final String FIELDS = "fields";
	private static final String OPTIONS = "options";
	private static final String TELESCOPES = "telescopes";
	private static final String TELESCOPE = "telescope";
	private static final String CONFIGURATIONFILE = "configurationfile";

	private static final String OPTION = "option";
	private static final String DEFAULT = "default";
	private static final String REQUIRED = "required";
	private static final String NAME = "name";
	private static final String LABEL = "label";
	private static final String VALUE = "value";
	private static final String TEXT = "text";
	private static final String BOOLEAN = "boolean";
	private static final String INTEGER = "integer";
	private static final String STRING = "string";
	private static final String DOUBLE = "double";
	private static final String OPTION_LIST = "optionlist";
	private static final String VALIDATION = "validation";
	private static final String LOWER_BOUNDARY = "lowerBoundary";
	private static final String UPPER_BOUNDARY = "upperBoundary";
	private static final String DEPENDENCIES = "dependencies";
	private static final String SUBFIELDS = "subfields";
	private static final String OR = "or";
	private static final String AND = "and";
	private static final String SELECT = "select";
	public Map configure(Document document, ServletContext servletContext) throws Exception {

		/*
		 * search for project
		 */
		Node rootNode = document.getDocumentElement();
		if (equalIgnorePrefix(rootNode, CONTEXTS)) {
			return parseRootNode(rootNode,servletContext );
		}
		return null;
	}

	protected Map parseRootNode(Node rootNode,ServletContext servletContext) throws Exception {
		Map contexts = new HashMap();

		for (int i = 0; i < rootNode.getChildNodes().getLength(); i++) {
			Node nodeChild = rootNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, CONTEXT)) {
					String category = getAttribute(nodeChild, CATEGORY);
					contexts.put(category, parseContextNode(nodeChild,servletContext ));
				}
			}

		}
		return contexts;

	}

	protected ContextType parseContextNode(Node contextNode,ServletContext servletContext)
			throws Exception {
		ContextType contextConfiguration = new ContextType();

		for (int i = 0; i < contextNode.getChildNodes().getLength(); i++) {
			Node nodeChild = contextNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {
				if (equal(nodeChild, CONFIGURATION)) {
					ConfigurationType configurationType =contextConfiguration.getConfigurationType();
					configurationType = parseConfigurationType(configurationType,nodeChild);
					contextConfiguration.setConfigurationType(configurationType);
				}else if (equal(nodeChild, FIELDS)) {
					contextConfiguration.setFieldsDefinitionType(parseFieldsDefinitionType(nodeChild));
				}else if (equal(nodeChild, TELESCOPES)) {
					ConfigurationType configurationType =contextConfiguration.getConfigurationType();
					configurationType = parseTelescopes(configurationType,nodeChild,servletContext);
					contextConfiguration.setConfigurationType(configurationType);
					
				}
			}

		}
		return contextConfiguration;

	}

	private ConfigurationType parseTelescopes(
			ConfigurationType configurationType, Node node,
			ServletContext servletContext) {
		if(configurationType == null){
			configurationType = new ConfigurationType();
		}
		for (int i = 0; i < node.getChildNodes().getLength(); i++) {
			Node nodeChild = node.getChildNodes().item(i);
			if(equal(nodeChild, TELESCOPE)){
				String name = "";
				for (int j = 0; j < nodeChild.getChildNodes().getLength(); j++) {
					Node telescopeNode = nodeChild.getChildNodes().item(j);
					if(equal(telescopeNode,NAME)){
						name = getValue(telescopeNode);
					}
					if(equal(telescopeNode,CONFIGURATIONFILE)){
						//contextConfiguration.setFieldsDefinitionType(parseFieldsDefinitionType(nodeChild));
						String configFileUri = getValue(telescopeNode);
						log.info("parsing telescope: "+name+" from "+configFileUri);
						try{
							String telescopeFile = NorthStarUtils.read(configFileUri,servletContext);		
							XMLConverter xmlConverter = XMLConverter.getInstance();
							Document telescopeConfigDocument = xmlConverter
									.convertStringToDocument(telescopeFile);
							Node rootNode = telescopeConfigDocument.getDocumentElement();
							if (equalIgnorePrefix(rootNode, CONFIGURATION)) {
								configurationType = parseTelescopeFile(rootNode,configurationType);
							}
						}catch(Exception e ){
							log.warn("Could not read XML file: "+configFileUri);
						}
					}
				}
				
			}
		}
		return configurationType;
	}

	private ConfigurationType parseTelescopeFile(Node configurationNode,
			ConfigurationType configurationType) {
		for (int i = 0; i < configurationNode.getChildNodes().getLength(); i++) {
			Node nodeChild = configurationNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, OPTIONS)) {

						try {
							parseOptionsNode(configurationType.getOptionsConfigurationTypes(), nodeChild);
						} catch (Exception e) {
							log.warn("could not parse options node ");
							log.warn(e.toString());
						}

				}
			}

		}
		return configurationType;
	}

	protected ConfigurationType parseConfigurationType(ConfigurationType configuration, Node configurationNode) throws Exception {
		if(configuration == null){
			configuration = new ConfigurationType();
		}
		for (int i = 0; i < configurationNode.getChildNodes().getLength(); i++) {
			Node nodeChild = configurationNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, OPTIONS)) {
					parseOptionsNode(configuration
							.getOptionsConfigurationTypes(), nodeChild);
				}
			}

		}
		return configuration;

	}
	protected FieldsDefinitionType parseFieldsDefinitionType(Node configurationNode) throws Exception {
		FieldsDefinitionType fieldsDefinitionType = new FieldsDefinitionType();
		for (int i = 0; i < configurationNode.getChildNodes().getLength(); i++) {
			Node nodeChild = configurationNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, DOUBLE)) {
					parseFieldNode(fieldsDefinitionType
							.getFields(), nodeChild, new DoubleType());
				}else if (equal(nodeChild, INTEGER)) {
					parseFieldNode(fieldsDefinitionType
							.getFields(), nodeChild, new IntegerType());
				}else if (equal(nodeChild, STRING)) {
					parseFieldNode(fieldsDefinitionType
							.getFields(), nodeChild, new StringType());
				}else if (equal(nodeChild, TEXT)) {
					parseFieldNode(fieldsDefinitionType
							.getFields(), nodeChild, new TextType());
				}else if (equal(nodeChild, BOOLEAN)) {
					parseFieldNode(fieldsDefinitionType
							.getFields(), nodeChild, new BooleanType());
				}else if (equal(nodeChild, OPTION_LIST)) {
					parseFieldNode(fieldsDefinitionType
							.getFields(), nodeChild, new OptionListType());
				}
				
			}

		}
		return fieldsDefinitionType;

	}

	protected void parseOptionsNode(Map fieldConfigurations,
			Node optionsNode) throws Exception {
		String name = getAttribute(optionsNode, NAME);
		OptionsConfigurationType optionsFieldConfiguration = null;
		if(fieldConfigurations.containsKey(name)){
			optionsFieldConfiguration = (OptionsConfigurationType) fieldConfigurations.get(name);
		}else {
			optionsFieldConfiguration = new OptionsConfigurationType();
		}
		
		for (int i = 0; i < optionsNode.getChildNodes().getLength(); i++) {
			Node nodeChild = optionsNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, OPTION)) {
					parseOptionNode(optionsFieldConfiguration
							.getOptions(), nodeChild);
				} else if (equal(nodeChild, DEFAULT)) {
					optionsFieldConfiguration.setDefaultOption(getValue(nodeChild));
				}
			}

		}
		fieldConfigurations.put(name, optionsFieldConfiguration);

	}
	protected void parseFieldNode(Map fieldConfigurations,
			Node numberNode,FieldType fieldConfiguration) throws Exception {
		String name = getAttribute(numberNode, NAME);
		String required = getAttribute(numberNode, REQUIRED);
		if (!AstronValidator.isBlankOrNull(required)){
			if (required.equals("false")){
				fieldConfiguration.setRequired(false);
			}
		}
		for (int i = 0; i < numberNode.getChildNodes().getLength(); i++) {
			Node nodeChild = numberNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, VALIDATION)) {
					fieldConfiguration.setValidation(parseValidationNode(nodeChild));
					
				}else if (equal(nodeChild, DEFAULT)) {
					fieldConfiguration.setDefaultValue(getValue(nodeChild));
					
				}
			}

		}
		fieldConfigurations.put(name, fieldConfiguration);

	}
	protected ValidationType parseValidationNode(
			Node optionsNode) throws Exception {
		ValidationType validationConfiguration = new ValidationType();
		for (int i = 0; i < optionsNode.getChildNodes().getLength(); i++) {
			Node nodeChild = optionsNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, UPPER_BOUNDARY)) {
					validationConfiguration.setUpperBoundary(getValue(nodeChild));
				} else if (equal(nodeChild, LOWER_BOUNDARY)) {
					validationConfiguration.setLowerBoundary(getValue(nodeChild));
				}
			}

		}
		return validationConfiguration;
	}
	protected void parseOptionNode(List options,
			Node optionsNode) throws Exception {
		ConfigureOptionType optionFieldConfiguration = new ConfigureOptionType();
		for (int i = 0; i < optionsNode.getChildNodes().getLength(); i++) {
			Node nodeChild = optionsNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {
				if (equal(nodeChild, VALUE)) {
					optionFieldConfiguration.setValue(getValue(nodeChild));
				} else if (equal(nodeChild, LABEL)) {
					optionFieldConfiguration.setLabel(getValue(nodeChild));
				} else if (equal(nodeChild, SUBFIELDS)) {
					optionFieldConfiguration.setSubfields(parseFieldsDefinitionType(nodeChild));
				}else if (equal(nodeChild, DEPENDENCIES)) {
					parseDependenciesNode(optionFieldConfiguration
							, nodeChild);
				}
			}

		}
		options.add(optionFieldConfiguration);

	}

	protected void parseDependenciesNode( ConfigureOptionType optionFieldConfiguration,
			Node optionsNode) throws Exception {
		for (int i = 0; i < optionsNode.getChildNodes().getLength(); i++) {
			Node nodeChild = optionsNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, OR)) {
					optionFieldConfiguration.setDependenciesConfiguration(parseAndOrNodeNode(OR,nodeChild));
				}else if (equal(nodeChild, AND)) {
					optionFieldConfiguration.setDependenciesConfiguration(parseAndOrNodeNode(AND,nodeChild));
				} 
			}

		}

	}
	protected DependenciesType parseAndOrNodeNode(String type,
			Node optionsNode) throws Exception {
		DependenciesType dependenciesConfiguration = new DependenciesType();
		dependenciesConfiguration.setType(type);
		for (int i = 0; i < optionsNode.getChildNodes().getLength(); i++) {
			Node nodeChild = optionsNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, OR)) {
					dependenciesConfiguration.getAndOrItems().add(parseAndOrNodeNode(OR,nodeChild));
				}else if (equal(nodeChild, AND)) {
					dependenciesConfiguration.getAndOrItems().add(parseAndOrNodeNode(AND,nodeChild));
				}else if (equal(nodeChild, SELECT)) {
					parseSelectNode(dependenciesConfiguration.getItems(), nodeChild);
				} 
			}

		}
		return dependenciesConfiguration;

	}
	protected void parseSelectNode(Map items,
			Node optionsNode) throws Exception {
		String name = getAttribute(optionsNode, NAME);
		List selectList = (List) items.get(name);
		if (selectList == null){
			selectList = new ArrayList();
		}
		for (int i = 0; i < optionsNode.getChildNodes().getLength(); i++) {
			Node nodeChild = optionsNode.getChildNodes().item(i);
			/*
			 * if child is an element
			 */
			if (AstronValidator.implementsInterface(Element.class, nodeChild
					.getClass())) {

				if (equal(nodeChild, VALUE)) {
					selectList.add(getValue(nodeChild));
				}
			}

		}
		items.put(name,selectList);

	}


	/**
	 * The getValue method returns the value of an node
	 * 
	 * @param node
	 * @return
	 */
	protected String getValue(Node node) {
		String value = null;
		if (node.getFirstChild() != null) {
			value = node.getFirstChild().getNodeValue();
			if (log.isDebugEnabled()) {
				log.debug("Node: " + node.getNodeName() + " value: " + value);
			}
		}
		return value;
	}

	/**
	 * The equal method compares if an node has the given name
	 * 
	 * @param node
	 * @param nodeName
	 * @return
	 */
	protected boolean equal(Node node, String nodeName) {
		return node.getNodeName().equals(nodeName);
	}

	/* help subroutines for parsing DOM */
	protected String getAttribute(Node n, String attrLabel) {
		NamedNodeMap attributes = n.getAttributes();
		if (attributes.getNamedItem(attrLabel) == null) {
			return null;
		}
		return attributes.getNamedItem(attrLabel).getNodeValue();
	}
	/**
	 * Compares if a node has the given name, ignoring the prefix of the node
	 * @param node
	 * @param nodeName
	 * @return true, if equals
	 */
	protected boolean equalIgnorePrefix(Node node, String nodeName) {
		String withoutPrefix = removePrefix(node);
		return withoutPrefix.equals(nodeName);
	}
	/**
	 * Returns the node name without prefix
	 * @param node
	 * @return node name withoud prefix
	 */
	protected String removePrefix(Node node){
		String[] nodeSplit = node.getNodeName().split(":");
		String withoutPrefix = nodeSplit[nodeSplit.length - 1];
		return withoutPrefix;
	}
}
