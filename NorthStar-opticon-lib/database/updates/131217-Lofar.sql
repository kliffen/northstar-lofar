ALTER TABLE lofar_proposal_astron_nl_northstar.justification
ADD COLUMN observationstrategy text,
ADD COLUMN fillertime TINYINT(1) NOT NULL DEFAULT 0 ,
ADD COLUMN fillertimereason text,
ADD COLUMN coobservationteam TINYINT(1) NOT NULL DEFAULT 0,
ADD COLUMN ltastoragelocation text;