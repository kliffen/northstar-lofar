// Copyright (C) 2010.
// ASTRON (Netherlands Institute for Radio Astronomy)
// P.O.Box 2, 7990 AA Dwingeloo, The Netherlands
//
// This file is part of the NorthStar software package
// The NorthStar software package is free software: you can redistribute it and/or
// modify it under the terms of the GNU General Public License as published
// by the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version
//
// The NorthStar software package is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details
//
// You should have received a copy of the GNU General Public License along
// with the NorthStar software package. If not, see <http://www.gnu.org/licenses/>

package eu.opticon.northstar.control.observingrequest.observation;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.xml.rpc.ServiceException;

import nl.astron.database.exception.DatabaseException;
import nl.astron.service.lofar.StorageCalculator;
import nl.astron.service.lofar.client.StorageCalculatorBinding;
import nl.astron.util.AstroCoordinate;
import nl.astron.util.AstronConverter;

import org.apache.struts.util.LabelValueBean;
import org.apache.struts.util.MessageResources;

import eu.opticon.northstar.business.OpticonConstants;
import eu.opticon.northstar.business.OpticonTargetUtils;
import eu.opticon.northstar.control.observingrequest.instrument.InstrumentBean;
import eu.opticon.northstar.control.observingrequest.instrument.SetUpInstrumentBean;
import eu.opticon.northstar.data.entities.OpticonObservation;
import eu.opticon.northstar.data.entities.OpticonTarget;
import eu.radionet.northstar.business.NorthStarDelegate;
import eu.radionet.northstar.business.OptionsDelegate;
import eu.radionet.northstar.business.configuration.options.FieldsDefinitionType;
import eu.radionet.northstar.business.exception.ParseTargetListException;
import eu.radionet.northstar.business.util.NorthStarUtils;
import eu.radionet.northstar.control.Constants;
import eu.radionet.northstar.control.proposal.ProposalData;
import eu.radionet.northstar.control.proposal.observingrequest.ValueBean;
import eu.radionet.northstar.control.proposal.observingrequest.observation.ObservationForm;
import eu.radionet.northstar.control.proposal.observingrequest.observation.SetUpObservationAction;
import eu.radionet.northstar.control.util.OptionsUtils;
import eu.radionet.northstar.data.entities.Observation;
import eu.radionet.northstar.data.entities.ObservingRequest;
import eu.radionet.northstar.data.entities.Target;

/**
 * This class contains telescope specific form data of an observation
 */
public class SetUpOpticonObservationAction extends SetUpObservationAction 
{
	protected StorageCalculator sCalc = null;
	protected Map translations = new HashMap();
	
	public void performFormInitialisation(HttpServletRequest request, ObservationForm observationForm){
		InstrumentBean instrBean = new InstrumentBean();
		// this makes sure that the instrument bean exists, even though the bean is hidden.
		request.setAttribute("instrumentBean",instrBean);
		OpticonObservationForm opticonObservationForm = (OpticonObservationForm) observationForm;
		opticonObservationForm.setShowInstrumentBean("false");
	}
	
	 /**
     * telescope specific options. Extract the "example option" options 
     * from /web/conf/modules/skeleton/skeleton-options.xml
     * @see eu.radionet.northstar.control.proposal.observingrequest.SetUpObservingRequestAction#fillOptions(eu.radionet.northstar.control.proposal.observingrequest.ObservingRequestForm)
     */
    public void fillTargetOptions(HttpServletRequest request, ObservationForm observationForm)
        throws DatabaseException
    {
        OpticonObservationForm opticonObservationForm = (OpticonObservationForm) observationForm;
        
        opticonObservationForm.setMoonOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.TARGET_MOON, new HashMap(), contextConfiguration));
        opticonObservationForm.setSeeingOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.TARGET_SEEING, new HashMap(), contextConfiguration));
        opticonObservationForm.setWaterOptions(OptionsUtils.getLabelValueBeansWithoutNoneSpecified(
        		OpticonConstants.TARGET_WATER, new HashMap(), contextConfiguration));
        opticonObservationForm.setSkyQualities(OptionsUtils.getLabelValueBeans(
        		OpticonConstants.TARGET_SKY_QUALITIES, new HashMap(), contextConfiguration));
		
        List telescopeConfigurations = OptionsUtils.getLabelValueBeans(
        		OpticonConstants.TELESCOPE_CONFIGURATIONS, new HashMap(), contextConfiguration);
		        
 
        FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
        // set options to display 
		opticonObservationForm.setDisplayTargetRa(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_RA,
						fieldsDefinitionType));	
		opticonObservationForm.setDisplayTargetDec(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_DEC,
						fieldsDefinitionType));	

        opticonObservationForm.setDisplaySeeingOptions(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING,
						fieldsDefinitionType));	
		opticonObservationForm.setDisplaySeeingRange(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SEEING_RANGE,
						fieldsDefinitionType));	
		opticonObservationForm.setDisplayTargetExposureTime(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_EXPOSURE_TIME,
						fieldsDefinitionType));
		opticonObservationForm.setDisplayTargetFlux(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX,
						fieldsDefinitionType));	
		opticonObservationForm.setDisplayTargetFluxFrequency(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_FLUX_FREQUENCY,
						fieldsDefinitionType));	
		opticonObservationForm.setDisplayTargetSpectralIndex(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECTRAL_INDEX,
						fieldsDefinitionType));	
		
		opticonObservationForm.setDisplaySpecifyTargetSubbandList(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SPECIFY_SUBBAND,
						fieldsDefinitionType));
		// subband list itself will also activate when specify is selected
		opticonObservationForm.setDisplayTargetSubbandList( 
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SUBBAND_LIST,
						fieldsDefinitionType));
		
		opticonObservationForm.setDisplayTargetTotalSubbandList( 
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_TOTAL_SUBBAND_LIST,
						fieldsDefinitionType));		
		/* these are handled in the code
		opticonObservationForm.setDisplayTargetCentralFrequency( 
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_CENTRAL_FREQUENCY,
				fieldsDefinitionType));	
		opticonObservationForm.setDisplayTargetBandwidth( 
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_BANDWIDTH,
						fieldsDefinitionType));	
		*/
		opticonObservationForm.setDisplayTargetSkyQuality(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SKY_QUALITIES,
						fieldsDefinitionType));
		opticonObservationForm.setDisplayTargetMoon(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_MOON,
						fieldsDefinitionType));	
       opticonObservationForm.setDisplayTargetWater(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_WATER,
						fieldsDefinitionType));	
       opticonObservationForm.setDisplayTargetDiameter(

    		   OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_DIAMETER,
						fieldsDefinitionType));	
       opticonObservationForm.setDisplayTargetSn(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SN,
						fieldsDefinitionType));	
       opticonObservationForm.setDisplayTargetMagnitude(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_MAGNITUDE,
						fieldsDefinitionType));	
       opticonObservationForm.setDisplayTargetStorage(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_STORAGE,
						fieldsDefinitionType));	
       opticonObservationForm.setDisplayTargetRuns(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_RUNS,
						fieldsDefinitionType));	
       opticonObservationForm.setSingleTargetPerRun( 
    		   OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SINGLE_PER_RUN,
						fieldsDefinitionType));	
       opticonObservationForm.setDisplayTargetOpportunity(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_OPPORTUNITY,
						fieldsDefinitionType));	
       opticonObservationForm.setDisplayTargetSelectPipeline(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SELECT_PIPELINE,
						fieldsDefinitionType));	
       opticonObservationForm.setDisplayTargetSelectRunnumber(
				OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_SELECT_RUNNUMBER,
						fieldsDefinitionType));	
       opticonObservationForm.setDisplayTargetCalibrationBeam(
    		   OptionsDelegate.allowedToDisplay(OpticonConstants.TARGET_CALIBRATION_BEAM,
    		   fieldsDefinitionType));	
       
       if( OptionsDelegate.allowedToDisplay(Constants.AUTO_TARGET_COMMIT,
    		   fieldsDefinitionType)){
    	   opticonObservationForm.setAutoCommit(true);
    	   opticonObservationForm.setTargetSummaryValues(new ArrayList());
       }
      
       
		//set the required values
		opticonObservationForm.setRequiredSeeingRange(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_SEEING_RANGE,
						fieldsDefinitionType));	
		opticonObservationForm.setRequiredTargetFlux(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_FLUX,
						fieldsDefinitionType));	
		opticonObservationForm.setRequiredTargetFluxFrequency(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_FLUX_FREQUENCY,
						fieldsDefinitionType));	
		opticonObservationForm.setRequiredTargetSpectralIndex(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_SPECTRAL_INDEX,
						fieldsDefinitionType));	
		opticonObservationForm.setRequiredTargetSkyQuality(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_SKY_QUALITIES,
						fieldsDefinitionType));	
		opticonObservationForm.setRequiredSn(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_SN,
						fieldsDefinitionType));	
		opticonObservationForm.setRequiredTargetDiameter(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_DIAMETER,
						fieldsDefinitionType));	
		opticonObservationForm.setRequiredTargetRa(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_RA,
						fieldsDefinitionType));	
		opticonObservationForm.setRequiredTargetDec(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_DEC,
						fieldsDefinitionType));	
		opticonObservationForm.setRequiredTargetSubbandList(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_SUBBAND_LIST,
						fieldsDefinitionType));
		
		opticonObservationForm.setRequiredTargetExposureTime(
				OptionsDelegate.isRequired(OpticonConstants.TARGET_EXPOSURE_TIME,
						fieldsDefinitionType));
    }
    

	public void fillTargets(ObservingRequest observingRequest, ObservationForm observationForm,Observation selectedObservation) 
		throws DatabaseException
	{
		OpticonObservationForm opticonObservationForm = (OpticonObservationForm) observationForm;
		List targets = null;
		if(selectedObservation != null ){
			targets= selectedObservation.getTargets();
		}
		// observingrequest should have 0 targets if on tabbed page
		else if (observingRequest!= null){
			targets=observingRequest.getTargets();
        }
		
		// combine existing targets (from observation) and new targets (from temp targets)
    	// new targets are stored in observingrequest??
		List tempList = new ArrayList();
		if(targets != null){ // && opticonObservationForm.getTempTargets() .size() == 0){
    		//FIXME remove empty objects in list !!!
			Iterator tarit = targets.iterator();
			while(tarit.hasNext()){
				if(tarit.next() == null){
					tarit.remove();
				}
			}
			// sort the list if not copy/edit/delete target button is selected
			if(opticonObservationForm.getSelectedTargetId() < 0){
				// reorder target list
		        Collections.sort(targets, new Comparator<OpticonTarget>() {
		        public int compare(OpticonTarget o1, OpticonTarget o2) {
		        		if(o1==null ||  o2 ==null){
		        			return 0;
		        		}
		        		if(o1.getRunnumber() == null){
		        			o1.setRunnumber("1");
		        		}
		        		if(o2.getRunnumber() == null){
		        			o2.setRunnumber("1");
		        		}
		        		if(o1.isFirstInRun() && new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber())) == 0){
		        			return -1;
		        		}
		        		if(o2.isFirstInRun() && new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber())) == 0){
		        			return 1;
		        		}
		        		return new Integer(o1.getRunnumber()).compareTo(new Integer(o2.getRunnumber()));
		            }
		        });
			}
    		tempList.addAll(targets);
    		observingRequest.setTargets(targets);
    	}
		opticonObservationForm.setTempTargets(tempList);
    	
    	List summaryColumns = createSummaryColumns(opticonObservationForm);
        	
        String[] SummaryLabels = new String[summaryColumns.size()];
        Iterator columnIt = summaryColumns.iterator();
        int i=0;
        while (columnIt.hasNext()){
        	String name = (String) columnIt.next();
        	try{
        		ProposalData proposalData = (ProposalData) request.getSession().getAttribute(Constants.PROPOSAL_DATA);
        		SummaryLabels[i] = NorthStarUtils.getLabel(request.getSession().getServletContext(),proposalData.getProposal(),"label.opticon.targettable."+name);//opticonResources.getMessage("label.opticon.targettable."+name);
        	}catch(FileNotFoundException fe){
        		log.warn("no label found "+fe.getMessage());
        	}
        	i++;
        }
        columnIt = null;
        opticonObservationForm.setTargetSummaryLabels(SummaryLabels);
        
        // done
		List targetSummaryValues = new ArrayList();
		double totalSpecifiedTime = 0;
		// do not show summary values if auto commit is on (only one target)
		FieldsDefinitionType fieldsDefinitionType = (FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();
		if(OptionsDelegate.allowedToDisplay(Constants.AUTO_TARGET_COMMIT,
	    		   fieldsDefinitionType) ){
			if(observationForm.getTempTargets() != null && observationForm.getTempTargets().size() >0 ){
	        	observationForm.setSelectedTargetId(0);
	        	observationForm.setEditTargetButton(0, "edit");
	         }
		}else{
			// get value for maximum targets in same run from config file
			List maxtargetList = new ArrayList();
			String Urlmax = null;
			maxtargetList = OptionsUtils.getLabelValueBeans(Constants.MAX_TARGETS_SAME_RUN ,
						new HashMap(), contextConfiguration);
			
			
			if (maxtargetList != null && maxtargetList.size() > 1){
				LabelValueBean urlValue = (LabelValueBean) maxtargetList.get(1);
				Urlmax =  urlValue.getValue();
				opticonObservationForm.setMaxNumberOfTargetsonSameRunReached(""+AstronConverter.toInteger(Urlmax).intValue());
			}
			
			opticonObservationForm.getRunnumbers().clear();
			Set<String> numberSet = new HashSet();
			int maxNumber = 0;
			
			// temptargets is the current list of targets...
			Iterator targetsIt = opticonObservationForm.getTempTargets().iterator();
			
			while (targetsIt.hasNext()) 
			{
				OpticonTarget opticonTarget = (OpticonTarget) targetsIt.next();
				List selectedRuns = fillSelectedRuns(opticonTarget,observingRequest.getObservations());
				opticonTarget.setSelectedRuns(selectedRuns);			
				
				Double totalExposureTime = NorthStarDelegate.getResourceValue(
						opticonTarget.getAllocations(),	OpticonConstants.TOTAL_DURATION);
				if (totalExposureTime != null) 
				{
					totalSpecifiedTime += totalExposureTime.doubleValue();
				}
				// put each target in the list				
				List targetValues = addTarget(opticonTarget, summaryColumns);
				targetSummaryValues.add(targetValues);
				
				// get runnumber
				String number = opticonTarget.getRunnumber();
				if(number != null && number != ""){
					String run = opticonTarget.getObservingrun();
					// only add number if it is in the selected observation?
					//if(run != null && run.equalsIgnoreCase(opticonObservationForm.getSelectedObservingRun())){
						numberSet.add(number);
						if(Integer.parseInt(number) > maxNumber){
							maxNumber = Integer.parseInt(number);
						}
					//}
				}
			}
			
			for(String nr : numberSet){
				LabelValueBean lvb = new LabelValueBean();
				lvb.setLabel(nr); 
				lvb.setValue(nr);
				opticonObservationForm.getRunnumbers().add(lvb);
			}
			// always add one extra, and fill the numberset
			maxNumber++;
			maxNumber =findFirstMissing(numberSet.toArray(), maxNumber);
			
			LabelValueBean lvb = new LabelValueBean();
			lvb.setLabel(String.valueOf(maxNumber) + " (new)"); 
			lvb.setValue(String.valueOf(maxNumber));
			opticonObservationForm.getRunnumbers().add(lvb);
			//opticonObservationForm.setSelectedRunnumber(String.valueOf(maxNumber));
			
		}
		Collections.sort(opticonObservationForm.getRunnumbers());
		opticonObservationForm.setTargetSummaryValues(targetSummaryValues);
		opticonObservationForm.setTotalDuration(OpticonTargetUtils.getHoursFromSeconds(
				new Double(totalSpecifiedTime)));
		
		List urlList = new ArrayList();
		String Url = null;
		urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.MAX_TARGETS ,
					new HashMap(), contextConfiguration);
		opticonObservationForm.setMaxNumberOfTargetsReached(false);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			if (opticonObservationForm.getTempTargets().size() >= AstronConverter.toInteger(Url).intValue()){
				if(opticonObservationForm.getSelectedTargetId() < 0){
					opticonObservationForm.setMaxNumberOfTargetsReached(true);
				}
			}
		}
		
		if(opticonObservationForm.isTargetSpecifySubbandList() == false){
			opticonObservationForm.setDisplayTargetBandwidth(true);
			opticonObservationForm.setDisplayTargetCentralFrequency(true);
			opticonObservationForm.setTargetSubbandList(null);
			opticonObservationForm.setTargetTotalSubbandList(null);
		}else{
			opticonObservationForm.setDisplayTargetBandwidth(false);
			opticonObservationForm.setDisplayTargetCentralFrequency(false);
		}
		
		// add observation to list of runs or pipelines and fill runnumbers
		opticonObservationForm.getObservingRuns().clear();
		opticonObservationForm.getPipelines().clear();
		List<Observation> obs = observingRequest.getObservations();
		
		int observations = 0;
		int pipelines = 0;
		
		LabelValueBean lvb = new LabelValueBean();
		lvb.setLabel(" "); 
		lvb.setValue("");
		opticonObservationForm.getObservingRuns().add(lvb);
		opticonObservationForm.getPipelines().add(lvb);
		for(int j=0;j<obs.size();j++){
			OpticonObservation ob = (OpticonObservation) obs.get(j);
			
// HAH 11/7/18 Change request SOS https://support.astron.nl/jira/browse/ROHD-970
//			if(!ob.getInstrument().isPiggyBack())
//			{
				char chr = (char) (observations+65);
				lvb = new LabelValueBean();
				lvb.setLabel(String.valueOf(chr));
				lvb.setValue(String.valueOf(chr));
				opticonObservationForm.getObservingRuns().add(lvb);
				observations++;
//			}
//			else
//			{
//				observations++;
//			}
		}
		
		List<Observation> pipes = observingRequest.getPipelines();
		for(int j=0;j<pipes.size();j++){
			char chr = (char) (j+65);
			lvb = new LabelValueBean();
			lvb.setLabel(String.valueOf(chr));
			lvb.setValue(String.valueOf(chr));
			opticonObservationForm.getPipelines().add(lvb);
		}
		
		List<Observation> tbbObservationList = observingRequest.getTbbObservations();
		for(int j=0;j<tbbObservationList.size();j++){
			char chr = (char) (j+65);
			lvb = new LabelValueBean();
			lvb.setLabel(String.valueOf(chr));
			lvb.setValue(String.valueOf(chr));
			opticonObservationForm.getTbbObservations().add(lvb);
		}
	}
	
	private int findFirstMissing(Object[] array, int maxNumber) {
		String[] sarray = Arrays.copyOf(array, array.length, String[].class);
		if (sarray != null) {
			int intArray[] = new int[sarray.length];
			for (int i = 0; i < sarray.length; i++) {
			intArray[i] = Integer.parseInt(sarray[i]);
			}
		for(int i = 0; i < intArray.length; ++i) {
            while(intArray[i] != i + 1) {
                if(intArray[i] <= 0 || intArray[i] > intArray.length || intArray[i] == intArray[intArray[i]-1])
                    break;
                int temp = intArray[i];
                intArray[i] = intArray[temp - 1];
                intArray[temp - 1] = temp;
            }
        }
         
        for(int i = 0; i < intArray.length; ++i)
            if(intArray[i] != i + 1)
                return i + 1;
        return intArray.length + 1;
		}
		return maxNumber;
	}

	protected void processTargets(List nTargets, ObservingRequest observingRequest) {
		if(observingRequest.getObservations() != null && 
				observingRequest.getObservations().size() >0){
			int obsSize = observingRequest.getObservations().size();
			//TODO give a warning or something
			if(obsSize>50){
				obsSize=50;
			}
			for(int i=0;i< obsSize;i++){
				// each observation gets a character:
				Observation obs = (Observation) observingRequest.getObservations().get(i);
				char chr = (char) (i+65);
				
				Iterator targit = nTargets.iterator();
				// get the original list of targets.
				List targetList = obs.getTargets();
				// now we go trough all targets and all their selected runs and check if the character matches.
				while (targit.hasNext()){
					Target target = (Target) targit.next();
					if(target.getSelectedRuns() != null && target.getSelectedRuns().size() >0 ){
						for (int j=0; j<target.getSelectedRuns().size(); j++) {
							String run = (String) target.getSelectedRuns().get(j);
							if(run.indexOf(chr) >= 0){
								targetList.add(target);
							}
						}
					}
				}
			
				obs.setTargets(targetList);
			}
		}
		
		if(observingRequest.getPipelines() != null && 
				observingRequest.getPipelines().size() >0){
			int obsSize = observingRequest.getPipelines().size();
			//TODO give a warning or something
			if(obsSize>50){
				obsSize=50;
			}
			for(int i=0;i< obsSize;i++){
				// each observation gets a character:
				Observation obs = (Observation) observingRequest.getPipelines().get(i);
				char chr = (char) (i+65);
				
				Iterator targit = nTargets.iterator();
				// get the original list of targets.
				List targetList = obs.getTargets();
				// now we go trough all targets and all their selected runs and check if the character matches.
				while (targit.hasNext()){
					Target target = (Target) targit.next();
					if(target.getSelectedRuns() != null && target.getSelectedRuns().size() >0 ){
						for (int j=0; j<target.getSelectedRuns().size(); j++) {
							String run = (String) target.getSelectedRuns().get(j);
							if(run.indexOf(chr) >= 0){
								targetList.add(target);
							}
						}
					}
				}
			
				obs.setTargets(targetList);
			}
		}
	}
	
	
	private List fillSelectedRuns(OpticonTarget opticonTarget, List observations) {
		List result = new ArrayList();
		int obsSize = observations.size();
		//TODO give a warning or something
		if(obsSize>50){
			obsSize=50;
		}
		for(int i=0;i<observations.size();i++){
			OpticonObservation obs = (OpticonObservation) observations.get(i);
			List obstargets= obs.getTargets();
			if(obstargets != null){ // new unstored observation
				Iterator obsit = obstargets.iterator();
				while(obsit.hasNext() ){
					OpticonTarget obsTarget = (OpticonTarget) obsit.next();
					if(obsTarget.equals(opticonTarget)){
						char chr = (char) (i+65);
						result.add(String.valueOf(chr) );
					}
				}
			}
		}
		if(result.size() > 0){
			return result;
		}
		return null;
	}


	public void fillObservationForm(Observation observation, ObservationForm observationForm) throws DatabaseException 
	{
        OpticonObservation opticonObservation = (OpticonObservation) observation;
        OpticonObservationForm opticonObservationForm = (OpticonObservationForm) observationForm;
        
		opticonObservationForm.setSelectedObservationTime(opticonObservation.getObservationPhase());
		
		//
		// get calculator url from config file
		String Url = null;
		java.net.URL serviceLocation = null;
		List urlList = OptionsUtils.getLabelValueBeans(OpticonConstants.STORAGECALCULATORURL ,
				new HashMap(), contextConfiguration);
		if (urlList != null && urlList.size() > 1){
			LabelValueBean urlValue = (LabelValueBean) urlList.get(1);
			Url =  urlValue.getValue();
			try {
				serviceLocation = new java.net.URL(Url);
			} catch (MalformedURLException e) {
				log.warn("invalid astron service Url: "+Url);
				serviceLocation = null;
			}
		}

//		opticonObservationForm.setObservationNoiseLevel(AstronConverter.toString(opticonObservation.getNoiseLevel()));

		super.fillObservationForm(observation,observationForm);
    }

	public void fillTargetFields(int targetIndex, ObservingRequest observingRequest, ObservationForm observationForm)
		throws DatabaseException
	{
		OpticonObservationForm opticonObservationForm = (OpticonObservationForm) observationForm;
		opticonObservationForm.setSelectedRuns(null);
		if(opticonObservationForm.getTargetSummaryValues().size() == 0 ){
			opticonObservationForm.setEnableTimeAndObservation(true);
			opticonObservationForm.setTargetFirstInRun(true);
		}else{
			opticonObservationForm.setSelectedObservingRun("");
			opticonObservationForm.setEnableTimeAndObservation(false);
			opticonObservationForm.setTargetFirstInRun(false);
		}
		if (targetIndex >= 0)
		{
				OpticonTarget selectedTarget = (OpticonTarget) observationForm.getTempTargets().get(targetIndex);
				opticonObservationForm.setFieldName(selectedTarget.getFieldName() );
				// targetId will be -1 if this is a new target
				Integer tid = selectedTarget.getId();
				if(tid == null){
					opticonObservationForm.setTargetId("-1");
				}else{
					opticonObservationForm.setTargetId(tid.toString());
				}
				if (selectedTarget.getRa() != null && selectedTarget.getDecl() != null) 
				{
					AstroCoordinate coordinate = new AstroCoordinate(
							selectedTarget.getRa().doubleValue(),
							selectedTarget.getDecl().doubleValue());
					opticonObservationForm.setRa(coordinate.RAString());
					opticonObservationForm.setDec(coordinate.DecString());
				} 
				else 
				{
					opticonObservationForm.setRa(null);
					opticonObservationForm.setDec(null);
				}
				opticonObservationForm.setEpoch(selectedTarget.getEpoch());
				double totalSpecifiedTime = 0;
				Double totalExposureTime = NorthStarDelegate.getResourceValue(
						selectedTarget.getAllocations(),	OpticonConstants.TOTAL_DURATION);
				if (totalExposureTime != null) 
				{
					totalSpecifiedTime += totalExposureTime.doubleValue();
				}
				
				if(totalSpecifiedTime > 0){
					opticonObservationForm.setEnableTimeAndObservation(true);
				}
				
				opticonObservationForm.setTargetExposureTime(AstronConverter.getMinutesFromSeconds(totalExposureTime));
				opticonObservationForm.setSelectedTargetMoon(selectedTarget.getMoon());
				opticonObservationForm.setSelectedTargetSeeing(selectedTarget.getSeeing());
				opticonObservationForm.setTargetSeeingUpper(AstronConverter.toString(selectedTarget.getSeeingUpper())); // .toString()
				opticonObservationForm.setTargetSeeingLower(AstronConverter.toString(selectedTarget.getSeeingLower()));
				opticonObservationForm.setSelectedTargetWater(selectedTarget.getWater());
				opticonObservationForm.setSelectedTargetSn(AstronConverter.toString(selectedTarget.getSn()));
				opticonObservationForm.setSelectedTargetMagnitude(AstronConverter.toString(selectedTarget.getMagnitude()));
				opticonObservationForm.setTargetFlux(AstronConverter.toString(selectedTarget.getFlux()));
				opticonObservationForm.setTargetFluxFrequency(AstronConverter.toString(selectedTarget.getFluxFrequency()));
				opticonObservationForm.setTargetSpectralIndex(AstronConverter.toString(selectedTarget.getSpectralIndex()));
				opticonObservationForm.setSelectedTargetSkyQuality(selectedTarget.getSkyQuality());
				opticonObservationForm.setTargetDiameter(AstronConverter.toString(selectedTarget.getDiameter()));
				opticonObservationForm.setTargetStorage(AstronConverter.toString(selectedTarget.getStorage() ));
				opticonObservationForm.setSelectedTargetComments(selectedTarget.getComments());
				opticonObservationForm.setTargetOpportunity(selectedTarget.isOpportunity());
				opticonObservationForm.setTargetSpecifySubbandList(selectedTarget.isSpecifySubbandList());
				opticonObservationForm.setTargetBandwidth(AstronConverter.toString(selectedTarget.getBandwidth()));
				opticonObservationForm.setTargetCentralFrequency(AstronConverter.toString(selectedTarget.getCentralFrequency()));
				opticonObservationForm.setTargetSubbandList(selectedTarget.getSubbandList());
				opticonObservationForm.setTargetTotalSubbandList(sumSubbandList(selectedTarget.getSubbandList()));
				
				if(selectedTarget.getSelectedRuns() != null){
					opticonObservationForm.setSelectedRuns(OptionsUtils.getArray(selectedTarget.getSelectedRuns()));
				} else{
					opticonObservationForm.setSelectedRuns(null);
				}
				opticonObservationForm.setSelectedObservingRun(selectedTarget.getObservingrun());
				//opticonObservationForm.setSelectedPipeline(selectedTarget.getPipeline());
				opticonObservationForm.setSelectedPipeline(selectedTarget.getPipeline()!=null?selectedTarget.getPipeline().split(";"):null);
				opticonObservationForm.setTargetFirstInRun(selectedTarget.isFirstInRun());
				opticonObservationForm.setSelectedRunnumber(selectedTarget.getRunnumber());
				if(selectedTarget.getRunnumber() != null && 
						opticonObservationForm.getRunnumbers().size() == Integer.valueOf(selectedTarget.getRunnumber()).intValue()) {
					opticonObservationForm.setEnableTimeAndObservation(true);
				}
				opticonObservationForm.setCalibrationBeam(selectedTarget.isCalibrationBeam() );
				
				
		}
		else if(opticonObservationForm.getClearTargetButton() != null)
		{
			opticonObservationForm.setFieldName(null);
			opticonObservationForm.setTargetId(null);
			opticonObservationForm.setRa(null);
			opticonObservationForm.setDec(null);
			opticonObservationForm.setEpoch(null);		
			opticonObservationForm.setTargetExposureTime(null);
			opticonObservationForm.setSelectedTargetMoon(
					OptionsUtils.getDefaultSelectedOptionOrValue(OpticonConstants.TARGET_MOON,
							null,contextConfiguration));
			opticonObservationForm.setSelectedTargetSeeing(
					OptionsUtils.getDefaultSelectedOptionOrValue(OpticonConstants.TARGET_SEEING,
							null,contextConfiguration));
			opticonObservationForm.setSelectedTargetWater(
					OptionsUtils.getDefaultSelectedOptionOrValue(OpticonConstants.TARGET_WATER,
		                    null,contextConfiguration));
			opticonObservationForm.setSelectedTargetSn(null);
			opticonObservationForm.setSelectedTargetMagnitude(null);
			opticonObservationForm.setSelectedTargetComments(null);
			opticonObservationForm.setTargetFlux(null);
			opticonObservationForm.setTargetFluxFrequency(null);
			opticonObservationForm.setTargetSpectralIndex(null);
			opticonObservationForm.setTargetSeeingLower(null);
			opticonObservationForm.setTargetSeeingUpper(null);
			opticonObservationForm.setSelectedTargetSkyQuality(null);
			opticonObservationForm.setTargetDiameter(null);
			opticonObservationForm.setTargetStorage(null);
			opticonObservationForm.setTargetOpportunity(false);
			opticonObservationForm.setSelectedRuns(null);
			opticonObservationForm.setTargetCentralFrequency(null);
			opticonObservationForm.setTargetBandwidth(null);
			opticonObservationForm.setTargetSubbandList(null);
			opticonObservationForm.setTargetTotalSubbandList(null);
			opticonObservationForm.setTargetSpecifySubbandList(false);
			opticonObservationForm.setDisplayTargetBandwidth(false);
			opticonObservationForm.setDisplayTargetCentralFrequency(false);
			opticonObservationForm.setSelectedObservingRun(null);
			opticonObservationForm.setSelectedPipeline(null);
			opticonObservationForm.setSelectedRunnumber(null);
			opticonObservationForm.setCalibrationBeam(false);
			opticonObservationForm.setTargetFirstInRun(false);
		}
		
		/*// default specify subbands via central frequency
		//if(opticonObservationForm.isTargetSpecifySubbandList() == false){
			opticonObservationForm.setDisplayTargetBandwidth(true);
			opticonObservationForm.setDisplayTargetCentralFrequency(true);
			//if(!AstronValidator.isBlankOrNull(opticonObservationForm.getTargetCentralFrequency()) && AstronValidator.isBlankOrNull(opticonObservationForm.getTargetBandwidth()))
			opticonObservationForm.setTargetSubbandList(calculateTargetSubbandList(observingRequest,opticonObservationForm));
			//else
			//	opticonObservationForm.setTargetSubbandList(null);
			log.warn("clear button pressed"+opticonObservationForm.getTargetSubbandList());
		}else{
			opticonObservationForm.setTargetSpecifySubbandList(true);
			opticonObservationForm.setDisplayTargetBandwidth(false);
			opticonObservationForm.setDisplayTargetCentralFrequency(false);
		}*/
	}
 	
	private String calculateTargetSubbandList(ObservingRequest observingRequest, OpticonObservationForm opticonObservationForm) {
		// check for invalid input
		/*if(target.getObservingrun() == null){
			return "no observation selected";
		}
		
		char chr = (char) target.getObservingrun().charAt(0);
		int pos = chr - 65;
		if (pos < 0 || pos > observingRequest.getObservations().size()){
			return "invalid observation selected";
		}
		
		OpticonObservation obs = (OpticonObservation) observingRequest.getObservations().get(pos);
		*/
		String result = "no value";
		CalculateSubbandListAction calculator = new CalculateSubbandListAction();
		boolean isEmpty=true;
		try{
			double centralFrequency = Double.parseDouble(opticonObservationForm.getTargetCentralFrequency());
			double bandwidth = Double.parseDouble(opticonObservationForm.getTargetBandwidth());
			if(centralFrequency>0 && bandwidth>0)
				isEmpty = false;
			result = calculator.calculate(200,centralFrequency,bandwidth);
		}catch(Exception ex){
			result = "invalid numbers";
			if(isEmpty)
				result="";
		}	
		
		return result;
	}

	protected void displayInstruments(String instrumentConfiguration, Map enteredTelescopes,
			OpticonObservationForm opticonObservationForm) {
		
			}
	
	public void fillObservationOptions(Observation observation,ObservationForm observationForm) throws DatabaseException 
	{
		
		OpticonObservationForm opticonObservationForm = (OpticonObservationForm) observationForm;
		OpticonObservation opticonObservation = (OpticonObservation) observation;
        
        List telescopeConfigurations = OptionsUtils.getLabelValueBeans(
        		OpticonConstants.TELESCOPE_CONFIGURATIONS, new HashMap(), contextConfiguration);

        FieldsDefinitionType fieldsDefinitionType = 
			(FieldsDefinitionType) contextConfiguration.getFieldsDefinitionType();

/*
		 * Check if the list contains more than 2 items. (Non specified is also
		 * an item!) If so a dropbox is needed.
		 */
		if (telescopeConfigurations.size() < 3)
		{
			//remove the Non specified
			telescopeConfigurations.remove(0);
			/* one element left, so only one option. Fill both fields. The form is for
			 * the check in the jsp to show the  instruments, en the data entity is used to
			 * generate the list of possible instruments
			 */
			opticonObservation.setTelescopeConfiguration(
					((LabelValueBean) telescopeConfigurations.get(0)).getLabel());
		}   

		/*
		 *   Fill or create instrument form if needed, 
		 */
		InstrumentBean instrBean = new InstrumentBean();
		// order is important, first load fillform, then filloptions
		SetUpInstrumentBean.fillForm(request, opticonObservation, instrBean);
		SetUpInstrumentBean.setDefaults(contextConfiguration, instrBean);
		SetUpInstrumentBean.fillOptions(request, instrBean);		
		
		opticonObservationForm.setShowInstrumentBean("true");
		if(instrBean.isDisplayCalculatedSize()){
			if(sCalc == null){
				sCalc = SetUpInstrumentBean.bindCalc(contextConfiguration);
			}
			
			if(sCalc != null){
				Double dataSize;
				try {
					dataSize = SetUpInstrumentBean.calculateStorage(opticonObservation, 0, 0, sCalc);
					BigDecimal bd = new BigDecimal(dataSize.doubleValue());
				    bd = bd.setScale(2,BigDecimal.ROUND_UP);
				    instrBean.setEstimatedDataStorage(bd.toString()); //bd.toEngineeringString() not java 1.4 compatible
				    instrBean.setDataStorageSpecifics(SetUpInstrumentBean.getDataStorageSpecifics(opticonObservation));
				    
				    double cepload = SetUpInstrumentBean.calculateCepLoad(opticonObservation,contextConfiguration,dataSize.doubleValue());
				    bd = new BigDecimal(cepload);
				    bd = bd.setScale(0,BigDecimal.ROUND_UP);
				    instrBean.setEstimatedCepLoad(bd.toString());
				    
				} catch (ServiceException e) {
					e.printStackTrace();
				}
			}else{
				log.warn("could not bind calculator");
			}
		}

//        opticonObservationForm.setDisplayObservationNoiseLevel(
//				OptionsDelegate.allowedToDisplay(OpticonConstants.OBSERVATION_NOISE_LEVEL,
//						fieldsDefinitionType));
//        opticonObservationForm.setRequiredObservationNoiseLevel(
//				OptionsDelegate.isRequired(OpticonConstants.OBSERVATION_NOISE_LEVEL,
//						fieldsDefinitionType));

		request.setAttribute("instrumentBean",instrBean);
	}

	private List createSummaryColumns(ObservationForm observationForm){
		// here the used columns are defined !!
		OpticonObservationForm opticonObservationForm = (OpticonObservationForm) observationForm;

        List summaryColumns = new ArrayList();
        if (opticonObservationForm.isDisplayTargetSelectRunnumber()){
        	summaryColumns.add("Run#");
        }//make run number as the first column
        summaryColumns.add("Field");
        summaryColumns.add("Ra");
        summaryColumns.add("Dec");
        summaryColumns.add("Epoch");
        //summaryColumns.add("Exposure");
        if (opticonObservationForm.isDisplayTargetExposureTime()){
        	summaryColumns.add("Exposure");
        }
        if (opticonObservationForm.isDisplayTargetMoon()){
        	summaryColumns.add("Moon");
        }
        if (opticonObservationForm.isDisplaySeeingOptions()){
        	summaryColumns.add("Seeing");
        }
        if (opticonObservationForm.isDisplaySeeingRange()){
        	summaryColumns.add("SeeingLow");
        	summaryColumns.add("SeeingUp");
        }
        if (opticonObservationForm.isDisplayTargetWater()){
        	summaryColumns.add("Water");
        }
        if (opticonObservationForm.isDisplayTargetMagnitude()){
        	summaryColumns.add("Magnitude");
        }
        if (opticonObservationForm.isDisplayTargetSn()){
        	summaryColumns.add("SN");
        }
        /*if (opticonObservationForm.isDisplayTargetFlux()){
        	summaryColumns.add("Flux");
        }
        if (opticonObservationForm.isDisplayTargetFluxFrequency()){
        	summaryColumns.add("FluxFrequency");
        }
        if (opticonObservationForm.isDisplayTargetSpectralIndex()){
        	summaryColumns.add("SpectralIndex");
        }*/
        if (opticonObservationForm.isDisplayTargetSkyQuality()){
        	summaryColumns.add("Sky");
        }
        if (opticonObservationForm.isDisplayTargetDiameter()){
        	summaryColumns.add("Diameter");
        }
        if (opticonObservationForm.isDisplayTargetSubbandList()){
        	summaryColumns.add("Subbands");
        }
        if (opticonObservationForm.isDisplayTargetTotalSubbandList()){
        	summaryColumns.add("TotalSubbands");
        }        
        if (opticonObservationForm.isDisplayTargetStorage()){
        	summaryColumns.add("Storage");
        }
        if (opticonObservationForm.isDisplayTargetRuns()){
        	summaryColumns.add("Runs");
        }
        if (opticonObservationForm.isDisplayTargetCalibrationBeam()){
        	summaryColumns.add("Calibr.");
        }
       
        if (opticonObservationForm.isSingleTargetPerRun()){
        	summaryColumns.add("Obs.");
        }
        if (opticonObservationForm.isDisplayTargetSelectPipeline()){
        	summaryColumns.add("Pipe.");
        }
        summaryColumns.add("Comments");
        return summaryColumns;
	}
	
	private List addTarget(OpticonTarget opticonTarget, List summaryColumns){
		Double totalExposureTime = NorthStarDelegate.getResourceValue(
				opticonTarget.getAllocations(),	OpticonConstants.TOTAL_DURATION);
		
		// fill list according to column names
		List targetValues = new ArrayList();
		
		Iterator columnIt = summaryColumns.iterator();
		while (columnIt.hasNext()){
			String colName= (String) columnIt.next();
			if (colName.equals("Field")){
				targetValues.add(new ValueBean(opticonTarget.getFieldName()));
			}
			//Added by Roel
			else if (colName.equals("Run#")) {
				targetValues.add(new ValueBean(opticonTarget.getRunnumber()));
			}
			else if (colName.equals("Ra")){
				if (opticonTarget.getRa() != null && opticonTarget.getDecl() != null) 
				{
					AstroCoordinate coordinate = new AstroCoordinate(
							opticonTarget.getRa().doubleValue(),
							opticonTarget.getDecl().doubleValue());
					targetValues.add(new ValueBean(coordinate.RAString()));
					targetValues.add(new ValueBean(coordinate.DecString()));
				} 
				else 
				{
					targetValues.add(new ValueBean(null));
					targetValues.add(new ValueBean(null));
				}
			}
			else if (colName.equals("Epoch")){	
				targetValues.add(new ValueBean(opticonTarget.getEpoch()));
			}
			else if (colName.equals("Exposure")){	
				//if(opticonTarget.isFirstInRun())
				targetValues.add(new ValueBean(OpticonTargetUtils.getHoursFromSeconds(totalExposureTime)));
				//else
				//	targetValues.add(new ValueBean(""));
			}
			else if (colName.equals("Moon")){		
				targetValues.add(new ValueBean(opticonTarget.getMoon()));
			}
			else if (colName.equals("Seeing")){		
				targetValues.add(new ValueBean(opticonTarget.getSeeing()));// , opticonObservationForm.isDisplaySeeingOptions()));
			}
			else if (colName.equals("SeeingLow")){		
				targetValues.add(new ValueBean(AstronConverter.toString(opticonTarget.getSeeingLower() ))); 
			}
			else if (colName.equals("SeeingUp")){		
				targetValues.add(new ValueBean(AstronConverter.toString(opticonTarget.getSeeingUpper()))); 
			}
			else if (colName.equals("Water")){		
				targetValues.add(new ValueBean(opticonTarget.getWater()));
			}
			else if (colName.equals("SN")){		
				targetValues.add(new ValueBean(AstronConverter.toString(opticonTarget.getSn())));
			}
			else if (colName.equals("Magnitude")){		
				targetValues.add(new ValueBean(AstronConverter.toString(opticonTarget.getMagnitude())));
			}
			else if (colName.equals("Flux")){		
				targetValues.add(new ValueBean(AstronConverter.toString(opticonTarget.getFlux())));
			}
			else if (colName.equals("FluxFrequency")){		
				targetValues.add(new ValueBean(AstronConverter.toString(opticonTarget.getFluxFrequency())));
			}
			else if (colName.equals("Subbands")){	
				String sublist = opticonTarget.getSubbandList();
				if(opticonTarget.getSubbandList() != null && opticonTarget.getSubbandList().length() > 24){
					sublist = opticonTarget.getSubbandList().substring(0,24) + "...";
				}
				targetValues.add(new ValueBean(sublist));
			}
			else if (colName.equals("TotalSubbands")){	
				targetValues.add(new ValueBean(sumSubbandList(opticonTarget.getSubbandList())));
			}			
			else if (colName.equals("SpectralIndex")){		
				targetValues.add(new ValueBean(AstronConverter.toString(opticonTarget.getSpectralIndex())));
			}
			else if (colName.equals("Sky")){		
				targetValues.add(new ValueBean(opticonTarget.getSkyQuality()));
			}
			else if (colName.equals("Diameter")){	
				if(opticonTarget.getDiameter() != null && opticonTarget.getDiameter() == new Double(0d)){
					targetValues.add(new ValueBean("point"));
				}else{
					targetValues.add(new ValueBean(AstronConverter.toString(opticonTarget.getDiameter() )));
				}
			}
			else if (colName.equals("Runs")){	
				if(opticonTarget.getSelectedRuns() != null ){
					String runs = "";
					Iterator runit = opticonTarget.getSelectedRuns().iterator();
					while (runit.hasNext()){
						String run = (String) runit.next();
						runs += " "+run;
					}
					targetValues.add(new ValueBean(runs));
				}else{
					targetValues.add(new ValueBean(" "));
				}
			}
			else if (colName.equals("Comments")){	
				String coms = opticonTarget.getComments();
				if(coms != null && coms.length() > 24){
					coms = coms.substring(0,21) + "..";
				}
				targetValues.add(new ValueBean(coms));
			}
			else if (colName.equals("estimatedStorage")){		
				targetValues.add(new ValueBean(AstronConverter.toString(opticonTarget.getStorage())));
			}
			else if (colName.equals("Storage")){		
				targetValues.add(new ValueBean(AstronConverter.toString(opticonTarget.getStorage())));
			}
			else if (colName.equals("Run#")){	
				if( opticonTarget.isFirstInRun())
				targetValues.add(new ValueBean(opticonTarget.getRunnumber()));
				else
					targetValues.add(new ValueBean(""));
			}
			else if (colName.equals("Calibr.")){
				if(opticonTarget.isCalibrationBeam()){
					targetValues.add(new ValueBean("Y"));
				}else{
					targetValues.add(new ValueBean(""));
				}
			}
			else if (colName.equals("Obs.")){		
				targetValues.add(new ValueBean(opticonTarget.getObservingrun()));
			}
			else if (colName.equals("Pipe.")){		
				targetValues.add(new ValueBean(opticonTarget.getPipeline()));
			}else if (colName.equals("Pipe.")){		
				targetValues.add(new ValueBean(opticonTarget.getPipeline()));
			}
		}
		return targetValues;
	}

	private String sumSubbandList( String subbandList) {
		String targetTotalSubbandList = null;
		try {
			int sumSubbandList = OpticonTargetUtils.getNumberofSubBands(subbandList);
			targetTotalSubbandList = Integer.toString(sumSubbandList);
		} catch (ParseTargetListException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return targetTotalSubbandList;
	}
}

