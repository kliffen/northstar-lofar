<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@ taglib uri="/WEB-INF/astron.tld" prefix="astron" %>
<tiles:get name="errors" />
<html:form action="/processChangeStatus" method="POST">

<html:hidden property="proposalId"/>
<TABLE>
	<tr>
		<td colspan="2">
			<bean:write name="changeStatusForm" property="proposalTitle"/>
			(<bean:write name="changeStatusForm" property="proposalCode"/>)
		</td>				
	</tr>
	<tr>
		<td colspan="2">&nbsp;</td>				
	</tr>
	<tr>
		<td colspan="2"><b>Change status:</b></td>				
	</tr>
	<tr>
		<td class="right">Current status:&nbsp;</td>
		<td class="bold"><bean:write name="changeStatusForm" property="currentStatus"/>

		</td>
	</tr>		
	<tr>
		<td class="right">Choose new status:&nbsp;</td>
		<td class="left">
					<bean:define id="statusses" name="changeStatusForm" property="statusses"/> 
				<html:select property="selectedStatus">
					<html:options collection="statusses" property="value" labelProperty="label"/>			
				</html:select><span class="error"><astron:errors property="selectedStatus" ifnotexist="*"/></span>

		</td>
	</tr>	
	
	<tr>
		<td colspan="2"><b>Email:</b></td>				
	</tr>
	<tr>
		<td class="right">Subject:&nbsp;</td>
		<td class="left"><html:text property="subject" size="60"/><span class="error"><astron:errors property="subject" ifnotexist="*"/></span></td>
	</tr>	

	<tr>
		<td class="right" valign="top">Message :&nbsp;</td>
		<td class="left"><html:textarea property="message" cols="72" rows="20"/><span class="error"><astron:errors property="message" ifnotexist="*"/></span></td>
	</tr>

</TABLE><br>
<table width="100%">
	<tr>
			<TD width="150">&nbsp;</TD>
		<td align="right"><html:submit property="nextButton"  tabindex="5" styleClass="list_next">Next ></html:submit>
 		</td>
		<td align="right"><html:cancel styleClass="list_decline_t">Cancel</html:cancel></TD>
	</tr>
</table>
</html:form>
