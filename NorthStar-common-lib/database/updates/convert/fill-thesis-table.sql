INSERT INTO thesis (additionalissues_id, indexid,
student_name, supervisor_name, expectedcompletiondate, datarequired  )
SELECT ad.id, 0, ad.studentname,
ad.supervisorname, ad.expectedcompletiondate,
ad.datarequired
FROM additionalissues as ad
WHERE thesis = 1;

ALTER TABLE additionalissues DROP COLUMN studentname;
ALTER TABLE additionalissues DROP COLUMN supervisorname;
ALTER TABLE additionalissues DROP COLUMN expectedcompletiondate;
ALTER TABLE additionalissues DROP COLUMN datarequired;
ALTER TABLE additionalissues DROP COLUMN thesis;